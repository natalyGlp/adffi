<?php
error_reporting(E_ALL);
header('Content-Type: text/html; charset=UTF-8',true);
defined('YII_DEBUG') or define('YII_DEBUG',true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);


ini_set("display_errors", 1);
ini_set("track_errors", 1);
ini_set("html_errors", 1);

// change the following paths if necessary
$yii=dirname(__FILE__).'/../framework/yii.php';
$config=dirname(__FILE__).'/protected/config/console.php';

require_once($yii);

Yii::createConsoleApplication($config)->run();