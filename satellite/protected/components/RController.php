<?php
/**
* Заглушка до того момента, когда цмс перейдет на rights
*/
class RController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to 'column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/site_manage';

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array(
            'Сайты'=>array('/sites/admin'),
            'Управление',
            );

	public function filterRights($filterChain)
	{
		$filterChain->run();
	}
}
