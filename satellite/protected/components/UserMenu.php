<?php
class UserMenu extends CWidget{
	public $title;
	public function init()
	{
		$this->title=CHtml::encode(Yii::app()->user->name);
		parent::init();
	}

	public function run(){
		$this->render('userMenu');
	}
}