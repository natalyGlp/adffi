<?php if (is_array(Yii::app()->controller->menu) && count(Yii::app()->controller->menu)):?>
	<div class="accordion-group">
	    <div class="accordion-heading">
	        <a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">Меню</a>
	    </div>
	    <div class="accordion-body collapse in" id="collapseOne">
	        <div class="accordion-inner">
			<?php
				$this->widget('bootstrap.widgets.TbMenu', array(
				    'type'=>'list',
					'items' => Yii::app()->controller->menu,
					'encodeLabel' => false
				));
			?>
	        </div>
	    </div>
	</div>
<?php endif;?>