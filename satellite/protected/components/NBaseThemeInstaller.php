<?php
/**
 * Базовый класс для установки/удаления темы
 * 
 * @author Sviatoslav <dev@udf.su>
 * @version 1.0
 * @package satellite.components 
 */
abstract class NBaseThemeInstaller extends CApplicationComponent
{
	/**
	 * @var Sites $site модель сайта (опционально)
	 */
	public $site = false;

	const TYPE_VISIT = 1;
	const TYPE_SHOP = 2;
	const TYPE_CATALOG = 3;

	protected $types = array();

	const LAYOUT_NONE = 'none';
	const LAYOUT_RIGHT = 'right';
	const LAYOUT_LEFT = 'left';
	const LAYOUT_BOTH = 'both';
	protected $layouts = array(
		self::LAYOUT_NONE,
		self::LAYOUT_RIGHT,
		self::LAYOUT_LEFT,
		self::LAYOUT_BOTH,
		);

	protected $colors = array();

	protected $modules = array();

	protected $widgets = array();

	/**
	 * @var string $version версия темы. Будет нужна для контроля обновлений
	 */
	protected $version = '1';

	/**
	 * Список slug=>layout всех страниц, у которых должен быть уникальный лейаут
	 */
	protected $uniquePages = array();

	private $_db;

	/**
	 * @param boolean $throwException если true, то функция выкинит исключение, если не задана модель сайта
	 *
	 * @return соединение с бд сайта, если его модель была передана компоненту
	 */
	protected function getDb($throwException = true)
	{
		if(!isset($this->site) && $throwException) {
			throw new CException('Не задано обязательное свойство NBaseThemeInstaller::site');
		}

		if(!isset($this->_db) && isset($this->site)) {
			$this->_db = Yii::createComponent($this->site->dbConfigArray);
		}
		
		return $this->_db;
	}

	/**
	 * Функция, которая вызывается при установке темы на определенный сайт
	 * Если тема должна добавить специфические страницы, модули и виджеты с учетом типа сайта (визитка, магазин...), 
	 * то здесь должна производиться их инициализация, достаточная, что бы потом донастроить с помощью ThemeConfig
	 *
	 * @return boolean true если все прошло успешно
	 */
	public function install()
	{
		$this->db->createCommand('UPDATE {{page}} SET layout="'.$this->id.'"')->execute();
		return true;
	}

	/**
	 * Функция, которая должна откатывать все настройки темы 
	 * (сделанные через NBaseThemeInstaller::install и ThemeConfig)
	 * ВАЖНО: этот метод ни в коем случае не должен приводить потере какой 
	 * либо информации связанной с основным контентом сайта
	 *
	 * TODO: продумать как будет работать смена типа сайта
	 *
	 * @return boolean true если все прошло успешно
	 */
	public function uninstall()
	{
		return true;
	}

	/**
	 * Вызывается в случае, если на сайте установлена более старая тема,
	 * которая должна быть обновлена
	 * 
	 * @return boolean успешность обновления
	 */
	public function update()
	{
		// TODO: запуск миграций
		return true;
	}

	/**
	 * Этот метод должен позаботиться о правильном изменении лейаута и цвета темы
	 *
	 * TODO: продумать как происходить смена цвета темы
	 *
	 * @return boolean true если все прошло успешно
	 */
	public function configure()
	{
		// выбираем тему
		$this->db->createCommand('UPDATE {{page}} SET layout=:theme')->execute(array(
			':theme' => $this->id,
			));
			
		// Проставляем лейауты с учетом страниц с уникальным лейаутом
		$this->db->createCommand('UPDATE {{page}} SET display_type=:layout')->execute(array(
			':layout' => $this->site->layout,
			));

		// Обрабатываем исключения
		$command = $this->db->createCommand('UPDATE {{page}} SET display_type=:layout WHERE slug=:slug');
		foreach ($this->uniquePages as $slug => $layout) 
		{
			$command->execute(array(
				':slug' => $slug,
				':layout' => $layout,
				));
		}

		return true;
	}

	/**
	 * @return string id темы
	 */
	public function getId()
	{
		$id = str_replace('ThemeInstaller', '', get_class($this));
		return lcfirst($id);
	}

	public function getColors()
	{
		return is_array($this->colors) && count($this->colors) > 0 ? array_combine($this->colors, $this->colors) : array();
	}

	public function getWidgets()
	{
		return $this->widgets ? $this->widgets : array();
	}

	public function getModules()
	{
		return $this->modules ? $this->modules : array();
	}

	public function getTypes()
	{
		$types = array(
			self::TYPE_VISIT => 'Сайт визитка',
			self::TYPE_SHOP => 'Онлайн магазин',
			self::TYPE_CATALOG => 'Каталог',
			);
		$types = array_intersect_key($types, array_flip($this->types));
		return $types;
	}

	public function getLayouts()
	{
		$layouts = array(
			self::LAYOUT_NONE => 'Без сайдбара',
			self::LAYOUT_RIGHT => 'Левый сайдбар',
			self::LAYOUT_LEFT => 'Правый сайдбар',
			self::LAYOUT_BOTH => 'Оба сайдбара',
			);
		$layouts = array_intersect_key($layouts, array_flip($this->layouts));
		return $layouts;
	}

	public function getPreview()
	{
		// $assets = Yii::app()->assetManager->publish(Yii::getPathOfAlias('themes.'.$this->id.'.install.assets'), false, -1, YII_DEBUG);

		return 'default.jpg';
	}

	public function getVersion()
	{
		return $this->version;
	}
}