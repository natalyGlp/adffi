<?php
/**
 * Базовый класс для реализации настроек темы
 * 
 * @author Sviatoslav <dev@udf.su>
 * @version 1.0
 * @package satellite.components 
 */

Yii::import('cms.models.settings.Settings');
abstract class BaseThemeConfig extends CFormModel
{
	private $_defaultDb;

	/**
	 * @return array настройки элементов (elements) формы CForm
	 */
	abstract public function getFormConfig();

	/**
	 * Сохраняет данные формы в конфигурации сайта
	 * @return true/false в зависимости от успешности
	 */
	abstract public function configure();

	/**
	 * Производит загрузку данных формы (дефолтных или тех, что были сохранены в предыдущий раз)
	 */
	abstract public function load();

	public function init()
	{
		$this->_defaultDb = Yii::app()->{CONNECTION_NAME};
		$this->load();
	}

	/**
	 * Меняет или создает настройку в сателлите
	 * @param mixed $value    значение настройки
	 * @param string $key      ключ настройки
	 * @param string $category категория настройки
	 */
	protected function setSetting($value, $key, $category = 'satellite')
	{
		// используем бд настраиваемого сайта
		$this->switchDb();

		$setting = Settings::model()->findByAttributes(array(
			'category' => $category,
			'key' => $key,
			));
		if(!$setting)
			$setting = new Settings();

		$setting->attributes = array(
			'category' => $category,
			'key' => $key,
			'value' => serialize($value),
			);

		return $setting->save();
	}

	/**
	 * Возвращает значение настройки
	 * @param string $key      ключ настройки
	 * @param string $category категория настройки
	 */
	protected function getSetting($key, $category = 'satellite')
	{
		// используем бд настраиваемого сайта
		$this->switchDb();

		$setting = Settings::model()->findByAttributes(array(
			'category' => $category,
			'key' => $key,
			));

		return $setting ? @unserialize($setting->value) : null;
	}

	/**
	 * Переключает подключение CONNECTION_NAME
	 * @param  CDbConnection $db ьд, к которой нужно подключится, еслм $db == false, значит вернутся на дефолтную бд
	 */
	protected function switchDb($db = false)
	{
		if($db === false) {
			$db = $this->_defaultDb;
		}
		CmsActiveRecord::$db[CONNECTION_NAME] = $db;
		Yii::app()->setComponent(CONNECTION_NAME, $db);
	}
}