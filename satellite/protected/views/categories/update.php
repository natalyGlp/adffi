<?php
$this->breadcrumbs=array(
	'Редактирование категории'
);
?>

<div class="panel panel-default">
    <div class="panel-heading">
    	<h2>Редактирование категории</h2>
    </div>
  	<div class="panel-body no-padding">
  	
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

	</div>
</div>