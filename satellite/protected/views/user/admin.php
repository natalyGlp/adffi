<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array('Пользователи');

$this->menu = array(
	array('label'=>'Создать пользователя', 'url'=>array('create')),
	array('label'=>'Управление ролями', 'url'=>array('auth/assignment')),
);
$this->pageTitle = 'Пользователи';
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

/* echo CHtml::link('Расширеный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model' => $model,
	'data' => $data
)); ?>
</div>

<?php
*/
?>

    <div class="panel-heading">
    	<h2>Управление пользователями</h2>
    </div>
  	<div class="panel-body">
		<a href="/user/create" class="btn btn-success">Создать</a>
		<?php
		$this->widget('bootstrap.widgets.TbGridView', array(
		    'type'=>'striped',
			'id' => 'user-grid',
			'dataProvider' => $model->search(),
			'filter' => $model,
			'columns' => array(
				'username',
				'email',
				'password',
				array(
					'name' => 'role',
					'filter' => Yii::app()->user->isAdmin ? $model->roles : $model->roles2,
					'value' => 'isset($data->roles[$data->role]) ? $data->roles[$data->role] : ""'
				),
				array(
					'class' => 'bootstrap.widgets.TbButtonColumn',
					'template' => '{update}{delete}'
				),
			),
		));
		?>
	</div>