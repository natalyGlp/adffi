

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'user-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array(
            'class' => 'form-horizontal row-border',
            ),
    )); ?>
    

    <?php echo $form->errorSummary($model); ?>

    <div class="panel-body">
        <div class="form-group">
            <?php echo $form->labelEx($model,'username',array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-sm-8">
            <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
            
            </div>
            <div class="col-sm-2"><?php echo $form->error($model,'username'); ?></div>   
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'email',array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-sm-8">
            <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
            
            </div>
            <div class="col-sm-2"><?php echo $form->error($model,'email'); ?></div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'password',array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-sm-8">
            <?php echo $form->textField($model,'password',array('class'=>'form-control')); ?>
            
            </div>
            <div class="col-sm-2"><?php echo $form->error($model,'password'); ?></div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'role',array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-sm-8">
            <?php echo $form->dropDownList($model,'role', Yii::app()->user->isAdmin ? $model->roles : $model->roles2,array('class'=>'form-control')); ?>
            
            </div>
            <div class="col-sm-2"><?php echo $form->error($model,'role'); ?></div>
        </div>
        <hr/>
        <?php
            $this->widget('ext.nespi.ThematicsTree', array(
                'id' => 'thematics-tree',
                'model' => $model,
            ));
        ?>
        <div class="form-group">
            <div class="col-sm-8">
            <div class="span8">
            <?php
                $this->widget('ext.nespi.SiteSelector', array(
                    'model' => $model,
                    'attribute' => '_sites',
                ));
            ?>
            </div>
            </div>
        </div>    
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')); ?>
                <a onclick="javascript:history.back();" class='btn btn-inverse'>назад</a>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
