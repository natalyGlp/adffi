<div class="form">
<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
	));

	$this->widget('CTreeView', array(
		'id' => 'thematics-tree',
		'data' => $data,
		'htmlOptions' => array('style' => 'width:49%; margin-right:5px; float:left;')
	));

	$sites = Sites::model()->findAll(array(
		'select' => array('id', 'url')
	));
?>

	<div class="span8">
		<select name="User[sites][]" multiple="multiple" id="searchable">
			<?php foreach ($sites as $site): ?>
				<option value="<?php echo $site->id; ?>"><?php echo $site->url;?></option>
			<?php endforeach ?>
		</select>
	</div>
	<br clear="all"/>
	<hr/>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Фильтровать',array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget();?>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.thematics').click(function(){
				var parent = $(this).parent('label').parent('li');
				parent.children('ul').find('input').prop('checked', $(this).prop('checked'));
			});
		});
	</script>
</div><!-- search-form -->