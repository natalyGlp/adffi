<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Создать пользователя', 'url'=>array('create')),
	array('label'=>'Пользователи', 'url'=>array('admin')),
);
$this->pageTitle = 'Редактирование пользователя '.$model->id;
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>