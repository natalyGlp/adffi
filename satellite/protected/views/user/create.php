<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	'Создание',
);

$this->menu=array(
	array('label'=>'Пользователи', 'url'=>array('admin')),
);

$this->pageTitle = 'Создание пользователя';
?>
<div class="panel panel-default">
    <div class="panel-heading">
    	<h2>Создание пользователя</h2>
    </div>
		
		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

</div>