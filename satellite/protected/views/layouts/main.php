<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        $this->renderPartial('cms.views.layouts.header');
        Yii::app()->clientScript->registerCssFile('/css/satellite.css');
        ?>
    </head>
    <body>

        <div id="loading_layer" style="display:none"><img src="<?php echo GxcHelpers::assetUrl('img/ajax_loader.gif', 'gebo') ?>" alt="" /></div>
        <div class="style_switcher">
            <div class="sepH_c">
                <p>Colors:</p>
                <div class="clearfix">
                    <a href="javascript:void(0)" class="style_item jQclr blue_theme style_active" title="blue">blue</a>
                    <a href="javascript:void(0)" class="style_item jQclr dark_theme" title="dark">dark</a>
                    <a href="javascript:void(0)" class="style_item jQclr green_theme" title="green">green</a>
                    <a href="javascript:void(0)" class="style_item jQclr brown_theme" title="brown">brown</a>
                    <a href="javascript:void(0)" class="style_item jQclr eastern_blue_theme" title="eastern_blue">eastern_blue</a>
                    <a href="javascript:void(0)" class="style_item jQclr tamarillo_theme" title="tamarillo">tamarillo</a>
                </div>
            </div>
            <div class="sepH_c">
                <p>Backgrounds:</p>
                <div class="clearfix">
                    <span class="style_item jQptrn style_active ptrn_def ssw_ptrn_def" title="">ptrn_def</span>
                    <span class="ssw_ptrn_a style_item jQptrn" title="ptrn_a">ptrn_a</span>
                    <span class="ssw_ptrn_b style_item jQptrn" title="ptrn_b">ptrn_b</span>
                    <span class="ssw_ptrn_c style_item jQptrn" title="ptrn_c">ptrn_c</span>
                    <span class="ssw_ptrn_d style_item jQptrn" title="ptrn_d">ptrn_d</span>
                    <span class="ssw_ptrn_e style_item jQptrn" title="ptrn_e">ptrn_e</span>
                </div>
            </div>
            <div class="sepH_c">
                <p>Layout:</p>
                <div class="clearfix">
                    <label class="radio inline"><input type="radio" name="ssw_layout" id="ssw_layout_fluid" value="" checked="checked"> Fluid</label>
                    <label class="radio inline"><input type="radio" name="ssw_layout" id="ssw_layout_fixed" value="gebo-fixed"> Fixed</label>
                </div>
            </div>
            <div class="sepH_c">
                <p>Sidebar position:</p>
                <div class="clearfix">
                    <label class="radio inline"><input type="radio" name="ssw_sidebar" id="ssw_sidebar_left" value="" checked="checked"> Left</label>
                    <label class="radio inline"><input type="radio" name="ssw_sidebar" id="ssw_sidebar_right" value="sidebar_right"> Right</label>
                </div>
            </div>

            <div class="gh_button-group">
                <a href="#" id="saveBtn" class="btn btn-primary btn-mini">Save</a>
                <a href="#" id="resetDefault" class="btn btn-mini">Reset</a>
            </div>
        </div>

        <div id="maincontainer" class="clearfix">
            <header>
            <?php
            $menu = array(
                array('label'=>'Тематики', 'url'=>array('/thematics/admin')),
                array('label'=>'Источники', 'url'=>array('/sources/admin')),
                array('label'=>'Сайты', 'url'=>array('/sites/admin')),
                array('label'=>'Пользователи', 'url'=>array('/user/admin')),
                array('label'=>'Темы', 'url'=>array('/themes/admin')),
            );
            if (isset(Yii::app()->params['headerMenu']) && is_array(Yii::app()->params['headerMenu'])) {
                $menu = CMap::mergeArray($menu, Yii::app()->params['headerMenu']);
            }

            $this->widget('bootstrap.widgets.TbNavbar', array(
                'brand' => '<img src="'.GxcHelpers::assetUrl('images/logo_small.png', 'backend').'"  title="'.t('Dashboard').'" class="ttip_b" style="height:40px;" />',
                'fixed' => 'top',
                'fluid' => true,
                'items' => array(
                    array(
                        'class' => 'YiiBootstrapSmartMenu',
                        'encodeLabel' => false,
                        'items' => $menu
                    ),
                    array(
                        'class' => 'YiiBootstrapSmartMenu',
                        'htmlOptions'=>array('class'=>'pull-right'),
                        'items' => array(
                            array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
                        )
                    )
                )
            ));
        
            ?>
            </header>
            <?php echo $content; ?>
        </div>

        <div class="modal hide fade" id="myModal"></div>
        <div id="qtip-rcontainer"></div>
    </body>
</html>