<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Nespi</title>
    <?php
        $this->renderPartial('cms.views.layouts._header');?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">

    <link type='text/css' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600' rel='stylesheet'>

    <link type="text/css" href="/glp/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">        <!-- Font Awesome -->
    <link type="text/css" href="/glp/fonts/themify-icons/themify-icons.css" rel="stylesheet">              <!-- Themify Icons -->
    <link type="text/css" href="/glp/css/styles.css" rel="stylesheet">                                     <!-- Core CSS with all styles -->

    <link type="text/css" href="/glp/plugins/codeprettifier/prettify.css" rel="stylesheet">                <!-- Code Prettifier -->
    <link type="text/css" href="/glp/plugins/iCheck/skins/minimal/blue.css" rel="stylesheet">              <!-- iCheck -->

    <!--[if lt IE 10]>
        <script type="text/javascript" src="/glp/js/media.match.min.js"></script>
        <script type="text/javascript" src="/glp/js/respond.min.js"></script>
        <script type="text/javascript" src="/glp/js/placeholder.min.js"></script>
    <![endif]-->
    <!-- The following CSS are included as plugins and can be removed if unused-->
    
 
<link type="text/css" href="/glp/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">

    </head>

    <body class="sidebar-scroll">   <!-- animated-content -->
        
        <header id="topnav" class="navbar navbar-default navbar-fixed-top" role="banner">

    <div class="logo-area">
        <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
            <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                <span class="icon-bg">
                    <i class="ti ti-menu"></i>
                </span>
            </a>
        </span>
        
        <a class="navbar-brand" href="/">qore.cms</a>

        <!-- <div class="toolbar-icon-bg hidden-xs" id="toolbar-search">
            <div class="input-group">
                <span class="input-group-btn"><button class="btn" type="button"><i class="ti ti-search"></i></button></span>
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn"><button class="btn" type="button"><i class="ti ti-close"></i></button></span>
            </div>
        </div>  -->

    </div><!-- logo-area -->


<!-- ГЕО ПОПРОСИЛ ВСТАВИТЬ БЕЗ РАБОТАЮЩЕГО ФУНКЦИОНАЛА -->



    <ul class="nav navbar-nav toolbar pull-right">
        <li class="dropdown toolbar-icon-bg">
            <a href="#" class="dropdown-toggle username" data-toggle="dropdown">
                <div class="img img-circle"><i class="ti ti-user"></i></div>
            </a>
            <ul class="dropdown-menu userinfo arrow">
                <!-- <li><a href="#/"><i class="ti ti-user"></i><span>Profile</span><span class="badge badge-info pull-right">80%</span></a></li> -->
                <li><a href="/site/logout"><i class="ti ti-shift-right"></i><span>Sign Out</span></a></li>
            </ul>
        </li>

    </ul>

<!-- КОНЕЦ    ГЕО ПОПРОСИЛ ВСТАВИТЬ БЕЗ РАБОТАЮЩЕГО ФУНКЦИОНАЛА -->




<!-- меню сверху -->
<ul class="nav navbar-nav toolbar pull-right">
    <li class="dropdown toolbar-icon-bg">
<?php
            // $menu = array(
            //     array('label'=>'Тематики', 'url'=>array('/thematics/admin')),
            //     array('label'=>'Источники', 'url'=>array('/sources/admin')),
            //     array('label'=>'Сайты', 'url'=>array('/sites/admin')),
            //     array('label'=>'Пользователи', 'url'=>array('/user/admin')),
            //     array('label'=>'Темы', 'url'=>array('/themes/admin')),
            // );
            // if (isset(Yii::app()->params['headerMenu']) && is_array(Yii::app()->params['headerMenu'])) {
            //     $menu = CMap::mergeArray($menu, Yii::app()->params['headerMenu']);
            // }

            // $this->widget('bootstrap.widgets.TbNavbar', array(
            //     'brand' => '<img src="'.GxcHelpers::assetUrl('images/logo_small.png', 'backend').'"  title="'.t('Dashboard').'" class="ttip_b" style="height:40px;" />',
            //     'fixed' => 'top',
            //     'fluid' => true,
            //     'items' => array(
            //         array(
            //             'class' => 'YiiBootstrapSmartMenu',
            //             'encodeLabel' => false,
            //             'items' => $menu
            //         ),
            //         array(
            //             'class' => 'YiiBootstrapSmartMenu',
            //             'htmlOptions'=>array('class'=>'pull-right'),
            //             'items' => array(
            //                 array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            //             )
            //         )
            //     )
            // ));
            ?>
        </li>
    </ul>
   
</header>
<!---- -- -->
<div id="wrapper">
            <div id="layout-static">
                <div class="static-sidebar-wrapper sidebar-default">
                    <div class="static-sidebar">
                        <div class="sidebar">
    <!-- <div class="widget">
        <div class="widget-body">
            <div class="userinfo">
                <div class="avatar">
                    <img src="/glp/demo/avatar_15.png" class="img-responsive img-circle"> 
                </div>
                <div class="info">
                    <a href="/site/logout"><span class="username"><?php echo Yii::app()->user->name; ?></span></a>
                    <br>
                    <span class="useremail">jon@avenxo.com</span>
                </div>
            </div>
        </div>
    </div> -->
    <div class="widget stay-on-collapse" id="widget-sidebar">
        <nav role="navigation" class="widget-body">
            <ul class="acc-menu">
                <li class="nav-separator"><span>Меню</span></li>
            <?php if(Yii::app()->user->checkAccess('Site.index')){ ?>
                <li><a href="/site/index"><i class="ti ti-home"></i><span>Главная</span></a></li>
            <?php } ?>
            <?php if(Yii::app()->user->checkAccess('Thematics.admin')){ ?>
                <li><a href="/thematics/admin"><i class="ti ti-layout-grid3"></i><span>Тематики</span></a></li>
            <?php } ?>
            <?php if(Yii::app()->user->checkAccess('Sources.admin')){ ?>
                <li><a href="/sources/admin"><i class="ti ti-control-shuffle"></i><span>Источники</span></a></li>
            <?php } ?>
            <?php if(Yii::app()->user->checkAccess('Sites.admin')){ ?>
                <li><a href="/sites/admin"><i class="ti ti-file"></i><span>Сайты</span></a></li>
            <?php } ?>
            <?php if(Yii::app()->user->checkAccess('User.admin')){ ?>
                <li><a href="javascript:;"><i class="ti ti-user"></i><span>Пользователи</span></a>
                    <ul class="acc-menu">
                        <li><a href="/user/admin">Управление</a></li>
                        <li><a href="/auth/role">Роли</a></li>
                        <li><a href="/auth/operation">Операции</a></li>
                        <li><a href="/auth/task">Задачи</a></li>
                        <li><a href="/auth">Assignments</a></li>
                   </ul>
                </li>
            <?php } ?>
            <?php if(Yii::app()->user->checkAccess('Themes.*')){ ?>
                <li><a href="/themes/admin"><i class="ti ti-pencil"></i><span>Темы</span></a></li>
            <?php } ?>
            <?php if(Yii::app()->user->checkAccess('Shop.*')){ ?>
                <li><a href="javascript:;"><i class="ti ti-settings"></i><span>Магазин</span></a>
                    <ul class="acc-menu">
                        <?php if(Yii::app()->user->checkAccess('Shop.default.*')){ ?>
                        <li><a href="/shop">Заказы</a></li>
                        <?php } ?>
                        <?php if(Yii::app()->user->checkAccess('Shop.product.*')){ ?>
                        <li><a href="/shop/product/admin">Управление товаром</a></li>
                        <?php } ?>
                        <?php if((Yii::app()->user->checkAccess('Shop.currency.*'))or (Yii::app()->user->checkAccess('Shop.delivery.*'))){ ?>
                        <li><a href="/shop/currency/settings">Настройки магазина</a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>

            </ul>
           
        </nav>
    </div>

</div>
                    </div>
                </div>
                <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">

                    
                                <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                                    'links' => $this->breadcrumbs
                                ));?><!-- breadcrumbs -->
            

<div class="container-fluid">
                                 
<?php echo $content; ?>

</div> <!-- .container-fluid -->
</div> <!-- #page-content -->
                    </div>
                    <footer role="contentinfo">
                        <div class="clearfix">
                            <ul class="list-unstyled list-inline pull-left">
                                <li><h6 style="margin: 0;">&copy; 2016 QORE</h6></li>
                            </ul>
                            <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
                        </div>
                    </footer>

                </div>
            </div>
        </div>

    
    <!-- Switcher -->
    <!-- <div class="demo-options">
        <div class="demo-options-icon"><i class="ti ti-paint-bucket"></i></div>
        <div class="demo-heading">Demo Settings</div>

        <div class="demo-body">
            <div class="tabular">
                <div class="tabular-row">
                    <div class="tabular-cell">Fixed Header</div>
                    <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" checked data-size="mini" data-on-color="success" data-off-color="default" name="demo-fixedheader" data-on-text="&nbsp;" data-off-text="&nbsp;"></div>
                </div>
                <div class="tabular-row">
                    <div class="tabular-cell">Boxed Layout</div>
                    <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" data-size="mini" data-on-color="success" data-off-color="default" name="demo-boxedlayout" data-on-text="&nbsp;" data-off-text="&nbsp;"></div>
                </div>
                <div class="tabular-row">
                    <div class="tabular-cell">Collapse Leftbar</div>
                    <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" data-size="mini" data-on-color="success" data-off-color="default" name="demo-collapseleftbar" data-on-text="&nbsp;" data-off-text="&nbsp;"></div>
                </div>
            </div>
        </div>

        <div class="demo-body">
            <div class="option-title">Topnav</div>
            <ul id="demo-header-color" class="demo-color-list">
                <li><span class="demo-cyan"></span></li>
                <li><span class="demo-light-blue"></span></li>
                <li><span class="demo-blue"></span></li>
                <li><span class="demo-indigo"></span></li>
                <li><span class="demo-deep-purple"></span></li> 
                <li><span class="demo-purple"></span></li> 
                <li><span class="demo-pink"></span></li> 
                <li><span class="demo-red"></span></li>
                <li><span class="demo-teal"></span></li>
                <li><span class="demo-green"></span></li>
                <li><span class="demo-light-green"></span></li>
                <li><span class="demo-lime"></span></li>
                <li><span class="demo-yellow"></span></li>
                <li><span class="demo-amber"></span></li>
                <li><span class="demo-orange"></span></li>               
                <li><span class="demo-deep-orange"></span></li>
                <li><span class="demo-midnightblue"></span></li>
                <li><span class="demo-bluegray"></span></li>
                <li><span class="demo-bluegraylight"></span></li>
                <li><span class="demo-black"></span></li> 
                <li><span class="demo-gray"></span></li> 
                <li><span class="demo-graylight"></span></li> 
                <li><span class="demo-default"></span></li>
                <li><span class="demo-brown"></span></li>
            </ul>
        </div>

        <div class="demo-body">
            <div class="option-title">Sidebar</div>
            <ul id="demo-sidebar-color" class="demo-color-list">
                <li><span class="demo-cyan"></span></li>
                <li><span class="demo-light-blue"></span></li>
                <li><span class="demo-blue"></span></li>
                <li><span class="demo-indigo"></span></li>
                <li><span class="demo-deep-purple"></span></li> 
                <li><span class="demo-purple"></span></li> 
                <li><span class="demo-pink"></span></li> 
                <li><span class="demo-red"></span></li>
                <li><span class="demo-teal"></span></li>
                <li><span class="demo-green"></span></li>
                <li><span class="demo-light-green"></span></li>
                <li><span class="demo-lime"></span></li>
                <li><span class="demo-yellow"></span></li>
                <li><span class="demo-amber"></span></li>
                <li><span class="demo-orange"></span></li>               
                <li><span class="demo-deep-orange"></span></li>
                <li><span class="demo-midnightblue"></span></li>
                <li><span class="demo-bluegray"></span></li>
                <li><span class="demo-bluegraylight"></span></li>
                <li><span class="demo-black"></span></li> 
                <li><span class="demo-gray"></span></li> 
                <li><span class="demo-graylight"></span></li> 
                <li><span class="demo-default"></span></li>
                <li><span class="demo-brown"></span></li>
            </ul>
        </div>



    </div> -->
<!-- /Switcher -->
    <!-- Load site level scripts -->
<!--Lобавить если что то будет не работать-->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
 -->
<!-- <script type="text/javascript" src="/glp/js/jquery-1.10.2.min.js"></script>                           <!-- Load jQuery -->
<!-- <script type="text/javascript" src="/glp/js/jqueryui-1.10.3.min.js"></script>                             <!-- Load jQueryUI -->
<!-- <script type="text/javascript" src="/glp/js/bootstrap.min.js"></script>                               <!-- Load Bootstrap -->
<script type="text/javascript" src="/glp/js/enquire.min.js"></script>                                     <!-- Load Enquire -->

<script type="text/javascript" src="/glp/plugins/velocityjs/velocity.min.js"></script>                    <!-- Load Velocity for Animated Content -->
<script type="text/javascript" src="/glp/plugins/velocityjs/velocity.ui.min.js"></script>

<script type="text/javascript" src="/glp/plugins/wijets/wijets.js"></script>                          <!-- Wijet -->

<script type="text/javascript" src="/glp/plugins/codeprettifier/prettify.js"></script>                <!-- Code Prettifier  -->
<script type="text/javascript" src="/glp/plugins/bootstrap-switch/bootstrap-switch.js"></script>      <!-- Swith/Toggle Button -->

<script type="text/javascript" src="/glp/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

<script type="text/javascript" src="/glp/plugins/iCheck/icheck.min.js"></script>                      <!-- iCheck -->

<script type="text/javascript" src="/glp/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->

<script type="text/javascript" src="/glp/js/application.js"></script>
<script type="text/javascript" src="/glp/demo/demo.js"></script>
<script type="text/javascript" src="/glp/demo/demo-switcher.js"></script>

<!-- End loading site level scripts -->
    

    

    <!-- End loading page level scripts-->
 
    </body>
</html>