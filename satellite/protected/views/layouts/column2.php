<?php $this->beginContent('//layouts/main'); ?>
 <!-- main content -->
    <div id="contentwrapper">
        <div class="main_content">
			<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			    'links' => $this->breadcrumbs
			));?><!-- breadcrumbs -->
            <?php
            $flashes = array('success' => 'alert-success', 'fail' => 'alert-error', 'info' => 'alert-info', 'error' => 'alert-error', 'warning' => '');
            foreach($flashes as $flash => $class)
                if(Yii::app()->user->hasFlash($flash))
                {
                    ?>
                    <div class="alert <?php echo $class; ?>">
                        <a class="close" data-dismiss="alert">×</a>
                        <?php echo Yii::app()->user->getFlash($flash) ?>
                    </div>
                    <?php
                }
            ?>
			<?php echo $content; ?>
        </div>
    </div>

    <!-- sidebar -->
    <a href="javascript:void(0)" class="sidebar_switch on_switch ttip_r" title="Hide Sidebar">Sidebar switch</a>
    <div class="sidebar">

        <div class="antiScroll">
            <div class="antiscroll-inner">
                <div class="antiscroll-content">

                    <div class="sidebar_inner">
                        <div id="side_accordion" class="accordion">
                            <?php if(!Yii::app()->user->isGuest) $this->widget('UserMenu'); ?>
                        	<?php echo $this->renderPartial('cms.views.layouts.calculator');?>
                        </div>
                    </div>

                    <?php
                    if(Yii::app()->notify->count(array('placement' => 'sidebar')) > 0):
                    ?>
                        <div class="sidebar_info">
                            <ul class="unstyled">
                                <?php
                                $notifies = Yii::app()->notify->fetch(array('placement' => 'sidebar'));
                                foreach ($notifies as $notify)
                                {
                                    ?>
                                    <li>
                                        <span class="act act-<?php echo $notify->level ?>"><?php echo $notify->counter ?></span>
                                        <strong title="<?php echo CHtml::encode($notify->message) ?>" class="ttip_t"><?php echo $notify->title ?></strong>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    <?php
                    endif;
                    ?>

                </div>
            </div>
        </div>

    </div>
<?php $this->endContent(); ?>