<?php
/**
 * Шаблон, который используется для рендеринга iframe
 * TODO: этот шаблон должен тянуться из satellite_project
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        $this->renderPartial('cms.views.layouts.header');
        Yii::app()->clientScript->registerCssFile('/css/satellite.css');
        ?>
    </head>
    <body style="padding:10px">
        <div id="loading_layer" style="display:none"><img src="<?= GxcHelpers::assetUrl('img/ajax_loader.gif', 'gebo') ?>" alt="" /></div>
        <div class="container" id="page">
            <?php echo $content; ?>
        </div><!-- page -->
    </body>
</html>