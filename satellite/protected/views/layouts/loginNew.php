<!DOCTYPE html>
<html>
<head>
<!--   <meta charset="utf-8">
	<title>NESPI</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="/css/login.css" type="text/css" media="screen" />
 -->
	<meta charset="utf-8">
    <title>Login Form NESPI</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="author" content="KaijuThemes">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600' rel='stylesheet' type='text/css'>
    <link type="text/css" href="/glp/plugins/iCheck/skins/minimal/blue.css" rel="stylesheet">
    <link type="text/css" href="/glp/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link type="text/css" href="/glp/fonts/themify-icons/themify-icons.css" rel="stylesheet">               <!-- Themify Icons -->
    <link type="text/css" href="/glp/css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
    <!--[if lt IE 9]>
        <link type="text/css" href="assets/css/ie8.css" rel="stylesheet">
        <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The following CSS are included as plugins and can be removed if unused-->
</head>
<body class="focused-form animated-content">
    <?php echo $content; ?>
    <!-- Load site level scripts -->

		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

		<script type="text/javascript" src="/glp/js/jquery-1.10.2.min.js"></script>                           <!-- Load jQuery -->
		<script type="text/javascript" src="/glp/js/jqueryui-1.10.3.min.js"></script>                             <!-- Load jQueryUI -->
		<script type="text/javascript" src="/glp/js/bootstrap.min.js"></script>                               <!-- Load Bootstrap -->
		<script type="text/javascript" src="/glp/js/enquire.min.js"></script>                                     <!-- Load Enquire -->

		<script type="text/javascript" src="/glp/plugins/velocityjs/velocity.min.js"></script>                    <!-- Load Velocity for Animated Content -->
		<script type="text/javascript" src="/glp/plugins/velocityjs/velocity.ui.min.js"></script>

		<script type="text/javascript" src="/glp/plugins/wijets/wijets.js"></script>                          <!-- Wijet -->

		<script type="text/javascript" src="/glp/plugins/codeprettifier/prettify.js"></script>                <!-- Code Prettifier  -->
		<script type="text/javascript" src="/glp/plugins/bootstrap-switch/bootstrap-switch.js"></script>      <!-- Swith/Toggle Button -->

		<script type="text/javascript" src="/glp/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

		<script type="text/javascript" src="/glp/plugins/iCheck/icheck.min.js"></script>                      <!-- iCheck -->

		<script type="text/javascript" src="/glp/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->

		<script type="text/javascript" src="/glp/js/application.js"></script>
		<script type="text/javascript" src="/glp/demo/demo.js"></script>
		<script type="text/javascript" src="/glp/demo/demo-switcher.js"></script>

<!-- End loading site level scripts -->
    <!-- Load page level scripts-->
    

    <!-- End loading page level scripts-->
</body>
</html>