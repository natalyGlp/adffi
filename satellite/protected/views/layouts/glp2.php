<?php $this->beginContent('//layouts/mainNew'); ?>

<div data-widget-group="group1">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" data-widget='{"draggable": "false"}'>
              <!--   <div class="panel-heading">
                    <h2>Excepteur Sint</h2>
                    <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
                </div> -->

                    <?php echo $content; ?>

            </div>
        </div>
    </div>
</div>
			

<?php $this->endContent(); ?>