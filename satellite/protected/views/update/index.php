<?php
echo CHtml::beginForm();
?>
<div class="row">
<?php
	echo CHtml::label('Что делать?', 'action');
	echo CHtml::dropDownList('action', isset($_POST['action']) ? $_POST['action'] : '', $actions, array('empty' => '-- выберите действие --'));
?>
</div>
<div class="row">
<?php
	echo CHtml::label('Путь миграции', 'migrationPath');
	echo CHtml::dropDownList('migrationPath', isset($_POST['migrationPath']) ? $_POST['migrationPath'] : '', $migrationPathes, array('empty' => '-- выберите путь --'));
?>
</div>
<div class="row">
<?php
	echo CHtml::label('Ограничение', 'limit');
	echo CHtml::textField('limit', isset($_POST['limit']) ? $_POST['limit'] : '');
?>
</div>
<div class="row">
<?php
	echo CHtml::passwordField('password', '');
?>
</div>
<div class="row">
<?php
	echo CHtml::submitButton('Запустить миграцию');
?>
</div>
<br /><br />
<div class="row">
<?php
	echo CHtml::textArea('console', CHtml::encode($data), array('style' => 'width: 500px; height: 200px;'));
?>
</div>
<?php
echo CHtml::endForm();