
<div class="row">
	<div class="col-md-3">
		<div class="info-tile tile-orange">
			<div class="tile-icon"><i class="ti ti-shopping-cart-full"></i></div>
			<div class="tile-heading"><span>Заявки</span></div>
			<div class="tile-body"><span><?php echo $model->getAllOrders(); ?></span></div>
			<!-- <div class="tile-footer"><span class="text-success">22.5% <i class="fa fa-level-up"></i></span></div> -->
		</div>
	</div>
	<div class="col-md-3">
		<div class="info-tile tile-success">
			<div class="tile-icon"><i class="ti ti-bar-chart"></i></div>
			<div class="tile-heading"><span>Заказы</span></div>
			<div class="tile-body"><span><?php echo $model->getApprovedOrders(); ?></span></div>
			<!-- <div class="tile-footer"><span class="text-danger">12.7% <i class="fa fa-level-down"></i></span></div> -->
		</div>
	</div>
	<div class="col-md-3">
		<div class="info-tile tile-info">
			<div class="tile-icon"><i class="ti ti-stats-up"></i></div>
			<div class="tile-heading"><span>Доходы</span></div>
			<div class="tile-body"><span><?php echo $model->getIncome(); ?> </span></div>
			<!-- <div class="tile-footer"><span class="text-success">5.2% <i class="fa fa-level-up"></i></span></div> -->
		</div>
	</div>
	<div class="col-md-3">
		<div class="info-tile tile-danger">
			<div class="tile-icon"><i class="ti ti-bar-chart-alt"></i></div>
			<div class="tile-heading"><span>Прибыль</span></div>
			<div class="tile-body"><span><?php echo $model->getProfit(); ?> </span></div>
			<!-- <div class="tile-footer"><span class="text-danger">10.5% <i class="fa fa-level-down"></i></span></div> -->
		</div>
	</div>
</div>


<div data-widget-group="group1">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-lightgray" data-widget='{"id" : "wiget9", "draggable": "false"}'>
				<div class="panel-heading">
					<h2>Топ тематик</h2>
					<div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-editbox" data-widget-controls=""></div>
				
				<div class="panel-body no-padding">
					<table class="table browsers m-n">
						<tbody>
							<?php
							foreach ($topThematics as $key => $sites) {
								$data = Thematics::model()->findByPk($sites->id_thematic);
								echo "
								<tr>
									<td >".$data->title."</td>
									<td></td>
								</tr>
								";
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-lightgray" data-widget='{"draggable": "false"}'>
				<div class="panel-heading">
					<h2>Топ Сайтов</h2>
					<div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				
				<div class="panel-body no-padding">
					<table class="table browsers m-n">
						<tbody>
							<?php
							foreach ($topSites as $key => $site) {
								echo "
								<tr>
									<td >".$site->title."</td>
									<td><a href=\"".$site->url."\"><i class=\"ti ti-world\"></i></a></td>
								</tr>
								";
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>


	

	<div class="row">
		
		<div class="col-md-4">
			<div class="panel panel-lightgray" data-widget='{"draggable": "false"}'>
				<div class="panel-heading">
					<h2>ТОП Пользователей</h2>
					<div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-body no-padding">
					<table class="table browsers m-n">
						<tbody>
							<?php
							foreach ($topUsers as $key => $user) {
								echo "
								<tr>
									<td >".$user->username."</td>
									<td>".$user->id."</td>
								</tr>
								";
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	
		
		<div class="col-md-8">
			<div class="panel panel-lightgray" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2>World Map</h2>
                    <div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
                </div>
                <div class="panel-body">
					<div id="worldmap" style="height: 272px; width: 100%;" class="mt-sm mb-sm"></div>
                </div>
            </div>
		</div>
		
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-lightgray" data-widget='{"id" : "wiget9", "draggable": "false"}'>
				<div class="panel-heading">
					<h2>Топ Продаж</h2>
					<div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-body no-padding">
					<table class="table browsers m-n">
						<tbody>
							<?php
							foreach ($topProducts as $key => $product) {
								$data = Object::model()->findByPk($product->prod_id);
								echo "
								<tr>
									<td >".$data->object_title."</td>
									<td>".$product->quantity."</td>
								</tr>
								";
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-lightgray" data-widget='{"draggable": "false"}'>
				<div class="panel-heading">
					<h2>Новинки</h2>
					<div class="panel-ctrls button-icon-bg" 
						data-actions-container="" 
						data-action-collapse='{"target": ".panel-body"}'
						data-action-colorpicker=''
						data-action-refresh-demo='{"type": "circular"}'
						>
					</div>
				</div>
				<div class="panel-body no-padding">
					<table class="table browsers m-n">
						<tbody>
							<?php
							foreach ($newProducts as $key => $product) {
								echo "
								<tr>
									<td class=\"text-right\">".($key +1)."</td>
									<td>".$product->object_title."</td>
								</tr>
								";
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>


</div> 

   <!-- Load page level scripts -->
   <script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>       <!-- jVectorMap -->

<!-- Maps -->
<script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-cn-mill-en.js"></script>
<script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-dk-mill-en.js"></script>
<script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-europe-mill-en.js"></script>
<script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-in-mill-en.js"></script>
<script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-nl-mill-en.js"></script>
<script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-se-mill-en.js"></script>
<script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-us-aea-en.js"></script>
<script type="text/javascript" src="../glp/plugins/jvectormap/jquery-jvectormap-us-ny-newyork-mill-en.js"></script>
<!-- /Maps -->

<script type="text/javascript" src="../glp/demo/demo-jvectormap.js"></script>
