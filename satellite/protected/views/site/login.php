<section class="login-box">
    <aside class="left-part">  
        <h2>ВХОД В СИСТЕМУ</h2>
        <?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'enableAjaxValidation'=>true,
		)); ?>
            <div class="clearfix"> 
       			<?php echo $form->textField($model,'username', array('onfocus' => "if(this.value=='Логин') this.value='';", 'onblur' => "if(!this.value) this.value='Логин';")); ?>
            </div>
            <div class="clearfix">  
				<?php echo $form->passwordField($model,'password'); ?>
            </div>
            <div class="clearfix">          
                <label class="remember"><?php echo $form->checkBox($model,'rememberMe'); ?>Запомнить<br></label>   
            </div>
            <div class="clear"></div>
            <?php echo CHtml::submitButton('Войти', array('class' => 'login-button')); ?>
		<?php $this->endWidget(); ?>
        <div class="clear"></div>            
        <p>Для создания учётной<br />
          	необходимо обратиться к<br />
          	системному администратору.</p> 
    </aside> 

    <aside class="central-part">
        <logoblock>
            <a href="/"><logo class="logo"></logo></a>
            <slogan>
              ver. 3.123.22    
            </slogan>
        </logoblock>
        <p>Система администрирования сайта<br />Nespi ver. 3.123.22 это последняя<br />разработка компании &lt;glp&gt; и<br />является надёжным помошником в<br />управлении сайтом</p>
        <copyright>Разработано в компании NESPI>. 2012 г.</copyright>
    </aside>

    <aside class="right-part">
        <blockquote class="study-material">
            <p><span>УЧЕБНЫЕ МАТЕРИАЛЫ</span> для быстрого обучения навигации, принципам роботы, настройки и ускорения работы в системе управления сайтом NESPI</p>
            <a href="#"><img src="/images/study-m.jpg"></a>
        </blockquote> 
        <blockquote class="study">
            <a href="#"><img src="/images/study.jpg"></a>
            <p><span>УЧЕБНЫЕ МАТЕРИАЛЫ</span> для быстрого обучения навигации, принципам роботы, настройки и ускорения работы в системе управления сайтом NESPI</p>
        </blockquote> 
    </aside>
</section>