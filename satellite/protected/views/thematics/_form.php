
	

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'thematics-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array(
			'class' => 'form-horizontal row-border',
			),
	)); ?>

		<?php echo $form->errorSummary($model); ?>
		<div class="panel-body">
			<div class="form-group">
				<?php echo $form->labelEx($model,'title',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
					<?php echo $form->error($model,'title'); ?>
				</div>
			</div>
		</div>
			

		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-success')); ?>
					<a onclick="javascript:history.back();" class='btn btn-inverse'>назад</a>
				</div>
			</div>
		</div>

	<?php $this->endWidget(); ?>

	
