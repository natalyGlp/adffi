<?php
/* @var $this ThematicsController */
/* @var $model Thematics */
$this->breadcrumbs=array(
	'Создание'
);
?>
<div class="panel panel-default">
    <div class="panel-heading"><h2>Создание тематики</h2></div>
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>