<?php
/* @var $this ThematicsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Thematics',
);

$this->menu=array(
	array('label'=>'Create Thematics', 'url'=>array('create')),
	array('label'=>'Manage Thematics', 'url'=>array('admin')),
);
?>

<h1>Thematics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
