<?php
/* @var $this ThematicsController */
/* @var $model Thematics */
$this->breadcrumbs=array(
	'Тематики'
);

$this->menu = array(
	array(
		'label' => 'Создать тематику', 
		'url' => '/thematics/create',
		'linkOptions' => array(
			'class' => 'doManageThematics'
		)
	)
);

$this->pageTitle = 'Тематики';
?>
<style type="text/css">
.thematics-block{ visibility: hidden;}
</style>
<!-- <div class="panel panel-sky" data-widget='{"draggable": "true"}'> -->
<div class="panel panel-default">
    <div class="panel-heading">
    	<h2>Тематики</h2>
    </div>
  	<div class="panel-body">
		<?php if(Yii::app()->user->checkAccess('Thematics.create')) { ?>
    	<h2><a href="/thematics/create" class="btn btn-success">Создать</a></h2>
		<?php } ?>
    	<table class="table table-hover m-n" >
            <thead>
                <tr>
                    <th style="padding-right:100px">Категория</th>
                    <!-- <th>Population (1000s)</th> -->
                    <th>Подкатегория</th>
                    <th>изменить</th>
                    <th>удалить</th>
                    <th>источники</th>
                    <th>добавить источник</th>
                    <th>добавить сайт</th>
                </tr>
            </thead>
            <tbody>

            	<?php
            	foreach ($data as $key => $value) {
					echo "<tr><td style='font-weight: bold'>".$value["text"]."</td>";

					echo '<td><a  href="/categories/create/?id_thematic='.$key.'"><i style=\'color: #000\' title="создать подкатегорию" class="fa fa-folder-open-o"></i> создать</a>';

					echo "</td>";
					echo '<td><a href="/thematics/update/?id='.$key.'"><i style=\'color: #000\' title="редактировать тематику" class="fa fa-wrench"></i></a></td>';
					echo '<td><a href="/thematics/delete?id='.$key.'"><i style=\'color: #000\' title="удалить тематику" class="fa fa-trash-o"></i></a></td>';
					echo '<td><a href="/sources/admin/?Sources[id_thematic]='.$key.'"><i style=\'color: #000\' title="посмотреть/управлять предложенными источниками"class="fa fa-eye"></i></a></td>';
					echo '<td><a href="/sources/create/?Sources[id_thematic]='.$key.'"><i style=\'color: #000\' title="создать предложенный источник источник" class="fa fa-plus"></i></a></td>';
					echo '<td><a href="/sites/create/?Sites[id_thematic]='.$key.'"><i style=\'color: #000\' title="создать сайт по определенной тематике" class="fa fa-plus"></i></a></td>';
					echo "</tr>";
					if(isset($value['children']))
					 	tree($value['children']);
				}
            	?>

            </tbody>
        </table>
    </div>
</div>
<!-- </div> -->

<?php
//echo print_r($data,true);
// $this->widget('CTreeView', array(
// 	'id' => 'thematics-tree',
// 	'htmlOptions' => array(
// 		'class' => 'thematics-tree',
// 		),
// 	'data' => $data
// ));


function tree($data){

	// echo '<table class="table table-striped m-n" >
	//         <tbody>';
	
	foreach ($data as $key => $v) {
			echo "<tr><td colspan=2 class=\"pod-td\" style=\"padding-left: 25px; color: #000\" ><span> ".$v["text"]."</span></td>";
			echo '<td><a href="/categories/update/?id='.$key.'"><i style=\'color: #000\' title="редактировать подкатегорию" class="fa fa-wrench"></i></a></td>';
			echo '<td><a href="/categories/delete?id='.$key.'"><i style=\'color: #000\' title="удалить подкатегорию" class="fa fa-trash-o"></i></a></td>';
			echo '<td><a href="/sources/admin/?Sources[id_thematic]='.$key.'"><i style=\'color: #000\' title="управлять предложенными источниками" class="fa fa-eye"></i></a></td>';
			echo '<td><a href="/sources/create/?Sources[id_thematic]='.$key.'"><i style=\'color: #000\' title="создать источник" class="fa fa-plus"></i></a></td>';
			echo '<td><a href="/sites/create/?Sites[id_thematic]='.$key.'"><i style=\'color: #000\' title="создать сайт"class="fa fa-plus"></i></a></td>';
			echo "</tr>";
			// if(isset($v['children']))
			// 	tree($v);
	}	
	// echo "</tbody>";
	// echo "</table>";
}
?>
