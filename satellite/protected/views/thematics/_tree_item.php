<?php
$updateUrl = '/'.$type.'/update/?id=' . $data->id;
?>
<div class="tree-item-content <?php echo $odd ? ' odd' : '';?>">
	<a href="/sites/admin/?Sites[id_thematic]=<?php echo $type=='thematics'?$data->id:$data->id_thematic.'&Sites[_categories][]='.$data->id;?>"><?php echo $data->title;?></a>
	<span class="thematics-block">
		<?php if ($type=='thematics'): ?>
			<a href="<?php echo '/categories/create/?id_thematic=' . $data->id;?>" class="doManageThematics">
				<span data-tooltip class="has-tip icon-plus" title="Добавить категорию"></span>
			</a>
		<?php endif ?>
		<a href="<?php echo $updateUrl;?>" class="doManageThematics">
			<span data-tooltip class="has-tip icon-pencil" title="Редактировать <?php echo $type=='thematics'?'тематику':'категорию' ?>"></span>
		</a>
		<a href="<?php echo Yii::app()->createUrl('/'.$type.'/delete/', array('id' => $data->id));?>">
			<span data-tooltip class="icon-trash has-tip" title="Удалить <?php echo $type=='thematics'?'тематику':'категорию' ?>"></span>
		</a>
		&nbsp;&nbsp;&nbsp;
		<a href="/sites/create/?Sites[id_thematic]=<?php echo $type=='thematics'?$data->id:$data->id_thematic.'&Sites[_categories][]='.$data->id;?>">
			<span data-tooltip class="icon-file has-tip" title="Добавить сайт"></span>
		</a>
		&nbsp;&nbsp;&nbsp;
		<a href="/sources/admin/?Sources[id_thematic]=<?php echo $type=='thematics'?$data->id:$data->id_thematic.'&Sources[_categories][]='.$data->id;?>">
			<span data-tooltip class="icon-info-sign has-tip" title="Просмотреть источники"></span>
		</a>
		<a href="/sources/create/?Sources[id_thematic]=<?php echo $type=='thematics'?$data->id:$data->id_thematic.'&Sources[_categories][]='.$data->id;?>">
			<span data-tooltip class="icon-plus-sign has-tip" title="Добавить источник"></span>
		</a>
	</span>
</div>