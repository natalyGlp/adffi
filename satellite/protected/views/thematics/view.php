<?php
/* @var $this ThematicsController */
/* @var $model Thematics */

$this->breadcrumbs=array(
	'Thematics'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Thematics', 'url'=>array('index')),
	array('label'=>'Create Thematics', 'url'=>array('create')),
	array('label'=>'Update Thematics', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Thematics', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Thematics', 'url'=>array('admin')),
);
?>

<h1>View Thematics #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'active',
	),
));