<?php
/* @var $this SourcesController */
/* @var $model Sources */
$this->breadcrumbs=array(
	'Источники'=>array('admin'),
	'Предложенные',
);

$this->menu = array(
	array('label'=>'Источники', 'url'=>array_merge(array('admin'), $_GET)),
	array('label'=>'Создать источник', 'url'=>array_merge(array('create'), $_GET)),
	array('label'=>'Предложить источник', 'url'=>array_merge(array('recommend'), $_GET),
		'visible' => Yii::app()->user->checkAccess('Sources.recommend')),
);
?>
<div class="panel no-padding">
    <div class="panel-heading">
		<h2>Предложенные источники</h2>
	</div>
	<div class="panel-body">
		<?php 
		$this->widget('bootstrap.widgets.TbGridView', array(
		    'type'=>'striped',
			'id'=>'sources-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
				array(
					'name' => 'id_thematic',
					'value' => 'isset($data->thematic->title) ? $data->thematic->title : ""',
					'filter' => Thematics::model()->list
				),
				array(
					'name' => 'url',
					'type' => 'raw',
					'value' => '"<a target=\"_blank\" href=\"".$data->url."\">".$data->url."</a>"'
				),
				'description',
				array(
					'name' => 'active',
					'header' => 'Рекоммендация',
					'filter' => false,
					'type' => 'raw',
					'value' => '"<a class=\"btn\" href=\"/sources/approve/?id=".$data->id."\">Добавить в источники</a>"'
				),
				array(
					'class'=>'bootstrap.widgets.TbButtonColumn',
					'template' => '{delete}'
				)
			)
		));
		?>		
	</div>
</div>
