<?php
/* @var $this SourcesController */
/* @var $model Sources */

$this->breadcrumbs=array(
	'Источники'=>array('admin'),
	'Создание',
);

$this->menu=array(
	array('label'=>'Источники', 'url'=>array('admin')),
);
?>
<div class="panel panel-default">
    <div class="panel-heading"><h2>Создание источников</h2></div>
  	<div class="panel-body no-padding">

		<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

	</div>
</div>