<?php
/* @var $this SourcesController */
/* @var $model Sources */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sources-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
			'class' => 'form-horizontal row-border',
			),
)); ?>

	<div class="panel-body">
		<div class="form-group">
			<?php echo $form->labelEx($model,'id_thematic',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-8">
				<?php echo $form->dropDownList($model,'id_thematic',Thematics::model()->list,array('class'=>'form-control')); ?>
				
			</div>
			<div class="col-sm-2"><?php echo $form->error($model,'id_thematic'); ?></div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'categories',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-8">
				<div id="cats_place">
					<?php echo $form->checkBoxList($model, 'categories', array());  ?>
				</div>
			</div>
			<div class="col-sm-2"><?php echo $form->error($model,'categories'); ?></div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'url',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-8">
			<?php echo $form->textField($model,'url',array('class'=>'form-control')); ?>
			</div>
			<div class="col-sm-2">
			<?php echo $form->error($model,'url'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'rss',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-8">
			<?php echo $form->textField($model,'rss',array('class'=>'form-control')); ?>
			</div>
			<div class="col-sm-2">
			<?php echo $form->error($model,'rss'); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'description',array('class'=>'col-sm-2 control-label')); ?>
			<div class="col-sm-8">
			<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
			</div>
			<div class="col-sm-2"><?php echo $form->error($model,'description'); ?></div>
		</div>
	</div>
	

	<div class="panel-footer">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-success')); ?>
				<a onclick="javascript:history.back();" class='btn btn-inverse'>назад</a>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>
<script>
$(document).ready(function(){
	$("#Sources_id_thematic").change(function(){
		$("#Sources_categories").html("");
		var val = $(this).val(),
			cats = <?php echo CJavaScript::encode($model->categories);?>;

		if(val !== ""){
			$.post('/categories/ajaxgetlistforsources/?sid=<?php echo $model->id;?>&id_thematic='+val, $.param({cats: cats}), function(data){
				$('#cats_place').html('').append($(data));
			});
		}
	}).change();
});
</script>
