<?php
/* @var $this SourcesController */
/* @var $model Sources */

$this->breadcrumbs=array(
	'Источники'=>array('admin'),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Создать источник', 'url'=>array('create')),
	array(
		'label'=>'Просмотреть источник', 
		'url'=>array('view', 'id' => $model->id),
		'visible' => Yii::app()->user->checkAccess('Sources.view')
	),
	array('label'=>'Источники', 'url'=>array('admin')),
);
?>
<div class="panel no-padding">
    <div class="panel-heading">
		<h2>Редактирование источника <?php echo $model->id; ?></h2>
	</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

</div>