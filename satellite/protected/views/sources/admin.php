<?php
/* @var $this SourcesController */
/* @var $model Sources */

$this->breadcrumbs=array(
	'Источники'
);

$this->menu = array(
	array(
		'label'=>'Создать источник', 
		'url'=>array_merge(array('create'), $_GET),
		'visible' => Yii::app()->user->checkAccess('Sources.create')
	),
	array(
		'label'=>'Предложить источник', 
		'url'=>array_merge(array('recommend'), $_GET),
		'visible' => Yii::app()->user->checkAccess('Sources.recommend')
	),
	array(
		'type' => 'html',
		'label'=>'Предложeнные источники <span class="badge">'.
		Yii::app()->db->createCommand()->select('COUNT(id)')->from('sources')->where('active=0')->queryScalar().
		'</span>', 'url'=>array_merge(array('recommended'), $_GET),
		'visible' => Yii::app()->user->checkAccess('Sources.recommended')
	),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#sources-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$this->pageTitle = 'Источники';
?>
<div class="panel panel-default">
    <div class="panel-heading">
    	<h2>Тематики</h2>
    </div>
  	<div class="panel-body">
<?php //$this->renderPartial('_search', array('model' => $model)); ?>
<?php if(Yii::app()->user->checkAccess('Sources.create')) {?>
<a href="/sources/create" class="btn btn-success">Cоздать</a>
<?php } if(Yii::app()->user->checkAccess('Sources.recommend')) {?>
<a href="/sources/recommend" class="btn btn-success">Предложить</a>
<?php } if(Yii::app()->user->checkAccess('Sources.recommended')) { ?>
<a href="/sources/recommended" class="btn btn-success">Предложенные источники</a>
<?php
}
	$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped',
	'id'=>'sources-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name' => 'id_thematic',
			'value' => 'isset($data->thematic->title) ? $data->thematic->title : ""',
			'filter' => Thematics::model()->list
		),
		array(
			'name' => 'url',
			'type' => 'raw',
			'value' => '"<a target=\"_blank\" href=\"".$data->url."\">".$data->url."</a>"'
		),
		'description',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => (Yii::app()->user->checkAccess('Sources.view') ? '{view}' : '')
						
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => (Yii::app()->user->checkAccess('Sources.update') ? '{update}' : '')
						  
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => (Yii::app()->user->checkAccess('Sources.delete') ? '{delete}' : '')
		),
	),
));
?>
</div>
</div>