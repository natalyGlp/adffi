<?php
/* @var $this SourcesController */
/* @var $model Sources */

$this->breadcrumbs=array(
	'Источники'=>array('admin'),
	'Предложение',
);

$this->menu=array(
	array('label'=>'Источники', 'url'=>array('admin')),
);
?>
<div class="panel no-padding">
    <div class="panel-heading">
		<h2>Предложение источника</h2>
	</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	
</div>