<?php
/* @var $this SourcesController */
/* @var $model Sources */
$this->breadcrumbs=array(
	'Источники'=>array('admin'),
	'Просмотр',
);

$this->menu = array(
	array('label'=>'Создать источник', 'url'=>array('create')),
	array('label'=>'Редактировать источник', 'url'=>array('update', 'id' => $model->id)),
	array('label'=>'Источники', 'url'=>array('admin')),
);
$this->pageTitle = 'Просмотр источника: '.$model->url
?>

<b>Отметит </b><select id="read_checed">
	<option></option>
	<option value="1">Прочитанными</option>
	<option value="2">Непрочитанными</option>
</select>
<?php 
$dataProvider = new CActiveDataProvider('Feeds', array(
	'criteria' => array(
		'condition' => 'id_source=?',
		'params' => array($model->id),
		'order' => 'date DESC'
	)
));

$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'bordered hovered',
	'dataProvider' => $dataProvider,
	'responsiveTable' => true,
	'template' => "{items}{pager}",
    'afterAjaxUpdate' => "js:function(){
    	$('.rss-post > .title').click(function(){
			$(this).parent('td').parent('.rss-post').addClass('readed');
		});
    }",
    'rowCssClassExpression' => '$data->readed ? "rss-post readed" : "rss-post"',
	'columns' => array(
		array(
			'name' => 'id',
			'value' => 'CHtml::checkBox("feed[".$data->id."]", "", array("class" => "post-box", "value" => $data->id))',
			'header' => '<input type="checkbox" id="toggle"/>',
			'type' => 'raw',
			'sortable' => false
		),
		array(
			'name' => 'title',
			'header' => 'Описание',
			'type' => 'raw',
			'sortable' => false,
			'value' => '"<a class=\"title\" target=\"_blank\" href=\"".$data->readedLink."\">".$data->title."</a><div>".Yii::app()->controller->words_limit(strip_tags($data->description), 25)."..."."</div>"'
		)
	)
));
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.rss-post > .title').click(function(){
			$(this).parent('td').parent('.rss-post').addClass('readed');
		});

		$('#read_checed').change(function(){
			var data = $('.post-box:checked').serialize(),
				url = '',
				type = $(this).val();

			if(type == ''){
				return false;
			}
			
			if(type == 2){
				url = '/sources/ajaxnoreaded';
			}else{
				url = '/sources/ajaxreaded';
			}

			$.post(url, data, function(data){
				if(data == 'success'){
					var parent = $('.post-box:checked').parent('.rss-post');
					if(type == 2){
						parent.removeClass('readed');
					}else{
						parent.addClass('readed');
					}
				}else{
					console.log(data);
				}

				$('.post-box:checked').removeAttr('checked');
			});

			$(this).val('');
		}).change();
	});
</script>