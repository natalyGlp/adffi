<div class="rss-post<?php echo $data->readed ? ' readed' : '';?>">
	<?php echo CHtml::checkBox('feed['.$data->id.']', '', array('class' => 'post-box', 'value' => $data->id));?>
	<a class="title" target="_blank" href="<?php echo $data->readedLink;?>"><?php echo $data->title;?></a>
	<?php echo $this->words_limit(strip_tags($data->description), 15).'...';?>
</div>