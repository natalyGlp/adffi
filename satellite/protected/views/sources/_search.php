<?php
/* @var $this SitesController */
/* @var $model Sites */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="row">
		<?php echo $form->label($model, 'id_thematic'); ?>
		<?php echo $form->dropDownList($model,'id_thematic', Thematics::model()->list, array('empty' => 'Выберите тематику')); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'categories'); ?>
		<div id="cats_place">
			<?php echo $form->checkBoxList($model, 'categories', array());  ?>
		</div>
		<?php echo $form->error($model,'categories'); ?>
	</div>
	<hr/>
<!-- 	<div class="row buttons">
		<?php echo CHtml::submitButton('Search',array('class' => 'btn')); ?>
	</div> -->

<?php $this->endWidget(); ?>
<script>
$(document).ready(function(){
	$("#Sources_id_thematic").change(function(){
		$("#Sources_categories").html("");
		var val = $(this).val(),
			cats = <?php echo CJavaScript::encode($model->categories);?>;

		if(val !== ""){
			$.post('/categories/ajaxgetlistforsources/?sid=<?php echo $model->id;?>&id_thematic='+val, $.param({cats: cats}), function(data){
				$('#cats_place').html('').append($(data));
			});
		}
	}).change();
});
</script>
</div><!-- search-form -->