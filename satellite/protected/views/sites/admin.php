<?php
/* @var $this SitesController */
/* @var $model Sites */

$this->breadcrumbs = array('Сайты');

$this->menu = array(array(
	'label'=>'Создать сайт', 
	'url'=>array_merge(array('create'), $_GET),
	'visible' => Yii::app()->user->checkAccess('Sites.create'),
	'htmlOptions' => array('class' => 'btn-success'),
),array(
	'label'=>'Проверить обновления', 
	'url'=> array('/update/index'),
	'visible' => Yii::app()->user->isAdmin,
	'htmlOptions' => array('class' => 'btn-info'),
));

$this->pageTitle = 'Сайты';

// $this->widget('bootstrap.widgets.TbButtonGroup', array(
// 	'type' => 'primary',
// 	'buttons' => array(reset($this->menu)),
// 	'htmlOptions' => array('style'=>'margin: 20px 0;')
// ));
?>
<div class="panel no-padding">


    <div class="panel-heading">
    	<h2>Сайты</h2>
    </div>
  	<div class="panel-body">
<?php
    if(Yii::app()->user->checkAccess('Sites.create')) {
    ?>
    <a href="/sites/create" class="btn btn-success">Cоздать</a>
<?php
}
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped',
	'id'=>'sites-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name' => 'tcFilter',
			'value' => '$data->categoryString',
			'filter' => Thematics::model()->listWithChildren
		),
		'title',
		array(
			'name' => 'url',
			'type' => 'raw',
			'value' => '"<a target=\"_blank\" href=\"".$data->url."\">".$data->url."</a>"'
		),
		'theme',
		'debug',
		'api_key',
		/*array(
			'name' =>'api_key',
			'value' => 'md5($data->api_key)'
		),*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array(
				'style' => 'width: 50px',
				),
			'template' => (Yii::app()->user->checkAccess('Sites.moderate') ? '{moderate} ' : ''),
			'buttons' => array(
				'moderate' => array(
					'url' => 'Yii::app()->createUrl("beobject/admin", array("site" => $data->cleanUrl))',
					'icon' => 'edit',
					'label' => 'Управление',
					'options' => array('target' => '_blank'),
				),
			),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array(
				'style' => 'width: 50px',
				),
			'template' => (Yii::app()->user->checkAccess('Sites.update') ? '{update} ' : ''),
			'buttons' => array(
				'update' => array(
					'icon' => 'wrench',
					),
			),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array(
				'style' => 'width: 50px',
				),
			'template' => (Yii::app()->user->checkAccess('Sites.delete') ? '{delete}' : ''),
			'buttons' => array(
				'delete' => array(
					'label' => 'Удалить сайт',
					'click' => "
function() {
	if(!confirm('Вы действительно хотите удалить этот сайт?')) return false;
	var data = {};
	if(!confirm('Оставить файлы сайта и базу данных?'))
	{
		data = {deleteSiteFiles: 1};
	}
	var th=this;
	var afterDelete=function(){};
	$.fn.yiiGridView.update('sites-grid', {
		type:'POST',
		url:$(this).attr('href'),
		data: data,
		success:function(data) {
			$.fn.yiiGridView.update('sites-grid');
			afterDelete(th,true,data);
		},
		error:function(XHR) {
			return afterDelete(th,false,XHR);
		}
	});
	return false;
}
",
				),
			)
		),
	),
));
?>
</div>
</div>