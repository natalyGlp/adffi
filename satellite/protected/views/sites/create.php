<?php
/**
 * @var $this SitesController 
 * @var $model Sites 
 * @var string $tab активная вкладка (tech|general|config)
 */


$this->breadcrumbs=array(
	'Сайты'=>array('admin'),
	$model->isNewRecord ? 'Создание' : 'Редактирование сайта "' . $model->title . '" ('.$model->cleanUrl.')',
);

$this->menu=array(
	array('label' => 'Создать сайт', 'url'=>array('create'), 'visible' => Yii::app()->user->checkAccess('Sites.create')),
	array('label' => 'Сайты', 'url'=>array('admin')),
	array(
		'label' => 'Управление', 
		'url' => array('beobject/admin', 'site' => $model->cleanUrl),
		'visible' => Yii::app()->user->checkAccess('Sites.moderate') && !$model->isNewRecord,
	),
	array(
		'label' => 'Перейти на сайт', 
		'url' => 'http://'.$model->cleanUrl,
		'visible' => !$model->isNewRecord,
		'linkOptions' => array(
			'target' => '_blank',
			),
	),
);
$this->pageTitle = $model->isNewRecord ? 'Создание сайта' : 'Редактирование сайта';

$tabsSettings = array(
		array(
			'label' => 'Технические настройки', 
			'id' => 'techTab',
			'content' => '$this->renderPartial("_formTech", array("model"=>$model), true)', 
			'active' => $tab == 'tech',
			'visible' => Yii::app()->user->checkAccess('Sites.create'),
			'url' => $this->createUrl('update', array('tab' => 'tech', 'id' => $model->primaryKey)),
			),
		array(
			'label'=>'Основные настройки', 
			'id' => 'generalTab',
			'content'=> '$this->renderPartial("_formGeneral", array("model"=>$model), true)', 
			'active' => $tab == 'general',
			'visible' => !$model->isNewRecord && Yii::app()->user->checkAccess('Sites.update'),
			'url' => $this->createUrl('update', array('tab' => 'general', 'id' => $model->primaryKey)),
			),
		array(
			'label'=>'Специальные настройки', 
			'id' => 'specialTab',
			'content'=> '$this->actionConfig($model->primaryKey, true)', 
			'active' => $tab == 'special',
			'visible' => !$model->isNewRecord && Yii::app()->user->checkAccess('Sites.config'),
			'url' => $this->createUrl('update', array('tab' => 'special', 'id' => $model->primaryKey)),
			),
		);

// проверяем условие visible и только тогда запускаем рендеринг контента таба
foreach ($tabsSettings as &$tab) 
{
	if(!$tab['active']) continue; // FIXME: сейчас табы работают как меню
	if(isset($tab['visible']) && $tab['visible'])
	{
		$tab['content'] = eval('return '.$tab['content'].';');
	}
}
?>
<div class="panel no-padding my-blueprint">
	<div class="panel-heading">
		<h2><?php echo $this->pageTitle; ?></h2>
	</div>
	<div class="panel-body">

		<?php $this->widget('bootstrap.widgets.TbTabs', array(
			'type'=>'tabs', // 'tabs' or 'pills'
			'tabs'=> $tabsSettings
		));
		?>

	</div>
</div>

