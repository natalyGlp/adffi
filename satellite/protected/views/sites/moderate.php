<?php
// DEPRECATED
/* @var $this SitesController */
/* @var $model Sites */

	$this->breadcrumbs=array(
		'Сайты'=>array('admin'),
		'Управление',
	);

	$this->menu = array(
        array('label' => t('Manage Content'), 'url' => array('/sites/moderate', 'site'=>$site)),
		array('label' => t('Create Content'), 'url' => array('/beobject/create', 'site'=>$site)),
	);

	foreach(GxcHelpers::getAvailableContentTypeLikeMenu('&site='.$site) as $arr){
		$this->menu[] = $arr;
	}

	$this->menu[] = array('label' => t('Create Term'), 'url' => array('/beterm/create', 'site'=>$site));
	$this->menu[] = array('label' => t('Manage Terms'), 'url' => array('/beterm/admin', 'site'=>$site));
    $this->menu[] = array('label' => t('Create Taxonomy'), 'url' => array('/betaxonomy/create', 'site'=>$site));
    $this->menu[] = array('label' => t('Mangage Taxonomy'), 'url' => array('/betaxonomy/admin', 'site'=>$site));
    $this->menu[] = array('label' => t('Create Menu'), 'url' => array('/bemenu/create', 'site'=>$site));
    $this->menu[] = array('label' => t('Manage Menus'), 'url' => array('/bemenu/admin', 'site'=>$site));
    $this->menu[] = array('label' => t('Create Page'), 'url' => array('/bepage/create', 'site'=>$site));
    $this->menu[] = array('label' => t('Manage Pages'), 'url' => array('/bepage/admin', 'site'=>$site));
    $this->menu[] = array('label' => t('Create Gallery'), 'url' => array('/begalleries/create', 'site'=>$site));
    $this->menu[] = array('label' => t('Manage Galleries'), 'url' => array('/begalleries/admin', 'site'=>$site));
    $this->menu[] = array('label' => t('Manage Albums'), 'url' => array('/bealbums/admin', 'site'=>$site));
    $this->menu[] = array('label' => t('Create Albums'), 'url' => array('/bealbums/create', 'site'=>$site));
    $this->menu[] = array('label' => t('Create Mail Template'), 'url' => array('/bemailtemplate/create', 'site'=>$site));
    $this->menu[] = array('label' => t('Manage Mail templates'), 'url' => array('/bemailtemplate/admin', 'site'=>$site));
    $this->menu[] = array('label' => t('Create Language'), 'url' => array('/belanguage/create', 'site'=>$site));
    $this->menu[] = array('label' => t('Manage Languages'), 'url' => array('/belanguage/admin', 'site'=>$site));
    $this->menu[] = array('label' => t('General setup'), 'url' => array('/besettings/general', 'site'=>$site));
    $this->menu[] = array('label' => t('System setup'), 'url' => array('/besettings/system', 'site'=>$site));
    $this->menu[] = array('label' => t('Social setup'), 'url' => array('/besettings/social', 'site'=>$site));

include(Yii::getPathOfAlias('cms.widgets.views.object.object_manage_widget') . '.php');