<?php
/* @var $this SitesController */
/* @var $model Sites */

$this->breadcrumbs=array(
	'Sites'=>array('admin'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Update', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	'---',
	array('label'=>'List Sites', 'url'=>array('admin')),
	array('label'=>'Create New Site', 'url'=>array('create')),
);
?>

<h1>View Sites #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_thematic',
		'title',
		'url',
		'db_server',
		'db_user',
		'db_password',
		'db_name',
		'config',
	),
)); ?>
