<?php
/* @var $this SitesController */
/* @var $model Sites */
/* @var $form CActiveForm */
?>

<div class="panel-body">

<?php 
$cs = Yii::app()->clientScript;

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'tech-form',
	'action' => $model->isNewRecord ? $this->createUrl('create') : $this->createUrl('update', array('id' => $model->primaryKey)),
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<label for="" class="col-sm-2 control-label">Управление доступностью сайта</label>
		<div class="col-sm-8">
			<div class="checkbox_list">
				<?php echo $form->checkBox($model, 'sandboxed');  ?>
				<?php echo $form->labelEx($model,'sandboxed'); ?>
				<?php echo $form->error($model,'sandboxed'); ?>
			</div>
			<div class="checkbox_list">
				<?php echo $form->checkBox($model, 'status');  ?>
				<?php echo $form->labelEx($model,'status'); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
			<div class="checkbox_list">
				<?php echo $form->checkBox($model, 'debug');  ?>
				<?php echo $form->labelEx($model,'debug'); ?>
				<?php echo $form->error($model,'debug'); ?>
			</div>
		</div>
	</div>



	<div class="form-group">
		<?php echo $form->labelEx($model,'id_thematic',array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?php echo $form->dropDownList($model,'id_thematic', Thematics::model()->list, array('class'=>'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'id_thematic'); ?>
		</div>
	</div>

	<script type="text/javascript">
	$(function() {
		$("#Sites_id_thematic").change(function(){
			$("#Sites__categories").html("");
			var val = $(this).val(),
				cats = <?php echo CJavaScript::encode($model->_categories);?>;
				
			if(val !== ""){
				$.post('/categories/ajaxgetlistforsites/?sid=<?php echo $model->id;?>&id_thematic='+val, $.param({cats: cats}), function(data){
					$('#cats_place').html('').append($(data));
				});
			}
		}).change();
	})
	</script>

	<div class="form-group">
		<?php echo $form->labelEx($model,'_categories', array('class'=>'col-sm-2 control-label')); ?>
		<div id="cats_place" class="col-sm-8">
			<?php echo $form->checkBoxList($model, '_categories', array());  ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'_categories'); ?>
		</div>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'title', array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'title'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'url', array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'url'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'db_server', array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?php echo $form->textField($model,'db_server',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'db_server'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'db_name', array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?php echo $form->textField($model,'db_name',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'db_name'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'db_user', array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?php echo $form->textField($model,'db_user',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'db_user'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'db_password', array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?php echo $form->textField($model,'db_password',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'db_password'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'db_prefix', array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?php echo $form->textField($model,'db_prefix', array('size'=>60,'maxlength'=>255,'class'=>'form-control',
				'disabled' => !empty($model->db_prefix),
				'value' => empty($model->db_prefix) ? 'gxc_' : $model->db_prefix,
			)); ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'db_prefix'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'api_key', array('class'=>'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?php echo $form->textField($model,'api_key', array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<?php echo $form->error($model,'api_key'); ?>
		</div>
	</div>

	<hr/>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<?php echo CHtml::hiddenField('tab', 'tech'); ?>
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-success')); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
</div>