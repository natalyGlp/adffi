<div class="col-sm-3 col-xs-12">
	<div class="thumbnail thema-block<?php echo $data->themeId == $model->theme ? ' active' : '';?>" data-value="<?php echo $data->themeId;?>" data-layouts="<?php echo $data->layouts;?>">
		<a class="btn btn-warning" href="<?php echo $data->image;?>" title="<?php echo $data->description; ?>">
			<i class="icon-white icon-zoom-in"></i>
		</a>
		<?php echo CHtml::image($data->image, '', array('width' => '200px'));?>
		<span class="checked"></span>
		<p>
			<b><?php echo $data->title; ?></b><br/>
			<span><?php echo $data->description; ?></span>
		</p>
	</div>
</div>
