<?php
/**
 * @var Sites $model
 * @var Themes $theme
 * @var NBaseThemeInstaller $installer - установщик темы
 */
?>
<div class="row" style="overflow:hidden">
	<div class="span6">
		<div class="row">
			<h3>Тип сайта</h3>
			<hr />
			<?php if(count($installer->types)): ?>
				<?php
				echo CHtml::activeDropDownList($model, 'type', $installer->types, array('style' => 'display:none'));
				$buttons = array();
				foreach ($installer->types as $value => $type) {
					$buttons[] = array(
						'label'=>$type,
						'htmlOptions' => array(
							'data-value' => $value,
							),
						);
				}
				$this->widget('bootstrap.widgets.TbButtonGroup', array(
					'type' => '',
					'toggle' => 'radio',
					'buttons' => $buttons,
					'htmlOptions' => array(
						'data-for' => CHtml::activeId($model, 'type'),
						),
				)); ?>
			<?php else: echo 'Не поддеживается'; endif; ?>
		</div>
		<br />

		<div class="row">
			<h3>Цвет сайта</h3>
			<hr />
			<?php if(count($installer->colors)): ?>
				<?php
				echo CHtml::activeDropDownList($model, 'color', $installer->colors, array('style' => 'display:none'));
				$buttons = array();
				foreach ($installer->colors as $value => $color) {
					$buttons[] = array(
						'label'=>'<i class="icon icon-file" style="background: '.$color.'"></i>',
						'htmlOptions' => array(
							'data-value' => $value,
							),
						);
				}
				$this->widget('bootstrap.widgets.TbButtonGroup', array(
					'type' => '',
					'toggle' => 'radio',
					'encodeLabel' => false,
					'buttons' => $buttons,
					'htmlOptions' => array(
						'data-for' => CHtml::activeId($model, 'color'),
						),
				)); ?>
			<?php else: echo 'Не поддеживается'; endif; ?>
		</div>
		<br />

		<div class="row">
			<h3>Макет</h3>
			<hr />
			<ul>
			<?php foreach ($installer->layouts as $val => $l): ?>
				<li class="layouts-block<?php echo $model->layout==$val ? ' active' : ''?>" id="l_<?php echo $val;?>" data-value="<?php echo $val;?>">
					<img src="/images/<?php echo $val; ?>.gif" width="200px"/>
					<?php echo CHtml::activeRadioButton($model, 'layout', array('value' => $val, 'uncheckValue' => null));?>
				</li>
			<?php endforeach ?>
			</ul>
		</div>
	</div>
	<div class="span7">
		<button id="themePreview" class="btn btn-warning" style="float:right"><i class="icon-white icon-zoom-in"></i> Live</button>
		<h3>Предпросмотр</h3>
		<hr />
		<p>
			<?php echo CHtml::image($theme->image, 'Theme preview', array(
				'align' => 'center'
			));?>
		</p>
	</div>
</div>
<br />

<div class="row well" style="overflow:hidden">
	<div class="span6">
		<h3>Модули сайта</h3>
		<hr />
		<?php echo CHtml::activeCheckBoxList($model, 'modules', $installer->modules); ?>
	</div>
	<div class="span6">
		<h3>Виджеты сайта</h3>
		<hr />
		<?php echo CHtml::activeCheckBoxList($model, 'widgets', $installer->widgets); ?>
	</div>
</div>
<div class="row alert alert-danger">
	<h4>Внимание!</h4> При нажатии кнопки "Сохранить" настройки старой темы будут удалены
</div>

<script type="text/javascript">
$(function(){
	// выбор цвета/типа сайта через btn-group
	$('.btn-group[data-for]').each(function() {
		var $field = $('#'+$(this).data('for'));
		if(!$field.length)
			return;

		$(this).find('a.active').removeClass('active');
		
		var val = $field.val();
		$el = $(this).find('a[data-value='+val+']');
		if($el.length)
			$el.addClass('active');
	});
	$('.btn-group[data-for]').on('click', 'a', function() {
		var $field = $('#'+$(this).parents('.btn-group').eq(0).data('for'));
		$field.val($(this).data('value'));
	});

	// выбор лейаута сайта
	$('.layouts-block input[type=radio]').hide();
	$('.layouts-block').click(function(){
		$(".layouts-block").removeClass('active');
		$(this).addClass('active');
		$('.layouts-block input[type=radio]').removeAttr('checked');
		$(this).children('input[type=radio]').attr('checked', 'checked');
	});
});
</script>