<?php
/**
 * @param CForm $form форма настроек
 * @param CActiveRecord $site модель сайта
 * @param boolean $partialReturn true, если форма вызывалась из другого экшена. в этом случае вьюха рендерится через renderPartial
 */

if(!$partialReturn)
{
	$this->pageTitle = 'Редактирование настроек темы "'.$site->_config['themes'].'" сайта "'.$site->cleanUrl.'"';
	$this->breadcrumbs = array(
		'Сайты' => 'admin', 
		$this->pageTitle
		);
	$this->menu=array(
		array('label'=>'Manage Sites', 'url'=>array('admin')),
		array('label'=>'Update '.$site->cleanUrl, 'url'=>array('update', 'id'=>$site->primaryKey)),
	);
}



if($form)
	echo $form->render();
else
	echo '<div class="alert alert-info">У данной темы нету настроек</div>';