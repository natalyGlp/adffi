<?php
/* @var $this SitesController */
/* @var $data Sites */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tematic')); ?>:</b>
	<?php echo CHtml::encode($data->id_tematic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('db_server')); ?>:</b>
	<?php echo CHtml::encode($data->db_server); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('db_user')); ?>:</b>
	<?php echo CHtml::encode($data->db_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('db_password')); ?>:</b>
	<?php echo CHtml::encode($data->db_password); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('db_name')); ?>:</b>
	<?php echo CHtml::encode($data->db_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('config')); ?>:</b>
	<?php echo CHtml::encode($data->config); ?>
	<br />

	*/ ?>

</div>