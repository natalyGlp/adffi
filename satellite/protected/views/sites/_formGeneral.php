<?php
/* @var $this SitesController */
/* @var $model Sites */
/* @var $form CActiveForm */
?>

<div class="panel-body">

<?php 
GxcHelpers::registerCss('jcarousel/css/jcarousel.basic.css');
GxcHelpers::registerCss('jcarousel/css/skin.css');
GxcHelpers::registerCss('colorbox/colorbox.css');
GxcHelpers::registerJs('jcarousel/jquery.jcarousel.js');
GxcHelpers::registerJs('colorbox/jquery.colorbox.min.js'); // TODO: зачем?
GxcHelpers::registerJs('js/jquery.imagesloaded.min.js', 'gebo');
GxcHelpers::registerJs('js/jquery.wookmark.js', 'gebo');
GxcHelpers::registerJs('js/gebo_gallery.js', 'gebo');

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'general-form',
	'action' => $model->isNewRecord ? $this->createUrl('create') : $this->createUrl('update', array('id' => $model->primaryKey)),
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal'
	)
)); ?>

	<?php echo $form->errorSummary($model); ?>


		<h3>Тема сайта</h3>
		<hr />
		<?php 
			$currSiteTheme = $model->themeModel;
			$currSiteThemeLayouts = $currSiteTheme ? $currSiteTheme->layouts : '';
			$catIds = array_values(CHtml::listData($model->th_categories, 'id_category', 'id_category'));
			$catIds = implode(',', $catIds);

			$dataProvider=new CActiveDataProvider('Themes', array(
				'criteria' => array(
					'condition' => 't.active=1 AND (th_categories.id_category IN (:catIds) OR th_thematics.id_thematic=:thematicId OR (th_categories.id IS NULL AND th_thematics.id IS NULL))',
					'params' => array(
						':catIds' => $catIds,
						':thematicId' => $model->id_thematic,
						),
					'order' => ($currSiteTheme ? 't.id='.$currSiteTheme->primaryKey.' DESC, ' : '') . 't.id desc',
					'with' => array('th_thematics', 'th_categories'),
					'together' => true,
					'group' => 't.id',
				),
				'pagination' => array(
					'pagesize' => 4,
				)
			));

			$this->widget('bootstrap.widgets.TbListView', array(
				'dataProvider'=>$dataProvider,
				'emptyText' => 'Нет тем для данной тематики сайта',
				'afterAjaxUpdate' => 'function(){
					gebo_gal_grid.mixed();
				}',
				'viewData' => array('model' => $model),
				'itemView'=>'_theme',
				'template' => '<div class="wmk_grid" id="mixed_grid"><div id="layoutsList" class="row" data-default-layouts="'.$currSiteThemeLayouts.'">{items}</div><br clear="all"/></div>{pager}'
			));

			echo CHtml::activeHiddenField($model, 'theme');
		?>
	<br clear="all"/>

	<div id="themeDetailsContainer"><div id="themeDetails"></div></div>

	<hr/>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<?php echo CHtml::hiddenField('tab', 'general'); ?>
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-success')); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>

	<script type="text/javascript">
		$(function(){
			var $theme = $('#<?php echo CHtml::activeId($model, 'theme'); ?>');
			// Инициализация галереи в табах
			$('.nav-tabs').on('mouseup', 'li', function() {
				var interval = setInterval(function() {
					if($('#mixed_grid').is(':visible')) {
						gebo_gal_grid.mixed();
						clearInterval(interval);
					}
				}, 50);
			});

			// Добывает инфу о выбранной теме
			function fetchThemeDetails()
			{
				var themeId = $theme.val();

				if(themeId == '')
					return

				$.ajax({
					url: '<?php echo $this->createUrl("theme") ?>',
					type: 'post',
					data: {themeId: themeId, siteId: <?php echo $model->primaryKey; ?>},
					success: function(data) {
						$('#themeDetailsContainer').css('height', $('#themeDetailsContainer').height())
						$('#themeDetails').fadeOut('fast', function(){
							$('#themeDetails')
								.empty()
								.html(data)
								.fadeIn('fast', function() {
									$('#themeDetailsContainer').css('height', 'auto');
								});
						});
					}
				});
			}

			// переключение тем
			$('body').on('click', '.thema-block', function(e) {
				// клик по кнопке предпросмотра
				if($(e.target).hasClass('btn') || $(e.target).parents('.btn').length)
					return;

				$(".thema-block").removeClass('active');
				$(this).addClass('active');

				var val = $(this).data('value');

				$('.layouts-block input[type=radio]').removeAttr('checked');
				$('.layouts-block').removeClass('active');
				$theme.val(val);

				
				$('.nav-tabs a[href=#specialTab]').hide(300);
				fetchThemeDetails();
			});

			// запрос информации о теме
			fetchThemeDetails();
		});
	</script>
</div>