<?php
$this->breadcrumbs=array(
	'Темы'=>array('admin'),
	'Создание'
);

$this->menu=array(
array('label'=>'Темы','url'=>array('admin')),
);
$this->pageTitle = 'Создание темы';
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>