<?php
$this->breadcrumbs=array(
	'Темы'=>array('admin'),
	'Редактирование'
);

	
$this->menu=array(
array('label'=>'Темы','url'=>array('admin')),
	array('label'=>'Создать тему','url'=>array('create')),
);
$this->pageTitle = 'Редактирование темы';
?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>