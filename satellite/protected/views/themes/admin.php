<?php
$this->breadcrumbs = array(
    'Темы',
);

$this->menu = array(
    array('label' => 'Создать тему', 'url' => array('create')),
);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2>Темы</h2>
    </div>
    <div class="panel-body">
        <a href="/themes/create" class="btn btn-success">Создать</a>
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'type' => 'primary',
            'buttons' => array(
                array(
                    'label' => '',
                    'url' => array('discover'),
                    'visible' => Yii::app()->user->isAdmin,
                    'icon' => 'refresh white',
                    'htmlOptions' => array('class' => 'btn-info'),
                )
            ),
            'htmlOptions' => array('style' => '')
        ));

        $this->pageTitle = 'Темы';

        $this->widget('bootstrap.widgets.TbGridView', array(
            'id' => 'themes-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            // 'filterPosition' => 'header', //Устанавливаем позицию фильтра
             'htmlOptions' => array(
                 'class' => 'table table-hover table-striped',
             ),
            'columns' => array(
                array(
                    'name' => 'preview',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::image($data->image, "", array("width" => "80px","style"=>"height:100px", "class" => "thumbnail")), $data->image)',
                    'filter' => false
                ),
                //'title',
                array(
                    'name' => 'title',
                    'filter' => false,
                ),
                'description',
                array(
                    'type' => 'raw',
                    'name' => 'layouts',
                    'value' => '$data->layouts_types',
                    'filter' => $model->arr_layouts
                ),
                array(
                    'name' => 'active',
                    'value' => 'isset($data->status[$data->active]) ? $data->status[$data->active] : ""',
                    'filter' => $model->status
                ),
                array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '{update}',
                ),
                array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '{delete}',
                ),
            ),
        ));
        ?>
    </div>
</div>