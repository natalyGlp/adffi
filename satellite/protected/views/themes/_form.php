<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'themes-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype' => 'multipart/form-data'
	)
)); ?>

<?php echo $form->errorSummary($model); ?>
	<?php if (!$model->isNewRecord): ?>
		<?php echo CHtml::image($model->image, '', array('width' => '200px')); ?><br/><br/>
	<?php endif ?>

	<div class="">
		<?php
		echo CHtml::checkBox('uniTheme', 0, array('id'=>'uniTheme'));
		echo CHtml::label('Универсальная тема', 'uniTheme');
		?>
		<?php
			$this->widget('ext.nespi.ThematicsTree', array(
				'id' => 'thematics-tree',
				'model' => $model,
				'htmlOptions' => array('style' => 'margin: 20px 0'),	
			));
		?>
	</div>

	<script type="text/javascript">
	$(function() {
		$('#uniTheme').change(function() {
			var isChecked = $(this).prop('checked');
			if(isChecked)
				$('#thematics-tree input:checked').prop('checked', false);
		}).prop('checked', $('#thematics-tree input:checked').length == 0);
		$('#thematics-tree').on('change', 'input', function() {
			var isChecked = $(this).prop('checked');
			if(isChecked)
				$('#uniTheme').prop('checked', false);
		});
	})
	</script>

	<?php if ($model->isNewRecord): ?>
		<?php echo $form->fileFieldRow($model,'zip',array('class'=>'span5','maxlength'=>255)); ?>
	<?php endif ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
	<?php echo $form->dropDownListRow($model,'active',$model->status); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
