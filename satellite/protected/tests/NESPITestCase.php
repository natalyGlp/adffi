<?php
/**
 * The base class for all `hoauth` tests
 */
class NESPITestCase extends CTestCase
{
    /**
     * DEPRECATED
     */
    public static function getMethod($class, $methodName)
    {
        self::getObjMethod($class, $methodName);
    }
    /**
     * helper method to get protected methods for unit testing
     * $method->invokeArgs($obj, array()); // $obj is instance of $class
     */
    protected static function getObjMethod($class, $methodName)
    {
        $class = new ReflectionClass($class);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method;
    }

    /**
     * DEPRECATED
     */
    public static function inv($obj, $method, $args = array())
    {
        return self::invObjMethod($obj, $method, $args);
    }

    /**
     * Invokes object's protected method
     * @param  [type] $obj    [description]
     * @param  [type] $method [description]
     * @param  array  $args   [description]
     * @return [type]         [description]
     */
    public static function invObjMethod($obj, $method, $args = array())
    {
        $m = self::getMethod(get_class($obj), $method);
        return $m->invokeArgs($obj, array());
    }

    /**
     * Sets protected property of an $obj
     */
    public static function setObjProperty($obj, $propertyName, $propertyValue)
    {
        $refObject = new ReflectionObject($obj);
        $refProperty = $refObject->getProperty($propertyName);
        $refProperty->setAccessible(true);
        $refProperty->setValue($obj, $propertyValue);
    }
}