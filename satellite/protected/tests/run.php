<?php
$vendor = dirname(__FILE__).'/vendor';

if (!is_dir($vendor)) {
    throw new CException('Что бы запустить тесты установите пакеты composer (composer install)');
}

exec('php ' . $vendor . '/phpunit/phpunit/phpunit ' . implode(' ', array_slice($argv, 1)), $output);

echo implode(PHP_EOL, $output);
