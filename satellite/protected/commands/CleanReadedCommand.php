<?php
/**
* CleanReadedCommand
*/
class CleanReadedCommand extends  CConsoleCommand{
	public function run($args){
		$db = Yii::app()->db;
		$links = $db->createCommand()->select('link')->from('feeds')->where('readed<=(NOW() - INTERVAL 2 DAY)')->queryAll(false);
		$db->createCommand()->delete('feeds', 'readed<=(NOW() - INTERVAL 2 DAY)');
		foreach($links as $link){
			$db->createCommand()->insert('feeds_bl', array('link' => $link[0]));
		}
	}
}