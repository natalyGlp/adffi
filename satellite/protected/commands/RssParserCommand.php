<?php
/**
* RssParserCommand
*/
Yii::import('ext.yii-feed-widget.*');
class RssParserCommand extends  CConsoleCommand{
	public function run($args){
		$db = Yii::app()->db;

		$urls = $_urls = array();
		if(count($args)){
			$_urls = $db->createCommand()->select(array('rss', 'id'))->from('sources')->where('id IN('.implode(',', $args).') AND active=1')->queryAll(false);
		}else{
			$_urls = $db->createCommand()->select(array('rss', 'id'))->from('sources')->where('rss!="" AND active=1')->queryAll(false);
		}

		foreach($_urls as $u){
			$urls[$u[1]] = $u[0];
		}

		foreach($urls as $id => $url){
			$feed = new SimplePie();

	        $feed->set_feed_url($url);
	        $feed->cache = false;
	        $feed->init();
	        $feed->handle_content_type();
	        $items = $feed->get_items(0, 99999);

	        foreach($items as $item){
	        	if(
	        		$db->createCommand()->select('COUNT(id)')->from('feeds')->where('id_source=? AND link=?', array($id, $item->get_permalink()))->queryScalar()
	        	 || $db->createCommand()->select('COUNT(id)')->from('feeds_bl')->where('link=?', array($item->get_permalink()))->queryScalar()
	        	){
	        		continue;
	        	}

	        	$new = new Feeds;
	        	$new->id_source = $id;
	        	$new->link = $item->get_permalink();
	        	$new->date = $item->get_date('Y-m-d H:i:s');
	        	$new->description = $item->get_description();
	        	$new->title = $item->get_title();
	        	$new->save();
	        }
		}
	}
}