<?php

class CategoriesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';
	public $layout='//layouts/glp1';

	public function actionAjaxgetlistforsites($sid = null, $id_thematic){
		$arr = CHtml::listData(Categories::model()->findAll(array(
			'select' => array('id', 'title'),
			'condition' => 'id_thematic=?',
			'params' => array($id_thematic),
			'order' => 'title ASC'
		)), 'id', 'title');

		$model = Sites::model()->count('id=?', array((int)$sid)) ? Sites::model()->findByPk((int)$sid) : new Sites;

		if($model->isNewRecord){
			$model->_categories = isset($_POST['cats']) ? $_POST['cats'] : array_keys($arr);
		}else{
			$checked = Yii::app()->db
								 ->createCommand()
								 ->select('id_category')
								 ->from('category_sites')
								 ->where('id_site=?', array($sid))
								 ->queryAll(false);

			foreach($checked as $c){
				$model->_categories[$c[0]] = $c[0];
			}
		}
		
		echo CHtml::activeCheckBoxList($model, '_categories', $arr, array(
			'template' => '<div class="checkbox_list">{input} {label}</div>', 
			'separator' => ''
		));
	}

	public function actionAjaxgetlistforsources($sid = null, $id_thematic){
		$arr = CHtml::listData(Categories::model()->findAll(array(
			'select' => array('id', 'title'),
			'condition' => 'id_thematic=?',
			'params' => array($id_thematic),
			'order' => 'title ASC'
		)), 'id', 'title');

		$model = Sources::model()->count('id=?', array((int)$sid)) ? Sources::model()->findByPk((int)$sid) : new Sources;

		if($model->isNewRecord){
			$model->categories = isset($_POST['cats']) ? $_POST['cats'] : array_keys($arr);
		}else{
			$checked = Yii::app()->db
								 ->createCommand()
								 ->select('id_category')
								 ->from('category_sources')
								 ->where('id_source=?', array($sid))
								 ->queryAll(false);

			foreach($checked as $c){
				$model->categories[$c[0]] = $c[0];
			}
		}
		
		echo CHtml::activeCheckBoxList($model, 'categories', $arr, array(
			'template' => '<div class="checkbox_list">{label} {input}</div>', 
			'separator' => ''
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id_thematic)
	{
		$this->layout='//layouts/glp1';
		$model=new Categories;
		$model->id_thematic = $id_thematic;
		// // Uncomment the following line if AJAX validation is needed
		// // $this->performAjaxValidation($model);

		// if(isset($_POST['Categories']))
		// {
		// 	$model->attributes=$_POST['Categories'];
		// 	if($model->save()){
		// 		echo 'success';
		// 		Yii::app()->end();
		// 	}
		// }

		// $this->renderPartial('Categories',array(
		// 	'model'=>$model,
		// ));

		if(isset($_POST['Categories'])){
			$model->setAttributes($_POST['Categories'], true);
			// if($model->save()){
			// 	echo 'success';
			// 	Yii::app()->end();
			// }
			if($model->save())
				$this->redirect(array('/thematics/admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layout='//layouts/glp1';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Categories']))
		{
			$model->attributes=$_POST['Categories'];
			// if($model->save()){
			// 	echo 'success';
			// 	Yii::app()->end();
			// }
			if($model->save())
				$this->redirect(array('/thematics/admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		$this->redirect('/thematics/admin');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Categories');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Categories('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Categories']))
			$model->attributes=$_GET['Categories'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Categories the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Categories::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Categories $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='categories-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
