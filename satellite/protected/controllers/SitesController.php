<?php
class SitesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';
	public $layout='//layouts/glp1';

	public function actionGetlayouts($theme)
	{
		$arr = Sites::model()->getLayouts($theme);
		foreach($arr as $k => $v){
			$name = str_replace('.php', '', $k);
			echo '<option value="'.$name.'">'.$name.'</option>';
		}
		Yii::app()->end();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$this->actionUpdate();
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id = false)
	{
		$model = $id ? $this->loadModel($id) : new Sites();

		// Определяем активную вкладку
		$tab = Yii::app()->user->checkAccess('Sites.create') ? 'tech' : 'general';
		if($this->action->id == 'config')
			$tab = 'special';
		if(isset($_POST['tab']))
			$tab = $_POST['tab'];
		if(isset($_GET['tab']))
			$tab = $_GET['tab'];


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_GET['Sites'])){
			$model->attributes = $_GET['Sites'];
			if(isset($_GET['Sites']['_categories'])){
				$model->_categories = $_GET['Sites']['_categories'];
			}
		}

		if(isset($_POST['Sites'])){
			$model->attributes = $_POST['Sites'];

			if($model->save()) {
				$model->clearCache();
				$this->redirect(array('update','id'=>$model->id, 'tab' => $tab));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'tab' => $tab,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		if(isset($_POST['deleteSiteFiles']))
			$model->deleteSiteData();
		
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// $dataProvider=new CActiveDataProvider('Sites');
		// $this->render('index',array(
		// 	'dataProvider'=>$dataProvider,
		// ));
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Sites('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Sites'])){
			$model->attributes=$_GET['Sites'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionTheme()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_POST['siteId']) && isset($_POST['themeId']))
		{
			$model = Sites::model()->findByPk($_POST['siteId']);
			if(!$model)
				$model = new Sites();

			$model->theme = $_POST['themeId'];
			$installer = Themes::getInstaller($model);
			$theme = Themes::model()->findByAttributes(array('themeId' => $model->theme));

			$this->renderPartial('_themeOptions', array(
				'model' => $model,
				'installer' => $installer,
				'theme' => $theme,
				));
		}
	}

	/**
	 * Конфигурация темы сайта
	 *
	 * @param integer $id id модели
	 * @param boolean $partialReturn если true, метод вернет результат renderPartial(..., ..., true);
	 */
	public function actionConfig($id, $partialReturn = false)
	{
		$site = $this->loadModel($id);
		// Подключим бд редактируемого сайта
		$db = Yii::createComponent($site->dbConfigArray);
		Yii::app()->setComponent(CONNECTION_NAME, $db);

		$theme = $site->theme;
		$className = 'ThemeConfig';
		$classPath = Yii::getPathOfAlias('themes.'.$theme.'.install.'.$className).'.php';
		if(file_exists($classPath) && !class_exists($className, false)) {
			require $classPath;
		}

		if(class_exists($className, false)) {
			$model = new $className;

			$this->performAjaxValidation($model);

			$config = array(
				 // хардкодим екшен формы, что бы можно было вызвать этот экшен из другого экшена
				'action' => $this->createUrl('config', array('id' => $id)),
				'buttons'=>array(
					'submit'=>array(
						'layoutType'=>'success',
						'type' => 'submit',
						'label'=> 'Сохранить',
						'htmlOptions' => array(
							'class' => 'submit',
							'id' => 'submit',
						),
					)
				),
				'activeForm'=>array(
					'class' => 'bootstrap.widgets.TbActiveForm',
					'id' => 'sites-form',
					'enableAjaxValidation'=>true,
					'enableClientValidation'=>false,
					'clientOptions' => array(
						'validateOnSubmit' => true,
						'validateOnChange' => true,
					),
				),
				'elements' => $model->getFormConfig(),
				'model' => $model,
				);

			Yii::import('bootstrap.widgets.TbForm');
			$form = new TbForm($config);
			
			if(!$partialReturn && $form->submitted('submit') && $form->validate()) {
				if($model->configure()) {
					Yii::app()->user->setFlash('success', 'Настройки сайта успешно обновленны');
					$site->clearCache();
				} else
					Yii::app()->user->setFlash('fail', 'Во время обновления настроек сайта произошли ошибки');

				$this->actionUpdate($id);

				return;
			}
		}

		if($partialReturn)
			return $this->renderPartial('config', array(
				'site' => $site,
				'form' => isset($form) ? $form : null,
				'partialReturn' => $partialReturn,
				), $partialReturn);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Sites the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id){
		$model=Sites::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Sites $model the model to be validated
	 */
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='sites-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}