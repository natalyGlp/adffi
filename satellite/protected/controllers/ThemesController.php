<?php

class ThemesController extends Controller
{

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	//public $layout='//layouts/column2';
	public $layout='//layouts/glp1';
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Themes;

		if (isset($_POST['Themes'])) {
			$model->setAttributes($_POST['Themes']);
            $model->zip=CUploadedFile::getInstance($model,'zip');

			if (isset($_FILES['Themes']['tmp_name']['zip']) && !empty($_FILES['Themes']['tmp_name']['zip'])) {
				$zip = Yii::app()->zip;
				$layoutsPath = Yii::getPathOfAlias('themes');
				$zip->extractZip($_FILES['Themes']['tmp_name']['zip'], $layoutsPath);

				$themeId = str_replace('.zip', '', $_FILES['Themes']['name']['zip']);
				$themePath = $layoutsPath.'/'.$themeId;
				if (file_exists($themePath)) {
					recursiveChmod($themePath, 0777, 0777);

					$installer = Themes::getInstaller($themeId);

					$model->layouts = implode(',', array_keys($installer->layouts));
					$model->preview = $installer->preview;
					$model->themeId = $themeId;
				}
			}

			if ($model->save()) {
				$this->redirect(array('update','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->setScenario('update');
		if(isset($_POST['Themes'])){
			$model->attributes=$_POST['Themes'];
			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{

		$dataProvider=new CActiveDataProvider('Themes');
		$dataProvider->pagination->pageSize=50;
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Сканирует папку с темами и добавляет все новые темы
	 */
	public function actionDiscover()
	{
		$themesDirs = scandir(Yii::getPathOfAlias('themes'));
		$models = Themes::model()->findAll();
		$availableThemes = array();
		foreach ($models as $model) {
			$availableThemes[$model->themeId] = $model;
		}

		foreach ($themesDirs as $dir) {
			if($dir[0] == '.' || strpos($dir, '~') !== false) {
				continue;
			}

			$themeId = $dir;

			if(!array_key_exists($themeId, $availableThemes)) {
				// Добавляем новую тему
				$model = new Themes();
				$model->attributes = array(
					'themeId' => $themeId,
					'title' => $themeId,
					'description' => $themeId,
					);
			} else {
				// просто обновим тему
				$model = $availableThemes[$themeId];
			}

			$installer = Themes::getInstaller($themeId);
			$model->layouts = implode(',', array_keys($installer->layouts));
			$model->preview = $installer->preview;
			$model->save();
		}

		$this->redirect('admin');
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Themes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='themes-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAdmin()
	{
		$model=new Themes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Themes'])){
			$model->attributes=$_GET['Themes'];
		}

		// Увеличение фоток
		$url = Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.frontend.fancybox'), false, false, -1, YII_DEBUG);
		$cs = Yii::app()->clientScript;
		$cs->registerCssFile($url.DIRECTORY_SEPARATOR."jquery.fancybox-1.3.4.css");
		$cs->registerScriptFile($url.DIRECTORY_SEPARATOR."jquery.fancybox-1.3.4.pack.js");
		$cs->registerScript(__FILE__.'#fancyInit', "
			$('table a').each(function() {
				if(/\.(jpg|png|gif|jpeg)$/.test(this.href.toLowerCase()) && this.target != '_blank' && $(this).has('img'))
					$(this).fancybox(); 
			})
			");

		$this->render('admin',array(
			'model'=>$model,
		));
	}
}

function recursiveChmod ($path, $filePerm=0644, $dirPerm=0755) {
    // Check if the path exists
    if (!file_exists($path)) {
        return(false);
    }
 
    // See whether this is a file
    if (is_file($path)) {
        // Chmod the file with our given filepermissions
        @chmod($path, $filePerm);
 
    // If this is a directory...
    } elseif (is_dir($path)) {
        // Then get an array of the contents
        $entries = scandir($path);
 
        // Remove "." and ".." from the list
        $ignore = array('..', '.');
 
        // Parse every result...
        foreach ($entries as $entry) {
            // And call this function again recursively, with the same permissions
    		if (in_array($entry, $ignore)) continue;
            recursiveChmod($path."/".$entry, $filePerm, $dirPerm);
        }
        // When we are done with the contents of the directory, we chmod the directory itself
        @chmod($path, $dirPerm);
    }
 
    // Everything seemed to work out well, return true
    return(true);
}