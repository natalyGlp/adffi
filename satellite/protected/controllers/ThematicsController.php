<?php
class ThematicsController extends Controller{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';
	public $layout='//layouts/glp1';


	public function init(){
		if(Yii::app()->user->checkUserRole(User::MODERATOR)){
			$url = Yii::app()->createAbsoluteUrl('/sources/admin');
			$this->redirect($url);
		}
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id){
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate(){
		$model=new Thematics;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		// if(isset($_GET['Thematics'])){
		// 	$model->attributes = $_GET['Thematics'];
		// }

		if(isset($_POST['Thematics'])){
			$model->setAttributes($_POST['Thematics'], true);
			// if($model->save()){
			// 	echo 'success';
			// 	Yii::app()->end();
			// }
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));

		//Yii::app()->end();

		// $model=new Sources;
		// if(isset($_GET['Sources'])){
		// 	$model->attributes = $_GET['Sources'];
		// 	if(isset($_GET['Sources']['categories'])){
		// 		$model->categories = $_GET['Sources']['categories'];
		// 	}
		// }

		// if(isset($_POST['Sources'])){
		// 	$model->setAttributes($_POST['Sources'], false);
		// 	$model->categories = isset($_POST['Sources']['categories']) ? $_POST['Sources']['categories'] : array();
		// 	if($model->save())
		// 		$this->redirect(array('update','id'=>$model->id));
		// }

		// $this->render('create',array(
		// 	'model'=>$model,
		// ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Thematics'])){
			$model->setAttributes($_POST['Thematics'], false);
			// if($model->save()){
			// 	echo 'success';
			// 	Yii::app()->end();
			// }
			if($model->save())
				$this->redirect(array('admin'));
		}

		// $this->renderPartial('update',array(
		// 	'model'=>$model,
		// ));
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id){
		$this->loadModel($id)->delete();
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('Thematics');
		$dataProvider->pagination->pageSize=50;
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}


	/**
	 * Manages all models.
	 */
/*	public function actionAdmin()
	{
		// $model =  Thematics::model()->findAll(array(
		// 	'select' => array('id', 'title'),
		// 	'condition' => 'active=1',
		// 	'order' => 'title ASC'
		// ));
		$model = new  Thematics('search');
		// $model->unsetAttributes();
		// if(isset($_GET['User'])){
		// 	$model->setAttributes($_GET['User'], false);
		// }

		$this->render('admin',array(
			'data'=>$model,
		));
	}*/

	public function actionAdmin(){
		$data = array();
		$thematics = Thematics::model()->findAll(array(
			'select' => array('id', 'title'),
			'condition' => 'active=1',
			'order' => 'title ASC'
		));

		$ids = array();
		$i = 0;
		foreach($thematics as $thema){
			$data[$thema->id]['text'] = $this->renderPartial('_tree_item', array('type' => 'thematics', 'odd' => $i%2==0, 'data' => $thema), true);
			$ids[$thema->id] = $thema->id;
			$data[$thema->id]['expanded'] = false;
			$i++;
		}

		if(count($ids)){
			$categories = Categories::model()->findAll(array(
			'select' => array('id', 'title', 'id_thematic'),
				'condition' => 'active=1 AND id_thematic IN('.implode(',', $ids).')',
				'order' => 'title ASC'
			));

			$i = 0;
			foreach($categories as $cat){
				$data[$cat->id_thematic]['children'][$cat->id]['text'] = $this->renderPartial('_tree_item', array('type' => 'categories', 'odd' => $i%2==0, 'data' => $cat), true);
				$i++;
			}
		}

		$this->render('admin',array(
			'data'=>$data,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Thematics the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id){
		$model=Thematics::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Thematics $model the model to be validated
	 */
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='thematics-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
