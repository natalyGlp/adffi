<?php
Yii::import('cms.controllers.BeorderController');
Yii::import('cms.models.order.*');
Yii::import('cms.models.object.*');
Yii::import('application.modules.shop.models.*');

class SiteController extends Controller{
	public $layout='column1';


	/**
	 * Declares class-based actions.
	 */
	public function actions(){
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError(){
		$this->layout='column2';
	    if($error=Yii::app()->errorHandler->error){
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
	
	/**
	 * Displays the login page
	 */
	public function actionLogin(){
		// $this->layout = 'login';
		$this->layout = 'loginNew';
		Yii::app()->clientScript->reset();

		if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='validate-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm'])){
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$url = Yii::app()->user->returnUrl;
				
				if(Yii::app()->user->checkUserRole(User::MODERATOR)){
					$url = Yii::app()->createAbsoluteUrl('/shop');
				}

				$this->redirect($url);
			}
		}
		// display the login form
		$this->render('_login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout(){
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionIndex(){

		$model = new Order;
	//	$products = new OrderCheck;
		$newProductCriteria = new CDbCriteria;
		
	//	$filterDateFrom = date("Y-m-d 00:00:00",time()-2592000);
		$newProductCriteria->select = array('object_title');
		$newProductCriteria->compare('object_type','catalog');
		$newProductCriteria->limit = 5;
		$newProductCriteria->order = 'object_date DESC';
		$newProducts = Object::model()->findAll($newProductCriteria);

		$topProductCriteria = new CDbCriteria;
		$topProductCriteria->select = array('prod_id, count(*) as quantity');
		$topProductCriteria->group = 'prod_id';
		$topProductCriteria->order = 'count(*) DESC';
		$topProductCriteria->limit = 5;

		$topProducts = OrderCheck::model()->findAll($topProductCriteria);

		$topUserCriteria = new CDbCriteria;
		$topUserCriteria->select = array('user.username, count(*) as id');
		$topUserCriteria->alias = 'user';
		$topUserCriteria->group = 'id_user';
		$topUserCriteria->join = 'LEFT JOIN `user_sites` as `sites` ON sites.id_user = user.id';
		$topUserCriteria->order = 'count(*) DESC';
		$topUserCriteria->limit = 5;

		$topUsers = User::model()->findAll($topUserCriteria);

		$topSiteCriteria = new CDbCriteria;
		$topSiteCriteria->select = array('title','url');
		$topSiteCriteria->limit = 5;
		$topSiteCriteria->order = 'id DESC';
		$topSites = Sites::model()->findAll($topSiteCriteria);

		$topThemCriteria = new CDbCriteria;
		$topThemCriteria->select = array('id_thematic, count(*) as id');
		$topThemCriteria->group = 'id_thematic';
		$topThemCriteria->order = 'count(*) DESC';
		$topThemCriteria->limit = 5;

		$topThematics = Sites::model()->findAll($topThemCriteria);

		$this->layout = 'glp1';
		$this->render('index',array(
			'model'=>$model,
			'newProducts'=>$newProducts, 
			'topProducts'=>$topProducts, 
			'topUsers'=>$topUsers, 
			'topSites'=>$topSites,
			'topThematics'=>$topThematics
			)
		);
	}
}
