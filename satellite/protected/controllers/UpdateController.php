<?php
/**
 * Контроллер для запуска автообновлений NESPI
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.controllers
 */
// TODO: автобекап бд перед миграцией
// TODO: поменять структуру бд миграций. хранить версию, название таблицы, мб время применения.
// в названии миграции лучше использовать такой формат: таблица(модель)_старая версия_новая версия_коммент
// TODO: людской вывод информации клиенту
// TODO: возможность ручного применения миграций
//
// http://www.infoq.com/articles/db-versioning-scripts
// Flyway, MyBatis Migrations, Liquidbase
// http://joxi.ru/zdCKU_3JTJDkU-cOYDI
Yii::import('cms.controllers.BeupdateController');
class UpdateController extends BeupdateController
{
    //public $layout='//layouts/column2';
    public $layout='//layouts/glp1';

    public function init()
    {
        parent::init();
        $this->menu = array(array(
            'label' => 'Все сайты',
            'url' => array('/sites/admin'),
        ), array(
            'label' => 'Проверить обновления',
            'url' => array('index'),
        ), array(
            'label' => 'Проверить обновления сателлитки',
            'url' => array('satellite'),
        ), array(
            'label' => 'Ручной запуск миграций',
            'url' => array('migrate'),
        ), array(
            'label' => 'Очистить кэши всех сайтов',
            'url' => array('clear', 'id' => 'cache'),
        ), array(
            'label' => 'Очистить assets всех сайтов',
            'url' => array('clear', 'id' => 'assets'),
        ), array(
            'label' => 'Обновить файловые системы сайтов',
            'url' => array('clear', 'id' => 'fs'),
        ));
    }

    /**
     * Экшен для обновления файлов сателлитки
     */
    public function actionSatellite()
    {
        $this->render('index', array(
            'updateRoute' => 'updatesatellite',
            'checkUpdateRoute' => 'checksatelliteupdates',
        ));
    }

    /**
     * NOTE: разница лиш в том, что файл версии находится по другому пути и все версии сателитки имеют преффикс S
     */
    public function actionCheckSatelliteUpdates()
    {
        $this->_versionFileAlias = 'application.version';
        $this->_defaultVersion = 'S0';
        $this->actionCheckUpdates();
    }

    /**
     * NOTE: разница лиш в том, что файл версии находится по другому пути и все версии сателитки имеют преффикс S
     */
    public function actionUpdateSatellite()
    {
        $this->_versionFileAlias = 'application.version';
        $this->_defaultVersion = 'S0';
        $this->actionUpdate();
    }

    public function actionMigrate()
    {
        $output = '';
        if (Yii::app()->request->isPostRequest) {
            $action = 'new';
            if (isset($_POST['migrate-up'])) {
                $action = 'up';
            }
            if (isset($_POST['migrate-new'])) {
                $action = 'new';
            }

            $error = false;
            try {
                $output .= PHP_EOL . parent::runMigration($action, 'application.migrations');
                $output .= PHP_EOL . $this->migrateSubDirs($action, 'application.modules');
                $output .= PHP_EOL . $this->migrateSubDirs($action, 'cms.migrations', null, true);
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', $e->getMessage());
                $error = true;
            }

            if (!$error) {
                Yii::app()->user->setFlash('success', 'Миграция сайтов завершена успешно');
            }
        }

        $html = '<form method="POST">
            <p><input value="List new Migrations" type="submit" name="migrate-list" class="btn" />
            <p><input value="Run Migrations" type="submit" name="migrate-up" class="btn btn-success" />
        </form>';

        if (!empty($output)) {
            $html .= "<pre>$output</pre>";
        }

        $this->renderText($html);
    }

    protected function migrateSubDirs($action, $path, $limit = null, $runOnSatellites = false)
    {
        $dirs = scandir(Yii::getPathOfAlias($path));
        $output = '';
        foreach ($dirs as $dir) {
            if ($dir[0] == '.') {
                continue;
            }

            $migrationPath = $path . '.' . $dir;

            if (strpos($migrationPath, 'migrations') === false) {
                $migrationPath .= '.migrations';
            }

            if ($runOnSatellites) {
                $output .= PHP_EOL . $this->runMigration($action, $migrationPath, $limit);
            } else {
                $output .= PHP_EOL . parent::runMigration($action, $migrationPath, $limit);
            }
        }

        return $output;
    }

    /**
     * Ищем информацию о модулях и их версиях
     */
    protected function getComponentsVersions()
    {
        if (in_array($this->action->id, array('update', 'checkupdates'))) {
            return parent::getComponentsVersions();
        }

        $components = array();
        $modules = Yii::app()->modules;
        foreach ($modules as $moduleId => $devnull) {
            $versionFile = Yii::getPathOfAlias('application.modules.' . $moduleId . '.version');

            $version = '{"version": "' . preg_replace('/\d+/', $moduleId . '$0', $this->_defaultVersion) . '"}';
            if (file_exists($versionFile)) {
                $version = file_get_contents($versionFile);
            }

            $version = CJSON::decode($version);
            $components['modules'][] = $version['version'];
        }
        return $components;
    }

    /**
     * Переопределяем метод запуска миграций таким образом, что бы применять их на всех сайтах сателлитки
     */
    protected function runMigration($action = 'up', $path = 'application.migrations', $limit = null)
    {
        if ($this->action->id == 'update' || $this->action->id == 'migrate') {
            $currentDb = Yii::app()->db;
            $sites = Sites::model()->findAll();
            foreach ($sites as $site) {
                // производим ротацию бд
                try {
                    $siteDb = $site->siteDbConnection;
                    $siteDb->schema->refresh();
                    Yii::app()->setComponent('db', $siteDb);
                    Yii::app()->setComponent(CONNECTION_NAME, $siteDb);

                    $this->log('Migrating site ' . $site->url);
                    $result = parent::runMigration($action, $path, $limit);
                    if (strpos($result, 'Migration failed') !== false || strpos($result, 'Error') !== false) {
                        return $result;
                    }
                } catch (Exception $e) {
                    throw new CException("Ошибка миграции сайта {$site->title} ({$site->cleanUrl}, db: {$site->db_name}): " . $e->getMessage());
                }
            }
            Yii::app()->setComponent('db', $currentDb);
            return $result;
        } else {
            return parent::runMigration($action, $path, $limit);
        }
    }

    public function actionClear($id = 'cache')
    {
        if ($id == 'cache') {
            self::clearSitesCache();
            Yii::app()->user->setFlash('success', 'Кэши сайтов были успешно очищены');
        } elseif ($id == 'assets') {
            self::clearSitesAssets();
            Yii::app()->user->setFlash('success', 'Assets сайтов были успешно очищены');
        } elseif ($id == 'fs') {
            self::updateSitesFS();
            Yii::app()->user->setFlash('success', 'Файловые системы сайтов были успешно обновлены');
        }

        $this->redirect('index');
    }

    public static function eachSite($callback)
    {
        $sites = Sites::model()->findAll();
        foreach ($sites as $site) {
            $callback($site);
        }
    }

    public static function clearSitesCache()
    {
        self::eachSite(function ($site) {
            $site->clearCache();
        });
    }

    public static function clearSitesAssets()
    {
        self::eachSite(function ($site) {
            $site->clearAssets();
        });
    }

    public static function updateSitesFS()
    {
        self::eachSite(function ($site) {
            $site->manageSiteFS();
        });
    }

    public function getViewFile($view)
    {
        $parentId = get_parent_class($this);
        $parentId = str_replace('controller', '', strtolower($parentId));
        $view = str_replace($this->id, $parentId, $view);
        return parent::getViewFile($view);
    }

    /**
     * @link http://www.yiiframework.com/wiki/226/run-yiic-directly-from-your-app-without-a-shell/
     */
    // public function actionIndex()
    // {
    //     $actions = array(
    //         'new',
    //         'history',
    //         'up',
    //         'down',
    //         );
    //     $actions = array_combine($actions, $actions);
    //     $migrationPathes = array(
    //         'application.migrations',
    //         'cms.migrations',
    //         'cms.extensions.yii-notify.migrations',
    //         );
    //     $migrationPathes = array_combine($migrationPathes, $migrationPathes);
    //     $data = '';
    //     if(Yii::app()->request->isPostRequest && isset($_POST['password']) && $_POST['password'] == 'glpsat')
    //     {
    //         $action = isset($_POST['action']) && in_array($_POST['action'], $actions) ? $_POST['action'] : reset($actions);
    //         $migrationPath = isset($_POST['migrationPath']) && in_array($_POST['migrationPath'], $migrationPathes) ? $_POST['migrationPath'] : reset($migrationPathes);
    //         $data = $this->runMigration($action, $migrationPath, isset($_POST['limit']) ? $_POST['limit'] : null);
    //     }

    //     $this->render('index', array(
    //         'actions' => $actions,
    //         'migrationPathes' => $migrationPathes,
    //         'data' => $data,
    //         ));
    // }
}
