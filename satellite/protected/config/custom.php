<?php
/**
 * В этом файле находятся настройки специфические для текущего приложения сателлитной сети
 */
return array(
	'preload'=>array('shopBootstrap'),

	'modules' => array(
        'shop' => array(
            'components' => array(
                'affiliate' => array(
                   'affiliateUrl' => 'http://cpa.proffi.co/scripts/server.php',
                   'userName' => 'support@proffi.co',
                   'password' => 'Ba4OMCSr4vYm',
                ),
            ),
        ),
    ),
	'components' => array(
		'shopBootstrap'=>array( // инициализация костылей для супермагазина
			'class'=>'application.modules.shop.components.ShopBootstrap',
			'db' => array(
				 'connectionString' => 'mysql:host=localhost;dbname=g1_shop',
//				'connectionString' => 'mysql:host=localhost;dbname=_shop',
				 'username' => 'g1',
                 'password' => 'D53SeyXsa',
//				'username' => 'root',
//				'password' => '111',
    			'enableParamLogging'=>YII_DEBUG, // логирует SQL вместе с привязанными параметрами
				'schemaCachingDuration' => 3600,
				'emulatePrepare' => true,
				'charset' => 'utf8',
				'tablePrefix' => 'gxc_',
				'class' => 'CDbConnection',
			),
		),
	),
	
);
