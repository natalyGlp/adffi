<?php
/**
 * ВНИМАНИЕ! Содержимое этого файла  должно быть одинаковое для всех сателлитных сетей
 * Для специфической настройки приложения используйте файл custom.php
 */

define('CONNECTION_NAME', 'cms_db');

require_once CMS_PATH.'/common/define.php';
require_once CMS_FOLDER.'/components/globals.php';
require_once CORE_FOLDER.'/vendor/autoload.php';

Yii::setPathOfAlias('uploads', dirname(__FILE__).'/../../uploads');
Yii::setPathOfAlias('common', COMMON_FOLDER);
Yii::setPathOfAlias('cms', CMS_FOLDER);
Yii::setPathOfAlias('core', CORE_FOLDER);
Yii::setPathOfAlias('frontend', FRONT_END);
Yii::setPathOfAlias('themes', LAYOUTS_PATH);
Yii::setPathOfAlias('backend', BACK_END);
Yii::setPathOfAlias('cmswidgets', CMS_FOLDER.'/widgets');
Yii::setPathOfAlias('bootstrap', CORE_FOLDER.'/vendor/clevertech/yii-booster/src');
Yii::setPathOfAlias('satellite.widgets', dirname(__FILE__).'/../extensions/nespi');
Yii::setPathOfAlias('cms.views.layouts.main', dirname(__FILE__).'/../views/layouts/main');

$params = require dirname(__FILE__).'/params.php';

return CMap::mergeArray(array(
    'basePath'=>dirname(__FILE__).'/..',
    'name'=>'Satellite',
    'id' => 'satellite',
    'language' => 'ru',
    'sourceLanguage' => 'ru',

    'preload'=>array(
        'log',
        'bootstrap',
        'debug'
    ),

    'onBeginRequest' => array('Sites', 'prepareForRequestIfSiteEdited'),

    'import'=>array(
        'application.models.*',
        'cmswidgets.*',
        'cms.components.*',
        'cms.components.storages.*',
        'application.components.*',
        'cms.extensions.*',
        'cms.models.*',
        'cms.modules.*',
        'ext.yii-mail.YiiMailMessage',
        'cms.components.user.*',
        // 'cms.models.user.*',
        'cms.models.settings.*',
        'cms.models.object.*',
        'cms.models.resource.*',
        'cms.models.page.*',
        'cms.models.galleries.*',
        'cms.models.banners.*',
        'cms.models.mail.*',
        'cms.models.catalog.*',
         // Import widgets  Classes
        'cmswidgets.*',
        //Import Common Classes
        'common.components.*',
        'common.extensions.*',

        //Translate Module
        'cms.modules.translate.TranslateModule',
        'cms.modules.translate.controllers.*',
        'cms.modules.translate.models.*',

        //Yii Mail Extensions
        'cms.extensions.yii-mail.*',
    ),

    'controllerMap' => CMap::mergeArray(array(
        'YiiFeedWidget' => 'ext.yii-feed-widget.YiiFeedWidgetController'
    ), require CMS_FOLDER.'/data/controllerMap.php'),

    'defaultController' => 'shop/',
    'modules' => array(
        'gii' => array(
            'class'=>'system.gii.GiiModule',
            'password'=>'12345',
            'ipFilters'=>array('127.0.0.1'),
            'generatorPaths' => array(
                'bootstrap.gii'
            ),
        ),
        'auth'=>array(
            'strictMode' => false, // when enabled authorization items cannot be assigned children of the same type.
            'userClass' => 'User', // the name of the user model class.
            'userIdColumn' => 'id', // the name of the user id column.
            'userNameColumn' => 'username', // the name of the user name column.,
            'appLayout' => 'application.views.layouts.main'
        ),
    ),
    'components' => array(
        'zip'=>array(
            'class'=>'ext.zip.EZip',
        ),
        'notify' => array(
            'class' => 'cms.extensions.yii-notify.components.NotifyComponent',
            ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'php',
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
        'bootstrap'=>array(
            'class'=>'bootstrap.components.Bootstrap',
            'responsiveCss' => true,
            // отключаем css и js на ajax запросах
            'ajaxCssLoad' => false,
            'ajaxJsLoad' => false,
        ),
        'authManager'=>array(
                'class'=>'auth.components.CachedDbAuthManager',
                'itemTable'=>'AuthItem',
                'itemChildTable'=>'AuthItemChild',
                'assignmentTable'=>'AuthAssignment',
                // 'defaultRoles' => array('Guest', 'Admin', 'Redactor', 'Moderator'),
                'behaviors'=>array(
                'auth'=>array(
                    'class'=>'auth.components.AuthBehavior',
                ),
            ),
        ),
        'user'=>array(
            'class' => 'auth.components.AuthWebUser',
            'guestName' => 'Гость',
            'allowAutoLogin' => true,
            'loginUrl' => array('/site/login'),
            'admins'=>array('admin'),
            'autoUpdateFlash' => false,
        ),
        'db'=>require dirname(__FILE__).'/db.php',
        'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),
        'debug' => array(
            'class' => 'core.vendor.zhuravljov.yii2-debug.Yii2Debug',
        ),
        'errorHandler'=>array(
            'errorAction'=>'site/error',
        ),
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName' => false,
            'rules'=>array(
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

                '<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
            ),
        ),
        'image' => array(
            'class'=> 'cms.extensions.image.CImageComponent',
            'driver'=>'GD'
        ),
        'nespiMessages' => array(
            'class'=>'CPhpMessageSource',
            'basePath'=> Yii::getPathOfAlias('cms.messages'),
            'language' => 'en',
        ),
        //Use the Settings Extension and Store value in Database
        'settings'=>array(
                'class'     => 'cms.extensions.settings.CmsSettings',
                'cacheId'   => 'global_website_settings',
                'cacheTime' => 84000,
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                ),
                array(
                    'class'=>'CWebLogRoute',
                    'enabled' => false,
                ),
            ),
        ),

        // замена стандартному ClientScript, которая умеет не отправлять скрипты при аякс запросах. пока что для теста работает только на бекенде
        'clientScript' => array(
            'class' => '\nlac\NLSClientScript',
            //'excludePattern' => '/\.tpl/i', //js regexp, files with matching paths won't be filtered is set to other than 'null'
            //'includePattern' => '/\.php/', //js regexp, only files with matching paths will be filtered if set to other than 'null'

            'mergeJs' => false, //def:true
            // 'compressMergedJs' => false, //def:false

            'mergeCss' => false, //def:true
            // 'compressMergedCss' => false, //def:false

            // 'mergeJsExcludePattern' => '/edit_area/', //won't merge js files with matching names

            // 'mergeIfXhr' => true, //def:false, if true->attempts to merge the js files even if the request was xhr (if all other merging conditions are satisfied)

            // 'serverBaseUrl' => 'http://localhost', //can be optionally set here
            // 'mergeAbove' => 1, //def:1, only "more than this value" files will be merged,
            // 'curlTimeOut' => 10, //def:10, see curl_setopt() doc
            // 'curlConnectionTimeOut' => 10, //def:10, see curl_setopt() doc

            // 'appVersion'=>1.0 //if set, it will be appended to the urls of the merged scripts/css
          )
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>$params,
), require dirname(__FILE__).'/custom.php');
