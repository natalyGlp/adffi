<?php
// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.

$_SERVER['HTTP_HOST'] = '';
$_SERVER['REQUEST_URI'] = '/';

require_once CMS_PATH . '/common/globals.php';
require_once CMS_PATH . '/common/define.php';

Yii::setPathOfAlias('common', COMMON_FOLDER);
Yii::setPathOfAlias('cms', CMS_FOLDER);
Yii::setPathOfAlias('frontend', FRONT_END);
Yii::setPathOfAlias('backend', BACK_END);
Yii::setPathOfAlias('cmswidgets', CMS_FOLDER . '/widgets');

$params = require (dirname(__FILE__) . '/params.php');
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Satellite',
    'id' => 'satellite',
    'import' => array(
        'ext.yii-mail.YiiMailMessage',
        'application.models.*',
        'cmswidgets.*',
        'cms.models.*',
        'cms.models.object.*',
    ),
    'components' => array(
        'db' => require_once (dirname(__FILE__) . '/db.php'),
        'notify' => array(
            'class' => 'cms.extensions.yii-notify.components.NotifyComponent',
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'php',
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
    ),
    // database migration, don't ask for confirmation
    'commandMap'=>array(
        'migrate'=>array(
            'class'=>'system.cli.commands.MigrateCommand',
            'migrationTable' => '{{migration}}'
        ),
    ),
    'params' => $params,
);
