<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is displayed in the header section
	'title'=>'My Yii Blog',
	// this is used in error pages
	'adminEmail'=>'no-replay@satellite.com',
	// number of posts displayed per page
	'postsPerPage'=>50,
	// maximum number of comments that can be displayed in recent comments portlet
	'recentCommentCount'=>50,
	// maximum number of tags that can be displayed in tag cloud portlet
	'tagCloudCount'=>20,
	// whether post comments need to be approved before published
	'commentNeedApproval'=>true,
	// the copyright information displayed in the footer section
	'copyrightInfo'=>'Copyright &copy; 2009 by My Company.',

	// Директория в которой сателлитка будет создавать новые сайты
	// если использовать относительный путь, то в качестве начальной директории будет директория сателлитки
	'sitesDir' => '../sites',
	// путь к директории песочницы
	'sandboxDir' => '..',
);
