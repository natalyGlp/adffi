<?php

/**
 * This is the model class for table "thematics".
 *
 * The followings are the available columns in table 'thematics':
 * @property integer $id
 * @property string $title
 * @property integer $active
 */
class Thematics extends CActiveRecord
{
	const CACHE_ID_THEMATICS_CHILDREN = 'Thematics::getListWithChildren()';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Thematics the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'thematics';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, active', 'safe', 'on'=>'search'),
		);
	}

	public function getList(){
		return CHtml::listData(self::model()->findAll(array(
			'select' => array('id', 'title'),
			'condition' => 'active=1',
			'order' => 'title ASC'
		)), 'id', 'title');
	}

	/**
	 * Возвращает дерево тематик с их категориями для выпадающего меню
	 * @return array список для выпадающего меню
	 */
	public function getListWithChildren()
	{
		$cacheId = self::CACHE_ID_THEMATICS_CHILDREN;
		if(!($list = Yii::app()->cache->get($cacheId))) {
			$thematics = $this->with('categories')->findAll(array(
				'condition' => 'categories.active=1 AND t.active=1',
				'order' => 't.title ASC'
				));

			$list = array();

			foreach ($thematics as $t) {
				$list['t'.$t->primaryKey] = $t->title;

				if($t->categories) {
					foreach ($t->categories as $c) {
						$list['c'.$c->primaryKey] = ' - ' . $c->title;
					}
				}
			}

			Yii::app()->cache->set($cacheId, $list);
		}

		return $list;
	}

	protected function afterSave()
	{
		// Обновляем кэш
		//Yii::app()->cache->sate(self::CACHE_ID_THEMATICS_CHILDREN, null);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categories' => array(self::HAS_MANY, 'Categories', 'id_thematic')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id' => 'ID',
			'title' => 'Название',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search(){
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;


		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			)
		));
	}
}