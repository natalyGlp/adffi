<?php
/**
 * This is the model class for table "sources".
 *
 * The followings are the available columns in table 'sources':
 * @property integer $id
 * @property integer $id_thematic
 * @property string $description
 * @property integer $active
 */
class Sources extends CActiveRecord{
	public $categories = array();
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sources the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'sources';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		return array(
			array('id_thematic, url', 'required'),
			array('id_thematic, active', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_thematic, description, active', 'safe', 'on'=>'search'),
		);
	}


	public function beforeSave(){
		if($this->isNewRecord){
			$this->id_user = Yii::app()->user->id;
		}

		return parent::beforeSave();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		return array(
			'thematic' => array(self::BELONGS_TO, 'Thematics', 'id_thematic'),
			'feeds' => array(self::HAS_MANY, 'Feeds', 'id_source')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id' => 'ID',
			'url' => 'URL',
			'categories' => 'Категории',
			'id_thematic' => 'Тематика',
			'description' => 'Описание',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search(){
		$criteria = new CDbCriteria;



		if(!Yii::app()->user->isAdmin && !Yii::app()->user->checkAccess('redactor')){
			$this->categories = array();
			$user = User::model()->findByPk(Yii::app()->user->id);
			foreach($user->categories as $cat){
				$this->categories[$cat->id] = $cat->id;
			}
		}


		if(is_array($this->categories) && count($this->categories)){
			$ids = array();

			$_ids = Yii::app()->db
							  ->createCommand()
							  ->select('id_source')
							  ->from('category_sources')
							  ->where('id_category IN('.implode(',', $this->categories).')')
							  ->queryAll(false);

			foreach($_ids as $id){
				$ids[$id[0]] = $id[0];
			}
			$criteria->addInCondition('id', $ids);
		}


		$criteria->compare('id_thematic','='.$this->id_thematic);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			)
		));
	}

	public function afterSave(){
		$db = Yii::app()->db;
		$db->createCommand()->delete('category_sources', 'id_source=?', array($this->id));
		foreach($this->categories as $cat){
			$db->createCommand()->insert('category_sources', array(
				'id_source' => $this->id,
				'id_category' => $cat
			));
		}
		return parent::afterSave();
	}
}