<?php
class User extends CActiveRecord{
	const REDACTOR = 'Redactor';
	const MODERATOR = 'Moderator';
	const ADMIN = 'Admin';
	
	public $roles = array('Redactor' => 'Редактор', 'Moderator' => 'Модератор', 'Admin' => 'Админ');
	public $roles2 = array('Redactor' => 'Редактор', 'Moderator' => 'Модератор');
	public $_thematics = array();
	public $_sites = array();
	public $_categories = array();

	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $profile
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return '{{user}}';
	}

	public function getRole(){
		return Yii::app()->db->createCommand()->select('itemname')->from('AuthAssignment')->where('userid=?', array($this->id))->queryScalar();
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email', 'required'),
			array('username, password, email', 'length', 'max'=>128),
			array('_thematics, _categories, _sites', 'safe'),
			array('_sites', 'arrayValidator'),
		);
	}

	public function arrayValidator($attribute, $params)
	{
		if(!is_array($this->{$attribute})) {
			$this->addError($attribute, 'Поле {attribute} должно быть массивом', array(
				'{attribute}' => $this->getAttributeLabel($attribute),
				));
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'posts' => array(self::HAS_MANY, 'Post', 'author_id'),
			'sites' => array(self::MANY_MANY, 'Sites', 'user_sites(id_user, id_site)'),
			'categories' => array(self::MANY_MANY, 'Categories', 'user_category(id_user, id_category)'),
			'thematics' => array(self::MANY_MANY, 'Thematics', 'user_thematics(id_user, id_thematic)')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id' => 'Id',
			'username' => 'Логин',
			'password' => 'Пароль',
			'email' => 'Email',
			'role' => 'Роль',
		);
	}

	protected function afterSave()
	{
		$db = Yii::app()->db;
		$db->createCommand()->delete('user_sites', 'id_user=?', array($this->primaryKey));
		$db->createCommand()->delete('user_thematics', 'id_user=?', array($this->primaryKey));
		$db->createCommand()->delete('user_category', 'id_user=?', array($this->primaryKey));

		if(is_array($this->_thematics))
			foreach($this->_thematics as $tid)
			{
				$db->createCommand()->insert('user_thematics', array(
					'id_user' => $this->primaryKey,
					'id_thematic' => $tid
				));
			}

		if(is_array($this->_categories))
			foreach($this->_categories as $cid)
			{
				$db->createCommand()->insert('user_category', array(
					'id_user' => $this->primaryKey,
					'id_category' => $cid
				));
			}

		foreach($this->_sites as $sid){
			$db->createCommand()->insert('user_sites', array(
				'id_user' => $this->primaryKey,
				'id_site' => $sid
			));
		}

		if(isset($_POST['User']['role'])){
			if($db->createCommand()->select('COUNT(*)')->from('AuthAssignment')->where('userid=?', array($this->id))->queryScalar()){
				$db->createCommand()->update('AuthAssignment', array('itemname' => $_POST['User']['role']), 'userid=:uid', array(':uid' => $this->primaryKey));
			}else{
				$db->createCommand()->insert('AuthAssignment', array('itemname' => $_POST['User']['role'], 'userid' => $this->primaryKey));
			}
		}

		return parent::afterSave();
	}

	protected function afterFind()
	{
		$db = Yii::app()->db;
		$thematics = $db->createCommand()->select('id_thematic')->from('user_thematics')->where('id_user=?', array($this->primaryKey))->queryAll();
		$this->_thematics = CHtml::listData($thematics, 'id_thematic', 'id_thematic');

		$categories = $db->createCommand()->select('id_category')->from('user_category')->where('id_user=?', array($this->primaryKey))->queryAll();
		$this->_categories = CHtml::listData($categories, 'id_category', 'id_category');

		return parent::afterFind();
	}

	protected function beforeDelete()
	{
		if($this->username == 'admin') {
			throw new CHttpException('Нельзя удалять пользователя admin');
		}

		return parent::beforeDelete();
	}

	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password){
		return $password == $this->password;
		return crypt($password,$this->password)===$this->password;
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @return string hash
	 */
	public function hashPassword($password){
		// return crypt($password, $this->generateSalt());
	}

	/**
	 * Generates a salt that can be used to generate a password hash.
	 *
	 * The {@link http://php.net/manual/en/function.crypt.php PHP `crypt()` built-in function}
	 * requires, for the Blowfish hash algorithm, a salt string in a specific format:
	 *  - "$2a$"
	 *  - a two digit cost parameter
	 *  - "$"
	 *  - 22 characters from the alphabet "./0-9A-Za-z".
	 *
	 * @param int cost parameter for Blowfish hash algorithm
	 * @return string the salt
	 */
	protected function generateSalt($cost=10){
		if(!is_numeric($cost)||$cost<4||$cost>31){
			throw new CException(Yii::t('Cost parameter must be between 4 and 31.'));
		}
		// Get some pseudo-random data from mt_rand().
		$rand='';
		for($i=0;$i<8;++$i)
			$rand.=pack('S',mt_rand(0,0xffff));
		// Add the microtime for a little more entropy.
		$rand.=microtime();
		// Mix the bits cryptographically.
		$rand=sha1($rand,true);
		// Form the prefix that specifies hash algorithm type and cost parameter.
		$salt='$2a$'.str_pad((int)$cost,2,'0',STR_PAD_RIGHT).'$';
		// Append the random salt string in the required base64 format.
		$salt.=strtr(substr(base64_encode($rand),0,22),array('+'=>'.'));
		return $salt;
	}

	/**
	 * Отправка email
	 *
	 * @param string $subject - тема письма
	 * @param string|array $message - тело письма, если array, то первый элемент - вьюха, остальные - ее параметры
	 */
	public function sendEmail($subject = 'No Subject', $message = '')
	{
		$m = new YiiMailMessage;
		$m->addTo($this->email);
		$m->from = Yii::app()->params['adminEmail'];

		if(is_array($message)) // Используется вьюха
		{
			$m->view = reset($message);
			$message = array_slice($message, 1);
		}
		$m->subject = strip_tags($subject);
		$m->setBody($message, 'text/html');

		Yii::app()->mail->send($m);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search(){
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;

		if(!Yii::app()->user->isAdmin){
			$ids = array();
			$_ids = Yii::app()->db->createCommand()->select('userid')->from('AuthAssignment')->where('itemname IN("'.self::REDACTOR.'", "'.self::MODERATOR.'")')->queryAll(false);
			foreach($_ids as $id){
				$ids[$id[0]] = $id[0];
			}
			$criteria->addInCondition('t.id', $ids);
		}

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.username',$this->username, true);
		$criteria->compare('t.email',$this->email, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			)
		));
	}

	/**
	 * @access public
	 * @return string полное имя и фималилию пользвателя
	 */
	public function getFullName()
	{
		return $this->username;
	}

	public function getSites()
	{

		//$sites = $this->_sites;
		$sites = array();
		$ids = Yii::app()->db->createCommand()->select('id_site')->from('user_sites')->where('id_user ="'.$this->id.'"')->queryAll(true);
		foreach ($ids as $key => $value) {
			$model = Sites::model()->findByPk($value['id_site']);
			if(isset($model->url)){
				$site = trim(str_replace('http:', '', $model->url), '/');
				array_push($sites, $site);
			}
		}
		return $sites;
	}
}