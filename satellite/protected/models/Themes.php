<?php

/**
 * This is the model class for table "themes".
 *
 * The followings are the available columns in table 'themes':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $layouts
 * @property integer $preview
 * @property integer $themeId
 * @property integer $active
 */
class Themes extends CActiveRecord
{
	public $status = array('Не активна', 'Активна');
	public $arr_layouts = array(
		'left' => 'С левой колонкой',
		'right' => 'С правой колонкой',
		'both' => 'С левой и правой колонкой',
		'none' => 'Без колонок'
	);
	public $_thematics = array();
	public $_categories = array();

	public $zip;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Themes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getLayouts_types(){
		$arr = explode(',', $this->layouts);
		$text = ''; 
		foreach($arr as $v){
			$text .= $this->arr_layouts[$v]."<br/>";
		}
		return $text;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{themes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('themeId, title, description, layouts', 'required'),
			array('zip', 'required', 'on' => 'create'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('title, layouts', 'length', 'max'=>255),
            array('zip', 'file', 'types'=>'zip', 'allowEmpty' => true),
			array('_thematics, _categories', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, description, layouts, active', 'safe', 'on'=>'search'),
		);
	}

	public function getImage()
	{
		$themeInstallPath = Yii::getPathOfAlias('themes.' . $this->themeId . '.install');
		if (file_exists($themeInstallPath)) {
			$dir = dirname($this->preview);
			$assets = Yii::app()->assetManager->publish($themeInstallPath.'/assets', false, -1, YII_DEBUG);

			return $assets.'/images/'.$this->preview;
		}

		return null;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'th_thematics' => array(self::HAS_MANY, 'ThemesThematic', 'id_theme'),
			'th_categories' => array(self::HAS_MANY, 'ThemesCategory', 'id_theme'),
			'thematics' => array(self::HAS_MANY, 'Thematics', array('id_thematic'=>'id'), 'through'=>'th_thematics'),
			'categories' => array(self::HAS_MANY, 'Categories', array('id_category'=>'id'), 'through'=>'th_categories'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'description' => 'Описание',
			'layouts' => 'Типы макетов',
			'active' => 'Статус',
			'zip' => 'ZIP пакет темы',
			'preview' => 'Скриншот'
		);
	}

	public function afterFind(){
		$db = Yii::app()->db;
		$thematics = $db->createCommand()->select('id_thematic')->from('themes_thematic')->where('id_theme=?', array($this->primaryKey))->queryAll();
		$this->_thematics = CHtml::listData($thematics, 'id_thematic', 'id_thematic');

		$categories = $db->createCommand()->select('id_category')->from('themes_category')->where('id_theme=?', array($this->primaryKey))->queryAll();
		$this->_categories = CHtml::listData($categories, 'id_category', 'id_category');

		return parent::afterFind();
	}

	public function afterSave()
	{
		$db = Yii::app()->db;
		$db->createCommand()->delete('themes_thematic', 'id_theme=?', array($this->primaryKey));
		$db->createCommand()->delete('themes_category', 'id_theme=?', array($this->primaryKey));

		if(is_array($this->_thematics))
			foreach($this->_thematics as $tid)
			{
				$db->createCommand()->insert('themes_thematic', array(
					'id_theme' => $this->primaryKey,
					'id_thematic' => $tid
				));
			}

		if(is_array($this->_categories))
			foreach($this->_categories as $cid)
			{
				$db->createCommand()->insert('themes_category', array(
					'id_theme' => $this->primaryKey,
					'id_category' => $cid
				));
			}

		return parent::afterSave();
	}

	/**
	 * @return NBaseThemeInstaller установщик темы сайта
	 */
	public static function getInstaller($theme, $site = null)
	{
		if($theme instanceof Sites)
		{
			$site = $theme;
			$theme = $site->theme;
		}
		
		$className = ucfirst(preg_replace('/[\-]/', '', $theme)).'ThemeInstaller';
		Yii::import('themes.'.$theme.'.install.'.$className);
		$installer = Yii::createComponent(array(
			'class' => $className,
			'site' => $site,
			));
		return $installer;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search(){
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		if(!empty($this->layouts)){
			$criteria->condition = new CDbExpression('FIND_IN_SET( :layouts, layouts )');
			$criteria->params[':layouts'] = $this->layouts;
		}

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50,
			)
		));
	}
}