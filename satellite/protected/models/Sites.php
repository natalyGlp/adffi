<?php
/**
 * This is the model class for table "sites".
 *
 * The followings are the available columns in table 'sites':
 * @property integer $id
 * @property integer $id_thematic
 * @property string $title
 * @property string $url
 * @property string $db_server
 * @property string $db_user
 * @property string $db_password
 * @property string $db_name
 * @property string $db_prefix префикс ТАБЛИЦ бд
 * @property integer $status
 * @property integer $sandboxed
 * @property integer $debug
 * @property integer $type
 * @property string $theme
 * @property string $layout
 * @property string $color
 * @property string $config
 */
class Sites extends CActiveRecord
{
    /**
     * @var фильтрация сайтов по категориям / привязка сайта к категориям
     */
    public $_categories = null;
    public $thematics = array();
    public $sites = array();

    public $modules = array();
    public $widgets = array();

    /**
     * @var string Фильтр по тематикам и категориям
     */
    public $tcFilter;

    /**
     * @var string $_oldUrl старый путь к файлу конфига бд
     */
    protected $_oldUrl;

    /**
     * @var string $_oldUrl старая тема сайта
     */
    protected $_oldTheme;

    /**
     * @var CDbConnection подключение к бд сайта
     */
    private $_siteDb;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sites';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_thematic, title, url, db_server, db_user, db_name', 'required'),
            array('id_thematic, status, debug, sandboxed, id_thematic', 'numerical', 'integerOnly' => true),
            array('url', 'unique'),
            array('url', 'url', 'defaultScheme' => 'http'),
            array('_categories, modules, widgets, color, type, theme, layout, api_key', 'safe'),
            array('title, url, db_server, db_user, db_password, db_name, api_key', 'length', 'max' => 255),
            array('db_prefix', 'default', 'value' => 'gxc_'),
            array('db_prefix', 'freezeValue', 'initial' => $this->db_prefix),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, id_thematic, _categories, tcFilter, title, url, api_key', 'safe', 'on' => 'search'),
        );
    }

    public function freezeValue($attribute, $params)
    {
        if (!empty($params['initial'])) {
            $this->$attribute = $params['initial'];
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'thematic' => array(self::BELONGS_TO, 'Thematics', 'id_thematic'),
            'themeModel' => array(self::BELONGS_TO, 'Themes', array('theme' => 'themeId')),
            'th_categories' => array(self::HAS_MANY, 'SitesCategory', 'id_site'),
            'categories' => array(self::HAS_MANY, 'Categories', array('id_category' => 'id'), 'through' => 'th_categories'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_thematic' => 'Тематика',
            'tcFilter' => 'Тематика',
            'title' => 'Название/описание',
            'url' => 'URL',
            'categories' => 'Категории',
            '_categories' => 'Категории',
            'db_server' => 'Сервер БД',
            'db_user' => 'Пользователь БД',
            'db_password' => 'Пароль БД',
            'db_name' => 'Имя БД',
            'db_prefix' => 'Префикс таблиц',
            'config' => 'Настройки',
            'sandboxed' => 'Доступен в режиме песочницы',
            'status' => 'Доступен в режиме продакшен',
            'debug' => 'Дебаг режим',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Sites the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getLayouts($theme)
    {
        $arr = array();
        $text = '';
        $themePath = Yii::getPathOfAlias('themes.' . $theme);
        if ($handle = opendir($themePath)) {
            while (false !== ($file = readdir($handle))) {
                if (preg_match('/(.php)$/i', $file)) {
                    $arr[$file] = $file;
                }
            }

            closedir($handle);
        }

        return $text;
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            // проверяем правильные ли данные для бд
            $valid = @mysql_connect($this->db_server, $this->db_user, $this->db_password);
            if (!$valid) {
                $this->addError('db_server', 'Ошибка при подключении к базе данных');
                return false;
            }
        }
        return parent::beforeValidate();
    }

    public function beforeSave()
    {
        $this->config = CJSON::encode(array(
            'modules' => $this->modules,
            'widgets' => $this->widgets,
        ));

        // Обновим файловую структуру сайта
        $this->manageSiteFS();

        // настройка бд
        $this->setupDb();

        return parent::beforeSave();
    }

    /**
     * Производит создание/обновление структуры сайта
     */
    public function manageSiteFS()
    {
        // переименуем/создадим директорию под сайт
        if (!empty($this->url) && !empty($this->_oldUrl)
             && $this->url != $this->_oldUrl) {
            @rename($this->getSiteDir($this->_oldUrl), $this->getSiteDir($this->url));
        }

        // скопируем заготовку сайта
        // проверку на существование $this->siteDir не делаем для того,
        // что бы поддерживать директорию в актуальном состоянии и что бы быть уверенным,
        // что это не просто пустая директория с именем сайта
        $siteSamplePath = Yii::getPathOfAlias('application.data.siteSample');
        $this->copyDirR($siteSamplePath, $this->siteDir);

        // Права для загрузок, ассетов и рантайма
        $this->recursiveChmod($this->siteDir . '/assets', 0777, 0777);
        $this->recursiveChmod($this->siteDir . '/uploads', 0777, 0777);
        $this->recursiveChmod($this->siteDir . '/protected/runtime', 0777, 0777);

        // обновляем файл конфигурации бд
        $content = "<?php return " . $this->getDbConfigArray(true) . "; ?>";
        $file = $this->dbConfigPath;
        file_put_contents($file, $content);
        @chmod($file, 0777);

        // Обработка статуса сайта
        $this->processSiteStatus();
    }

    /**
     * Обрабатывает debug, production, sandbox статусы сайта
     */
    public function processSiteStatus()
    {
        // директория с шаблонами структуры фс для новых сайтов
        $dataDir = Yii::getPathOfAlias('application.data');

        $htaccessPath = $this->siteDir . '/.htaccess';
        if ($this->status == 1) {
            copy($dataDir . '/online.htaccess', $htaccessPath);
        } else {
            copy($dataDir . '/offline.htaccess', $htaccessPath);
        }

        // создаем index.php
        $indexFileStr = file_get_contents($dataDir . '/index.tpl');
        $indexFileStr = strtr($indexFileStr, array(
            '{{YII_DEBUG}}' => $this->debug ? 'true' : 'false',
            '{{CORE_PATH}}' => "'" . realpath(CORE_FOLDER) . "'",
            '{{PROJECT_PATH}}' => "'" . realpath(CMS_PATH) . "'",
        ));
        $indexFilePath = $this->siteDir . '/index.php';
        file_put_contents($indexFilePath, $indexFileStr);

        if (Yii::app()->params['sandboxDir'] && file_exists($this->getSiteDir(false, true))) {
            // удаляем сайт из песочницы
            unlink($this->getSiteDir(false, true));
        }

        if ($this->sandboxed) {
            // Добавляем сайт в песочницу
            symlink($this->getSiteDir(), $this->getSiteDir(false, true));
        }
    }

    /**
     * Производит инициализацию базы данных и генерирует конфиги
     */
    public function setupDb()
    {
        try {
            $db = new PDO("mysql:host=" . $this->db_server, $this->db_user, $this->db_password, array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            ));
        } catch (Exception $e) {
            throw new CException('Ошибка подключения к базе данных: ' . $e->getMessage());
        }
        $doInitDb = false;

        if ($db->exec('CREATE DATABASE `' . $this->db_name . '` CHARACTER SET utf8 COLLATE utf8_general_ci;')) {
            $doInitDb = true;
        } else {
            $db->exec("USE `{$this->db_name}`");
            // Проверим не пустая ли бд. если пуста - восстановление дампа
            $result = $db->query("SHOW TABLES LIKE '" . $this->db_prefix . "page'");
            if ($result->fetch(PDO::FETCH_NUM) > 0) {
                $doInitDb = true;
            }
        }

        if ($doInitDb) {
            // восстанавливаем основную структуру
            $this->runSqlFile(Yii::getPathOfAlias('cms.data.cms') . '.sql');
        } else {
            // База данных уже существует
            $error = $db->errorInfo();
            $error = count($error) == 3 ? ': [' . $error[0] . ']' . $error[2] : '';
            Yii::app()->user->setFlash('warning', '<b>База данных не была созданна</b>, так как она уже существует' . $error);
        }

        $db->exec("USE `{$this->db_name}`");

        // устанавливаем настраиваем тему
        if ($this->theme && $this->layout) {
            $installer = Themes::getInstaller($this);
            if ($this->_oldTheme != $this->theme) {
                if ($this->_oldTheme) {
                    $oldThemeInstaller = Themes::getInstaller($this->_oldTheme, $this);
                    if (!$oldThemeInstaller->uninstall()) {
                        throw new CException("Ошибка при удалении темы " . $this->_oldTheme);
                    }
                }

                if ($installer->install()) {
                    Yii::app()->user->setFlash('success', 'Тема <b>' . $this->theme . '</b> была успешно установленна');
                } else {
                    throw new CException("Ошибка при установке темы " . $this->_oldTheme);
                }
            }

            if ($installer->configure()) {
                // установка цвета, лейаута
                Yii::app()->user->setFlash('success', 'Тема <b>' . $this->theme . '</b> была успешно настроена');
            } else {
                throw new CException("Ошибка при настройке темы " . $this->_oldTheme);
            }
        }

        // утанавливаем url главной стр, имя сайта и емейл админа
        // TODO: setCmsSetting не должен быть зависим от бд. нужно вынести бд в отдельный метод
        $email = User::model()->findByPk(Yii::app()->user->id)->email;
        $this->setCmsSetting('site_url', 'http://' . $this->cleanUrl . '/', 'general');
        $this->setCmsSetting('site_name', $this->title, 'general');
        $this->setCmsSetting('support_email', $email, 'system', false);// DEPRECATED
        $this->setCmsSetting('support_email', $email, 'general', false);

        unset($db);
    }

    /**
     * @return CDbConnection подключение к бд текущего сайта
     */
    public function getSiteDbConnection()
    {
        if (!$this->hasDbConfig) {
            return null;
        }

        if (!$this->_siteDb) {
            $this->_siteDb = Yii::createComponent($this->getDbConfigArray());
        }

        return $this->_siteDb;
    }

    /**
     * Устанавливает настройку в таблицу {{settings}} текущего сайта
     * @param string $key
     * @param mixed $value
     * @param string $category
     */
    public function setCmsSetting($key, $value, $category = 'system', $allowOverwrite = true)
    {
        $db = $this->siteDbConnection;

        $data = array(
            "value" => serialize($value),
            "key" => $key,
            "category" => $category,
        );

        $hasSetting = $db->createCommand()
            ->select('key')
            ->from('{{settings}}')
            ->where('`category`=:category AND `key`=:key AND `value`=:value', array(
                ':category' => $data['category'],
                ':key' => $data['key'],
                ':value' => $data['value'],
            ))->queryScalar();

        if ($hasSetting) {
            if ($allowOverwrite) {
                $db->createCommand()->update('{{settings}}', $data, '`key`=:key AND `category`=:category', array(
                    ':category' => $data['category'],
                    ':key' => $data['key'],
                ));
            }
        } else {
            $db->createCommand()->insert('{{settings}}', $data);
        }
    }

    /**
     * @param boolean $forFs если true, функция проведет дополнительную замену /->_ (site.com/nespi -> site.com_nespi) так, что бы возвращаемая строка была совместима с файловой системой
     * @param string $url url сайта для которого нужно сгенерировать путь. Если false, то будет использовано соответствующее поле модели
     * @return string без протокола и слешей на концах строки
     */
    public function getCleanUrl($forFs = false, $url = false)
    {
        $url = $url ? $url : $this->url;
        return self::clearUrl($url, $forFs);
    }

    public static function clearUrl($url, $forFs = false)
    {
        $url = trim(str_replace('http://', '', $url), '/');
        if ($forFs) {
            str_replace('/', '_', $url);
        }

        return $url;
    }

    /**
     * Очищает кэш сайта
     */
    public function clearCache()
    {
        if ($this->isNewRecord) {
            return;
        }

        $cachePath = $this->siteDir . '/protected/runtime/cache';
        recursive_remove_directory($cachePath, true);
    }

    /**
     * Очищает assets сайта
     */
    public function clearAssets()
    {
        if ($this->isNewRecord) {
            return;
        }

        $cachePath = $this->siteDir . '/assets';
        recursive_remove_directory($cachePath, true);
    }

    public function afterFind()
    {
        $config = CJSON::decode($this->config);
        $this->widgets = isset($config['widgets']) ? $config['widgets'] : array();
        $this->modules = isset($config['modules']) ? $config['modules'] : array();

        $this->_oldUrl = $this->url;
        $this->_oldTheme = $this->theme;
        return parent::afterFind();
    }

    /**
     * Полностью удаляет сайт, как с диска, так и его базу данных
     */
    public function deleteSiteData()
    {
        $db = mysql_connect($this->db_server, $this->db_user, $this->db_password) or die("Could not connect: " . mysql_error());
        $result = mysql_query('DROP DATABASE IF EXISTS ' . $this->db_name . ';', $db);
        mysql_close($db);

        // Используем функцию из globals.php
        return $result && recursive_remove_directory($this->siteDir);
    }

    public function getThemeMetadataPath()
    {
        return !empty($this->theme) ?
        Yii::getPathOfAlias('themes.' . $this->theme . '.install')
        : null;
    }

    /**
     * @return string путь к файлу в котором находится конфиг бд
     */
    public function getDBConfigPath()
    {
        return $this->siteDir . '/protected/config/db.php';
    }

    /**
     * Подключает базу данных редактируемого сайта
     */
    public static function prepareForRequestIfSiteEdited()
    {
        if (isset($_GET['site']) && !empty($_GET['site']) && isset(Yii::app()->params['sitesDir'])) {
            $site = $_GET['site'];
            $dbConfig = Sites::findDbConfigByHost($site);

            if (!$dbConfig) {
                throw new CHttpException(404, 'Сайт ' . $site . ' не существует');
            }

            $db = Yii::createComponent($dbConfig);
            Yii::app()->setComponent(CONNECTION_NAME, $db);

            $siteDir = self::getSiteDirByHost($site);
            Yii::app()->runtimePath = $siteDir . '/protected/runtime';
            Yii::app()->cache->cachePath = $siteDir . '/protected/runtime/cache';
            Yii::setPathOfAlias('webroot', $siteDir);
            Yii::setPathOfAlias('application', $siteDir . '/protected');
            Yii::setPathOfAlias('uploads', $siteDir . '/uploads');
        }
    }

    /**
     * @return array массив настройки бд для сайта подключенного к сателлитке по его имени хоста
     * @todo вообще-то этот метод вполне мог бы работать на основе инфы из бд
     */
    public static function findDbConfigByHost($host)
    {
        $siteDir = self::getSiteDirByHost($host);

        $curSiteDbConfigPath = $siteDir . '/protected/config/db.php';

        if (!file_exists($curSiteDbConfigPath)) {
            return null;
        }

        return require_once ($curSiteDbConfigPath);
    }

    /**
     * @param string $url url сайта для которого нужно сгенерировать путь. Если false, то будет использовано соответствующее поле модели
     * @param string $sandbox если true, будет возвращен путь к песочнице
     * @return string путь к сайту с которым свяханна текущая модель.
     * @note директория (sitesDir), в которой находятся сайты может быть настроена в  файле application.config.params
     */
    public function getSiteDir($url = false, $sandbox = false)
    {
        $url = $url ? $url : $this->url;
        return self::getSiteDirByHost($url, $sandbox);
    }

    public static function getSiteDirByHost($url = false, $sandbox = false)
    {
        $sitesPath = $sandbox ? Yii::app()->params['sandboxDir'] : Yii::app()->params['sitesDir'];
        if (strlen($sitesPath) > 0 && $sitesPath[0] != '/' || empty($sitesPath)) {
            // относительный путь
            $sitesPath = realpath(Yii::getPathOfAlias('webroot') . '/' . $sitesPath);
        }

        if (!$sitesPath || !is_dir($sitesPath)) {
            if ($sandbox) {
                throw new CException('Параметр приложения "sandboxDir" (' . Yii::app()->params['sandboxDir'] . ') указывает на директорию, которой не существует');
            } else {
                throw new CException('Параметр приложения "sitesDir" (' . Yii::app()->params['sitesDir'] . ') указывает на директорию, которой не существует');
            }
        }

        $url = self::clearUrl($url, true);

        // конвертим domain.com.ua -> domain для песочницы
        // TODO: возможно есть смысл вынести параметры конвертации названия сайта для песочницы в конфиг
        if ($sandbox) {
            $path = preg_replace('/\..*/', '', $url);
        } else {
            if (!isset(Yii::app()->params['sitePath'])) {
                Yii::app()          ->params['sitePath'] = '{host}';
            }

            $path = strtr(Yii::app()->params['sitePath'], array(
                '{host}' => $url,
            ));
        }

        return $sitesPath . '/' . $path;
    }

    /**
     * Ищет модель сайта по его Url
     * @param  string $url url сайта
     * @return null|Sites
     */
    public function findByUrl($url)
    {
        $url = $this->getCleanUrl(false, $url);

        return $this->find(array(
            'condition' => 'url LIKE :url',
            'params' => array(
                ':url' => "%$url%",
            ),
        ));
    }

    /**
     * @param boolean $asString если true, то метод вернет строку настроек бд для того, что бы ее можно было сохранить в файле конфига бд
     * @return array массив с настройками бд для текущего сайта
     */
    public function getDbConfigArray($asString = false)
    {
        if (!$this->hasDbConfig) {
            return null;
        }

        $config = array(
            'connectionString' => 'mysql:host=' . $this->db_server . ';dbname=' . $this->db_name,
            'enableParamLogging' => $this->debug > 0,
            'enableProfiling' => $this->debug > 0,
            'emulatePrepare' => true,
            'username' => $this->db_user,
            'password' => $this->db_password,
            'charset' => 'utf8',
            'tablePrefix' => $this->db_prefix,
            'class' => 'CDbConnection',
        );

        if ($asString) {
            $config = var_export($config, true);
        }

        return $config;
    }

    protected function getHasDbConfig()
    {
        return isset($this->db_server) && isset($this->db_name) && isset($this->db_user) && isset($this->db_password);
    }

    public function afterSave()
    {
        if ($this->_categories) {
            $this->updateCategories($this->_categories);
        }

        return parent::afterSave();
    }

    /**
     * Обновляет связь с категориями
     */
    public function updateCategories($categories = array())
    {
        $db = Yii::app()->db;
        $db->createCommand()->delete('category_sites', 'id_site=?', array($this->id));
        foreach ($categories as $cat) {
            $db->createCommand()->insert('category_sites', array(
                'id_site' => $this->id,
                'id_category' => $cat,
            ));
        }
    }

    /**
     * @return string строка с иерархией тематика -> категория текущего сайта
     */
    public function getCategoryString()
    {
        $string = isset($this->thematic->title) ? $this->thematic->title : '';

        if (!empty($string) && isset($this->categories[0]->title)) {
            $string .= ' → ';
            $string .= isset($this->categories[0]->title) ? $this->categories[0]->title : '';
        }

        return $string;
    }

    /**
     * [getUserSites description]
     * @return [type] [description]
     */
    public static function getUserSites()
    {
        $criteria = new CDbCriteria();

        if (!Yii::app()->user->isAdmin) {
            $criteria->compare('{{user_sites}}.id_user', Yii::app()->user->id);
            $criteria->join = 'LEFT JOIN {{user_sites}} ON {{user_sites}}.id_site = t.id';
        }

        return Sites::model()->findAll($criteria);
    }

    public function runSqlFile($sqlFile, $success = 'База данных успешно создана')
    {
        if (file_exists($sqlFile)) {
            $sql = file_get_contents($sqlFile);
            if (strpos($sqlFile, 'gz')) {
                $sql = gzinflate(substr($sql, 10, -8));
            }

            // Производим замену префиксов таблиц
            $sql = str_replace('gxc_', $this->db_prefix, $sql);

            // запускаем sql комманды
            $db = new CDbConnection('mysql:host=' . $this->db_server . ';dbname=' . $this->db_name, $this->db_user, $this->db_password);
            $db->active = true;
            $command = $db->createCommand($sql);
            $rowCount = $command->execute();

            Yii::app()->user->setFlash('success', $success);
        }
    }

    /**
     * Рекурсивно копирует директорию
     * @param string $source путь к директории, которую нужно копировать
     * @param string $dest куда копировать
     * @param integer $filePerm права на файлы
     * @param integer $dirPerm права на директории
     */
    public function copyDirR($source, $dest, $filePerm = 0777, $dirPerm = 0777)
    {
        if (is_file($dest) && !is_dir($dest)) {
            throw new Exception('Путь $dest (' . $dest . ') не должен указывать на файл.');
        }

        if (!file_exists($dest)) {
            mkdir($dest, 0777, true);
        }

        foreach ($iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST
        ) as $item) {
            $destItemPath = $dest . '/' . $iterator->getSubPathName();
            if ($item->isDir()) {
                if (!is_dir($destItemPath)) {
                    mkdir($destItemPath);
                }

                @chmod($destItemPath, $dirPerm);
            } else {
                copy($item, $destItemPath);
                @chmod($destItemPath, $filePerm);
            }
        }
    }

    // УКРАДЕНО ИЗ ThemesController
    // TODO: нужно вынести эту функцию куда-нибудь в общедоступное место
    public function recursiveChmod($path, $filePerm = 0777, $dirPerm = 0777)
    {
        // Check if the path exists
        if (!file_exists($path)) {
            return (false);
        }

        // See whether this is a file
        if (is_file($path)) {
            // Chmod the file with our given filepermissions
            @chmod($path, $filePerm);

            // If this is a directory...
        } elseif (is_dir($path)) {
            // Then get an array of the contents
            $entries = scandir($path);

            // Remove "." and ".." from the list
            $ignore = array('..', '.');

            // Parse every result...
            foreach ($entries as $entry) {
                // And call this function again recursively, with the same permissions
                if (in_array($entry, $ignore)) {
                    continue;
                }

                $this->recursiveChmod($path . "/" . $entry, $filePerm, $dirPerm);
            }
            // When we are done with the contents of the directory, we chmod the directory itself
            @chmod($path, $dirPerm);
        }

        // Everything seemed to work out well, return true
        return (true);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $ids = array();

        if (!Yii::app()->user->isAdmin && !Yii::app()->user->checkAccess('redactor')) {
            $this->_categories = array();
            $user = User::model()->findByPk(Yii::app()->user->id);
            foreach ($user->categories as $cat) {
                $this->_categories[$cat->id] = $cat->id;
            }

            foreach ($user->sites as $cat) {
                $this->sites[$cat->id] = $cat->id;
            }
        }

        // TODO: проверить не удастся ли юзеру подделать запрос и увидеть тематику, на которую у него нету прав
        if (!empty($this->tcFilter)) {
            $tcId = substr($this->tcFilter, 1);
            if ($this->tcFilter[0] == 't') {
                $this->id_thematic = $tcId;
            } else {
                if (!is_array($this->_categories)) {
                    $this->_categories = array();
                }

                array_push($this->_categories, $tcId);
            }
        }

        if (count($this->_categories)) {
            $_ids = Yii::app()->db
                ->createCommand()
                ->select('id_site')
                ->from('category_sites')
                ->where('id_category IN(' . implode(',', $this->_categories) . ')')
                ->queryColumn();

            $ids = CMap::mergeArray($ids, $_ids);
        }

        if (count($this->sites)) {
            foreach ($this->sites as $id) {
                $ids[] = $id;
            }
        }

        if (count($ids)) {
            $criteria->addInCondition('id', $ids);
        } elseif (!Yii::app()->user->isAdmin) {
            // пользователю нельзя админить
            $criteria->compare('id', 0);
        }

        $criteria->compare('id_thematic', '=' . $this->id_thematic);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('url', $this->url, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 50),
        ));
    }
}
