<?php

/**
 * This is the model class for table "feeds".
 *
 * The followings are the available columns in table 'feeds':
 * @property integer $id
 * @property integer $id_source
 * @property string $title
 * @property string $link
 * @property string $description
 * @property string $date
 * @property integer $readed
 */
class Feeds extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Feeds the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'feeds';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_source, title, link, description, date', 'required'),
			array('id_source, readed', 'numerical', 'integerOnly'=>true),
			array('title, link, date', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please id_source those attributes that should not be searched.
			array('id, id_source, title, link, description, date, readed', 'safe', 'on'=>'search'),
		);
	}

	public function getReadedLink(){
		return Yii::app()->createAbsoluteUrl('/sources/readlink', array('id' => $this->id, 'url' => $this->link));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'source' => array(self::BELONGS_TO, 'Sources', 'id_source'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_source' => 'Id Site',
			'title' => 'Title',
			'link' => 'Link',
			'description' => 'Description',
			'date' => 'Pub Date',
			'readed' => 'Readerd',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_source',$this->id_source);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('date',$this->pubDate,true);
		$criteria->compare('readed',$this->readed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}