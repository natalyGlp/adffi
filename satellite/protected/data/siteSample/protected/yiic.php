<?php
$yiiConsole = true;
$_SERVER['HTTP_HOST'] = '';

// заберем константы с индексного файла
require_once(dirname(__FILE__).'/../index.php');

$config = BACK_END . '/config/console.php';
$config = require($config);
// используем одну и ту же директорию для backend и frontend
$config['runtimePath'] = dirname(__FILE__).'/runtime';
$config['components']['db'] = $config['components'][CONNECTION_NAME] = require(dirname(__FILE__).'/config/db.php');

$yiic = CORE_FOLDER . '/yii/framework/yiic.php';
require_once($yiic);
