<?php
// if(preg_match('/(jpg|gif|png|jpeg)$/i', ))
error_reporting(E_ALL);

// This hack need for fix yii bug with header
header('Content-Type: text/html; charset=UTF-8',true);

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', {{YII_DEBUG}});
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

//You need to specify the path to CORE FOLDER CORRECTLY
ini_set("display_errors", YII_DEBUG);

define('CORE_FOLDER', {{CORE_PATH}});
define('CMS_PATH', {{PROJECT_PATH}});

// change the following paths if necessary
$define = CMS_PATH.'/common/define.php';
require_once($define);

$yii=CORE_FOLDER.'/yii/framework/yii.php';
require_once($yii);

Yii::setPathOfAlias('uploads', dirname(__FILE__).'/uploads');

if(!isset($yiiConsole))
{
	$globals=COMMON_FOLDER.'/globals.php';
	$config = (isset($backend) ? BACK_END : FRONT_END).'/config/main.php';

	require_once($globals);
	require_once($define);

	$config = require($config);
	// используем одну и ту же директорию для backend и frontend
	$config['runtimePath'] = dirname(__FILE__).'/protected/runtime';
	$config['components'][CONNECTION_NAME] = require(dirname(__FILE__).'/protected/config/db.php');

	if(isset($backend)) {
		$config['components']['request']['scriptUrl'] = '/project/backend/index.php';
		$config['components']['assetManager']['basePath'] = dirname(__FILE__).'/assets';
		$config['components']['assetManager']['baseUrl'] = '/assets';
	}
	Yii::createWebApplication($config)->run();
}
