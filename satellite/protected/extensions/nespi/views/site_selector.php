<?php
/**
 * @var array() $sitesAvailable список сайтов доступных текущему юзеру
 * @var CActiveRecord $model модель к которой крепится наше поле
 * @var string $selectorId id селектора
 * @var string $span boostrap span class (например span8)
 */
?>

<?php echo CHtml::activeLabelEx($model, $this->attribute, array(
    'label' => 'Выберите сайты',
    )); ?>
<?php
    $selected = array();
    foreach ($selectedSites as $siteId) {
        $selected[$siteId] = array('selected' => true);
    }
    echo CHtml::activeDropDownList($model, $this->attribute.'[]', $sitesAvailable, array(
        'id' => $selectorId,
        'multiple' => 'multiple',
        'options' => $selected,
        ));
    echo CHtml::error($model, $this->attribute);
?>