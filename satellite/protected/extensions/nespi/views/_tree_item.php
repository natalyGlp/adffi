<span class="treeCheckbox">
	<?php 
	echo CHtml::activeCheckBox($model, $attribute, array(
		'data-type' => $type,
		'uncheckValue' => null,
		'value' => $data->primaryKey,
		)) 
		. ' ' . 
		CHtml::activeLabel($model, $attribute, array(
		'label' => $data->title
		))
	?>
</span>