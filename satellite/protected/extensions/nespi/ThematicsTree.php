<?php
/**
 * Виджет для отображения тематик и дочерних категорий в виде дерева.
 * Виджет прикрепляется к модели, которая поддерживает тематики и категории
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.extensions.nespi
 */

// TODO: перенести сюда dropdown версию выбора тематики и категории
// TODO: моджно сделать режим с radio для тематик, что бы сделать работу виджета похожую на dropdown режим
class ThematicsTree extends CWidget
{
	/**
	 * @var CActiveRecord $model модель, к которой необходимо привязать виджет
	 */
	public $model;

	public $htmlOptions = array();

	public function init()
	{
		if(!isset($this->model))
			throw new CException('Для работы виджета необходимо указать свойство ThematicsTree::model');

		$this->registerScripts();			
	}

	public function run()
	{
		$this->widget('CTreeView', array(
			'id' => $this->id,
			'data' => $this->treeData,
			'htmlOptions' => $this->htmlOptions,
		));
	}

	/**
	 * Готовит данные для виджета CTreeView
	 */
	protected function getTreeData($expanded = false)
	{
		$model = $this->model;
		$data = array();
		$thematics = Thematics::model()->findAll(array(
			'select' => array('id', 'title'),
			'condition' => 'active=1',
			'order' => 'title ASC'
		));

		$ids = array();
		$i = 0;
		// заглушка для случая, когда не выбрана ни одна тематика
		echo CHtml::activeHiddenField($this->model, '_thematics', array('value' => ''));
		foreach($thematics as $thema){
			$data[$thema->id]['text'] = $this->render('_tree_item', array(
				'attribute' => '_thematics['.$thema->id.']', 
				'type' => 'parent', 
				'data' => $thema,
				'model' => $this->model,
			), true);
			$ids[$thema->id] = $thema->id;
			$data[$thema->id]['expanded'] = $expanded || isset($model->_thematics[$thema->id]);
			$i++;
		}

		if(count($ids)){
			$categories = Categories::model()->findAll(array(
			'select' => array('id', 'title', 'id_thematic'),
				'condition' => 'active=1 AND id_thematic IN('.implode(',', $ids).')',
				'order' => 'title ASC'
			));

			$i = 0;
			// заглушка для случая, когда не выбрана ни одна категория
			echo CHtml::activeHiddenField($this->model, '_categories', array('value' => ''));
			foreach($categories as $cat){
				$data[$cat->id_thematic]['children'][$cat->id]['text'] = $this->render('_tree_item', array(
					'attribute' => '_categories['.$cat->id.']', 
					'type' => 'child', 
					'data' => $cat,
					'model' => $this->model,
				), true);

				if(isset($model->_categories[$cat->id]))
					$data[$cat->id_thematic]['expanded'] = true;

				$i++;
			}
		}

		return $data;
	}

	protected function registerScripts()
	{
		$cs = Yii::app()->clientScript;
		$cs->registerScript(__FILE__.'#'.$this->id, '
			$(function(){
				$("#'.$this->id.'").on("change", "input[type=checkbox]", function(){
					if($(this).data("type") == "parent")
					{
						var $li = $(this).parents("li").eq(0);
						$li.find("ul input").prop("checked", $(this).prop("checked"));
						if($(this).prop("checked") && $li.hasClass("expandable")) // Открываем узел
							$li.find("> .hitarea").click();
					}else{
						var $ul = $(this).parents("ul").eq(0);
						var isChecked = $ul.find("input").length == $ul.find("input:checked").length;
						$ul.parent("li").find("input[data-type=parent]").prop("checked", isChecked);
					}
				});
			});
		', CClientScript::POS_END);
	}
}