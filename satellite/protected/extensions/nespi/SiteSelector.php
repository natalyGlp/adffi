<?php
/**
 * Виджет для выбора сайтов, принадлежащих юзеру.
 *
 * Виджет прикрепляется к модели, которая поддерживает тематики и категории
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.extensions.nespi
 */

class SiteSelector extends CInputWidget
{
    public function init()
    {
        parent::init();

        $this->registerScripts();

    }

    public function run()
    {
        $sitesAvailable = Sites::getUserSites();

        $this->render('site_selector', array(
            'sitesAvailable' => CHtml::listData($sitesAvailable, 'primaryKey', 'url'),
            'selectedSites' => $this->model->{$this->attribute},
            'selectorId' => $this->id,
            'model' => $this->model,
        ));
    }

    protected function registerScripts()
    {
        GxcHelpers::registerCss('multi-select/css/multi-select.css');
        GxcHelpers::registerJs('multi-select/js/jquery.multi-select.js');
        GxcHelpers::registerJs('multi-select/js/jquery.quicksearch.js');
        Yii::app()->clientScript
                  ->registerScript(__FILE__ . '#' . $this->id, '
                $(\'#' . $this->id . '\').multiSelect({
                    selectableHeader: \'<div class="search-header"><input type="text" id="' . $this->id . '-search" autocomplete="off" placeholder="Search"></div>\',
                    selectionHeader: \'<div class="search-selected"></div>\'
                });

				$("#' . $this->id . '-search").quicksearch($(".ms-elem-selectable", "#ms-' . $this->id . '" )).on("keydown", function(e){
                    if (e.keyCode == 40){
                        $(this).trigger("focusout");
                        $("#ms-' . $this->id . '").focus();
                        return false;
                    }
                });
			')
        ;
    }
}
