<?php
/**
 * Создает таблицы для хранения связей темы сайта с тематиками сателлитки
 */
class m140214_173216_themes_thematics_and_cats extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('{{themes_thematic}}', array(
			'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'id_theme' => 'int(11) NOT NULL',
			'id_thematic' => 'int(11) NOT NULL',
			'KEY `id_theme` (`id_theme`,`id_thematic`)',
			),'engine InnoDB DEFAULT CHARSET=utf8');

		$this->createTable('{{themes_category}}', array(
			'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'id_theme' => 'int(11) NOT NULL',
			'id_category' => 'int(11) NOT NULL',
			'KEY `id_theme` (`id_theme`,`id_category`)',
			),'engine InnoDB DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropTable('{{themes_thematic}}');
		$this->dropTable('{{themes_category}}');
	}
}