<?php
/**
 * Добавляет поле "дебаг режим" в таблицу {{sites}}
 */
class m140207_154036_sites_debug_mode extends CDbMigration
{
	public function up()
	{
		$this->addColumn('{{sites}}', 'debug', 'TINYINT(1) UNSIGNED NOT NULL AFTER sandboxed');
	}

	public function down()
	{
		$this->dropColumn('{{sites}}', 'debug');
	}
}