<?php
/**
 * Добавляет поддержку типа темы, цвета, виджетов и модулей для сайтов сателлитки
 * Исправление названий некоторых полей Themes, удаление не нужных колонок
 * TODO: в будущем нужно лучше проработать хранение информации о доступных темах в бд
 *
 * Таблицы: {{sites}}, {{themes}}
 */
class m140220_161603_sites_layout_thema_color_etc extends CDbMigration
{
	public function up()
	{
		$this->addColumn('{{sites}}', 'type', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT "1" AFTER status');
		$this->addColumn('{{sites}}', 'color', 'VARCHAR(64) AFTER status');
		$this->addColumn('{{sites}}', 'layout', 'VARCHAR(64) AFTER status');
		$this->addColumn('{{sites}}', 'theme', 'VARCHAR(64) AFTER status');

		$this->addColumn('{{sites}}', 'db_prefix', 'VARCHAR(16) AFTER db_name');
		$this->update('{{sites}}', array(
			'db_prefix' => 'gxc_',
			));

		// переносим значения из config в layout/theme
		$db = $this->getDbConnection();
		$sites = $db->createCommand()->select('*')->from('{{sites}}')->queryAll();

		foreach ($sites as $site) 
		{
			$config = CJSON::decode($site['config']);
			$this->update('{{sites}}', array(
				'layout' => isset($config['layout']) ? $config['layout'] : '',
				'theme' => isset($config['themes']) ? $config['themes'] : '',
				'config' => CJSON::encode(array()),
				), 'id='.$site['id']);
		}


		$this->renameColumn('{{themes}}', 'path', 'themeId');
		$this->renameColumn('{{themes}}', 'previev', 'preview');
		$this->dropColumn('{{themes}}', 'logo_size');
	}

	public function down()
	{
		// переносим значения из config в layout/theme
		$db = $this->getDbConnection();
		$sites = $db->createCommand()->select('*')->from('{{sites}}')->queryAll();

		foreach ($sites as $site) 
		{
			$this->update('{{sites}}', array(
				'config' => CJSON::encode(array(
					'layout' => $site['layout'],
					'themes' => $site['theme'],
					)),
				), 'id='.$site['id']);
		}

		$this->dropColumn('{{sites}}', 'type');
		$this->dropColumn('{{sites}}', 'color');
		$this->dropColumn('{{sites}}', 'layout');
		$this->dropColumn('{{sites}}', 'theme');
		$this->dropColumn('{{sites}}', 'db_prefix');

		$this->renameColumn('{{themes}}', 'themeId', 'path');
		$this->renameColumn('{{themes}}', 'preview', 'previev');
		$this->addColumn('{{themes}}', 'logo_size', 'varchar(255) AFTER layouts');
		$this->update('{{themes}}', array('logo_size' => '200,100'));
	}
}