<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $model AuthItemForm */
/* @var $item CAuthItem */
/* @var $form TbActiveForm */

$this->breadcrumbs = array(
	$this->capitalize($this->getTypeText(true)) => array('index'),
	$item->description => array('view', 'name' => $item->name),
	Yii::t('AuthModule.main', 'Edit'),
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h2><?php echo CHtml::encode($item->description); ?> <small>(<?php echo $this->getTypeText(); ?>)</small></h2>
	</div>
	<div class="panel-body">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'type'=>'horizontal',
		)); ?>

		<?php echo $form->hiddenField($model, 'type'); ?>
		<div class="form-group">
			<?php echo $form->labelEx($model, 'name', array('class'=>'col-sm-2 control-label'))?>
			<div class="col-sm-8">
				<?php echo $form->textField($model, 'name', array(
					'disabled'=>true,
					'title'=>Yii::t('AuthModule.main', 'System name cannot be changed after creation.'),
					'class'=>'form-control'
				)); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model, 'description', array('class'=>'col-sm-2 control-label'))?>
			<div class="col-sm-8">
				<?php echo $form->textField($model, 'description', array('class'=>'form-control')); ?>
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<div class="col-sm-8 col-sm-offset-2">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'submit',
				'type' => 'success',
				'label' => Yii::t('AuthModule.main', 'Save'),
			)); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'type' => 'link',
				'label' => Yii::t('AuthModule.main', 'Cancel'),
				'url' => array('view', 'name' => $item->name),
			)); ?>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>