<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $model AuthItemForm */
/* @var $form TbActiveForm */

$this->breadcrumbs = array(
	$this->capitalize($this->getTypeText(true)) => array('index'),
	Yii::t('AuthModule.main', 'New {type}', array('{type}' => $this->getTypeText())),
);
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<h2><?php echo Yii::t('AuthModule.main', 'New {type}', array('{type}' => $this->getTypeText())); ?></h2>
	</div>
	<div class="panel-body">
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'type'=>'horizontal',
		)); ?>
		<div class="form-group">
			<?php echo $form->hiddenField($model, 'type'); ?>
			<?php echo $form->labelEx($model, 'name', array('class'=>'col-sm-2 control-label'))?>
			<div class="col-sm-8">
				<?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model, 'description', array('class'=>'col-sm-2 control-label'))?>
			<div class="col-sm-8">
				<?php echo $form->textField($model, 'description', array('class'=>'form-control')); ?>
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<div class="col-sm-8 col-sm-offset-2">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'submit',
				'type' => 'success',
				'label' => Yii::t('AuthModule.main', 'Create'),
			)); ?>
			<?php $this->widget('TbButton', array(
				'type' => 'link',
				'label' => Yii::t('AuthModule.main', 'Cancel'),
				'url' => array('index'),
			)); ?>
		</div>
	</div>
</div>



<?php $this->endWidget(); ?>
