<?php
/* @var $this AssignmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    Yii::t('AuthModule.main', 'Assignments'),
);
?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h2><?php echo Yii::t('AuthModule.main', 'Assignments'); ?></h2>
  </div>
  <div class="panel-body">
    <?php $this->widget('bootstrap.widgets.TbGridView', array(
      'type' => 'striped hover',
      'dataProvider' => $dataProvider,
      'emptyText' => Yii::t('AuthModule.main', 'No assignments found.'),
      'template'=>"{items}\n{pager}",
      'columns' => array(
        array(
          'header' => Yii::t('AuthModule.main', 'User'),
          'class' => 'AuthAssignmentNameColumn',
        ),
        array(
          'header' => Yii::t('AuthModule.main', 'Assigned items'),
          'class' => 'AuthAssignmentItemsColumn',
        ),
        array(
          'class' => 'AuthAssignmentViewColumn',
        ),
      ),
    )); ?>
  </div>
</div>


