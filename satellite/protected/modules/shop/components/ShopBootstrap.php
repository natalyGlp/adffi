<?php
/**
 * Инициализация костылей для супер магазина
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package modules.shop.components
 */
class ShopBootstrap extends CApplicationComponent
{
	/**
	 * @var array $db Настройки базы данных для супер магазина
	 */
	public $db;
	protected $_dbInstance;

	/**
	 * @var string $uploadsPath путь к директории с загрузками супер магазина
	 */
	public $uploadsPath;

	function init()
	{
		Yii::import('application.modules.shop.components.*');
		
		if(!isset($this->db))
			throw new CException('Отсутствует обязательный параметр db модуля магазина');
		
		if(!isset($_GET['site']) ||(isset($_GET['site']) && empty($_GET['site'])))
		{
			$db = Yii::createComponent($this->db);
			Yii::app()->setComponent(CONNECTION_NAME, $db);
			$this->_dbInstance = $db;
		}

		// Добавляем себя в меню
		$notifies = Yii::app()->notify->fetch(array('category' => 'shopNewOrders'));
		if(count($notifies) > 0)
			$counter = ' <span class="label">'.$notifies[0]->counter.'</span>';
		else
			$counter = '';
		$params = array(
			'headerMenu' => array(
				array('label'=>'Магазин'.$counter, 'url'=>array('/shop'), 'encode' => false),
			)
		);
		
		Yii::app()->params = CMap::mergeArray(Yii::app()->params, $params);
	}

	public function getDbConnection()
	{
		return $this->_dbInstance;
	}
}