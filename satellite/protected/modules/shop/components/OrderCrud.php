<?php
/**
 * Отвечает за CRUD заказов с учетом связей
 */
class OrderCrud extends CComponent
{
    protected $_orderId = null;
    protected $_orderData = array();
    protected $_models = array();
    protected $_operatorId = false;

    /**
     * @var boolean $throwExceptions определяет характер поведения save() при ошибках.
     *              Если true, то обьект будет выкидывать исключения
     *              в противном случае save() будет просто возвращать false
     */
    public $throwExceptions = true;

    public function __construct($orderId = null)
    {
        Yii::import('cms.models.order.*');

        $this->_orderId = $orderId;
        $this->instantiateModels($orderId);
    }

    public function setData($orderData)
    {
        $this->_orderData = $orderData;
    }

    public function ajaxValidate()
    {
        if (isset($_POST['ajax'])) {
            echo CActiveForm::validate(array(
                $this->_models['order'],
                $this->_models['address'],
                $this->_models['client'],
            ));
            Yii::app()->end();
        }
    }

    public function setOperator($id)
    {
        $this->_operatorId = $id;
    }

    public function getModel($key)
    {
        if (!isset($this->_models[$key])) {
            throw new CException("$key model is not exists");
        }

        return $this->_models[$key];
    }

    public function save()
    {
        $transaction = Yii::app()->{CONNECTION_NAME}->beginTransaction();
        try {
            $this->processClientAddress();

            $this->processOrder($this->_models['address']);

            $this->processClient($this->_models['order']);

            $this->processProducts($this->_models['order']);

            $transaction->commit();
        } catch (Exception $e) {
            $this->log($e->getMessage(), 'error');
            $transaction->rollBack();
            if ($this->throwExceptions) {
                throw $e;
            }
            return false;
        }

        return true;
    }

    protected function processClientAddress()
    {
        // TODO: OrderAddress is DEPRECATED
        $address = $this->_models['address'];
        if ($address->isNewRecord) {
            $address->user_id = new CDbExpression('NULL');
        }
        $address->attributes = $this->_orderData['OrderAddress'];

        if (!$address->save()) {
            throw new Exception("Error Processing Address\nErrors dump:\n" . var_export($address->errors, true));
        }
    }

    protected function processOrder($address)
    {
        $order = $this->_models['order'];
        // TODO: присваиваем не безопасно, так как в форме в админке есть поля,
        // которые нельзя заполнять обычному юзеру. Лучше переспмотреть правила валидации

        Yii::log(' данны CRU - '.print_r($_POST,true));
        if ($order->isNewRecord) {
            $order->setAttributes(array(
                'user_id' => new CDbExpression('NULL'),
                'ip' => Yii::app()->request->getUserHostAddress(),
                'referer' => ( Yii::app()->request->urlReferrer=='' or is_null(Yii::app()->request->urlReferrer) ) ? (is_null($_POST['orderData']['referer']))?'wrong' :$_POST['orderData']['referer'] : Yii::app()->request->urlReferrer ,
                'address_id' => $address->primaryKey,
            ), false);
        }
        $order->attributes = $this->_orderData['Order'];

        if ($this->_operatorId) {
            $order->operator_id = $this->_operatorId;
        }

        if (!$order->save()) {
            throw new Exception("Error Processing Order\nErrors dump:\n" . var_export($order->errors, true));
        }
    }

    protected function processClient($order)
    {
        $client = $this->_models['client'];
        $client->attributes = $this->_orderData['Client'];

        $client->order_id = $order->primaryKey;

        if (!$client->save()) {
            throw new Exception("Error Processing Client Data\nErrors dump:\n" . var_export($client->errors, true));
        }
    }

    /**
     * Adds new products to the order, changes amount of the old products,
     * processes deleted from order products
     *
     * NOTE: OrderCheck should be array of arrays(prod_id=>integer, quantity=>integer)
     */
    protected function processProducts($order)
    {
        if (!isset($this->_orderData['OrderCheck']) || !isset($this->_orderData['OrderCheck'][0])) {
            throw new Exception("There is no products in order\nErrors dump:\n" . var_export($this->_orderData, true));
        }

        $existedProducts = array();
        foreach ($this->getCurrentCheck() as $check) {
            $existedProducts[$check->prod_id] = $check;
        }

        $submittedProducts = array_reduce($this->_orderData['OrderCheck'], function($acc, $check) {
            $acc[$check['prod_id']] = $check;

            return $acc;
        }, array());
        $prods = CatalogObject::model()->findAllByAttributes(array(
            'object_id' => array_keys($submittedProducts),
            'object_type' => 'catalog',
        ));
        foreach ($prods as $prod) {
            $check = isset($existedProducts[$prod->primaryKey]) ? $existedProducts[$prod->primaryKey] : new OrderCheck;
            if ($check->isNewRecord) {
                $check->attributes = array(
                    'order_id' => $order->primaryKey,
                    'prod_id' => $prod->primaryKey,
                );
            }

            $check->attributes = $submittedProducts[$prod->primaryKey]; // prod_id, quantity

            $check->currency_id = $order->currency_id;

            if (!$check->save()) {
                throw new Exception("Error Processing Check Data\nErrors dump:\n" . var_export($check->errors, true));
            }
        }

        $this->processRemovedProducts($existedProducts, $submittedProducts);

        // обновляем модели чека, что бы внешние классы получали актуальную инфу (например для рендеринга)
        $this->_models['checks'] = $this->getCurrentCheck();
    }

    /**
     * Compares $initialProducts with $currentProducts and deletes $initialProducts that
     * is not in $currentProducts
     *
     * @param  array $initialProducts the array with OrderCheck models, that was before
     * @param  array $currentProducts the array with products, that we have now
     * NOTE: the both arrays should have product primary keys as index
     */
    protected function processRemovedProducts($initialProducts, $currentProducts)
    {
        $currentProductsIds = array_keys($currentProducts);
        $initialProductsIds = array_keys($initialProducts);

        $removedProducts = array_values(array_diff($initialProductsIds, $currentProductsIds));
        foreach ($removedProducts as $productId) {
            if (!$initialProducts[$productId]->delete()) {
                throw new Exception("Can't delete product from check. Check id: {$initialProducts[$productId]->primaryKey}, Product id: $productId");
            }
        }
    }

    protected function instantiateModels($orderId)
    {
        if ($orderId) {
            // FIXME: user был закоменчен так как бывают случаи, когда таблица пользователей находится в другой бд
            $this->_models['order'] = Order::model()->with('address'/*, 'user'*/, 'client')->findByPk($orderId);

            if (!$this->_models['order']) {
                throw new CException("There is no order with id $orderId");
            }

            $this->_models['address'] = $this->_models['order']->address; // TODO: deprecated

            if (isset($this->_models['order']->user_id)) {
                $this->_models['client'] = $this->_models['order']->user;
            } else {
                $this->_models['client'] = $this->_models['order']->client;
            }
        } else {
            $this->_models['order'] = new Order();
            $this->_models['address'] = new OrderAddress(); // TODO: deprecated
            $this->_models['client'] = new Client();
        }

        $this->_models['checks'] = $this->getCurrentCheck();
    }

    protected function getCurrentCheck()
    {
        if (isset($this->_orderId)) {
            return OrderCheck::model()->with('prod')->findAllByAttributes(array('order_id' => $this->_orderId));
        } else {
            return array();
        }
    }

    protected function log($message, $level = 'info')
    {
        Yii::log($message, $level, get_class($this));
    }
}
