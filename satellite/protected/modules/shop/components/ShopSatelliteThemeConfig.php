<?php
/**
 * Настройки базовой темы
 * 
 * @author Sviatoslav <dev@udf.su>
 * @version 1.0
 * @package satellite.modules.shop.components
 */

Yii::import('application.modules.shop.widgets.ShopContentlistSelector');
class ShopSatelliteThemeConfig extends BaseThemeConfig
{
	/**
	 * @var integer $productId - id продукта, который будет продаваться на лендинге
	 */
	public $productId;

	public function rules()
	{
		return array(
			array('productId', 'safe'),
			);
	}

	public function load()
	{
		if(!isset(Yii::app()->modules['shop']))
			throw new CException('Для работы темы необходимо активировать модуль магазина');
			
		$this->productId = $this->getSetting('productId');

		// в форме нам нужна связь с бд магазина
		$this->switchDb(Yii::app()->shopBootstrap->dbConnection);
	}

	public function attributeLabels()
	{
		return array(
			'productId' => 'Выберите продукт или группу продуктов',
			);
	}

	public function getFormConfig()
	{
		return array(
			'productId' => array('type'=>'ShopContentlistSelector'),
			);
	}

	/**
	 * Выполняется после успешной валидации формы
	 * @return boolean true, если форма сохранена успешно
	 */
	public function configure()
	{
		$valid = $this->setSetting($this->productId, 'productId');
		$valid = $valid && $this->setSetting(Yii::app()->createAbsoluteUrl('shop/api'), 'apiUrl');

		return $valid;
	}
}
