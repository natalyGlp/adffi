<?php
/**
 * Вспомогательный класс для получения информации о продуктах
 * для сайтов, подключенных к сателлитному магазину
 */

namespace shop\components;

class ShopSatellite extends \CComponent
{
    protected $_host;
    protected $_options = array(
        'featured' => false,
    );
    protected $_cache = array();

    /**
     * @var array
     */
    protected $_countries = array(
        // 'UA' => array(
        //     'name' => 'Украина',
        //     'currency' => 'UAH', // валюта по умолчанию
        // ),
        'RU' => array(
            'name' => 'Россия',
            'currency' => 'RUR', // валюта по умолчанию
        ),
        'KZ' => array(
            'name' => 'Казахстан',
            'currency' => 'KZT', // валюта по умолчанию
        ),
    );

    public function __construct($host)
    {
        $host = preg_replace('/^https?:\/\//', '', $host);
        $host = trim($host, '/');

        $this->_host = $host;
    }

    public function setOptions(array $options)
    {
        $this->_options = \CMap::mergeArray($this->_options, $options);
    }

    public function getProducts()
    {
        if (!isset($this->_cache['products'][$this->_options['featured']])) {
            if (!isset($this->_cache['products'])) {
                $this->_cache['products'] = array();
            }

            $this->_cache['products'][$this->_options['featured']] = \ShopProductSites::findAllProductsByHost($this->_host, array(
                'condition' => 'is_featured='.($this->_options['featured']*1),
            ));
        }

        return $this->_cache['products'][$this->_options['featured']];
    }

    /**
     * @return array|null данные для текущего продукта
     */
    public function getStoreData()
    {
        return array(
            'products' => $this->getProductsInfo(),
            'productIndexById' => $this->getProductIndexById(),
            'countries' => $this->_countries,
            'delivery' => array(
                // 'UANP' => array(
                //     'name' => 'Нова Пошта',
                //     'message' => 'Доставка наложенным платежом Новой Почтой.<br />Оплата при получении товара!',
                // ),
            ),
            'deals' => array( // скидки
            ),
        );
    }

    /**
     * @param CatalogObject $product обьект продукта
     * @return array массив с информацией о продукте
     */
    protected function getProductsInfo($includeVariants = true, $products = false)
    {
        if (!$products) {
            $products = $this->getProducts();
        }

        $return = array();
        foreach ($products as $product) {
            // TODO: возможность задать размеры через сателлитку
            $photos = \GxcHelpers::getResourceObjectFromDatabase($product, 'thumbnail');

            foreach ($photos as &$photo) {
                $photo = array(
                    'link' => \Yii::app()->createAbsoluteUrl($photo['link']),
                );
            }
            $currentProduct = array(
                'id' => $product->primaryKey,
                'name' => $product->object_name,
                'photos' => $photos,
                'prices' => $this->getProductPrices($product),
                'variants' => array(),
                'amountAvailable' => $product->calculateProductCount(),
                'delivery' => array(
                    'UANP' => array(
                        //'value' => (float) $productData->deliveryPrice,
                        'currency' => 'UAH',
                    ),
                ),
            );

            if ($includeVariants && count($product->variants) > 0) {
                $currentProduct['variants'] = $this->getProductsInfo(false, \CMap::mergeArray(array($product), $product->variants));

                $this->syncVariantsCurrency($currentProduct);
            }

            array_push($return, $currentProduct);
        }

        return $return;
    }

    protected function syncVariantsCurrency($product)
    {
        foreach ($product['variants'] as $variant) {
            $variant['prices'] = array_intersect_key($variant['prices'], $product['prices']);
        }
    }

    protected function getProductIndexById()
    {
        $productIndexById = array();
        foreach ($this->getProducts() as $index => $product) {
            $productIndexById[$product->primaryKey] = $index;
        }

        return $productIndexById;
    }

    protected function getProductPrices($product)
    {
        $data = array();
        foreach ($product->productData as $prices) {
            $data[$prices->currency->code] = array(
                'price' => (float)$prices->sellPrice,
                'tax' => (float)$prices->tax,
                'defaultPrice' => (float)$prices->default_price,
                'deliveryPrice' => (float)$prices->deliveryPrice,
                'oldPrice' => (float)$prices->oldPrice,
                'currencySign' => $prices->currency->sign,
                'currencyCode' => $prices->currency->code,
            );
        }

        return $data;
    }
}
