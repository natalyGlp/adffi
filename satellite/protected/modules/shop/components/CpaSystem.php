<?php
/**
 * Class AffiliateSystem Компонент для работы с API Аффилиатки
 */
Yii::import('models.Sites');
class CpaSystem extends CApplicationComponent
{
    const TRANSACTION_STATUS_APPROVE = 'approved';
    const TRANSACTION_STATUS_PENDING = 'P';
    const TRANSACTION_STATUS_DECLINE = 'decline';

    protected $_session;
    public $cpaUrl = "http://cpa.qore.us/api/transaction/";
    public $userName;
    public $password;

 
    /**
     * Изменяет статус на Подтвержден
     * @param $orderId
     * @return bool
     */
    public function approveOrder($orderId,$api_key,$comment)
    {
        return $this->setTransactionStatus($orderId,$api_key,$comment, self::TRANSACTION_STATUS_APPROVE);
    }

    /**
     * Изменяет статус на ОТМЕНЕН
     * @param $orderId
     * @return bool
     */
    public function declineOrder($orderId,$api_key,$comment)
    {
        return $this->setTransactionStatus($orderId,$api_key,$comment, self::TRANSACTION_STATUS_DECLINE);
    }

    /**
     * Изменяет статус заказа
     * @param $orderId
     * @param $status
     * @return bool
     */
    protected function setTransactionStatus($orderId,$api_key,$comment, $status)
    {
        
        $model = Sites::model()->find(
            array(
              'condition' => 'url = :url',
              'params'    => array(':url' => "http://".$api_key)
            )
        );
         
    
        //hash mda api_key
        $url = $this->cpaUrl.md5($model->api_key)."/".$orderId;
        YII::log("CPA data send ".print_r($model,true)." id = ".$orderId." url = ".$url." status = ".$status);
       
        $ch = curl_init("$url");
        curl_setopt_array($ch, array(
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER=> array(
                'Content-Type: application/x-www-form-urlencoded',
                ),
            CURLOPT_POSTFIELDS => "status=".$status."&statusReason=".$comment
            )
        );
        $response = curl_exec($ch);
        if($response ===FALSE){
            Yii::log("Nat curl cpaProffi error");
            return false;
        }
        $responseData = json_decode($response,true);
        YII::log("data answer CPA Proffi = ".print_r($responseData,true));
        if($response['status'])
            return true;
        else
            return false;


    }
}
