<?php
class OrderCreator extends CComponent
{
    public function __construct()
    {
        Yii::import('cms.models.order.*');
    }

    /**
     * Валидна ли модель
     * @var bool
     */
    protected $_valid = true;

    /**
     * Обрабатывает заказ отправленный с лендинга
     * @param $orderData
     * @return bool|Order
     * @throws CDbException
     */
    public function issueOrder($orderData)
    {
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $address = new OrderAddress();
            $address->setAttributes(array(
                'user_id' => new CDbExpression('NULL'),
                'country' => $orderData['country'],
                'city' => $orderData['city'],
                'street' => (!empty($orderData['zip']) ? $orderData['zip'] . ', ' : '') . $orderData['street'],
                'house' => $orderData['house'],
                'flat' => $orderData['flat'],
                'telephone' => $orderData['phone'],
            ), false);
            $this->_valid = $this->_valid && $address->save();

            if (!$this->_valid) {
                throw new Exception("Error Processing Address\nErrors dump:\n" . var_export($address->errors, true));
            }

            $order = new Order();
            $order->setAttributes(array(
                'user_id' => new CDbExpression('NULL'),
                'address_id' => $address->primaryKey,
                'type' => isset($orderData['delivery']) && $orderData['delivery'] > 0 ? $orderData['delivery'] : 3, // доставка
                'currency_provider' => $orderData['currencyProvider'], // серевис через который был оплачен заказ
                'currency_id' => $orderData['currency_id'],
                'referer' => Yii::app()->request->urlReferrer,
                'ip' => $orderData['ip'],
                'comment' => $orderData['comment'],
            ), false);
            $this->_valid = $this->_valid && $order->save();

            if (!$this->_valid) {
                throw new Exception("Error Processing Order\nErrors dump:\n" . var_export($order->errors, true));
            }

            $client = new Client();
            $client->setAttributes(array(
                'order_id' => $order->primaryKey,
                'first_name' => $orderData['name'],
                'last_name' => new CDbExpression('NULL'),
                'email' => $orderData['email'],
            ), false);
            $this->_valid = $this->_valid && $client->save();

            if (!$this->_valid) {
                throw new Exception("Error Processing Client Data\nErrors dump:\n" . var_export($client->errors, true));
            }

            if (!isset($orderData['products']) || count($orderData['products']) === 0) {
                throw new Exception("There is no products in order\nErrors dump:\n" . var_export($orderData, true));
            }

            foreach ($orderData['products'] as $productData) {
                $check = new OrderCheck();
                $check->setAttributes(array(
                    'order_id' => $order->primaryKey,
                    'prod_id' => $productData['id'],
                    'quantity' => $productData['amount'],
                    'currency_id' => $orderData['currency_id'],
                ), false);

                $this->_valid = $this->_valid && $check->save();
            }

            if (!$this->_valid || count($orderData['products']) === 0) {
                throw new Exception("Error Processing Check Data\nErrors dump:\n" . var_export($check->errors, true));
            }

            $transaction->commit();
        } catch (Exception $e) {
            $this->log($e->getMessage(), 'error');
            $transaction->rollBack();
            return false;
        }

        return $order;
    }

    protected function log($message, $level = 'info')
    {
        Yii::log($message, $level, get_class($this));
    }
}
