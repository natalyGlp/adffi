<?php
/**
 * Class AffiliateSystem Компонент для работы с API Аффилиатки
 */
class AffiliateSystem extends CApplicationComponent
{
    const TRANSACTION_STATUS_APPROVE = 'A';
    const TRANSACTION_STATUS_PENDING = 'P';
    const TRANSACTION_STATUS_DECLINE = 'D';

    protected $_session;
    public $affiliateUrl;
    public $userName;
    public $password;

    /**
     * Инициализация АПИ и логин
     */
    public function init()
    {
        try {
            require_once Yii::getPathOfAlias('shop.extensions') . '/PapApi.class.php';

            $this->_session = new Gpf_Api_Session($this->affiliateUrl);

            if (!$this->_session->login($this->userName, $this->password)) {
                throw new CException("Cannot login. Message: " . $this->_session->getMessage());
            }
            parent::init();
        } catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
        }
    }
    /**
     * Изменяет статус на Подтвержден
     * @param $orderId
     * @return bool
     */
    public function approveOrder($orderId)
    {
        return $this->setTransactionStatus($orderId, self::TRANSACTION_STATUS_APPROVE);
    }

    /**
     * Изменяет статус на ОТМЕНЕН
     * @param $orderId
     * @return bool
     */
    public function declineOrder($orderId)
    {
        return $this->setTransactionStatus($orderId, self::TRANSACTION_STATUS_DECLINE);
    }

    /**
     * Изменяет статус заказа
     * @param $orderId
     * @param $status
     * @return bool
     */
    protected function setTransactionStatus($orderId, $status)
    {
        if (!$this->isInitialized) {
            return false;
        }

        $transaction = new Pap_Api_Transaction($this->_session);
    //    Yii::log(" orderId = ".$orderId." -  ".$status);
    //    if($transaction->setOrderId($orderId)){
            $transaction->setOrderId($orderId);
            // подтверждаем только основную комиссию
            // родителей рефералов (если они будут) — игнорим
            $transaction->setTier(1);
            $transaction->load();

            $transaction->setStatus($status);
            return $transaction->save() ? true : false;
    //    }else{return false;}

    }
}
