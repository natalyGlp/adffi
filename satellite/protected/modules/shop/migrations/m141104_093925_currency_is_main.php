<?php

class m141104_093925_currency_is_main extends CDbMigration
{
    protected $_cmsdb;
    public function __construct()
    {
        $config = require (Yii::getPathOfAlias('application.config.custom') . '.php');
        $db = Yii::createComponent($config['components']['shopBootstrap']['db']);
        $this->_cmsdb = $db;

        $this->dbConnection->schema->refresh();
    }

    public function getDbConnection()
    {
        return $this->_cmsdb;
    }

	public function up()
	{
        $this->addColumn('{{currency}}', 'is_main', 'tinyint(1) DEFAULT NULL');
        /**
         * USD по дефолту
         */
        $this->update(
            '{{currency}}',
            array('is_main' => 1),
            'id = :id',
            array(':id'=>2)
        );
    }

	public function down()
	{
        $this->dropColumn('{{currency}}', 'is_main');
	}
}