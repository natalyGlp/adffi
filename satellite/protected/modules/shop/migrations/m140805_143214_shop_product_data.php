<?php

class m140805_143214_shop_product_data extends CDbMigration
{
	protected $_cmsdb;
	public function __construct()
	{
		$config = require(Yii::getPathOfAlias('application.config.custom').'.php');
		$db = Yii::createComponent($config['components']['shopBootstrap']['db']);
		$this->_cmsdb = $db;

		$this->dbConnection->schema->refresh();
	}

	public function getDbConnection()
	{
		return $this->_cmsdb;
	}

	public function safeUp()
	{
		if(!$this->dbConnection->schema->getTable('{{filters}}') && !$this->dbConnection->schema->getTable('{{object_meta}}')) {
			throw new Exception('Wrong db! The tables `filters` and `object_meta` cannot be found');
		}

		if(!$this->dbConnection->schema->getTable('{{shop_product_data}}')) {
			$this->createTable('{{shop_product_data}}', array(
			  'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
			  'product_id' => 'INT(11) UNSIGNED NOT NULL',
			  'custom_price' => 'decimal(19,2) UNSIGNED NULL',
			  'default_price' => 'decimal(19,2) UNSIGNED NOT NULL',
			  'tax' => 'decimal(19,2) UNSIGNED NULL',
			  'tax_type' => 'TINYINT(2) UNSIGNED',
			  'delivery_price' => 'decimal(19,2) UNSIGNED NULL',
			  'delivery_price_type' => 'TINYINT(2) UNSIGNED NOT NULL',
			  'discount' => 'decimal(19,2) UNSIGNED NULL',
			  'discount_type' => 'TINYINT(2) UNSIGNED NOT NULL',
			  'product_count' => 'INT(11) UNSIGNED NOT NULL',
			  'PRIMARY KEY (`id`)',
			  'UNIQUE KEY `product_id` (`product_id`)',
			), 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
		}

		// почистим таблицу с фильтрафи для профилактики. там точно много хлама...
		$this->delete('{{filters}}', 
			'id_object NOT IN (SELECT object_id FROM {{object}})' 
			);

		$filterData = $this->dbConnection->createCommand()
			->select('*')
			->from('{{filters}}')
			->queryAll();

		$_metaData = $this->dbConnection->createCommand()
			->select('*')
			->from('{{object_meta}}')
			->where('meta_key IN("oldPrice","sellTax","buyPrice","deliveryPrice","productCount")')
			->queryAll();
		$metaData = array();
		foreach ($_metaData as $meta) {
			$metaData[$meta['meta_object_id']][$meta['meta_key']] = $meta['meta_value'];
		}

		foreach ($filterData as $filter) {
			$meta = isset($metaData[$filter['id_object']]) ? $metaData[$filter['id_object']] : array();
			$discount = 0;
			if(isset($meta['oldPrice'])) {
				$discount =  $meta['oldPrice'] - $filter['price'];
			}
			if($discount <= 0) {
				$discount = new CDbExpression('NULL');
			}
			try {
				$this->insert('{{shop_product_data}}', array(
					  'product_id' => $filter['id_object'],
					  'custom_price' => new CDbExpression('NULL'),
					  'default_price' => isset($meta['oldPrice']) ? $meta['oldPrice'] : $filter['price'],
					  'tax' => new CDbExpression('NULL'),
					  'tax_type' => 0, // percent amount
					  'delivery_price' => isset($meta['deliveryPrice']) ? $meta['deliveryPrice'] : new CDbExpression('NULL'),
					  'delivery_price_type' => 1, // fixed amount
					  'discount' => $discount,
					  'discount_type' => 1, // fixed amount
					  'product_count' => isset($meta['productCount']) ? $meta['productCount'] : 0,
					));
			} catch(Exception $e) {}
		}
	}

	public function down()
	{
		$this->dropTable('{{shop_product_data}}');
	}
}
