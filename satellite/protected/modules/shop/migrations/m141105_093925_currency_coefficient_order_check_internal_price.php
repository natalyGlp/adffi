<?php

class m141105_093925_currency_coefficient_order_check_internal_price extends CDbMigration
{
    protected $_cmsdb;
    public function __construct()
    {
        $config = require (Yii::getPathOfAlias('application.config.custom') . '.php');
        $db = Yii::createComponent($config['components']['shopBootstrap']['db']);
        $this->_cmsdb = $db;

        $this->dbConnection->schema->refresh();
    }

    public function getDbConnection()
    {
        return $this->_cmsdb;
    }

	public function up()
	{
        try {
            $this->addColumn('{{currency}}', 'coefficient', 'decimal(10,6) NOT NULL');
            $this->addColumn('{{order_check}}', 'internal_price', 'decimal(10,2) NOT NULL');
        } catch(Exception $e) {
            $this->dropColumn('{{currency}}', 'coefficient');
            $this->dropColumn('{{order_check}}', 'internal_price');
            $this->addColumn('{{currency}}', 'coefficient', 'decimal(10,6) NOT NULL');
            $this->addColumn('{{order_check}}', 'internal_price', 'decimal(10,2) NOT NULL');
        }
        /**
         * USD по дефолту
         */
        $this->update(
            '{{currency}}',
            array('coefficient' => 0.741),
            'id = :id',
            array(':id'=>1)
        );
        $this->update(
            '{{currency}}',
            array('coefficient' => 1),
            'id = :id',
            array(':id'=>2)
        );
        $this->update(
            '{{currency}}',
            array('coefficient' => 1.251),
            'id = :id',
            array(':id'=>3)
        );
        $this->update(
            '{{currency}}',
            array('coefficient' => 0.0238),
            'id = :id',
            array(':id'=>4)
        );
        $this->update(
            '{{currency}}',
            array('coefficient' => 0.0055),
            'id = :id',
            array(':id'=>5)
        );
    }

	public function down()
	{
        $this->dropColumn('{{currency}}', 'is_main');
        $this->dropColumn('{{order_check}}', 'internal_price');
	}
}