<?php
/**
 * Добавляем колонку currencies
 */
class m141029_102932_currencies_for_product_sites extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{shop_product_sites}}', 'currencies', 'VARCHAR(255) NULL');

        $this->update('{{shop_product_sites}}', array(
            'currencies' => 'UAH',
            ));
    }

    public function down()
    {
        $this->dropColumn('{{shop_product_sites}}', 'currencies', 'VARCHAR(255) NULL');
    }
}
