<?php

class m141113_133736_currency_code_to_currency_id extends CDbMigration
{
    protected $_cmsdb;
    public function __construct()
    {
        $config = require (Yii::getPathOfAlias('application.config.custom') . '.php');
        $db = Yii::createComponent($config['components']['shopBootstrap']['db']);
        $this->_cmsdb = $db;

        $this->dbConnection->schema->refresh();
    }

    public function getDbConnection()
    {
        return $this->_cmsdb;
    }

	public function up()
	{
        /*===================ORDER===================================*/
        $this->update(
            '{{order}}',
            array('currency' => 1),
            'currency = :code',
            array(':code'=>'UAH')
        );
        $this->update(
            '{{order}}',
            array('currency' => 2),
            'currency = :code',
            array(':code'=>'USD')
        );
        $this->update(
            '{{order}}',
            array('currency' => 3),
            'currency = :code',
            array(':code'=>'EUR')
        );
        $this->update(
            '{{order}}',
            array('currency' => 4),
            'currency = :code',
            array(':code'=>'RUB')
        );
        $this->update(
            '{{order}}',
            array('currency' => 5),
            'currency = :code',
            array(':code'=>'KZT')
        );
        $this->renameColumn('{{order}}', 'currency', 'currency_id');
        $this->alterColumn('{{order}}', 'currency_id','INT(3) NOT NULL');

        /*===================ORDER_CHECK===================================*/

        $this->update(
            '{{order_check}}',
            array('currency' => 1),
            'currency = :code',
            array(':code'=>'UAH')
        );
        $this->update(
            '{{order_check}}',
            array('currency' => 2),
            'currency = :code',
            array(':code'=>'USD')
        );
        $this->update(
            '{{order_check}}',
            array('currency' => 3),
            'currency = :code',
            array(':code'=>'EUR')
        );
        $this->update(
            '{{order_check}}',
            array('currency' => 4),
            'currency = :code',
            array(':code'=>'RUB')
        );
        $this->update(
            '{{order_check}}',
            array('currency' => 5),
            'currency = :code',
            array(':code'=>'KZT')
        );
        $this->renameColumn('{{order_check}}', 'currency', 'currency_id');
        $this->alterColumn('{{order_check}}', 'currency_id','INT(3) NOT NULL');

	}

	public function down()
	{
        /*===================ORDER===================================*/

        $this->alterColumn('{{order}}', 'currency_id','VARCHAR(3) NOT NULL');
        $this->renameColumn('{{order}}', 'currency_id', 'currency');
        $this->update(
            '{{order}}',
            array('currency' => 'UAH'),
            'currency = :code',
            array(':code'=>1)
        );
        $this->update(
            '{{order}}',
            array('currency' => 'USD'),
            'currency = :code',
            array(':code'=>2)
        );
        $this->update(
            '{{order}}',
            array('currency' => 'EUR'),
            'currency = :code',
            array(':code'=>3)
        );
        $this->update(
            '{{order}}',
            array('currency' => 'RUB'),
            'currency = :code',
            array(':code'=>4)
        );
        $this->update(
            '{{order}}',
            array('currency' => 'KZT'),
            'currency = :code',
            array(':code'=>5)
        );




        /*===================ORDER_CHECK===================================*/

        $this->alterColumn('{{order_check}}', 'currency_id','VARCHAR(3) NOT NULL');
        $this->renameColumn('{{order_check}}', 'currency_id', 'currency');
        $this->update(
            '{{order_check}}',
            array('currency' => 'UAH'),
            'currency = :code',
            array(':code'=>1)
        );
        $this->update(
            '{{order_check}}',
            array('currency' => 'USD'),
            'currency = :code',
            array(':code'=>2)
        );
        $this->update(
            '{{order_check}}',
            array('currency' => 'EUR'),
            'currency = :code',
            array(':code'=>3)
        );
        $this->update(
            '{{order_check}}',
            array('currency' => 'RUB'),
            'currency = :code',
            array(':code'=>4)
        );
        $this->update(
            '{{order_check}}',
            array('currency' => 'KZT'),
            'currency = :code',
            array(':code'=>5)
        );
	}
}