<?php
/**
 * Таблица для привязки сайтов к продкутам
 */
class m140801_105210_product_sites_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('{{shop_product_sites}}', array(
			'product_id' => 'INT(11) UNSIGNED NOT NULL',
			'site_id' => 'INT(11) UNSIGNED NOT NULL',
			'is_featured' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
			), 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->createIndex('uniq_product_sites_pair', '{{shop_product_sites}}', 'product_id,site_id', true);
	}

	public function down()
	{
		$this->dropTable('{{shop_product_sites}}');
	}
}