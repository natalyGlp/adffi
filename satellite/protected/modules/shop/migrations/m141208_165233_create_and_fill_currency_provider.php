<?php

class m141208_165233_create_and_fill_currency_provider extends CDbMigration
{
    protected $_cmsdb;
    public function __construct()
    {
        $config = require (Yii::getPathOfAlias('application.config.custom') . '.php');
        $db = Yii::createComponent($config['components']['shopBootstrap']['db']);
        $this->_cmsdb = $db;

        $this->dbConnection->schema->refresh();
    }

    public function getDbConnection()
    {
        return $this->_cmsdb;
    }

	public function up()
	{
        $this->createTable('{{currency_provider}}', array(
                'id' => 'int(11) NOT NULL AUTO_INCREMENT',
                'name'=> 'varchar(150) NOT NULL',
                'PRIMARY KEY (`id`)',
            ),
            'ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8'
        );

        $this->insert(
            '{{currency_provider}}',
            array('name' => 'Оплата наличными', 'id'=>1)
        );
        $this->insert(
            '{{currency_provider}}',
            array('name' => 'WMR', 'id'=>2)
        );
        $this->insert(
            '{{currency_provider}}',
            array('name' => 'WMZ', 'id'=>3)
        );
        $this->insert(
            '{{currency_provider}}',
            array('name' => 'WMU', 'id'=>4)
        );
        $this->insert(
            '{{currency_provider}}',
            array('name' => 'Приват24: UAH', 'id'=>5)
        );
        $this->insert(
            '{{currency_provider}}',
            array('name' => 'Приват24: USD', 'id'=>6)
        );
        $this->insert(
            '{{currency_provider}}',
            array('name' => 'Приват24: EUR', 'id'=>7)
        );
        $this->insert(
            '{{currency_provider}}',
            array('name' => 'Безналичный расчет', 'id'=>8)
        );
	}

	public function down()
	{
        $this->dropTable('{{currency_provider}}');
	}
}