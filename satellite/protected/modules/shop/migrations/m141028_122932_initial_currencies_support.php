<?php

class m141028_122932_initial_currencies_support extends CDbMigration
{
    protected $_cmsdb;
    public function __construct()
    {
        $config = require (Yii::getPathOfAlias('application.config.custom') . '.php');
        $db = Yii::createComponent($config['components']['shopBootstrap']['db']);
        $this->_cmsdb = $db;

        $this->dbConnection->schema->refresh();
    }

    public function getDbConnection()
    {
        return $this->_cmsdb;
    }

    public function up()
    {
        $this->dropCurrencyIfExist();

        $this->createTable('{{currency}}', array(
            'id' => 'int(11) NOT NULL AUTO_INCREMENT',
            'name' => 'varchar(50) NOT NULL',
            'sign' => 'varchar(10) NOT NULL',
            'code' => 'varchar(10) NOT NULL DEFAULT ""',
            'PRIMARY KEY (`id`)',
            'UNIQUE KEY `code` (`code`)',
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->insert('{{currency}}', array(
            'id' => '1',
            'name' => 'Ukrainian hryvnia',
            'sign' => 'грн',
            'code' => 'UAH',
        ));
        $this->insert('{{currency}}', array(
            'id' => '2',
            'name' => 'United States dollar',
            'sign' => '$',
            'code' => 'USD',
        ));
        $this->insert('{{currency}}', array(
            'id' => '3',
            'name' => 'Euros',
            'sign' => '€',
            'code' => 'EUR',
        ));
        $this->insert('{{currency}}', array(
            'id' => '4',
            'name' => 'Russian ruble',
            'sign' => 'руб',
            'code' => 'RUB',
        ));
        $this->insert('{{currency}}', array(
            'id' => '5',
            'name' => 'Kazakhstan tenge',
            'sign' => '₸',
            'code' => 'KZT',
        ));

        try {
            $this->dropIndex('product_id', '{{shop_product_data}}');
        } catch(Exception $e) {
        }
        try {
            $this->addColumn('{{shop_product_data}}', 'currency_id', 'int(11) NOT NULL AFTER `product_id`');
        } catch(Exception $e) {
            $this->dropColumn('{{shop_product_data}}', 'currency_id');
            $this->addColumn('{{shop_product_data}}', 'currency_id', 'int(11) NOT NULL AFTER `product_id`');
        }
        $this->update('{{shop_product_data}}', array(
            'currency_id' => 1,
        ), 'currency_id = 0');
        $this->createIndex('product_id', '{{shop_product_data}}', 'currency_id, product_id', true);
    }

    protected function dropCurrencyIfExist()
    {
        if ($this->dbConnection->schema->getTable('{{currency}}')) {
            $this->dropTable('{{currency}}');
        }
    }

    public function down()
    {
        $this->dropCurrencyIfExist();
        $this->dropIndex('product_id', '{{shop_product_data}}');
        $this->dropColumn('{{shop_product_data}}', 'currency_id');
        $this->createIndex('product_id', '{{shop_product_data}}', 'product_id');
    }
}
