<?php
/**
 * Виджет, для отображения формы создания контент листа выборки группы товаров для сателлитного магазина
 * 
 * @author Sviatoslav <dev@udf.su>
 * @version 1.0
 * @package satellite.modules.shop.widgets 
 */
Yii::import('cms.widgets.page.BlockContentListManageWidget');
class ShopContentlistSelector extends BlockContentListManageWidget
{
}