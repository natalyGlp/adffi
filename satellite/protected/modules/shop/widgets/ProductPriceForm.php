<?php
class ProductPriceForm extends CWidget
{
    public $object;
    public $form;

    private $_currencies;

    public function init()
    {
        Yii::import('cms.models.currency.Currency');
        $this->_currencies = CHtml::listData(Currency::model()->findAll(), 'id', 'attributes');

        $this->registerAssets();
    }

    public function run()
    {
        $models = empty($this->object->productData) ? array(new ShopProductData()) : $this->object->productData;
        // TODO: заменить на дефолтную валюту
        if(empty($this->object->productData)) $models[0]->currency_id = 1;

        $this->render('product_prices', array(
            'models' => $models,
            'form' => $this->form,
            'currencies' => $this->_currencies,
        ));
    }

    /**
     * Рендерит дропдаун бутстрапа с возможностью выбрать
     * тип скидки/доставки — фиксированная сумма или проценты от стоимости заказа
     */
    public function renderValueTypeSelector($model, $attribute)
    {
        // TODO: заменить на виджет TbDropDownList
        // @see https://bitbucket.org/glp_NESPI/satellite/raw/fbaa5c9553201940e6221680d01ac34cef5a4a6b/protected/modules/crm/widgets/TbDropDownList.php

        $currencySymbol = $this->_currencies[$model->currency_id]['sign'];
        $currencyType = $model->{$attribute} == ShopProductData::TYPE_FIXED ? $currencySymbol : '%';

        $types = array(
            1 => array(
                'label' => $currencySymbol,
                'value' => ShopProductData::TYPE_FIXED,
                'cssClass' => 'js-currency-sign',
                ),
            2 => array(
                'label' => '%',
                'value' => ShopProductData::TYPE_PERCENT,
                'cssClass' => '',
                ),
            );
        ?>
        <div class="btn-group">
            <button class="btn dropdown-toggle" data-toggle="dropdown">
                <span class="js-label"><?php echo $currencyType ?></span> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right">
            <?php
                foreach ($types as $type) {
                    ?>
                    <li data-value="<?php echo $type['value']; ?>">
                        <a href="#" class="<?php echo $type['cssClass']; ?>"><?php echo $type['label']; ?></a>
                    </li>
                <?php
                }
            ?>
            </ul>
        </div>
        <?php
    }

    public function registerAssets()
    {
        $assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets', false, -1, YII_DEBUG);

        Yii::app()->clientScript->registerScriptFile($assetsUrl.'/js/product-sites.js');
    }
}
