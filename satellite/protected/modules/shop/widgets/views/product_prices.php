
<?php
$collapse = $this->beginWidget('bootstrap.widgets.TbCollapse', array(
    'htmlOptions' => array(
        'class' => 'accordion js-product-accordion',
        ),
));

foreach ($models as $index => $model) {
    if (!isset($currencies[$model->currency_id])) {
        continue;
    }

    $containerId = $collapse->getNextContainerId();
    $parentId = $collapse->id;
    $stateCssClass = $model->hasErrors() || $model->isNewRecord ? 'in' : '';
    $currency = $currencies[$model->currency_id];
    ?>
    <div class="accordion-group js-product-accordion-item" data-code="<?php echo $currency['code'] ?>">
        <div class="accordion-heading accordion-heading--custom-icon">
            <a href="#" class="pull-right js-remove-accordion-item"><i class="icon icon-remove"></i></a>
            <a class="accordion-toggle" data-parent="#<?php echo $parentId ?>" href="#<?php echo $containerId ?>" data-toggle="collapse">
                <?php echo $currency['name'] . ', ' . $currency['sign']; ?>
            </a>
        </div>
        <div id="<?php echo $containerId ?>" class="accordion-body collapse <?php echo $stateCssClass ?>">
            <div class="accordion-inner">
                <?php $this->render('product_price_form', array(
                    'form' => $form,
                    'model' => $model,
                    'currencySign' => $currency['sign'],
                    'index' => $index,
                )); ?>
            </div>
        </div>
    </div>
    <?php
}

$this->endWidget('bootstrap.widgets.TbCollapse');
?>

<div class="btn-group js-add-currency">
    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="icon icon-plus"></i> Добавить валюту
    </a>
    <ul class="dropdown-menu">
    <?php
    foreach ($currencies as $currency) {
        echo CHtml::tag(
            'li',
            array(),
            CHtml::link($currency['name'], '#', array(
                'data-id' => $currency['id'],
                'data-name' => $currency['name'],
                'data-sign' => $currency['sign'],
                'data-code' => $currency['code'],
            ))
        );
    }
    ?>
    </ul>
</div>

<?php
Yii::app()->clientScript
    ->registerScript(__FILE__.'#currency form and calculator', '
        $("#'.$collapse->id.'").productPriceCalculator({
            typePercent: '.ShopProductData::TYPE_PERCENT.'
        });
        $(".js-add-currency").productPrices({
            startIndex: '.$index.',
            template: $(".js-product-accordion-item")[0].outerHTML,
        });
    ')
    ;