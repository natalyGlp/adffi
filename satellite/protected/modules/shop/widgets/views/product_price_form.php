<?php
/**
 * @var ShopProductData $model
 * @var CActiveForm $form
 */

$namePrefix = "[$index]";
?>

<div class="product-price-form js-product-price-form">
    <?= $form->hiddenField($model, $namePrefix.'currency_id', array(
        'class' => 'js-product-currency',
    )); ?>

    <?php
    if ($model->hasErrors('currency_id')) {
        ?>
        <div class="error">
            <?php echo $form->error($model, 'currency_id'); ?>
        </div>
        <?php
    }
    ?>


        <div class="form-group form-group-sidebar">
            <div class="col-sm-12">
                <?php echo $form->labelEx($model, $namePrefix.'default_price'); ?>    
            </div>
            <div class="col-sm-12">
                <div class="input-group">
                    <?php echo $form->textField($model, $namePrefix.'default_price', array(
                        'class' => 'js-product-default-price form-control',
                    )); ?>
                    <span class="input-group-addon add-on js-currency-sign"><?php echo $currencySign; ?></span>    
                </div>
            </div>
            <div class="col-sm-12">
                <?php echo $form->error($model, 'default_price'); ?>
            </div>    
        </div>

        <div class="form-group form-group-sidebar">
            <div class="col-sm-12">
                <?php echo $form->labelEx($model, $namePrefix.'discount'); ?>
            </div>
            <div class="col-sm-12">
                <div class="input-group">
                    <?php echo $form->textField($model, $namePrefix.'discount', array(
                        'class' => 'js-product-discount form-control',
                    )); ?>
                    <?php echo $this->renderValueTypeSelector($model, 'discount_type', array(
                        'class' => 'input-group-addon'
                    )) ?>
                </div>
            </div>    
            <div class="col-sm-12">
                <?php echo $form->hiddenField($model, $namePrefix.'discount_type', array(
                    'class' => 'js-product-discount-type'
                )); ?>
                <?php echo $form->error($model, 'discount'); ?>
            </div>
        </div>

        <div class="form-group form-group-sidebar">
            <div class="col-sm-12">
                <?php echo $form->labelEx($model, $namePrefix.'tax'); ?>
            </div>
            <div class="col-sm-12">
                <div class="input-group">
                    <?php echo $form->textField($model, $namePrefix.'tax', array(
                        'class' => 'js-product-tax form-control',
                    )); ?>
                    <?php echo $this->renderValueTypeSelector($model, 'tax_type', array(
                        'class' => 'input-group-addon'
                    )) ?>
                </div>
            </div>    
            <div class="col-sm-12">
                <?php echo $form->hiddenField($model, $namePrefix.'tax_type', array(
                    'class' => 'js-product-tax-type'
                )); ?>
                <?php echo $form->error($model, 'tax'); ?>
            </div>
        </div>
        <div class="form-group form-group-sidebar">
            <div class="col-sm-12">
                <?php echo $form->labelEx($model, $namePrefix.'delivery_price'); ?>
            </div>
            <div class="col-sm-12">
                <div class="input-group">
                <?php echo $form->textField($model, $namePrefix.'delivery_price', array(
                    'class' => 'js-product-delivery-price form-control',
                )); ?>
                <?php echo $this->renderValueTypeSelector($model, 'delivery_price_type') ?>
                </div>
            </div>    
            <div class="col-sm-12">
        <?php echo $form->hiddenField($model, $namePrefix.'delivery_price_type', array(
            'class' => 'js-product-delivery-price-type'
        )); ?>
            </div>
            <div class="col-sm-12">
                <?php echo $form->error($model, 'delivery_price'); ?>
            </div>
        </div>
    
        <div class="form-group form-group-sidebar">
            <div class="col-sm-12">
                <?php echo $form->labelEx($model, $namePrefix.'custom_price'); ?>
            </div>
            <div class="col-sm-12">
                <div class="input-group">
                    <?php echo $form->textField($model, $namePrefix.'custom_price', array(
                        'class' => 'js-product-custom-price form-control',
                    )); ?>
                    <span class="input-group-addon add-on js-currency-sign"><?php echo $currencySign; ?></span>
                </div>
            </div>
            <div class="col-sm-12">
                <?php echo $form->error($model, 'custom_price'); ?>
            </div>
        </div>
        <p class="text-info"><?php echo t('Set custom price to <b>overwrite</b> all taxes and discounts'); ?></p>

        <div class="f_success form-group form-group-sidebar">
            <div class="col-sm-12">
                <label for="product-price"><?php echo t('Price shown to a client'); ?></label>
            </div>
            <div class="col-sm-12">
                <div class="input-group">
                    <input type="text" class="js-product-price form-control" disabled>
                    <span class="input-group-addon add-on js-currency-sign"><?php echo $currencySign; ?></span>    
                </div>
                
            </div>
        </div>
</div>
