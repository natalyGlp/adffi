<?php
$siteRowTplId = $this->id . '-site-row-tpl';
$selectorId = $this->id . '-selector';
?>
<div class="row-fluid product-sites-selector js-product-sites-selector">
    <div class="span4">
        <?php
            $this->widget('ext.nespi.SiteSelector', array(
                'model' => $model,
                'attribute' => 'productSites',
                'id' => $selectorId,
            ));
        ?>
    </div>
    <div class="span8">
        <table class="table">
            <thead>
                <tr>
                    <th>Сайт</th>
                    <th><?php echo $model->getAttributeLabel('isFeaturedOnSites') ?></th>
                    <th>Валюты</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="js-selected-sites">
            </tbody>
        </table>
    </div>
</div>

<script type="text/template" id="<?php echo $siteRowTplId; ?>">
    <td width="20%" class="js-site-url"></td>
    <td width="10%">
    <?php echo $form->checkBox($model, 'isFeaturedOnSites', array(
        'uncheckValue' => null,
        'class' => 'js-site-featured',
    )) ?>
    </td>
    <td width="100%">
        <?php echo $form->hiddenField($model, 'siteCurrencies', array(
            'class' => 'js-site-currencies',
            'value' => '',
            'placeholder' => 'Введите код валюты'
        )) ?>
    </td>
    <td><a href="#"><i class="icon-remove js-remove-site"></i></a></td>
</script>

<?php
Yii::app()->clientScript
    ->registerScript(__FILE__, '
        var $siteSelectField = $("#'.$selectorId.'");
        var $container = $siteSelectField.closest(".row");
        $container.insertBefore($container.prev()); // между object_slug и object_content

        $siteSelectField.productSiteSelector({
            selectedSitesContainer: $(".js-selected-sites"),
            template: $("#'.$siteRowTplId.'").html(),
            featured: '.CJavaScript::encode($model->isFeaturedOnSites).',
            currencies: '.CJavaScript::encode($model->siteCurrencies).'
        });
    ')
    ;
