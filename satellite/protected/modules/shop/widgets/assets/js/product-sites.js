(function($) {
    // TODO: http://rivetsjs.com and backbone
    'use strict';

    $.productPrices = {
        currenciesAvailable: [],
    };

    $.fn.productPrices = function(options) {
        var i = options.startIndex;
        var template = options.template;

        this.on("click", ".dropdown-menu a", function(event) {
            event.preventDefault();

            i++;

            var $form = $(template);

            changeFieldsIndex($form, i);

            $form.find("input").val("");

            setupAccordion($form, i);

            clearValidationErrors($form);

            setCurrency($form, {
                id: $(this).data("id"),
                name: $(this).data("name"),
                code: $(this).data("code"),
                sign: $(this).data("sign")
            });

            $(".js-product-accordion").append($form);

            // ресетим дропдауны, что бы заменить символ валюты (если были выбраны не проценты)
            $(".dropdown-menu li:last-child a", $form).click();

            updateCurrenciesList();
        });

        $("body").on("click", ".js-remove-accordion-item", function(event) {
            event.preventDefault();

            $(this).parents(".js-product-accordion-item").remove();

            updateCurrenciesList();
        });

        updateCurrenciesList();

        function changeFieldsIndex($form, i)
        {
            var formHtml = $form.html();
            formHtml = formHtml.replace(/\[\d+\]/g, "["+i+"]");
            formHtml = formHtml.replace(/_\d+_/g, "_"+i+"_");
            $form.html(formHtml);
        }

        function setupAccordion($form, i)
        {
            var acToggle = $form.find(".accordion-toggle")[0];
            acToggle.href = acToggle.hash.replace(/\d+$/, i);
            $form.find(".accordion-body")
                .addClass("in")
                .css("height", "auto")
                .attr("id", acToggle.hash.slice(1))
                ;
        }

        function clearValidationErrors($form)
        {
            $form.find(".errorMessage").html("");
            $form.find(".error").removeClass("error");
        }

        function setCurrency($form, currency)
        {
            var $accordionTitle = $(".accordion-toggle", $form);
            var $currencyField = $(".js-product-currency", $form);
            var $currencySigns = $(".js-currency-sign", $form);
            $accordionTitle.html(currency.name + ", " + currency.sign);
            $currencySigns.html(currency.sign);
            $currencyField.val(currency.id);

            $form.data(currency);
        }

        function updateCurrenciesList()
        {
            $.productPrices.currenciesAvailable = [];
            $(".js-add-currency .dropdown-menu a").each(function() {
                var currencyId = $(this).data("id");

                toggleCurrencyAvailability($(this), currencyId);
            });

            $($.productPrices).trigger("currencyChange");
        }

        function toggleCurrencyAvailability($dropdownLink, currencyId)
        {
            if($(".js-product-currency[value="+currencyId+"]").length) {
                $dropdownLink.hide();
                $.productPrices.currenciesAvailable.push($dropdownLink.data("code"));
            } else {
                $dropdownLink.show();
            }
        }
    };

    $.fn.productPriceCalculator = function(options) {
        var typePrecent = options.typePrecent;

        this
            .on("click", ".dropdown-menu li", function(event) {
                event.preventDefault();

                // смена типа параметра: фиксированная сумма/проценты
                btDropdownSelect($(this).closest(".row-fluid"), $(this));

                $(this).parents(".js-product-price-form").trigger("calculate");
            })
            .on("change keyup", "input, select", calculate)
            .on("calculate", ".js-product-price-form", calculate)
            ;

        function btDropdownSelect($container, $selectedItem)
        {
            $container.find("[data-toggle=dropdown] .js-label").html($selectedItem.text());
            $container.find("input[type=hidden]").val($selectedItem.data("value"));
        }

        function wantToSell(price) {
            price *= 1;

            var modifiers = {};

            function get(key) {
                return modifiers[key]*1 ? modifiers[key]*1 : 0;
            }

            function saveAmount(obj)
            {
                if(typeof obj.amount == "number") {
                    modifiers[obj.what] = obj.amount;
                }
            }

            function _with(what)
            {
                saveAmount(this);
                this.amount = null;
                this.type = 1;
                this.what = what;

                return this;
            }

            function ofAmount(amount)
            {
                this.amount = amount*1;

                if(this.what == "discount") {
                    this.amount = -this.amount;
                }

                return this;
            }

            function ofType(type)
            {
                this.type = type;
                if (type == typePrecent) {
                    // Проценты вешаются на результирующую на данный момент сумму
                    this.amount = this.getPrice(true) * this.amount / 100;
                }

                return this;
            }

            function getPrice(doNotSave)
            {
                if(!doNotSave) {
                    saveAmount(this);
                }

                var currentPrice = price + get("discount") + get("tax") + get("delivery");

                return currentPrice.toFixed(2);
            }
            return {
                with: _with,
                ofType: ofType,
                ofAmount: ofAmount,
                getPrice: getPrice,
            };
        }

        function calculate()
        {
            var $form = $(this);
            if (!$form.hasClass('js-product-price-form')) {
                $form = $(this).parents(".js-product-price-form");
            }

            var calculator = $form.data("calculator") || createCalculator($form);
            $form.data("calculator", calculator);

            calculator();
        }

        function createCalculator(container)
        {
            var $defaultPrice = $(".js-product-default-price", container),
                $discount = $(".js-product-discount", container),
                $discountType = $(".js-product-discount-type", container),
                $tax = $(".js-product-tax", container),
                $taxType = $(".js-product-tax-type", container),
                $delivery = $(".js-product-delivery-price", container),
                $deliveryType = $(".js-product-delivery-price-type", container),
                $customPrice = $(".js-product-custom-price", container),
                $price = $(".js-product-price", container)
                ;

            return function() {
                if($customPrice.val() !== "") {
                    $price.val($customPrice.val());
                    return;
                }

                $price.val(
                    wantToSell($defaultPrice.val())
                        .with("discount").ofAmount($discount.val()).ofType($discountType.val())
                        .with("tax").ofAmount($tax.val()).ofType($taxType.val())
                        .with("delivery").ofAmount($delivery.val()).ofType($deliveryType.val())
                        .getPrice()
                    );
            };
        }

        $(".js-product-price-form").trigger("calculate");
    };

    (function() {
        var $siteSelectField;
        var $selectedSitesContainer;

        $.fn.productSiteSelector = function(options) {
            $siteSelectField = this;
            $selectedSitesContainer = options.selectedSitesContainer;

            Row.prototype.template = options.template;

            var collection = new RowCollection();
            collection.init({
                featured: options.featured,
                currencies: options.currencies
            });
        };

        function RowCollection() {}
        RowCollection.prototype = $.extend(RowCollection.prototype, {
            init: function(data) {
                this._rows = {};
                this.initial = {
                    featured: data.featured,
                    currencies: data.currencies,
                };

                $siteSelectField.change($.proxy(function() {
                    this.removeAll();

                    this.syncWithSourceSelectField();
                }, this)).change();
            },

            syncWithSourceSelectField: function() {
                var initialFeatured = this.initial.featured;
                var initialCurrencies = this.initial.currencies;
                var that = this;

                $siteSelectField.find(":selected").each(function() {
                    var id = $(this).val();
                    var url = $(this).text();

                    if(!that.hasRow(id)) {
                        that.createRow({
                            id: id,
                            url: url,
                            featured: initialFeatured && initialFeatured[id] == "1",
                            currencies: initialCurrencies && initialCurrencies[id] || '',
                        });
                    }

                    that.appendRow(id);
                });

                if(initialFeatured) {
                    initialFeatured = null;
                }
                if(initialCurrencies) {
                    initialCurrencies = null;
                }
            },

            createRow: function (data) {
                if(this.hasRow(data.id)) {
                    return;
                }

                this._rows[data.id] = new Row(data);
            },

            appendRow: function(id) {
                if(!this.hasRow(id)) {
                    throw new Error("There is no row with id "+id);
                }

                this._rows[id].render();

                $selectedSitesContainer.append(this._rows[id].$el);
            },

            hasRow: function(id) {
                return !!this._rows[id];
            },

            removeAll: function() {
                $.each(this._rows, function() {
                    this.remove();
                });
            }
        });

        function Row(data) {
            this.data = data;
            this.el = document.createElement('tr');
            this.$el = $(this.el);
        }

        Row.prototype = $.extend(Row.prototype, {
            render: function() {
                this.$el.html(this.template);

                correctFieldNames.call(this);
                this.$el.find('.js-site-currencies').select2({
                    createSearchChoice: function() { return null; },
                    tags: this.getAvailableCurrencies(),
                    width: '100%'
                });

                setEvents.call(this);

                this.insertData();

                return this;
            },

            remove: function() {
                this.$el.off();
                this.$el.remove();
            },

            getAvailableCurrencies: function() {
                return $.productPrices.currenciesAvailable;
            },

            insertData: function() {
                this.$el.find(".js-site-url").text(this.data.url);
                this.$el.find(".js-site-featured").attr("checked", this.data.featured);
                this.$el.find(".js-site-currencies")
                    .val(this.data.currencies)
                    .select2('val', this.data.currencies.split(","))
                    ;
            },

            loadData: function() {
                this.data.url = this.$el.find(".js-site-url").text();
                this.data.featured = this.$el.find(".js-site-featured").is(":checked");
                this.data.currencies = this.$el.find(".js-site-currencies").val();
            }
        });

        function setEvents() {
            this.$el
                .on("click", ".js-remove-site", $.proxy(function(event) {
                    event.preventDefault();

                    this.remove();
                    $siteSelectField.multiSelect("deselect", this.data.id);
                }, this))
                .on("change", $.proxy(function(event) {
                    this.loadData();
                }, this))
                ;

            $($.productPrices).on("currencyChange", $.proxy(this.render, this));
        }

        function correctFieldNames() {
            appendIndexToName(this.$el.find(".js-site-featured"), this.data.id);
            appendIndexToName(this.$el.find(".js-site-currencies"), this.data.id);
        }

        function appendIndexToName($field, index) {
            $field.attr("name", $field.attr("name")+"["+index+"]");
        }
    }());
}(jQuery));
