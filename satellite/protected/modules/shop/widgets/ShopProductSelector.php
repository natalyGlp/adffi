<?php
/**
 * Виджет, подсказывающий товары магазина по их id/названию
 * Этот виджет в первую очередь предназначен для использования в настройках тем
 * 
 * @author Sviatoslav <dev@udf.su>
 * @version 1.0
 * @package satellite.modules.shop.widgets 
 */
Yii::import('zii.widgets.jui.CJuiAutoComplete');
class ShopProductSelector extends CJuiAutoComplete
{
	public function init()
	{
		$this->sourceUrl = Yii::app()->createUrl('shop/default/acprod');
		parent::init();
	}

	/**
	 * @note js скопирован из вьюхи create.php цшвпуе CreateOrderWidget
	 */
	public function run()
	{
		parent::run();
		list($name,$id)=$this->resolveNameID();
		Yii::app()->clientScript->registerScript(__FILE__, '

			window.getProductTitle = function(data) {
				var productTitle = '. CJavaScript::encode('
										<div style="overflow:auto">
											<span style="float:left;margin-right:10px;">{thumb}</span>
											<b>{name}<span class="edited" style="display:none;">*</span></b>
											<br />
											<span style="color:#666666;font-size:10px;">{prodId}</span>
										</div>').';
				productTitle = productTitle.replace(/\{name\}/g, data.name);
				productTitle = productTitle.replace(/\{thumb\}/g, data.thumb);
				productTitle = productTitle.replace(/\{prodId\}/g, data.prodId);

				return productTitle;
			}
			$("#'.$id.'").data( "autocomplete" )._renderItem = function( ul, item ) {
				return $( "<li></li>" )
					.data( "item.autocomplete", item )
					.append( "<a>" + getProductTitle(item) + "</a>" )
					.appendTo( ul );
			};
		');
	}
}