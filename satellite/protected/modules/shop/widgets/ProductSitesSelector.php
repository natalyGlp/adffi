<?php

class ProductSitesSelector extends CWidget
{
    public $object;
    public $form;

    public function init()
    {
        if (!isset($this->object) || !($this->object instanceof Object)) {
            throw new CException(get_class($this).'::object is required and should be instance of Object class');
        }

        if (!isset($this->form)) {
            throw new CException(get_class($this).'::form is required');
        }

        $this->registerAssets();
    }

    public function run()
    {
        $this->object->fetchProductSites();

        $this->render('product_sites_selector', array(
            'model' => $this->object,
            'form' => $this->form,
            ));
    }

    public function registerAssets()
    {
        $assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets', false, -1, YII_DEBUG);

        Yii::app()->clientScript
            ->registerCssFile($assetsUrl.'/css/product-sites-selector.css')
            ->registerScriptFile($assetsUrl.'/js/product-sites.js')
        ;

        Bootstrap::getBooster()->registerPackage('select2');
    }
}
