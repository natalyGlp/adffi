<?php
/**
 * Контроллер для управления заказами супер магазина
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package modules.shop.controllers
 */
Yii::import('cms.controllers.BeorderController');
Yii::import('models.Sites');

class DefaultController extends BeorderController
{
    /**
     * @return array action filters
     */

    public function filters()
    {
        return array(
            array('auth.filters.AuthFilter'),
        );
    }

    //public $layout='//layouts/column2';
    public $layout='//layouts/glp1';
	public $pageTitle = 'Управление заказами';
	public function getViewFile($view) 
	{
		$parentId = get_parent_class($this);
		$parentId = str_replace('controller', '', strtolower($parentId));
        $view = str_replace('.'.$this->id.'.', '.'.$parentId.'.', $view);
		return parent::getViewFile($view);
	}

    public function actionTest(){
        $model = Sites::model()->find(
                  array(
                      'condition' => 'id = :id',
                      'params'    => array(':id' => 70)
                  )
              );
        echo '<script type="text/javascript" src="http://cpa.studiobanzai.com/js/tracker.js"></script>
<script type="text/javascript">
        var cpaTracker = new CpaTracker();
        cpaTracker.setAccount("Mg==");
        var sale = cpaTracker.generateSale();
        sale.setOrderID("2997");
        sale.setProductID("Test Product");
        cpaTracker.register();
</script>';
        echo "112cgfd = ".print_r($model->api_key,true);
        //$this->render('settings');
    }

    public function actionCreate()
    {
        $this->render('create');
        //$this->renderPartial('create', null, false, true);
    }
    public function actionUpdate()
    {
        $this->render('create');
        //$this->renderPartial('create', null, false, true);
    }
}