<?php
/**
 * Контроллер для управления терминами таксономии
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package modules.shop.controllers
 */
Yii::import('cms.controllers.BetermController');
class TermController extends BetermController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            array('auth.filters.AuthFilter'),
        );
    }

    //public $layout='//layouts/column2';
    public $layout='//layouts/glp1';
	public function getViewFile($view) 
	{
		$parentId = get_parent_class($this);
		$parentId = str_replace('controller', '', strtolower($parentId));
		$view = str_replace('.'.$this->id.'.', '.'.$parentId.'.', $view);
		return parent::getViewFile($view);
	}
}