<?php
/**
 * Контроллер для управления товарами супер магазина
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package modules.shop.controllers
 */
Yii::import('cms.controllers.BeobjectController');
class ProductController extends BeobjectController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            array('auth.filters.AuthFilter'),
        );
    }

    
    //public $layout='//layouts/column2';
    public $layout='//layouts/glp1';
	public $defaultAction = 'admin';
	public $page; // @see ProductController::init()

	public function init()
	{
		// а сделаем ка мы вид, что front_layouts находится у нас в модуле
		Yii::setPathOfAlias('common.front_layouts.nespi.content_type.catalog.object_form_widget', Yii::getPathOfAlias('application.modules.shop.views.product.object_form_widget'));
		Yii::setPathOfAlias('common.front_layouts.nespi.content_type.catalog.CatalogObject', Yii::getPathOfAlias('application.modules.shop.models.CatalogObject'));
		$this->page = (object) array(
			'layout' => 'nespi'
			);

		parent::init();
	}

	public function getViewFile($view) 
	{
		$parentId = get_parent_class($this);
		$parentId = str_replace('controller', '', strtolower($parentId));
        $view = str_replace('.'.$this->id.'.', '.'.$parentId.'.', $view);
		return parent::getViewFile($view);
	}

	/**
	 * Хардкодим фильтрацию по нужному контент типу
	 */
    public function actionAdmin($type='')
    {       
    	parent::actionAdmin('catalog');
    }
}