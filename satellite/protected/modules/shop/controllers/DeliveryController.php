<?php
Yii::import('cms.models.order.OrderType');

class DeliveryController extends Controller
{
    /**
     * Actions for CRUD можно использовать повторно
     * @return array
     * modelName Название модели, с которой работает action
     * redirectTo масив для редиректа на нужную страницу можно упустить(по дефолту array('index'))
     */
    public $layout='//layouts/glp1';
    public function actions()
    {
        return array(
            'create' => array(
                'class'=>'shop.extensions.actions.CreateAction',
                'modelName' => 'OrderType',
            ),
            'delete' => array(
                'class'=>'shop.extensions.actions.DeleteAction',
                'modelName' => 'OrderType',
            ),
            'update' => array(
                'class'=>'shop.extensions.actions.UpdateAction',
                'modelName' => 'OrderType',
            ),
        );
    }

    /**
     * action for Administration currencies
     */
    public function actionIndex()
    {
        $model=new OrderType('search');
        $model->unsetAttributes();
        if(isset($_GET['OrderType']))
            $model->attributes=$_GET['OrderType'];

        $this->render('index',array(
            'model'=>$model,
        ));
    }
}