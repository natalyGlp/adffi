<?php
/**
 * API с которым будут связываться лендинги
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package modules.shop.controllers
 */
Yii::import('cms.models.order.Order');
//Yii::import('cms.models.order.Order');

class ApiController extends Controller
{
    /**
     * API у нас общедоступная, во всяком случае на данный момент
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    public function actionIndex()
    {
        Yii::setPathOfAlias('cms.content_type.catalog.CatalogObject', Yii::getPathOfAlias('application.modules.shop.models.CatalogObject'));
       
        if (isset($_POST['orderData'])) {
            $this->issueOrder($_POST['orderData']);
        } elseif ($this->isRequestForProducts) {
            $shopSatellite = new \shop\components\ShopSatellite($this->referrerHost);
            $shopSatellite->setOptions(array(
                'featured' => $this->isRequestForFeatured,
            ));
            $storeData = $shopSatellite->getStoreData();

            if (!$storeData) {
                throw new CHttpException(404, "No data available");
                Yii::app()->end();
            }
        //    YII::log("save NATAly test for api".print_r($storeData,true));
            $this->renderPartial('json', array(
                'data' => $storeData,
                'callback' => isset($_GET['callback']) ? $_GET['callback'] : null,
            ));
        }
    }

    public function actionIntegration()
    {
        //получить данные 
        $orderData = CMap::mergeArray(array(
            // дефолтные значения
            'name' => '',
            'product'=> '',
            'country' => 'UA',
            'city' => '',
            'street' => '',
            'house' => '',
            'flat' => '',
            'phone' => '111',
            'domain' => '',
            'email' => '',
            'zip' => '',
            'delivery' => '',
            'amount' => 1,
            'currencyProvider' => 1,
            'comment' => '',
            'currency' => 'UAH',
        ), $_REQUEST); 

        $orderData['currency_id'] = Currency::model()->findByAttributes(array(
            'code' => $orderData['currency'],
        ));
        if ($orderData['currency_id']) {
            $orderData['currency_id'] = $orderData['currency_id']->primaryKey;
        } else {
            throw new CException('Wrong currency code: ' . $orderData['currency']);
        }
        $product = array('id' => $orderData['product'], 'amount'=>1 );
        $orderData['products'] = array('0' => array('id' => $orderData['product'], 'amount'=>1 ) );
         //ToDo проверить есть ли такая запись?
        //begin ToDo
        $ord = Order::model()->findByAttributes(array(
            'order_id' => $orderData['order_id'],
            'api_client' => $orderData['api_client'],
        ));
        if(isset($ord)){
            // Для LeadVertex
            if(strpos($orderData['api_client'], 'LeadVertex')===false){
                //Нужно обновить статус
                $ord->status = $orderData['status'];
                $ord->comment = $orderData['comment'];
                $ord->update();
                
                return  "OK";
            }else{
                $st = array('spam'=>4,'return'=>4,'canceled'=>4,'processing'=>1,'accepted'=>5,'paid'=>5);
                if($orderData['status']!==null){
                    Yii::log("sts = ".$orderData['status'].print_r($orderData['status'],true));
                    $var = $orderData['status'];
                    Yii::log("Check st = ".print_r($orderData['status'],true));
                    $ord->status = $st["$var"];
                    $ord->comment = $orderData['comment'];
                    $ord->update();
                  
                    return  "OK";
                }
            }
            //end ToDo
        }
        else
        {

            $orderCrud = new OrderCrud();
            $orderCrud->throwExceptions = false;
            $orderCheck = array_map(function($product) {
                return array(
                    'prod_id' => $product['id'],
                    'quantity' => $product['amount'],
                );
            }, $orderData['products']);

            $orderCrud->setData(array(
                'Order' => array(
                    'user_id' => new CDbExpression('NULL'),
                    'type' => isset($orderData['delivery']) && $orderData['delivery'] > 0 ? $orderData['delivery'] : 3, // доставка
                    'currency_provider' => $orderData['currencyProvider'], // серевис через который был оплачен заказ
                    'currency_id' => $orderData['currency_id'],
                    'ip' => $orderData['domain'],
                    'referer' => $orderData['domain'],
                    'comment' => $orderData['comment'],
                    'order_id' => $orderData['order_id'],
                    'api_client' => $orderData['api_client'],
                    'status' => $orderData['status'],

                ),
                'Client' => array(
                    'first_name' => $orderData['name'],
                    'last_name' => new CDbExpression('NULL'),
                    'email' => $orderData['email'],
                ),
                'OrderAddress' => array(
                    'user_id' => new CDbExpression('NULL'),
                    'country' => $orderData['country'],
                    'city' => $orderData['city'],
                    'street' => (!empty($orderData['zip']) ? $orderData['zip'] . ', ' : '') . $orderData['street'],
                    'house' => $orderData['house'],
                    'flat' => $orderData['flat'],
                    'telephone' => $orderData['phone'],
                ),
                'OrderCheck' => $orderCheck,//array($orderData['product'] => 1 ),
            ));
            $orderModel = null;
            echo " == ".print_r($orderCrud,true);
            if ($orderCrud->save()) {
                // Отправляем алерты на мыло
                $this->notifyProductOwners($orderData);
                $orderModel = $orderCrud->getModel('order');
            }
            $this->responseOrderStatus($orderModel);
        }
    }

    public function actionUpdate()
    {
        //получить данные 
        $orderData = CMap::mergeArray(array(
            // дефолтные значения
            'name' => '',
            'product'=> '',
            'country' => 'UA',
            'city' => '',
            'street' => '',
            'house' => '',
            'flat' => '',
            'phone' => '111',
            'domain' => '',
            'email' => '',
            'zip' => '',
            'delivery' => '',
            'amount' => 1,
            'currencyProvider' => 1,
            'comment' => '',
            'currency' => 'UAH',
        ), $_REQUEST); 

        $orderData['currency_id'] = Currency::model()->findByAttributes(array(
            'code' => $orderData['currency'],
        ));
        if ($orderData['currency_id']) {
            $orderData['currency_id'] = $orderData['currency_id']->primaryKey;
        } else {
            throw new CException('Wrong currency code: ' . $orderData['currency']);
        }
        $product = array('id' => $orderData['product'], 'amount'=>1 );
        $orderData['products'] = array('0' => array('id' => $orderData['product'], 'amount'=>1 ) );
       
        $orderCrud = new OrderCrud();
        $orderCrud->throwExceptions = false;
        $orderCheck = array_map(function($product) {
            return array(
                'prod_id' => $product['id'],
                'quantity' => $product['amount'],
            );
        }, $orderData['products']);

        $orderCrud->setData(array(
            'Order' => array(
                'user_id' => new CDbExpression('NULL'),
                'type' => isset($orderData['delivery']) && $orderData['delivery'] > 0 ? $orderData['delivery'] : 3, // доставка
                'currency_provider' => $orderData['currencyProvider'], // серевис через который был оплачен заказ
                'currency_id' => $orderData['currency_id'],
                'ip' => $orderData['domain'],
                'referer' => $orderData['domain'],
                'comment' => $orderData['comment'],
                'order_id' => $orderData['order_id'],
                'api_client' => $orderData['api_client'],
                'status' => $orderData['status'],

            ),
            'Client' => array(
                'first_name' => $orderData['name'],
                'last_name' => new CDbExpression('NULL'),
                'email' => $orderData['email'],
            ),
            'OrderAddress' => array(
                'user_id' => new CDbExpression('NULL'),
                'country' => $orderData['country'],
                'city' => $orderData['city'],
                'street' => (!empty($orderData['zip']) ? $orderData['zip'] . ', ' : '') . $orderData['street'],
                'house' => $orderData['house'],
                'flat' => $orderData['flat'],
                'telephone' => $orderData['phone'],
            ),
            'OrderCheck' => $orderCheck,//array($orderData['product'] => 1 ),
        ));
        $orderModel = null;
        echo " == ".print_r($orderCrud,true);
        if ($orderCrud->save()) {
            // Отправляем алерты на мыло
            $this->notifyProductOwners($orderData);
            $orderModel = $orderCrud->getModel('order');
        }
        $this->responseOrderStatus($orderModel);
     
    }
    

    protected function issueOrder($postOrderData)
    {
        // TODO: предусмотреть дополнительную защиту с помощью md5 подписей
        // TODO: проверить может ли с этого URL продаваться продукт
        $orderData = CMap::mergeArray(array(
            // дефолтные значения
            'name' => '',
            'country' => 'UA',
            'city' => '',
            'street' => '',
            'house' => '',
            'flat' => '',
            'phone' => '',
            'email' => '',
            'zip' => '',
            'delivery' => '',
            'amount' => 1,
            'currencyProvider' => 1,
            'comment' => '',
            'currency' => 'UAH',
        ), $postOrderData);

        $orderData['currency_id'] = Currency::model()->findByAttributes(array(
            'code' => $orderData['currency'],
        ));
        if ($orderData['currency_id']) {
            $orderData['currency_id'] = $orderData['currency_id']->primaryKey;
        } else {
            throw new CException('Wrong currency code: ' . $orderData['currency']);
        }

        $orderCrud = new OrderCrud();
        $orderCrud->throwExceptions = false;
        $orderCheck = array_map(function($product) {
            return array(
                'prod_id' => $product['id'],
                'quantity' => $product['amount'],
            );
        }, $orderData['products']);
        $orderCrud->setData(array(
            'Order' => array(
                'user_id' => new CDbExpression('NULL'),
                'type' => isset($orderData['delivery']) && $orderData['delivery'] > 0 ? $orderData['delivery'] : 3, // доставка
                'currency_provider' => $orderData['currencyProvider'], // серевис через который был оплачен заказ
                'currency_id' => $orderData['currency_id'],
                'ip' => $orderData['ip'],
                'comment' => $orderData['comment'],
            ),
            'Client' => array(
                'first_name' => $orderData['name'],
                'last_name' => new CDbExpression('NULL'),
                'email' => $orderData['email'],
            ),
            'OrderAddress' => array(
                'user_id' => new CDbExpression('NULL'),
                'country' => $orderData['country'],
                'city' => $orderData['city'],
                'street' => (!empty($orderData['zip']) ? $orderData['zip'] . ', ' : '') . $orderData['street'],
                'house' => $orderData['house'],
                'flat' => $orderData['flat'],
                'telephone' => $orderData['phone'],
            ),
            'OrderCheck' => $orderCheck,
        ));

        $orderModel = null;
        if ($orderCrud->save()) {
            // Отправляем алерты на мыло
            $this->notifyProductOwners($orderData);
            $orderModel = $orderCrud->getModel('order');
        }

        $this->responseOrderStatus($orderModel);
    }

    protected function notifyProductOwners($orderData)
    {
        $productIds = array_values(CHtml::listData($orderData['products'], 'id', 'id'));
        $products = CatalogObject::model()->findAllByAttributes(array('object_id' => $productIds));
        $ownerIds = array_values(CHtml::listData($products, 'object_author', 'object_author'));

        $ownerIds = CMap::mergeArray(
            $ownerIds,
            Yii::app()->db->createCommand()
                ->select('t.id_user')
                ->from('{{user_sites}} t')
                ->leftJoin(ShopProductSites::model()->tableName() . ' shopProd', 't.id_site = shopProd.site_id')
                ->where('shopProd.product_id IN (' . implode(',', $productIds) . ')')
                ->queryColumn()
        );

        array_push($ownerIds, 1); // root admin

        $detailUrl = Yii::app()->createAbsoluteUrl('shop/default/index');
        $subject = 'Новый заказ с ' . $this->referrerHost;
        $message = 'Для просмотра деталей заказа перейдите по ссылке ' . CHtml::link($detailUrl, $detailUrl);

        $users = User::model()->findAllByPk($ownerIds);
        foreach ($users as $user) {
            $user->sendEmail($subject, $message);
        }
    }
//тут отдача ордер айди
    protected function responseOrderStatus($orderModel)
    {
        $data = array('orderStatus' => 'fail');
        if ($orderModel) {
            //TODO: отправить productId
            $data['productId'] = $orderModel->getTotalProduct();
            $data['orderId'] = $orderModel->primaryKey;
            $data['orderPrice'] = $orderModel->getTotalPrice(false, false);
            $data['orderCurrency'] = $orderModel->currency->code;
            $data['orderStatus'] = 'success';
        }

        $this->renderPartial('json', array(
            'data' => $data,
        ));
    }

    protected function getIsRequestForProducts()
    {
        return !Yii::app()->request->isPostRequest && isset($_GET['getProducts']);
    }

    protected function getIsRequestForFeatured()
    {
        return isset($_GET['featured']) && $_GET['featured'] != '0';
    }

    protected function getReferrerHost()
    {
        return isset($_GET['for']) ? $_GET['for'] : Yii::app()->request->urlReferrer;
    }

    protected function log($message, $level = 'info')
    {
        Yii::log($message, $level, get_class($this));
    }

    public function actionChangeOrderStatus()
    {
        
        $flag = false;
        $api_key = Yii::app()->getRequest()->getParam('api_key');
        if(Yii::app()->getRequest()->getIsPostRequest()){
            $order_id = Yii::app()->getRequest()->getParam('order_id');
            $status = Yii::app()->getRequest()->getParam('status');
        }
        $users = $this->getAuthorized($api_key);

        $info_status = (isset($status)) ? $status : "no data";
        $info_order_id = (isset($order_id)) ? $order_id : "no data";
        $info_api_key = (isset($api_key)) ? $api_key : "no data";
        $ip = (isset(Yii::app()->request->userHostAddress)) ? Yii::app()->request->userHostAddress : "no data";
       
        $message = "action: ChangeOrderStatus; api_key=".$info_api_key."; order_id=".$info_order_id."; status=".$info_status." from ip: $ip ";
        if(isset($users)){
            $sites = $users->getSites();
            if(isset($order_id))
                $order = Order::model()->findByAttributes(array('id' => $order_id));
            if((isset($order)) && (in_array($order->referer, $sites)))
                $flag=true;
            if($flag & isset($status)){
                if($order->status=='4'){
                    $data['info']="status can not be change from cancel to another ";
                    $data['result'] ='false';
                //    $data['result'] ='false';
                }else{
                    $order->status = $status;
                    $order->comment = (Yii::app()->getRequest()->getParam('status')) ? Yii::app()->getRequest()->getParam('status') : "";
                    $order->update();
                    $data['info']="status succesfully changed ";
                    $data['result'] ='true';
                }

            }
        }
        else
        {  
            Yii::log("ERROR 401 ".$message);
            // return http_response_code(401); 
            return header("HTTP/1.0 404 Unauthorized");
        }
        if(!isset($data)){
            Yii::log("ERROR 400 ".$message);
            $data['result']='error';
            // http_response_code(400);
            header("HTTP/1.0 400 Bad Request");
        }
        Yii::log("SUCCESS 200 ".$message.print_r($data,true));
        $this->renderPartial('json', array(
            'data' => $data,
        ));
    }
    
    protected function getAuthorized($api_key)
    {
        $users = User::model()->with('sites')->find('MD5(email)=:email', array(':email'=>$api_key));
        return $users;
    }

    public function actionGetOrders()
    {
        //key of offers md5 of email of users
        Yii::import('cms.models.order.*');
        Yii::import('cms.models.object.*');

        if((!Yii::app()->getRequest()->getIsPostRequest()) and (!Yii::app()->getRequest()->getIsPutRequest())){
            $api_key = Yii::app()->getRequest()->getParam('api_key');
            $order_id = Yii::app()->getRequest()->getParam('order_id');
        }
        else
        {
            Yii::log("ERROR 400 ");
            // http_response_code(400);
            return header("HTTP/1.0 400 Bad Request");
        }
        $flag = false;
    
        $info_order_id = (isset($order_id)) ? $order_id : "no data";
        $info_api_key = (isset($api_key)) ? $api_key : "no data";
       
        $message = "action: ChangeOrderStatus; api_key=".$info_api_key."; order_id=".$info_order_id." from ip: ".Yii::app()->request->userHostAddress;
        $users = User::model()->with('sites')->find('MD5(email)=:email', array(':email'=>$api_key));
        
        if(isset($users)){ //одновремнно отсекли неизвестные/ но не доконца
            $sites = $users->getSites();
            foreach ($sites as $key => $value) {
                if(!isset($order_id))
                    $orders = Order::model()->findAll('referer=:referer', array(':referer'=>$value));
                //TOD if issset
                if(isset($orders)){
                    foreach ($orders as $order) {
                        $data['orders'][$order->id] = array(
                            'total_price'=>$order->getTotalPrice(false, false),
                            'currency'=>$order->currency->code,
                            'status'=>$order->status,
                            'comment'=>$order->comment,
                            'date'=>$order->date,
                            'ip'=>$order->ip,
                            'product_id'=>$order->getTotalProduct(),
                            'adress_city'=>$order->address->city,
                            'adress_street'=>$order->address->street,
                            'adress_country'=>$order->address->country,
                            'adress_house'=>$order->address->house,
                            'adress_flat'=>$order->address->flat,
                            'adress_phone'=>$order->address->telephone,
                            'client_firstName'=>$order->client->first_name,
                            'client_lastName'=>$order->client->last_name,
                            'client_email'=>$order->client->email,
                            'delivery'=>$order->type,
                            'land'=>$order->referer,

                        );
                    }
                }
            }
            if(isset($order_id))
                $order = Order::model()->findByPk($order_id);
            if((isset($order)) && (in_array($order->referer, $sites)))
                $flag=true;
            if($flag){
                $data['orders'][$order->id] = array(
                    'total_price'=>$order->getTotalPrice(false, false),
                    'currency'=>$order->currency->code,
                    'status'=>$order->status,
                    'comment'=>$order->comment,
                    'date'=>$order->date,
                    'ip'=>$order->ip,
                    'product_id'=>$order->getTotalProduct(),
                    'adress_city'=>$order->address->city,
                    'adress_street'=>$order->address->street,
                    'adress_country'=>$order->address->country,
                    'adress_house'=>$order->address->house,
                    'adress_flat'=>$order->address->flat,
                    'adress_phone'=>$order->address->telephone,
                    'client_firstName'=>$order->client->first_name,
                    'client_lastName'=>$order->client->last_name,
                    'client_email'=>$order->client->email,
                    'delivery'=>$order->type,
                    'land'=>$order->referer,

                );
            }
            Yii::log($message);
        }else{
           Yii::log("ERROR 401 ".$message);
           return header("HTTP/1.0 401 Unauthorized");
            // return http_response_code(401); 
        }
        if(!isset($data)){
            $data['result']='error';
            Yii::log("ERROR 400 ".$message);
            // http_response_code(400);
            header("HTTP/1.0 400 Bad Request");
        }
        Yii::log("SUCCESS 200 ".$message.print_r($data,true));
        $this->renderPartial('json', array(
            'data' => $data,
        ));
    }
}
