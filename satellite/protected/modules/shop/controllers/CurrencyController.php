<?php
/**
 * Created by PhpStorm.
 * User: Prog
 * Date: 30.10.14
 * Time: 11:57
 */
Yii::import('cms.models.currency.Currency');
Yii::import('cms.models.order.OrderType');

class CurrencyController extends Controller
{

    /**
     * Actions for CRUD можно использовать повторно
     * @return array
     * modelName Название модели, с которой работает action
     * redirectTo масив для редиректа на нужную страницу можно упустить(по дефолту array('index'))
     */
    public $layout='//layouts/glp1';
    public function actions()
    {
        return array(
            'create' => array(
                'class'=>'shop.extensions.actions.CreateAction',
                'modelName' => 'Currency'
            ),
            'delete' => array(
                'class'=>'shop.extensions.actions.DeleteAction',
                'modelName' => 'Currency'
            ),
            'update' => array(
                'class'=>'shop.extensions.actions.UpdateAction',
                'modelName' => 'Currency'
            ),
        );
    }

    /**
     * action for Administration currencies
     */
    public function actionIndex()
    {
        $model=new Currency('search');
        $model->unsetAttributes();
        if(isset($_GET['Currency']))
            $model->attributes=$_GET['Currency'];

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    public function actionSettings(){
        $this->layout='//layouts/glp1';
        $delivery = new OrderType('search');
        $currency = new Currency('search');
        $this->render('settings',array("currency"=>$currency, "delivery"=>$delivery));
    }
}