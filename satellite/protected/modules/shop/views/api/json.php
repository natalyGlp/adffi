<?php
/**
 * @var $this ApiController
 * @var $data ShopProductData
 * @var $callback
 */

header('Content-type: application/json');
$json = CJSON::encode($data);
if (isset($callback) && $callback) {
    echo $callback . ' (' . $json . ');';
} else {
    echo $json;
}
