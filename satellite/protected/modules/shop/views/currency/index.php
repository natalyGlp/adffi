<?php
/* @var $this CurrencyController */
/* @var $model Currency */
header('Location: /shop/currency/settings');
$this->breadcrumbs=array(
    ShopModule::t('Магазин')=>array('/shop/default/index'),
    ShopModule::t('Управление валютами'),
);
?>
<?= CHtml::link(ShopModule::t('Добавить валюту'), array('/shop/currency/create'), array('class'=>'btn btn-success')); ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'currency-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'name',
        'sign',
        'code',
        'coefficient',
        'is_main',
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=> '{update}&nbsp;{delete}',
        ),
    ),
));
?>