<?php
/* @var $this CurrencyController */
/* @var $model Currency */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'currency-form',
    'enableAjaxValidation'=>true,
    'htmlOptions' => array(
    	'class' => 'form-horizontal'
    )
)); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		
	</div>
	<div class="panel-body">
<?php echo $form->errorSummary($model); ?>
			<div class="form-group">
				<?php echo $form->labelEx($model,'name',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'name',array('class'=>'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model,'code',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'code',array('class'=>'form-control','maxlength'=>255)); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model,'sign',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'sign',array('class'=>'form-control')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model,'coefficient',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'coefficient',array('class'=>'form-control')); ?>
				</div>
			</div>
	</div>
<div class="panel-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'success',
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>
</div>

<?php $this->endWidget(); ?>