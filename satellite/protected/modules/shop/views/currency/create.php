<?php
/* @var $this CurrencyController */
/* @var $model Currency */

$this->breadcrumbs=array(
    ShopModule::t('Магазин')=>array('/shop/default/index'),
    ShopModule::t('Управление валютами')=>array('index'),
    ShopModule::t('Добавление'),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>