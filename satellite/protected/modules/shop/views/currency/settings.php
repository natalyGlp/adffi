<?php
$this->breadcrumbs=array(
    ShopModule::t('Магазин')=>array('/shop/default/index'),
    ShopModule::t('Настройки магазина'),
);
?>

<div class="page-content">
 <div class="container-fluid">
                                 
<div data-widget-group="group1">
    <div class="row">
        <div class="col-sm-12">
            <div class="tab-container tab-default">
                <ul class="nav nav-tabs">
                    <?php if(Yii::app()->user->checkAccess('Shop.currency.*')){ ?>
                        <li class="active"><a href="#tab-currency" data-toggle="tab">Валюты</a></li>
                    <?php } ?>
                    <?php if(Yii::app()->user->checkAccess('Shop.delivery.*')){ ?>
                        <li><a href="#tab-delivery" data-toggle="tab">Способы доставки</a></li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-currency">
                        <?= CHtml::link(ShopModule::t('Добавить валюту'), array('/shop/currency/create'), array('class'=>'btn btn-success')); ?>
                        <?php $this->widget('bootstrap.widgets.TbGridView',array(
                            'id'=>'currency-grid',
                            'dataProvider'=>$currency->search(),
                            'filter'=>$currency,
                            'columns'=>array(
                                'id',
                                'name',
                                'sign',
                                'code',
                                'coefficient',
                                'is_main',
                                array(
                                    'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'template'=> '{update}',
                                ),
                                array(
                                    'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'template'=> '{delete}',
                                ),
                            ),
                        ));
                        ?>
                    </div>
                    <div class="tab-pane" id="tab-delivery">
                        <?= CHtml::link(ShopModule::t('Добавить способ доставки'), array('/shop/delivery/create'), array('class'=>'btn btn-success')); ?>
                        <?php $this->widget('bootstrap.widgets.TbGridView',array(
                            'id'=>'orderType-grid',
                            'dataProvider'=>$delivery->search(),
                            'filter'=>$delivery,
                            'columns'=>array(
                                'id',
                                'type',
                                'name',
                                'value',
                                'message',
                                'condition',
                                array(
                                    'name' => 'enabled',
                                    'type' => 'raw',
                                    'value' => '$data->status',
                                    'filter'=> array(
                                        1=>ShopModule::t('Активен'),
                                        0=>ShopModule::t('Не активен')
                                    )
                                ),
                                array(
                                    'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'template'=> '{update}',
                                    'buttons' => array(
                                        'update' => array
                                        (
                                            'label'=>'редактировать',
                                        //    'icon'=>'plus',
                                            'url'=>'Yii::app()->createUrl("/shop/delivery/update", array("id"=>$data->id))',
                                            // 'options'=>array(
                                            //     'class'=>'btn btn-small',
                                            // ),
                                        ),
                                    ),
                                ),
                                array(
                                    'class'=>'bootstrap.widgets.TbButtonColumn',
                                    'template'=> '{delete}',
                                    'buttons' => array(
                                        'delete' => array
                                        (
                                            'label'=>'удалить',
                                            //'icon'=>'plus',
                                            'url'=>'Yii::app()->createUrl("/shop/delivery/delete", array("id"=>$data->id))',
                                            // 'options'=>array(
                                            //     'class'=>'btn btn-small',
                                            // ),
                                        ),
                                    ),
                                ),
                            ),
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

                            </div> <!-- .container-fluid -->
                        </div>