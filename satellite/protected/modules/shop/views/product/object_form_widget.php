<?php
/* @var $form CActiveForm */
?>
<div class="container-fluid">
    <div data-widget-group="group1">
<?php 
    $form = $this->beginWidget('CActiveForm', array(
        'id'=>'object-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('class'=>'form-horizontal'),
    ));
?>
<?php echo $form->errorSummary(CMap::mergeArray(array($model), $model->productData)); ?>
<!-- <div class="form-wrapper"> -->
    <!-- <div id="form-sidebar"> -->

<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default" data-widget='{"draggable": "false"}'>
            <div class="panel-heading">
                <h2><?php echo _t('Товар'); ?></h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
            </div>
            <div class="panel-editbox" data-widget-controls=""></div>
            <div class="panel-body">
                <?php $this->render('cms.widgets.views.object.object_language_name_content_widget', array(
                    'model' => $model,
                    'type' => $type,
                    'form' => $form,
                    'versions' => $versions,
                ));?>
            </div>
        </div>  

        <div class="panel panel-default" data-widget='{"draggable": "false"}'>
            <div class="panel-heading">
                <h2><?php echo _t('Сайты'); ?></h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
            </div>
            <div class="panel-editbox" data-widget-controls=""></div>
            <div class="panel-body">
                 <?php $this->widget('application.modules.shop.widgets.ProductSitesSelector', array(
                    'object' => $model,
                    'form' => $form,
                )); ?>
            </div>
        </div>   

        <div class="panel panel-default" data-widget='{"draggable": "false"}'>
            <div class="panel-heading">
                <h2><?php echo _t('Product Variants'); ?></h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
            </div>
            <div class="panel-editbox" data-widget-controls=""></div>
            <div class="panel-body">
                <?php
                // привязка товаров
                $this->widget('cms.widgets.shop.ShopProductVariants', array(
                    'model' => $model,
                    ));
                ?>
            </div>
        </div>

        <div class="panel panel-default" data-widget='{"draggable": "false"}'>
            <div class="panel-heading">
                <h2><?php echo _t('Resources'); ?></h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
            </div>
            <div class="panel-editbox" data-widget-controls=""></div>
            <div class="panel-body">
                <?php $this->render('cms.widgets.views.object.object_resource_form_widget', array(
                    'model' => $model,
                    'type' => $type,
                    'content_resources' => $content_resources,
                ));?>
            </div>
        </div>   


    </div>
    <div class="col-sm-4">
        <div class="panel panel-default" data-widget='{"draggable": "false"}'>
            <div class="panel-heading">
                <h2><?php echo t('Склад'); ?></h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
            </div>
            <div class="panel-editbox" data-widget-controls=""></div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-12"><?php echo $form->labelEx($model, 'product_count'); ?></label>
                    <div class="col-sm-12">
                        <?php echo $form->textField($model, 'product_count',array('class'=>'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo t('Резерв'); ?></label>
                    <div class="col-sm-8">
                        <span class="label label-warning">
                        <?php Yii::import('cms.models.order.Order');
                            echo Order::getReserved($model->primaryKey);
                        ?>
                        </span>
                        <?php echo $form->error($model, 'product_count'); ?>
                    </div>

                </div>

            </div>
        </div>  

        <div class="panel panel-default" data-widget='{"draggable": "false"}'>
            <div class="panel-heading">
                <h2><?php echo _t('Цена продукта'); ?></h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
            </div>
            <div class="panel-editbox" data-widget-controls=""></div>
            <div class="panel-body">
                 <?php $this->widget('application.modules.shop.widgets.ProductPriceForm', array(
                    'object' => $model,
                    'form' => $form,
                    )); ?>
            </div>
        </div>  

        <div class="panel panel-default" data-widget='{"draggable": "false"}'>
            <div class="panel-heading">
                <h2><?php echo _t('Publish'); ?></h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body"}'></div>
            </div>
            <div class="panel-editbox" data-widget-controls=""></div>
            <div class="panel-body">
                <?php 
                $this->render('cms.widgets.views.object.object_publish_sidebar_form', array(
                    'form' => $form,
                    'model' => $model,
                    'content_status' => $content_status,
                    'type' => $type,
                    'terms' => $terms,
                    'selected_terms' => $selected_terms,
                ));?>
            </div>
        </div> 


    </div>
</div>








      

 					
 
<br class="clear" />
<?php $this->endWidget(); ?>
    </div>
</div><!-- form -->
<!-- //Render Partial for Javascript Stuff -->
<?php $this->render('cms.widgets.views.object.object_form_javascript', array(
    'model' => $model,
    'form' => $form,
    'type' => $type,
));?>
