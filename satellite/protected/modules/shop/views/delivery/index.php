<?php
/* @var $this DeliveryController */
/* @var $model OrderType */
header('Location: /shop/currency/settings');
$this->breadcrumbs=array(
    ShopModule::t('Магазин')=>array('/shop/default/index'),
    ShopModule::t('Управление способами доставки'),
);
?>
<?= CHtml::link(ShopModule::t('Добавить способ доставки'), array('/shop/delivery/create'), array('class'=>'btn btn-success')); ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'orderType-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'type',
        'name',
        'value',
        'message',
        'condition',
        array(
            'name' => 'enabled',
            'type' => 'raw',
            'value' => '$data->status',
            'filter'=> array(
                1=>ShopModule::t('Активен'),
                0=>ShopModule::t('Не активен')
            )
        ),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=> '{update}&nbsp;{delete}',
        ),
    ),
));
?>