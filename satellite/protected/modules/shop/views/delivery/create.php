<?php
/* @var $this DeliveryController */
/* @var $model OrderType */

$this->breadcrumbs=array(
    ShopModule::t('Магазин') => array('/shop/default/index'),
    ShopModule::t('Управление способами доставки')=>array('index'),
    ShopModule::t('Добавление'),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>