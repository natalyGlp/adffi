<?php
/* @var $this DeliveryController */
/* @var $model OrderType */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'orderType-form',
    'enableAjaxValidation'=>true,
    'htmlOptions' => array(
    	'class' => 'form-horizontal'
    )
)); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		
	</div>
	<div class="panel-body">
			<?php echo $form->errorSummary($model); ?>
			<div class="form-group">
				<?php echo $form->labelEx($model,'type',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'type',array('class'=>'form-control')); ?>
				</div>	
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'name',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'name',array('class'=>'form-control')); ?>
				</div>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'value',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'value',array('class'=>'form-control')); ?>
				</div>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'message',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'message',array('class'=>'form-control')); ?>
				</div>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'condition',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->textField($model,'condition',array('class'=>'form-control')); ?>
				</div>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'enabled',array('class'=>'col-sm-2 control-label')); ?>
				<div class="col-sm-8">
					<?php echo $form->dropDownList($model,'enabled', array(1 => ShopModule::t('Активен'), 0 => ShopModule::t('Не активен'), 'class'=>'form-control')); ?>
				</div>
			</div>
			
			<?php echo $form->error($model,'enabled',array('class'=>'span5 form-control')); ?>
	
	</div>
	<div class="panel-footer">
		    <?php $this->widget('bootstrap.widgets.TbButton', array(
		        'buttonType'=>'submit',
		        'type'=>'success',
		        'label'=>$model->isNewRecord ? 'Create' : 'Save',
		    )); ?>			
	</div>
</div>




<?php $this->endWidget(); ?>