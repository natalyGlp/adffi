<?php
/**
 * Модуль супер магазина.
 * Супер магазин - это модуль предназначенный для централизованного управления заказами из сателлитной сети лендингов.
 *
 * Подключение:
 * - добавить модуль в конфиге
 *  'modules' => array(
 *     'shop',
 *     ...
 *  )
 * - добавить компонент ShopBootstrap и добавить его в прелоад
 *  'components' => array(
 *      'shopBootstrap'=>array( // инициализация костылей для супермагазина
 *          'class'=>'application.modules.shop.components.ShopBootstrap',
 *          'db' => array(
 *              'connectionString' => 'mysql:host=localhost;dbname=dbname',
 *              'username' => 'username',
 *              'password' => 'password',
 *              'schemaCachingDuration' => 3600,
 *              'emulatePrepare' => true,
 *              'charset' => 'utf8',
 *              'tablePrefix' => 'gxc_',
 *              'class' => 'CDbConnection',
 *              ),
 *      ),
 *     ...
 *  )
 *
 *  'preload'=>array('log','bootstrap', 'shopBootstrap'),
 *
 * - так же нужно добавить пункт в главное меню (файл config/params.php)
 *    'headerMenu' => array(
 *        array('label'=>'Магазин', 'url'=>array('/shop')),
 *        ),
 *
 * @todo надо, что бы модули подключались в файле params.php ?
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package modules.shop
 */
class ShopModule extends CWebModule
{
    public function init()
    {
        // import the module-level models and components
        $this->setImport(array(
            'shop.models.*',
            'shop.components.*',
            'cms.models.currency.*',
        ));

        // Ну нам ну ооочень нужен этот класс
        Yii::import('shop.models.CatalogObject', true);

        $this->setComponent('affiliate', array(
            'class' => 'shop.components.AffiliateSystem',
        ));
        $this->setComponent('cpa', array(
            'class' => 'shop.components.CpaSystem',
        ));
        $this->affiliate->init();
        Yii::log("data connection ".print_r($this->affiliate,true));
        
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here

            $controller->menu = array(
                array('label' => 'Заказы',
                    'url' => array('default/index'),
                    'icon' => 'shopping-cart',
                ),
                '---',
                array('label' => 'Управление товаром',
                    'url' => array('product/admin'),
                    'icon' => 'list',
                ),
                array('label' => 'Новый товар',
                    'url' => array('product/create',
                        'type' => 'catalog'),
                    'icon' => 'plus',
                ),
                '---',
                array('label' => 'Настройки магазина',
                    'url' => array('shop/settings'),
                    'icon' => 'wrench',
                    'itemOptions' => array(
                        'class' => 'dropup',
                    ),
                    'items' => array(
                        array(
                            'label' => self::t('Валюты'),
                            'url' => array('/shop/currency/index')
                        ),
                        array(
                            'label' => self::t('Способы доставки'),
                            'url' => array('/shop/delivery/index')
                        )
                    )),
            );

            return true;
        } else {
            return false;
        }
    }

    public static function t($message, $params = array())
    {
        return Yii::t('shop.messages', $message, $params, null, 'ru');
    }
}
