<?php
Yii::import('application.modules.shop.models.*');

class CatalogObject extends Object
{
    public $object_type = 'catalog';
    public $product_count;

    /**
     * @var array $_sites сайты на которых показывать продукт
     * @see  self::getProductSites(), self::setProductSites()
     */
    public $productSites = array();
    /**
     * @var array $isFeaturedOnSites id сайтов на которых данный продукт должен особо пиариться
     */
    public $isFeaturedOnSites = array();

    /**
     * @var array site_id => коды валют через запятую (UAH, RUB etc.)
     */
    public $siteCurrencies = array();

    public $productSitesFilter;

    /**
     * Статусы товара
     */
    const STATUS_AVAILABLE = 1;
    const STATUS_UNAVAILABLE = 10;
    const STATUS_REORDER = 11;
    const STATUS_ORDERED = 12;
    const STATUS_ARCHIVED = 13;

    const NOTIFY_CATEGORY_UNAVAILABLE = 'shopObjectStatus_unavailable';
    const NOTIFY_CATEGORY_NEED_APPROVE = 'shopObjectStatus_ordered';

    public static function getObjectStatus()
    {
        return array(
            self::STATUS_AVAILABLE => t("Available"),
            self::STATUS_UNAVAILABLE => t("Unavailable"),
            self::STATUS_REORDER => t("Reorder"),
            self::STATUS_ORDERED => t("Ordered"),
            self::STATUS_ARCHIVED => t("Out of Production"),
            self::STATUS_DRAFT => t("Draft"),
            self::STATUS_HIDDEN => t("Hidden")
        );
    }

    public function getSellableStatuses()
    {
        return array(
            t('No'),
            t('Yes'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Object the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Залогениным юзерам даем доступ только к их
     * материалам или только к материалам на их сайтах
     * @return array критерий поиска
     */
    public function defaultScope()
    {
        $criteria = array();

        if (!Yii::app()->user->isGuest && !Yii::app()->user->isAdmin) {
            $cacheId = 'user-products-criteria-' . Yii::app()->user->id;
            if (!($condition = Yii::app()->cache->get($cacheId))) {
                $productsAvailable = Yii::app()->db->createCommand()
                    ->select('t.product_id')
                    ->from(ShopProductSites::model()->tableName() . ' t')
                    ->where('us.id_user = :uid', array(
                        ':uid' => Yii::app()->user->id,
                    ))
                    ->leftJoin('{{user_sites}} us', 'us.id_site = t.site_id')
                    ->queryColumn()
                    ;

                $productsAvailable = CMap::mergeArray(
                    $this->dbConnection->createCommand()
                        ->select('t.object_id')
                        ->from($this->tableName() . ' t')
                        ->where('t.id_parent IN ('.implode(',', $productsAvailable).')')
                        ->queryColumn(),
                    $productsAvailable
                );

                $condition = 'object_author = :uid OR object_id IN (' . implode(',', $productsAvailable) . ')';

                Yii::app()->cache->set($cacheId, $condition, 60);
            }

            $criteria = array(
                'condition' => $condition,
                'params' => array(
                    ':uid' => Yii::app()->user->id,
                ),
            );
        }

        return $criteria;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return CMap::mergeArray(array(
            array('sellable, shop_id, productSites, siteCurrencies, isFeaturedOnSites', 'safe'),
            array('product_count', 'required'),
            array('siteCurrencies', 'ensureSiteCurrencies'),
            array('productSitesFilter', 'safe', 'on' => 'search'),
        ), parent::rules());
    }

    public function ensureSiteCurrencies($attribute, $params = array())
    {
        foreach ($this->productSites as $siteId) {
            if (!isset($this->siteCurrencies[$siteId]) || empty($this->siteCurrencies[$siteId])) {
                $this->addError('siteCurrencies', ShopModule::t('Каждый выбранный сайт должен иметь минимум одну валюту'));
                return;
            }
        }
    }

    public function getMetaAttributes()
    {
        return array(
            'product_count',
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return CMap::mergeArray(array(
            'filters' => array(self::HAS_ONE, 'Filters', 'id_object'),
            'catalogattributes' => array(self::HAS_ONE, 'CatalogAttributes', 'id_object'),
            'productData' => array(self::HAS_MANY, 'ShopProductData', 'product_id', 'with'=>'currency')
        ), parent::relations());
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return CMap::mergeArray(array(
            'primaryKey' => ShopModule::t('ID'),
            'isFeaturedOnSites' => ShopModule::t('Продвигать'),
            'productSites' => ShopModule::t('Сайты'),
            'productSitesFilter' => ShopModule::t('Сайты'),
            'product_count' => ShopModule::t('Кол-во на складе'),
            'siteCurrencies' => ShopModule::t('Валюты продукта'),
        ), parent::attributeLabels());
    }

    public function validate($attributes = null, $clearErrors = true)
    {
        $isValid = parent::validate($attributes, $clearErrors);

        return $this->validateProductData() && $isValid;
    }

    protected function validateProductData()
    {
        if ($this->scenario == 'variant') {
            return true; // @see CatalogObject::afterSave()
        }

        // убеждаемся, что у нас есть модель данных о товаре
        if (!isset($_POST['ShopProductData']) || empty($_POST['ShopProductData'])) {
            $model = new ShopProductData();
            $model->addError('currency_id', 'Необходимо указать минимум одну валюту');
            $this->productData = array($model);
            return false;
        }

        $models = array();
        $isValid = true;
        foreach ($_POST['ShopProductData'] as $i => $attributes) {
            $model = isset($this->productData[$i]) ? $this->productData[$i] : new ShopProductData();
            $model->attributes = $attributes;
            $isValid = $model->validate() && $isValid;

            $models[] = $model;
        }
        $this->productData = $models;

        return $isValid;
    }

    protected function afterSave()
    {
        // Боримся с рекурсией afterSave при добавлении новых связей
        // отменяем обработку каких либо связей при изменении вариантов товара
     ///    YII::log("api_Nataly PRODUCT?");
        if ($this->scenario != 'variant') {
            $this->bindVariants();

            $this->bindToSites();

            $this->saveProductData();

            $this->processStatus();
        }

        return parent::afterSave();
    }

    protected function bindVariants()
    {
        if (isset($_POST['ProductVariants'])) {
            $productVariants = $_POST['ProductVariants'];

            $this->updateAll(array('id_parent' => 0), 'object_type="' . $this->object_type . '" AND id_parent="' . $this->primaryKey . '"');

            foreach ($productVariants as $index => $postData) {
                $object = empty($postData['object_id']) ? new CatalogObject() : CatalogObject::model()->findByPk($postData['object_id']);
                $object->scenario = 'variant';

                $object->object_name = $postData['object_name'];
                $object->id_parent = $this->primaryKey;
                $object->like_count = $index+1;
                $object->save();
            }
        }
    }

    protected function saveProductData()
    {
        $pksToHold = array();
        foreach ($this->productData as $model) {
            $model->product_id = $this->primaryKey;
            $model->save(false);
            $pksToHold[] = $model->primaryKey;
        }

        if (isset($model)) {
            $model->deleteAll(array(
                'condition' => 'product_id=:productId AND id NOT IN ('.implode(', ', $pksToHold).')',
                'params' => array(
                    ':productId' => $this->primaryKey,
                ),
            ));
        }
    }

    protected function bindToSites()
    {
        ShopProductSites::model()->deleteAllByAttributes(array('product_id' => $this->primaryKey));
        if (count($this->productSites)) {
            $availableSites = Yii::app()->user->isAdmin ? array() : User::model()->findByPk(Yii::app()->user->id)->sites;
            $availableSites = CHtml::listData($availableSites, 'id', 'id');
            foreach ($this->productSites as $siteId) {
                if (!isset($availableSites[$siteId]) && !Yii::app()->user->isAdmin) {
                    continue;// юзер не имеет права выбирать этот сайт
                }

                $shopProductSites = new ShopProductSites();
                $shopProductSites->setAttributes(array(
                    'product_id' => $this->primaryKey,
                    'site_id' => $siteId,
                    'is_featured' => isset($this->isFeaturedOnSites[$siteId]) * 1,
                    'currencies' => isset($this->siteCurrencies[$siteId]) ? $this->siteCurrencies[$siteId] : '',
                ));
                $shopProductSites->save();

                // на всякий случай обновляем связь с api сателлитки
                Sites::model()->findByPk($siteId)->setCmsSetting('apiUrl', Yii::app()->createAbsoluteUrl('shop/api'), 'satellite');
            }
        }
    }

    /**
     * @see shop.widgets.ProductSitesSelector
     */
    public function fetchProductSites()
    {
        $models = ShopProductSites::model()->findAllByAttributes(array('product_id' => $this->primaryKey));

        foreach ($models as $model) {
            $this->productSites[] = $model->site_id;
            $this->isFeaturedOnSites[$model->site_id] = $model->is_featured;
            $this->siteCurrencies[$model->site_id] = $model->currencies;
        }
    }

    /**
     * Excute after Delete Object
     */
    protected function afterDelete()
    {
        parent::afterDelete();
        // удалим все фильтры и аттрибуты
        Filters::model()->deleteAll('id_object = :obj', array(':obj' => $this->object_id));

        CatalogAttributes::model()->deleteAll('id_object = :obj', array(':obj' => $this->object_id));
    }

    /**
     * Считает количество товара с учетом резервов
     */
    public function calculateProductCount()
    {
        Yii::import('cms.models.order.Order');

        $nonReservingStatuses = array(
            Order::NOT_COMPLETE,
            Order::COMPLETE,
            Order::CANCELED,
        );

        $reserved = Order::getReserved($this->primaryKey);

        return $this->product_count - $reserved;
    }

    /**
     * Автоматически определяет статус товара (или использует $forceStatus),
     * создавая необходимые алерты для персонала
     *
     * @param integer $forceStatus id статуса, который нужно "насильно" выставить товару
     */
    public function processStatus($forceStatus = false)
    {
        if ($forceStatus) {
            $this->object_status = $forceStatus;
        } elseif ($this->product_count < 5) {
            if ($this->object_status != self::STATUS_ORDERED) {
                if ($this->product_count == 0) {
                    $this->object_status = self::STATUS_UNAVAILABLE;
                } else {
                    $this->object_status = self::STATUS_REORDER;
                }
            }
        } else {
            if ($this->object_status != self::STATUS_REORDER) {
                $this->object_status = self::STATUS_AVAILABLE;
            }
        }
    }

    public function notifyStatus()
    {
        $category = self::NOTIFY_CATEGORY_UNAVAILABLE;
        $categoryOrdered = self::NOTIFY_CATEGORY_NEED_APPROVE;

        $count = $this->count('object_status IN (' . self::STATUS_REORDER . ',' . self::STATUS_UNAVAILABLE . ') AND object_id <> ' . $this->primaryKey);
        $countOrdered = $this->count('object_status=' . self::STATUS_ORDERED . ' AND object_id <> ' . $this->primaryKey);

        switch ($this->object_status) {
            case self::STATUS_UNAVAILABLE:
            case self::STATUS_REORDER:
                $count++;
                break;

            case self::STATUS_ORDERED:
                $countOrdered++;
                break;
        }
        $defaultParams = array(
            'placement' => 'sidebar',
            'level' => 'danger',
            'updateByCategory' => true,
        );

        if ($count > 0) {
            $url = Yii::app()->createUrl('shop/product/admin', array('CatalogObject[object_status][0]' => self::STATUS_REORDER, 'CatalogObject[object_status][1]' => self::STATUS_UNAVAILABLE));
            Yii::app()->notify->create($category, CMap::mergeArray($defaultParams, array(
                'counter' => $count,
                'title' => CHtml::link('Необходим дозаказ', $url),
                'url' => $url,
                'message' => 'Необходим дозаказ ' . $count . ' товаров',
            )));
        } else {
            Yii::app()->notify->disable($category);
        }

        if ($countOrdered > 0) {
            $url = Yii::app()->createUrl('shop/product/admin', array('CatalogObject[object_status][0]' => self::STATUS_ORDERED));
            Yii::app()->notify->create($categoryOrdered, CMap::mergeArray($defaultParams, array(
                'counter' => $countOrdered,
                'title' => CHtml::link('Необходимо обновить наличие', $url),
                'url' => $url,
                'message' => 'Необходимо подтвердить наличие ' . $countOrdered . ' товаров',
            )));
        } else {
            Yii::app()->notify->disable($categoryOrdered);
        }
    }

    public function getGridColumns()
    {
        $columns = parent::getGridColumns();

        $columns[] = array(
            'name' => 'productSitesFilter',
            'value' => 'implode(",", $data->sitesList);',
            'filter' => CHtml::listData(Sites::getUserSites(), 'id', 'cleanUrl'),
        );

        return $columns;
    }

    /**
     * @return array список сайтов, на которых продается текущий товар
     */
    public function getSitesList()
    {
        $models = ShopProductSites::model()->with('site')->findAllByAttributes(array('product_id' => $this->primaryKey));

        $list = array();

        foreach ($models as $model) {
            array_push($list, $model->site->cleanUrl);
        }

        return $list;
    }

    public function doSearch($type = 0, $object_type = '')
    {
        $searchProvider = parent::doSearch($type, $object_type);

        if (!empty($this->productSitesFilter)) {
            // фильтрация по сайтам
            $sitesFilterCriteria = new CDbCriteria();
            $models = ShopProductSites::model()->findAllByAttributes(array('site_id' => $this->productSitesFilter));

            $ids = CHtml::listData($models, 'product_id', 'product_id');
            $sitesFilterCriteria->addInCondition('object_id', $ids);

            $searchProvider->criteria->mergeWith($sitesFilterCriteria);
        }

        return $searchProvider;
    }

    /**
     * Возвращает варианты товара
     */
    public function getVariants()
    {
        return $this->findAllByAttributes(array(
            'id_parent' => $this->primaryKey,
            'object_type' => $this->object_type,
        ), array(
            // TODO: костыль, что бы обеспечить возможность менять порядок вариантов.
            // В будущем есть смысл вынести связь вариантов в отдельную таблицу
            'order' => 'like_count ASC',
        ));
    }
}
