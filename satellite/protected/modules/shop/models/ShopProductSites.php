<?php

/**
 * This is the model class for table "shop_product_sites".
 *
 * The followings are the available columns in table 'shop_product_sites':
 * @property string $product_id
 * @property string $site_id
 * @property integer $is_featured
 * @property integer $currencies
 */
class ShopProductSites extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ShopProductSites the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'shop_product_sites';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('product_id, site_id, currencies', 'required'),
            array('is_featured', 'numerical', 'integerOnly' => true),
            array('product_id, site_id', 'length', 'max' => 11),
            array('currencies', 'length', 'max' => 255),
            array('product_id, site_id, is_featured, currencies', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'site' => array(self::BELONGS_TO, 'Sites', 'site_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'product_id' => 'Продукт',
            'site_id' => 'Сайт',
            'is_featured' => 'Продвигаемый',
        );
    }

    public static function findAllProductsByHost($host, $customCriteria = array())
    {
        $model = new self();

        // get productIds and currencies for host
        $criteria = new CDbCriteria();
        $criteria->join = 'LEFT JOIN ' . Sites::model()->tableName() . ' `site` ON site.id = ' . $model->tableAlias . '.site_id';
        $criteria->select = 'product_id, currencies';
        $criteria->compare('site.url', 'http://'.$host);
        $criteria->mergeWith($customCriteria);

        $command = $model->commandBuilder->createFindCommand($model->getTableSchema(), $criteria, $model->getTableAlias());
        $hostProducts = $command->queryAll();

        $productIds = array();
        $currencies = '';
        foreach ($hostProducts as $hostProduct) {
            $productIds[] = $hostProduct['product_id'];
            $currencies .= ',' . $hostProduct['currencies'];
        }
        // remove currency dublicates and get ids
        $currencies = preg_replace('/[^\w]+/', ',', $currencies);
        $currencies = trim($currencies, ',');
        $currencies = explode(',', $currencies);
        $currencies = Currency::model()->findAllByAttributes(array('code' => $currencies));
        $currencies = CHtml::listData($currencies, 'id', 'id');

        $productCriteria = new CDbCriteria();
        $productCriteria->with = array('productData');
        $productCriteria->compare('object_id', $productIds);
        $productCriteria->compare('currency_id', $currencies);
        GxcHelpers::applyObjectStatusCriteria($productCriteria);

        $products = CatalogObject::model()->findAll($productCriteria);

        return $products;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('product_id', $this->product_id, true);
        $criteria->compare('site_id', $this->site_id, true);
        $criteria->compare('is_featured', $this->is_featured);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>50,
            )
        ));
    }
}
