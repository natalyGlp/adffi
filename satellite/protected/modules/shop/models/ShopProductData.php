<?php
/**
 * This is the model class for table "{{shop_product_data}}".
 *
 * The followings are the available columns in table '{{shop_product_data}}':
 * @property integer $id
 * @property string $product_id
 * @property string $price
 * @property string $default_price
 * @property string $tax
 * @property integer $tax_type
 * @property string $delivery_price
 * @property integer $delivery_price_type
 * @property string $discount
 * @property integer $discount_type
 * @property string $product_count
 */

class ShopProductData extends CmsActiveRecord
{
    /**
     * Константы типов параметров (фиксированная сумма, процент от стоимости заказа и т.п.)
     */
    const TYPE_FIXED = 1;
    const TYPE_PERCENT = 2;

    public $tax_type = self::TYPE_PERCENT;
    public $delivery_price_type = self::TYPE_FIXED;
    public $discount_type = self::TYPE_FIXED;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ShopProductData the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{shop_product_data}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        /**
         * @return array
         */
        return array(
            array('default_price, currency_id', 'required'),
            array('currency_id', 'cms.extensions.validators.UniquePairValidator', 'pairAttribute' => 'product_id'),
            array('tax_type, delivery_price_type, discount_type', 'numerical', 'integerOnly' => true),
            array('product_id', 'length', 'max' => 11),
            array('custom_price, default_price, tax, delivery_price, discount', 'length', 'max' => 19),
            array('id, product_id, custom_price, currency_id, default_price, tax, tax_type, delivery_price, delivery_price_type, discount, discount_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            array(self::BELONGS_TO, 'CatalogObject', 'product_id'),
            'currency' => array(self::BELONGS_TO, 'Currency', 'currency_id')
        );
    }

    protected function beforeSave()
    {
        $check = array(
            'discount',
            'delivery_price',
            'tax',
            'custom_price',
        );

        foreach ($check as $attribute) {
            if (empty($this->{$attribute})) {
                $this->{$attribute} = new CDbExpression('NULL');
            }
        }

        return parent::beforeSave();
    }

    /**
     * Расчет цены для текущего товара на основе его количества, томости доставки и тд
     * @var integer $amount количество товара
     */
    public function calculateTotalPrice($amount = 1)
    {
        return $amount * $this->sellprice + $this->deliveryPrice;
    }

    /**
     * Возвращает цену, по которой юзер может купить товар
     *
     * Примеры:
     *     ->getSellPrice(array('discount' => false)) — получить цену товара без учета скидки
     *
     * @param array $params параметры рассчета цены
     * @return float цена
     */
    public function getSellPrice($params = array())
    {
        if (!empty($this->custom_price)) {
            return $this->custom_price;
        }

        $price = $this->default_price;

        $discountDisabled = isset($params['discount']) && !$params['discount'];

        if ($this->discount > 0 && !$discountDisabled) {
            $price = $this->applyDiscount($price, $this->discount, $this->discount_type);
        }

        if ($this->tax > 0) {
            $price = $this->applyTax($price, $this->tax, $this->tax_type);
        }

        return $price;
    }

    /**
     * Шорткат для получения цены товара без скидки
     * @return float цена товара
     */
    public function getOldPrice()
    {
        return $this->getSellPrice(array('discount' => false));
    }

    /**
     * @return boolean есть ли у товара скидочная цена
     */
    public function getHasDiscount()
    {
        return !empty($this->discount);
    }

    /**
     * @return string форматированное значение скидки
     */
    public function getDiscountAmount()
    {
        if ($this->hasDiscount) {
            if ($this->discount_type == self::TYPE_FIXED) {
                throw new Exception('Unimplemented');
            } else {
                return $this->discount . '%';
            }
        }

        return false;
    }

    /**
     * @return float цена доставки товара
     */
    public function getDeliveryPrice()
    {
        $delivery = $this->delivery_price;

        if ($this->delivery_price_type == self::TYPE_PERCENT) {
            $delivery = $this->modifyPrice($this->sellPrice, 100 - $this->delivery_price, $this->delivery_price_type, -1);
        }

        return $delivery;
    }

    /**
     * Применяет налоги к цене
     * @param  float $price цена
     * @param  float $tax   налог
     * @param  integer $type  тип налога (проценты или фиксированый)
     * @return float        результирующая цена
     */
    public function applyTax($price, $tax, $type)
    {
        return $this->modifyPrice($price, $tax, $type);
    }

    /**
     * Применяет скидку к цене
     * @param  float $price цена
     * @param  float $discount   величина скидки
     * @param  integer $type  тип скидки (проценты или фиксированая)
     * @return float        результирующая цена
     */
    public function applyDiscount($price, $discount, $type)
    {
        return $this->modifyPrice($price, $discount, $type, -1);
    }

    /**
     * Модифицирует цену, увеличивая/уменьшая ее на заданную величину или процент
     * @param  float  $price цена
     * @param  float  $value величина изменения
     * @param  integer  $type  тип величины изменения (проценты/фикисрованно)
     * @param  integer $sign  знак изменения 1/-1
     * @return float         результирующая цена
     */
    public function modifyPrice($price, $value, $type, $sign = 1)
    {
        if ($type == self::TYPE_FIXED) {
            return $price + $value * $sign;
        } else {
            return $price * (1 + $value * $sign / 100);
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'product_id' => 'Продукт',
            'currency_id' => 'Валюта',
            'custom_price' => 'Переопределить цену',
            'default_price' => 'Цена продажи',
            'tax' => 'Налог',
            'tax_type' => 'Тип налога',
            'delivery_price' => 'Стоимость доставки',
            'delivery_price_type' => 'Тип стоимости доставки',
            'discount' => 'Скидка',
            'discount_type' => 'Тип скидки',
        );
    }
}
