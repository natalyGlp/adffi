<?php
Yii::import('application.modules.shop.components.OrderCrud');

class OrderCrudTest extends NESPITestCase
{
    public $crud;

    public function setUp()
    {
        $this->crud = $this->getMockBuilder('OrderCrud')
            ->setMethods(array('instantiateModels'))
            ->getMock();
    }

    public function testConstruct()
    {
        $crud = $this->getMockBuilder('OrderCrud')
            ->setMethods(array('instantiateModels'))
            ->getMock();

        $crud->expects($this->once())
            ->method('instantiateModels');

        $crud->__construct();
    }

    public function testConstructWithArgs()
    {
        $crud = $this->getMockBuilder('OrderCrud')
            ->setMethods(array('instantiateModels'))
            ->getMock();

        $crud->expects($this->once())
            ->method('instantiateModels')
            ->with($this->equalTo(5));;

        $crud->__construct(5);
    }

    public function testGetModel()
    {
        $this->setObjProperty($this->crud, '_models', array('foo' => 'bar'));

        $this->assertEquals('bar', $this->crud->getModel('foo'));
    }

    public function testAjaxValidate()
    {
        $this->assertTrue(is_callable(array($this->crud, 'ajaxValidate')));
    }

    // TODO: нужно написать ту самую серию тестов для проверки создания моделей связанных с заказом
}