<?php

class CreateAction extends CAction
{
    public $modelName;
    public $redirectTo = array('index');

    public function run()
    {
        /**
         * @var $model CActiveRecord
         */
        $model = new $this->modelName;
        $this->performAjaxValidation($model);

        if(isset($_POST[$this->modelName]))
        {
            $model->attributes=$_POST[$this->modelName];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', ShopModule::t('Данные успешно сохранены!'));
                $this->getController()->redirect(array('index'));
            }
        }
        $this->getController()->render('create',array(
            'model'=>$model,
        ));

    }

    /**
     * validate before save
     * @param $model
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']===lcfirst($this->modelName).'-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}