<?php

class UpdateAction extends CAction
{
    public $modelName;
    public $redirectTo = array('index');

    public function run($id)
    {
        /**
         * @var $model CActiveRecord
         */
        $model = CmsActiveRecord::model($this->modelName)->findByPk($id);
        $this->performAjaxValidation($model);

        if(isset($_POST[$this->modelName]))
        {
            $model->attributes=$_POST[$this->modelName];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', ShopModule::t('Данные успешно сохранены!'));
                $this->getController()->redirect($this->redirectTo);
            }
        }

        $this->getController()->render('update',array(
            'model'=>$model,
        ));

    }

    /**
     * validate before save
     * @param $model
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']===lcfirst($this->modelName).'-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}