<?php

class DeleteAction extends CAction
{
    public $modelName;
    public $redirectTo = array('index');

    public function run($id)
    {
        CmsActiveRecord::model($this->modelName)->deleteByPk($id);
        if(!isset($_GET['ajax']))
            $this->getController()->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : $this->redirectTo);
    }
}