<?php
header('Content-Type: text/html; charset=UTF-8', true);
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

error_reporting(E_ALL);
ini_set("display_errors", YII_DEBUG);

//CMS OPTIONS
/*define('CORE_FOLDER', dirname(__FILE__).'/../cms_main_project_mysql/core');
define('CMS_PATH', dirname(__FILE__).'/../satellite_project/project');
*/
define('CORE_FOLDER', dirname(__FILE__).'/../core');
define('CMS_PATH', dirname(__FILE__).'/../satellite_project/project');

if (!isset($yiiConsole)) {
    // change the following paths if necessary
    $yii = CORE_FOLDER.'/yii/framework/yii.php';
    $config = dirname(__FILE__).'/protected/config/main.php';

    require_once $yii;

    Yii::createWebApplication($config)->run();
    
}
