<?php
$cms_version='1.0';

// dirname(__FILE__).'../..' -> webroot (satellite_project dir)
defined('CORE_FOLDER') or define('CORE_FOLDER', realpath(dirname(__FILE__).'/../..').'/cms_main_project_mysql/core');
defined('CMS_PATH') or define('CMS_PATH', realpath(dirname(__FILE__).'/..')); // satellite_project/project
defined('COMMON_FOLDER') or define('COMMON_FOLDER', CMS_PATH.'/common');
defined('FRONT_END') or define('FRONT_END', CMS_PATH.'/frontend/protected');
defined('BACK_END') or define('BACK_END', CMS_PATH.'/backend/protected');
defined('LAYOUTS_PATH') or define('LAYOUTS_PATH', COMMON_FOLDER.'/front_layouts');

// uploads dirs
// DEPRECATED
defined('UPLOADS_FOLDER') or define('UPLOADS_FOLDER', dirname(CMS_PATH).'/uploads');
defined('RESOURCES_FOLDER') or define('RESOURCES_FOLDER', UPLOADS_FOLDER.'/resources');
defined('BANNERS_FOLDER') or define('BANNERS_FOLDER', UPLOADS_FOLDER.'/banners');
defined('THUMBS_FOLDER') or define('THUMBS_FOLDER', UPLOADS_FOLDER.'/thumbs');
defined('AVATAR_FOLDER') or define('AVATAR_FOLDER', UPLOADS_FOLDER.'/avatars');

// core dirs
defined('CMS_FOLDER') or define('CMS_FOLDER', CORE_FOLDER.'/cms'.$cms_version);
defined('CMS_WIDGETS') or define('CMS_WIDGETS', CMS_FOLDER.'/widgets');

// url settings
defined('HOME') or define('HOME', 'http://'.$_SERVER['HTTP_HOST'].'/');
defined('BACKEND_SITE_URL') or define('BACKEND_SITE_URL', HOME.'project/backend');
defined('UPLOADS_URL') or define('UPLOADS_URL', '/uploads');
defined('RESOURCE_URL') or define('RESOURCE_URL', UPLOADS_URL . '/resources');
defined('AVATAR_URL') or define('AVATAR_URL', UPLOADS_URL . '/avatars');
defined('GALLERY_URL') or define('GALLERY_URL', UPLOADS_URL . '/resources/galleries');

// technical constants
defined('USER_SALT') or define('USER_SALT', 'sdad12313ssgdpahcxrwwqas');
defined('USER_RECOVER_PASS_SALT') or define('USER_RECOVER_PASS_SALT', 'erc5cvm034ax403449');
defined('FRONTEND_CLEAR_CACHE_KEY') or define('FRONTEND_CLEAR_CACHE_KEY', 'ads2312xxz23197532zcvv');
defined('APP_TIMEZONE') or define('APP_TIMEZONE', 'Europe/Kiev');
defined('CONNECTION_NAME') or define('CONNECTION_NAME', 'db');
