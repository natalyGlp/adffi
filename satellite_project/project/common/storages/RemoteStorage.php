<?php

/**
 * Class for handle Upload to Local Storage
 *
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.storages
 */

class RemoteStorage
{

    public $max_file_size=ConstantDefine::UPLOAD_MAX_SIZE;
    public $min_file_size=ConstantDefine::UPLOAD_MIN_SIZE;
    public $allow_types=array();

    public function __construct($max_file_size = ConstantDefine::UPLOAD_MAX_SIZE, $min_file_size = ConstantDefine::UPLOAD_MIN_SIZE, $allow_types = array())
    {
        $this->max_file_size=$max_file_size;
        $this->min_file_size=$min_file_size;
        $this->allow_types=$allow_types;
    }

    public function uploadFile(&$resource, $model, &$process, &$message, $remote = false)
    {
        $message=t('You can not upload files using this storage');
        $process = false;

        return false;
    }

    public function getRemoteFile(&$resource, &$model, &$process, &$message, $path, $ext, $changeresname = true)
    {
        // TODO: Определиться использовать RemoteStorage::allow_types или то, что задается в настройках ресурсов в модели обьекта XxxObject

        $host = str_replace('www.', '', parse_url($path, PHP_URL_HOST));
        if (count($this->allow_types) > 0) {

            // проверка осуществляется по хосту
            if (!in_array(strtolower($host), $this->allow_types)) {
                $message=t('This host is not allowed!');
                $process=false;

                return false;
            }
        }

        $methodName = 'get'.ucfirst(substr($host, 0, strpos($host, '.')));

        if (method_exists($this, $methodName)) {
            $videoInfo = $this->$methodName($path);
        } else {
            $message=t('Unsupported video hosting!');
            $process=false;

            return false;
        }

        $videoInfo['provider'] = $host;

        if (!($path = $this->saveVideoThumb($videoInfo['thumb']))) {
            $process=false;
            $message=t('Error while getting Remote File. Try again later.');

            return false;
        }

        $resource->resource_where = $model->where = $model->where;
        $resource->resource_path = $path;
        $resource->resource_type = $model->type = 'videoembed';
        $resource->resource_body = $model->body = CJSON::encode($videoInfo);

        // if ($changeresname) {
        //  $resource->resource_name = basename($path);
        // }

        //Resource::generateThumb($filename,$folder,$filename);
        $process=true;

        return true;
    }

    protected function getYoutube($url)
    {
        $videoEntry = self::getYtVideoEntry($url);
        $videoInfo = array(
            'id' => self::getYtVideoId($url),
            'url' => $url,
            'thumb' => self::getYtVideoThumbnail($videoEntry),
            );

        return $videoInfo;
    }

    /**
     * @see http://developer.vimeo.com/apis/oembed
     */
    protected function getVimeo($url)
    {

        $oembed_endpoint = 'http://vimeo.com/api/oembed';

        // Create the URLs
        $json_url = $oembed_endpoint . '.json?url=' . rawurlencode($url);// . '&width=640';

        $curl = curl_init($json_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($curl);
        curl_close($curl);

        $data = CJSON::decode($return);

        $videoInfo = array(
            'id' => $data['video_id'],
            'url' => $url,
            'thumb' => $data['thumbnail_url'],
            );

        return $videoInfo;
    }

    /**
     * Загружает превьюшку видео и в случае успеха возвращает локальный путь к ней
     */
    protected function saveVideoThumb($thumb)
    {

        $ext = pathinfo($thumb, PATHINFO_EXTENSION);

        $ch = curl_init($thumb);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $rawdata=curl_exec($ch);
        curl_close($ch);

        if (!$rawdata) {
            return false;
        }

        $filename=gen_uuid();

        // folder for uploaded files
        $resourcesPath = Yii::getPathOfAlias('uploads.resources');
        $folder = date('Y').'/'.date('m').'/';
        $folderPath = $resourcesPath.'/'.$folder;
        if (!(file_exists($folderPath) &&
            (is_dir($folderPath)))) {
            mkdir($folderPath, 0777, true);
        }

        //Check if File exists, so Rename the Filename again;
        while (file_exists($folderPath.'/'.$filename.'.'.strtolower($ext))) {
            $filename .= rand(10, 99);
        }

        $filename=$filename.'.'.$ext;

        $path = $folder.'/'.$filename;
        $fullpath = $resourcesPath.'/'.$path;

        $fp = fopen($fullpath, 'x');
        fwrite($fp, $rawdata);
        fclose($fp);

        return $path;
    }

    public static function getFilePath($file)
    {
        return RESOURCE_URL.'/'.$file;
    }

    public function deleteResource($resource)
    {
        $path = Yii::getPathOfAlias('uploads.resources').'/'.$resource->resource_path;
        if (file_exists($path)) {
            unlink($path);
        }
    }

    /**
     * Возвращает ссылку на изображение - превью видеоролика
     * @param  Zend_Gdata_YouTube_VideoEntry $entry обьект представляющий информацию о видео записи
     * @return string                        url картинки
     */
    public static function getYtVideoThumbnail($entry)
    {
        // TODO: возможность менять размеры и, как вариант, сохранение картинки на сайте
        //$thumbnails = $entry->getVideoThumbnails();
        return $entry->mediaGroup->thumbnail[0]->url;
    }

    /**
     * Возвращает обьект, описывающий YouTube видео
     *
     * @param  string                        $url url ролика
     * @return Zend_Gdata_YouTube_VideoEntry $entry обьект видео записи
     */
    public static function getYtVideoEntry($url)
    {
        $videoId = self::getYtVideoId($url);

        Yii::import('cms.vendors.*');
        require_once 'Zend/Loader.php';

        Zend_Loader::loadClass('Zend_Gdata_YouTube');

        $yt = new Zend_Gdata_YouTube();

        $entry = $yt->getVideoEntry($videoId);

        return $entry;
    }

    /**
     * Возвращает id видеоролика с youtube. URL
     * @param  string $url url видеоролика (например: http://www.youtube.com/watch?v=EB4ljWxG5P4)
     * @return string video id
     */
    public static function getYtVideoId($url)
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $params);

        return $params['v'];
    }
}
