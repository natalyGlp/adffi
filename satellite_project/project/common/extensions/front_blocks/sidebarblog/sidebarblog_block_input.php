<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'limit'),''); ?>
	<?php echo CHtml::textField("Block[limit]",$block_model->limit ,array('id'=>'Block-limit')); ?>
	<?php echo $form->error($model,'limit'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'title'),''); ?>
	<?php echo CHtml::textField("Block[title]",$block_model->title ,array('id'=>'Block-title')); ?>
	<?php echo $form->error($model,'title'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'title_link'),''); ?>
	<?php echo CHtml::textField("Block[title_link]",$block_model->title_link ,array('id'=>'Block-title_link')); ?>
	<?php echo $form->error($model,'title_link'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'link_title'),''); ?>
	<?php echo CHtml::textField("Block[link_title]",$block_model->link_title ,array('id'=>'Block-link_title')); ?>
	<?php echo $form->error($model,'link_title'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'link_href'),''); ?>
	<?php echo CHtml::textField("Block[link_href]",$block_model->link_href ,array('id'=>'Block-link_href')); ?>
	<?php echo $form->error($model,'link_href'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'content_type'),''); ?>
	<?php echo CHtml::dropDownList("Block[content_type]",$block_model->content_type ,
		GxcHelpers::getAvailableContentType(true),
	array('id'=>'Block-content_type')); ?>
	<?php echo $form->error($model,'content_type'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'cssclass'),''); ?>
	<?php echo CHtml::textField("Block[cssclass]",$block_model->cssclass,
	array('id'=>'Block-cssclass')); ?>
	<?php echo $form->error($model,'cssclass'); ?>
</div>
</div>
