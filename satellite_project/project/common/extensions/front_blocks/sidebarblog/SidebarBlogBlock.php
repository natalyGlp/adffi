<?php

/**
 * Class for render Block
 * 
 * 
 * @author Yura Kovalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package common.front_blocks.html
 */

class SidebarBlogBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='sidebarblog';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    
    //HTML attribute
    public $limit=3;    
    public $title='';    
    public $title_link='';    
    public $link_title='';    
    public $link_href='';    
    public $posts=array() ;  
    public $cssclass='';
    public $content_type='article';    
    /*ДОработка
    Сдлать выбор из категории а не из типа контента
    */
    public function setParams($params){
        $this->limit=isset($params['limit']) ? $params['limit'] : 3;
        $this->title=isset($params['title']) ? $params['title'] : '';
        $this->title_link=isset($params['title_link']) ? $params['title_link'] : '';
        $this->link_title=isset($params['link_title']) ? $params['link_title'] : '';
        $this->link_href=isset($params['link_href']) ? $params['link_href'] : '';
        $this->content_type=isset($params['content_type']) ? $params['content_type'] : 'article';
        $this->cssclass=isset($params['cssclass']) ? $params['cssclass'] : '';
    }
    
    public function run()
    {        
            $this->renderContent();
    }       
 
 
    protected function renderContent()
    {
	if(isset($this->block) && ($this->block!=null)){	    
           // var_dump($this->block->block_id); die();
            $params=unserialize($this->block->params);
	        $this->setParams($params);    
            $criteria=new CDbCriteria;
            $lang=Language::model()->findByAttributes(array('lang_name'=>Yii::app()->language));
            $criteria->condition='object_type=:obj_type AND object_status=:obj_status and lang=:lang';
            $criteria->limit=$this->limit;
            $criteria->params=array(':obj_type'=>$this->content_type, 'obj_status'=>ConstantDefine::OBJECT_STATUS_PUBLISHED, ':lang'=>$lang->lang_id);
            $this->posts=Object::model()->findAll($criteria); 
            $this->render(BlockRenderWidget::setRenderOutput($this),array());
	} else {
	    echo '';
	}
       
    }
    
    public function validate(){
        $errors=false;
    	if(!is_numeric($this->limit)){
    		$this->errors['limit']=t('Limit must be a numeric');
            $errors=true;
        }
        if(!is_string($this->title)){
            $this->errors['title']=t('Title must be a string');
            $errors=true;
        }
        if(!is_string($this->title_link)){
            $this->errors['title_link']=t('Title link must be a string');
            $errors=true;
        }
        if(!is_string($this->link_title)){
            $this->errors['link_title']=t('Link title must be a string');
            $errors=true;
        }
        if(!is_string($this->link_href)){
            $this->errors['link_href']=t('Link href must be a string');
            $errors=true;
        }
        if(!is_string($this->cssclass)){
            $this->errors['cssclass']=t('CSS Class href must be a string');
            $errors=true;
        }
        return !$errors;
    }    
    public function params()
    {
            return array(
                 'limit'=>'Article count',
                 'title'=>'Title',
                 'title_link'=>'Title link',
                 'title'=>'Title link',
                 'link_title'=>'Reed more text',
                 'link_href'=>'Reed more link',
                 'content_type'=>'Content type',
                 'cssclass'=>'CSS Class'
            );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
}

?>