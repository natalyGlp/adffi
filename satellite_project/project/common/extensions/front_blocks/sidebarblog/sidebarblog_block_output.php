<blog <? if ($this->cssclass!='') echo 'class="'.$this->cssclass.'"' ?>>
<? 
if (!empty($this->posts)) 
{ 
	if ($this->title!='') 
	{ echo '<p>';
		if ($this->title_link!='') 
		{
			echo CHtml::link($this->title,$this->title_link,array('class'=>'sidebar_blog_title'));
		} else 
		{
			echo $this->title;
		}
		echo '</p>';
	}
	foreach($this->posts as $p) 
	{
  		$link = $p->getObjectLink();
		?>
		<blockquote>
		<p style="margin-top:0">
		<? $thumb=GxcHelpers::getResourceObjectFromDatabase($p,'thumbnail');
		if (count($thumb)>=1) {
		echo CHtml::link(CHtml::image($thumb[0]['link'],$p->object_title,array('align'=>'left','style'=>'margin-right: 20px;')),Object::getLink($p->object_id)); } ?>
		<? echo CHtml::link($p->object_title,$link,array('class'=>'blog_link')) ?>
		</p>
</blockquote>
	<?}
	if (($this->link_title!='') && ($this->link_href!='')) 
	{
		echo CHtml::link($this->link_title,$this->link_href,array('class'=>'sidebar_reed_more'));
	}
}
 ?>
 </blog> 
