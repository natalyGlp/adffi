<?php
    $mycs=Yii::app()->getClientScript();                    
    if(YII_DEBUG)
        $ckeditor_asset=Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.ckeditor'), false, -1, true);                    
    else
        $ckeditor_asset=Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.ckeditor'), false, -1, false);                    
                    
    $urlScript_ckeditor= $ckeditor_asset.'/ckeditor.js';
    $urlScript_ckeditor_jquery=$ckeditor_asset.'/adapters/jquery.js';
    $urlScript_ckfinder= $ckeditor_asset.'/ckfinder/ckfinder.js';
    $mycs->registerScriptFile($urlScript_ckeditor, CClientScript::POS_HEAD);
    $mycs->registerScriptFile($urlScript_ckfinder, CClientScript::POS_HEAD);
    $mycs->registerScriptFile($urlScript_ckeditor_jquery, CClientScript::POS_HEAD);                    
?>

<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'html'),''); ?>
	<?php echo CHtml::textArea("Block[html]",$block_model->html ,array(
		'id'=>'ckeditor_content',
        'rows'=>15, 
        'cols'=>50, 
        'style'=>'width:90%'
    )); ?>
	<?php echo $form->error($model,'html'); ?>

</div>
</div>
<?php $this->render('cmswidgets.views.object.object_form_javascript',array('model'=>$model,'form'=>$form,'type'=>'','ckeditor'=>$ckeditor_asset)); ?>