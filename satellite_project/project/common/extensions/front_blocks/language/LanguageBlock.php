<?php

/**
 * Class for render Language checker
 * 
 * 
 * @author Yura KOvalenko <kimmongod@gmail.com>
 * @version 1.
 * @package common.front_blocks.language
 */

class LanguageBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='language';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    
    //HTML attribute
    //public $html;    
    
    
    
    public function setParams($params){
	  //  $this->html=isset($params['html']) ? $params['html'] : '';
    }
    
    public function run()
    {        
            $this->renderContent();
    }       
 
 
    protected function renderContent()
    {
	if(isset($this->block) && ($this->block!=null)){	    
           // var_dump($this->block->block_id); die();
            $params=unserialize($this->block->params);
	        $this->setParams($params);                            
            $this->render(BlockRenderWidget::setRenderOutput($this),array());
	} else {
	    echo '';
	}
       
    }
    
    public function validate(){
		return true ;
    }
    
    public function params()
    {
            return array(
            );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
}