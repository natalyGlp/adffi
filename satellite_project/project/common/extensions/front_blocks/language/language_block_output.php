 <ul class="<?=$this->cssClass?>">
 <?
  foreach (Yii::app()->translate->acceptedLanguages as $key => $value) {
		if (!isset($_GET['lang'])) 
		{
			$link='http://'.$_SERVER['SERVER_NAME'].Yii::app()->request->url.'?lang='.$key; 

		} else 
		{
			 Yii::app()->translate->setLanguage($_GET['lang']);
			 $link='http://'.$_SERVER['SERVER_NAME'].Yii::app()->request->url;
			 if (strpos($link,'lang='.Yii::app()->translate->getLanguage())) 
			 {
				$link=str_replace('lang='.Yii::app()->translate->getLanguage(),'lang='.$key, $link);
			 }
		}

		$linkClass = 'lang-'.Yii::app()->translate->getLanguage().($key==Yii::app()->translate->getLanguage()?' active':'');
		?>
		<li class="<?=$linkClass?>">
		<?= CHtml::link($value,$link) ?> 
		</li>
		<?php
	} 
	?>
</ul>
