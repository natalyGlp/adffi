<aside class="car-model">
    <div class="title">
		<?php
			$brand = $post->brand;
			$thumb = GxcHelpers::getResourceObjectFromDatabase($post,'thumbnail', 'detali_view_');
			if (count($thumb)>=1):?>
				<?php echo CHtml::image($thumb[0]['link'],$post->object_title,array('align'=>'center')); ?>
		<?php endif; ?>
    	<h2><?php echo $post->fullName;?></h2>
    	<div class="clear"></div>
    </div>
    <!-- Start tabs-standard -->
    
</aside>
<div class="sliderkit tabs-standard model-title" style="display: block;">
    <div class="sliderkit-nav">
            <div class="sliderkit-nav-clip">
              	<ul>
              		<?php $cn = get_class($post); if ($cn != 'ModelObject'): ?>
	                <li>
	                	<a href="
	                	<?php 
	                		echo $cn::getLink(
	                			($cn == 'ModelObject' ? $post->object_id :
	                			(isset($post->filters->filter1) ? $post->filters->filter1 : 0))
	                		);
	                	?>
	                	">
	                		Комплектации
	                	</a>
	                </li>
              		<?php endif ?>
	                <li><a href="<?php echo FRONT_SITE_URL.Yii::app()->language;?>/photos/<?php echo $post->object_type;?>/<?php echo $post->object_slug;?>.html">Фото</a></li>
	                <!-- <li><a href="#video">Видео</a></li>
	                <li><a href="#questions">Вопросы</a></li>
	                <li><a href="#comments">Отзывы</a></li> -->
	                <li><a href="#test-drive">Тест-драйв</a></li>
	                <li>
	                	<a href="
	                	<?php
	                		$cn = get_class($post);
	                		$pid = (
	                			$cn == 'ModelObject' 
	                			? $post->id_parent 
	                			:  Yii::app()->{CONNECTION_NAME}
	                							  ->createCommand()
	                							  ->select('id_parent')
	                							  ->from('{{object}}')
	                							  ->where('object_id=?', array((isset($post->filters->filter1) ? $post->filters->filter1 : 0)))
	                							  ->queryScalar()
	                		);
							
							if($pid){
	                			echo $cn::getLink($pid);
							}else{
								echo '#';
							}
	                	?>
	                	">
	                	История модели
	                	</a>
	                </li>
              	</ul>
            </div>
    </div>
</div>