<?php
/**
 * Class for render LogoSlider * 
 * 
 * @author Tuan Nguyen <voinovev@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.logo_slider */
class LogoSliderBlock extends CWidget
{

    //Do not delete these attr block, page and errors
    public $id = 'logo_slider';
    public $block = null;
    public $errors = array();
    public $page = null;
    public $layout_asset = '';
    public $class;
    public $picture;

    public function setParams($params)
    {
        $this->class = isset($params['class']) ? $params['class'] : '';
        $this->picture =  isset($params['picture'])  ? $params['picture'] : '';

        return;
    }

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        if (isset($this->block) && ($this->block != null))
        {
            //Start working with LogoSlider here
            $params = unserialize($this->block->params);
            $this->setParams($params);
            Yii::app()->clientScript->registerScript('logo-slider', '
                $(".photoslider-bullets").sliderkit({
                    auto:true,
                    circular:true,
                    mousewheel:false,
                    shownavitems:5,
                    panelfx:"sliding",
                    panelfxspeed:1000,
                    panelfxeasing:"easeOutExpo"
                });
            ');

            $gid = (int)Yii::app()->db->createCommand()->select(array('id'))->from('{{galleries}}')->where('path=?', array('slider'))->queryScalar();

            $this->render(BlockRenderWidget::setRenderOutput($this), array(
                'photos' => GalleryPhotos::model()->findAll(array(
                    'select' => array('img','ext', 'gallery_id', 'description'),
                    'condition'=>'gallery_id=:id', 
                    'params'=>array(':id'=>$gid)
                ))
            ));
        } else
        {
            echo '';
        }
    }

    public function validate()
    {
        return true;
    }

    public function params()
    {
        return array(
                    'picture' => t('Picture url'),
                    'class' => t('Css class')
            );
    }

    public function beforeBlockSave()
    {
        return true;
    }

    public function afterBlockSave()
    {
        return true;
    }

}