<aside class="sliderkit photoslider-bullets">
    <logoblock>
        <a href="/"><logo class="logo"></logo></a>
    </logoblock>
    <div class="sliderkit-nav">
        <div class="sliderkit-nav-clip">
            <ul>
                <?php for($i=0;$i<count($photos);$i++):?>
                    <li><a href="#"></a></li>
                <?php endfor;?>
            </ul>
        </div>
    </div>	
    <div class="sliderkit-panels">
        <?php foreach ($photos as $photo): ?>
            <div class="sliderkit-panel">
                <?php echo CHtml::image($photo->getImage('_thumb'),''); ?>
                <div class="sliderkit-panel-textbox">
                    <?php echo $photo->description;?>
                </div>
            </div>
        <?php endforeach ?>
    </div>
    <div class="slidershadow"></div>
</aside>