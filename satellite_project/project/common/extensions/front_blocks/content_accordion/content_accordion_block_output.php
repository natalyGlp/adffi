
<contentaccordion class="accordion<?php echo($this->class)?" ".$this->class:"";?>" id="Block<?=$this->block->block_id; ?>" style="display:block; overflow:hidden; width:<?=$this->width?>px; height:<?=$this->height;?>px;"> 
<?foreach ($this->content as $key => $value):?>
  <h3><a href="#"><?=$value->object_name;?></a></h3>
  <div><?=$value->object_content;?></div>
<?endforeach?>
</contentaccordion>

<script type="text/javascript">
$(function(){
  $(".accordion").accordion({
      fillSpace:true
  });
});
</script>
