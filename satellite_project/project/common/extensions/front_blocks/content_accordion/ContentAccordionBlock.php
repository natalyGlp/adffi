<?php

/**
 * Class for render Content Accordion * 
 * 
 * @author Bazai best programmer <developer@studiobanzai.com>
 * @version 0.1
 * @package common.front_blocks.content_accordion */
class ContentAccordionBlock extends CWidget {

    //Do not delete these attr block, page and errors
    public $id = 'content_accordion';
    public $block = null;
    public $errors = array();
    public $page = null;
    public $layout_asset = '';
    public $class;
    public $width = 960;
    public $height = 600;
    public $content_list;
    public $content = array();
    public $display_type;

    //Display types for the list view render 

    const DISPLAY_TYPE_HOMEPAGE = 0;
    const DISPLAY_TYPE_CATEGORY = 1;    

    public function setParams($params)
    {
        $this->class = isset($params['class']) ? $params['class'] : '';
        $this->width = isset($params['width']) ? $params['width'] : '960';
        $this->height = isset($params['height']) ? $params['height'] : '600';
        $this->display_type = isset($params['display_type']) ? $params['display_type'] : self::DISPLAY_TYPE_HOMEPAGE;
        $this->content_list = isset($params['content_list']) ? $params['content_list'] : null;
        
        return;
    }

    public function run()
    {  
        $this->renderContent();
    }

    protected function renderContent()
    {
        if (isset($this->block) && ($this->block != null))
        {
            //$url = Yii::app()->assetManager->publish(dirname(__FILE__) . '/bootstrap/');
            //Yii::app()->clientScript->registerCssFile($url . "/css/bootstrap.min.css");
            //Yii::app()->clientScript->registerScriptFile($url . "/js/bootstrap.min.js");
            
            $params = unserialize($this->block->params);
            $this->setParams($params);
            
          
            if ($this->content_list != null)
            foreach ($this->content_list as $id => $cont)
            {
                $content_list_data_provider = ContentAccordionBlock::getContentList($cont, null, null, ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD);
                $this->content = array_merge($this->content, $content_list_data_provider);
            }

            
            $this->render(BlockRenderWidget::setRenderOutput($this), array());
        } else  echo '';
        
    }

    public function validate()
    {
        return true;
    }

    public function params()
    {
        return array(
            'class' => t('Css class'),
            'width' => t('Width'),
            'height' => t('height'),
            'content_list' => t('Content list'),
            'display_type' => t('Display type'),            
        );
    }

    public function beforeBlockSave()
    {
        return true;
    }

    public function afterBlockSave()
    {
        return true;
    }
    
    public static function getDisplayTypes()
    {
        return array(
            self::DISPLAY_TYPE_HOMEPAGE => t("Display in Homepage"),
            self::DISPLAY_TYPE_CATEGORY => t("Display in Category page"));
    }    
    
    public static function getContentList($content_list_id, $max = null, $pagination = null, $return_type = ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD)
    {
        return GxcHelpers::getContentList($content_list_id, $max, $pagination, $return_type);
    }    

}

?>