<?php
/**
 * Class for render SlideRight * 
 * 
 * @author Tuan Nguyen <voinovev@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.slide_right */
class SlideRightBlock extends CWidget{
    //Do not delete these attr block, page and errors
    public $id = 'slide_right';
    public $block = null;
    public $errors = array();
    public $page = null;
    public $layout_asset = '';
    public $class;

    public function setParams($params){
        $this->class = isset($params['class']) ? $params['class'] : '';
        return;
    }

    public function run(){
        $this->renderContent();
    }

    protected function renderContent(){
        if (isset($this->block) && ($this->block != null)){
            $params = unserialize($this->block->params);
            $this->setParams($params);
            Yii::app()->clientScript->registerScript('slide_right', '
                var slide_to_right_i = 1,
                    slide_r_max = '.((int)Object::model()->count('t.object_type=?', array('publication'))).';
                $("#slide_to_right").click(function(){
                    if(slide_r_max <= slide_to_right_i){
                        slide_to_right_i = 0;
                    }

                    $.get("/ajax/ajaxsliderright/?i="+slide_to_right_i, function(data){
                        $("#right-slider-data-place").hide("slow", function(){
                            $(this).html(data).show("slow");
                        });
                    });
                    slide_to_right_i++;
                });
            ');

            $this->render(BlockRenderWidget::setRenderOutput($this), array(
                'data' => Object::model()->find(array(
                    'select' => array('object_id', 'object_content', 'object_title', 'object_date'),
                    'condition' => 't.object_type=?',
                    'params' => array('publication'),
                    'order' => 't.object_date DESC'
                ))
            ));
        } else {
            echo '';
        }
    }

    public function validate(){
        return true;
    }

    public function params(){
        return array(
            'class' => t('Css class')
        );
    }

    public function beforeBlockSave(){
        return true;
    }

    public function afterBlockSave(){
        return true;
    }
}