<table border="0" cellpadding="0" cellspacing="0" >
    <tbody>
        <tr>
            <td class="important-post-left">&nbsp;</td>
            <td class="important-post-box">
                <div id="right-slider-data-place">
                    <a href="<?php echo $data->getObjectLink();?>">
                    <?php $thumb=GxcHelpers::getResourceObjectFromDatabase($data,'thumbnail');
                    if (count($thumb)>=1) { ?>
                    <?php echo CHtml::image($thumb[0]['link'],$data->object_title,array('align'=>'left')); ?>
                    <?} ?>
                    <h2><?php echo $data->object_title; ?></h2></a>
                    <p><?php echo $data->getExcerpt(); ?></p>
                </div>
                <aside class="read-more-box"><a href="javascript://" id="slide_to_right" class="read-more"></a></aside>
            </td>
            <td class="important-post-right">&nbsp;</td>
        </tr>
    </tbody>
</table>