<?php  
class YoutubeGrabber extends CModel 
{
	public $streamlink;    
    public $first_count_video=10;
    public $more_count_video=3;
    public $width=300;
    public $height=200;
    public $videos= array();
    public $order= 'viewCount';
    public $link= 'thumb';

    public function rules() 
    {
    	return array(
    		array('width, height, more_count_video, first_count_video','numerical'),
            array('width, height, more_count_video, first_count_video, streamlink','required'),
    		);
    }
    public function attributeNames() 
    {
    	return array(
    		'width'=>t('asd'),
    		'height'=>t('Высота'),
    		);
    }
    public function attributeLabels() 
    {
    	return array(
    		'width'=>t('Width'),
    		'height'=>t('Height'),
    		);
    }
}