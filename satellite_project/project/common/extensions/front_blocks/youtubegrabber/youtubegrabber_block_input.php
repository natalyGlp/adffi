<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'streamlink'),''); ?>
	<?php echo CHtml::textField("Block[streamlink]",
				    $block_model->streamlink ,array('id'=>'Block-streamlink')); ?>
	<?php echo $form->error($model,'streamlink'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'first_count_video'),''); ?>
	<?php echo CHtml::textField("Block[first_count_video]",
				    $block_model->first_count_video ,array('id'=>'Block-first_count_video')); ?>
	<?php echo $form->error($model,'first_count_video'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'more_count_video'),''); ?>
	<?php echo CHtml::textField("Block[more_count_video]",
				    $block_model->more_count_video ,array('id'=>'Block-more_count_video')); ?>
	<?php echo $form->error($model,'more_count_video'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'width'),''); ?>
	<?php echo CHtml::textField("Block[width]",
				    $block_model->width ,array('id'=>'Block-width')); ?>
	<?php echo $form->error($model,'width'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'height'),''); ?>
	<?php echo CHtml::textField("Block[height]",
				    $block_model->height ,array('id'=>'Block-height')); ?>
	<?php echo $form->error($model,'height'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'order'),''); ?>
	<?php echo CHtml::dropDownList("Block[order]",
				    $block_model->order,$block_model->order_varios,array('id'=>'Block-order')); ?>
	<?php echo $form->error($model,'order'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'link'),''); ?>
	<?php echo CHtml::dropDownList("Block[link]",
				    $block_model->link,$block_model->link_varios,array('id'=>'Block-link')); ?>
	<?php echo $form->error($model,'link'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'style_class'),''); ?>
	<?php echo CHtml::textField("Block[style_class]",
				    $block_model->style_class ,array('id'=>'Block-style_class')); ?>
	<?php echo $form->error($model,'style_class'); ?>
</div>
</div>
