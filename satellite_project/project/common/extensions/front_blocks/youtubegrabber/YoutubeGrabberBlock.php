<?php

/**
 * Class for render HTML Content Block
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.html
 */

class YoutubeGrabberBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='youtubegrabber';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    public $style_class;
    public $streamlink;    
    public $first_count_video=10;
    public $more_count_video=3;
    public $width=300;
    public $height=200;
    public $videos= array();
    public $order= 'viewCount';
    public $link= 'thumb';
    public $order_varios=array('relevance'=>'Relevance','published'=>'Published date','viewCount'=>'View Count','rating'=>'Rating');
    public $link_varios=array('thumb'=>'Thumbnail','video'=>'Video embed');
    
    
    public function setParams($params){
        $this->streamlink=isset($params['streamlink']) ? $params['streamlink'] : $this->streamlink;
        $this->first_count_video=isset($params['first_count_video']) ? $params['first_count_video'] : $this->first_count_video;
        $this->more_count_video=isset($params['more_count_video']) ? $params['more_count_video'] : $this->more_count_video;
        $this->width=isset($params['width']) ? $params['width'] : $this->width;
        $this->height=isset($params['height']) ? $params['height'] : $this->height;
        $this->order=isset($params['order']) ? $params['order'] : $this->order;
        $this->link=isset($params['link']) ? $params['link'] : $this->link;
        $this->style_class=isset($params['style_class']) ? $params['style_class'] : '';
    }
    
    public function run()
    {        
        $url = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets/');
        Yii::app()->clientScript->registerCssFile($url."/video_style.css");
        $this->renderContent();
    }       
 
 
    protected function renderContent()
    {
        
        //$videoFeed = $yt->getuserUploads('jessiewaremusic');
        
	if(isset($this->block) && ($this->block!=null)){	    
            //Set Params from Block Params
            $params=unserialize($this->block->params);
	    $this->setParams($params);                            
        $this->videos=$this->getVideos();
        if (isset($_GET['page']) && isset($_GET['videofeed']) && ($_GET['videofeed']==1) && is_numeric($_GET['page'])) 
        {
            $this->videos=$this->getVideos(array('streamlink'=>$this->streamlink,'max-results'=>$this->more_count_video,'start-index'=>$this->first_count_video+$this->more_count_video*($_GET['page']-2)+1));
        }
            $this->render(BlockRenderWidget::setRenderOutput($this),array());
	} else {
	    echo '';
	}
       
    }
    
    public function validate(){
     //   var_dump($_POST['Block']);
    Yii::import('common.front_blocks.youtubegrabber.*');
    $model= new YoutubeGrabber; 
    $model->attributes=$_POST['Block'];
    if(!$model->validate()){
        foreach ($model->getErrors() as $key =>$errors){
        $this->errors[$key]=t($errors[0]); }
                return false ;
    }
    else
        return true ;
    }
    
    public function params()
    {
            return array(
                    'streamlink' => t('Link to yuotube stream'),                   
                    'first_count_video' => t('Count video'),                   
                    'more_count_video' => t('More count video'),                   
                    'width' => t('Width thumbnails'),                   
                    'height' => t('Height thumbnails'),                   
                    'order' => t('Order parameters'),                   
                    'link' => t('Thumbnail or video embed'),                   
                    'style_class' => t('CSS style class'),                   
            );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
    public function getVideos($params=array()) 
    {
        $deffparams=array(
            'user'=>$this->streamlink,
            'orderby'=>$this->order,
            'max-results'=>$this->first_count_video,
            'start-index'=>1
            );
        $params=CMap::mergeArray($deffparams,$params); 
        Yii::import('cms.vendors.*');
        require_once("Loader.php");
        Zend_Loader::loadClass('Zend_Gdata_YouTube');
        $yt = new Zend_Gdata_YouTube();
        $query = $yt->newVideoQuery();
        if (isset($params['user'])) $query->setAuthor($params['user']);
        $query->setMaxResults($params['max-results']);
        $query->setStartIndex($params['start-index']);
        $query->setOrderBy($params['orderby']);
        $videoFeed = $yt->getVideoFeed($query);
        $result=array();
          foreach ($videoFeed as $videoEntry) {
            //var_dump($videoEntry);
            $vid['thumb']=$videoEntry ->getVideoThumbnails();
            $vid['desc']=$videoEntry ->getVideoDescription();
            $vid['title']=$videoEntry ->getVideoTitle();
            $vid['url']=$videoEntry ->getVideoWatchPageUrl();
            if ($this->link=='video') 
            {
                $vid['video']='<iframe title="YouTube video player" width="'.$this->width.'" height="'.$this->height.'" 
                        src="'.$this->getVideoCode($vid['url']).'" frameborder="0" allowfullscreen></iframe>';
                                    }
            $result[]=$vid;
           // var_dump($vid);
          }
          return $result;
          //var_dump(count($result));
               }
protected function getVideoCode($url)  
{
    $code='';
    $get_vars  = explode('&', html_entity_decode(parse_url($url, PHP_URL_QUERY)));
    foreach($get_vars as $var) {
        $exploded_var = explode('=', $var);
        if ($exploded_var[0] === 'v')
            $code=$exploded_var[1];
}
if ($code!='') 
{
    return "http://www.youtube.com/embed/$code";
}
}
}
?>