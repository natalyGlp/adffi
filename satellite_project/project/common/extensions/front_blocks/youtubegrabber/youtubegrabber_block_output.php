<? if (count($this->videos)>=1) { ?><videos class="videostream-wrapper <? if ($this->style_class!='') echo $this->style_class; ?>">
<script>
jQuery(document).ready(function($) {
	var page=1;
	$('#more<?=$this->block->block_id;?>').click(function(event)
	{
		event.preventDefault();
		page=page+1;
		$('#moreloader<?=$this->block->block_id;?>').css('display','inline-block');
		$.ajax({
  		url: '?page='+page+'&videofeed=1',
		  success: function(data) {
		  	$('#moreloader<?=$this->block->block_id;?>').css('display','none');
		  	if ($('#videostream<?=$this->block->block_id;?>',$('div'+data+'</div>')).length!=0) 
		  	{
		  		$('#videostream<?=$this->block->block_id;?> .clear').remove();
		  	}
    	$('#videostream<?=$this->block->block_id;?>').append($('#videostream<?=$this->block->block_id;?>',$('div'+data+'</div>')));
  		},

		});
	});
});

</script>
<div id="videostream<?=$this->block->block_id;?>" class="videostream">
	<? foreach($this->videos as $vid) 
	{	?>
		<div class="videostream_item" style="width:<?=$this->width; ?>px">
			<div class="videostream-title">
				<? 	echo $vid['title']; ?>
			</div>	
			<div class="cideostream-video">
				<? 
				if($this->link=='video')   {
					echo $vid['video'];

				} else {
				echo CHtml::link(CHtml::image($vid['thumb']['0']['url'],$vid['title'],array('width'=>$this->width,'height'=>$this->height)),$vid['url'],array('target'=>'_blank')); } ?>
		  	</div>
		  	<div class="videostream-desc">
				<? 	echo $vid['desc']; ?>
			</div>	
		</div>
	<?} ?>
    <div class="clear"></div>
</div>
<div class="morebtn_cont">
	<span id="moreloader<?=$this->block->block_id;?>" class="more_loader">Loading...</span>
	<a id="more<?=$this->block->block_id;?>" class="more_btn" href="#">More video</a>
</div>
</videos>
<? } ?>
