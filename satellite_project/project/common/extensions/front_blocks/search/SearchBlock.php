<?php

/**
 * Class for render search * 
 * 
 * @author Sviat <dev@udf.su>
 * @version 1.0
 * @package common.front_blocks.search 
 */

Yii::import('common.front_blocks.listview.ListViewBlock');
class SearchBlock extends ListViewBlock {

	//Do not delete these attr block, page and errors
	public $id = 'search';
	public $allowSearch = true;
	
	public function run()
	{
		if(!isset($_GET['q']) && !isset($_GET['date']) && !isset($_GET['term']) && !isset($_GET['tag']) && !isset($_GET['author_id']))
			throw new CHttpException('404',t('Oops! Page not found!'));

		parent::run();
	}
}

?>