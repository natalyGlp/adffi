<?php

/**
 * Class for render Comment List * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.comment_list */

class CommentListBlock extends CWidget
{
	
	//Do not delete these attr block, page and errors
	public $id='comment_list';
	public $block=null;     
	public $errors=array();
	public $page=null;
	public $layout_asset='';

	public $cssClass = '';
	
	/**
	 * $object_id The id of the content to which the searched comments belong.
	 * @var int
	 */
	public $object_id;
		
	
	public function setParams($params){
		  return; 
	}
	
	public function run()
	{                 
		   $this->renderContent();         
	}       
 
 
	protected function renderContent()
	{     
	   if(isset($this->block) && ($this->block!=null)){
				//Start working with Comment List here
				$params=unserialize($this->block->params);
				$this->setParams($params);  

				$this->render(BlockRenderWidget::setRenderOutput($this),array(
					'dataProvider' => self::getCommentList(),
					));                                                          	       		     
		} else {
			echo '';
		}
			  
	   
	}
	
	public function validate(){	
		return true ;
	}
	
	public function params()
	{
		 return array();
	}
	
	public function beforeBlockSave(){
	return true;
	}
	
	public function afterBlockSave(){
	return true;
	}

	public function getCssClass($class)
	{
		return $class . ' ' . $this->cssClass . '_' . $class;
	}
	
	/**
	 * Get the comment list of the object or current page by `slug` or `objectSlug`
	 * @return CActiveDataProvider
	 */
	public static function getCommentList()
	{
		$controller = Yii::app()->controller;
		$object = $controller->object;
		$allowComments = false;
		if($object)
		{
			$allowComments = $object->comment_status == ConstantDefine::OBJECT_ALLOW_COMMENT;
			$guid = $object->guid;
		}else
		{
			// checking if we have the page with the $slug
			$guid = $controller->page->guid;
			$allowComments = true;
		}

		if(isset($guid) && $allowComments)
		{

				//Search for the PUBLISHED comments that belong to the $object_id
				$dataProvider = new CActiveDataProvider('Comment',array(
					'criteria'=>array(
						'condition'=>'t.parent_guid = :guid and t.status = :status',
						'params'=>array(
							':guid'=>$guid,
							':status'=>ConstantDefine::COMMENT_STATUS_PUBLISHED,
						),
					),
				));

				return $dataProvider;
		}
		//return null if object not found or not allowed for comment
		return null;
	}
	
}

?>