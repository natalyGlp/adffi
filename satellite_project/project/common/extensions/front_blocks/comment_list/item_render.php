<?php
	$user = User::model()->findByEmail($data->email);
?>
<article class="<?= $this->getCssClass('comment'); ?>">
	<header>
		<?= CHtml::image(GxcHelpers::getUserAvatar('96',$user->avatar,$this->layout_asset.'/images/avatar.png'), $data->author_name);?>
		<h2><?= $data->author_name ?></h2>
		<p class="date"><?= $data->createTime; ?></p>
	</header>
	<div class="<?= $this->getCssClass('comment_content'); ?>">
		<?php echo $data->content; ?>
	</div>

	<div class="row button">
		<?php 
		//Reply button for each comment : 
		//when button clicked, the field content of comment form will be focused
		//and pre-filled by the text : @ + [author_name being replied]
			$this->widget('zii.widgets.jui.CJuiButton', array(
  			'id'=>'btReply'.$data->comment_id,
  			'buttonType'=>'submit',
 			'name'=>$data->author_name,
  			'caption'=>'Reply',
 			'options'=>array('icons'=>'js:{primary:"ui-icon-wrench"}'),
  			'onclick'=>'js:function(){
					var name = $(this).attr("name");
					$("#Comment_content").focus();
					$("#Comment_content").val("@"+name+":\n");
					return false;
					}',
 			));
		?>
	</div>
</article>