<!-- 
- Display the comment list of the given object
- Display a form that allow users to give their comment -->

<?php 

//Display the list if its itemCount > 0
if (isset($dataProvider) && $dataProvider != null) : ?>

<?php if($dataProvider->getItemCount(false) > 0): ?>
	<section class="<?= $this->getCssClass('comments_container'); ?>">
	<h2><?php echo t('Comments'); ?></h2>
	<?php 
	$this->widget('zii.widgets.CListView',
		array
		(
			'viewData'=>array('asset'=>$this->layout_asset),
			'dataProvider'=>$dataProvider,
			'itemView'=>'common.front_blocks.comment_list.item_render',
			'summaryText'=>'',
			'ajaxUpdate'=>true,
			'enablePagination'=> true,
			'enableSorting'=>false,
			'sortableAttributes'=>array(),
			)
		);   

		?>
	<?php endif; ?> 
	</section>

	<h2><?php echo t('Enter a comment'); ?></h2>
	<?php 
		if(Yii::app()->user->isGuest)
			$this->widget('cms.extensions.hoauth.widgets.HOAuth', array(
				'onlyIcons' => true,
				'route' => 'app',
			)); 
		else
		{
			// TODO: нужно вставить сюда блок (к примеру signinajax)
			echo t('You logged in as ' . Yii::app()->user->name) . ' (' . CHtml::link(t('Logout'), '/app/logout') . ')';
		}
	?>
	<?php $this->widget('ModelCreateWidget', array('model_name'=>'Comment',));?>

<?php endif;?>	