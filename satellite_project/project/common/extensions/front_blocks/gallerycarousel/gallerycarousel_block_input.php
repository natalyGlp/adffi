<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'width'),''); ?>
    <?php echo CHtml::textField("Block[width]",$block_model->width,array('id'=>'Block-width')); ?>
    <?php echo $form->error($model,'width'); ?>
</div>
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'full_height'),''); ?>
    <?php echo CHtml::textField("Block[full_height]",$block_model->full_height,array('id'=>'Block-full_height')); ?>
    <?php echo $form->error($model,'full_height'); ?>
</div>
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'nav_height'),''); ?>
    <?php echo CHtml::textField("Block[nav_height]",$block_model->nav_height,array('id'=>'Block-nav_height')); ?>
    <?php echo $form->error($model,'nav_height'); ?>
</div>
<div class="accordion" id="slide-accordion">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#slide-accordion" href="#collapseOne">
        Advanced options
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse">
      <div class="accordion-inner">
            <div class="row">
                <?php echo CHtml::label(Block::getLabel($block_model,'class'),''); ?>
                <?php echo CHtml::textField("Block[class]",$block_model->class,array('id'=>'Block-class')); ?>
                <?php echo $form->error($model,'class'); ?>
            </div>
            <div class="row">
                <?php echo CHtml::label(Block::getLabel($block_model,'autospeed'),''); ?>
                <?php echo CHtml::textField("Block[autospeed]",$block_model->autospeed,array('id'=>'Block-autospeed')); ?>
                <?php echo $form->error($model,'autospeed'); ?>
            </div>
            <div class="row">
                <?php echo CHtml::label(Block::getLabel($block_model,'panelfxspeed'),''); ?>
                <?php echo CHtml::textField("Block[panelfxspeed]",$block_model->panelfxspeed,array('id'=>'Block-panelfxspeed')); ?>
                <?php echo $form->error($model,'panelfxspeed'); ?>
            </div>
            <div class="row">
                <?php echo CHtml::label(Block::getLabel($block_model,'navitemscount'),''); ?>
                <?php echo CHtml::textField("Block[navitemscount]",$block_model->navitemscount,array('id'=>'Block-navitemscount')); ?>
                <?php echo $form->error($model,'navitemscount'); ?>
            </div>
            <div class="row">
                    <?php    echo CHtml::label(Block::getLabel($block_model,'autoplay'),'Block[autoplay]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[autoplay]', $block_model->autoplay); ?>
                <?php echo $form->error($model,'autoplay'); ?>
            </div>
            <div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'circular'),'Block[circular]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[circular]', $block_model->circular); ?>
                <?php echo $form->error($model,'circular'); ?>
            </div>
            <div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'mousewheel'),'Block[mousewheel]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[mousewheel]', $block_model->mousewheel); ?>
                <?php echo $form->error($model,'mousewheel'); ?>
            </div>
            <div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'navitemshover'),'Block[navitemshover]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[navitemshover]', $block_model->navitemshover); ?>
                <?php echo $form->error($model,'navitemshover'); ?>
            </div>
            <div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'panelfx'),'Block[panelfx]',array('class'=>'inline')); ?>
                <?php  echo CHtml::dropDownList('Block[panelfx]', $block_model->panelfx, $block_model->getSlideEffects(),array('id'=>'Block-panelfx')); ?>
                <?php echo $form->error($model,'panelfx'); ?>
            </div>
            <div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'showtitle'),'Block[showtitle]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[showtitle]', $block_model->showtitle); ?>
                <?php echo $form->error($model,'showtitle'); ?>
            </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
    <div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'gal_id'),''); ?>
    <?php echo CHtml::dropDownList("Block[gal_id]",$block_model->gal_id, 
                    Chtml::listData(Galleries::model()->findAll(),'id','title') ,array('id'=>'Block-gal_id',
                          )); ?>
    <?php echo $form->error($model,'gal_id'); ?>
</div>  
 
</div>        
</div>
