<!-- Slider Kit compatibility -->
<!--link rel="stylesheet" type="text/css" href="http://cms.webtests.in.ua/contentslider/sliderkit-demos.css" /-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentsldier/sliderkit-demos-ie6.css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentsldier/sliderkit-demos-ie7.css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentsldier/sliderkit-demos-ie8.css" /><![endif]-->

<aside class="sliderkit carousel-demo2">
    <h2>ПАРТНЁРЫ</h2>
    <div class="sliderkit-nav">
        <div class="sliderkit-nav-clip">
            <ul>
                <?php foreach ($photos as $key => $value): ?>
                <li>
                     <?php echo CHtml::image($value->getImage('_thumb'),''); ?>
                </li>
                <?php endforeach;?>
            </ul>
        </div>
        <div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-prev"><a href="#"><span>Previous</span></a></div>
        <div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-next"><a href="#"><span>Next</span></a></div>
    </div>
</aside>  