<div class="horizontal_slider_container">
<?php 
$block_id='Block'.$this->block->block_id;
?>
	<?php if ($this->content_list != null) : ?>
		<?php 
        $content = array();   	
				foreach ($this->content_list as $id => $cont) {
          $content_list_data_provider = ContentSliderBlock::getContentList($cont, null , null, ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD);
          $content = array_merge($content,$content_list_data_provider);
        }
                $count=count($content);
                $item_width=(int)$this->width/$count;
                $height=$this->full_height;
                $content_height=$this->full_height-$this->nav_height;
                $nav_height=$this->nav_height;
  ?>       
  <?php endif;
   ?>
<script>
$(document).ready(function()
{
  $("#<? echo $block_id; ?>.newslider-horizontal").sliderkit({
          auto: <? if ($this->autoplay) echo 'true'; else echo 'false'; ?>  ,
          autospeed: <? echo $this->autospeed; ?>,
          circular: <? if ($this->circular) echo 'true'; else echo 'false'; ?>,
          shownavitems: <? echo $count ?>,
          panelfx: '<? echo $this->panelfx; ?>',
          panelfxspeed:<? echo $this->panelfxspeed; ?>,
          mousewheel: <? if ($this->mousewheel) echo 'true'; else echo 'false'; ?>,
          navitemshover: <? if ($this->navitemshover) echo 'true'; else echo 'false'; ?>,
          fastchange:false
        }); 
});
</script>
<!-- Slider Kit compatibility -->
    <script type="text/javascript" src="http://shared.webtests.in.ua/contentsldier/html5.js"></script>
    <link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentsldier/sliderkit-demos.css" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentsldier/sliderkit-demos-ie6.css" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentsldier/sliderkit-demos-ie7.css" /><![endif]-->
    <!--[if IE 8]><link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentsldier/sliderkit-demos-ie8.css" /><![endif]-->
<contentslider  id="<? echo $block_id; ?>" class="sliderkit newslider-horizontal <? if ($this->class!='') echo $this->class; ?>" style="width: <? echo $this->width; ?>px; height: <?= $height; ?>px">
          <!--div class="sliderkit-nav" style="width: <? echo $this->width; ?>px; height: <?= $nav_height; ?>px">
            <div class="sliderkit-nav-clip" style="width: <? echo $this->width; ?>px; height: <?= $nav_height; ?>px">
              <ul style="width: <? echo $this->width; ?>px;">
                <? foreach ($content as $key => $value) { ?>
                <li style="width: <? echo $item_width; ?>px;"><a href="#" title="[link title]"><? echo $value->object_name; ?></a></li>
                <? } ?>
              </ul>
            </div>
          </div!-->
          <div class="sliderkit-panels" style="width: <? echo $this->width; ?>px; height: <?= $content_height; ?>px">
            <? foreach ($content as $key => $value) { ?>
            <div class="sliderkit-panel" style="width: <? echo $this->width-20; ?>px; height: <?= $content_height-20; ?>px;">
              <div class="sliderkit-news">
                <a href="<? echo $value->getObjectLink() ?>">
                  <? 
                  $list_current_resource=GxcHelpers::getResourceObjectFromDatabase($value,'thumbnail');
                if (count($list_current_resource)>=1) 
                {
                  echo CHtml::image($list_current_resource[0]['link'],$value->object_name); 
                } ?>
                  
                <h3><? echo $value->object_name; ?></h3>
                <? echo $value->object_content; 
                                 ?>
              </a>
              </div>
            </div>
            <? } ?>
            </div>
            <div class="sliderkit-btn sliderkit-go-btn sliderkit-go-prev"><a href="#" ><span>Previous</span></a></div>
            <div class="sliderkit-btn sliderkit-go-btn sliderkit-go-next"><a href="#" ><span>Next</span></a></div>
</contentslider>
</div>