<?php

class ContentSlider extends CModel {
    
    public $width = 900;
    public $full_height = 200;
    public $nav_height = 50;
    public $class = '';
    
    public $autoplay = true; 
    public $circular = true;
    public $mousewheel = true;
    public $navitemshover = false;
    public $panelfx = 'sliding';
    public $panelfxspeed = '1000';
    public $autospeed = 1000;

    
    

    public function rules()
    {
        return array(
            array('width, full_height, nav_height, panelfxspeed, autospeed', 'numerical'),
            array('autospeed', 'numerical', 'min' => '1000', 'max' => '100000'),
            array('panelfxspeed', 'numerical', 'min' => '100', 'max' => '10000'),
            array('width, nav_height,full_height', 'required'),
            array('autoplay, circular, mousewheel, navitemshover', 'boolean', 'allowEmpty' => true)
        );
    }

    public function attributeNames()
    {
        return array(
            'width' => t('Width'),
            'height' => t('Height'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'width' => t('Width'),
            'height' => t('Height'),
        );
    }

}