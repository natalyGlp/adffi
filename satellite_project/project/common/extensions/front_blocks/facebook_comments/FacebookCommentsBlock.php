<?php
/**
 * Class for render Facebook Comments * 
 * 
 * @author Bazai best programmer <developer@studiobanzai.com>
 * @version 0.1
 * @package common.front_blocks.facebook_comments */

class FacebookCommentsBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='facebook_comments';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';
    public $class;
    public $width;
    public $perpage;




    public function setParams($params)
    {
        $this->class = isset($params['class']) ? $params['class'] : '';
        $this->width = isset($params['width']) ? $params['width'] : '';
        $this->perpage = isset($params['perpage']) ? $params['perpage'] : '';
          return; 
    }
    
    public function run()
    {                 
           $this->renderContent();         
    }       
 
 
    protected function renderContent()
    {     
       if(isset($this->block) && ($this->block!=null)){
       			//Start working with Facebook Comments here
				$params=unserialize($this->block->params);
	    		$this->setParams($params);                            
            	$this->render(BlockRenderWidget::setRenderOutput($this),array());                                                          	       		     
		} else {
			echo '';
		}
			  
       
    }
    
    public function validate(){	
        if ($this->width == "")
        {
            $this->errors['width'] = t('Please set the width');
            return false;
        }
        else 
        if ($this->perpage == "")
        {
            $this->errors['perpage'] = t('Please select the comments per page count');
            return false;
        }
        else
            return true;
    }
    
    public function params()
    {
         return array(
            'class' => t('Css class'),
            'width' => t('Width'),
            'perpage' => t('Comments per page'),
         );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
	
	
}

?>