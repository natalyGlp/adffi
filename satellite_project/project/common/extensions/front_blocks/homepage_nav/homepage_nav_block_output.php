<aside class="services-box">
    <blockquote class="service tm">
        <a href="/">
            <div class="img-area">
                <img src="/images/tm.png">
            </div>
            <h2>TradeMarker</h2>
        </a>
    </blockquote>
    <blockquote class="service note">
        <a href="/ru_ru/registration">
            <div class="img-area">
                <img src="/images/<?php echo Yii::app()->controller->region==2 ? 'sidebar-' : '';?>note.png">
            </div>
            <h2>Регистрация<br> торговой марки</h2>
        </a>
    </blockquote>
    <blockquote class="service calc">
        <a href="/ru_ru/calculator">
            <div class="img-area">
                <img src="/images/<?php echo Yii::app()->controller->region==2 ? 'sidebar-' : '';?>calc.png">
            </div>
            <h2>Тарифный<br> калькулятор</h2>
        </a>
    </blockquote>
  </aside>