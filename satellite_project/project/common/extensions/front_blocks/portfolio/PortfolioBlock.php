<?php
/**
 * Class for render Portfolio * 
 * 
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.portfolio */
class PortfolioBlock extends CWidget
{

    //Do not delete these attr block, page and errors
    public $id = 'portfolio';
    public $block = null;
    public $errors = array();
    public $page = null;
    public $layout_asset = '';
    public $class;
    public $picture;

    public function setParams($params)
    {
        $this->class = isset($params['class']) ? $params['class'] : '';
        $this->picture =  isset($params['picture'])  ? $params['picture'] : '';

        return;
    }

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        if (isset($this->block) && ($this->block != null))
        {
            //Start working with Logo here
            $params = unserialize($this->block->params);
            $this->setParams($params);
            $posts=array();
            $posts=Object::model()->findAllByAttributes(array('object_type'=>'portfolio','object_status'=>ConstantDefine::OBJECT_STATUS_PUBLISHED));
            $this->render(BlockRenderWidget::setRenderOutput($this), array('posts'=>$posts));
        } else
        {
            echo '';
        }
    }

    public function validate()
    {
        return true;
    }

    public function params()
    {
        return array(
                    'picture' => t('Picture url'),
                    'class' => t('Css class')
            );
    }

    public function beforeBlockSave()
    {
        return true;
    }

    public function afterBlockSave()
    {
        return true;
    }

}

?>