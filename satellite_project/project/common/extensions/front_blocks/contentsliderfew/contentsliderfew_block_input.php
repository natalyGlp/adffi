<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'width'),''); ?>
    <?php echo CHtml::textField("Block[width]",$block_model->width,array('id'=>'Block-width')); ?>
    <?php echo $form->error($model,'width'); ?>
</div>
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'full_height'),''); ?>
    <?php echo CHtml::textField("Block[full_height]",$block_model->full_height,array('id'=>'Block-full_height')); ?>
    <?php echo $form->error($model,'full_height'); ?>
</div>
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'nav_height'),''); ?>
    <?php echo CHtml::textField("Block[nav_height]",$block_model->nav_height,array('id'=>'Block-nav_height')); ?>
    <?php echo $form->error($model,'nav_height'); ?>
</div>
<div class="accordion" id="slide-accordion">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#slide-accordion" href="#collapseOne">
        Advanced options
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse">
      <div class="accordion-inner">
            <div class="row">
                <?php echo CHtml::label(Block::getLabel($block_model,'class'),''); ?>
                <?php echo CHtml::textField("Block[class]",$block_model->class,array('id'=>'Block-class')); ?>
                <?php echo $form->error($model,'class'); ?>
            </div>
            <div class="row">
                <?php echo CHtml::label(Block::getLabel($block_model,'autospeed'),''); ?>
                <?php echo CHtml::textField("Block[autospeed]",$block_model->autospeed,array('id'=>'Block-autospeed')); ?>
                <?php echo $form->error($model,'autospeed'); ?>
            </div>
            <div class="row">
                <?php echo CHtml::label(Block::getLabel($block_model,'count'),''); ?>
                <?php echo CHtml::textField("Block[count]",$block_model->count,array('id'=>'Block-count')); ?>
                <?php echo $form->error($model,'count'); ?>
            </div>
            <div class="row">
                <?php echo CHtml::label(Block::getLabel($block_model,'panelfxspeed'),''); ?>
                <?php echo CHtml::textField("Block[panelfxspeed]",$block_model->panelfxspeed,array('id'=>'Block-panelfxspeed')); ?>
                <?php echo $form->error($model,'panelfxspeed'); ?>
            </div>
            <div class="row">
                    <?php    echo CHtml::label(Block::getLabel($block_model,'autoplay'),'Block[autoplay]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[autoplay]', $block_model->autoplay); ?>
                <?php echo $form->error($model,'autoplay'); ?>
            </div>
            <div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'circular'),'Block[circular]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[circular]', $block_model->circular); ?>
                <?php echo $form->error($model,'circular'); ?>
            </div>
            <div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'mousewheel'),'Block[mousewheel]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[mousewheel]', $block_model->mousewheel); ?>
                <?php echo $form->error($model,'mousewheel'); ?>
            </div>
            <div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'navitemshover'),'Block[navitemshover]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[navitemshover]', $block_model->navitemshover); ?>
                <?php echo $form->error($model,'navitemshover'); ?>
            </div>
            <div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'panelfx'),'Block[panelfx]',array('class'=>'inline')); ?>
                <?php  echo CHtml::dropDownList('Block[panelfx]', $block_model->panelfx, $block_model->getSlideEffects(),array('id'=>'Block-panelfx')); ?>
                <?php echo $form->error($model,'panelfx'); ?>
            </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
<div class="content-box ">
        <div class="content-box-header">
        <h3><?php echo t('Content list');?></h3>                                
        </div> 
        <div class="content-box-content" style="display: block; padding: 0 0 15px 0">
                <div class="tab-content default-tab">
                            <ul id="content_list" style="margin:0px 0 10px 0px">
                            </ul>
                </div>                                                
        </div>
</div>  
 
</div>        
<p><?php echo '<b>'.t('Note:').'</b> '.t('When you create a content list here, it will appear on the above Content list box'); ?></p>
<div class="row" style="border:1px dotted #CCC">
        
      <iframe id='contentlist_iframe'  src="<?php echo Yii::app()->request->baseUrl;?>/becontentlist/create/embed/iframe" frameborder="0" onLoad="autoResize(this);" height="30px" width="100%"></iframe>
</div>           
<script type="text/javascript">
     <?php  if ((is_array($block_model->content_list)) && (!empty($block_model->content_list))) : ?>               
            
             <?php       
                    $content_items=array();
                    foreach($block_model->content_list as $obj_id){
                        $temp_object=ContentList::model()->findByPk($obj_id);
                        if($temp_object){
                            $content_items['item_'.$temp_object->content_list_id]['id']=$temp_object->content_list_id;
                            $content_items['item_'.$temp_object->content_list_id]['title']=$temp_object->name;
                        }
                    }

                    echo 'var manual_content_list = '.json_encode($content_items).';'; 
             ?>
                    $.each(manual_content_list, function(k,v) {                        
                             addContentList(v.title,v.id);              
                    }); 
                           
     <?php endif;  ?>
     function addContentList(linkTitle,linkId){
         if($('#item_'+linkId).length>0){             
             $('#input_title_item_'+linkId).val(linkTitle);
             $('#link_item_title_item_'+linkId).html(linkTitle);
         } else {
            var nextli='item_'+linkId;
            var li='<li id=\"'+nextli+'\"><input type=\"hidden\" name=\"content_list_title[]\" id=\"input_title_'+nextli+'\" value=\"'+linkTitle+'\" /><input type=\"hidden\" name=\"Block[content_list][]\" id=\"input_id_'+nextli+'\" value=\"'+linkId+'\" /><a id=\"link_item_title_'+nextli+'\" href=\"javascript:void(0);\" onClick=\"updateContentList(\''+linkId+'\');\" target="_blank">'+linkTitle+'</a> - <a href=\"javascript:void(0);\" onClick=\"deleteContentList(\''+nextli+'\');\">Delete</a></li>';        
            $('#content_list').append(li);
            return;
         }
     }
     function deleteContentList(id){        
         $("#"+id).remove();
     }
     $('#content_list').sortable();
     function updateContentList(id) {
        $('#contentlist_iframe').attr('src','<?php echo Yii::app()->request->baseUrl;?>/becontentlist/update/id/'+id+'/embed/iframe');
     }
      //Function to handle close Iframe from  Tree Form, update item when adding 
     function resetIframe() {         
        $('#contentlist_iframe').attr('src','<?php echo Yii::app()->request->baseUrl;?>/becontentlist/create/embed/iframe');
     } 
</script>
</div>
