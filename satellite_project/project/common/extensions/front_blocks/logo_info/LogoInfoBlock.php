<?php

/**
 * Class for render Logo and Info Block
 * 
 * 
 * @author Tuan Nguyen <voinovev@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.logo_info
 */
class LogoInfoBlock extends CWidget {

    //Do not delete these attr block, page and errors
    public $id = 'logo_info';
    public $block = null;
    public $errors = array();
    public $page = null;
    public $layout_asset = '';
    public $class;
    public $picture;
    public $social_share;

    public function setParams($params){
        $this->class = isset($params['class']) ? $params['class'] : '';
        $this->picture = isset($params['picture']) ? $params['picture'] : '';
        $this->social_share = isset($params['social_share']) ? $params['social_share'] : '';

        return;
    }

    public function run(){
        $this->renderContent();
    }

    protected function renderContent(){
        if (isset($this->block) && ($this->block != null)){
            //Set Params from Block Params
           // var_dump($this->block->params); die();
            $params = unserialize($this->block->params);
            $this->setParams($params);
            $this->render(BlockRenderWidget::setRenderOutput($this), array(
                'social_share' => $this->social_share
            ));
        } else {
            echo '';
        }
    }

    public function validate(){
        return true;
    }

    public function params(){
        return array(
            'picture' => t('Picture url'),
            'class' => t('Css class'),
            'social_share' => t('Social share')
        );
    }

    public function beforeBlockSave(){
        return true;
    }

    public function afterBlockSave(){
        return true;
    }
}