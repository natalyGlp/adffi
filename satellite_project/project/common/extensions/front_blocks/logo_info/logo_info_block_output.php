<logoblock>
    <a href="/">
        <logo class="logo" style="background: url("<?php echo Yii::app()->settings->get('general', 'logo');?>") no-repeat;">
	        <slogan><?php echo Yii::app()->settings->get('general', 'slogan');?></slogan>    
        </logo>       
    </a>   
</logoblock>