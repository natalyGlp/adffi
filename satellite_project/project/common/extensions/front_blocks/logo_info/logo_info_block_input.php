
<div name="div-block-content-<?php echo $block_model->id;?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'picture'), ''); ?>
        <?php 
            echo CHtml::textField(
                "Block[picture]",
                $block_model->picture,
                array('id' => 'Block-picture')
            );
        ?>
        <?php echo $form->error($model, 'class'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'class'), ''); ?>
        <?php 
            echo CHtml::textField(
                "Block[class]",
                $block_model->class,
                array('id' => 'Block-class')
            );
        ?>
        <?php echo $form->error($model, 'class'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'social_share'), ''); ?>
        <?php 
            echo CHtml::textArea(
                "Block[social_share]",
                $block_model->class,
                array('id' => 'Block-social_share')
            );
        ?>
        <?php echo $form->error($model, 'social_share'); ?>
    </div>
</div>
