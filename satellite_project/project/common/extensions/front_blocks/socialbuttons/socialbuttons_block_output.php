<socialbuttons<?php echo($this->class) ? " class=\"" . $this->class . "\"" : ""; ?>>
        <?php
        $line = trim($this->social_email);
        if ($line)
            echo '<a target="blank" href="maito:' . $line . '"><div id="soc-e-mail">'.$line. '</div></a>';
        ?>
        <?php
        if ($this->need_fb)
        { $line = trim(settings()->get('social', 'facebook'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-fb">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_gp)
        {
            $line = trim(settings()->get('social', 'gplus'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-gp">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_yt)
        {
            $line = trim(settings()->get('social', 'youtube'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-yt">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_od)
        {
            $line = trim(settings()->get('social', 'odnoklassniki'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-od">' . '</div></a>';
        }
        ?>  
        <?php
        if ($this->need_sk)
        {
            $line = trim(settings()->get('social', 'skype'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-sk">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_ld)
        {
            $line = trim(settings()->get('social', 'linkedin'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-ld">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_tw)
        {
            $line = trim(settings()->get('social', 'twitter'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-tw">' . '</div></a>';
        }
        ?>   
        <?php
        if ($this->need_vk)
        {
            $line = trim(settings()->get('social', 'vkontakte'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-vk">' . '</div></a>';
        }
        ?>   
        <?php
        if ($this->need_vi)
        {
            $line = trim(settings()->get('social', 'vimeo'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-vi">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_rss)
        {
            $line = trim(settings()->get('social', 'rss'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-rss">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_in)
        {
            $line = trim(settings()->get('social', 'instagram'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-is">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_dr)
        {
            $line = trim(settings()->get('social', 'dribbble'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-dr">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_pr)
        {
            $line = trim(settings()->get('social', 'pinterest'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-pi">' . '</div></a>';
        }
        ?>
        <?php
        if ($this->need_lj)
        {
            $line = trim(settings()->get('social', 'livejournal'));
            if ($line)
                echo '<a target="blank" href="' . $line . '"><div id="soc-lj">' . '</div></a>';
        }
        ?>    
    
</socialbuttons>
