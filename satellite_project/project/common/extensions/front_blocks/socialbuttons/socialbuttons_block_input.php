<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, "class"), ""); ?>
        <?php echo CHtml::textField("Block[class]", $block_model->class, array("id" => "Block-class")); ?>
        <?php echo $form->error($model, "class"); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_fb'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_fb]", $block_model->need_fb, array('id' => 'Block-need_fb')); ?>
        <?php echo $form->error($model, 'need_fb'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_vk'), 'Block-need_vk'); ?>
        <?php echo CHtml::checkBox("Block[need_vk]", $block_model->need_vk, array('id' => 'Block-need_vk')); ?>
        <?php echo $form->error($model, 'need_vk'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_gp'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_gp]", $block_model->need_gp, array('id' => 'Block-need_gp')); ?>
        <?php echo $form->error($model, 'need_gp'); ?>
    </div>   

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_yt'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_yt]", $block_model->need_yt, array('id' => 'Block-need_yt')); ?>
        <?php echo $form->error($model, 'need_yt'); ?>
    </div>   
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_tw'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_tw]", $block_model->need_tw, array('id' => 'Block-need_tw')); ?>
        <?php echo $form->error($model, 'need_tw'); ?>
    </div>    
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_sk'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_sk]", $block_model->need_sk, array('id' => 'Block-need_sk')); ?>
        <?php echo $form->error($model, 'need_sk'); ?>
    </div> 

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_ld'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_ld]", $block_model->need_ld, array('id' => 'Block-need_ld')); ?>
        <?php echo $form->error($model, 'need_ld'); ?>
    </div> 
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_od'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_od]", $block_model->need_od, array('id' => 'Block-need_od')); ?>
        <?php echo $form->error($model, 'need_od'); ?>
    </div>    
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_vi'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_vi]", $block_model->need_vi, array('id' => 'Block-need_vi')); ?>
        <?php echo $form->error($model, 'need_vi'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_rss'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_rss]", $block_model->need_rss, array('id' => 'Block-need_rss')); ?>
        <?php echo $form->error($model, 'need_rss'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_in'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_in]", $block_model->need_in, array('id' => 'Block-need_in')); ?>
        <?php echo $form->error($model, 'need_in'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_dr'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_dr]", $block_model->need_dr, array('id' => 'Block-need_dr')); ?>
        <?php echo $form->error($model, 'need_dr'); ?>
    </div>
    
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_pr'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_pr]", $block_model->need_pr, array('id' => 'Block-need_pr')); ?>
        <?php echo $form->error($model, 'need_pr'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_lj'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_lj]", $block_model->need_lj, array('id' => 'Block-need_lj')); ?>
        <?php echo $form->error($model, 'need_lj'); ?>
    </div>
    
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, "social_email"), ""); ?>
        <?php echo CHtml::textField("Block[social_email]", $block_model->social_email, array("id" => "Block-social_email")); ?>
        <?php echo $form->error($model, "social_email"); ?>
    </div>
    