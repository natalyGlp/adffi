<?php
/**
 * Class for render SocialButtons * 
 * 
 * @author Bazai best programmer <developer@studiobanzai.com>
 * @version 0.1
 * @package common.front_blocks.socialbuttons */

class SocialButtonsBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='socialbuttons';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';
    public $class;
    public $need_fb;
    public $need_vk;
    public $need_gp;
    public $need_yt;
    public $need_tw;
    public $need_sk;
    public $need_ld;
    public $need_od;
    public $need_vi;
    public $need_rss;
    public $need_in;
    public $need_dr;
    public $need_pr;
    public $need_lj;
    public $social_email;
        
    
    public function setParams($params)
    {
            $this->class = isset($params['class']) ? $params['class'] : '';
            $this->need_fb = isset($params['need_fb']) ? $params['need_fb'] : '';
            $this->need_vk = isset($params['need_vk']) ? $params['need_vk'] : '';
            $this->need_gp = isset($params['need_gp']) ? $params['need_gp'] : '';
            $this->need_yt = isset($params['need_yt']) ? $params['need_yt'] : '';
            $this->need_tw = isset($params['need_tw']) ? $params['need_tw'] : '';
            $this->need_sk = isset($params['need_sk']) ? $params['need_sk'] : '';
            $this->need_ld = isset($params['need_ld']) ? $params['need_ld'] : '';
            $this->need_od = isset($params['need_od']) ? $params['need_od'] : '';
            $this->need_vi = isset($params['need_vi']) ? $params['need_vi'] : '';
            $this->need_rss = isset($params['need_rss']) ? $params['need_rss'] : '';
            $this->need_in = isset($params['need_in']) ? $params['need_in'] : '';
            $this->need_dr = isset($params['need_dr']) ? $params['need_dr'] : '';
            $this->need_pr = isset($params['need_pr']) ? $params['need_pr'] : '';
            $this->need_pr = isset($params['need_lj']) ? $params['need_lj'] : '';
            $this->social_email = isset($params['social_email']) ? $params['social_email'] : '';
          return; 
    }
    
    public function run()
    {                 
           $this->renderContent();         
    }       
 
 
    protected function renderContent()
    {     
       if(isset($this->block) && ($this->block!=null)){
       			//Start working with SocialButtons here
				$params=unserialize($this->block->params);
	    		$this->setParams($params);                            
            	$this->render(BlockRenderWidget::setRenderOutput($this),array());                                                          	       		     
		} else {
			echo '';
		}
			  
       
    }
    
    public function validate(){	
		return true ;
    }
    
    public function params()
    {
         return array(
             'class' => t('Css class'),
             'need_fb' => t('Show Facebook button'),
             'need_vk' => t('Show Vkontakte button'),
             'need_gp' => t('Show Gmail button'),
             'need_yt' => t('Show Youtube button'),
             'need_sk' => t('Show Skype button'),
             'need_ld' => t('Show LinkedIn button'),
             'need_tw' => t('Show Twitter button'),
             'need_od' => t('Show Odnoklassniki button'),
             'need_vi' => t('Show vimeo button'),
             'need_rss' => t('Show rss button'),
             'need_in' => t('Show instagram button'),
             'need_dr' => t('Show Dribbbble button'),
             'need_pr' => t('Show Pinterest button'),
             'need_lj' => t('Show livejournal button'),
             'social_email' => t('Show this Email in buttons'),
             );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
	
	
}

?>