<?php

/**
 * Class for render Sign up Box
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.signup
 */

class SignupBlock extends CWidget
{
	
	//Do not delete these attr block, page and errors
	public $id='signup';
	public $block=null;     
	public $errors=array();
	public $page=null;
	public $layout_asset='';
	
	
	public function setParams($params){
	  return; 
  }
  
  public function run()
  {        
	$this->renderContent();
}       


protected function renderContent()
{
	
  
	if(isset($this->block) && ($this->block!=null)){	
		$model=new UserRegisterForm;
				// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='userregister-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

				// collect user input data
		if(isset($_POST['UserRegisterForm']))
		{
			$model->attributes=$_POST['UserRegisterForm'];

			// validate user input password
			if($model->validate()){
				$new_user = new User;
				$new_user->scenario='create';
				//$new_user->username=$model->username;                                                               
				$new_user->username=$new_user->email=$model->email;                                                                                               
				$new_user->display_name=$model->username;
				$old_password=$new_user->password=$model->password;           
				
				//Create hash activation keyd
				if($new_user->save()){      
            	$new_user->sendActivationEmail();  
            	                                                         
					//Redirect to the Dashboard Page                                                                                                                  
					$login_form=new UserLoginForm();
					$login_form->username=$new_user->username;
					$login_form->password=$old_password;
					if($login_form->login()){
						Yii::app()->controller->redirect(bu());
					} else {
						throw new CHttpException(503,t('Error while setting up your Account. Please try again later'));
					}
					
				}                               
			}
		}
		$this->render(BlockRenderWidget::setRenderOutput($this),array('model'=>$model));
	} else {
	   echo '';
   }
   
}

public function validate(){	
  return true ;
}

public function params()
{
   return array();
}

public function beforeBlockSave(){
	return true;
}

public function afterBlockSave(){
	return true;
}
}

?>