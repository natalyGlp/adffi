<?php  
class SliderFix extends CModel 
{
	public $width;
    public $height;
    public $nav_bar;
    public $nav_left;
    public $nav_right;
    public $nav_play;
    public $thumb;
    public $auto;
    public $autospeed;
    public $panelclick;	
    public function rules() 
    {
    	return array(
    		array('width, height','numerical'),
    		array('autospeed','numerical','min'=>'1000','max'=>'10000'),
    		array('width, height','required'),

    		);
    }
    public function attributeNames() 
    {
    	return array(
    		'width'=>t('asd'),
    		'height'=>t('Высота'),
    		);
    }
    public function attributeLabels() 
    {
    	return array(
    		'width'=>t('Width'),
    		'height'=>t('Height'),
    		);
    }
}