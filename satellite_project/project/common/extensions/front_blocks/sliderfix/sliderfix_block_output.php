<?
$block_id='block'.$this->block->block_id;
 $gallery=Galleries::model()->findByPk($this->gal_id); ?>
<style type="text/css">
    .sliderkit img,
    .sliderkit-panel
    {
        width:<?=$this->width?>px !important;
        height:<?=$this->height?>px !important;
    }
</style>    
<script>
$(document).ready(function()
{
	$("#<? echo $block_id; ?>").sliderkit({
					autospeed:<? echo $this->autospeed; ?>,
					mousewheel:true,
					circular:true
				});
})
</script>
<!-- Slider Kit compatibility -->
    <script type="text/javascript" src="http://shared.webtests.in.ua/html5.js"></script>
    <link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentslider/sliderkit-demos.css" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentslider/sliderkit-demos-ie6.css" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentslider/sliderkit-demos-ie7.css" /><![endif]-->
    <!--[if IE 8]><link rel="stylesheet" type="text/css" href="http://shared.webtests.in.ua/contentslider/sliderkit-demos-ie8.css" /><![endif]-->
<? 
$sizes=$gallery->getSizes($this->width);
$cur_type=$gallery->getSizes($this->width,'type');
 ?>

<slider class="horisontal">
<div id="<? echo $block_id; ?>" class="sliderkit slideshow-carousel" style="height: <?=$this->height?>px; width: <?=$this->width?>px;">
	<?
	  if ($this->thumb) { ?>
					<div class="sliderkit-nav">
						<div class="sliderkit-nav-clip">
							<ul>
								
								<? foreach ($gallery->pictures as $p){ ?>
									<li><a href="#" title="<?php echo $p->title ?>"><img src="<? echo $p->getImage('_thumb')  ?>" alt="<? echo $p->title; ?>" /></a></li>	
								<? } ?>								
							</ul>
						</div>
					</div> 
					<? } ?>
					<?
					 if ($this->nav_bar) { ?>
					<div class="sliderkit-controls">
						 <? if ($this->nav_left) { ?>
						<div class="sliderkit-btn sliderkit-go-btn sliderkit-go-prev"><a href="#" title="Previous"><span>Previous</span></a></div>
						<? } ?>
						<? if ($this->nav_play) { ?>
						<div class="sliderkit-btn sliderkit-play-btn"><a href="#" title="Play/Pause"><span>Play</span></a></div>
						<? } ?>
						<? if ($this->nav_right) { ?>
						<div class="sliderkit-btn sliderkit-go-btn sliderkit-go-next"><a href="#" title="Next"><span>Next</span></a></div>
						<? } ?>
					</div>
					<? } ?>
					<div class="sliderkit-panels" style="height: <?=$this->height?>px; width: <?=$this->width?>px;">
						 
							<? foreach ($gallery->pictures as $p) 
								{ ?>
								<div class="sliderkit-panel">
									<? if ($p->link!='') {
										echo CHtml::link(CHtml::image($p->getImage($cur_type),$p->title),$p->link);
									} else 
									{
										echo CHtml::image($p->getImage($cur_type),$p->title);
									} ?>
									</div>
								<? } ?>								
						
						
					</div>
				</div>	
</slider>	