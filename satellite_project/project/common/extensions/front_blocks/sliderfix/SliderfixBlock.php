<?php

/**
 * Class for render gal_id Content Block
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.gal_id
 */

class SliderfixBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='sliderfix';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    

    public $gal_id;    
    public $width=800; //+
    public $height=600; //+
    public $nav_bar=0;//+
    public $nav_left;//+
    public $nav_right;//+
    public $nav_play;//+
    public $thumb;//+
    public $autospeed=4000; //+
    public $panelclick;
    public function setParams($params){
        $this->gal_id=isset($params['gal_id']) ? $params['gal_id'] : '';
        $this->thumb=isset($params['thumb']) ? $params['thumb'] : '';
        $this->width=isset($params['width']) ? $params['width'] : '';
        $this->height=isset($params['height']) ? $params['height'] : '';
        $this->nav_bar=isset($params['nav_bar']) ? $params['nav_bar'] : '';
        $this->nav_play=isset($params['nav_play']) ? $params['nav_play'] : '';
        $this->nav_left=isset($params['nav_left']) ? $params['nav_left'] : '';
        $this->nav_right=isset($params['nav_right']) ? $params['nav_right'] : '';
        $this->autospeed=isset($params['autospeed']) ? $params['autospeed'] : '';
	    $this->panelclick=isset($params['panelclick']) ? $params['panelclick'] : '';
    }
    
    public function run()
    {        
        $url = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets/',true);
        Yii::app()->clientScript->registerCssFile($url."/slider/css/sliderkit-core.css");
        Yii::app()->clientScript->registerScriptFile($url."/slider/js/external/jquery.mousewheel.min.js");
        Yii::app()->clientScript->registerScriptFile($url."/slider/js/sliderkit/jquery.sliderkit.1.9.2.pack.js");
        $this->renderContent();
    }       
 
 
    protected function renderContent()
    {
	if(isset($this->block) && ($this->block!=null)){	    
            //Set Params from Block Params
            $params=unserialize($this->block->params);
	    $this->setParams($params);                            
            $this->render(BlockRenderWidget::setRenderOutput($this),array());
	} else {
	    echo '';
	}
       
    }
    
    public function validate(){
     //   var_dump($_POST['Block']);
    Yii::import('common.front_blocks.sliderfix.*');
    $model= new SliderFix; 
    $model->attributes=$_POST['Block'];
    if(!$model->validate()){
        foreach ($model->getErrors() as $key =>$errors){
        $this->errors[$key]=t($errors[0]); }
                return false ;
	}
	else
		return true ;
    }
    
    public function params()
    {
            return array(
                    'gal_id' => t('Gallery name'),                   
                    'thumb' => t('Thumbneils'),          
                    'width'=>t('Width'),
                    'height'=>t('Height'),
                    'nav_bar'=>t('Navigation block'),
                    'nav_play'=>t('Play/Stop Button'),
                    'nav_left'=>t('Navigation left'),
                    'nav_right'=>t('Navigation right'),
                    'autospeed'=>t('Autospeed'),
            );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
}

?>