<div name="div-block-content-<?php echo $block_model->id;?>">
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'gal_id'),''); ?>
		<?php echo CHtml::dropDownList("Block[gal_id]",$block_model->gal_id, 
					    Chtml::listData(Galleries::model()->findAll(),'id','title') ,array('id'=>'Block-gal_id',
	                                    'rows'=>15, 'cols'=>50, 'style'=>'width:90%'
	                         
	                          )); ?>
		<?php echo $form->error($model,'gal_id'); ?>

	</div>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'width'),''); ?>
		<?php echo CHtml::textField("Block[width]",$block_model->width, 
					    array('id'=>'Block-width')); ?>
		<?php echo $form->error($model,'width'); ?>
	</div>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'height'),''); ?>
		<?php echo CHtml::textField("Block[height]",$block_model->height, 
					    array('id'=>'Block-height')); ?>
		<?php echo $form->error($model,'height'); ?>
	</div>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'thumb'),''); ?>
		<?php echo CHtml::dropDownList("Block[thumb]",$block_model->thumb, 
						array('1'=>'Показывать','0'=>'Не показывать'),
					    array('id'=>'Block-thumb')); ?>
		<?php echo $form->error($model,'thumb'); ?>
	</div>
	<div class="row">
		<?php
		 echo CHtml::label(Block::getLabel($block_model,'nav_bar'),'Block-nav_bar'); ?>
		<?php echo CHtml::checkBox("Block[nav_bar]",$block_model->nav_bar, 
						array('id'=>'Block-nav_bar')); ?>
		<?php echo $form->error($model,'nav_bar'); ?>
		<div class="subrows">
			<div class="row">
				<?php
				 echo CHtml::label(Block::getLabel($block_model,'nav_play'),'Block-nav_play'); ?>
				<?php echo CHtml::checkBox("Block[nav_play]",$block_model->nav_play, 
								array('id'=>'Block-nav_play')); ?>
				<?php echo $form->error($model,'nav_play'); ?>
			</div>
			<div class="row">
					<?php
					 echo CHtml::label(Block::getLabel($block_model,'nav_left'),'Block-nav_left'); ?>
					<?php echo CHtml::checkBox("Block[nav_left]",$block_model->nav_left, 
									array('id'=>'Block-nav_left')); ?>
					<?php echo $form->error($model,'nav_left'); ?>
			</div>
			<div class="row">
					<?php
					 echo CHtml::label(Block::getLabel($block_model,'nav_right'),'Block-nav_right'); ?>
					<?php echo CHtml::checkBox("Block[nav_right]",$block_model->nav_right, 
									array('id'=>'Block-nav_right')); ?>
					<?php echo $form->error($model,'nav_right'); ?>
			</div>
		</div>

	</div>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'autospeed'),'Block-autospeed'); ?>
		<?php echo CHtml::textField("Block[autospeed]",$block_model->autospeed, 
					    array('id'=>'Block-autospeed')); ?>
		<?php echo $form->error($model,'autospeed'); ?>
	</div>
</div>
