<?php
/**
 * Class for render Slogan * 
 * 
 * @author Bazai best programmer <developer@studiobanzai.com>
 * @version 0.1
 * @package common.front_blocks.slogan */
class SloganBlock extends CWidget {

    //Do not delete these attr block, page and errors
    public $id = 'slogan';
    public $block = null;
    public $errors = array();
    public $page = null;
    public $layout_asset = '';
    public $class;

    public function setParams($params)
    {
        $this->class = isset($params['class']) ? $params['class'] : '';
        return;
    }

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        if (isset($this->block) && ($this->block != null))
        {
            //Start working with Slogan here
            $params = unserialize($this->block->params);
            $this->setParams($params);
            $this->render(BlockRenderWidget::setRenderOutput($this), array());
        } else
        {
            echo '';
        }
    }

    public function validate()
    {
        return true;
    }

    public function params()
    {
        return array(
            'class' => t('Css class'),
        );
    }

    public function beforeBlockSave()
    {
        return true;
    }

    public function afterBlockSave()
    {
        return true;
    }

}

?>