<div class="listposts-container">
    <?php
    	$dataProvider=new CActiveDataProvider('NewsObject', array(
    		'criteria' => $criteria
    	));

    	$this->widget('zii.widgets.CListView', array_merge( array(
			'dataProvider'=>$dataProvider,
            'itemView'=>'common.content_type.news.item_render_list',
            'summaryText'=>'',
            'ajaxUpdate'=>false,
            'pager' => array(
                'header' => '',
                'nextPageLabel' => '&nbsp;',
                'prevPageLabel' => '&nbsp;'
            ),
            'template' => '{items}<br clear="all"/><hr>{pager}<br clear="all"/>', 
            'enablePagination'=> true
        ), array()));
    ?>
</div>