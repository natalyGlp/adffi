<aside class="equipment">
	<div class="title"><h2>Комплектации <?php echo $post->fullName;?></h2></div>
	<table border="0" cellpadding="0" cellspacing="0" style="width: 700px;">

	<tr class="title">
        <td class="engine">Комплектация</td>
        <td class="car-equipment">Двигатель</td>
        <td class="drive">Привод</td>
        <td class="power">Мощность</td>
        <td class="consumption">Расход</td>
        <td class="price">Стоимость</td>
    </tr>
    <?php if (count($catalogs)): ?>
		<?php foreach ($catalogs as $catalog): ?>
			<tr>
		        <td class="engine"><?php echo CHtml::link($catalog->object_name, Object::getLink($catalog->object_id));?></td>
		        <td class="car-equipment"><?php echo $catalog->engine;?></td>
		        <td class="drive"><?php echo $catalog->drive;?></td>
		        <td class="power"><?php echo $catalog->power;?></td>
		        <td class="consumption"><?php echo $catalog->consumption;?></td>
		        <td class="price"><?php echo $catalog->price;?></td>
		    </tr>
		<?php endforeach ?>
    <?php else: ?>
		<tr>
		    <td colspan="6" class="engine">Нет комплектаций</td>
		</tr>
    <?php endif;?>
	</table>
	<!-- <button type="button" class="calculate">Сравнить отмеченные комплектации</button> -->
</aside>