<gallery  class="preview">
<?php 
if ($this->title!='') 
{
  echo '<h2>'.$this->title.'</h2>';
}
	$photos=GalleryPhotos::model()->findAll(array('condition'=>'gallery_id=:id','limit'=>$this->count, 'params'=>array(':id'=>$this->gal_id)));
  if (!empty($photos))  
    foreach ($photos as $photo) { 
        echo CHtml::link(CHtml::image($photo->getImage('_thumb'),''),$photo->getImage('_org'),array('rel'=>'gallery','class'=>'gallery_pre_link'));
    } ?>
<div class="clear">
  
</div>
<?
if ($this->link_title!='' && $this->link_href!='') 
{
  echo CHtml::link($this->link_title,$this->link_href,array('class'=>'galley_preview_link'));
}
 ?>
</gallery>