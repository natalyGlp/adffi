<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'gal_id'),''); ?>
	<?php echo CHtml::dropDownList("Block[gal_id]",$block_model->gal_id, 
				    Chtml::listData(Galleries::model()->findAll(),'id','title') ,array('id'=>'Block-gal_id',
                          )); ?>
	<?php echo $form->error($model,'gal_id'); ?>

</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'count'),''); ?>
	<?php echo CHtml::textField("Block[count]",$block_model->count,array('id'=>'Block-count',
                          )); ?>
	<?php echo $form->error($model,'count'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'title'),''); ?>
	<?php echo CHtml::textField("Block[title]",$block_model->title ,array('id'=>'Block-title',
                          )); ?>
	<?php echo $form->error($model,'title'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'link_title'),''); ?>
	<?php echo CHtml::textField("Block[link_title]",$block_model->link_title,array('id'=>'Block-link_title',
                          )); ?>
	<?php echo $form->error($model,'link_title'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'link_href'),''); ?>
	<?php echo CHtml::textField("Block[link_href]",$block_model->link_href ,array('id'=>'Block-link_href',
                          )); ?>
	<?php echo $form->error($model,'link_href'); ?>
</div>
</div>
