<?php

/**
 * Class for render gal_id Content Block
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.gal_id
 */

class GallerypreviewBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='gallerypreview';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    
    //HTML attribute
    public $gal_id;    
    public $count='4';    
    public $title='';    
    public $link_title='';    
    public $link_href='';    
    
    
    
    public function setParams($params){
        $this->gal_id=isset($params['gal_id']) ? $params['gal_id'] : '';
        $this->count=isset($params['count']) ? $params['count'] : '4';
        $this->title=isset($params['title']) ? $params['title'] : '';
        $this->link_title=isset($params['link_title']) ? $params['link_title'] : '';
	    $this->link_href=isset($params['link_href']) ? $params['link_href'] : '';
    }
    
    public function run()
    {
        $url = Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.frontend'), false);
        Yii::app()->clientScript->registerCssFile($url."/fancybox/jquery.fancybox-1.3.4.css");
        Yii::app()->clientScript->registerScriptFile($url."/fancybox/jquery.fancybox-1.3.4.pack.js");
        Yii::app()->clientScript->registerScriptFile($url."/fancybox/gallery.js");
        $this->renderContent();
    }       
 
 
    protected function renderContent(){
    	if(isset($this->block) && ($this->block!=null)){	    
            //Set Params from Block Params
            $params=unserialize($this->block->params);
    	    $this->setParams($params);                            
            $this->render(BlockRenderWidget::setRenderOutput($this),array());
    	} else {
    	    echo '';
    	}
    }
    
    public function validate(){
        $errors=false;
	if($this->gal_id==""){
		$this->errors['gal_id']=t('gal_id content is required'); 
        $errors=true;
    }
    if(!is_numeric($this->count)){
        $this->errors['count']=t('This field nust be numeric.'); 
        $errors=true;
    }
    if(!is_string($this->title)){
        $this->errors['title']=t('This field nust be string.'); 
        $errors=true;
    }
    if(!is_string($this->link_title)){
        $this->errors['link_title']=t('This field nust be string.'); 
        $errors=true;
    }
    if(!is_string($this->link_href)){
        $this->errors['link_href']=t('This field nust be string.'); 
        $errors=true;
    }
    if ($errors) {
                return false ; 
    	}
	else
		return true ;
    }
    
    public function params()
    {
            return array(
                    'gal_id' => t('Gallery name'),                   
                    'count' => t('Count items'),                   
                    'title' => t('Title'),                   
                    'link_title' => t('Link title'),                   
                    'link_href' => t('Link href'),                   
            );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
}

?>