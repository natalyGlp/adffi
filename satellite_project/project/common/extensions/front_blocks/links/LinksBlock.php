<?php

/**
 * Class for render Block
 * 
 * 
 * @author Yura Kovalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package common.front_blocks.model_title
 */

class LinksBlock extends CWidget{
    public $id='links';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
        
    public function setParams($params){
    }
    
    public function run(){        
        $this->renderContent();
    }       
 
    protected function renderContent(){
        if(isset($this->block) && ($this->block!=null)){
            $params=unserialize($this->block->params);
            $this->setParams($params);                            
            $this->render(BlockRenderWidget::setRenderOutput($this),array());
        } else {
            echo '';
        }
    }
    
    public function validate(){
        return true;
    }
    
    public function params(){
        return array();
    }
    
    public function beforeBlockSave(){
       return true;
    }
    
    public function afterBlockSave(){
       return true;
    }
}