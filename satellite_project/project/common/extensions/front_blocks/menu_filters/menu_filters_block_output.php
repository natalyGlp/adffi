<div class="line"></div>
<?php 
$options = null;
if (isset($menus)):?>
    <menu class="<?php echo $this->getAlign().' '.$this->class?>">
        <?php if ($this->title!=''):?>
            <h3 class="menu_title">
                <?php if ($this->title_href=='') echo $this->title; else echo CHtml::link($this->title,$this->title_href); ?>
            </h3>
        <?php endif; ?>
        <ul class="menu_level_1">       
            <?php foreach ($menus as $menu):?>  
                <?php $second_level_menus=MenuFiltersBlock::getMenuItems($menu['id'],$this->menu_id);?>
                <li class="<?php echo $menu['css_class'].(count($second_level_menus) ? ' has-child' : '');?> <?php echo isset(Yii::app()->request->url) && (Yii::app()->request->url == $menu['link'] || 'http://'.$_SERVER['HTTP_HOST'].Yii::app()->request->url == $menu['link']) ?'active':''.$menu['link'];?>">
                    <a href="<?php echo $menu['link'];?>"><?php echo $menu['name'];?></a>
                    <?php 
                        if ($menu['css_class'] == 'has-filters'){
                            $options = FiltersOptions::model()->findByPk($menu['value']);
                            if($options){
                    ?>
                        <ul class="menu_level_2">
                            <?php foreach ($options->attributes  as $key => $value): ?>
                                <?php if (preg_match('/^(filter)/i', $key) && !empty($value) && $key != 'filter11'): ?>
                                <li>
                                    <a class="drop-btn" href="#<?php echo $key;?>"><?php echo $value;?></a>
                                </li>
                                <?php endif ?>
                            <?php endforeach ?>
                        </ul>
                    <?php
                            }
                        }
                    ?>
                    <?php 
                        if(count($second_level_menus)>0) : ?>
                        <ul class="menu_level_2">
                            <?php foreach ($second_level_menus as $second_menu) : ?>
                                <li><a class="<?php echo $second_menu['css_class'];?>" href="<?php echo $second_menu['link'];?>"><?php echo $second_menu['name'];?></a>                                     
                                    <?php 
                                        $third_level_menus=MenuFiltersBlock::getMenuItems($second_menu['id'],$this->menu_id);
                                        if(count($third_level_menus)>0) : ?>
                                        <ul class="menu_level_3">
                                            <?php foreach ($third_level_menus as $third_menu) : ?>
                                                <li><a href="<?php echo $third_menu['link'];?>"><?php echo $third_menu['name'];?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <?php endif; ?>
                                    </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php endif; ?> 
                    </li>
                <?php endforeach;?>
            </ul>
      <br style="clear: left">
    </menu>      
<?php endif;?>
<?php if ($options): ?>
    <div id="drop-menu" class="procreator">
        <aside class="left-part">
            <h2>Фильтры поиска</h2>
            <div id="filters_place">

            </div>
            <br clear="all"/>
            <button type="button" id="send-filters-btn" class="search-btn" for="filtres">Искать</button>
        </aside>
        <aside id="filters-right">
            <?php foreach ($options->attributes  as $key => $value): ?>
                <?php if (preg_match('/^(filter)/i', $key) && !empty($value) && $key != 'filter11'): ?>
                <div id="<?php echo $key;?>" class="filter-tab-content">
                    <?php
                        switch($options->options[$key]){
                            case 1:
                            case 2:{
                                $array = isset($options->options['select_options'][$key]) ? $options->options['select_options'][$key] : array();

                                foreach($array as $k => $val){
                                    echo '<div class="filters_cols filters_cols_'.$key.'" id="div_'.$key.'_'.$k.'">';
                                    echo '<input name="'.$key.'['.$k.']" data-label="'.$val.'" data-name="'.$value.'" type="checkbox" data-key="'.$key.'" data-value="'.$k.'" name="'.$key.'[]" class="filter-item" id="'.$key.'_'.$k.'" value="'.$k.'">';
                                    echo '<label for="'.$key.'_'.$k.'" name="'.$key.'[]" class="">'.$val.'</label>';
                                    echo '</div>';
                                }

                                echo '<div class="clear"></div>';
                                break;
                            }
                            case 3:
                            case 4:{
                                $terms = Term::model()->findAll(array(
                                    'select' => array('term_id', 'name'),
                                    'condition' => 'taxonomy_id=?',
                                    'params' => array($options->options['taxonomy_id'][$key])
                                ));

                                foreach($terms as $term){
                                    echo '<div class="filters_cols" id="div_'.$key.'_'.$term->term_id.'">';
                                    echo '<input name="'.$key.'['.$term->term_id.']" data-label="'.$term->name.'" data-key="'.$key.'" data-value="'.$term->term_id.'" data-name="'.$value.'"  type="checkbox" name="'.$key.'[]" class="filter-item" id="'.$key.'_'.$term->term_id.'" value="'.$term->term_id.'">';
                                    echo '<label for="'.$key.'_'.$term->term_id.'" name="'.$key.'[]" class="">'.$term->name.'</label>';
                                    echo '</div>';
                                }

                                echo '<div class="clear"></div>';
                                break;
                            }
                            case 5:{

                                break;
                            }
                            case '6':{
                                echo '
                                <input name="'.$key.'" type="text" data-key="'.$key.'" data-value="0" class="filter-item" data-name="'.$value.'"  id="'.$key.'_0"/>
                                <script>
                                    $(document).ready(function(){
                                        $("#'.$key.'_0").datepicker({
                                            format: " yyyy",
                                            viewMode: "years", 
                                            minViewMode: "years"
                                        });
                                    });
                                </script>
                                ';     
                                break;
                            }
                            case '7':{
                                echo '
                                <input name="'.$key.'" type="text" data-key="'.$key.'" data-value="0" class="filter-item" data-name="'.$value.'"  id="'.$key.'_0"/>
                                <script>
                                    $(document).ready(function(){
                                        $("#'.$key.'_0").datepicker({
                                            format: " yyyy",
                                            viewMode: "years", 
                                            minViewMode: "years"
                                        });
                                    });
                                </script>
                                ';     
                                break;
                            }
                            case 8:{
                                $min = (int) Yii::app()->{CONNECTION_NAME}
                                                       ->createCommand()
                                                       ->select('MIN('.$key.')')
                                                       ->from('{{filters}}')
                                                       ->where('id_options=?', array($options->id))
                                                       ->queryScalar();

                                $max = (int) Yii::app()->{CONNECTION_NAME}
                                                       ->createCommand()
                                                       ->select('MAX('.$key.')')
                                                       ->from('{{filters}}')
                                                       ->where('id_options=?', array($options->id))
                                                       ->queryScalar();

                                echo '<div class="scale">';
                                echo '<div class="scale-min">'.$min.'</div>';
                                echo $min == 0 ? '<div class="scale-center">'.($max / 2).'</div>' : '';
                                echo '<div class="scale-max">'.$max.'</div>';
                                echo '<div id="slider-range-'.$key.'"></div></div>';
                                echo '<div class="range-data">
                                          <label>'.t('From').':&nbsp;<input name="'.$key.'[min]" data-name="'.$value.'" data-key="'.$key.'" data-value="min" class="filter-item" id="'.$key.'_0" type="text" readonly/></label> - 
                                          <label>'.t('to').':&nbsp;<input name="'.$key.'[max]" data-name="'.$value.'" data-key="'.$key.'" data-value="max" class="filter-item" id="'.$key.'_1" type="text" readonly/></label>
                                      </div>';
                                echo '<script>';
                                echo '
                                $(document).ready(function() {
                                    $( "#slider-range-'.$key.'" ).slider({
                                        range: true,
                                        min: '.$min.',
                                        max: '.$max.',
                                        '.($max - $min > 10000 ? 'step:1000,' : '').'
                                        values: [ '.$min.', '.$max.' ],
                                        slide: function( event, ui ) {
                                            $( "#filters-right #'.$key.'_0" ).val( ui.values[0] );
                                            $( "#filters-right #'.$key.'_1" ).val( ui.values[1] );
                                        },
                                        change: function( event, ui ) {
                                            $( "#filters-right #'.$key.'_1" ).val( ui.values[1] );
                                            $( "#filters-right #'.$key.'_0" ).val( ui.values[0] );


                                            if($("#filters_place > #'.$key.'").length){
                                                if($("#filters_place #'.$key.'_0").length){
                                                    $("#filters_place #'.$key.'_0").html("от "+ui.values[ 0 ]+" - до "+ui.values[ 1 ]).append(
                                                        $("<a href=\"#'.$key.'_0\" class=\"del-filter-button\">х</a>")
                                                    );
                                                }else{
                                                    $("#filters_place > #'.$key.' > .options").append(
                                                        $(
                                                            "<li id=\"'.$key.'_0\">"+
                                                                "от "+ui.values[ 0 ]+" - до "+ui.values[ 1 ]+
                                                                "<a href=\"#'.$key.'_0\" class=\"del-filter-button\">х</a>"+
                                                            "</li>"
                                                        )
                                                    );
                                                }
                                                
                                                initDel();
                                            }else{
                                                $("#filters_place").append(
                                                    $(
                                                        "<div class=\"clearfix\" id=\"'.$key.'\">"+
                                                            "<label>'.$value.': </label><br/>"+
                                                            "<ul class=\"options\">"+
                                                                "<li id=\"'.$key.'_0\">"+
                                                                    "от "+ui.values[ 0 ]+" - до "+ui.values[ 1 ]+
                                                                    "<a href=\"#'.$key.'_0\" class=\"del-filter-button\">х</a>"+
                                                                "</li>"+
                                                            "</ul>"+
                                                        "</div>"
                                                    )
                                                );
                                                
                                                initDel();
                                            }
                                        }
                                    });

                                    $( "#filters-right #'.$key.'_0" ).val( $( "#slider-range-'.$key.'" ).slider("values", 0) );
                                    $( "#filters-right #'.$key.'_1" ).val( $( "#slider-range-'.$key.'" ).slider("values", 1) );
                                });
                                ';
                                echo '</script>';
                                break;
                            }
                            case 9:{
                    ?>
                    <slider>
                        <ul id="logo-carousel" class="jcarousel jcarousel-skin-tango">
                        <?php
                        $criteria = new CDbCriteria;
                        $criteria->select = array('t.object_id', 't.object_name');
                        $criteria->condition = 't.object_type=? AND t.object_status=?';
                        $criteria->params = array('brand', 1);

                        $brands = Object::model()->findAll($criteria);

                        foreach($brands as $i => $data){
                            $thumb = GxcHelpers::getResourceObjectFromDatabase($data, 'thumbnail', 'filter_brands_');
                            $img = isset($thumb[0]['link']) ? $thumb[0]['link'] : '';

                            echo '<li>
                                      <a style="display:block;width:73px;height:73px;line-height:73px;background:url(\'' . $img . '\') no-repeat center center;" class="filter-item filter-item-img" href="javascript://">
                                          <input data-label="'.$data->object_name.'" data-name="'.$value.'" data-key="'.$key.'" data-value="'.$data->object_id.'" type="checkbox" name="'.$key.'[]" class="filter-item" id="'.$key.'_'.$i.'" value="'.$i.'">
                                      </a>
                                  </li>';
                        }

                        ?>
                        </ul>
                        <script type="text/javascript">
                            $(window).load(function() {
                                $('#logo-carousel').jcarousel({
                                    vertical: true,
                                    scroll: 4,
                                    circular: true
                                });
                            });
                        </script>
                    </slider>
                    <?php
                                break;
                            }
                        }
                    ?>
                </div>
                <?php endif;?>
            <?php endforeach;?>
        </aside>

        <script type="text/javascript">
            function rmFlter(id){
                $('#filters_place '+id).remove();
                $('.filters_cols '+id).removeAttr('checked');
            }

            function initDel(){
                $('.del-filter-button').click(function(e){
                    e.preventDefault();
                    rmFlter($(this).attr('href'));
                });
            }

            $(document).ready(function(){
                var menu_hovered = false;
    
                $(".has-filters").hover(function(){
                    $("#drop-menu").show();
                    if(!menu_hovered){
                        $(".drop-btn:eq(0)").click();
                    }
                }).click(function(e){
                    e.preventDefault();
                    return false;
                });

                $(".drop-btn").click(function(e){
                    e.preventDefault();
                    $("#drop-menu").show();
                    $(".has-filters").addClass("active");
                    $(".filter-tab-content").hide();
                    $("#filters-right " + $(this).attr("href")).show();
                    $("#filters-right " + $(this).attr("href")).css("display", "block");    
                    $(".drop-btn").removeClass("active");    
                    $(this).addClass("active");

                    menu_hovered = true;
                    return false;
                });

                $(".menu_level_1 > li").hover(function(){
                    if(!$(this).hasClass("has-filters")){
                        menu_hovered = false;
                        $(".has-filters").removeClass("active");
                        $("#drop-menu").hide();
                    }
                });

                
                $('#send-filters-btn').click(function(){
                    console.log($('#filters-right input').serialize());
                });

                initDel();

                $('.filters_cols_filter2, .filter-item-img').click(function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    var el = $(this).children('input');

                    if(el.attr('checked') == 'checked'){
                        el.removeAttr('checked');
                    }else{
                        el.attr('checked', 'checked');
                    }

                    el.change();
                });

                $('.filter-item').change(function(){
                    // console.log(this.value, $(this).attr('checked'));
                    
                    if($(this).attr('checked') != 'checked'){
                        rmFlter('#'+$(this).data('key')+'_'+$(this).data('value'));
                    }else if($(this).attr('checked') == 'checked' && $('#filters_place > #'+$(this).data('key')).length){
                        $('#filters_place > #'+$(this).data('key')+' > .options').append(
                            $(
                                '<li id="'+$(this).data('key')+'_'+$(this).data('value')+'">'+
                                    $(this).data('label')+
                                    '<a href="#'+$(this).data('key')+'_'+$(this).data('value')+'" class="del-filter-button">х</a>'+
                                '</li>'
                            )
                        );
                        
                        initDel();
                    }else if($(this).attr('checked') == 'checked'){
                        $('#filters_place').append(
                            $(
                                '<div class="clearfix" id="'+$(this).data('key')+'">'+
                                    '<label>'+$(this).data('name')+': </label><br/>'+
                                    '<ul class="options">'+
                                        '<li id="'+$(this).data('key')+'_'+$(this).data('value')+'">'+
                                            $(this).data('label')+
                                            '<a href="#'+$(this).data('key')+'_'+$(this).data('value')+'" class="del-filter-button">х</a>'+
                                        '</li>'+
                                    '</ul>'+
                                 '</div>'
                            )
                        );
                        
                        initDel();
                    }
                });
            });
        </script>
    </div>
<?php endif ?>