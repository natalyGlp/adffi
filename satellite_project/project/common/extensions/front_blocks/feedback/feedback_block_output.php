<?php $blockid=$this->block->block_id; /* id блока*/ ?>
<feedback <?php if ($this->cssclass!='') echo 'class="'.$this->cssclass.'"'; ?>>
    <div class="title">
        <h2><?php echo t($this->title); ?></h2>
    </div>
    <?php $this->render('cmswidgets.views.notification_frontend'); ?>
    
    <?php 
        $form = $this->beginWidget('CActiveForm', array(
    	   'enableClientValidation'=>true,
    	   'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'validateOnChange'=>false
            ),     
        )); 
    ?>
    <div class="clearfix">
        <label for="name2" class="labelBlur" style="display: inline; "><?php echo $form->labelEx($model,'name2'); ?></label>
           <div class="input">
        <?php echo $form->textField($model,'name2',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'name2'); ?>
        </div>   
    </div>
    <div class="clearfix">
        <label for="name" class="labelBlur" style="display: inline; "><?php echo $form->labelEx($model,'name'); ?></label>
           <div class="input">
        <?php echo $form->textField($model,'name',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'name'); ?>
        </div>   
    </div>
    <div class="clearfix">
        <label for="email" class="labelBlur" style="display: inline; "><?php echo $form->labelEx($model,'email'); ?></label>
           <div class="input">
        <?php echo $form->textField($model,'email',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'email'); ?>
        </div>   
    </div>
    <div class="clearfix">
        <label for="address" class="labelBlur" style="display: inline; "><?php echo $form->labelEx($model,'address'); ?></label>
           <div class="input">
        <?php echo $form->textField($model,'address',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'address'); ?>
        </div>   
    </div>
    <div class="clearfix">
        <label for="phone" class="labelBlur" style="display: inline; "><?php echo $form->labelEx($model,'phone'); ?></label>
           <div class="input">
        <?php echo $form->textField($model,'phone',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'phone'); ?>
        </div>   
    </div>
    <div class="clearfix" id="messages">
        <label for="phone" class="labelBlur" style="display: inline; "><?php echo $form->labelEx($model,'message'); ?></label>
        <div class="input">
            <?php echo $form->textArea($model,'message',array('rows'=>6,'cols'=>50, 'class'=>'feedback_message')); ?>
            <?php echo $form->error($model,'message'); ?>
        </div>   
    </div>
    <div class="actions" >
         <?php 
         echo CHtml::SubmitButton(t('Оформить заявку'), array('class' => 'calc-btn', 'style' => 'margin:10px 0 0 0;')); ?>
    </div>
    <?php $this->endWidget(); ?>

</feedback>

