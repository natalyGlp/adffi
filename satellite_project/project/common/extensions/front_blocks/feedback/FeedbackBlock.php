<?php

/**
 * Class for render FeebBack ajax
 * 
 * 
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.feedback
 */
class FeedbackBlock extends CWidget{
    //Do not delete these attr block, page and errors
    public $id='feedback';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';
    public $email='kimmongod@gmail.com';
    public $send = '';
    public $sendflag = false;
    public $title = '';
    public $cssclass = '';

    public function setParams($params){ 
        $this->title = isset($params['title']) ? $params['title'] : '';
        $this->cssclass = isset($params['cssclass']) ? $params['cssclass'] : '';
    }

    public function run(){     
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery.ui.dialog.css');
        
        $this->renderContent();
    }

    protected function renderContent(){
        $params=unserialize($this->block->params);
        $this->setParams($params); 
    	Yii::import('common.front_blocks.feedbackajax.models.*');
    	$model = new FeedBackModel();
    	if (!Yii::app()->request->isAjaxRequest && isset($_POST['FeedBackModel'])){
            $model->setAttributes($_POST['FeedBackModel'], false);
            if ($model->validate()){
                $mailtemplate= MailTemplate::getModelBySlug('zaiyavka');
                if ($mailtemplate){
                    /* feedback mail template keys*/
                    $keys=array(array('{subject}','{username}'),array('Оформление заявки',$model->name));
                    /* feedback mail template values*/
                    $values=array(
                        array('{name2}','{name}','{phone}','{address}','{email}','{message}'),
                        array($model->name2, $model->name, $model->phone, $model->address, $model->email, $model->message)
                    );
                    $subject=$mailtemplate->prepareMail('mail_title',$keys);
                    $text=$mailtemplate->prepareMail('mail_text',$values);
                } else {
                    $subject='';
                    $text='';
                }
                $m = new YiiMailMessage;
                $m->addTo(Yii::app()->settings->get('system','support_email'));
                $m->from = Yii::app()->settings->get('system','support_email');
                $m->subject=$subject;
                $m_content=$text;
                $m->setBody($m_content, 'text/html');
                Yii::app()->mail->send($m);
                $this->sendflag=true;
                if ($this->sendflag)  {
                    Yii::app()->user->setFlash('success', t('Mail send.'));
                    Yii::app()->controller->redirect($_SERVER['REQUEST_URI']);
                } else {
                    $this->send=t('Error when send');
                }
            } else {
                $this->send=t('Error validating');
            }
        }

    	if(isset($this->block) && ($this->block!=null)){
            Yii::app()->clientScript->registerScript('feedback', '
                var labels = {
                    "FeedBackModel_name": "'.$model->getAttributeLabel('name').'",
                    "FeedBackModel_email": "'.$model->getAttributeLabel('email').'",
                    "FeedBackModel_message": "'.$model->getAttributeLabel('message').'"
                }

                $("#FeedBackModel_name, #FeedBackModel_email, #FeedBackModel_message").click(function(){
                    if($(this).val() == labels[$(this).attr("id")]){
                        $(this).val("");
                    }
                }).blur(function(){
                    if($(this).val() == ""){
                        $(this).val(labels[$(this).attr("id")]);
                    }
                });
            ');

    		$this->render(BlockRenderWidget::setRenderOutput($this),array('model'=>$model,'send'=>$this->send));
		} else {
		    echo '';
		}
	}

	public function validate(){ 
        return true ;
    }
    
    public function params(){
        return array(
            'title'=>'Title',
            'cssclass'=>'CSS Class'
        );
    }
    
    public function beforeBlockSave(){
        return true;
    }
    
    public function afterBlockSave(){
        return true;
    }
}
