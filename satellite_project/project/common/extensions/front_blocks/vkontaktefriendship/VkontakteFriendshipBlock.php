<?php
/**
 * Class for render VkontakteFriendship * 
 * 
 * @author Bazai best programmer <developer@studiobanzai.com>
 * @version 0.1
 * @package common.front_blocks.vkontaktefriendship */

class VkontakteFriendshipBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='vkontaktefriendship';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';
    public $class;
    public $url;
    public $width;
    public $height;    
        
    
    public function setParams($params)
    {
        $this->class = isset($params['class']) ? $params['class'] : '';
        $this->url = isset($params['url']) ? $params['url'] : '';
        $this->width = isset($params['width']) ? $params['width'] : '';
        $this->height = isset($params['height']) ? $params['height'] : '';
          return; 
    }
    
    public function run()
    {                 
           $this->renderContent();         
    }       
 
 
    protected function renderContent()
    {     
       if(isset($this->block) && ($this->block!=null)){
       			//Start working with VkontakteFriendship here
				$params=unserialize($this->block->params);
	    		$this->setParams($params);                            
            	$this->render(BlockRenderWidget::setRenderOutput($this),array());                                                          	       		     
		} else {
			echo '';
		}
			  
       
    }
    
    public function validate(){	
		return true ;
    }
    
    public function params()
    {
         return array(
            'class' => t('Css class'),
            'url'   => t('Vkontakte group id'),
            'width' => t('Width'),
            'height' => t('Height'), 
         );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
	
	
}

?>