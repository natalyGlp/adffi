<?php if (isset($menus)):?>
      <menu class="<?php echo $this->getAlign().' '.$this->class?>">
			<?php if ($this->title!=''){?>
				<h3 class="menu_title">
					<?php if ($this->title_href=='') echo $this->title; else echo CHtml::link($this->title,$this->title_href); ?>
				</h3>
			<?php } 
			$this->renderMenu($menus, $this->menu_id);
			?>
		<br style="clear: left">
		</menu>      
<?php endif;
?>