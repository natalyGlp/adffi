<?php if ($this->content_list != null) : ?>
<div class="horizontal_slider_container">
    <?php $block_id='Block'.$this->block->block_id;?>
		<?php 
            $content = array(); 
			foreach ($this->content_list as $id => $cont) {
                $content_list_data_provider = ContentCarouselBlock::getContentList($cont, null , null, ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD);
                $content = array_merge($content, $content_list_data_provider);
            } 
              
            $this->navitemscount = 3;
            $count = count($content);
            $item_width = (int)$this->width/$this->navitemscount;
            $height = $this->full_height;
            $content_height = $this->full_height-$this->nav_height;
            $nav_height = $this->nav_height;
        ?>       
    <contentslider id="<?php echo $block_id; ?>">
        <ul style="width: <?php echo $this->width; ?>px; height: <?php echo $nav_height; ?>px" class="jcarousel jcarousel-skin-tango">
            <?php $i = 1; foreach($content as $key => $value): 
                $thumb = GxcHelpers::getResourceObjectFromDatabase($value, 'thumbnail', 'filter_brands_');
                $img = isset($thumb[0]['link']) ? $thumb[0]['link'] : '';
            ?>
                <li style="width:73px;height:73px;">
                    <a alt="<?php echo $value->object_name;?>" href="<?php echo Object::getLink($value->object_id); ?>" style="display:block;width:73px;height:73px;line-height:73px;background:url('<?php echo $img;?>') no-repeat center center;"></a>
                </li>
            <?php $i++; endforeach;?>
        </ul>
        <div class="clear"></div>
    </contentslider>

    <script type="text/javascript">
        $(window).load(function() {
            $('#<?php echo $block_id; ?> ul').jcarousel({
                vertical: true,
                scroll: 3,
                circular: true
            });
        });
    </script>
</div>
<?php endif;?>