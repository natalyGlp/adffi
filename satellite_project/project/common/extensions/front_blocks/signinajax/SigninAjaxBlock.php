<?php

/**
 * Class for render Sign in Box ajax
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.signin
 */
// TODO: пускай этот класс наследует SignupBlock
class SigninAjaxBlock extends CWidget
{

    //Do not delete these attr block, page and errors
    public $id='signinajax';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';
    public $refresh=false;
    public $redirect_to_backend=false;

    
    public function setParams($params){
        //  var_dump($params);
      $this->refresh=isset($params['refresh']) ? true : false; 
      $this->redirect_to_backend=isset($params['redirect_to_backend']) ? true : false; 
  }

  public function run()
  {     
    Yii::app()->clientScript->registerCoreScript('jquery.ui');
    Yii::app()->clientScript->registerCssFile(
        Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');
    Yii::app()->clientScript->registerCssFile(
        Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery.ui.dialog.css');

    $this->renderContent();

}       


protected function renderContent()
{
    $params=unserialize($this->block->params);
    $this->setParams($params);
    $login=false;
    $reg=false; 
    if(isset($this->block) && ($this->block!=null)){


        if(isset($_GET['required'])){        	
        	user()->setFlash('error',t('You need to sign in before continue'));
        }
        /* Проверка авторизации пользователя*/
        $model=new UserLoginForm;

        $reg_model=new UserRegisterForm;
         /*$reg_model->username='qwe'.time();
        $reg_model->email='qwe'.time().'@asd.ru';
        $reg_model->password='qweqwe';*/
		// if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
         echo CActiveForm::validate($model);
         Yii::app()->end();
     }

		// collect user input data
     if(isset($_POST['UserLoginForm']))
     {
         $model->attributes=$_POST['UserLoginForm'];
			// validate user input and redirect to the previous page if valid
         if($model->validate() && $model->login()){
            $login=true;
        }

    } 
    /* конец проверки авторизации пользователя*/       
    /* Проверка регистрации пользователя*/      
    if(isset($_POST['UserRegisterForm'])){
        $reg_model->attributes=$_POST['UserRegisterForm'];

                        // validate user input password
        if($reg_model->validate()){
            $new_user = new User;
            $new_user->scenario='create';
            //$new_user->username=$reg_model->username;                                                               
            $new_user->username=$new_user->email=$reg_model->email;                                                                                               
            $new_user->display_name=$reg_model->username;
            $new_user->password=$reg_model->password;           

            $reg=$new_user->save();  
            $new_user->sendActivationEmail();                           
        }
    }      
    /* Проверка регистрации пользователя*/            
    $this->render(BlockRenderWidget::setRenderOutput($this),array('model'=>$model,'login'=>$login,'reg_model'=>$reg_model, 'reg'=>$reg));
} else {
   echo '';
}

}

public function validate(){ 
    return true ;
}

public function params()
{
   return array(
    'redirect_to_backend'=>t('Redirect to Adminpanel'),
    'refresh'=>t('Refresh the page'));
}

public function beforeBlockSave(){
    return true;
}

public function afterBlockSave(){
    return true;
}
}

?>