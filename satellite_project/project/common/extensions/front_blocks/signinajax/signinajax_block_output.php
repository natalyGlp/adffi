<? $blockid=$this->block->block_id; /* id блока*/  
$loguotlink='<a id="logoutlink'.$this->block->block_id.'" class="loguot_link" href="'.SITE_NAME_URL.'app/logout/'.'">'.T('Logout').'</a>'; // ссылка для выхода. ?>
<div id="blocklayout<? echo $this->block->block_id; ?>">
 <? if (Yii::app()->user->isGuest || $login==true) { 
/* Если пользвалень не залогинен то выводить форму авторизации или регистрации.*/
    ?>
<script>
/* Функция закрытия окна при удачном входе пользователя*/
function close_login_window(data){  
<? if($this->redirect_to_backend && $login==true) 
{?>
    setTimeout( function() {
        window.location='<? echo BACKEND_SITE_URL?>';
        }, 4000);
<? } ?>
<? if($this->refresh && $login==true) 
{?>
    setTimeout( function() {
        window.location='<? echo $_SERVER["REQUEST_URI"];?>';
        }, 4000);
<? } ?> 
    if ($('#login<? echo $blockid; ?>', '<div>'+data+'</div>').length>=1){
        $("#loginlink<? echo $blockid; ?>").remove();
        $("#reglink<? echo $blockid; ?>").remove();
        $("#blocklayout<? echo $blockid; ?>").append('<? echo $loguotlink; ?>');
          setTimeout( function() {
        $("#dialog<? echo $this->block->block_id; ?>").dialog("close");}, 4000);
    } 
 }
</script>
<? /* ФОрма авторизации*/ ?>
<div class="dialog-form" id="dialog<? echo $this->block->block_id; ?>">

    <div class="form-stacked" id="block<? echo $this->block->block_id; ?>">
    <? 
        $options = array(
        'success' => 'js:function(data, textStatus, XMLHttpRequest) {
            $("#block'.$this->block->block_id.'").html($("#block'.$this->block->block_id.'","<div>"+data+"</div>").html());
         close_login_window(data);
         }',
        'error' => 'js:function(XMLHttpRequest, textStatus, errorThrown) { 
            $("#block'.$this->block->block_id.'").html(\'<h3 class="error">Произошла ошибка на сервере.<br/>Перезагрузите страницу и повторите позже.</h3>\');
          }',
         'complete' => 'js:function(XMLHttpRequest, textStatus, errorThrown) {  }'
);

    if ($login) {
        echo CHtml::hiddenField('login'.$blockid,$login,array('id'=>'login'.$blockid));
        echo t('Спасибо что вошли.');?>

    <?
    }  else {?>
    <div class="website-info">
        <h1><?php echo t('Sign into your Account'); ?></h1>
    </div>
    <?php $this->render('cmswidgets.views.notification_frontend'); ?>
    
    <?php $form=$this->beginWidget('CActiveForm', array(
       'id'=>'login-content'.$blockid,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),     
        )); 
    ?>
 
    <div class="clearfix">
        
        <label for="username" class="labelBlur" style="display: inline; "><?php echo t('Email'); ?></label>
       	   <div class="input">
        <?php echo $form->textField($model,'username',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'username'); ?>
        </div>   
    </div>        
     
   <div class="clearfix">
        <label for="password" class="labelBlur" style="display: inline; "><?php echo t('Password'); ?></label>
        <div class="input">
        <?php echo $form->passwordField($model,'password',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'password'); ?>
        </div>
    </div>
    <p style="font:13px Verdana; padding: 5px;"><a href="<?php echo bu();?>/forgot-password"><?php echo t('Forgot password?'); ?></a>                             
                    </p>
    <div class="clearfix">     
         <label><?php echo $form->checkBox($model,'rememberMe',array('style'=>'float:left; margin-right:10px')); ?> <?php echo t('Remember me on this computer') ?></label>
         <?php echo $form->error($model,'rememberMe'); ?>
    </div>
    <div class="actions">
         <?php 
         
         echo CHtml::ajaxSubmitButton(t('Sign in'),'',$options); ?>
    
    </div>
    
    <p style="font:15px Verdana; padding-top: 20px"><?php echo t("Don't have an account?"); ?> 
                            <a class="reg-link" href="#"><?php echo t('Register'); ?></a>.
                    </p>
    <?php $this->endWidget(); ?>
  
    </div> <?  } ?>
</div>
<? /*Форма регистрации */ ?>
<? 
$this->render('common.front_blocks.signinajax.register_form',array('model'=>$reg_model,'reg'=>$reg, 'block'=>$this->block,'login'=>$login)) ?>
<? /* Конец формы авторизации */ ?>
<? /* ссылка для входа и скрипт открытия окна. */ ?>
<a id="loginlink<? echo $this->block->block_id; ?>" class="login_link" href="#"><? echo t('Login') ?></a>
<a id="reglink<? echo $this->block->block_id; ?>" class="registration_link" href="#"><? echo t('Register') ?></a>
<script>

    $(function() {
        <? /*  Открытие формы авторизации*/ ?>
        $('#dialog<? echo $this->block->block_id; ?>').dialog({
            autoOpen: false,
            height: 460,
            width: 350,
            modal: true,
            resizable: false
        });
        $('#loginlink<? echo $this->block->block_id; ?> ,.login-link').live('click', function(event) {
            event.preventDefault();
                $( "#register-form-<? echo $this->block->block_id; ?>" ).dialog( "close" );
                $( "#dialog<? echo $this->block->block_id; ?>" ).dialog( "open" );

            });
        <? /*  Открытие формы регистрации*/ ?>
        $('#register-form-<? echo $this->block->block_id; ?>').dialog({
            autoOpen: false,
            height: 550,
            width: 375,
            modal: true,
            resizable: false
        });
        $('#reglink<? echo $this->block->block_id; ?> , .reg-link').live('click', function(event) {
            event.preventDefault();
                $( "#dialog<? echo $this->block->block_id; ?>" ).dialog( "close" );
                $( "#register-form-<? echo $this->block->block_id; ?>" ).dialog( "open" );
            });
    });
</script>
<? } else { echo $loguotlink; } /* Вывод ссылки выхода если пользователь не залогинен. */?>
</div>
