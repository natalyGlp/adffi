<? $blockid=$block->block_id;  
$loguotlink='<a id="logoutlink'.$this->block->block_id.'" class="loguot_link" href="'.SITE_NAME_URL.'app/logout/'.'">'.T('Logout').'</a>'; // ссылка для выхода.
?>
<script>
/* Функция закрытия окна при удачном входе пользователя*/
function close_reg_window(data){  
<? if($this->redirect_to_backend && $reg==true) 
{?>
    setTimeout( function() {
        window.location='<? echo BACKEND_SITE_URL?>';
        }, 4000);
<? } ?>
<? if($this->refresh && $reg==true) 
{?>
    setTimeout( function() {
        window.location='<? echo $_SERVER["REQUEST_URI"];?>';
        }, 4000);
<? } ?> 
    if ($('#loginreg<? echo $blockid; ?>', '<div>'+data+'</div>').length>=1){
        /*$("#loginlink<? echo $blockid; ?>").remove();
        $("#reglink<? echo $blockid; ?>").remove();
        $("#blocklayout<? echo $blockid; ?>").append('<? echo $loguotlink; ?>');*/
          setTimeout( function() {
        $("#register-form-<? echo $this->block->block_id; ?>").dialog("close");}, 4000);
    } 
 }
</script>
<? 
$options = array(       
        'success' => 'js:function(data, textStatus, XMLHttpRequest) {
            $("#register-form-'.$this->block->block_id.'").html($("#register-form-'.$this->block->block_id.'","<div>"+data+"</div>").html());
            console.log("asd");
         close_reg_window(data);
         }',
        'error' => 'js:function(XMLHttpRequest, textStatus, errorThrown) { 
            $("#register-form-'.$this->block->block_id.'").html(\'<h3 class="error">Произошла ошибка на сервере.<br/>Перезагрузите страницу и повторите позже.</h3>\');
          }',
         'complete' => 'js:function(XMLHttpRequest, textStatus, errorThrown) {  }'
);
?>
<div class="form-stacked dialog-form" id="register-form-<? echo $blockid; ?>" >
    <? if($reg) {
        echo CHtml::hiddenField('loginreg'.$blockid,$login,array('id'=>'loginreg'.$blockid));
        echo t('Спасибо что зарегистрировались.<br/> Вам на почту было отправленно письмо для подтверждения регистрации.');
     } else {
        ?>
    <div class="website-info">
        <h1><?php echo t('Sign up for mywordbook.com'); ?></h1>
    </div>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'userregister-form'.$blockid,
         'enableClientValidation'=>true,
            'clientOptions'=>array(
                    'validateOnSubmit'=>true,
            ),   
        )); 
    ?>

    <div class="clearfix">
      	<label for="user_full_name" class="labelBlur" style="display: inline; "><?php echo t('Username'); ?></label>
        <div class="input">
        <?php echo $form->textField($model,'username',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'username'); ?>
        </div>      
    </div>
        
    <div class="clearfix">
      <label for="user_email" class="labelBlur" style="display: inline; "><?php echo t('Email'); ?></label>
       <div class="input">
        <?php echo $form->textField($model,'email',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'email'); ?>
       </div>
    </div>
    
    <div class="clearfix">
      <label for="user_password" class="labelBlur" style="display: inline; "><?php echo t('Password'); ?></label>
          <div class="input">
        <?php echo $form->passwordField($model,'password',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
        <?php echo $form->error($model,'password'); ?>
        </div>
    </div>
    <div class="actions">
    
         <?php echo CHtml::ajaxSubmitButton(t('Register'),'',$options); ?>
        	<p style="margin-top:10px; color:#aaa">
    <?php echo t('By clicking "Register" you confirm that you accept the'); ?>
                        <a href="<?php echo FRONT_SITE_URL;?>/terms" onclick="window.open(this.href);return false;">Terms of Service.</a>
                </p>
    
    </div>
    
    <p style="font:15px Verdana; padding-top: 20px"><?php echo t('Already have an account?'); ?> 
                            <a class="login-link" href="#"><?php echo t('Sign in here'); ?></a>.
                    </p>
    <?php $this->endWidget(); ?>
    <? } ?>
</div>