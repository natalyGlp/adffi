<?php

/**
 * Class for render form for changing Avatar
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.avatar
 */

class AvatarBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='avatar';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';
        
    
    public function setParams($params){
          return; 
    }
    
    public function run()
    {       
            if(!user()->isGuest){ 
                    $this->renderContent();
              } else {
                 user()->setFlash('error',t('You need to sign in before continue'));                                                            
                Yii::app()->controller->redirect(bu().'/sign-in');
            }
    }       
 
 
    protected function renderContent()
    {     
               
        
            if(isset($this->block) && ($this->block!=null)){	              
                    $model=new UserAvatarForm;
                    if(isset($_POST['UserAvatarForm']))
                    {
                        $model->attributes=$_POST['UserAvatarForm'];
                        $model->image=CUploadedFile::getInstance($model,'image');                                                
                        if($model->validate())
                        {
                            $path = UserAvatarForm::processUploadedImage($model->image); 
                            UserAvatarForm::updateUserAvatar($path);              
                        }
                    }
                   
                                       
            $this->render(BlockRenderWidget::setRenderOutput($this),array('model'=>$model));
            } else {
                echo '';
            }
      
        
	
       
    }
    
    public function validate(){	
		return true ;
    }
    
    public function params()
    {
         return array();
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
}

?>