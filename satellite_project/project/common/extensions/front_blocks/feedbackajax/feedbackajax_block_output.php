<?php $blockid=$this->block->block_id; /* id блока*/  
$options = array(
		'success' => 'js:function(data, textStatus, XMLHttpRequest) {
			var IsJsonString = function(str) {
				try {
					JSON.parse(str);
				} catch (e) {
					return false;
				}
				return true;
			}

			/* Функция закрытия окна при удачном входе пользователя*/
			var close_window = function(data){  
				if ($("#send<? echo $blockid; ?>", "<div>"+data+"</div>").length>=1){
					setTimeout( function() {
						$("#dialog<? echo $this->block->block_id; ?>").dialog("close");
					}, 4000);
				}
			}
			console.log(data);
			if(!IsJsonString(data)){
			$("#feedbackform'.$this->block->block_id.'").html($("#feedbackform'.$this->block->block_id.'","<div>"+data+"</div>").html());
		 close_window(data); } else {
			console.log(data);
			var obj = jQuery.parseJSON(data);
			console.log(obj);
			$("#feedbackform'.$this->block->block_id.' .errorMessage").hide();
			$.each(obj, function(key, val) {
					$("#feedbackform'.$this->block->block_id.' #"+key+"_em_").text(val[0]);                                                    
					$("#feedbackform'.$this->block->block_id.' #"+key+"_em_").show();
					});
		 }
		 }',
		'error' => 'js:function(XMLHttpRequest, textStatus, errorThrown) { 
			$("#feedbackform'.$this->block->block_id.'").html(\'<h3 class="error">Произошла ошибка на сервере.<br/>Перезагрузите страницу и повторите позже.</h3>\');
		  }',
		 'complete' => 'js:function(XMLHttpRequest, textStatus, errorThrown) {  }'
);
 ?>
<div class="ajaxfidback-wrapper" id="blocklayout<? echo $this->block->block_id; ?>">

	<div class="ajaxfeedback-dialog-form" id="dialog<? echo $this->block->block_id; ?>">
	<div class="form-stacked" style="display: none;" id="feedbackform<? echo $this->block->block_id; ?>">
	<?php if ($this->sendflag) {
		echo CHtml::hiddenField('send'.$blockid,$send,array('id'=>'send'.$blockid));
		echo t('Спасибо, Ваше письмо отправленно. Мы с Вами свяжемся.');

	}else { ?>
	<?php $this->render('cmswidgets.views.notification_frontend'); ?>
	
	<?php $form=$this->beginWidget('CActiveForm', array(
	   'enableAjaxValidation'=>true,
	   'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'validateOnChange'=>false
	),     
		)); 
	?>
	<div class="clearfix">
		<div class="input">
		<?php echo $form->textField($model,'name',array(
			'size'=>30,
			'class'=>'userform',
			'autoComplete'=>'off', 
			'placeholder' => $this->createPlaceholder($model, 'name'),
		)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div>   
	</div>
	<div class="clearfix">
		<div class="input">
		<?php echo $form->textField($model,'email',array(
			'size'=>30,
			'class'=>'userform',
			'autoComplete'=>'off', 
			'placeholder' => $this->createPlaceholder($model, 'email'),
		)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div>   
	</div>
	<div class="clearfix">
		<div class="input">
		<?php echo $form->textField($model,'phone',array(
			'size'=>30,'class'=>'userform',
			'autoComplete'=>'off', 
			'placeholder' => $this->createPlaceholder($model, 'phone'),
		)); ?>
		<?php echo $form->error($model,'phone'); ?>
		</div>   
	</div>
	<div class="clearfix">
		<div class="input">
		<?php echo $form->textArea($model,'message',array(
			'rows'=>6,
			'cols'=>50, 
			'class'=>'feedback_message',
			'placeholder' => $this->createPlaceholder($model, 'message'),
		)); ?>
		<?php echo $form->error($model,'message'); ?>
		</div>   
	</div>
	<div class="actions">
		 <?php 
		 echo CHtml::ajaxSubmitButton($this->submitButtonTitle,'',$options); ?>
	</div>
	<?php 
	$this->endWidget(); ?>
  <? } ?>
</div>
</div>

<?php if(!$this->bindToSelector): 
$bindToSelector = '#feedbackajax' . $this->block->block_id;
?>
<feedback class="feedbackajax<?php if ($this->cssclass!='') echo ' '.$this->cssclass; ?>" id="feedbackajax<? echo $this->block->block_id; ?>">
	<button type="button" class="feedback-btn"><?=$this->submitButtonTitle?></button>    
</feedback>
<?php else:
$bindToSelector = $this->bindToSelector;
endif; ?>


<script>
$(function() {
	<? /*  Открытие почтовой формы */ ?>
	$('#dialog<? echo $this->block->block_id; ?>').dialog({
		autoOpen: false,
		title: '<?= $this->formTitle ? $this->formTitle : ''; ?>',
		modal: true,
		draggable: false,
		resizable: false,
		create:function () {
			$('.ui-dialog').addClass('popup-contact-form feedbacklink');
			$('.form-stacked').css({'display':'block'});
		}
	}).parent().css('position', 'fixed');
	$('<? echo $bindToSelector; ?> ').live('click', function(event) {
		event.preventDefault();
		$( "#dialog<? echo $this->block->block_id; ?>" ).dialog( "open" );
	});
});
</script>

</div>