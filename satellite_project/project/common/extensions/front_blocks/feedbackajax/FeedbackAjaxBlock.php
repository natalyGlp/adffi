<?php

/**
 * Class for render FeebBack ajax
 * 
 * 
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.feedback
 */
class FeedbackAjaxBlock extends CWidget
{
	
	//Do not delete these attr block, page and errors
	public $id='feedbackajax';
	public $block=null;     
	public $errors=array();
	public $page=null;
	public $layout_asset='';
	public $send = '';
	public $sendflag = false;
	public $cssclass = '';
	public $bindToSelector = false;
	public $logEmails = false;
	public $formTitle = 'Связаться с нами';
	public $submitButtonTitle = 'Отправить';
	public $mailTemplate = 'feedback'; // id шаблона почтового сообщения (шаблон создается в админке)

	public function setParams($params){ 
		$this->cssclass = isset($params['cssclass']) ? $params['cssclass'] : '';
		$this->mailTemplate = isset($params['mailTemplate']) ? $params['mailTemplate'] : '';
		$this->formTitle = isset($params['formTitle']) && !empty($params['formTitle']) ? $params['formTitle'] : false;
		$this->bindToSelector = isset($params['bindToSelector']) ? $params['bindToSelector'] : false;
		$this->logEmails = isset($params['logEmails']) ? $params['logEmails'] : false;
		$this->submitButtonTitle = isset($params['submitButtonTitle']) ? $params['submitButtonTitle'] : $this->submitButtonTitle;
	}

	public function run(){
		$this->renderContent();
	}
	
	function createPlaceholder($model, $attribute)
	{
		return $model->getAttributeLabel($attribute) . ($model->isAttributeRequired($attribute) ? ' *' : '');
	}

	protected function renderContent(){
		$params=unserialize($this->block->params);
		$this->setParams($params); 
		$url = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/', true);
		Yii::app()->clientScript->registerCssFile($url . "/feedback.css");
		Yii::import('common.front_blocks.' . $this->id . '.models.*');
		$modelClassName = str_replace('Block', 'Model', get_class($this));
		$model = new $modelClassName();
		
		if (isset($_POST[$modelClassName]))
		{
			// AJAX валидация
			if(isset($_POST['ajax']))
			{
				// очищаем буфер вывода
				while(@ob_end_clean());
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			$model->attributes=$_POST[$modelClassName];
			if ($model->validate())
			{
				$m = new YiiMailMessage;
				$m->addTo(Yii::app()->settings->get('system','support_email'));
				$m->from = Yii::app()->settings->get('system','support_email');

				$mailtemplate = MailTemplate::getModelBySlug($this->mailTemplate);
				if($this->logEmails)
				{
					$this->sendflag = $this->logEmail($model);
					$subject = t('New email on your site');
					$link = Yii::app()->createAbsoluteUrl('/') . '/project/backend/beemail';
					$text = t('Some one has send you email, you can see it here: ' . CHtml::link($link, $link));
				}
				else
				{
					if ($mailtemplate)
					{
						$keys = array_map(function ($val) {return '{' . $val . '}';}, array_keys($model->attributes));
						$keys[] = '{subject}';
						/* feedback mail template values*/
						$values = array_values($model->attributes);
						$values[] = $mailtemplate->mail_title;
						$vars = array($keys, $values);
						
						$subject=$mailtemplate->prepareMail('mail_title', $vars);
						$text=$mailtemplate->prepareMail('mail_text', $vars);
					} else {
						$subject=$mailtemplate->mail_title;
						$text='<pre>' . var_export($model->attributes) . '</pre>';
					}
				}

				$m->subject=$subject;
				$m_content=$text;
				$m->setBody($m_content, 'text/html');
				Yii::app()->mail->send($m);
				$this->sendflag=true;

				if ($this->sendflag)  {
					$this->send=t('Mail send.'); 
				} else {
					$this->send=t('Error when send');
				}
			} else {
				$this->send=t('Error validating');
			}
		}


		if(isset($this->block) && ($this->block!=null)){
					$this->render(BlockRenderWidget::setRenderOutput($this),array('model'=>$model,'send'=>$this->send));
		} else {
			echo '';
		}
	}

	public function logEmail($model)
	{
		return EmailLog::saveForm($model);
	}

	public function validate(){ 
		return true ;
	}
	
	public function params(){
		return array(
			'cssclass'=> t('CSS Class'),
			'mailTemplate'=>t('Mail Template Slug'),
			'bindToSelector'=>t('Bind feedback to element selector (jQuery)'),
			'formTitle'=>t('Feedback Form Title'),
			'logEmails' => t('Save emails on backend'),
			'submitButtonTitle' => t('Submit Button Title'),
		);
	}
	
	public function beforeBlockSave(){
		return true;
	}
	
	public function afterBlockSave(){
		return true;
	}
}
