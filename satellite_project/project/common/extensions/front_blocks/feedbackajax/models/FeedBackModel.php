<?php
/**
 * This is the model class for Feedback Form.
* 
 * 
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.feedback
 */
class FeedBackModel extends CFormModel {
	public $name;
	public $email;
	public $address;
	public $message;
	public $phone;
	public $subject;
	public $name2;
	public function rules(){
		return array(
			array('name, email, message','required','message'=>t('The field \'{attribute}\' can not be empty.')),
			array('email','email')
		);
	}

	public function attributeLabels(){
		return array(
	        'name'=>t('Имя, фамилия'),
			'email'=>t('E-mail Адрес'),
			'phone'=>t('Телефон'),
			'address'=>t('Адрес'),
			'message'=>t('Текст сообщения'),
			'subject'=>t('Тема'),
			'name2'=>t('Наименование юридического лица или ФИО физического лица'),
		);
	}
	public function activeCaptcha() {
		$code = Yii::app()->controller->createAction('captcha')->getVerifyCode();
		if ($code != $this->verifyCode)
			$this->addError('verifyCode', 'Неправильный код проверки.');

		if(!Yii::app()->request->isAjaxRequest)
			Yii::app()->controller->createAction('captcha')->getVerifyCode(true);
	}
}