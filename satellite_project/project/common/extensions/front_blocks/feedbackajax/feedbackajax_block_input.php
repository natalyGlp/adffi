<div name="div-block-content-<?php echo $block_model->id;?>">
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'formTitle'),''); ?>
		<?php echo CHtml::textField("Block[formTitle]", $block_model->formTitle ,array('id'=>'Block-formTitle', )); ?>
		<?php echo $form->error($model,'formTitle'); ?>
	</div>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'submitButtonTitle'),''); ?>
		<?php echo CHtml::textField("Block[submitButtonTitle]", $block_model->submitButtonTitle ,array('id'=>'Block-submitButtonTitle', )); ?>
		<?php echo $form->error($model,'submitsubmitButtonTitle'); ?>
	</div>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'cssclass'),''); ?>
		<?php echo CHtml::textField("Block[cssclass]", $block_model->cssclass ,array('id'=>'Block-cssclass', )); ?>
		<?php echo $form->error($model,'cssclass'); ?>
	</div>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'bindToSelector'),''); ?>
		<?php echo CHtml::textField("Block[bindToSelector]", $block_model->bindToSelector ,array('id'=>'Block-bindToSelector', )); ?>
		<?php echo $form->error($model,'bindToSelector'); ?>
	</div>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'mailTemplate'),''); ?>
		<?php echo CHtml::textField("Block[mailTemplate]", $block_model->mailTemplate ,array('id'=>'Block-mailTemplate', )); ?>
		<?php echo $form->error($model,'mailTemplate'); ?>
	</div>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'logEmails'),''); ?>
		<?php echo CHtml::checkBox("Block[logEmails]", $block_model->logEmails ,array('id'=>'Block-logEmails', )); ?>
		<?php echo $form->error($model,'logEmails'); ?>
	</div>
</div>
