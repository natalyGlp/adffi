<?php
/**
 * Class for render Logo * 
 * 
 * @author Tuan Nguyen <voinovev@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.logo */
class LogoBlock extends CWidget
{

    //Do not delete these attr block, page and errors
    public $id = 'logo';
    public $block = null;
    public $errors = array();
    public $page = null;
    public $layout_asset = '';
    public $class;
    public $logoPath;

    public function setParams($params)
    {
        $this->class = isset($params['class']) ? $params['class'] : '';
        $this->logoPath =  isset($params['logoPath'])  ? $params['logoPath'] : '';

        return;
    }

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        if (isset($this->block) && ($this->block != null))
        {
            //Start working with Logo here
            $params = unserialize($this->block->params);
            $this->setParams($params);
            $this->render(BlockRenderWidget::setRenderOutput($this), array());
        } else
        {
            echo '';
        }
    }

    public function validate()
    {
        return true;
    }

    public function params()
    {
        return array(
                    'logoPath' => t('Logo url'),
                    'class' => t('Css class')
            );
    }

    public function beforeBlockSave()
    {
        return true;
    }

    public function afterBlockSave()
    {
        return true;
    }

}

?>