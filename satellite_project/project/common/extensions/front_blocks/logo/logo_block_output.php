<div class="logoBlock<?php echo !empty($this->class)?' '.$this->class:''; ?>">
	<a href="<?php echo FRONT_SITE_URL; ?>">
		<img src="<?php echo $this->logoPath ?>" title="<?php echo settings()->get('general', 'site_name'); ?>" alt="<?php echo settings()->get('general', 'site_name'); ?>" />   
	</a>
</div>