<?php

/**
 * Class for render Block
 * 
 * 
 * @author Yura Kovalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package common.front_blocks.html
 */

class CategorypageBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='categorypage';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    

    public $title;
    public $pagination=10;

    
    
    public function setParams($params){
	    $this->title=isset($params['title']) ? $params['title'] : '';
	    $this->pagination=isset($params['pagination']) ? $params['pagination'] : 10;
    }
    
    public function run()
    {
        $this->renderContent();
    }       
 
 
    protected function renderContent()
    {
        if(isset($this->block) && ($this->block!=null)){
            $params=unserialize($this->block->params);
            $this->setParams($params);
            $objectSlug = $this->controller->objectSlug;
            if ($objectSlug)
              $term = Term::model()->findByAttributes(array('slug'=>$slug));
            else $term = false;
            if ($term) {
                $condition = 't.object_status = :status and t.object_date < :time and object_id in (select object_id from {{object_term}} where term_id=:term_id)';
                $params = array(':status' => ConstantDefine::OBJECT_STATUS_PUBLISHED, ':time' => time(),':term_id'=>$term->term_id);
                $dataProvider= new CActiveDataProvider('Object',array(
                        'criteria' => array(
                            'condition'=>$condition,
                            'params'=>$params,
                        ),
                        'pagination' => array(
                            'pageSize' => $this->pagination,
                            'pageVar' => 'page'
                        )
                    )
                );
                $this->render(BlockRenderWidget::setRenderOutput($this),array('posts'=>$dataProvider,'term'=>$term));
            }
            else
            {
                throw new CHttpException('404',t('Oops! Page not found!'));
            }
	    } else {
            echo '';
        }

    }
    
    public function validate(){
			return true ;
    }
    
    public function params()
    {
            return array(
                    'title'=>t('Title'),
                    'pagination' => t('Pagination Size (0 -- pagination page unlimited)'),
            );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
}

?>
