<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'title'),''); ?>
	<?php echo CHtml::textField("Block[title]",$block_model->title); ?>
	<?php echo $form->error($model,'title'); ?>
</div>
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'pagination'),''); ?>
    <?php echo CHtml::textField("Block[pagination]",$block_model->pagination); ?>
    <?php echo $form->error($model,'pagination'); ?>
</div>
</div>
