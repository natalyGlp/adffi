<h1> <? echo $term->name ?></h1>
<?
$render_view='common.front_blocks.categorypage.item_render_list';
if (GxcHelpers::getPathOfAlias('common.front_blocks.categorypage.'.$term->slug.'_item_render_list'))
    $render_view='common.front_blocks.categorypage.'.$term->slug.'_item_render_list';
    $this->widget('zii.widgets.CListView',
        array
        (
            'viewData'=>array('asset'=>$this->layout_asset),
            'dataProvider'=>$posts,
            'itemView'=>$render_view,
            'summaryText'=>'',
            'ajaxUpdate'=>true,
            'enablePagination'=> true,
            'enableSorting'=>false,
            'sortableAttributes'=>array(),
        )
);