<?php
$link = $data->getObjectLink();
?>
<blockquote>
    <div>
        <p style="margin-top:0;">
            <? $thumb=GxcHelpers::getResourceObjectFromDatabase($data,'thumbnail');
            if (count($thumb)>=1) {
                //echo CHtml::link(CHtml::image($thumb[0]['link'],$data->object_title,array('align'=>'left')),Object::getLink($data->object_id));
            } ?>
            <strong><a href="<?php echo $link; ?>"><?php echo CHtml::encode($data->object_name); ?></a></strong>
            <span><?php //echo date("m/d/Y", $data->object_date); ?></span>
            <br />
        <div class="content">
            <?php echo $data->getExcerpt(); ?>
            <a  class="read-more" href="<? echo Object::getLink($data->object_id);?>" title=""><? echo t('Перейти к статье>'); ?></a>
        </p>
    </div>
</blockquote>