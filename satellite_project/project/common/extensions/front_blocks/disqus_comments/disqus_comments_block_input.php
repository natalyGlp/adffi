<div name="div-block-content-<?php echo $block_model->id;?>">
    <div class="row">
      <?php echo CHtml::label(Block::getLabel($block_model, "shortname"), ""); ?>
      <?php echo CHtml::textField("Block[shortname]", $block_model->shortname, array("id" => "Block-shortname"));?>
      <?php echo $form->error($model, "shortname"); ?>
    </div>    
    <div class="row">
      <?php echo CHtml::label(Block::getLabel($block_model, "class"), ""); ?>
      <?php echo CHtml::textField("Block[class]", $block_model->class, array("id" => "Block-class"));?>
      <?php echo $form->error($model, "class"); ?>
    </div>