<div class="vertical_slider_container">
<?php 
$block_id='Block'.$this->block->block_id;
?>
	<?php if ($this->content_list != null) : ?>
		<?php 
        $content = array();   	
				foreach ($this->content_list as $id => $cont) {
          $content_list_data_provider = ContentSliderBlock::getContentList($cont, null , null, ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD);
          $content = array_merge($content,$content_list_data_provider);
        }
                $count=count($content);
                $item_height=(int)$this->height/$count;
                $width=$this->full_width;
                $content_width=$this->full_width-$this->nav_width;
                $nav_width=$this->nav_width;
  ?>       
  <?php endif;
   ?>
<script>
$(document).ready(function()
{
  $("#<? echo $block_id; ?>.newslider-vertical").sliderkit({
          auto: <? if ($this->autoplay) echo 'true'; else echo 'false'; ?>  ,
          autospeed: <? echo $this->autospeed; ?>,
          circular: <? if ($this->circular) echo 'true'; else echo 'false'; ?>,
          shownavitems: <? echo $count ?>,
          panelfx: '<? echo $this->panelfx; ?>',
          panelfxspeed:<? echo $this->panelfxspeed; ?>,
          mousewheel: <? if ($this->mousewheel) echo 'true'; else echo 'false'; ?>,
          navitemshover: <? if ($this->navitemshover) echo 'true'; else echo 'false'; ?>,
          fastchange:false,
          verticalnav: true,
          verticalpanels: true,
        }); 
});
</script>

<contentslider  id="<? echo $block_id; ?>" class="sliderkit newslider-vertical <? if ($this->class!='') echo $this->class; ?>" style="width: <? echo $width; ?>px; height: <?= $item_height*$count; ?>px">
          <div class="sliderkit-nav" style="height: <? echo $this->height; ?>px; width: <?= $nav_width; ?>px">
            <div class="sliderkit-nav-clip" style="height: <? echo $this->height; ?>px; width: <?= $nav_width; ?>px">
              <ul style="width: <? echo $this->nav_width; ?>px;">
                <? foreach ($content as $key => $value) { ?>
                <li style="height: <? echo $item_height-5; ?>px;"><a href="#" title="[link title]"><? echo $value->object_name; ?></a></li>
                <? } ?>
              </ul>
            </div>
          </div>
          <div class="sliderkit-panels" style="height: <? echo $this->height; ?>px; width: <?= $content_width; ?>px">
            <? foreach ($content as $key => $value) { ?>
            <div class="sliderkit-panel" style="height: <? echo $this->height-20; ?>px; width: <?= $content_width-20; ?>px;">
              <div class="sliderkit-news">
                <a href="<? echo $value->getObjectLink() ?>">
                  <? 
                  $list_current_resource=GxcHelpers::getResourceObjectFromDatabase($value,'thumbnail');
                if (count($list_current_resource)>=1) 
                {
                  echo CHtml::image($list_current_resource[0]['link'],''); 
                } ?>
                  
                <h3><? echo $value->object_name; ?></h3>
                <? echo $value->object_content; 
                                 ?>
              </a>
              </div>
            </div>
            <? } ?>
            </div>
          </div>
</contentslider>
</div>