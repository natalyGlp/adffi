<?php  
class ContentSliderv extends CModel 
{
	/* Slider proporites */
    public $width=900;
    public $full_height=200;
    public $nav_height=50;
    public $class='';
    
    /* add params*/
    public $autoplay=true; // + 
    public $circular=true; // true/false
    public $mousewheel=true; // true/false
    public $navitemshover=false; // true/false
    public $panelfx='sliding'; //fading,sliding,none
    public $panelfxspeed='1000';
    public $autospeed=1000; // true/false
    /* add params*/
    /* slider proporites*/
    public function rules() 
    {
    	return array(
    		array('full_height, full_width, nav_width, panelfxspeed, autospeed','numerical'),
            array('autospeed','numerical','min'=>'1000','max'=>'10000'),
    		array('panelfxspeed','numerical','min'=>'100','max'=>'10000'),
    		array('width, nav_height,full_height','required'),
            array('autoplay, circular, mousewheel, navitemshover','boolean','allowEmpty'=>true)

    		);
    }
    public function attributeNames() 
    {
    	return array(
    		'width'=>t('asd'),
    		'full_height'=>t('Высота'),
    		);
    }
    public function attributeLabels() 
    {
    	return array(
    		'width'=>t('Width'),
    		'full_height'=>t('Height'),
    		);
    }
}