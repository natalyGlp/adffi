<?php

/**
 * Class for render Content based on Content list
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.html
 */

class ContentSlidervBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='contentsliderv';
    public $block=null;    
    public $errors=array();
    public $page=null;
    public $layout_asset='';
    /* Slider proporites */
    public $height=500;
    public $full_width=900;
    public $nav_width=100;
    public $class='';
    
    /* add params*/
    public $autoplay=true; // + 
    public $circular=true; // true/false
    public $mousewheel=true; // true/false
    public $navitemshover=false; // true/false
    public $panelfx='fading'; //fading,sliding,none
    public $panelfxspeed='1000';
    public $autospeed=1000; // true/false
    /* add params*/
    /* slider proporites*/
    
    //Content list attribute
    public $content_list;    
	public $display_type;	
	
	//Display types for the list view render 
	const DISPLAY_TYPE_HOMEPAGE=0;
	  	
    const DISPLAY_TYPE_CATEGORY=1;
    
    
    public function setParams($params){
       // var_dump($params);
        $this->content_list=isset($params['content_list']) ? $params['content_list'] : null;
        $this->display_type=isset($params['display_type']) ? $params['display_type'] : self::DISPLAY_TYPE_HOMEPAGE;
        $this->class=isset($params['class']) ? $params['class'] : '';
        $this->height=isset($params['height']) ? $params['height'] : '500';
        $this->full_width=isset($params['full_width']) ? $params['full_width'] : '900';
		$this->nav_width=isset($params['nav_width']) ? $params['nav_width'] : '100';
        $this->autoplay=isset($params['autoplay']) ? true : false;
        $this->circular=isset($params['circular']) ? true : false;
        $this->mousewheel=isset($params['mousewheel']) ? true : false;
        $this->navitemshover=isset($params['navitemshover']) ? true : false;
        $this->panelfx=isset($params['panelfx']) ? $params['panelfx'] : 'fading';
        $this->panelfxspeed=isset($params['panelfxspeed']) ? $params['panelfxspeed'] :  $this->panelfxspeed;
        $this->autospeed=isset($params['autospeed']) ? $params['autospeed'] : $this->autospeed;
    }
    public function run()
    {
            $this->renderContent();
    }       
 
 
    protected function renderContent()
    {

        $url = Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.frontend'), false);
        Yii::app()->clientScript->registerCssFile($url."/slider/css/sliderkit-core.css");
        Yii::app()->clientScript->registerScriptFile($url."/slider/js/external/jquery.mousewheel.min.js");
        Yii::app()->clientScript->registerScriptFile($url."/slider/js/external/jquery.easing.1.3.min.js");
        Yii::app()->clientScript->registerScriptFile($url."/slider/js/sliderkit/jquery.sliderkit.1.9.2.pack.js");
		if(isset($this->block) && ($this->block!=null)){	    
	            //Set Params from Block Params
	            $params=unserialize($this->block->params);
		    	$this->setParams($params);            	                                        
	            $this->render(BlockRenderWidget::setRenderOutput($this),array());
		} else {
		    echo '';
		}
       
    }
    public function validate(){
        Yii::import('common.front_blocks.contentslider.*');
        $model= new ContentSlider; 
    $model->attributes=$_POST['Block'];
    if ($model->validate()) 
    {
        return true;
    } else {
        $this->errors=$model->getErrors();
        return false;
    }
    }
    
    public function params()
    {
            return array(
                    'content_list' => t('Content list'),                   
                    'display_type' => t('Display type'),
                    'height' => t('Height'),
                    'class' => t('Css class'),
                    'autoplay' => t('Autoplay'),
                    'circular' => t('circular'),
                    'mousewheel' => t('Mousewheel'),
                    'panelfx' => t('Panel Effect'),
                    'panelfxspeed' => t('Panel Effect Speed'),
                    'autospeed' => t('Slider Speed'),
                    'navitemshover' => t('Change slide when navigation hover. '),
                    'nav_width' => t('Navigation width'),
                    'full_width' => t('Full width'),
            );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
	public function getSlideEffects() 
    {
        return array(
            'sliding'=>'Sliding',
            'fading'=>'Fading',
            'none'=>'none',
            );
    }
	public static function getDisplayTypes(){
        return array(
           self::DISPLAY_TYPE_HOMEPAGE=>t("Display in Homepage"),	  	
           self::DISPLAY_TYPE_CATEGORY=>t("Display in Category page"));
    }
	
	public static function getContentList($content_list_id, $max=null, $pagination=null, $return_type=ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD) {        		
			return GxcHelpers::getContentList($content_list_id, $max, $pagination, $return_type);                
    }
}

?>