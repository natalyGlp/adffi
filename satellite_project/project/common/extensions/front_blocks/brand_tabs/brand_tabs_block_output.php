<aside class="car-model-tabs">
        <!-- Start tabs-standard -->
        <div class="sliderkit tabs-standard" style="display: block;">
          <div class="sliderkit-nav">
            <div class="sliderkit-nav-clip">
              <h2>Выбор модели</h2>
              <ul>
                <li class="sliderkit-selected"><a href="#">В продаже</a></li>
                <li><a href="#">Снятые с производства</a></li>
                <li><a href="#">Анонсированные</a></li>
                <li><a href="#">Нет в продаже в Украине</a></li>
              </ul>
            </div>
          </div>

          <div class="sliderkit-panels">
            <div class="sliderkit-panel sliderkit-panel-active" style="display: block;">
                <?php foreach ($models_0 as $key => $value): ?>
                	<?php $this->render('common.content_type.model.item_render_list', array('data' => $value));?>
                <?php endforeach ?>
            </div>
            <div class="sliderkit-panel" style="display: none;">
                <?php foreach ($models_1 as $key => $value): ?>
                	<?php $this->render('common.content_type.model.item_render_list', array('data' => $value));?>
                <?php endforeach ?>
            </div>     
            <div class="sliderkit-panel" style="display: none;">
                <?php foreach ($models_2 as $key => $value): ?>
                	<?php $this->render('common.content_type.model.item_render_list', array('data' => $value));?>
                <?php endforeach ?>
            </div>  
            <div class="sliderkit-panel" style="display: none;">
                <?php foreach ($models_3 as $key => $value): ?>
                	<?php $this->render('common.content_type.model.item_render_list', array('data' => $value));?>
                <?php endforeach ?>
            </div>                                        
          </div>
        </div>
        <!-- // end of tabs-standard -->
        <div class="clear"></div>
    </aside>

  <script type="text/javascript">
      $(window).load(function(){ 
        $(".tabs-standard").sliderkit({
          auto:false,
          tabs:true,
          mousewheel:false,
          circular:true,
          panelfx:"none"
        });     
      });
  </script>