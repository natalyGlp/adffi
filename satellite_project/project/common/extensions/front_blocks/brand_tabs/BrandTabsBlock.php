<?php

/**
 * Class for render Block
 * 
 * 
 * @author Yura Kovalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package common.front_blocks.model_title
 */

class BrandTabsBlock extends CWidget{
    
    //Do not delete these attr block, page and errors
    public $id='brand_tabs';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    
    
    public function run() {        
        $this->renderContent();
    }       
 
 
    protected function renderContent(){
    	if(isset($this->block) && ($this->block!=null)){	    
            // var_dump($this->block->block_id); die();
            $params=unserialize($this->block->params);         /*Переделать*/
            $segments = explode('/', Yii::app()->request->url);
            $slug = isset($segments[3]) ? $segments[3] : 'home';
                    /*Переделать*/
                    //$slug = str_replace('.html','' , $slug);    //Старая схема определения slug;
                    $slug = $slug == 'home' ? 'home' : substr($slug,0, strpos($slug,'.html'));
                    $show_title = $slug == 'home' ? false : true;


                    $modelName = 'Object';
                    if(isset($segments[2])){
                        Yii::import('common.content_type.'.$segments[2].'.'.ucfirst($segments[2]).'Object');
                        $modelName = ucfirst($segments[2]).'Object';
                    }

                    if ($slug)
                        $content = $modelName::model()->with('language')->find(array('condition'=>'object_slug=:paramId','params'=>array(':paramId'=>$slug))); 
                    if (isset($content)) {
                        $post_id = $content->object_id; 
                    
                        if(!isset($post_id))  
                            $post_id=(int)$_GET['id'];

                        unset($content);
                    }

                    if(isset($post_id)){
                        $post = Yii::app()->cache->get(Yii::app()->request->url);
                                
                        if(!$post){
                            $post=$modelName::model()->with('language')->find(array(
                                'select' => array('object_id', 'object_content', 'object_name', 'object_date', 'object_slug', 'object_type'),
                                'condition' => 't.object_id=?',
                                'params' => array($post_id)
                            ));    
                            Yii::app()->cache->set(Yii::app()->request->url, $post, 7200);  
                        }   

                            
                $cur_lang=Yii::app()->translate->getLanguage();
                $cur_lang_id=Language::model()->findByAttributes(array('lang_name'=>$cur_lang));
                if ($post->language->lang_name!=$cur_lang){
                    /*
                    Выводить сообщение либо редиректить
                    */
                    $other_content= $modelName::model()->findByAttributes(array('lang'=>$cur_lang_id->lang_id,'guid'=>$post->guid)); 
                    // var_dump($cur_lang_id->lang_id,$post->guid,$other_content);

                    if (!empty($other_content)) {
                        Yii::app()->controller->redirect(Object::getLink($other_content->object_id));
                    } else  { 
                        /*ДОработка*/
                        /* Вывести страницу с сообщением что данный вид контента не создан для даннйо языковой версии*/
                        //throw new CHttpException('404',t('Oops! Page not found!')); 
                    }
                }

                if($post){
                    Yii::import('common.content_type.model.ModelObject');
                    $db = Yii::app()->{CONNECTION_NAME};

                    $ids = $sellable_ids = $anonseds_ids = $no_sells_ids = $no_in_uk_ids = array();

                    $_ids = $db->createCommand()
                               ->select('meta_object_id')
                               ->from('{{object_meta}}')
                               ->where('meta_key=? AND meta_value=?', array('brand_id', $post->object_id))
                               ->queryAll(false);

                    $ids[0] = 0;
                    foreach($_ids as $id){
                        $ids[$id[0]] = $id[0];
                    }

                    $_sellable_ids = $db->createCommand()
                                        ->select('filter1')
                                        ->from('{{filters}}')
                                        ->where('filter9=? AND filter1 IN('.implode(',', $ids).') AND filter1!=""', array(0))
                                        ->queryAll(false);

                    $_anonseds_ids = $db->createCommand()
                                        ->select('filter1')
                                        ->from('{{filters}}')
                                        ->where('filter9=? AND filter1 IN('.implode(',', $ids).') AND filter1!=""', array(1))
                                        ->queryAll(false);

                    $_no_sells_ids = $db->createCommand()
                                        ->select('filter1')
                                        ->from('{{filters}}')
                                        ->where('filter9=? AND filter1 IN('.implode(',', $ids).') AND filter1!=""', array(2))
                                        ->queryAll(false);

                    $_no_in_uk_ids = $db->createCommand()
                                        ->select('filter1')
                                        ->from('{{filters}}')
                                        ->where('filter9=? AND filter1 IN('.implode(',', $ids).') AND filter1!=""', array(3))
                                        ->queryAll(false);
                  

                    foreach($_sellable_ids as $id){
                        $sellable_ids[$id[0]] = $id[0];
                    }

                    foreach($_anonseds_ids as $id){
                        $anonseds_ids[$id[0]] = $id[0];
                    }

                    foreach($_no_sells_ids as $id){
                        $no_sells_ids[$id[0]] = $id[0];
                    }

                    foreach($_no_in_uk_ids as $id){
                        $no_in_uk_ids[$id[0]] = $id[0];
                    }

                    $criteria = new CDbCriteria;
                    $criteria->compare('id_parent', '>0');
                    $criteria->addInCondition('object_id', $sellable_ids);
                    $models_0 = ModelObject::model()->findAll($criteria);

                    $criteria = new CDbCriteria;
                    $criteria->compare('id_parent', '>0');
                    $criteria->addInCondition('object_id', $anonseds_ids);
                    $models_1 = ModelObject::model()->findAll($criteria);

                    $criteria = new CDbCriteria;
                    $criteria->compare('id_parent', '>0');
                    $criteria->addInCondition('object_id', $no_sells_ids);
                    $models_2 = ModelObject::model()->findAll($criteria);

                    $criteria = new CDbCriteria;
                    $criteria->compare('id_parent', '>0');
                    $criteria->addInCondition('object_id', $no_in_uk_ids);
                    $models_3 = ModelObject::model()->findAll($criteria);


                    // print_r(array(
                    //     count($models_2),
                    //     count($models_1),
                    //     count($models_0),
                    //     count($models_3)
                    // ));

                    $this->render(BlockRenderWidget::setRenderOutput($this),array(
                        'post'=>$post, 
                        'show_title' => $show_title,
                        'models_0' => $models_0,
                        'models_1' => $models_1,
                        'models_2' => $models_2,
                        'models_3' => $models_3
                    ));
                } else {
                    throw new CHttpException('404',t('Page not found'));
                }
            } else {
                throw new CHttpException('404',t('Page not found'));    
            }
    	} else {
    	    echo '';
    	}
    }
    
    public function validate(){
        return true;
    }
    
    public function params(){
        return array();
    }
    
    public function beforeBlockSave(){
	   return true;
    }
    
    public function afterBlockSave(){
	   return true;
    }
}