<?php if ($gallery && count($photos)): ?>
    <aside class="sliderkit gallery-model">
		<div class="title">
			<h2>ФОТОГалерея</h2>
			<a href="<?php echo FRONT_SITE_URL.Yii::app()->language;?>/photos/<?php echo $post->object_type;?>/<?php echo $post->object_slug;?>.html" id="open_photos" class="more">смотреть все <?php echo $photos_cnt;?> фото</a>
		</div>
		<div class="sliderkit-nav">
			<div class="sliderkit-nav-clip">
			    <ul>
	            <?php foreach ($photos as $key => $value): ?>
	          	<?php if(!$value->hasImage('_small')) continue;?>
		            <li>
		               	<a href="<?php echo $value->getImage('_thumb');?>">
	                   		<?php echo CHtml::image($value->getImage('_small'), ''); ?>
	                   	</a>	
		            </li>
	            <?php endforeach;?>
			    </ul>
			</div>
			<div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-prev"><a href="#"><span>Previous</span></a></div>
			<div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-next"><a href="#"><span>Next</span></a></div>
		</div>
	</aside>


<script type="text/javascript">
    $(window).load(function(){        
        $(".gallery-model").sliderkit({
          	shownavitems:13,
          	scroll:1,
          	mousewheel:false,
          	circular:true,
          	start:2
        });    
    });
</script>
<?php endif;?>