<?php

/**
 * Class for render Block
 * 
 * 
 * @author Yura Kovalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package common.front_blocks.model_title
 */

class ModelCaruselBlock extends CWidget{
    
    //Do not delete these attr block, page and errors
    public $id='model_carusel';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    
    
    public function run() {    
        $this->renderContent();
    }       
 
 
    protected function renderContent(){ 
    	if(isset($this->block) && ($this->block!=null)){	    
            // var_dump($this->block->block_id); die();
            $params=unserialize($this->block->params);         /*Переделать*/
            $segments = explode('/', Yii::app()->request->url);
            $slug = isset($segments[3]) && preg_match('|(.html)$|i', $segments[3]) ? $segments[3] : (isset($segments[4]) ? $segments[4] : 'home');
                 
            $type = isset($_GET['type']) ? $_GET['type'] : (isset($segments[3]) && !preg_match('|(.html)$|i', $segments[3]) ? $segments[3] : (isset($segments[2]) ? $segments[2] : null));

            /*Переделать*/
            //$slug = str_replace('.html','' , $slug);    //Старая схема определения slug;
            $slug = $slug == 'home' ? 'home' : substr($slug,0, strpos($slug,'.html'));
            $show_title = $slug == 'home' ? false : true;

            $modelName = 'Object';
            if($type){
                $type = $type == 'parent_model' ? 'model' : $type;
                Yii::import('common.content_type.'.$type.'.'.ucfirst($type).'Object');
                $modelName = ucfirst($type).'Object';
            }
            if ($slug)
                $content = $modelName::model()->with('language')->find(array('select'=>array('object_id'),  'condition'=>'object_slug=:paramId','params'=>array(':paramId'=>$slug))); 

            if (isset($content)) {
                $post_id = $content->object_id; 
                    
                if(!isset($post_id))  
                    $post_id=(int)$_GET['id'];

                unset($content);
            }

            if(isset($post_id)){
                $post = Yii::app()->cache->get(Yii::app()->request->url);
                                
                if(!$post){
                    $post=$modelName::model()->with('language')->find(array(
                        'select' => array('object_id', 'object_content', 'object_name', 'object_date', 'object_slug', 'object_type'),
                        'condition' => 't.object_id=?',
                        'params' => array($post_id)
                    ));    
                    Yii::app()->cache->set(Yii::app()->request->url, $post, 7200);  
                }   

                            
                $cur_lang=Yii::app()->translate->getLanguage();
                $cur_lang_id=Language::model()->findByAttributes(array('lang_name'=>$cur_lang));
                if ($post->language->lang_name!=$cur_lang){
                    $other_content= $modelName::model()->findByAttributes(array('lang'=>$cur_lang_id->lang_id,'guid'=>$post->guid)); 

                    if (!empty($other_content)) {
                        Yii::app()->controller->redirect(Object::getLink($other_content->object_id));
                    } else  { 
                        /*ДОработка*/
                        /* Вывести страницу с сообщением что данный вид контента не создан для даннйо языковой версии*/
                        //throw new CHttpException('404',t('Oops! Page not found!')); 
                    }
                }

                if($post){
                                
                    //$gid = get_class($post) == 'ModelObject' ? $post->id_gallery : (isset($post->model->id_gallery) ? $post->model->id_gallery : 0);
                    $gid = (int)Yii::app()->{CONNECTION_NAME}
                                        ->createCommand()
                                        ->select('id')
                                        ->from('{{galleries}}')
                                        ->where('path=?', array($slug))
                                        ->queryScalar();


                    $gallery = Galleries::model()->findByPk($gid);
                    $photos_cnt = GalleryPhotos::model()->count(array(
                        'condition'=>'gallery_id=:id',
                        'params'=>array(':id'=>$gid),
                    ));

                    $photos = GalleryPhotos::model()->findAll(array(
                        'condition'=>'gallery_id=:id',
                        'params'=>array(':id'=>$gid),
                        'limit' => 25
                    ));

                    $this->render(BlockRenderWidget::setRenderOutput($this),array(
                        'post'=>$post, 
                        'show_title' => $show_title,
                        'gallery' => $gallery,
                        'photos_cnt' => $photos_cnt,
                        'photos' => $photos
                    ));
                } else {
                    throw new CHttpException('404',t('Page not found'));
                }
            } else {
                throw new CHttpException('404',t('Page not found'));    
            }
    	} else {
    	    echo '';
    	}
    }
    
    public function validate(){
        return true;
    }
    
    public function params(){
        return array();
    }
    
    public function beforeBlockSave(){
	   return true;
    }
    
    public function afterBlockSave(){
	   return true;
    }
}