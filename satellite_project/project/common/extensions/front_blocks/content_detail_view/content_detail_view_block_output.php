<div class="content-box">
<?php
if (GxcHelpers::getPathOfAlias('common.content_type.'.$post->object_type.'.item_render')) {
    $this->render('common.content_type.'.$post->object_type.'.item_render',array('post'=>$post, 'show_title' => $show_title)); 
} else {
    $this->render('common.content_type.article.item_render',array('post'=>$post, 'show_title' => $show_title));
}
?>
</div>