<div name="div-block-content-<?php echo $block_model->id; ?>">
	<div class="row">
		<?php 
		$sizes = CJSON::decode(Yii::app()->settings->get('system', 'sizes'));
		$values = array();
		$labels = array();
		foreach($sizes as $size)
		{
			$height = empty($size['max_height']) ? 'Auto' : $size['max_height'];
			$data[$size['version']] = "{$size['max_width']}x{$height} ({$size['version']})";
		}

		?>
		<?php echo CHtml::label(Block::getLabel($block_model,'imageSize'),''); ?>
		<?php echo CHtml::dropDownList("Block[imageSize]", $block_model->imageSize, $data); ?>
		<?php echo $form->error($model,'imageSize'); ?>
	</div> 
</div>