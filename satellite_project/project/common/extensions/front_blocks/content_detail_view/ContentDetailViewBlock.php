<?php

/**
 * Class for render Content Detail View
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.content_detail_view
 */

class ContentDetailViewBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='content_detail_view';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    
    
    public function setParams($params){
	   return;
    }
    
    public function run(){ 
        $this->renderContent();
    }       
 
 
    protected function renderContent(){   
        $show_title =  true;
		if(isset($this->block) && ($this->block!=null)){	    
	        //Set Params from Block Params
	        $params=unserialize($this->block->params);
            $this->setParams($params);
            /*Переделать*/
            $segments = explode('/', Yii::app()->request->url);
                    $slug = isset($segments[3]) && preg_match('|(.html)$|i', $segments[3]) ? $segments[3] : (isset($segments[4]) ? $segments[4] : 'home');
                 
            $type = isset($_GET['type']) ? $_GET['type'] : (isset($segments[3]) && !preg_match('|(.html)$|i', $segments[3]) ? $segments[3] : (isset($segments[2]) ? $segments[2] : null));

            /*Переделать*/
            //$slug = str_replace('.html','' , $slug);    //Старая схема определения slug;
            $slug = $slug == 'home' ? 'home' : substr($slug,0, strpos($slug,'.html'));
            $show_title = $slug == 'home' ? false : true;

            $modelName = 'Object';
            if($type){
                $type = $type == 'parent_model' ? 'model' : $type;
                Yii::import('common.content_type.'.$type.'.'.ucfirst($type).'Object');
                $modelName = ucfirst($type).'Object';
            }

            if ($slug)
                $content = $modelName::model()->with('language')->find(array('select'=>array('object_id'),  'condition'=>'object_slug=:paramId','params'=>array(':paramId'=>$slug))); 

            if (isset($content)) {
                $post_id = $content->object_id; 
                    
                if(!isset($post_id))  
                    $post_id=(int)$_GET['id'];

                unset($content);
            }

            if(isset($post_id)){
                $post=$modelName::model()->with('language')->find(array(
                    'select' => array('object_id', 'object_content', 'object_name', 'object_date', 'object_slug', 'object_type', 'description'),
                    'condition' => 't.object_id=?',
                    'params' => array($post_id)
                ));

                $cur_lang=Yii::app()->translate->getLanguage();
                $cur_lang_id=Language::model()->findByAttributes(array('lang_name'=>$cur_lang));

                if ($post->language->lang_name!=$cur_lang){
                    $other_content= $modelName::model()->findByAttributes(array('lang'=>$cur_lang_id->lang_id,'guid'=>$post->guid)); 

                    if (!empty($other_content)) {
                        Yii::app()->controller->redirect(Object::getLink($other_content->object_id));
                    }
                }

                if($post){
                    Yii::app()->controller->pageTitle=CHtml::encode($post->object_title);												
                    Yii::app()->controller->description=CHtml::encode($post->object_description);
                    Yii::app()->controller->keywords=CHtml::encode($post->object_keywords);	
                    Yii::app()->controller->change_title=true;   
                    $this->render(BlockRenderWidget::setRenderOutput($this),array('post'=>$post, 'show_title' => $show_title));	
                } else {
                    throw new CHttpException('404',t('Page not found'));
                }
            } else {
                throw new CHttpException('404',t('Page not found'));	
            }
		} else {
		    echo '';
		}
       
    }
    
    public function validate(){
		return true;
    }
    
    public function params()
    {
            return array(                                     
            );
    }
    
    public function beforeBlockSave(){
		return true;
    }
    
    public function afterBlockSave(){
		return true;
    }
}