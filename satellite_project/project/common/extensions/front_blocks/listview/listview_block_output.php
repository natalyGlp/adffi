<?php 
if ($this->content_list != null) : ?>
    <?php if ($this->display_type == ListViewBlock::DISPLAY_TYPE_HOMEPAGE): ?>
        <aside class="<?php echo $this->block->name;?>">
        <?php
            if ($this->title!=''){
            echo '<div class="title">';
                if ($this->title_link!=''){
                    echo CHtml::link('<h2>'.$this->title.'</h2>', Yii::app()->createAbsoluteUrl('/').$this->title_link);
                } else {
                echo '<h2>'.$this->title.'</h2>'; }

                 echo '<a href="'.Yii::app()->createAbsoluteUrl('/').$this->title_link.'" id="more-tests" class="more-model-artocles hide">все тест драйвы</a>';
                echo '<a href="'.Yii::app()->createAbsoluteUrl('/').$this->title_link.'" id="news_more" class="more-news hide">все новости</a><div class="clear"></div>
                </div>';
            }

            if ($this->max!=''){
                $template=array('template'=>'{items}');
                $max=$this->max;
            } else {
                $max= null;
                $template= array();
            }
            echo '<aside class="all-news">';
            foreach ($this->content_list as $content_list) {
                $content_list_data_provider = ListViewBlock::getContentList($content_list, $max , null, ConstantDefine::CONTENT_LIST_RETURN_DATA_PROVIDER);
                $path_to_render_file='common.front_blocks.listview.item_render';
                if (isset($content_list_data_provider) && $content_list_data_provider != null) : ?>
                <?php 
                    $this->widget('zii.widgets.CListView', array_merge( array(
                        'viewData'=>array('asset'=>$this->layout_asset, 'display_type' => $this->display_type),
                        'dataProvider'=>$content_list_data_provider,
                        'itemView'=>$content_list_data_provider->render_layout,
                        'summaryText'=>'',
                        'emptyText' => '',
                        'ajaxUpdate'=>false,
                        'template' => '{items}', 
                        'enablePagination'=> false,
                        'enableSorting'=>false,
                        'sortableAttributes'=>array(),
                    ), $template));
                endif;
            } 
            echo '<br clear="all"/></aside>';
        ?> 
                <a href="<?php echo Yii::app()->createAbsoluteUrl('/').$this->title_link;?>" class="more hide" id="salons_more"><?php echo t('все предложения автосалонов') ?>&gt;&gt;</a>
                <a href="<?php echo Yii::app()->createAbsoluteUrl('/').$this->title_link;?>" class="more hide" id="specialtheme_more"><?php echo t('в рубрику "СПЕЦТЕМА"');?>&gt;&gt;</a>
                <div class="clear"></div>
            
        <div class="clear"></div>
       </aside>
    <?php else:?>
     <div class="listposts-container <?php echo $this->block->name;?>">
        <?php
            if ($this->title!=''){
                if ($this->title_link!=''){
                    echo '<h2>'.CHtml::link($this->title, Yii::app()->createAbsoluteUrl('/').$this->title_link).'</h2>';
                } else {
                echo '<h2>'.$this->title.'</h2>'; }
            }

            if ($this->max!=''){
                $template=array('template'=>'{items}');
                $max=$this->max;
            } else {
                $max= null;
                $template= array();
            }

            foreach ($this->content_list as $content_list) {
                $content_list_data_provider = ListViewBlock::getContentList($content_list, $max , null, ConstantDefine::CONTENT_LIST_RETURN_DATA_PROVIDER);
                $path_to_render_file='common.front_blocks.listview.item_render';
                
                $segments = explode('/', Yii::app()->request->url);
                $slug = isset($segments[3]) && preg_match('|(.html)$|i', $segments[3]) ? $segments[3] : (isset($segments[4]) ? $segments[4] : 'home');
                 
                $type = isset($_GET['type']) ? $_GET['type'] : (isset($segments[3]) && !preg_match('|(.html)$|i', $segments[3]) ? $segments[3] : (isset($segments[2]) ? $segments[2] : null));

                $slug = $slug == 'home' ? 'home' : substr($slug,0, strpos($slug,'.html'));

                if($type == 'brand' && $slug){
                    $db = Yii::app()->{CONNECTION_NAME};

                    $object_id = $db->createCommand()
                                    ->select('object_id')
                                    ->from('{{object}}')
                                    ->where('object_slug=?', array($slug))
                                    ->queryScalar();

                    $ids = array();

                    $_ids = $db->createCommand()
                               ->select('meta_object_id')
                               ->from('{{object_meta}}')
                               ->where('meta_key=? AND meta_value=?', array('brand_id', $object_id))
                               ->queryAll(false);

                    $ids[0] = 0;
                    foreach($_ids as $id){
                        $ids[$id[0]] = $id[0];
                    }

                    $content_list_data_provider->criteria->addInCondition('t.object_id', $ids);
                    $content_list_data_provider->criteria->compare('t.id_parent', '>0');
                }

                if (isset($content_list_data_provider) && $content_list_data_provider != null) : ?>
                <?php
                    $this->widget('zii.widgets.CListView', array_merge( array(
                        'viewData'=>array('asset'=>$this->layout_asset, 'display_type' => $this->display_type),
                        'dataProvider'=>$content_list_data_provider,
                        'itemView'=>$content_list_data_provider->render_layout,
                        'summaryText'=>'',
                        'ajaxUpdate'=>false,
                        'pager' => array(
                            'header' => '',
                            'nextPageLabel' => '&nbsp;',
                            'prevPageLabel' => '&nbsp;'
                        ),
                        'template' => '{items}<br clear="all"/><hr>{pager}<br clear="all"/>', 
                        'enablePagination'=> true,
                        'enableSorting'=>false,
                        'sortableAttributes'=>array(),
                    ), $template));
                endif;
            } 
        ?>
        <div class="clear"></div>
        </div>
    <?php endif ?>
   
<?php endif; ?>