<?php

/**
 * Class for render Content based on Content list
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.listview
 */
class ListViewBlock extends CWidget {

	//Do not delete these attr block, page and errors
	public $id = 'listview';
	public $block = null;
	public $errors = array();
	public $page = null;
	public $layout_asset = '';
	//Content list attribute
	public $content_list;
	public $display_type;
	public $max = '' ;
	public $title = '' ;
	public $title_link = '' ;
	public $imageSize = 'list_view_';
	public $cssClass = '';
	public $allowSearch = false;
	/**
	 * @var array $luceneIndex индексы Lucene, в которых нужно искать
	 */
	public $luceneIndex = false;

	//Display types for the list view render 

	const DISPLAY_TYPE_HOMEPAGE = 0;
	const DISPLAY_TYPE_CATEGORY = 1;
	const DISPLAY_TYPE_ASIDE = 2;

	public function setParams($params)
	{
		$this->content_list = isset($params['content_list']) ? $params['content_list'] : null;
		$this->display_type = isset($params['display_type']) ? $params['display_type'] : self::DISPLAY_TYPE_HOMEPAGE;
		$this->max = isset($params['max']) ? $params['max'] : $this->max;
		$this->title = isset($params['title']) ? $params['title'] : $this->title;
		$this->title_link = isset($params['title_link']) ? $params['title_link'] : $this->title_link;
		$this->imageSize = isset($params['imageSize']) ? $params['imageSize'] : $this->imageSize;
		$this->allowSearch = isset($params['allowSearch']) ? $params['allowSearch'] : $this->allowSearch;
		$this->luceneIndex = isset($params['luceneIndex']) ? $params['luceneIndex'] : $this->luceneIndex;
		$this->cssClass = isset($params['cssClass']) ? $params['cssClass'] : false;
	}

	public function run()
	{
		if (isset($this->block) && ($this->block != null))
		{
			//Set Params from Block Params
			$params = unserialize($this->block->params);
			$this->setParams($params);

			if($this->allowSearch)
				Yii::import('cms.models.search.*');

			$this->renderContent();
		}
	}

	protected function renderContent()
	{
		$this->render(BlockRenderWidget::setRenderOutput($this), array());
	}

	public function validate()
	{
		return true;
	}

	public function params()
	{
		return array(
			'content_list' => t('Content list'),
			'display_type' => t('Display type'),
			'max' => t('Max records'),
			'title' => t('Title'),
			'title_link' => t('Title Link'),
			'imageSize' => t('Image size'),
			'cssClass' => t('Css Class'),
			'allowSearch' => t('Allow searching'),
			'luceneIndex' => t('Use the following indexes for searching'),
		);
	}

	public function beforeBlockSave()
	{
		return true;
	}

	public function afterBlockSave()
	{
		return true;
	}

	public static function getDisplayTypes()
	{
		return array(
			self::DISPLAY_TYPE_HOMEPAGE => t("Display in Homepage"),
			self::DISPLAY_TYPE_CATEGORY => t("Display in Category page"),
			self::DISPLAY_TYPE_ASIDE => t("Display as Aside"),
		);
	}

	public function getContentList($content_list_id, $max = null, $pagination = null, $return_type = ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD)
	{
		$params = array();
		if($this->allowSearch)
		{
			$params['search'] = array(
				'query' => isset($_GET['q']) ? $_GET['q'] : false,
				'date' => isset($_GET['date']) ? $_GET['date'] : false,
				'tag' => isset($_GET['tag']) ? $_GET['tag'] : false,
				'term' => isset($_GET['term']) ? $_GET['term'] : false,
				'authorId' => isset($_GET['author_id']) ? $_GET['author_id'] : false,
				'luceneIndex' => $this->luceneIndex,
				);
		}
		return GxcHelpers::getContentList($content_list_id, $max, $pagination, $return_type, null, $params);
	}
}

?>
