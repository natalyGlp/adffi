<?php $gallery=Galleries::model()->findByPk($this->gal_id);?>
<aside class="galleriview">
<?php
	if(Yii::app()->controller->region==2){
		$dataProvider = new CActiveDataProvider('GalleryPhotos', array(
			'criteria' => array(
				'condition'=>'gallery_id=:id',
				'params'=>array(':id'=>$this->gal_id),
				'limit' => 9,
				'order' => new CDbExpression('RAND()')
			)
		));
	}else{
		$dataProvider = new CActiveDataProvider('GalleryPhotos', array(
			'criteria' => array('condition'=>'gallery_id=:id','params'=>array(':id'=>$this->gal_id)),
			'pagination' => array('pageSize'=>150),
		));
	}

	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'common.front_blocks.galleryview._photo',
		'itemsTagName'=>'div',
		'template'=>'{items}{pager}',
		'tagName'=>'gallery',
		'template'=>'{items}',
		'sorterHeader'=>'<div class="cl"></div>'
	));
?>
</aside>