<?php

/**
 * Class for render gal_id Content Block
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.gal_id
 */

class GalleryviewBlock extends CWidget
{
    
    //Do not delete these attr block, page and errors
    public $id='galleryview';
    public $block=null;     
    public $errors=array();
    public $page=null;
    public $layout_asset='';    
    
    //HTML attribute
    public $gal_id;    
    public $count='0';    
    
    
    
    public function setParams($params){
        $this->gal_id=isset($params['gal_id']) ? $params['gal_id'] : '';
	    $this->count=isset($params['count']) ? $params['count'] : '0';
    }
    
    public function run()    {
        $url = Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.frontend'), false);
        Yii::app()->clientScript->registerCssFile($url."/fancybox/jquery.fancybox-1.3.4.css");
        Yii::app()->clientScript->registerScriptFile($url."/fancybox/jquery.fancybox-1.3.4.pack.js");
        Yii::app()->clientScript->registerScriptFile($url."/fancybox/gallery.js");
        Yii::app()->clientScript->registerCssFile($url."/gallery.css");
        $this->renderContent();
    }       
 
 
    protected function renderContent(){
    	if(isset($this->block) && ($this->block!=null)){	    
            //Set Params from Block Params
            $params=unserialize($this->block->params);
    	    $this->setParams($params);      

            $segments = explode('/', Yii::app()->request->url);
            $slug = isset($segments[3]) && preg_match('|(.html)$|i', $segments[3]) ? $segments[3] : (isset($segments[4]) ? $segments[4] : 'home');
            $slug = $slug == 'home' ? 'home' : substr($slug,0, strpos($slug,'.html'));       

            $this->gal_id = (int)Yii::app()->{CONNECTION_NAME}
                                             ->createCommand()
                                               ->select('id')
                                               ->from('{{galleries}}')
                                               ->where('path=?', array($slug))
                                               ->queryScalar();

            $this->render(BlockRenderWidget::setRenderOutput($this),array());
    	} else {
    	    echo '';
    	}       
    }
    
    public function validate(){
        $errors=false;
	/*if($this->gal_id==""){
		$this->errors['gal_id']=t('gal_id content is required'); 
        $errors=true;
    }*/
    if(is_numeric($this->count)){
        $this->errors['count']=t('This field nust br numeric.'); 
        $errors=true;
    }
    if ($errors) {
        return false ; 
    }else
		return true ;
    }
    
    public function params(){
        return array(
               'gal_id' => t('Gallery name'),                   
                'count' => t('Count items'),                   
        );
    }
    
    public function beforeBlockSave(){
	return true;
    }
    
    public function afterBlockSave(){
	return true;
    }
}