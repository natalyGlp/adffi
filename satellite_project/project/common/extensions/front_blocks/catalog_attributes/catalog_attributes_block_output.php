 <aside class="comparison"> 
	<?php if (isset($post->catalogattributes->data)): ?>   
		<!-- <button type="button" class="comparison-btn"><?php echo t('Сравнить отмеченные комплектации');?></button> -->
		<table border="0" cellpadding="0" cellspacing="0">
		<?php $options = $post->catalogattributes->options->options;?>
		<?php foreach ($post->catalogattributes->data as $key => $value): ?>
			<?php if(!isset($options[$key])) continue;?>
			<tr>
				<td><?php echo isset($options[$key]['name'])?$options[$key]['name']:'?';?></td>
				<td>
				<?php
					switch($options[$key]['type']){
					    case 3:{
					    	foreach ($options[$key]['list_data'] as $k => $v) {
					    		if(!in_array($k, $value)){
					    			continue;
					    		}

					    		echo $v.'<br/>';
					    	}
					    	break;
					    }
					    case 2:{
					    	echo isset($options[$key]['list_data'][$value]) ? $options[$key]['list_data'][$value] : '?';
					    	break;
					    }
					    default:{
					    	echo $value;
					    	break;
					    }
					}
				?>
				</td>
			</tr>
		<?php endforeach ?>
		</table>
	<?php endif ?>  
</aside>