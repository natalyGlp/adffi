<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'url'),''); ?>
	<?php echo CHtml::textField("Block[url]",$block_model->url ,array('id'=>'Block-url',)); ?>
	<?php echo $form->error($model,'url'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'width'),''); ?>
	<?php echo CHtml::textField("Block[width]", $block_model->width ,array('id'=>'Block-width', )); ?>
	<?php echo $form->error($model,'width'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'height'),''); ?>
	<?php echo CHtml::textField("Block[height]", $block_model->height ,array('id'=>'Block-height',)); ?>
	<?php echo $form->error($model,'height'); ?>
</div>        
<div class="row">
      <?php echo CHtml::label(Block::getLabel($block_model, "class"), ""); ?>
      <?php echo CHtml::textField("Block[class]", $block_model->class, array("id" => "Block-class"));?>
      <?php echo $form->error($model, "class"); ?>
</div>