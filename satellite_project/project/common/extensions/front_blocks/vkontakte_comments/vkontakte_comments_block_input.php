<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'api_id'),''); ?>
    <?php echo CHtml::textField("Block[api_id]", $block_model->api_id ,array('id'=>'Block-api_id', )); ?>
    <?php echo $form->error($model,'api_id'); ?>
</div>
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model,'width'),''); ?>
    <?php echo CHtml::textField("Block[width]", $block_model->width ,array('id'=>'Block-width', )); ?>
    <?php echo $form->error($model,'width'); ?>
</div>
<div class="row">
	<?php echo CHtml::label(Block::getLabel($block_model,'perpage'),''); ?>
	<?php echo CHtml::textField("Block[perpage]", $block_model->perpage ,array('id'=>'Block-perpage', )); ?>
	<?php echo $form->error($model,'perpage'); ?>
</div>
    
<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model, "class"), ""); ?>
    <?php echo CHtml::textField("Block[class]", $block_model->class, array("id" => "Block-class"));?>
    <?php echo $form->error($model, "class"); ?>
</div>