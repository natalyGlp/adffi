<?php
Yii::setPathOfAlias('common', COMMON_FOLDER);
Yii::setPathOfAlias('core', CORE_FOLDER);
Yii::setPathOfAlias('cms', CMS_FOLDER);
Yii::setPathOfAlias('frontend', FRONT_END);
Yii::setPathOfAlias('backend', BACK_END);
Yii::setPathOfAlias('cmswidgets', CMS_WIDGETS);
Yii::getPathOfAlias('uploads') || Yii::setPathOfAlias('uploads', CMS_PATH.'/../uploads');

require_once CORE_FOLDER.'/vendor/autoload.php';

return array(
    'name'=> "Nespi CMS",
    'timeZone' => APP_TIMEZONE,
    'sourceLanguage'=>'en_us',
    'language'=>'ru',
    'charset'=>'utf-8',

    // preloading 'log' component
    'preload'=>array('log', 'translate'),

    // autoloading model and component classes
        // autoloading from the CMS and Common Folder
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'cms.components.*',
        'cms.extensions.*',
        'cms.models.*',
        'cms.modules.*',

        'cms.components.storages.*',
        'cms.components.user.*',
        'cms.models.user.*',
        'cms.models.settings.*',
        'cms.models.object.*',
        'cms.models.banners.*',
        'cms.models.resource.*',
        'cms.models.page.*',
        'cms.models.galleries.*',
        'cms.models.mail.*',
        'cms.models.catalog.*',
         // Import widgets  Classes
        'cms.widgets.*',
        //Import Common Classes
        'common.components.*',
        'common.extensions.*',

        //Translate Module
        'cms.modules.translate.TranslateModule',
        'cms.modules.translate.controllers.*',
        'cms.modules.translate.models.*',

        //Yii Mail Extensions
        'cms.extensions.yii-mail.*',

        //Import Rights Modules
        'cms.modules.rights.*',
        'cms.modules.rights.models.*',
        'cms.modules.rights.components.*',
        'cms.modules.rights.RightsModule',
    ),
    'modules'=>array(
        // uncomment the following to enable the Gii tool
        /*
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'admin',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1', '::1'),
        ),*/

    'gii'=>array(
        'class'=>'system.gii.GiiModule',
        'password'=>'12345',
        'generatorPaths' => array('cms.extensions.mpgii'),//this line does the trick
        'ipFilters'=>array('127.0.0.1'),
    ),
    //Import Translate Module

    'translate'=>array(
        'class'=>'cms.modules.translate.TranslateModule',
    ),
    //Modules Rights
    'rights'=>array(
            'class'=>'cms.modules.rights.RightsModule',
            'install'=>false,   // Enables the installer.
            'appLayout'=>'//layouts/column2',
            'superuserName'=>'Admin',
            'userNameColumn' => 'fullName',
        ),
    ),
    // application components
    'components'=>array(
        //Edit your Database Connection here
        //Use MySQL database
        CONNECTION_NAME => array_merge(array(
            'enableParamLogging'=>true,
            ), require_once(COMMON_FOLDER.'/../config/db.php')),
        //User Componenets
        'user'=>array(
            'class'=>'cms.components.user.GxcUser',
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
            //'loginUrl'=>FRONT_SITE_URL.'/sign-in',
            'stateKeyPrefix'=>'gxc_system_user',
        ),
        'mail' => array(
            'class' => 'cms.extensions.yii-mail.YiiMail',
            'transportType'=>'php',
            'viewPath' => '',
        ),
        'image' => array(
            'class'=> 'cms.extensions.image.CImageComponent',
            'driver'=>'GD'
        ),
        //Auth Manager
        'authManager'=>array(
            'class'=>'cms.modules.rights.components.RDbAuthManager',
            'connectionID' => CONNECTION_NAME,
            'defaultRoles'=>array('Guest', 'Authenticated')
        ),
        //Use Cache System by File
        'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),
        //Use the Settings Extension and Store value in Database
        'settings'=>array(
            'class'     => 'cms.extensions.settings.CmsSettings',
            'cacheId'   => 'global_website_settings',
            'cacheTime' => 84000,
        ),
        'langRouting'=>array(
            'class'=>'cms.components.LangRouting'
        ),
        //Use Session Handle in Database
        'session' => array(
            'class' => 'CDbHttpSession',
            'connectionID' => CONNECTION_NAME,
            'autoCreateSessionTable'=>true,
            'sessionTableName'=>'gxc_session',
        ),
        // замена стандартному ClientScript, которая умеет не отправлять скрипты при аякс запросах. пока что для теста работает только на бекенде
        'clientScript' => array(
            'class' => '\nlac\NLSClientScript',
            //'excludePattern' => '/\.tpl/i', //js regexp, files with matching paths won't be filtered is set to other than 'null'
            //'includePattern' => '/\.php/', //js regexp, only files with matching paths will be filtered if set to other than 'null'
            'mergeJs' => false, //def:true
            // 'compressMergedJs' => false, //def:false
            'mergeCss' => false, //def:true
            // 'compressMergedCss' => false, //def:false
            // 'mergeJsExcludePattern' => '/edit_area/', //won't merge js files with matching names
            // 'mergeIfXhr' => true, //def:false, if true->attempts to merge the js files even if the request was xhr (if all other merging conditions are satisfied)
            // 'serverBaseUrl' => 'http://localhost', //can be optionally set here
            // 'mergeAbove' => 1, //def:1, only "more than this value" files will be merged,
            // 'curlTimeOut' => 10, //def:10, see curl_setopt() doc
            // 'curlConnectionTimeOut' => 10, //def:10, see curl_setopt() doc
            // 'appVersion'=>1.0 //if set, it will be appended to the urls of the merged scripts/css
        ),
        //Error Action when having Errors
        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        //Log the Site Error, Warning and Store into File
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning, info'
                ),
                // uncomment the following to show log messages on web pages
                /*array(
                    'class'=>'CProfileLogRoute',
                    'categories'=>'system.db.*',
                    'enabled' => false,
                    'levels'=>'trace, info, profile'
                ),*/
            ),
        ),
        // Use Message in Database and Translate Components
        'messages'=>array(
            'class'=>'CDbMessageSource',
            'connectionID' => CONNECTION_NAME,
            'sourceMessageTable'=>'gxc_source_message',
            'translatedMessageTable'=>'gxc_translated_message',
            'onMissingTranslation' => array('TranslateModule', 'missingTranslation'),
        ),

        'translate'=>array(
            'class'=>'cms.modules.translate.components.MPTranslate',
            'autoSetLanguage' => true, // руками не трогать!!
        ),

        //Enable Cookie Validation and Csrf Validation
        'request'=>array(
            'class'=>'HttpRequest',
            'enableCookieValidation'=>true,
            'enableCsrfValidation'=> false,
            'noCsrfValidationRoutes'=>array('site/caching', 'ajax')
        ),
    ),
);
