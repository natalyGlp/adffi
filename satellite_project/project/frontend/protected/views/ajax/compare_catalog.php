<aside class="compare-box car-compare-box" id="obj_<?php echo $catalog->object_id; ?>">
    <div class="delete">убрать из сравнения</div>
    <p class="title"><?php echo $catalog->object_name; ?></p>
    <?php
        $thumb = GxcHelpers::getResourceObjectFromDatabase($catalog->model,'thumbnail', 'detali_view_');

        if (isset($thumb[0])):?>
        <div class="image-wrapper">
            <?php echo CHtml::image($thumb[0]['link'], $catalog->object_name, array('align'=>'center')); ?>
        </div>
    <?php endif; ?>
    <table border="0" cellpadding="0" cellspacing="0" class="option-list">
        <?php 
            $attr = isset($catalog->catalogattributes->data) ? $catalog->catalogattributes->data : array();

            $options = $attributes_options->options;
        ?>
        <?php foreach ($catalog->catalogattributes->data as $key => $value): ?>
            <?php 
                if(!isset($options[$key])){
                    continue;
                }

                $data = '';
                switch($options[$key]['type']){
                    case 3:{
                        foreach ($options[$key]['list_data'] as $k => $v) {
                            if(!in_array($k, $value)){
                                continue;
                            }

                            $data .= !empty($v) ? $v.'<br/>' : '';
                        }
                        break;
                    }
                    case 2:{
                        $data = isset($options[$key]['list_data'][$value]) ? $options[$key]['list_data'][$value] : '';
                        break;
                    }
                    default:{
                        $data = $value;
                        break;
                    }
                }

                $data = empty($data) ? '-' : $data;
                echo '<tr><td><a title="'.str_replace('<br/>', ' ', $data).'" alt="'.str_replace('<br/>', ' ', $data).'">'.$data.'</a></td></tr>';
            ?>
        <?php endforeach; ?>
    </table>
    <a href="<?php echo Object::getLink($catalog->object_id); ?>" class="more">подробнее об авто</a>
</aside>