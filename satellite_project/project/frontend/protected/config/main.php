<?php

return CMap::mergeArray(
	require(COMMON_FOLDER.'/config.php'),
	array(
		'id' => 'frontend',
		'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',                
		'components'=>array(
            // URL Format and Rewrite			
			'urlManager'=>array(
				'urlFormat'=>'path',
				'showScriptName' =>false,
				'rules'=>array(     
					array(
						'class'=>'cms.components.GxcUrlManager',
						'connectionID' => 'db',
						),  					                        
					'<controller:\w+>/<id:\d+>'=>'<controller>/view',
					'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
					'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
					'/app/logout'=>'app/logout',
					'/app/oauth'=>'app/oauth',
					'/<slug>/'=>'site/index',
					),
				),


			'log'=>array(
				'class'=>'CLogRouter',
				'routes'=>array(
					array(
						'class'=>'CWebLogRoute',
						'enabled' => false,
						'filter'=> array(
							'class'=>'CLogFilter',
							'prefixSession' => false,
							'prefixUser' => false,
							'logUser' => false,
						),
					),
				),
			),

		),
	)
);
