<?php 
class AjaxController extends CController{
    public $page;
    public function init(){
        //костыль!!!!!!!!
        $this->page = (object) array('layout' => 'auto_0001');
    }

	public function actionAjaxsliderright($i){
		if(Object::model()->count('t.object_type=?', array('publication')) <= $i){
			$i=0;
		}

		$this->renderPartial('slideright', array(
			'data' => Object::model()->find(array(
                'condition' => 't.object_type=?',
                'params' => array('publication'),
                'limit' => 1,
                'offset' => (int)$i,
                'order' => 't.object_date DESC'
            ))
		));

		Yii::app()->end();
	}

	public function actionGetclass($id){
		$data = Object::model()->findByPk($id);
		$this->renderPartial('class', array('data' => $data));
	}

	public function actionBrandList($first, $last){
		$arr = array();
		$first = (int) $first;
		$criteria = new CDbCriteria;
		$criteria->condition = 't.object_type=? AND t.object_status=?';
		$criteria->params = array('brand', 1);

		$arr['total'] = Object::model()->count($criteria);

		$criteria->limit = 32;
		$criteria->offset = $first <= 1 ? 0 : (ceil($first / 4)-1) * 32;

		$brands = Object::model()->findAll($criteria);

		foreach($brands as $i => $data){
    		$thumb = GxcHelpers::getResourceObjectFromDatabase($data, 'thumbnail');
			$arr['items'][$i]['url'] = Object::getLink($data->object_id);
			$arr['items'][$i]['img'] = isset($thumb[0]['link']) ? $thumb[0]['link'] : '';
		}

		echo CJSON::encode($arr);
		Yii::app()->end();
	}

    public function actionCompare_catalog($id) {
        Yii::import('common.front_layouts.auto_0001.content_type.catalog.CatalogObject');
        $catalog = CatalogObject::model()->findByPk($id);
        if(!$catalog){
            throw new Exception(404);
        }


        $this->renderPartial('compare_catalog', array(
            'catalog' => $catalog,
            'attributes_options' => CatalogAttributesOptions::model()->findByPk(1)
        ));
    }

	public function actionCompare() {
		$id_brand = array(
            isset($_GET['id_brand_0']) ? $_GET['id_brand_0'] : 0,
            isset($_GET['id_brand_1']) ? $_GET['id_brand_1'] : 0,
            isset($_GET['id_brand_2']) ? $_GET['id_brand_2'] : 0
        );

        $id_model = array(
            isset($_GET['id_model_0']) ? $_GET['id_model_0'] : 0,
            isset($_GET['id_model_1']) ? $_GET['id_model_1'] : 0,
            isset($_GET['id_model_2']) ? $_GET['id_model_2'] : 0
        );

        if(isset($_GET['get_list_data'])){
            $arr = explode('.', $_GET['get_list_data']);
 			$models = array();

            switch($arr[0]){
                case 'id_brand': {
            		$bid = $id_brand[(isset($arr[1]) ? $arr[1] : 0)];
                    $_mids = Yii::app()->{CONNECTION_NAME}
                                       ->createCommand()
                                       ->select('meta_object_id')
                                       ->from('{{object_meta}}')
                                       ->where('meta_value=? AND meta_key=?', array($bid, 'brand_id'))
                                       ->queryAll(false);

                    $ids = array();
                    foreach($_mids as $mid) {
                        $ids[$mid[0]] = $mid[0];
                    }

                    $crit = new CDbCriteria;
                    $crit->select = array('object_id', 'object_name');
                    $crit->condition = 'id_parent>0';
                    $crit->compare('object_type', 'model');
                    $crit->addInCondition('object_id', $ids);
                    $crit->order = 'object_name ASC';
                    $models = Object::model()->findAll($crit);

           			echo '<option value="">'.t('Выберите модель').'</option>';
                    break;
                }
                case 'id_model': {
            		$bid = $id_model[(isset($arr[1]) ? $arr[1] : 0)];
                    $_mids = Yii::app()->{CONNECTION_NAME}
                                       ->createCommand()
                                       ->select('id_object')
                                       ->from('{{filters}}')
                                       ->where('filter1=?', array($bid))
                                       ->queryAll(false);

                    $ids = array();
                    foreach($_mids as $mid) {
                        $ids[$mid[0]] = $mid[0];
                    }


                    $crit = new CDbCriteria;
                    $crit->select = array('object_id', 'object_name');
                    $crit->compare('object_type', 'catalog');
                    $crit->addInCondition('object_id', $ids);
                    $crit->order = 'object_name ASC';

                    $models = Object::model()->findAll($crit);
                        
                    echo '<option value="">'.t('Выберите комплектацию').'</option>';
                    break;
                }
            }

            foreach($models as $model){
                echo '<option value="'.$model->object_id.'">'.$model->object_name.'</option>';
            }
            Yii::app()->end();
        }
	}
}