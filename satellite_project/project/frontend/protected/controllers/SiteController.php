<?php

class SiteController extends FeController
{
    public $defaultAction='index';
 
    public function actions()
    {
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
        );
    }

    /**
     * List of allowd default Actions for the user
     * @return type 
     */
    public function allowedActions()
    {
        return 'captcha,'.parent::allowedActions();
    }

    /**
     * Index Page of the Site, re route here
     *
     *
     * @param integer $path путь к странице
     */
    public function actionIndex()
    {
        parent::renderPageSlug();
    }
}
