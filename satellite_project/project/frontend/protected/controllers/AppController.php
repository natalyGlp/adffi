<?php 

class AppController extends FeController
{	 
	public $defaultAction='login';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		// TODO: обработка аватарок юзеров
		// TODO: виджет-обертка для виджета авторизации Hoauth
		return array(
			'oauth' => array(
				'class'=>'cms.extensions.hoauth.HOAuthAction',
				'model' => 'User',
				'scenario' => 'oauth',
				'userIdentityClass' => 'UserIdentityDb', 
				'attributes' => array(
					'email' => 'email',
					'username' => 'email',
					'display_name' => 'displayName',
					'gender' => 'gender',
					'birthday_month' => 'birthMonth',
					'birthday_day' => 'birthDay',
					'birthday_year' => 'birthYear',
					'location' => 'country',
					'avatar' => 'photoURL',
					'bio' => 'description',

					//'confirmed' => 1,
					//'status' => ConstantDefine::USER_STATUS_ACTIVE,
				),
			),
		);
	}

	public function actionLogin() 
	{
		$model=new UserLoginForm;
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
				   
		// collect user input data
		if(isset($_POST['UserLoginForm']))
		{
					   
			$model->attributes=$_POST['UserLoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$result='login';
			} else {
				$result=$this->renderPartial('common.front_blocks.signinajax.signinajax_block_output',array('model'=>$model),true);
			}
				
			echo CJavaScript::jsonEncode($result) ;
		}
		Yii::app()->end();
	}

	public function actionLogout() 
	{
		Yii::app()->user->logout();
		$this->redirect(HOME);
	}
}
