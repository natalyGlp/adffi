<?php
header('Content-Type: text/html; charset=UTF-8', true);

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
ini_set("display_errors", YII_DEBUG);
if(YII_DEBUG) error_reporting(E_ALL);

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);


// change the following paths if necessary
$define=dirname(__FILE__).'/../common/define.php';
require_once($define);

$yii=CORE_FOLDER.'/yii/framework/yii.php';
$globals=COMMON_FOLDER.'/globals.php';
$config=BACK_END.'/config/main.php';
require_once($yii);
require_once($globals);

Yii::createWebApplication($config)->run();
