<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.

Yii::setPathOfAlias('common',COMMON_FOLDER);
Yii::setPathOfAlias('cms',CMS_FOLDER);
Yii::setPathOfAlias('frontend',FRONT_END);
Yii::setPathOfAlias('backend',BACK_END);
Yii::setPathOfAlias('cmswidgets', CMS_FOLDER.'/widgets');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'commandPath'=>CMS_FOLDER.'/commands',
	'name'=>'My Console Application',
	'preload'=>array('log'),

	'import'=>array(
        //Import Common Classes                    
        'common.components.*',      
        'cms.models.*',   
        'cms.components.*',
	),
	// application components
	'components'=>array(
		'db'=>require_once(COMMON_FOLDER.'/../config/db.php'),
        'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),
        //Log the Site Error, Warning and Store into File
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'logFile' => 'console.log',
				),
			),
		),
	),
);