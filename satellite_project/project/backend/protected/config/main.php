<?php
Yii::setPathOfAlias('bootstrap', CORE_FOLDER . '/vendor/clevertech/yii-booster/src');
return CMap::mergeArray(
    require (COMMON_FOLDER . '/config.php'),
    array(
        'id' => 'backend',
        'basePath' => dirname(__FILE__) . '/..',
        'name' => 'BACKEND ZONE',
        'defaultController' => 'besite',
        'preload' => array('bootstrap'),
        'controllerMap' => require (CMS_FOLDER . '/data/controllerMap.php'),
        'components' => array(
            'errorHandler' => array(
                'errorAction' => 'besite/error',
            ),
            'notify' => array(
                'class' => 'cms.extensions.yii-notify-qeue.components.NotifyComponent',
            ),
            'user' => array(
                'class' => 'cms.components.user.GxcUser',
                'allowAutoLogin' => true,
                'loginUrl' => array('besite/login'),
                'stateKeyPrefix' => 'gxc_system_user',
            ),
            'urlManager' => array(
                'urlFormat' => 'path',
                'showScriptName' => false,
                'rules' => array(
                    '<controller:\w+>/<id:\d+>' => '<controller>/index',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                ),
            ),
            'bootstrap' => array(
                'class' => 'bootstrap.components.Bootstrap',
                'responsiveCss' => true,
                // отключаем css и js на ajax запросах
                'ajaxCssLoad' => false,
                'ajaxJsLoad' => false,
            ),
        ),
        'modules' => array(
            'gii' => array(
                'class' => 'system.gii.GiiModule',
                'password' => '123456',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters' => array('127.0.0.1', '::1'),
                'generatorPaths' => array(
                    'cms.gii',
                    'bootstrap.gii',
                ),
            )
        )
    )
);
