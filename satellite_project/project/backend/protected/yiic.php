<?php
$config=dirname(__FILE__).'/config/console.php';
$define=dirname(__FILE__).'/../../common/define.php';


// костыль, что бы можно было запустить консольное приложение
$_SERVER['HTTP_HOST'] = 'mysite.com';
$_SERVER['REQUEST_URI'] = '/';

require_once($define);

$globals=COMMON_FOLDER.'/globals.php';
$yiic=CORE_FOLDER.'/yii/framework/yiic.php';
require_once($globals);
require_once($yiic);
