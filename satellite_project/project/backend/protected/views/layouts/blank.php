<!DOCTYPE html>
<html>
<head>	    
	<?php 
        $backend_asset=Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.backend'), false, -1, YII_DEBUG);
			
        $this->renderPartial('application.views.layouts.header',array('backend_asset'=>$backend_asset)); 
        
        ?>
</head>
<body>
<div class="container" id="page" style="text-align:center">	
            	<?php echo $content; ?>	
</div><!-- page -->
 
</body>
</html>