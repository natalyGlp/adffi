<?php
/** 
 * Шаблон, который используется для рендеринга iframe
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>		    
	<?php 
        $backend_asset=Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.backend'), false, -1, YII_DEBUG);
			
        $this->renderPartial('application.views.layouts.header',array('backend_asset'=>$backend_asset)); 
        
        ?>
</head>

<body style="padding:10px">
<div id="loading_layer" style="display:none"><img src="<?php echo $backend_asset ?>/img/ajax_loader.gif" alt="" /></div>
<div class="container" id="page">  
    <?php echo $content; ?>			
</div><!-- page -->
</body>
</html>