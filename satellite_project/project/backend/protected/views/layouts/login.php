<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="login_page ">
<head>	    
    <meta name="robots" content="noindex,nofollow">
	<?php 
        $backend_asset=Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.backend'), false, -1, YII_DEBUG);
			
        $this->renderPartial('application.views.layouts.header',array('backend_asset'=>$backend_asset));  
        
        ?>
</head>
<body>
    <?php echo $content; ?>		
</body>
</html>