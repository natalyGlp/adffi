<?php 
    $this->beginContent('//layouts/main'); 
    $site = isset($this->site) ? $this->site : ''; // rights модуль не имеет свойства site в контроллере
?>

<div id="contentwrapper">
    <div class="main_content">
        <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
            'links' => $this->breadcrumbs
        ));?><!-- breadcrumbs -->

        <?php if(Yii::app()->hasComponent('translate')): ?>
            <div class="pull-right">
                <?php
                echo Yii::app()->translate->editLink('Manage translations', 'link', array('class' => 'btn')) . ' ';
                echo Yii::app()->translate->missingLink('Missing translations', 'link', array('class' => 'btn'));
                ?>             
            </div>
        <?php endif; ?>

        <h2><?php
            echo (isset($this->titleImage) && ($this->titleImage != '')) ? '<img src="' . $this->backendAssets . '/' . $this->titleImage . '" />' : '';
            echo isset($this->pageTitle) ? $this->pageTitle : ''; 
        ?></h2>
        <?php if (isset($this->pageHint) && ($this->pageHint != '')) : ?>
            <p><?php echo $this->pageHint; ?></p>
        <?php endif; ?>

        <?php if (isset($this->menu) && count($this->menu) > 0) : ?>
                <div class="header-info">
                    <?php
                    $this->widget('bootstrap.widgets.TbButtonGroup', array(
                        'type' => 'pills',
                        'buttons'=>$this->menu,
                        'htmlOptions' => array('style'=>'margin: 20px 0;')
                    ));
                    ?>
                </div>
        <?php endif; ?>
        <?php $this->renderPartial('cmswidgets.views.notification'); ?>
        <?php echo $content; ?>
    </div>
</div>
        
<!-- sidebar -->
<a href="javascript:void(0)" class="sidebar_switch on_switch ttip_r" title="Hide Sidebar">Sidebar switch</a>
<div class="sidebar">
    <div class="antiScroll">
        <div class="antiscroll-inner">
            <div class="antiscroll-content">
                <div class="sidebar_inner">
                    <form id="search-box" method="get" action="#" target="_blank" class="input-append">
                        <input id="topSearchBox" autocomplete="off" type="text" maxlength="2048" name="q" size="16" placeholder="<?php echo t('Search...') ?>" aria-haspopup="true" class="search_query input-medium" />
                        <button type="submit" class="btn"><i class="icon-search" id="searchbutton"></i></button>
                    </form>
                    <div id="side_accordion" class="accordion">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Content');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse in" id="collapseOne">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => CMap::mergeArray(array(
                                            array('label' => t('Manage Content'), 'url' => array('/beobject/admin', 'site'=>$site)),
                                            array('label' => t('Create Content'), 'url' => array('/beobject/create', 'site'=>$site))
                                        ), GxcHelpers::getAvailableContentTypeLikeMenu('&site='.$site)),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne1" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Category');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne1">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Create Term'), 'url' => array('/beterm/create', 'site'=>$site)),
                                            array('label' => t('Manage Terms'), 'url' => array('/beterm/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beterm' && in_array(Yii::app()->controller->action->id, array('update', 'admin'))),
                                            array('label' => t('Create Taxonomy'), 'url' => array('/betaxonomy/create', 'site'=>$site)),
                                            array('label' => t('Mangage Taxonomy'), 'url' => array('/betaxonomy/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'betaxonomy' && in_array(Yii::app()->controller->action->id, array('update', 'admin')))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne2" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Pages');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne2">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Create Menu'), 'url' => array('/bemenu/create', 'site'=>$site)),
                                            array('label' => t('Manage Menus'), 'url' => array('/bemenu/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'bemenu' && in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index'))),
                                            array('label' => t('Create Content List'), 'url' => array('/becontentlist/create', 'site'=>$site), 'visible' => user()->isAdmin ? true : false,),
                                            array('label' => t('Manage Content Lists'), 'url' => array('/becontentlist/admin', 'site'=>$site),
                                                'active' => ((Yii::app()->controller->id == 'becontentlist') && (in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index'))) ? true : false)),
                                            array('label' => t('Create Block'), 'url' => array('/beblock/create', 'site'=>$site), 'visible' => user()->isAdmin ? true : false,),
                                            array('label' => t('Manage Blocks'), 'url' => array('/beblock/admin', 'site'=>$site),
                                                'active' => ((Yii::app()->controller->id == 'beblock') && (in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index'))) ? true : false)),
                                            array('label' => t('Create Page'), 'url' => array('/bepage/create', 'site'=>$site)),
                                            array('label' => t('Manage Pages'), 'url' => array('/bepage/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'bepage' && in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index')))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseRes" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Resources');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseRes">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Create Resource'), 'url' => array('/beresource/create', 'site'=>$site)),
                                            array('label' => t('Manage Resource'), 'url' => array('/beresource/admin', 'site'=>$site),
                                                'active' => ((Yii::app()->controller->id == 'resource') && (in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index')))) ? true : false)
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne3" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Gallegies');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne3">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Create Gallery'), 'url' => array('/begalleries/create', 'site'=>$site)),
                                            array('label' => t('Manage Galleries'), 'url' => array('/begalleries/admin', 'site'=>$site)),
                                            array('label' => t('Manage Albums'), 'url' => array('/bealbums/admin', 'site'=>$site)),
                                            array('label' => t('Create Albums'), 'url' => array('/bealbums/create', 'site'=>$site)),
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseManage" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Manage');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseManage">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Comments'), 'url' => array('/becomment/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'comments'),
                                            array('label' => t('Email log'), 'url' => array('beemail/index', 'site'=>$site)),
                                            array('label' => t('Lucene Search Indexes'), 'url' => array('besearch/index', 'site'=>$site), 'active' => Yii::app()->controller->id == 'besearch'),
                                            array('label' => t('Sitemap'), 'url' => array('besitemap/index', 'site'=>$site), 'active' => Yii::app()->controller->id == 'besitemap'),
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne4" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Mails');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne4">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Create Mail Template'), 'url' => array('/bemailtemplate/create', 'site'=>$site)),
                                            array('label' => t('Manage Mail templates'), 'url' => array('/bemailtemplate/admin', 'site'=>$site)),
                                            array('label' => t('Email log'), 'url' => array('beemail/index', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beemail'),
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>


                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne25" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Shop');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne25">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Settings'), 'url' => array('/beshop/settings', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beshop' && Yii::app()->controller->action->id == 'settings'),
                                            array('label' => t('Manage Orders'), 'url' => array('/beorder/index', 'site'=>$site)),
                                            array('label' => t('Create Brand'), 'url' => array('/beobject/create', 'type' => 'brand', 'site'=>$site)),
                                            array('label' => t('Manage Brand'), 'url' => array('/beobject/admin', 'type' => 'brand', 'site'=>$site)),
                                            array('label' => t('Create Model'), 'url' => array('/beobject/create', 'type' => 'model', 'site'=>$site)),
                                            array('label' => t('Manage Model'), 'url' => array('/beobject/admin', 'type' => 'model', 'site'=>$site)),
                                            array('label' => t('Create Shop'), 'url' => array('/beobject/create', 'type' => 'shop', 'site'=>$site)),
                                            array('label' => t('Manage Shop'), 'url' => array('/beobject/admin', 'type' => 'shop', 'site'=>$site)),
                                            array('label' => t('Create Catalog'), 'url' => array('/beobject/create', 'type' => 'catalog', 'site'=>$site)),
                                            array('label' => t('Manage Catalog'), 'url' => array('/beobject/admin', 'type' => 'catalog', 'site'=>$site)),
                                            array('label' => t('Create Catalog Attributes'), 'url' => array('/becatalogattributes/create', 'site'=>$site)),
                                            array('label' => t('Manage Catalog Attributes'), 'url' => array('/becatalogattributes/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'becatalogattributes' && in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index'))),
                                            array('label' => t('Create Filters'), 'url' => array('/befilters/create', 'site'=>$site)),
                                            array('label' => t('Manage Filters'), 'url' => array('/befilters/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'befilters' && in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index')))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne15" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Banners');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne15">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Create Banner'), 'url' => array('/bebanners/create', 'site'=>$site)),
                                            array('label' => t('Manage Banners'), 'url' => array('/bebanners/admin', 'site'=>$site))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne5" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Languages');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne5">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Create Language'), 'url' => array('/belanguage/create', 'site'=>$site)),
                                            array('label' => t('Manage Languages'), 'url' => array('/belanguage/admin', 'site'=>$site))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseUser" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Users');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseUser">
                                <div class="accordion-inner">
                                <?php
                                    $arr = array(
                                            array('label' => t('Create User'), 'url' => array('/beuser/create', 'site'=>$site)),
                                            array('label' => t('Manage Users'), 'url' => array('/beuser/admin', 'site'=>$site),
                                                'active' => ((Yii::app()->controller->id == 'beuser') && (in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index')))) ? true : false
                                            ),
                                            array('label' => t('Setup oauth'), 'url' => array('/beuser/oauth', 'site'=>$site), 'active' => in_array(Yii::app()->controller->action->id, array('oauth'))),
                                    );

                                    if (!$site) {
                                        $arr = array_merge($arr, array(
                                            array(
                                                    'label'=>Rights::t('core', 'Assignments'),
                                                    'url'=>array('/rights/assignment/view'),
                                                    'itemOptions'=>array('class'=>'item-assignments'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                            array(
                                                    'label'=>Rights::t('core', 'Permissions'),
                                                    'url'=>array('/rights/authItem/permissions'),
                                                    'itemOptions'=>array('class'=>'item-permissions'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                            array(
                                                    'label'=>Rights::t('core', 'Roles'),
                                                    'url'=>array('/rights/authItem/roles'),
                                                    'itemOptions'=>array('class'=>'item-roles'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                            array(
                                                    'label'=>Rights::t('core', 'Tasks'),
                                                    'url'=>array('/rights/authItem/tasks'),
                                                    'itemOptions'=>array('class'=>'item-tasks'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                            array(
                                                    'label'=>Rights::t('core', 'Operations'),
                                                    'url'=>array('/rights/authItem/operations'),
                                                    'itemOptions'=>array('class'=>'item-operations'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                        ));
                                    }

                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => $arr,
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne6" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Settings');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne6">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('General setup'), 'url' => array('/besettings/general', 'site'=>$site)),
                                            array('label' => t('System setup'), 'url' => array('/besettings/system', 'site'=>$site)),
                                            array('label' => t('Social setup'), 'url' => array('/besettings/social', 'site'=>$site))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne7" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo t('Admin');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne7">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => t('Caching'), 'url' => array('/becaching/clear', 'site'=>$site), 'linkOptions' => array('id' => 'menu_8', 'class' => 'menu_8'), 'itemOptions' => array('id' => 'menu_8'),
                                                'items' => array(
                                                ),
                                                'visible' => (user()->isAdmin||user()->isOwner) ? true : false,
                                            ),
                                            array('label' => t('Logs'), 'url' => array('/beadmin/log', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beadmin' && Yii::app()->controller->action->id == 'log'),
                                            array('label' => t('Backups'), 'url' => array('/beadmin/backup', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beadmin' && Yii::app()->controller->action->id == 'backup'),
                                            array('label' => t('Update'),
                                                'url' => array('/beupdate/index', 'site'=>$site),
                                                'active' => Yii::app()->controller->id == 'beupdate' && Yii::app()->controller->action->id == 'index',
                                                'visible' => !$site,
                                                )
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->renderPartial('application.views.layouts.calculator');?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.accordion-body').removeClass('in');
    $('.accordion-inner li.active').parent('ul')
                                   .parent('.accordion-inner')
                                   .parent('.accordion-body').addClass('in');
    $().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
});
</script>

<?php $this->endContent(); ?>
