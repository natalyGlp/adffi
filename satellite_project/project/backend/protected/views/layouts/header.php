<meta charset="utf-8" />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta name="language" content="ru" />
<meta name="robots" content="noindex,nofollow">
<?php
    $backend_asset = Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.backend'), false, -1, YII_DEBUG); 

    $cs=Yii::app()->clientScript;
    $cssCoreUrl = $cs->getCoreScriptUrl();
    $cs->registerCoreScript('jquery');
    $cs->registerCoreScript('jquery.ui');

    /**
     * TODO:
     * Если понадобится использовать button/tooltip от jquery-ui, этот код поможет решить конфликты с bootstrap
     */
    // $cs->registerScript('bootstrapConflictFix', '
    //     $.widget.bridge("uibutton", $.ui.button);
    //     $.widget.bridge("uitooltip", $.ui.tooltip);
    //     ', CClientScript::POS_HEAD);

    $cs->registerCssFile($cssCoreUrl . '/jui/css/base/jquery-ui.css');
    $cs->registerCssFile($backend_asset.'/css/main.css');
    $cs->registerCssFile(GxcHelpers::backendAsset('/css/prettyPhoto.css'));
    $cs->registerCssFile(GxcHelpers::backendAsset('/lib/qtip2/jquery.qtip.min.css'));
    $cs->registerCssFile(GxcHelpers::backendAsset('/img/splashy/splashy.css'));
    $cs->registerCssFile(GxcHelpers::backendAsset('/img/flags/flags.css'));
    $cs->registerCssFile(GxcHelpers::backendAsset('/lib/jquery-ui/css/Aristo/Aristo.css'));
    $cs->registerCssFile(GxcHelpers::backendAsset('/css/style.css'));

    // $cs->registerCssFile(GxcHelpers::backendAsset('http://fonts.googleapis.com/css?family=PT+Sans'));
    // $cs->registerCssFile(GxcHelpers::backendAsset('/lib/chosen/chosen.css'));
    // $cs->registerCssFile(GxcHelpers::backendAsset('/lib/google-code-prettify/prettify.css'));
    // $cs->registerCssFile(GxcHelpers::backendAsset('/lib/colorbox/colorbox.css'));
?>
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="<?php echo $backend_asset?>/css/ie.css" />
            <script src="<?php echo $backend_asset?>/js/ie/html5.js"></script>
            <script src="<?php echo $backend_asset?>/js/ie/respond.min.js"></script>
        <![endif]-->

        <script>
            document.documentElement.className += ' js';
            var assetsUrl = '<?php echo $backend_asset; ?>';
        </script>
        <link rel="stylesheet" href="<?php echo $backend_asset?>/css/blue.css" id="link_theme" />
<?php
    $cs->registerScriptFile($backend_asset.'/js/backend.js', CClientScript::POS_HEAD);

    $cs->registerScriptFile($backend_asset.'/js/functions.js');
    $cs->registerScriptFile($backend_asset.'/js/jquery.prettyPhoto.js', CClientScript::POS_END);

    $cs->registerScript('init', '
        $("a[rel^=\'prettyPhoto\']").prettyPhoto({show_title: true,social_tools: \'\',deeplinking: false});

        $(".my-tabs > ul > li > a").click(function (e) {
                e.preventDefault();
                $(this).tab("show");
        });

        $(".my-tabs a:first").click();
        ');

    $cs->registerScriptFile($backend_asset . '/js/jquery-migrate.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($backend_asset . '/js/jquery.debouncedresize.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($backend_asset . '/js/jquery.actual.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($backend_asset . '/js/jquery_cookie.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($backend_asset . '/js/bootstrap.plugins.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($backend_asset . '/lib/qtip2/jquery.qtip.min.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/js/ios-orientationchange-fix.js', CClientScript::POS_END);
    $cs->registerScriptFile($backend_asset . '/lib/antiscroll/antiscroll.js', CClientScript::POS_END);
    $cs->registerScriptFile($backend_asset . '/lib/antiscroll/jquery-mousewheel.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/lib/colorbox/jquery.colorbox.js', CClientScript::POS_END); // ak lightbox
    $cs->registerScriptFile($backend_asset . '/js/selectNav.js', CClientScript::POS_END); // сжимание навигации для моб девайсов
    $cs->registerScriptFile($backend_asset . '/js/gebo_common.js', CClientScript::POS_END); // инициализация темы
    $cs->registerScriptFile($backend_asset . '/lib/UItoTop/jquery.ui.totop.min.js', CClientScript::POS_END); // используется
    // $cs->registerScriptFile($backend_asset . '/lib/jquery-ui/external/globalize.js', CClientScript::POS_END); / i18N?
    // $cs->registerScriptFile($backend_asset . '/js/forms/jquery.ui.touch-punch.min.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/js/forms/jquery.inputmask.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($backend_asset . '/js/forms/jquery.autosize.min.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/js/forms/jquery.counter.min.js', CClientScript::POS_END); // This word and character counter plugin allows you to count characters or words, up or down
    // $cs->registerScriptFile($backend_asset . '/lib/tag_handler/jquery.taghandler.min.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/lib/uniform/jquery.uniform.min.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/js/forms/jquery.progressbar.anim.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/lib/chosen/chosen.jquery.min.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/lib/tiny_mce/jquery.tinymce.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/lib/plupload/js/plupload.full.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/lib/plupload/js/jquery.plupload.queue/jquery.plupload.queue.full.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/lib/colorpicker/bootstrap-colorpicker.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/lib/complexify/jquery.complexify.min.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/lib/toggle_buttons/jquery.toggle.buttons.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/js/jquery.imagesloaded.min.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/js/jquery.wookmark.js', CClientScript::POS_END);

    // $cs->registerScriptFile($backend_asset . '/js/gebo_forms.js', CClientScript::POS_END);
    // $cs->registerScriptFile($backend_asset . '/js/gebo_gallery.js', CClientScript::POS_END);

    $cs->registerScript(__FILE__.'#loader', <<<EOC
        (function () {
            var geboOnloadFunction = function() {jQuery("html").removeClass("js")};
            var geboLoaderTimer = setTimeout(geboOnloadFunction,1000);
            $(document).ready(function() {geboOnloadFunction(); clearTimeout(geboLoaderTimer)});

            //Hide the second level menu
            $('#left-sidebar ul li ul').hide();            
            //Show the second level menu if an item inside it active
            $('li.list_active').parent("ul").show();
        
            $('#left-sidebar').children('ul').children('li').children('a').click(function () {          
                if($(this).parent().children('ul').length>0){                  
                    $(this).parent().children('ul').toggle();    
                }
            });
        })();
EOC
);