function thematicform(id) {
	window.event.preventDefault();
	var url = id ? '/thematics/update/?id='+id : '/thematics/create';

	$.get(url, function(data) {
        $('#myModal').html('').append($(data));
    });

    $('#myModal').modal('show');
	setTimeout(function() {
		initModalForm('#thematics-form', url);
	}, 500);
}

function initModalForm(selector, url) {
	$(selector).submit(function(e){
	   	e.preventDefault();
	    $.post(url, $(this).serialize(), function(data) {
	    	if(data === 'success'){
                $('#myModal').modal('hide');
                window.location.reload();
	    		return false;
	    	}
	    	setModalData(data);
			initModalForm(selector, url);
	    });
	});
}

function setModalData(data) {
	$('#myModal').html('').append($(data));
}

function categoryform(id_thematic, id) {
	window.event.preventDefault();
	var url = id ? '/categories/update/?id='+id : '/categories/create/?id_thematic='+id_thematic;

    $.get(url, function(data) {
        $('#myModal').html('').append($(data));
    });

    $('#myModal').modal('show');

	setTimeout(function() {
		initModalForm('#categories-form', url);
	}, 500);
}

/**
 *Convert a string to a Slug String
 *
 **/

var ru2en = {
    ru_str : "РђР‘Р’Р“Р”Р•РЃР–Р—РР™РљР›РњРќРћРџР РЎРўРЈР¤РҐР¦Р§РЁР©РЄР«Р¬Р­Р®РЇР°Р±РІРіРґРµС‘Р¶Р·РёР№РєР»РјРЅРѕРїСЂСЃС‚СѓС„С…С†С‡С€С‰СЉС‹СЊСЌСЋСЏ ",
    en_str : ['A','B','V','G','D','E','JO','ZH','Z','I','J','K','L','M','N','O','P','R','S','T',
    'U','F','H','C','CH','SH','SHH','','I','','JE','JU',
    'JA','a','b','v','g','d','e','jo','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f',
    'h','c','ch','sh','shh','','i','','je','ju','ja','_'],

    toSlug : function(org_str) {
        var tmp_str = [];
        for(var i = 0, l = org_str.length; i < l; i++) {
            var s = org_str.charAt(i), n = this.ru_str.indexOf(s);
            if(n >= 0) {
                tmp_str[tmp_str.length] = this.en_str[n];
            }
            else {
                tmp_str[tmp_str.length] = s;
            }
        }
        var str = tmp_str.join("");
        
        str = str.replace(/[^a-z0-9\s\'\:\/\[\]_]/gi,'');
        str = str.replace(/[\s\'\:\/\[\]-]+/g,' ');
        str = str.replace(/[ ]/g,'-');        
        str = str.toLowerCase();        
        return str;
    }

};

function inArray(elem, array){
    var len = array.length;
    for(var i = 0 ; i < len;i++){
        if(array[i] == elem){return i;}
    }
    return -1;
} 

if (!Array.indexOf)
{
  Array.indexOf = [].indexOf ?
      function (arr, obj, from) {console.log('1111'); return arr.indexOf(obj, from); }:
      function (arr, obj, from) { // (for IE6)
        console.log('12233'); 
        var l = arr.length,
            i = from ? parseInt( (1*from) + (from<0 ? l:0), 10) : 0;
        i = i<0 ? 0 : i;
        for (; i<l; i++) {
          if (i in arr  &&  arr[i] === obj) { return i; }
        }
        return -1;
      };
}

function str2url(str,encoding,ucfirst)
{
    str = ru2en.toSlug(str);
    return str;
}

/**
 *
 * Get the id before @ in an Email Address
 **/
function getIdinEmail(str_email){    
    var ind=str_email.indexOf("@");
    var my_slice=str_email.slice(0,ind);
    return my_slice
}

/**
 * Copy String From a Text Field to Another Text Field
 */
function CopyString(id_from,id_to,type){
    if(type=='slug'){
        $(id_from).keyup(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val(str2url($(id_from).val().trim()));
        });
        $(id_from).change(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val(str2url($(id_from).val().trim()));
        });
    } 
    
    if(type==''){
        $(id_from).keyup(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val($(id_from).val().trim());
        });
        $(id_from).change(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val($(id_from).val().trim());
        });
    }
    
    if(type=='email'){
        $(id_from).keyup(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val(getIdinEmail($(id_from).val().trim()));
        });
        $(id_from).change(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val(getIdinEmail($(id_from).val().trim()));
        });
    }
}

function autoResize(object){
    var newheight;        
    newheight=object.contentWindow.document.body.scrollHeight+100;         
    object.height= (newheight) + "px";

}

/**
 *Convert a string to a Slug String
 *
 **/

var ru2en = {
    ru_str : "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя ",
    en_str : ['A','B','V','G','D','E','JO','ZH','Z','I','J','K','L','M','N','O','P','R','S','T',
    'U','F','H','C','CH','SH','SHH','','I','','JE','JU',
    'JA','a','b','v','g','d','e','jo','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f',
    'h','c','ch','sh','shh','','i','','je','ju','ja','_'],

    toSlug : function(org_str) {
        var tmp_str = [];
        for(var i = 0, l = org_str.length; i < l; i++) {
            var s = org_str.charAt(i), n = this.ru_str.indexOf(s);
            if(n >= 0) {
                tmp_str[tmp_str.length] = this.en_str[n];
            }
            else {
                tmp_str[tmp_str.length] = s;
            }
        }
        var str = tmp_str.join("");
        
        str = str.replace(/[^a-z0-9\s\'\:\/\[\]_]/gi,'');
        str = str.replace(/[\s\'\:\/\[\]-]+/g,' ');
        str = str.replace(/[ ]/g,'-');        
        str = str.toLowerCase();        
        return str;
    }

}

function str2url(str,encoding,ucfirst)
{
    str = ru2en.toSlug(str);
    return str;
}

/**
 *
 * Get the id before @ in an Email Address
 **/
function getIdinEmail(str_email){    
    var ind=str_email.indexOf("@");
    var my_slice=str_email.slice(0,ind);
    return my_slice
}

/**
 * Copy String From a Text Field to Another Text Field
 */
function CopyString(id_from,id_to,type){
    if(type=='slug'){
        $(id_from).keyup(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val(str2url($(id_from).val().trim()));
        });
        $(id_from).change(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val(str2url($(id_from).val().trim()));
        });
    } 
    
    if(type==''){
        $(id_from).keyup(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val($(id_from).val().trim());
        });
        $(id_from).change(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val($(id_from).val().trim());
        });
    }
    
    if(type=='email'){
        $(id_from).keyup(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val(getIdinEmail($(id_from).val().trim()));
        });
        $(id_from).change(function() {
            $(id_to).val($(id_from).val().trim());
            $(id_to).val(getIdinEmail($(id_from).val().trim()));
        });
    }
}

function autoResize(object){
    var newheight;        
    newheight=object.contentWindow.document.body.scrollHeight+100;         
    object.height= (newheight) + "px";

}
