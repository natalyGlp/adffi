<?php

/**
 * Class of parent Controller for Front end of GXC CMS, extends from RController
 *
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.components
 */

class FeController extends RController
{
    public $description;
    public $region = '0';
    public $breadcrumbs = array();
    public $addBodyClasses = array();
    protected $_curPageModel = false;
    protected $_layoutAssets;

    // Переменные для хранения типа контента и id модели контен-обьекта
    protected $_slug;
    protected $_oType;
    protected $_oSlug;

    protected $_object = false;

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        Yii::app()->setLanguage(Yii::app()->translate->getLanguage());
    }

    /**
     * Filter by using Modules Rights
     *
     * @return type
     */
    public function filters()
    {
        return array(
            'rights',
        );
    }

    /**
     * List of allowd default Actions for the user
     * @return type
     */
    public function allowedActions()
    {
        return 'index,error';
    }

    protected function renderPageSlug()
    {
        if ($this->isPage) {
            $this->setRobotsMeta();
            $this->renderPage();
        } else {
            $this->pageNotFound();
        }
    }

    protected function setRobotsMeta()
    {
        $content = array();
        if ($this->page->allow_index) {
            $content[] = 'index';
        } else {
            $content[] = 'noindex';
        }
        if ($this->page->allow_follow) {
            $content[] = 'follow';
        } else {
            $content[] = 'nofollow';
        }

        Yii::app()->clientScript->registerMetaTag(implode(', ', $content), 'robots', null, array(), 'robots');
    }

    protected function renderPage()
    {
        $page = $this->page;
        $this->pageTitle = $page->title;
        $this->description = $page->description;
        $this->layout = $page->display_type;
        $this->breadcrumbs[] = $page->breadcrumb_title ? $page->breadcrumb_title : $page->title;

        // З.Ы. не путать! $page->layout на самом деле это не лейаут а название темы!
        // TODO: когда мы сможем делать норм обновления бд - переименовать Page::layout в theme

        // устанавливаем алиас для текущей темы
        Yii::setPathOfAlias('theme', Yii::getPathOfAlias('common.front_layouts.' . $page->layout));
        Yii::setPathOfAlias('currentTheme', Yii::getPathOfAlias('theme'));// DEPRECATED

        //depend on the layout of the page, use the corresponding file to render
        $this->render('theme.' . $page->display_type, array('page' => $page, 'layout_asset' => $this->layoutAssets));
    }

    public function getIsPage()
    {
        return !!$this->page;
    }

    public function getPage()
    {
        if ($this->_curPageModel === false) {
            $page = Page::model()->with('language')->findByAttributes(array(
                'slug' => $this->slug,
            ));

            if ($page) {
                $currentLanguage = Yii::app()->translate->getLanguage();

                if ($page->language->code != $currentLanguage) {
                    $page = $this->findPageTranslation($page, $currentLanguage);
                }
            }
            $this->_curPageModel = $page;
        }

        return $this->_curPageModel;
    }

    protected function findPageTranslation($page, $currentLanguage)
    {
        $translatedContent = Page::model()->with('language')->findByAttributes(array(
            'guid' => $page->guid
        ), array(
            'condition' => 'language.lang_short = :lang',
            'params' => array(
                ':lang' => $currentLanguage,
            ),
        ));

        if ($translatedContent) {
            $curUrl = Yii::app()->request->url;
            $translatedPageUrl = preg_replace('#^/' . $page->slug . '#', '/' . $translatedContent->slug, $curUrl);
            if ($curUrl != $translatedPageUrl) {
                $this->redirect($translatedPageUrl);
                Yii::app()->end();
            } else {
                return $translatedContent;
            }
        }

        return $page;
    }

    protected function beforeAction($action)
    {
        if ($action->id == 'index') {
            $this->initSlugs();
        }

        return parent::beforeAction($action);
    }

    protected function initSlugs()
    {
        $path = parse_url(Yii::app()->request->url);
        $path = $path['path'];
        $path = str_replace(Yii::app()->baseUrl, '', $path);

        $slug = false;
        $parts = explode('/', trim($path, '/'));
        // objectSlug всегда последний в урл и заканчивается на .html
        $objectSlug = preg_match('/(\.html)$/', end($parts)) ? str_replace('.html', '', end($parts)) : null;
        if ($objectSlug !== null) {
            array_pop($parts);// Был найден objectSlug - удалим его из частей url
        }

        // slug страницы или тип контента (XXXObject) - предпоследний (последний из-за array_pop)
        $this->_oType = $slug = end($parts) ? end($parts) : null;
        array_pop($parts);

        if (count($parts)) {
            if ($objectSlug) {
                // имеем дело с url типа: /photos/catalog/37_at_4wd.html (pageSlug/contentType/objectSlug)
                $slug = end($parts);
            } else {
                // e.g. /blabla/blabla -> без части с .html такие url не используются
                $this->pageNotFound();
            }
        }

        if ($objectSlug && !$slug) {
            // e.g. /blablabla.html
            $this->pageNotFound();
        }

        if ($slug == 'home' || Yii::app()->request->requestUri == '/index.php') {
            $this->redirect('/', true, 301);
        }

        $this->_slug = $slug;
        $this->_oSlug = $objectSlug;
    }

    protected function afterRender($view, &$output)
    {
        // в xml документах нам явно не нужны мета теги
        if (!header_sent('Content-Type: text/xml') && !header_sent('Content-Type: application/rss+xml')) {
            Yii::app()->clientScript->registerMetaTag($this->description, 'description');
        }

        $event = new CEvent($this);
        $event->params = array(
            'view' => $view,
            'output' => &$output,
        );
        $this->onAfterRender($event);
    }

    public function onAfterRender($event)
    {
        $this->raiseEvent('onAfterRender', $event);
    }

    public function getBodyCssClass()
    {
        return $this->layout . ' ' . $this->page->slug . 'Slug ' . implode(' ', $this->addBodyClasses);
    }

    public function getLayoutAssets()
    {
        // DEPRECATED
        return $this->themeAssets;
    }

    public function getThemeAssets()
    {
        if (!$this->_layoutAssets && $this->isPage) {
            $path = Yii::getPathOfAlias('common.front_layouts.' . $this->page->layout . '.assets');
            $this->_layoutAssets = Yii::app()->assetManager->publish($path, false, -1, YII_DEBUG);
        }
        return $this->_layoutAssets;
    }

    public function beginContent($view = null, $data = array())
    {
        $data = CMap::mergeArray(array('page' => $this->page, 'layout_asset' => $this->layoutAssets), $data);
        parent::beginContent($view, $data);
    }

    public function getSlug()
    {
        return isset($this->_slug) ? $this->_slug : Yii::app()->settings->get('general', 'homepage');
    }

    public function getObjectSlug()
    {
        return isset($this->_oSlug) ? $this->_oSlug : null;
    }

    /**
     * @return Возвращает тип обьекта из url или null
     */
    public function getObjectType()
    {
        return isset($this->_oType) ? $this->_oType : null;
    }

    /**
     * Возвращает (и сохраняет в переменную, что бы не делать много вызовов)
     * привязанный к текущему url обьект по его objectSlug
     * @return Object $object
     */
    public function getObject()
    {
        if ($this->_oSlug && $this->slug != 'home' && $this->_object === false) {
            $modelName = Object::importByType($this->slug);
            $this->_object = $modelName::model()->with('language')->findByAttributes(array(
                'object_slug' => $this->_oSlug
            ));
        } else {
            $this->_object = null;
        }

        return $this->_object;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                Yii::app()->controller->pageTitle = $error['code'] . ' — ' . $error['message'];

                $page = Page::model()->findByAttributes(array(
                    'slug' => 'error',
                    ));

                if ($page) {
                    $this->renderPage($page);
                } else {
                    $this->render('cms.views.error.404');
                }
            }
        }
    }

    /**
     * Возвращает ошибку 404
     */
    public function pageNotFound()
    {
        throw new CHttpException('404', _t('Oops! Page not found!'));
    }
}
