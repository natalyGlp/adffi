<?php
/**
 * Класс для доступа к API сателлитки
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.components
 */

class SatelliteOrderApi extends CApplicationComponent
{
    protected $superShopApiUrl;

    /**
     * Информация о последнем оплаченном заказе
     */
    protected static $_lastState;

    protected $_cache = array();

    /**
     * Константа для хранения ключа state в котором будет хранится только id заказа, который совершил юзер
     */
    const STATE_OID = 'satelliteOrderIdKey';
    const STATE_FORM_DATA = 'satelliteOrderFormData';

    public function __construct()
    {
        $this->superShopApiUrl = Yii::app()->settings->get('satellite', 'apiUrl');
    }

    /**
     * @return array все данные, которые положены текущему сайту от API магазина
     */
    public function getStoreData($params = array())
    {
        $productsListId = isset($params['productsListId']) ? $params['productsListId'] : '_default';

        if (!isset($this->_cache[$productsListId])) {
            if ($productsListId == '_default') {
                // привязка товаров через форму с сайтами при добавлении товара в сателлитке
                $apiRequest = array('getProducts' => '1');
                if (isset($params['featured']) && $params['featured']) {
                    $apiRequest['featured'] = '1';
                }
            } else {
                // DEPRECATED
                $productsList = Yii::app()->settings->get('satellite', $productsListId);
                $apiRequest = array(
                    'id' => is_array($productsList) ? CJSON::encode($productsList) : $productsList,
                );
            }

            $json = $this->get($apiRequest);
            $data = CJSON::decode($json);

            if ($data) {
                $this->mergeWithDefaultPrice($data['products']);
            }

            $this->_cache[$productsListId] = $data;
        }

        return $this->_cache[$productsListId];
    }

    /**
     * @return array Возвращает массив с продуктами, прикрепленными к текущему сателлитку
     */
    public function getProducts($params = array())
    {
        $data = $this->getStoreData($params);

        return $data ? $data['products'] : array();
    }

    /**
     * Копирует поля с дефолтной ценой на продукт
     * из $product['prices'] в $product
     * @param  array $products массив с информацией о продоваемых продуктах
     * @return array $products
     */
    protected function mergeWithDefaultPrice(&$products)
    {
        foreach ($products as &$product) {
            $defaultPrice = reset($product['prices']);
            $product = CMap::mergeArray($product, $defaultPrice);
        }
    }

    /**
     * Оформляет заказ
     * @param  integer $productId id продукта или списка(ов) продуктов
     * @param  SatelliteOrderFormModel $model модель заказа
     * @return boolean        true, если все прошло успешно
     */
    public function issueOrder($model)
    {
        $orderData = $this->normalizeOrderData($model);

        if (!$orderData) {
            return false;
        }

        $output = $this->post(array('orderData' => $orderData), array(
            CURLOPT_REFERER => Yii::app()->request->hostInfo,
        ));

        $data = CJSON::decode($output);

        $status = false;
        if (isset($data['orderStatus']) && $data['orderStatus'] == "success") {
            $status = true;

            $orderId = $data['orderId'];
            $orderData = CMap::mergeArray($orderData, $data);

            Yii::app()->user->setState(self::STATE_OID, $orderId);
            Yii::app()->user->setState(self::STATE_FORM_DATA, CJSON::encode($orderData));
        } else {
            $this->log("Ошибка обработки заказа:\n" . $output, 'error');
        }

        return $status;
    }

    /**
     * Приводит информацию из формы в формат, понятный для API сателлитки
     * @param  CModel $model
     * @return array
     */
    protected function normalizeOrderData(SatelliteOrderFormBaseModel $model)
    {
        $model->normalizeProducts();

        $orderData = CMap::mergeArray(array(
            'ip' => Yii::app()->request->getUserHostAddress(),
        ), $model->attributes);

        unset($orderData['amount']);
        unset($orderData['id']);

        if (count($orderData['products']) == 0) {
            $this->log("Ошибка обработки заказа: В заказе отсутствуют товары", 'error');
            return false;
        }

        return $orderData;
    }

    /**
     * @return array|boolean возвращает массив с информацией о только что оформленном заказе или false
     */
    public static function getOrderState()
    {
        if (!self::$_lastState && self::$_lastState !== false) {
            self::$_lastState = CJSON::decode(Yii::app()->user->getState(self::STATE_FORM_DATA));

            Yii::app()->user->setState(self::STATE_FORM_DATA, null);
            Yii::app()->user->setState(self::STATE_OID, null);
        }

        return self::$_lastState;
    }

    public function getApiUrl()
    {
        return $this->superShopApiUrl;
    }

    /**
     * Производит GET запрос к api
     *
     * @param  array $params параметры GET запроса
     */
    protected function get(array $params)
    {
        $curl = curl_init();

        $url = $this->superShopApiUrl . '?' . http_build_query($params);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_REFERER, Yii::app()->request->hostInfo);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if (!($ans = curl_exec($curl)) || ($statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE)) != 200) {
            $this->log('Не удалось совершить запрос к API сателлитки: ' . var_export(array($url, $ans), true));
            $ans = null;
        }

        curl_close($curl);

        return $ans;
    }

    /**
     * Производит POST запрос к api
     *
     * @param  array $postData параметры пост
     * @param  array $options дополнительные опции для cURL
     */
    protected function post(array $postData, $options = array())
    {
        $url = $this->superShopApiUrl;

        $ch = curl_init();

        $options = CMap::mergeArray(array(
            CURLOPT_POST => true,
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => http_build_query($postData),
            CURLOPT_RETURNTRANSFER => true,
        ), $options);
        curl_setopt_array($ch, $options);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    protected function log($message, $level = 'info')
    {
        Yii::log($message, $level, get_class($this));
    }
}
