<?php

/**
 * Class for handle Upload to Local Storage
 *
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.storages
 */

//This is a must Have Resource Class - Don't Delete this
class LocalStorage extends BaseStorage
{
    public function uploadFile($resource, $form)
    {
        if ($form->upload->size > $this->max_file_size) {
            $allow_size = $this->max_file_size / (1024 * 1024);
            $form->addError('upload', _t('File size is larger than allowed size: ') . $allow_size . ' mb');
            return false;
        }

        if ($form->upload->size < $this->min_file_size) {
            $form->addError('upload', _t('File is too small!'));
            return false;
        }

        $fileExtension = strtolower(CFileHelper::getExtension($form->upload->name));
        if (count($this->allow_types) > 0) {
            if (!in_array($fileExtension, $this->allow_types)) {
                $form->addError('upload', _t('File extension is not allowed!'));
                return false;
            }
        }

        $fileName = $this->generateFileName($form->upload->name);

        $fullPath = $this->getDestinationDir() . '/' . $fileName;
        if ($form->upload->saveAs($fullPath)) {
            $resource->resource_path = $this->getRelativeDestinationDir() . '/' . $fileName;

            return true;
        } else {
            $form->addError('upload', _t('Error while Uploading. Try again later.'));

            return false;
        }
    }

    protected function generateFileName($originalFileName, $forceRename = false)
    {
        $fileExtension = CFileHelper::getExtension($originalFileName);
        if ($forceRename) {
            $fileName = gen_uuid();
        } else {
            $fileName = pathinfo($originalFileName, PATHINFO_FILENAME);
            $fileName = GxcHelpers::toSlug($fileName);

            //Check if File exists, so Rename the Filename again;
            while (file_exists($this->getDestinationDir() . '/' . $fileName . '.' . $fileExtension)) {
                $fileName .= rand(10, 99);
            }
        }

        return $fileName . '.' . $fileExtension;
    }

    protected function getDestinationDir()
    {
        return $this->getResourcesDir() . '/' . $this->getRelativeDestinationDir();
    }

    protected function getRelativeDestinationDir()
    {
        $folder = date('Y') . '/' . date('m');
        $folderPath = $this->getResourcesDir() . '/' . $folder;
        if (!(file_exists($folderPath) && (is_dir($folderPath)))) {
            mkdir($folderPath, 0777, true);
        }

        return $folder;
    }

    public function uploadRemoteFile($link, $resource, $form, $ext)
    {
        if (count($this->allow_types) > 0) {
            if (!in_array(strtolower($ext), $this->allow_types)) {
                $form->addError('link', _t('File extension is not allowed!'));

                return false;
            }
        }

        $rawdata = $this->getRemoteFileContents($link);

        if (!$rawdata) {
            $form->addError('link', _t('Error while getting Remote File. Try again later.'));

            return false;
        }

        $fileName = $this->generateFileName(gen_uuid() . '.' . $ext, true);

        $fullPath = $this->getDestinationDir() . '/' . $fileName;
        $fp = fopen($fullPath, 'x');
        fwrite($fp, $rawdata);
        fclose($fp);

        $resource->resource_where = $form->where;
        $resource->resource_path = $this->getRelativeDestinationDir() . '/' . $fileName;

        return true;
    }

    public static function getFilePath($file)
    {
        return RESOURCE_URL . '/' . $file;
    }

    protected function getResourcesDir()
    {
        return Yii::getPathOfAlias('uploads.resources');
    }

    public function deleteResource($resource)
    {
        $path = $this->getResourcesDir() . '/' . $resource->resource_path;
        if (file_exists($path)) {
            unlink($path);
        }

        return;
    }
}
