<?php

/**
 * Class for handle Upload to Amazon S3 Storage
 *
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.storages
 */

class AmazonStorage extends BaseStorage
{
    public function uploadFile($resource, $form)
    {
        //Implement later
        /*
        $file=$form->upload->tempName;
        $resource->resource_name=$form->upload->name;
        $pvars   = array('remote'=>false,'my_file' => "@$file", 'key' => $es_key,
        'upload'=>'upload',
        'type'=>$form->type,
        'name'=>trim($form->name)!='' ? trim($form->name) : $form->upload->name,
        'body'=>trim($form->body),
        'ext'=>strtolower(CFileHelper::getExtension($form->upload->name)));
        $timeout = 30;
        $curl    = curl_init();
        $es_url = $form->type == 1 ? Yii::app()->getModule('mresource')->es_url_upload : Yii::app()->getModule('mresource')->video_es_url_upload;
        curl_setopt($curl, CURLOPT_URL, $es_url);
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $pvars);
        $res_upload = curl_exec($curl);
        curl_close ($curl);
        if($res_upload=='0'){
        $process=false;
        $message=_t('Error while Uploading. Try again later.');
        return false;
        } else{
        $resource->resource_path=$res_upload;
        $process=true;
        return true;
        }
         *
         */
        $form->addError('upload', _t('Amazon S3 Upload is currently not supported. Try again later!'));
        return false;
    }

    public function uploadRemoteFile($link, $resource, $form, $ext)
    {
        $form->addError('link', _t('Amazon S3 Upload is currently not supported. Try again later!'));
        return false;
    }

    public function getFilePath($file)
    {
        return '';
    }

    public function deleteResource($resource)
    {
        return;
    }
}
