<?php

/**
 * Class to Identity User by Database Information.
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.components.user
 */

class UserIdentityDb extends CUserIdentity{
  
	/**
	 *
	 * @var int Id of the User
	 */
	private $_id;
  
	/**
	 *
	 * @var CActiveRecord current User Model
	 */
	private $_model;



	public function __construct($username,$password=null)
	{
		// sets username and password values
		parent::__construct($username,$password);

	 	$username=strtolower($this->username);
		   
		$this->_model=User::model()->find('LOWER(email)=?',array($username));

		if ($this->_model === null)
		{
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}
		elseif($password === null)// open auth authentication
		{
			/**
			 * you can set here states for user logged in with oauth if you need
			 * you can also use hoauthAfterLogin()
			 * @link https://github.com/SleepWalker/hoauth/wiki/Callbacks
			 */
			$this->beforeAuthentication();
			$this->errorCode=self::ERROR_NONE;
		}
	}
		
		
	/**
	 * This function check the user Authentication 
	 * 
	 * @return int 
	 */
	public function authenticate()
	{
		if($this->errorCode === self::ERROR_UNKNOWN_IDENTITY)
		{
			$user = $this->model;
			if(!$user->validatePassword($this->password,$user->salt)){
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}
			elseif($user->confirmed==ConstantDefine::USER_STATUS_ACTIVE)
			{
					
				$this->beforeAuthentication();

				//Set the Error Code to None for Success
				$this->errorCode=self::ERROR_NONE;	
			} else {
				$this->errorCode=ConstantDefine::USER_ERROR_NOT_ACTIVE;
			}
		}

		return $this->errorCode;
	}

	/**
	 * Return the id of the user
	 * @return integer
	 */
	public function getId()
	{
		return $this->model->user_id;
	}

	/**
	 * Return the name of the current user
	 * @return string
	 */
	public function getName()
	{
		return $this->model->display_name;
	}
   
   
	/**
	 *
	 * Return the _model of the class
	 * @return CActiveRecord
	 */
	public function getModel()
	{
		return $this->_model;
	}

	public function beforeAuthentication()
	{
		$user = $this->model;         
					
		//If the site allow auto Login, create token to recheck for Cookies
		if(Yii::app()->user->allowAutoLogin)
		{
			// FIXME: эти строки не имеют право на жизнь в этом классе
			$autoLoginToken=sha1(uniqid(mt_rand(),true));
			$this->setState('autoLoginToken',$autoLoginToken);
			
			$connection=Yii::app()->{CONNECTION_NAME};
			
			//delete old keys
			$command=$connection->createCommand('DELETE FROM {{autologin_tokens}} WHERE user_id=:user_id');
			$command->bindValue(':user_id',$user->user_id,PDO::PARAM_STR);
			$command->execute();
			
			//set new
			$command=$connection->createCommand('INSERT INTO {{autologin_tokens}}(user_id,token) VALUES(:user_id,:token)');
			$command->bindValue(':user_id',$user->user_id,PDO::PARAM_STR);
			$command->bindValue(':token',$autoLoginToken,PDO::PARAM_STR);
			$command->execute();
		}
		
		//Start to set the recent_login time for this user
		$user->recent_login=time();
		$user->save();
	}
}
?>
