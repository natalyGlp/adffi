<?php

/**
 * Helpers Class for whole CMS
 *
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.components
 */
class GxcHelpers
{
    public static function getTruePath($path, $ext = 'php')
    {
        if (isset(Yii::app()->controller->page)) {
            $_path = Yii::getPathOfAlias('common.front_layouts.'.Yii::app()->controller->page->layout.'.'.$path);
            if (file_exists($_path) || file_exists($_path.'.'.$ext)) {
                return 'common.front_layouts.'.Yii::app()->controller->page->layout.'.'.$path;
            }
        }

        return 'cms.'.$path;
    }

    public static function import($alias, $forceInclude = false)
    {
        $alias = self::getTruePath($alias);
        return Yii::import($alias, $forceInclude);
    }

    /**
     * Умное подключение ассетов
     *     GxcHelpers::assetUrl('sliderkit/js/sliderkit.js');
     *     GxcHelpers::assetUrl('images/icons/icon.png', 'backend');
     *
     * @param  string $package lib|gebo|backend
     * @param  string $url     url относительно папки lib, gebo, backend
     * @return string          полный url для использования с CClientScript
     */
    public static function assetUrl($url, $package = 'lib')
    {
        // TODO: более высокоуровневый метод, который будет еще автоматически registerCss/registerScript в зависимости от расширения
        static $assetsUrls = array();
        $packageMap = array(
            'backend',
            'gebo',
            'lib',
        );

        if (!in_array($package, $packageMap)) {
            throw new CException("Unexisted package $package");
        }

        $url = ltrim($url, '/');
        $parts = explode('/', $url);
        $republish = true;

        $alias = 'cms.assets.'.$package;
        if ($package == 'lib') {
            $alias .= '.'.$parts[0];
            $url = str_replace($parts[0].'/', '', $url);
        }
        if ($package == 'backend') {
            $republish = YII_DEBUG;
        }

        if (!isset($assetsUrl[$alias])) {
            $assetsUrl[$alias] = Yii::app()->assetManager->publish(Yii::getPathOfAlias($alias), false, -1, $republish);
        }

        return $assetsUrl[$alias] . '/' . $url;
    }

    public static function registerAsset($url, $package = 'lib', $position = CClientScript::POS_END)
    {
        $url = self::assetUrl($url, $package);

        $ext = pathinfo($url, PATHINFO_EXTENSION);
        $cs = Yii::app()->clientScript;

        if ($ext == 'js') {
            $cs->registerScriptFile($url, $position);
        }

        if ($ext == 'css') {
            $cs->registerCssFile($url);
        }
    }

    public static function registerJs($url, $package = 'lib', $position = CClientScript::POS_END)
    {
        return self::registerAsset($url, $package, $position);
    }

    public static function registerCss($url, $package = 'lib', $position = CClientScript::POS_END)
    {
        return self::registerAsset($url, $package, $position);
    }

    public static function loadDetailModel($model_name, $id)
    {
        $model = call_user_func(array($model_name, 'model'))->findByPk((int) $id);
        if ($model === null) {
            throw new CHttpException(404, _t('The requested page does not exist.'));
        }
        return $model;
    }

    public static function deleteModel($model_name, $id)
    {
        if (Yii::app()->request->isPostRequest) {
// we only allow deletion via POST request
            GxcHelpers::loadDetailModel($model_name, $id)->delete();
// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                Yii::app()->request->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else {
            throw new CHttpException(400, _t('Invalid request. Please do not repeat this request again.'));
        }
    }

    public static function getAvailableLayouts($render_view = false)
    {
        $cache_id = $render_view ? 'gxchelpers-available-layouts' : 'gxchelpers-available-layouts-false';
        $layouts = Yii::app()->cache->get($cache_id);

        if ($layouts === false) {
            $layouts = array();
            $folders = get_subfolders_name(Yii::getPathOfAlias('common.front_layouts'));
            foreach ($folders as $folder) {
                $temp = parse_ini_file(Yii::getPathOfAlias('common.front_layouts.' . $folder . '') . '/info.ini');
                if ($render_view) {
                    $layouts[$temp['id']] = $temp['name'];
                } else {
                    $layouts[$temp['id']] = $temp;
                }
            }
            Yii::app()->cache->set($cache_id, $layouts, 7200);
        }

        return $layouts;
    }

    public static function getAvailableBlocks($render_view = false)
    {
        $cache_id = $render_view ? 'gxchelpers-available-blocks' : 'gxchelpers-available-blocks-false';
        $blocks = Yii::app()->cache->get($cache_id);

        if ($blocks === false) {
            $blocks = array();
            $rootpath = Yii::getPathOfAlias('cms.front_blocks');
            $dirs = scandir($rootpath);

            foreach ($dirs as $dir) {
                $infoIni = $rootpath.'/'.$dir.'/info.ini';
                if (!is_dir($rootpath.'/'.$dir) || !is_file($infoIni)) {
                    continue;
                }

                $temp = parse_ini_file($infoIni);
                if ($render_view) {
                    $blocks[$temp['id']] = $temp['name'];
                } else {
                    $blocks[$temp['id']] = $temp;
                }
            }

            if (isset(Yii::app()->controller->page->layout) && file_exists(Yii::getPathOfAlias('common.front_layouts.'.Yii::app()->controller->page->layout.'.front_blocks'))) {
                $rootpath = Yii::getPathOfAlias('common.front_layouts.'.Yii::app()->controller->page->layout.'.front_blocks');
                $dirs = scandir($rootpath);

                foreach ($dirs as $dir) {
                    $infoIni = $rootpath.'/'.$dir.'/info.ini';
                    if (!is_dir($rootpath.'/'.$dir) || !is_file($infoIni)) {
                        continue;
                    }

                    $temp = parse_ini_file($infoIni);
                    if ($render_view) {
                        $blocks[$temp['id']] = $temp['name'];
                    } else {
                        $blocks[$temp['id']] = $temp;
                    }
                }
            }

            if ($render_view) {
                asort($blocks);
            }

            Yii::app()->cache->set($cache_id, $blocks, 7200);
        }

        return $blocks;
    }

    public static function getStorages($get_class = false)
    {
        // TODO: спрятать в ResourceProvider
        $cache_id = $get_class ? 'gxchelpers-storages' : 'gxchelpers-storages-false';
        if (($types = Yii::app()->cache->get($cache_id)) === false) {
            $temp = parse_ini_file(Yii::getPathOfAlias('cms.components.storages') . '/info.ini');
            $types = array();
            foreach ($temp['storages'] as $key => $value) {
                if ($get_class) {
                    $types[$key] = trim($value);
                } else {
                    $types[$key] = trim(ucfirst($key));
                }
            }
            Yii::app()->cache->set($cache_id, $types, 7200);
        }

        return $types;
    }

    public static function getAvailableContentType($render_view = false)
    {
        $cache_id = $render_view ? 'gxchelpers-content-types' : 'gxchelpers-content-types-false';
        $types = Yii::app()->cache->get($cache_id);

        if ($types === false) {
            $types = array();

            $sourceDirs = array(
                Yii::getPathOfAlias('cms.content_type').'/*',
            );
            if (isset(Yii::app()->controller->page)) {
                $theme = Yii::app()->controller->page->layout;
                $sourceDirs[] = Yii::getPathOfAlias('common.front_layouts.'.$theme.'.content_type').'/*';
            }
            $folders = array_map('glob', $sourceDirs);
            $folders = array_reduce($folders, 'array_merge', array());

            foreach ($folders as $folder) {
                $infoPath = $folder . '/info.ini';
                if (!is_file($infoPath)) {
                    // ini для этого контент типа есть, но в ядре (переопределение вьюх и классов в теме)
                    // или
                    // такого контент типа просто не существует, лучше просто пропустим эту директорию и не будем генерировать ошибку
                    continue;
                }

                $contentTypeInfo = parse_ini_file($infoPath);
                if ($render_view) {
                    $types[$contentTypeInfo['id']] = $contentTypeInfo['name'];
                } else {
                    $types[$contentTypeInfo['id']] = $contentTypeInfo;
                }
            }

            Yii::app()->cache->set($cache_id, $types, 7200);
        }

        return $types;
    }

    public static function getAvailableContentTypeLikeMenu($add = '')
    {
        $links=array();
        $types=GxcHelpers::getAvailableContentType();
        if (!empty($types)) {
            foreach ($types as $key => $value) {
                $links[]=array(
                    'label'=>
                        CHtml::link(
                            'Manage '.$value['name'],
                            Yii::app()->createUrl('beobject/admin', array('type' => $value['id'])).$add,
                            array('class' => 'pull-left')
                        )
                        .CHtml::link(
                            '<i class="icon icon-plus"></i>',
                            Yii::app()->createUrl('beobject/create', array('type' => $value['id'])).$add,
                            array('class' => 'pull-right')
                        ),
                    'itemOptions' => array(
                        'class' => 'clearfix',
                        'style' => 'margin-top: 0',
                        ),
                    'url'=>null,
                    'active'=>(isset($_GET['type']) && $_GET['type']==$value['id'])
                    );
            }
        }
        return $links;
    }

    public static function generateAvatarThumb($upload_name, $folder, $filename)
    {
//Start to check if the File type is Image type, so we Generate Everythumb size for it
        if (in_array(strtolower(CFileHelper::getExtension($upload_name)), array('gif', 'jpg', 'png'))) {
//Start to create Thumbs for it
            $sizes = AvatarSize::getSizes();
            foreach ($sizes as $size) {
                $dest = Yii::getPathOfAlias('uploads.avatars') . '/' . $size['id'] . '/' . $folder;
                if (!file_exists($dest) && !is_dir($dest)) {
                    mkdir($dest, 0777, true);
                }

                $thumbs = new ImageResizer(
                    Yii::getPathOfAlias('uploads.avatars.root') . '/' . $folder . '/',
                    $filename,
                    $dest . '/',
                    $filename,
                    $size['width'],
                    $size['height'],
                    $size['ratio'],
                    90,
                    '#FFFFFF'
                );
                $thumbs->output();
            }
        }
    }

    public static function getUserAvatar($size, $avatar, $default)
    {
        if (strpos($avatar, 'http') !== false) {
            return $avatar;
        } elseif (($avatar != null) && ($avatar != '')) {
            return AVATAR_URL . '/' . $size . '/' . $avatar;
        } else {
            return $default;
        }
    }

    public static function renderTextBoxResourcePath($data)
    {
        return '<input type="text" class="pathResource" value="' . $data->getUrl() . '" />';
    }

    public static function renderLinkPreviewResource($data)
    {
        GxcHelpers::registerCss('prettyPhoto/css/prettyPhoto.css');
        GxcHelpers::registerJs('prettyPhoto/jquery.prettyPhoto.js');
        Yii::app()->clientScript->registerScript(__FILE__.'#pretty-photo', '
            $("a[rel^=\'prettyPhoto\']").prettyPhoto({show_title: true,social_tools: \'\',deeplinking: false});
        ');

        switch ($data->resource_type)
        {
            case 'image':
                $url = ObjectResourceProvider::getThumbUrl($data, '_cms_admin_thumb_');

                return '<a href="' . $data->getUrl() . '" rel="prettyPhoto" title="' . $data->resource_name . '">' . CHtml::image($url, $data->resource_name) . '</a>';
                break;
            case 'video':
                return '<a href="' . Yii::app()->controller->backend_asset . '/js/jwplayer/player.swf?width=470&amp;height=320&flashvars=file=' . $data->getUrl() . '" title="' . $data->resource_name . '" rel="prettyPhoto">' . _t('View') . '</a>';
                break;
            default:
                return '<a href="' . $data->getUrl() . '">' . _t('View') . '</a>';
                break;
        }
    }

    public static function getArrayResourceObjectBinding($ownid)
    {
        if (!isset($_POST['resource'][$ownid])) {
            return array();
        }

        $upload_files = array();
        $links = $_POST['resource'][$ownid]['link'];
        $resIds = $_POST['resource'][$ownid]['resid'];
        $types = $_POST['resource'][$ownid]['type'];
        if (count($links) > 0) {
            for ($i = 0; $i < count($links); $i++) {
                $upload_files[] = array(
                    'link' => $links[$i],
                    'resid' => $resIds[$i],
                    'type' => $types[$i],
                );
            }
        }

        return $upload_files;
    }

    public static function getResourceObjectFromDatabase($objectId, $resourceType, $thumbOptions = '')
    {
        if ($objectId instanceof Object) {
            $objectId = $objectId->primaryKey;
        }

        $provider = new ObjectResourceProvider($objectId, $resourceType);
        if (!empty($thumbOptions)) {
            $provider->setThumbOptions($thumbOptions);
        }

        return $provider->getAsArray();
    }

    public static function getResourcesSizes($sizeId = false)
    {
        $sizes = CJSON::decode(Yii::app()->settings->get('system', 'sizes'));
        $sizes = is_array($sizes) ? $sizes : array();
        // TODO: пусть в массиве $sizes вместо ключей будет 'version'
        $sizes[] = array( // размер для админки (/core/cms1.0/widgets/views/object/object_resource_form_widget.php)
            'version' => '_cms_admin_thumb_',
            'max_width' => '50',
            'max_height' => '50',
            'scale_up' => true,
            'fit' => 'fill',
            );

        if ($sizeId) {
            foreach ($sizes as $s) {
                if ($s['version'] != $sizeId) {
                    continue;
                }
                $sizes = $s;
                break;
            }
        }
        return $sizes;
    }

    /**
     * Encode the text into a string which all white spaces will be replaced by $rplChar
     * @param string $text  text to be encoded
     * @param Char $rplChar character to replace all the white spaces
     * @param boolean upWords   set True to uppercase the first character of each word, set False otherwise
     */
    public static function encode($text, $rplChar = '', $upWords = true)
    {
        $encodedText = null;
        if ($upWords) {
            $encodedText = ucwords($text);
        } else {
            $encodedText = strtolower($text);
        }

        if ($rplChar == '') {
            $encodedText = preg_replace('/\s[\s]+/', '', $encodedText);    // Strip off multiple spaces
            $encodedText = preg_replace('/[\s\W]+/', '', $encodedText);    // Strip off spaces and non-alpha-numeric
        } else {
            $encodedText = preg_replace('/\s[\s]+/', $rplChar, $encodedText);    // Strip off multiple spaces
            $encodedText = preg_replace('/[\s\W]+/', $rplChar, $encodedText);    // Strip off spaces and non-alpha-numeric
            $encodedText = preg_replace('/^[\\' . $rplChar . ']+/', '', $encodedText); // Strip off the starting $rplChar
            $encodedText = preg_replace('/[\\' . $rplChar . ']+$/', '', $encodedText); // // Strip off the ending $rplChar
        }
        return $encodedText;
    }

// Query Filter String from Litpi.com

    public static function queryFilterString($str)
    {
        // TODO: удалить метод
//Use RegEx for complex pattern
        $filterPattern = array(
            '/select.*(from|if|into)/i', // select table query,
            '/0x[0-9a-f]*/i', // hexa character
            '/\(.*\)/', // call a sql function
            '/union.*select/i', // UNION query
            '/insert.*values/i', // INSERT query
            '/order.*by/i'    // ORDER BY injection
        );
        $str = preg_replace($filterPattern, '', $str);

//Use normal replace for simple replacement
        $filterHaystack = array(
            '--', // query comment
            '||', // OR operator
            '\*', // OR operator
        );

        $str = str_replace($filterHaystack, '', $str);
        return $str;
    }

    /**
     * XSS Clean Data Input from Litpi.com
     */
    public static function xss_clean($data)
    {
        // TODO: удалить этот петод, так как он все равно возвращает данные не меняя их
        return $data;
// Fix &entity\n;
        $data = str_replace(array('&amp;', '&lt;', '&gt;'), array('&amp;amp;', '&amp;lt;', '&amp;gt;'), $data);
        $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
        $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
        $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

// Remove any attribute starting with "on" or xmlns
        $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

// Remove javascript: and vbscript: protocols
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

// Remove namespaced elements (we do not need them)
        $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

        do {
// Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
        } while ($old_data !== $data);

// we are done...
        return $data;
    }

    public static function getPathOfAlias($alias)
    {
        $file=Yii::getPathOfAlias($alias);
        $file_ext=$file.'.php';
        if (is_file($file_ext)) {
            return true;
        }
        return false;
    }

    public static function getContentList($contentListId, $max = null, $pagination = null, $return_type = ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD, $order = null, $options = array())
    {
        $provider = new ContentListProvider($contentListId);

        if ($max) {
            throw new CException('GxcHelpers::getContentList — $max param is deprecated');
        }
        if ($pagination) {
            throw new CException('GxcHelpers::getContentList — $pagination param is deprecated');
        }
        if ($order) {
            throw new CException('GxcHelpers::getContentList — $order param is deprecated');
        }
        if (isset($options['path'])) {
            throw new CException('GxcHelpers::getContentList — $options[\'path\'] param is deprecated');
        }

        if (isset($options['search'])) {
            $provider->setSearchOptions($options['search']);
        }

        if ($return_type == ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD) {
            return $provider->getAsActiveRecord();
        } else {
            return $provider->getAsDataProvider();
        }
    }

    /**
     * Generate slug from russian text..
     * TODO: Add ukrainian letters
     */
    public static function toSlug($string)
    {
        $russian = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya', ' ' => '-');

        $clean = strtr(mb_strtolower($string, 'utf-8'), $russian);
        $clean = preg_replace("/\s/i", "-", $clean);
        $clean = preg_replace("/\-+/i", "-", $clean);
        $clean = preg_replace("/[^a-z0-9\-_]/i", "", $clean);
        return $clean;
    }

    /* yura add function transliteration russian letters to translit */

    public static function translit($var, $separator = '', $allow_dot = false)
    {
        $var = mb_strtolower($var, 'UTF-8');
        $var = strtr($var, array(
            "ж" => "zh", "ц" => "ts", "ч" => "ch", "ш" => "sh",
            "щ" => "shch", "ь" => "", "ю" => "yu", "я" => "ya",
            "Ж" => "ZH", "Ц" => "TS", "Ч" => "CH", "Ш" => "SH",
            "Щ" => "SHCH", "Ь" => "", "Ю" => "YU", "Я" => "YA",
            "ї" => "i", "і" => "i", "Ї" => "I", "є" => "e", "Є" => "Ye",
            "." => "", "й" => "y"
        ));
        $NpjLettersFrom = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
            'е' => 'e', 'з' => 'z', 'и' => 'i', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'ц' => 'c', 'ы' => 'y'
        );
        $NpjBiLetters = array(
            "й" => "y", "ё" => "yo", "ж" => "zh", "х" => "h", "ч" => "ch",
            "ш" => "sh", "щ" => "shch", "э" => "e", "ю" => "yu", "я" => "ya",
            "ъ" => "", "ь" => "",);
        $NpjBLettersFrom = array(
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D',
            'Е' => 'E', 'З' => 'Z', 'И' => 'I', 'К' => 'K', 'Л' => 'L',
            'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Ц' => 'C', 'Ы' => 'y'
        );

        $NpjBLettersTo = "ABVGDEZIKLMNOPRSTUACY";
        $NpjBiGLetters = array(
            "Й" => "JJ", "Ё" => "JO", "Ж" => "ZH", "Х" => "KH", "Ч" => "CH",
            "Ш" => "SH", "Щ" => "SHH", "Э" => "JE", "Ю" => "YU", "Я" => "YA",
            "Ъ" => "", "Ь" => "",);
        $NpjCaps = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЪЫЭЮЯ";
        $NpjSmall = "абвгдеёжзийклмнопрстуфхцчшщьъыэюя";

        $var = str_replace(".php", "", $var);
        $var = trim(strip_tags($var));
        $var = preg_replace("/\s+/ms", $separator, $var);
        $var = strtr($var, $NpjBiGLetters);
        $var = strtr($var, $NpjBLettersFrom);
        $var = strtr($var, $NpjLettersFrom);
        $var = strtr($var, $NpjBiLetters);
        $var = preg_replace("/[^a-z0-9\_\-" . ($allow_dot ? '' : '.') . "]+/mi", "", $var);
        $var = preg_replace('#[\-]+#i', $separator, $var);

        return strtolower($var);
    }

    public static function runYiiCommand($commandName, $args = array(), $async = true)
    {
        $returnNotSuported = array(
            'returnCode' => 1,
            'output' => 'Exec is not supported'
            );

        if (!function_exists('exec')) {
            // EXEC не работает
            Yii::app()->user->setFlash('error', _t('Sorry, but your server configuration doesn\'t support exec() function'));
            return $returnNotSuported;
        }
        if (in_array('exec', array_map('trim', explode(', ', ini_get('disable_functions'))))) {
            // EXEC не работает
            Yii::app()->user->setFlash('error', _t('Sorry, but your server configuration doesn\'t permit the usage of exec() function.'));
            return $returnNotSuported;
        }
        if (ini_get('safe_mode')) {
            // EXEC не работает
            Yii::app()->user->setFlash('error', _t('Sorry, but your server is in a safe mode. Can\'t use an exec() function'));
            return $returnNotSuported;
        }

        // так же добавляем путь по которому лежит yiic
        $commandPath = Yii::getPathOfAlias('application.yiic');

        // тестим работает ли команда
        if (exec($commandPath) == '') {
            $commandPath = 'php ' . $commandPath . '.php';
        }

        $argsStr = '';
        foreach ($args as $key => $value) {
            $argsStr .= ' --' . $key .'='. $value;
        }

        if ($async) {
            // TODO: наверное стоит написать, что бы это тоже работало и на винде
            // $commandTmpl = 'bash -c "exec nohup setsid {command} > /dev/null 2>&1 &"'; // запуск за пределами процесса apache
            // $commandTmpl = '{command} > > /dev/null 2>&1 &'
            $commandTmpl = '{command} > /dev/null &';
        } else {
            $commandTmpl = '{command}';
        }

        $command = $commandPath . ' '.$commandName.' '. $argsStr;
        exec(strtr($commandTmpl, array('{command}' => $command)), $output, $return);
        return array(
            'returnCode' => $return,
            'output' => $output
            );
    }

    /**
     * Модифицирует критерию для обеспечения правильного отображения обьектов для юзера согласно их статусу
     *
     * @param CDbCriteria|array $criteria критерий или массив для модифицирования. Если массив будет не в формате критерия, то функция вернет массив в котором будет список доступных для юзера статусов обьекта
     * @return CDbCriteria
     */
    public static function applyObjectStatusCriteria(&$criteria = array())
    {
        $params = array(':objStatus' => Object::STATUS_PUBLISHED);
        if (
            Yii::app()->user->checkAccess(ConstantDefine::USER_GROUP_ADMIN)
         || Yii::app()->user->checkAccess(ConstantDefine::USER_GROUP_EDITOR)
         || Yii::app()->user->checkAccess(ConstantDefine::USER_GROUP_REPORTER)
         || Yii::app()->user->checkAccess(ConstantDefine::USER_GROUP_OWNER)
        ) {
            // этим ролям можно просматривать так же контент в статусе HIDDEN
            $condition = '(t.object_status = :objStatus OR t.object_status = :objStatusHidden)';
            $params[':objStatusHidden'] = Object::STATUS_DRAFT;
        } else {
            $condition = 't.object_status = :objStatus';
        }

        if ($criteria instanceof CDbCriteria) {
            $criteria->addCondition($condition);
            $criteria->params = CMap::mergeArray($criteria->params, $params);
        } elseif (isset($criteria['condition'])) {
            $criteria['condition'] = (!empty($criteria['condition']) ? '('.$criteria['condition'].') AND ' : '') . '('.$condition.')';
            $criteria['params'] = CMap::mergeArray($criteria['params'], $params);
        } else {
            $criteria = array_values($params);
        }

        return $criteria;
    }

    /*===========================*/
    // TODO: МЕТОДЫ, КОТОРЫЕ НУЖНО БУДЕТ ПЕРЕНЕСТИ В МОДУЛЬ АДМИНКИ, ЕСЛИ ТАКОЙ ВСЕ ЖЕ ПОЯВИТСЯ!

    /**
     * Возвращает адрес картинки-превьюшки для админки
     *
     * @param integer|Object $model id или обьект модели Object
     */
    public static function getObjectThumb($model, $htmlOptions = array())
    {
        if (is_numeric($model)) {
            $model = Object::model()->findByPk($model);
        }

        $noPhoto = 'Нету фото';

        if (!$model) {
            return $noPhoto;
        }

        $thumb = GxcHelpers::getResourceObjectFromDatabase($model, "thumbnail", "_cms_admin_thumb_");
        $thumb = count($thumb) > 0 ? CHtml::image($thumb[0]["link"], $model->object_name, $htmlOptions) : $noPhoto;

        return $thumb;
    }
}
