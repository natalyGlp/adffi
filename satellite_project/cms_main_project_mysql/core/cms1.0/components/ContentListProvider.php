<?php
/**
 * Создает критерий выборки контент листов
 * возвращает обьекты в виде CActiveDataProvider или массива из CActiveRecord
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 */

class ContentListProvider extends CComponent
{
    protected $listId;
    private $_contentList;
    private $_pagination;
    private $_searchOptions;
    private $_criteria = array();

    public function __construct($listId)
    {
        $this->listId = $listId;
    }

    public function setSearchOptions($options)
    {
        $this->_searchOptions = $options;
    }

    public function getAsActiveRecord()
    {
        $this->populateListCriteria();

        $objectClassName = $this->objectClassName;

        return $objectClassName::model()->findAll($this->_criteria);
    }

    public function getAsDataProvider()
    {
        $this->populateListCriteria();

        $options = array(
            'criteria' => $this->_criteria,
            'render_layout' => $this->getItemView(),
            'pagination' => $this->getPagination(),
        );

        if (isset($this->_pagination)) {
            $options['totalItemCount'] = $this->_pagination->itemCount;
        }

        return new RenderActiveDataProvider($this->objectClassName, $options);
    }

    protected function getItemView()
    {
        $view = GxcHelpers::getTruePath('front_blocks.listview.item_render');

        if ($type = $this->getObjectType()) {
            $view = GxcHelpers::getTruePath('content_type.' . $type . '.item_render_list');
        }

        return $view;
    }

    protected function getObjectClassName()
    {
        return Object::importByType($this->getObjectType());
    }

    protected function getObjectType()
    {
        $type = null;
        if ($this->contentList->type == ContentList::TYPE_AUTO) {
            $content_types = $this->contentList->content_type;
            if (!in_array('all', $content_types) && count($content_types) == 1) {
                $type = reset($content_types);
            }
        } else {
            if ($this->contentList->forceObjectType != '0') {
                $type = $this->contentList->forceObjectType;
            }
        }

        return $type;
    }

    protected function populateListCriteria()
    {
        if (!$this->contentList) {
            return array();
        }

        $this->_criteria = new CDbCriteria();
        GxcHelpers::applyObjectStatusCriteria($this->_criteria);

        switch ($this->contentList->type)
        {
            case ContentList::TYPE_AUTO:
                $this->populateAutoListCriteria();
                break;

            case ContentList::TYPE_MANUAL:
            case ContentList::TYPE_DYNAMIC:
                if (empty($this->contentList->manual_list)) {
                    throw new CException('There is no content in manual list');
                }
                $this->populateManualListCriteria();
                break;

            default:
                throw new CException('Unsupported content list type');
                break;
        }
    }

    protected function populateAutoListCriteria()
    {
        if (!$this->contentList->showFutureDates) {
            $this->_criteria->compare('t.object_date', '<' . time());
        }

        $content_types = $this->contentList->content_type;
        if (!in_array('all', $content_types)) {
            $this->_criteria->addInCondition('t.object_type', $content_types);
        }


        if ($this->contentList->number > 0) {
            $this->_criteria->limit = $this->contentList->number;
        }
        $this->applyOrderCriteria();

        if (isset($this->contentList->terms) && !in_array('0', $this->contentList->terms)) {
            $this->applyTermCriteria();
        }

        // DEPRECATED: !in_array('0', $this->contentList->lang), use all
        if (!in_array('all', $this->contentList->lang) && !in_array('0', $this->contentList->lang) && count(Yii::app()->translate->acceptedLanguages) > 1) {
            $this->applyLangCriteria();
        }


        // TODO: нужно создать какое-то событие в этом месте, что бы любой модуль мог сюда самостоятельно вклиниться
        if ($this->objectClassName == 'CatalogObject') {
            $this->applyShopFilterCriteria();
        }

        // Поддержка поиска
        if (isset($this->_searchOptions)) {
            $this->applySearchCriteria();
        }
    }

    protected function populateManualListCriteria()
    {
        $objectIds = $this->contentList->manual_list;

        if (count($objectIds) == 0) {
            return null;
        }

        $this->_criteria->addInCondition('t.object_id', $objectIds);
        $this->_criteria->order = 'FIELD(t.object_id, ' . implode(', ', $objectIds) . ')';
    }

    protected function applyOrderCriteria()
    {
        switch ($this->contentList->criteria) {
            case ContentList::CRITERIA_ALPHABETICAL:
                $orderBy = 't.object_name ASC';
                break;

            case ContentList::CRITERIA_WITH_MAIN:
                $orderBy = 't.is_main DESC, object_date DESC';
                break;

            case ContentList::CRITERIA_MOST_VIEWED_ALLTIME:
                $orderBy = 't.object_view DESC';
                break;

            case ContentList::CRITERIA_RANDOM:
                $orderBy = 'RAND()';
                break;

            case ContentList::CRITERIA_NEWEST:
            default:
                $orderBy = 't.object_date DESC';
                break;
        }

        $this->_criteria->order = $orderBy;
    }

    protected function applySearchCriteria()
    {
        $search = $this->_searchOptions;

        $searchCriteria = new CDbCriteria();
        if (isset($search['query']) && $search['query']) {
            if (!$search['luceneIndex']) {
                $searchCriteria->compare('t.object_date', $search['query'], false, 'OR');
                $searchCriteria->compare('t.object_content', $search['query'], true, 'OR');
                $searchCriteria->compare('t.object_title', $search['query'], true, 'OR');
                $searchCriteria->compare('t.object_name', $search['query'], true, 'OR');
            } else {
                Yii::import('cms.models.search.SearchLucene');

                // раскоментить, что бы включить обьединение терминов через И
                // SearchLucene::importLucene();
                // Zend_Search_Lucene_Search_QueryParser::setDefaultOperator(Zend_Search_Lucene_Search_QueryParser::B_AND);

                $results = SearchLucene::doSearch(mb_strtolower($search['query'], 'utf-8'), $search['luceneIndex'][0]);

                $pagination = new CPagination(count($results));
                $pagination->pageSize = $this->_criteria['limit'];
                $this->setPagination($pagination);

                // на каждой странице начиная с 2 прийдется добавлять в запрос id всех обьектов с предыдущих
                // потому, что CPagination используется для генерации лимитов в sql запросе, потому, если не заполнить запрос результатами
                // с прошлых страниц мы получил лимит, у которого будет оффсет больше чем количество строк в запросе
                $results = array_slice($results, 0, $pagination->offset+$pagination->pageSize);

                $object_ids = array();
                foreach ($results as $result) {
                    $object_ids[] = $result->primaryKey;
                }

                $searchCriteria->addInCondition('.object_id', $object_ids);

                if (count($results)) {
                    $searchCriteria->order = 'FIELD(t.object_id, ' . implode(',', $object_ids) . ')';
                }
            }
        }

        if (isset($search['date']) && $search['date']) {
            $searchCriteria->addBetweenCondition(
                't.object_date',
                strtotime($search['date'] . ' 00:00:00'),
                strtotime($search['date'] . ' 23:59:59'),
                'OR'
            );
        }

        if (isset($search['tag']) && $search['tag']) {
            if (!is_array($search['tag'])) {
                $search['tag'] = array($search['tag']);
            }
            foreach ($search['tag'] as $tag) {
                if (preg_match('/[a-z0-9-_]*/i', $tag)) {
                    // нужно сначала выбрать тег по его slug
                    $tagModel = Tag::model()->findByAttributes(array('slug' => $tag));
                    if ($tagModel) {
                        $tag = $tagModel->name;
                    }
                }
                $searchCriteria->compare('tags', $tag, true, 'OR');
            }
        }

        // Поиск по терминам таксономии
        // TODO: возможно можно обьединить с applyTermCriteria
        if (isset($search['term']) && $search['term']) {
            if (!is_array($search['term'])) {
                $search['term'] = array($search['term']);
            }

            foreach ($search['term'] as $term) {
                $termModel = Term::model()->find(array(
                    'condition' => 'name = :term OR slug = :term',
                    'params' => array(':term' => $term),
                    ));

                if ($termModel) {
                    $object_ids = CHtml::listData($termModel->objects, 'object_id', 'object_id');

                    $conditionOperator = 'AND';
                    $searchCriteria->addInCondition('t.object_id', $object_ids, $conditionOperator);
                }
            }
        }

        if (isset($search['authorId'])) {
            $searchCriteria->compare('t.object_author', $search['authorId'], false);
        }

        if ($searchCriteria->order !== '') {
            // search lucene order by relevance
            $this->_criteria->order = $searchCriteria->order;
        }
        $searchCriteria->mergeWith($this->_criteria);
        $this->_criteria = $searchCriteria;
    }

    protected function applyShopFilterCriteria()
    {
        if (isset($_GET['sort_order']) && isset($_GET['sort_type'])) {
            $sortCriteria = new CDbCriteria();
            $sortCriteria->join = 'INNER JOIN gxc_filters ON gxc_filters.id_object = object_id';
            $sortCriteria->order = $_GET['sort_type'].' '.$_GET['sort_order'];
        }

        if (!(isset($_GET['FID']) || isset($_POST['FID']))) {
            return null;
        }

        $shop = new $this->objectClassName();
        $filterData = (Yii::app()->request->isPostRequest) ? $_POST : $_GET;

        $dataProvider = $shop->filter($filterData);

        // POST запросами у нас отправляется фильтр через аякс
        // в этом случае нам нужно вернуть только количество результатов
        if (Yii::app()->request->isPostRequest) {
            while (@ob_end_clean());
            unset($filterData['ajax']); // так как в ответе мы будем генерировать ссылку с данными для фильтра, нам надо убрать элемент ajax
            if ($dataProvider->totalItemCount) {
                echo "Найдено товаров: <b>" . $dataProvider->totalItemCount . "</b>. "
                . CHtml::link(
                    'Показать',
                    preg_replace('/\?[^\?]*$/', '', $_SERVER["REQUEST_URI"]).'?'.http_build_query($filterData)
                );
            } else {
                echo "Нет подходящих товаров";
            }
            Yii::app()->end();
        }

        $filterCriteria = $dataProvider->criteria;
        if (isset($sortCriteria)) {
            $filterCriteria->mergeWith($sortCriteria);
        }

        $this_criteria->mergeWith($filterCriteria);
    }

    protected function applyTermCriteria()
    {
        $tempParamNames = array();
        $params = array();
        foreach ($this->contentList->terms as $pi => $term) {
            $tempParamNames[] = ':term'.$pi;
            $params[end($tempParamNames)] = $term;
        }

        $this->_criteria->addCondition('t.object_id IN (
            SELECT object_id
            FROM {{object_term}}
            WHERE term_id IN (' . implode(', ', $tempParamNames) . ')
            )');

        $this->_criteria->params = CMap::mergeArray($this->_criteria->params, $params);
    }

    protected function applyLangCriteria()
    {
        $contentList = $this->contentList;
        $langId = $contentList->lang;

        $useCurrentAppLanguage = in_array('auto', $contentList->lang);
        if ($useCurrentAppLanguage) {
            $defaultLanguage = Yii::app()->translate->defaultLanguage;
            $currentLangId = Yii::app()->translate->languageId;
            $langId = $currentLangId;

            if (Yii::app()->language != $defaultLanguage
                && $contentList->fallbackToMainLanguage) {
                // TODO: оптимизировать. этот класс не должен знать о всей структуре таблицы!
                $languageFallbackCriteria =  new CDbCriteria();
                $languageFallbackCriteria->join = 'LEFT JOIN {{object}} `translation` ON t.guid = translation.guid AND translation.lang = '.$currentLangId;
                $languageFallbackCriteria->select = array(
                    't.*',
                    'COALESCE(translation.object_id, t.object_id) object_id',
                    'COALESCE(translation.object_author, t.object_author) object_author',
                    'COALESCE(translation.object_date, t.object_date) object_date',
                    'COALESCE(translation.object_date_gmt, t.object_date_gmt) object_date_gmt',
                    'COALESCE(translation.object_content, t.object_content) object_content',
                    'COALESCE(translation.object_title, t.object_title) object_title',
                    'COALESCE(translation.object_excerpt, t.object_excerpt) object_excerpt',
                    'COALESCE(translation.object_status, t.object_status) object_status',
                    // 'COALESCE(translation.comment_status, t.comment_status) comment_status',
                    // 'COALESCE(translation.object_password, t.object_password) object_password',
                    'COALESCE(translation.object_name, t.object_name) object_name',
                    'COALESCE(translation.object_modified, t.object_modified) object_modified',
                    'COALESCE(translation.object_modified_gmt, t.object_modified_gmt) object_modified_gmt',
                    // 'COALESCE(translation.object_content_filtered, t.object_content_filtered) object_content_filtered',
                    'COALESCE(translation.object_parent, t.object_parent) object_parent',
                    'COALESCE(translation.guid, t.guid) guid',
                    'COALESCE(translation.object_type, t.object_type) object_type',
                    // 'COALESCE(translation.comment_count, t.comment_count) comment_count',
                    'COALESCE(translation.object_slug, t.object_slug) object_slug',
                    'COALESCE(translation.object_description, t.object_description) object_description',
                    'COALESCE(translation.lang, t.lang) lang',
                    // 'COALESCE(translation.object_author_name, t.object_author_name) object_author_name',
                    // 'COALESCE(translation.total_number_meta, t.total_number_meta) total_number_meta',
                    // 'COALESCE(translation.total_number_resource, t.total_number_resource) total_number_resource',
                    'COALESCE(translation.tags, t.tags) tags',
                    // 'COALESCE(translation.object_view, t.object_view) object_view',
                    // 'COALESCE(translation.like_count, t.like_count) like_count',
                    // 'COALESCE(translation.dislike_count, t.dislike_count) dislike_count',
                    // 'COALESCE(translation.rating_scores, t.rating_scores) rating_scores',
                    // 'COALESCE(translation.rating_average, t.rating_average) rating_average',
                    // 'COALESCE(translation.layout, t.layout) layout',
                    // 'COALESCE(translation.read_also, t.read_also) read_also',
                    // 'COALESCE(translation.read_also_limit, t.read_also_limit) read_also_limit',
                    // 'COALESCE(translation.read_also_ids, t.read_also_ids) read_also_ids',
                    // 'COALESCE(translation.sellable, t.sellable) sellable',
                    // 'COALESCE(translation.shop_id, t.shop_id) shop_id',
                    // 'COALESCE(translation.id_parent, t.id_parent) id_parent',
                    'COALESCE(translation.description, t.description) description',
                    'COALESCE(translation.is_main, t.is_main) is_main',
                    );

                $this->_criteria->mergeWith($languageFallbackCriteria);

                $langId = Yii::app()->translate->getLanguageId($defaultLanguage);

                // $langId = array($langId, Yii::app()->translate->getLanguageId($defaultLanguage));
                // $criteria->group = 'guid';
                // $criteria->order = 'FIELD(lang, ' . implode(', ', $langId) . ')';
            }
        }

        $this->_criteria->compare('t.lang', $langId);
    }

    protected function getContentList()
    {
        if (!$this->_contentList) {
            $this->_contentList = ContentList::model()->findbyPk($this->listId);
        }

        return $this->_contentList;
    }

    protected function getPagination()
    {
        if (!$this->contentList->paging) {
            return false;
        }

        $pagination = array(
            'pageSize' => $this->contentList->number,
            'pageVar' => 'page',
            'route' => 'slug/' . Yii::app()->controller->slug . (isset(Yii::app()->controller->objectSlug) ? '/'.Yii::app()->controller->objectSlug .'.html': ''), // FIXME: возможно будет не всегда верно работать
        );

        if (isset($this->_pagination)) {
            $this->_pagination->route = $pagination['route'];
            $pagination = $this->_pagination;
        }

        return $pagination;
    }

    protected function setPagination($pagination)
    {
        $this->_pagination = $pagination;
    }
}
