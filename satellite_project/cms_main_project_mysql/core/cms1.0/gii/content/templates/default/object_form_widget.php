<div class="form">
    <?php echo "<?php"; ?> $form = $this->beginWidget('CActiveForm', array(
        'id' => 'object-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <?php echo "<?php"; ?> echo $form->errorSummary($model);?>
<div class="form-wrapper">
    <div id="form-sidebar">
        <?php echo "<?php"; ?> $this->render('cms.widgets.views.object.object_publish_sidebar_form', array(
            'form' => $form,
            'model' => $model,
            'content_status' => $content_status,
            'type' => $type,
            'terms' => $terms,
            'selected_terms' => $selected_terms,
        ));?>
    </div>
    <div id="form-body">
        <div id="form-body-content">
            <!-- //Render Partial for Object Language Zone & Name & Content-->
            <?php echo "<?php"; ?> $this->render('cms.widgets.views.object.object_language_name_content_widget', array(
                'model' => $model,
                'type' => $type,
                'form' => $form,
                'versions' => $versions,
            ));?>
            <div class="row">
                            <!-- //Render Partial for Resource Binding -->
            <?php echo "<?php"; ?> $this->render('cms.widgets.views.object.object_resource_form_widget', array(
                'model' => $model,
                'type' => $type,
                'content_resources' => $content_resources,
            ));?>
            </div>

            <div class="row">
                <!--Start the Summary and SEO Box -->
                <div class="content-box ">
                    <!-- //Render Partial for SEO -->
                    <?php echo "<?php"; ?> $this->render('cms.widgets.views.object.object_seo_form_widget', array(
                        'model' => $model,
                        'form' => $form,
                    ));?>
                </div>
                <!-- End Summary and SEO Box -->
                </div>
            </div>
        </div>
    </div>
    <br class="clear" />
<?php echo "<?php"; ?> $this->endWidget();?>
</div><!-- form -->

<!-- //Render Partial for Javascript Stuff -->
<?php echo "<?php"; ?> $this->render('cms.widgets.views.object.object_form_javascript', array(
    'model' => $model,
    'form' => $form,
    'type' => $type,
));?>
