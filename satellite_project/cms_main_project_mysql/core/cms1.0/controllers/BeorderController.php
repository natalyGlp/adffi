<?php
/**
 * Backend Order Controller.
 *
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.controllers
 */

Yii::import('cms.models.order.*');
class BeorderController extends BeController
{
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->menu = array(
            array('label' => _t('All orders'), 'url' => array('index'), 'linkOptions' => array('class' => 'button')),
            array('label' => _t('Create new order'), 'url' => array('create'), 'linkOptions' => array('class' => 'button')),
        );

    }

    /**
     *
     */
    public function actionIndex($criteria = false, $render = 'render', $id = 'orderIndex')
    {
        $this->$render('index', array(
            'criteria' => $criteria,
            'id' => $id,
        ));
    }

    /**
     * Меняет статус заказа
     */
    public function actionStatus()
    {
        if (Yii::app()->request->isPostRequest) {
            $model = Order::model()->findByPk($_POST['id']);
            $model->status = $_POST['status'];
            $model->operator_id = Yii::app()->user->id;
            echo $model->save() * 1;
        }
    }

    /**
     * Выдает информацию о всех заказах юзера
     */
    public function actionUser($id)
    {
        $this->actionIndex(array(
            'condition' => 'user_id=' . $id,
        ), 'renderPartial', 'orderUser');
    }

    /**
     * Выдает информацию о всех заказах обработанных определенным оператором
     */
    public function actionOperator($id)
    {
        $this->actionIndex(array(
            'condition' => 'operator_id=' . $id,
        ), 'renderPartial', 'orderOperator');
    }

    /**
     *  Доп информация по заказу (выводит комментарий оператора)
     */
    public function actionMore($id)
    {
        $model = Order::model()->findByPk($id);
        echo '<h1>' . $model->getAttributeLabel('comment') . '</h1><pre>' . CHtml::encode($model->comment) . '</pre>';

        Yii::app()->end();
    }

    /**
     *  Оформить заказ
     */
    public function actionCreate()
    {
        // $this->render('create');
        $this->renderPartial('create', null, false, true);
    }

    /**
     *  Отредактировать заказ
     */
    public function actionUpdate()
    {
        $this->renderPartial('create', null, false, true);
    }

    /**
     *  Product suggestion for autocomplete
     *  @return array JSON array for jQuery UI AutoComplete
     */
    public function actionAcprod()
    {
        $data = new CActiveDataProvider('Object', array(
            'criteria' => array(
                'condition' => '(object_id LIKE :code OR object_name LIKE :name)AND object_type = "catalog"',
                'params' => array(
                    ':code' => (int) $_GET['term'] . '%',
                    ':name' => '%' . $_GET['term'] . '%',
                ),
            ),
        ));
        $data = $data->data;
        if (count($data)) {
            foreach ($data as $item) {
                $itemsArr[] = array(
                    'label' => $item->object_name,
                    'value' => $item->object_id,
                    'name' => $item->object_name,
                    'thumb' => GxcHelpers::getObjectThumb($item),
                    'prodId' => $item->object_id,
                );
            }

            header('Content-type: application/json');
            echo CJSON::encode($itemsArr);
        }
    }

    /**
     * Выдает более подробную информацию о продуктах в заказе
     */
    public function actionCheck($id)
    {
        $this->renderPartial('check', array(
            'id' => $id,
        ));
    }

    /**
     * возвращает json информаию о стоимости заказа
     *
     * @param integer $id id заказа
     * @return json
     */
    public function actionPrice($id = false)
    {
        $order = $id ? Order::model()->with('check')->findByPk($id) : new Order;

        if (isset($_POST['OrderCheck'])) {
            $currency_id = isset($_POST['Order']['currency_id']) ? $_POST['Order']['currency_id'] : $order->currency_id;
            $order->currency_id = $currency_id;

            $checkModels = OrderCheck::model()->findAllByAttributes(array(
                'prod_id' => array_keys($_POST['OrderCheck']),
                'order_id' => $id,
            ));
            $checkIdsList = CHtml::listData($checkModels, 'prod_id', 'prod_id');

            // только что добавленные в чек товары
            $newProducts = array_values(array_diff(array_keys($_POST['OrderCheck']), array_values($checkIdsList)));
            // добавим их в заказ для пересчета
            if (count($newProducts) > 0) {
                $newProductModels = CatalogObject::model()->findAllByAttributes(array(
                    'object_id' => $newProducts,
                ));

                foreach ($newProductModels as $newProductModel) {
                    $newCheckEntry = new OrderCheck();
                    $newCheckEntry->prod_id = $newProductModel->primaryKey;
                    $newCheckEntry->order_id = $id;
                    $newCheckEntry->quantity = 0; // количество этого товара выставится позже, так как он новый
                    array_push($checkModels, $newCheckEntry);
                }
            }
        } else {
            $checkModels = $order->check;
        }

        // пересчитаем сумму чека, так как в $_POST могут находится новые данные чека
        $summ = 0;
        $check = array();
        foreach ($checkModels as $item) {
            $item->order = $order;

            // изменилось количество товара или валюта, пересчитаем его сумму
            if (isset($_POST['OrderCheck'])) {
                $item->quantity = $_POST['OrderCheck'][$item->prod_id];
                $item->currency_id = $currency_id;
                $item->recalculatePrice();
            }

            array_push($check, array(
                'id' => $item->prod_id,
                'name' => $item->prod->object_name,
                'price' => $item->price,
                'totalPrice' => $item->total_price,
                'quantity' => $item->quantity,
                'formatedPrice' => $item->getPrice(true, false),
                'formatedTotalPrice' => $item->getTotalPrice(true, false),
            ));

            $summ += $item->total_price;
        }

        if ((!isset($_POST['OrderCheck']) || !count($_POST['OrderCheck'])) && count($_POST)) {
            // нет товаров в чеке
            $summ = 0;
        }

        $data = array(
            'id' => $order->primaryKey,
            'totalPrice' => (float) $summ,
            'formatedTotalPrice' => $order->formatMoney($summ, false),
            'currency_id' => $order->currency_id,
            'currencySymbol' => $order->currencySymbol,
            'check' => $check,
        );

        header('Content-type: text/json');
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionExport()
    {
        $orderModel = new Order('search');
        $orderModel->unsetAttributes();
        if (isset($_GET['Order'])) {
            if (isset($_GET['Order']['id'])) {
                // если были выбраны строки галочками, то тут буду id разделенные запятыми
                $_GET['Order']['id'] = explode(',', $_GET['Order']['id']);
            }

            $orderModel->attributes = $_GET['Order'];
        }

        // TODO: возможность переопределения формата экспорта. сейчас это хардкодинг под лендинги
        $this->widget('cms.extensions.excelview.EExcelView', array(
            'dataProvider' => $orderModel->search(),
            'libPath' => 'cms.vendors.phpexcel.Classes.PHPExcel',
            'title' => 'NESPIOrderExport_' . uniqid(),
            'grid_mode' => 'export',
            'creator' => 'NESPI.cms',
            'subject' => 'Order Export',
            'columns' => array(
                array(
                    'name' => '№',
                    'value' => '$data->id',
                ),
                array(
                    'name' => 'date',
                    'value' => 'date("d-m-Y H:m", strtotime($data->date))',
                ),
                array(
                    'name' => 'Товары',
                    'filter' => false,
                    'value' => '
                    call_user_func(function() use ($data)
                    {
                        // Создаем список превьюшек картинок
                        $str = array();
                        foreach($data->check as $check)
                        {
                            array_push($str, $check->prod->object_name . " ({$check->quantity})");
                        }
                        return implode("\n", $str);
                    })
                ',
                ),
                array(
                    'name' => 'ФИО',
                    'value' =>
                    '(
                        empty($data->user_id)
                        ? $data->client->first_name . " " . $data->client->last_name
                        : $data->user->first_name . " " . $data->user->last_name
                    )',
                ),
                array(
                    'name' => 'Телефон',
                    'value' => '$data->address->telephone',
                ),
                array(
                    'name' => 'Населенный пункт',
                    'value' => '$data->address->city',
                ),
                array(
                    'name' => 'Адрес',
                    'value' => '$data->address->fullAddress',
                ),
                array(
                    'name' => 'Стоимость',
                    'value' => '$data->getTotalPrice(true)',
                ),
                array(
                    'name' => 'Другая информация',
                    'value' => '$data->comment',
                ),
                array(
                    'name' => 'E-Mail',
                    'value' => '(empty($data->user_id) ? $data->client->email : $data->user->email )',
                ),
            ),
        ));
    }

    /**
     *  Возвращает pdf документ для распечатки бумажки "Подтверждение получения заказа"
     */
    public function actionPrint()
    {
        $id = $this->crudId;

        $order = Order::model()->findByPk($id);

        $mpdf = Yii::app()->ePdf->mpdf();
        // ищим файлы с лого (либо тема, либо в вьюхах модуля)
        $viewPathSuffix = '/cart/admin/check/';
        if (!(
            ($theme = Yii::app()->getTheme()) !== null

            && is_file($logoPath = $theme->viewPath . $viewPathSuffix . 'logo.png') !== false
            && is_file($logoGrayscalePath = $theme->viewPath . $viewPathSuffix . 'logo_grayscale.png') !== false)
        ) {
            $logoPath = Yii::getPathOfAlias('cart.views.admin.check') . '/logo_grayscale.png';
            $logoGrayscalePath = Yii::getPathOfAlias('cart.views.admin.check') . '/logo.png';
        }

        $mpdf->logo = file_get_contents($logoGrayscalePath);
        $mpdf->WriteHTML($this->renderPartial('check/check', array('order' => $order), true));

        $mpdf->AddPage();
        $mpdf->logo = file_get_contents($logoPath);
        $mpdf->WriteHTML($this->renderPartial('check/waranty', array('order' => $order), true));

        $mpdf->Output('confirm_' . $order->id . '.pdf', EYiiPdf::OUTPUT_TO_BROWSER);
    }

    /**
     * Load the model given by the id
     * @param int $commentId
     * @throws CHttpException
     * @return Comment
     */
    public static function loadModel($commentId)
    {
        $model = null;
        if ($commentId != 0) {
            $model = EmailLog::model()->findByPk($commentId);

        }
        if ($model == null) {
            throw new CHttpException(404, 'The request page does not exist');
        }

        return $model;

    }
}
