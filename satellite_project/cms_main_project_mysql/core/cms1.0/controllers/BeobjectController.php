<?php
/**
 * Backend Object Controller.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package backend.controllers
 *
 */
class BeobjectController extends BeController
{
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->menu = array(
            array(
                'label' => _t('Manage Content'),
                'url' => array('admin'),
                'linkOptions ' => array('class' => 'button'),
            ),
            array(
                'label' => _t('Create Content'),
                'url' => isset($_GET['type']) ? array('create', 'type' => $_GET['type']) : array('create'),
                'linkOptions' => array('class' => 'button'),
            ),
        );
    }

    /**
     * The function that do Create new Object
     *
     */
    public function actionCreate()
    {
        $this->render('object_create');
    }

    public function actionAutocompleteobject($term)
    {
        echo CJSON::encode(
            Yii::app()->{CONNECTION_NAME}->createCommand(array(
                'select' => array('object_id AS id', 'object_name AS label'),
                'from' => '{{object}}',
                'where' => 'object_name LIKE ? AND object_status=1',
                'params' => array('%' . $term . '%'),
                'order' => 'object_name ASC',
            ))->queryAll()
        );

        Yii::app()->end();
    }

    /**
     * The function that do Update Object
     *
     */
    public function actionUpdate()
    {
        $id = isset($_GET['id']) ? (int) ($_GET['id']) : 0;
        // $this->menu=array_merge($this->menu,
        //     array(
        //         array('label'=>_t('Update this content'), 'url'=>array('update','id'=>$id),'linkOptions'=>array('class'=>'btn')),
        //         array('label'=>_t('View this content'), 'url'=>array('view','id'=>$id),'linkOptions'=>array('class'=>'btn'))
        //     )
        // );

        $this->render('object_update');
    }

    /**
     * The function that do View User
     *
     */
    public function actionView()
    {
        $id = isset($_GET['id']) ? (int) ($_GET['id']) : 0;
        $this->menu = array_merge($this->menu, array(
            array('label' => _t('Update this content'), 'url' => array('update', 'id' => $id), 'linkOptions' => array('class' => 'btn')),
            array('label' => _t('View this content'), 'url' => array('view', 'id' => $id), 'linkOptions' => array('class' => 'btn')),
            //QNT added for manage all comments of the current content
            array('label' => _t('Manage comments'), 'url' => array('becomment/admin', 'object_id' => $id), 'linkOptions' => array('class' => 'btn')),
            //end QNT
        ));
        $this->render('object_view');
    }

    /**
     * The function that do Manage Object
     *
     */
    public function actionAdmin($type = '')
    {
        Yii::log('Nataly_log = '.print_r($type,true),'info','application');
        $this->render('object_admin', array('type' => 0, 'object_type' => $type));
    }

    /**
     * The function that do Manage Draft Object
     *
     */
    public function actionDraft($type = '')
    {
        $this->render('object_admin', array('type' => Object::STATUS_DRAFT, 'object_type' => $type));
    }

    /**
     * The function that do Manage Object
     *
     */
    public function actionPublished($type = '')
    {
        $this->render('object_admin', array('type' => Object::STATUS_PUBLISHED, 'object_type' => $type));
    }

    /**
     * This function sugget Person that the current user can send content to
     *
     */
    public function actionSuggestPeople()
    {
        $this->widget('cmswidgets.object.ObjectExtraWorkWidget', array('type' => 'suggest_people'));
    }

    /**
     * This function sugget Tags for Object
     *
     */
    public function actionSuggestTags()
    {

        $this->widget('cmswidgets.object.ObjectExtraWorkWidget', array('type' => 'suggest_tags'));
    }

    /**
     * The function is to Delete a Content
     *
     */
    public function actionDelete($id)
    {
        GxcHelpers::deleteModel('Object', $id);
    }
}
