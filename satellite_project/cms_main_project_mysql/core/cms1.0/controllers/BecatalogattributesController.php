<?php
/**
 * Backend catalog attributes Controller.
 *
 * @author Igogo <igogo@glp-centre.com>
 * @version 1.0
 * @package backend.controllers
 *
 */
class BecatalogattributesController extends BeController
{

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->menu = array(
        );
    }

    public function actionGetbackendform($id, $aoid)
    {
        $model = CatalogAttributes::model()->count('id=?', array($id)) ? CatalogAttributes::model()->findByPk($id) : new CatalogAttributes;

        $model->data = is_array($model->data) ? $model->data : array();

        $options = CatalogAttributesOptions::model()->findByPk($aoid);
        Yii::app()->clientScript->reset();

        $this->renderPartial('backendform', array('model' => $model, 'options' => $options, 'aoid' => $aoid), false, true);
    }

    public function actionGetparent($id)
    {
        $model = CatalogAttributes::model()->findByPk($id);
        echo CJSON::encode($model->data);
        Yii::app()->end();
    }

    /**
     * The function that do Create new Block
     *
     */
    public function actionCreate()
    {
        $this->render('create');
    }

    /**
     * The function that do Manage Block
     *
     */
    public function actionAdmin()
    {
        $this->render('admin');
    }

    /**
     * The function that update Block
     *
     */
    public function actionUpdate()
    {
        $id = isset($_GET['id']) ? (int) ($_GET['id']) : 0;
        $this->render('update', array('id' => $id));
    }

    /**
     * The function is to Delete Block
     *
     */
    public function actionDelete($id)
    {
        GxcHelpers::deleteModel('CatalogAttributesOptions', $id);
    }
}
