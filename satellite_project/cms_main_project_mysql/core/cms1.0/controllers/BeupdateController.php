<?php
/**
 * Контроллер для запуска автообновлений NESPI
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.controllers
 */

class BeupdateController extends BeController
{
    /**
     * @var string $_versionFileAlias путь к файлу с информацией о версии
     */
    protected $_versionFileAlias = 'cms.version';

    /**
     * @var string $_defaultVersion дефолтная версия в том случае, если файл версии пустой
     */
    protected $_defaultVersion = '0';

    protected function beforeAction($action)
    {
        if ($this->site) {
            return false;
        } else {
            return parent::beforeAction($action);
        }
    }

    public function actionIndex()
    {
        $this->render('index', array(
            'updateRoute' => 'update',
            'checkUpdateRoute' => 'checkupdates',
        ));
    }

    public function actionUpdate()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect('index');
        }

        $success = false;
        if ($_POST['password'] == 'glpupdate') {
            $updates = $this->getUpdates();

            $success = true;

            foreach ($updates as $componentType => $componentVersions) {
                foreach ($componentVersions as $version) {
                    if (!$this->runUpdate($version, $componentType)) {
                        $success = false;
                        break;
                    }
                }
            }

        }

        header('Content-type: application/json');
        echo CJSON::encode(array(
            'status' => $success,
        ));
    }

    public function actionCheckUpdates()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect('index');
        }

        $updates = $this->getUpdates();

        $list = array();
        foreach ($updates as $componentType) {
            $list = CMap::mergeArray($list, $componentType);
        }

        header('Content-type: application/json');
        echo CJSON::encode($list);

        // $this->renderPartial('updatesList', array(
        //     'updates' => $updates,
        //     ), false, true);
    }

    /**
     * Проверяет наличие обновлений и возвращает информацию о них
     *
     * @return array массив с информацией о доступных обновлениях
     */
    protected function getUpdates()
    {
        $versionData = $this->getComponentsVersions();

        $versionFilePath = Yii::getPathOfAlias($this->_versionFileAlias);
        $coreVersion = '';
        if (is_file($versionFilePath)) {
            $coreVersion = file_get_contents($versionFilePath);
        }

        $coreVersion = !empty($coreVersion) ? $coreVersion : '{"version": "' . $this->_defaultVersion . '"}';
        $coreVersion = CJSON::decode($coreVersion);

        $versionData['core'] = $coreVersion['version'];

        $updates = $this->sendRequest('list', array(
            'version' => $versionData,
        ));
        $updates = CJSON::decode($updates);
        $updates = $updates ? $updates : array();

        return $updates;
    }

    /**
     * Производит поиск компонентов системы и собирает массив с их версиями.
     * поддерживаемые ключи для сателлитки: modules
     *
     * Пример массива:
     * array(
     *     'modules' => array(
     *     'shop' => 'Sshop1392072284'
     *  )
     * )
     *
     * @return array массив с версиями компонентов cms
     */
    protected function getComponentsVersions()
    {

        $components = array();
        $versionFile = CMS_PATH . DIRECTORY_SEPARATOR . 'version';

        $version = '{"version": "' . preg_replace('/\d+/', 'project$0', $this->_defaultVersion) . '"}';
        if (file_exists($versionFile)) {
            $version = file_get_contents($versionFile);
        }

        $version = CJSON::decode($version);
        $components['project'][] = $version['version'];

        return $components;
    }

    /**
     * Производит автообновление
     *
     * @param string $version версия до которой обновится система
     * @param string $componentType тип компонента core, module и тд
     */
    protected function runUpdate($version, $componentType)
    {
        $path = $this->extractUpdate($version, $componentType);

        if (!$path) {
            $this->log('Error downloading update ' . $version, 'error');
            return false;
        }

        Yii::setPathOfAlias('update', $path);
        $className = 'u' . $version . '_AutoUpdater';
        include $path . '/.update/' . $className . '.php';

        $updater = new $className();
        $updater->init();

        $destination = $updater->getDestination(); // сюда распакуется обновление
        if (!is_dir($destination)) {
            $this->log('The update destination directory does not exists');
            $this->unlinkR($path);
            return false;
        }

        if (!$updater->beforeUpdate()) {
            return false;
        }

        // $migrationPathes = array(
        //     'update.extensions.yii-notify.migrations',
        //     'update.models.migrations',
        //     );
        $migrationPathes = $updater->getMigrations();

        foreach ($migrationPathes as $migration) {
            $result = $this->runMigration('up', $migration);
            if (strpos($result, 'Migration failed') !== false || strpos($result, 'Error') !== false) {
                $this->log('Error runing migrations at ' . $migration . PHP_EOL . $result, 'error');
                $this->unlinkR($path);
                return false;
            }
        }

        if (!$updater->beforeMove()) {
            return false;
        }

        // TODO: проверки ошибок
        $this->copyR($path, $destination);
        $this->unlinkR($destination . DIRECTORY_SEPARATOR . '.update');
        $this->unlinkR($path);

        $this->log('Successfully updated to ' . $version);

        $updater->afterUpdate();

        return true;
    }

    /**
     * Скачивает обновление с сервера обновлений и распаковывает его во временную диреткорию
     *
     * @param strin $version по которому нужно скачать обновление
     * @param string $componentType тип компонента core, module и тд
     * @return string путь к директории с распакованными файлами
     */
    protected function extractUpdate($version, $componentType)
    {
        $update = $this->sendRequest('get', array(
            'version' => $version,
            'componentType' => $componentType,
            'format' => 'zip',
        ));

        if ($update) {
            // временная папочка для хранения апдейта на время его установки
            $extractTo = Yii::app()->runtimePath . DIRECTORY_SEPARATOR . 'update' . DIRECTORY_SEPARATOR . uniqid();
            mkdir($extractTo, 0777, true);

            $saveTo = $extractTo . DIRECTORY_SEPARATOR . $version . '.zip';

            if (file_put_contents($saveTo, $update)) {
                $zip = new ZipArchive;
                if ($zip->open($saveTo) === true) {
                    if ($zip->extractTo($extractTo) === false) {
                        return false;
                    }

                    $zip->close();
                    @unlink($saveTo);
                    @exec("chmod -R 777 '" . $extractTo . "'");

                    return $extractTo;
                }
            }
        }

        return false;
    }

    /**
     * Запускает миграции
     *
     * @return string вывод консольной комманды
     */
    protected function runMigration($action = 'up', $path = 'application.migrations', $limit = null)
    {
        // TODO: КОСТЫЛЬ для изменения префикса таблицы миграций
        if (Yii::app()->db->schema->getTable('tbl_migration')) {
            Yii::app()->db->createCommand()->renameTable('tbl_migration', '{{migration}}');
        }

        // так как CMigrationCommand выкидывает die(), если директории миграции не существует, то выполним эту проверку здесь более изящно
        if (!is_dir(Yii::getPathOfAlias($path))) {
            return 'Error: The migration directory does not exist: ' . $path;
        }

        $runner = new CConsoleCommandRunner();
        $commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
        $runner->addCommands($commandPath);
        $action = is_numeric($limit) ? $action . ' ' . $limit : $action;
        $args = array('yiic', 'migrate', $action, '--interactive=0', '--migrationTable={{migration}}', '--migrationPath=' . $path);
        ob_start();
        try {
            // перехватываем исключения, что бы они не поломали нам буффер
            $runner->run($args);
        } catch (Exception $e) {
            ob_get_clean();
            throw $e;
        }

        return "Path: $path" . htmlentities(ob_get_clean(), null, Yii::app()->charset);
    }

    /**
     * Отправляет запрос к хосту автообновлений
     *
     * @access protected
     * @return array ответ сервера
     */
    protected function sendRequest($uri, $params)
    {
        $curl = curl_init();

        $params = array(
            'request' => CJSON::encode($params),
        );
        $params['hash'] = $this->signRequest($params);

        $serverUrl = 'http://nespi.sandbox.webtests.in.ua/update/';

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $serverUrl . $uri,
            CURLOPT_USERAGENT => 'NESPI Updater',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $params,
        ));

        if (!($ans = curl_exec($curl)) || ($statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE)) != 200) {
            $ans = null;
        }
        curl_close($curl);

        return $ans;
    }

    /**
     * Подписывает параметры запроса
     * @return string хеш подписи
     */
    protected function signRequest($params)
    {
        $secret = "A+<U7dr%y8O:kD<i@'e6}0MmTtDN04";

        ksort($params);
        $keysArr = array_keys($params);
        $hash = sha1(sha1(CJSON::encode($params) . implode('~', $keysArr)) . $secret);

        return $hash;
    }

    /**
     * Рекурсивно копирует директорию
     * @param string $source путь к директории, которую нужно копировать
     * @param string $dest куда копировать
     * @param integer $filePerm права на файлы
     * @param integer $dirPerm права на директории
     */
    protected function copyR($source, $dest, $filePerm = 0644, $dirPerm = 0755)
    {
        if (is_file($dest) && !is_dir($dest)) {
            throw new Exception('Путь $dest (' . $dest . ') не должен указывать на файл.');
        }

        if (!file_exists($dest)) {
            mkdir($dest);
        }

        foreach ($iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST
        ) as $item) {
            $destItemPath = $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName();
            if ($item->isDir()) {
                if (!file_exists($destItemPath)) {
                    mkdir($destItemPath);
                    @chmod($destItemPath, $dirPerm);
                }
            } else {
                copy($item, $destItemPath);
                @chmod($destItemPath, $filePerm);
            }
        }
    }

    /**
     * Recursively delete a directory
     *
     * @param string $dir Directory name
     * @param boolean $deleteRootToo Delete specified top-level directory as well
     */
    protected function unlinkR($dir, $deleteRootToo = true)
    {
        if (!$dh = @opendir($dir)) {
            return;
        }
        while (false !== ($obj = readdir($dh))) {
            if ($obj == '.' || $obj == '..') {
                continue;
            }

            if (!@unlink($dir . '/' . $obj)) {
                $this->unlinkR($dir . '/' . $obj, true);
            }
        }

        closedir($dh);

        if ($deleteRootToo) {
            @rmdir($dir);
        }

        return;
    }

    protected function log($message, $level = 'info')
    {
        Yii::log($message, $level, get_class($this));
    }
}
