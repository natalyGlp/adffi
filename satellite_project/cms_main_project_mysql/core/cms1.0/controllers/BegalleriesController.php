<?php
class BegalleriesController extends BeController
{
    public $model = 'Galleries';
    public $r_folder;
    public $r_url = RESOURCE_URL;
    public $upload_options = array(
        'bootstrap' => false,
    );
    public $image_versions = array(
        '_thumb' => array(
            'max_width' => 160,
            'max_height' => 120,
        ),
        '_small' => array(
            'max_width' => 200,
            'max_height' => 150,
        ),
        '_middle' => array(
            'max_width' => 280,
            'max_height' => 210,
        ),
        '_large' => array(
            'max_width' => 800,
            'max_height' => 600,
        ),
    );

    public function __construct($id, $module = null)
    {
        $this->r_folder = Yii::getPathOfAlias('uploads.resources');
        parent::__construct($id, $module);
        $this->menu = array(
            array(
                'label' => _t('Manage Galleries'),
                'url' => array(
                    'admin',
                ),
                'linkOptions' => array(
                    'class' => 'button',
                ),
            ),
            array(
                'label' => _t('Add Gallery'),
                'url' => array(
                    'create',
                ),
                'linkOptions' => array(
                    'class' => 'button',
                ),
            ),
        );
    }

    public function actionUpload($id)
    {
        $gallery = Galleries::model()->findByPk($id);
        $this->upload_options['upload_dir'] = $this->r_folder . '/galleries/' . $gallery->id . '/';
        $this->upload_options['upload_url'] = $this->r_url . '/galleries/' . $gallery->id . '/';

        $image_versions = json_decode($gallery->resize_options_json, true);
        $image_versions = is_array($image_versions) ? $image_versions : $this->image_versions;
        $this->upload_options['image_versions'] = array();
        $this->upload_options['image_versions'] = $image_versions;

        error_reporting(E_ALL|E_STRICT);

        header('Pragma: no-cache');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Content-Disposition: inline; filename="files.json"');
        header('X-Content-Type-Options: nosniff');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
        header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');

        $this->upload_options['upload_action'] = Yii::app()->createAbsoluteUrl('/begalleries/upload') . '/?id=' . $id . '&site=' . (isset($_GET['site']) ? $_GET['site'] : '');

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'OPTIONS':
                break;

            case 'HEAD':
            case 'GET':

                $this->widget('cms.extensions.YiiJqueryMultyUploader.Upload', array(
                    'options' => $this->upload_options,
                ))->get(GalleryPhotos::model()->findAll('gallery_id=?', array(
                    $id,
                )));
                break;

            case 'POST':
                if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
                    $this->widget('cms.extensions.YiiJqueryMultyUploader.Upload', array(
                        'options' => $this->upload_options,
                    ))->delete();
                } else {
                    $this->widget('cms.extensions.YiiJqueryMultyUploader.Upload', array(
                        'options' => $this->upload_options,
                    ))->post('GalleryPhotos', array(
                        'gallery_id' => $id,
                    ));
                }
                break;

            case 'DELETE':
                $model = isset($_GET['file']) ? GalleryPhotos::model()->findByPk((int) $_GET['file']) : null;
                $this->widget('cms.extensions.YiiJqueryMultyUploader.Upload', array(
                    'options' => $this->upload_options,
                ))->delete($model);
                break;

            default:
                header('HTTP/1.1 405 Method Not Allowed');
        }
    }

    public function actionAdmin()
    {
        $this->render('galleries_admin');
    }

    public function actionEdit($id)
    {
        $tit = _t("Update");
        $this->pageTitle = $this->pageTitle . " | " . $tit;
        $this->breadcrumbs[] = $tit;

        $model = Galleries::model()->findByPk((int) $id);

        if (isset($_POST[$this->model])) {
            $model->setAttributes($_POST[$this->model], false);

            if (isset($_POST['versions'])) {
                $vers = array();
                foreach ($_POST['versions'] as $v) {
                    if (isset($v['version'], $v['max_width'], $v['max_height']) && !empty($v['version']) && !empty($v['max_width']) && !empty($v['max_height'])) {
                        $vers[$v['version']] = array(
                            'max_width' => $v['max_width'],
                            'max_height' => $v['max_height'],
                        );
                    }
                }
                $model->resize_options_json = $vers;
            }
            $model->resize_options_json = json_encode($model->resize_options_json);
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', _t('Update gallery Successfully!'));
                } else {
                    user()->setFlash('error', _t('Error!'));
                }
            }
        }

        $model->resize_options_json = json_decode($model->resize_options_json, true);
        $model->resize_options_json = is_array($model->resize_options_json) ? $model->resize_options_json : $this->image_versions;
        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionPhotos($id)
    {

        // bootstrap.plugins.min.js конфликтует с скриптами загрузки, отключаем его
        Yii::app()->clientscript->scriptMap['bootstrap.plugins.min.js'] = false;
        $gallery = Galleries::model()->findByPk($id);
        $this->upload_options['bootstrap'] = true;
        $this->upload_options['upload_dir'] = $this->r_folder . '/galleries/' . $gallery->id . '/';
        $this->upload_options['upload_url'] = $this->r_url . '/galleries/' . $gallery->id . '/';
        $this->upload_options['upload_action'] = Yii::app()->createAbsoluteUrl('/begalleries/upload') . '?id=' . $id;
        $this->render('photos');
    }

    public function actionCreate()
    {
        $model = new Galleries;

        if (isset($_POST[$this->model])) {
            $model->setAttributes($_POST[$this->model], false);

            if (isset($_POST['versions'])) {
                $vers = array();
                foreach ($_POST['versions'] as $v) {
                    if (isset($v['version'], $v['max_width'], $v['max_height']) && !empty($v['version']) && !empty($v['max_width']) && !empty($v['max_height'])) {
                        $vers[$v['version']] = array(
                            'max_width' => $v['max_width'],
                            'max_height' => $v['max_height'],
                        );
                    }
                }
                $model->resize_options_json = $vers;
            }

            $model->resize_options_json = json_encode($model->resize_options_json);
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', _t('Create new gallery Successfully!'));
                } else {
                }
            }
        }

        $model->resize_options_json = !empty($model->resize_options_json) ? json_decode($model->resize_options_json, true) : $this->image_versions;
        $this->render('form', array(
            'model' => $model,
        ));
    }
    public function actionDelete($id)
    {
        $gallery = Galleries::model()->findByPk($id);
        AlbumsGalleries::model()->deleteAll('id_gallery=:id_gallery', array(
            ':id_gallery' => $id,
        ));
        $items = GalleryPhotos::model()->findAllByAttributes(array(
            'gallery_id' => $id,
        ));
        if (count($items) == 0) {
            $gallery->delete();
            @rmdir($this->r_folder . '/galleries/' . $gallery->id . '/');
            Yii::app()->user->setFlash('success', _t('Delete Successfully!'));
        } else {
            Yii::app()->user->setFlash('error', _t('Gallery not empty!'));
        }
    }
}
