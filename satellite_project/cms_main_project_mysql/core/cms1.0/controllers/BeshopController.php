<?php
/**
 * Backend Shop Controller.
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.controllers
 */

/**
 * NOTE: работает не через эту излишне дискретизированную систему с виджетами под каждый экщен контроллера
 */
Yii::import('cms.models.order.*');
class BeshopController extends BeController
{
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->menu = array(
            array('label' => _t('Settings'), 'url' => array('settings'), 'linkOptions' => array('class' => 'button')),
        );
    }

    /**
     * Выводит форму настроек магазина
     */
    public function actionSettings()
    {
        $model = new OrderType('search');
        $model->unsetAttributes();
        if (isset($_GET['OrderType'])) {
            $model->attributes = $_GET['OrderType'];
        }

        if (!isset($_GET['OrderType']['enabled'])) {
            $model->enabled = 1;
        }

        $orderTypesDataProvider = $model->search();
        $this->render('settings', array(
            'orderTypesDataProvider' => $orderTypesDataProvider,
            'filterModel' => $model,
        ));

    }

    /**
     * Добавление/редактирование способов доставки
     */
    public function actionOrderTypeUpdate($id = false)
    {
        $model = $id ? OrderType::model()->findByPk($id) : new OrderType();

        if (isset($_POST['ajax'])) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['OrderType'])) {
            $model->attributes = $_POST['OrderType'];
            if ($model->save()) {
                $this->redirect(array('ordertypeupdate', 'id' => $model->primaryKey));
            }
        }
        $params = array(
            'model' => $model,
        );
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('createOrderType', $params, false, true);
        } else {
            $this->render('createOrderType', $params);
        }

    }
    public function actionOrderTypeCreate()
    {
        $this->actionOrderTypeUpdate();
    }
}
