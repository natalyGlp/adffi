<?php
/**
 * Backend Email Controller.
 *
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.controllers
 *
 */
class BeemailController extends BeController
{
    public function __construct($id, $module = null)
    {
        $this->menu = array(
            array('label' => _t('View all emails'), 'url' => array('index'), 'linkOptions' => array('class' => 'button')),
        );
        parent::__construct($id, $module);

    }

    /**
     * The function that do View User
     *
     */
    public function actionView($id)
    {
        $this->menu = array_merge($this->menu,
            array(
                array('label' => _t('Set this email pending'), 'url' => array('set', 'id' => $id, 'status' => EmailLog::STATUS_PENDING), 'linkOptions' => array('class' => 'button')),
                array('label' => _t('Set this email as done'), 'url' => array('set', 'id' => $id, 'status' => EmailLog::STATUS_DONE), 'linkOptions' => array('class' => 'button')),
            )
        );
        $this->render('email_log_view');
    }

    /**
     * Manage comment given by the comment status (type)
     * and the object (object_id) to which all the comment belong
     */
    public function actionIndex()
    {
        // TODO: переключение статусов через ajax
        $this->render('email_log_index');

    }

    /**
     * Change the status of the comment given by the $id to "published"
     */
    public function actionSet($id = null, $status = null)
    {
        $model = self::loadModel($id);
        if (EmailLog::getStatus($status) && $model) {
            $model->status = $status;
            $model->save();
            Yii::app()->user->setFlash('success', _t('Email was successfully set as {status}', 'cms', array(
                '{status}' => $model->statusLabel,
            )));
        } else {
            Yii::app()->user->setFlash('error', _t('An error occured while changing email status'));
        }
        $this->redirect($model->backendViewUrl . "&site=" . Yii::app()->controller->site);
    }

    /**
     * Load the model given by the id
     * @param int $commentId
     * @throws CHttpException
     * @return Comment
     */
    public static function loadModel($commentId)
    {
        $model = null;
        if ($commentId != 0) {
            $model = EmailLog::model()->findByPk($commentId);

        }
        if ($model == null) {
            throw new CHttpException(404, 'The request page does not exist');
        }

        return $model;

    }

    /**
     * The function is to Delete a Comment
     *
     */
    public function actionDelete($id)
    {
        GxcHelpers::deleteModel('EmailLog', $id);
    }

}
