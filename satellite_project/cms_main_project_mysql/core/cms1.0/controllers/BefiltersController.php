<?php
/**
 * Backend filters Controller.
 *
 * @author Igogo <igogo@glp-centre.com>
 * @version 1.0
 * @package backend.controllers
 *
 */
class BefiltersController extends BeController
{
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->menu = array(
        );
    }

    public function init()
    {
        if (!isset($_GET['site']) || empty($_GET['site'])) {
            return true;
        }

        $this->breadcrumbs = array(
            'Сайты' => array('/sites/admin'),
            'Управление',
        );

        $this->layout = 'site_manage';
        $this->menu = Sites::model()->menu;
    }

    public function actionGetbackendform($id, $foid)
    {
        $model = Filters::model()->count('id=?', array($id)) ? Filters::model()->findByPk($id) : new Filters;
        $options = FiltersOptions::model()->findByPk($foid);
        Yii::app()->clientScript->reset();

        $this->renderPartial('backendform', array('model' => $model, 'options' => $options), false, true);
    }

    /**
     * The function that do Create new Block
     *
     */
    public function actionCreate()
    {
        $this->render('create');
    }

    /**
     * The function that do Manage Block
     *
     */
    public function actionAdmin()
    {
        $this->render('admin');
    }

    /**
     * The function that update Block
     *
     */
    public function actionUpdate()
    {
        $id = isset($_GET['id']) ? (int) ($_GET['id']) : 0;
        $this->render('update', array('id' => $id));
    }

    /**
     * The function is to Delete Block
     *
     */
    public function actionDelete($id)
    {
        GxcHelpers::deleteModel('FiltersOptions', $id);
    }
}
