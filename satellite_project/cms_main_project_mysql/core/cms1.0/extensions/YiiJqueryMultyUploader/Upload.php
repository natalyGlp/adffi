<?php 
function return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }

    return $val;
}
class Upload extends CWidget{
	public $options;
    public $add = '';
    public $ext=array(
        'video.jpeg' => array('avi', 'mp4', 'flv', 'mpeg', 'mpg', 'mpe', 'qt', 'mov', 'avi', 'movie', ),
        'audio.png' => array('mpga', 'mp2', 'mp3', 'aif', 'aiff', 'aifc', 'mid', 'midi', 'ram', 'rm', 'rpm', 'ra', 'rv', 'wav','ogg'),
        'zip-ico.png' => array('gz', 'tar', 'tgz', 'rar', 'zip'),
        'IMG' => array('jpeg', 'tiff', 'bmp', 'gif', 'jpg', 'tif', 'png'),
        'ppt.png' => array('ppt', 'pptx'),
        'xls.png' => array('xls', 'xl', 'xlsx'),
        'doc.png' => array('doc', 'docx', 'word'),
        'pdf.png' => array('pdf'),
        'psd.png' => array('psd')
    );

	public function init(){
		$options = $this->options;
        $site = isset($_GET['site']) && !empty($_GET['site']) ? 'http://'.$_GET['site'] : '';
        $this->options = array(
        	'upload_action' => Yii::app()->createAbsoluteUrl('site/upload'),
            'upload_dir' => Yii::app()->basePath.'/../upload/',
            'upload_url' => $site.Yii::app()->baseUrl.'/upload/',
            'param_name' => 'files',
            'delete_type' => 'DELETE',
            'max_file_size' => min(array('20000000',return_bytes(ini_get('post_max_size')))),
            'post_title' => 'title',
        	'bootstrap' => true,
            'post_description' => 'description',
            'min_file_size' => 1,
            'accept_file_types' => '/(\.|\/)(jpe?g|avi|undef|gif|bmp|tiff|doc|xls|ppt|pptx|rar|zip|pdf|mp4|flv|ogg|hqx|cpt|csv|bin|dms|lha|lzh|exe|class|psd|so|sea|dll|oda|ai|eps|ps|smi|smil|mif|wbxml|wmlc|dcr|dir|dxr|dvi|gtar|gz|php|php4|php3|phtml|phps|js|swf|sit|tar|tgz|xhtml|xht|mid|midi|mpga|mp2|mp3|aif|aiff|aifc|ram|rm|rpm|ra|rv|wav|jpe|png|tif|css|html|htm|shtml|txt|text|log|rtx|rtf|xml|xsl|mpeg|mpg|mpe|qt|mov|movie|docx|xlsx|word|xl|eml|json)$/i',
            'max_number_of_files' => null,
        	'previewFileTypes' => '/^image\/(gif|jpe?g|png)$/',
            'discard_aborted_uploads' => true,
            'orient_image' => false,
            'image_versions' => array()
        );
        
        $this->options['image_versions'] = array();
        
        if ($options) {
            $this->options = array_replace_recursive($this->options, $options);
        }
        $this->options['upload_action'] .= '&'.$this->add;
        $this->options['upload_url'] = $site.$this->options['upload_url'];

		$js = dirname(__FILE__).'/js';
        $url=Yii::app()->assetManager->publish($js = dirname(__FILE__).'/assets');
		//$jsUrl = Yii::app()->assetManager->publish($js);
        $imUrl = $url.'/img';
        $csUrl = $url.'/css';
		$jsUrl = $url.'/js';
		//$imUrl = Yii::app()->assetManager->publish($im);
		//$cs = dirname(__FILE__).'/css';
		//$csUrl = Yii::app()->assetManager->publish($cs);
		$cs = Yii::app()->clientScript;
		
		$cs->registerCoreScript('jquery');
		// if($this->options['bootstrap']){
		// 	$cs->registerCssFile($csUrl.'/bootstrap.min.css');
		// 	$cs->registerCssFile($csUrl.'/bootstrap-responsive.min.css');
		// }
		$cs->registerCssFile($csUrl.'/bootstrap-image-gallery.min.css');
		$cs->registerCssFile($csUrl.'/jquery.fileupload-ui.css');
		$cs->registerScriptFile($jsUrl.'/vendor/jquery.ui.widget.js', CClientScript::POS_END);
		$cs->registerScriptFile($jsUrl.'/tmpl.min.js', CClientScript::POS_END);
		$cs->registerScriptFile($jsUrl.'/load-image.min.js', CClientScript::POS_END);
		$cs->registerScriptFile($jsUrl.'/canvas-to-blob.min.js', CClientScript::POS_END);
		
		// if($this->options['bootstrap']){
		// 	$cs->registerScriptFile($jsUrl.'/bootstrap.min.js', CClientScript::POS_END);
		// }
		$cs->registerScriptFile($jsUrl.'/bootstrap-image-gallery.min.js', CClientScript::POS_END);
		$cs->registerScriptFile($jsUrl.'/jquery.iframe-transport.js', CClientScript::POS_END);
		$cs->registerScriptFile($jsUrl.'/jquery.fileupload.js', CClientScript::POS_END);
		$cs->registerScriptFile($jsUrl.'/jquery.fileupload-ip.js', CClientScript::POS_END);
		$cs->registerScriptFile($jsUrl.'/jquery.fileupload-ui.js', CClientScript::POS_END);
		$cs->registerScriptFile($jsUrl.'/locale.js', CClientScript::POS_END);
		$cs->registerScriptFile($jsUrl.'/main.js', CClientScript::POS_END);
		
		$cs->registerScriptFile($jsUrl.'/cors/jquery.xdr-transport.js', CClientScript::POS_END);

        if(!file_exists($this->options['upload_dir'])){
            @mkdir($this->options['upload_dir'], 0777, true);
            @chmod($this->options['upload_dir'], 0777, true);
        }
	}

	public function form(){
		$this->render('index');
	}
    
    protected function set_file_delete_url($file, $realname = null, $id = null) {
        $file->delete_url = $this->options['upload_action'].'&file='.($id ? $id : ($realname ? $realname : $file->name));
        $file->delete_type = $this->options['delete_type'];
        if ($file->delete_type !== 'DELETE') {
            $file->delete_url .= '&_method=DELETE';
        }
    }
    
    public function getVersion($file_name, $v, $r = ''){
    	$imgs = explode('.', $file_name);
    	$ext = end($imgs);
    	$img = str_replace('.'.$ext, '', $file_name);
    	$image = !empty($r) ? str_replace($r, $v, $img).'.'.$ext : $img.$v.'.'.$ext;
    	return $image;
    }
    
    public function get_file_object($file_name, $title = null, $id = null) {
    	$fn = $this->getVersion($file_name, '_org');
        $file_path = $this->options['upload_dir'].$fn;
        if (is_file($file_path) && $file_name[0] !== '.') {

            $file = new stdClass();
            $file->name = $title ? $title : $fn;
            $file->size = filesize($file_path);
            $file->id = $id;
            $file->url = $this->options['upload_url'].($fn);
            foreach($this->options['image_versions'] as $version => $options) {
                if (is_file($this->options['upload_dir'].$this->getVersion($file_name, $version))) {
                    $file->{$version.'_url'} = $this->options['upload_url'].$this->getVersion($file_name, $version);
                }
            }
            $this->set_file_delete_url($file, $file_name, $id);
            return $file;
        }
        return null;
    }
    
    public function get_file_objects() {
        return array_values(array_filter(array_map(
            array($this, 'get_file_object'),
            @scandir($this->options['upload_dir'])
        )));
    }

    public function create_scaled_image($file_name, $options, $v = '_org') {
        $file_path = $this->options['upload_dir'].$this->getVersion($file_name, '_org');
        $new_file_path = $this->options['upload_dir'].$this->getVersion($file_name, $v);
        list($img_width, $img_height) = @getimagesize($file_path);
        if (!$img_width || !$img_height) {
            return false;
        }
        $scale = min(
            $options['max_width'] / $img_width,
            $options['max_height'] / $img_height
        );
        if ($scale >= 1) {
            if ($file_path !== $new_file_path) {
                return copy($file_path, $new_file_path);
            }
            return true;
        }
        $new_width = $img_width * $scale;
        $new_height = $img_height * $scale;
        $new_img = @imagecreatetruecolor($new_width, $new_height);
        switch (strtolower(substr(strrchr($file_name, '.'), 1))) {
            case 'jpg':
            case 'jpeg':
                $src_img = @imagecreatefromjpeg($file_path);
                $write_image = 'imagejpeg';
                $image_quality = isset($options['jpeg_quality']) ?
                    $options['jpeg_quality'] : 75;
                break;
            case 'gif':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                $src_img = @imagecreatefromgif($file_path);
                $write_image = 'imagegif';
                $image_quality = null;
                break;
            case 'png':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                @imagealphablending($new_img, false);
                @imagesavealpha($new_img, true);
                $src_img = @imagecreatefrompng($file_path);
                $write_image = 'imagepng';
                $image_quality = isset($options['png_quality']) ?
                    $options['png_quality'] : 9;
                break;
            default:
                $src_img = null;
        }
        $success = $src_img && @imagecopyresampled(
            $new_img,
            $src_img,
            0, 0, 0, 0,
            $new_width,
            $new_height,
            $img_width,
            $img_height
        ) && $write_image($new_img, $new_file_path, $image_quality);
        @imagedestroy($src_img);
        @imagedestroy($new_img);
        chmod($new_file_path, 0777);
        return $success;
    }
    
    public function has_error($uploaded_file, $file, $error) {
        if ($error) {
            return $error;
        }
        if (!is_null($this->options['accept_file_types']) && !preg_match($this->options['accept_file_types'], $file->name)) {
            return 'acceptFileTypes';
        }
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = filesize($uploaded_file);
        } else {
            $file_size = $_SERVER['CONTENT_LENGTH'];
        }
        if ($this->options['max_file_size'] && (
                $file_size > $this->options['max_file_size'] ||
                $file->size > $this->options['max_file_size'])
            ) {
            return 'maxFileSize';
        }
        if ($this->options['min_file_size'] &&
            $file_size < $this->options['min_file_size']) {
            return 'minFileSize';
        }
        if (is_int($this->options['max_number_of_files']) && (
                count($this->get_file_objects()) >= $this->options['max_number_of_files'])
            ) {
            return 'maxNumberOfFiles';
        }
        return $error;
    }

    public function upcount_name_callback($matches) {
        $index = isset($matches[1]) ? intval($matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';
        return ' ('.$index.')'.$ext;
    }

    public function upcount_name($name) {
        return preg_replace_callback(
            '/(?:(?: \(([\d]+)\))?(\.[^.]+))?$/',
            array($this, 'upcount_name_callback'),
            $name,
            1
        );
    }
    
    public function trim_file_name($name, $type) {
    	
    	$arr = explode('.', $name);
    	$ext = end($arr);
    	$name = GxcHelpers::translit(str_replace('.'.$ext, '', $name), '', true);
    	$name = $name.'.'.$ext;
    	
        $file_name = trim(basename(stripslashes($name)), ".\x00..\x20");
        if (strpos($file_name, '.') === false &&
            preg_match('/^image\/(gif|jpe?g|png)/', $type, $matches)) {
            $file_name .= '.'.$matches[1];
        }
        if ($this->options['discard_aborted_uploads']) {
            while(is_file($this->options['upload_dir'].$file_name)) {
                $file_name = $this->upcount_name($file_name);
            }
        }
        return $file_name;
    }

    public function orient_image($file_path) {
      	$exif = @exif_read_data($file_path);
      	$orientation = intval(@$exif['Orientation']);
      	if (!in_array($orientation, array(3, 6, 8))) { 
      	    return false;
      	}
      	$image = @imagecreatefromjpeg($file_path);
      	switch ($orientation) {
        	  case 3:
          	    $image = @imagerotate($image, 180, 0);
          	    break;
        	  case 6:
          	    $image = @imagerotate($image, 270, 0);
          	    break;
        	  case 8:
          	    $image = @imagerotate($image, 90, 0);
          	    break;
          	default:
          	    return false;
      	}
      	$success = imagejpeg($image, $file_path);
      	@imagedestroy($image);
      	return $success;
    }
    
    public function handle_file_upload($uploaded_file, $name, $size, $type, $error) {
        //var_dump($uploaded_file, $name, $size, $type, $error);
        $file = new stdClass();
        $file->name = strtolower(time().rand(1,999).'_'.$this->trim_file_name($name, $type));
        $file->size = intval($size);
        $file->type = $type;
        $error = $this->has_error($uploaded_file, $file, $error);
        
		if(!file_exists($this->options['upload_dir'])) mkdir($this->options['upload_dir'],0777,true);
        if (!$error && $file->name) {
            $file_path = $this->options['upload_dir'].$this->getVersion($file->name, '_org');
            $append_file = !$this->options['discard_aborted_uploads'] &&
                is_file($file_path) && $file->size > filesize($file_path);
            clearstatcache();
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                file_put_contents(
                    $file_path,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }
            $file_size = filesize($file_path);
            if ($file_size === $file->size) {
            	chmod($file_path, 0777);
            	if ($this->options['orient_image']) {
            		$this->orient_image($file_path);
            	}
                $file->url = $this->options['upload_url'].rawurlencode($file->name);
                foreach($this->options['image_versions'] as $version => $options) {
                    if ($this->create_scaled_image($file->name, $options, $version)) {
                        $file->{$version.'_url'} = $this->options['upload_url'].rawurlencode($this->getVersion($file->name, $version));
                    }
                }
            } elseif ($this->options['discard_aborted_uploads']) {
                unlink($file_path);
                $file->error = 'abort';
            }
            $file->size = $file_size;
            $this->set_file_delete_url($file);
        } else {
            $file->error = $error;
        }
        return $file;
    }
    
    public function get($model = null, $isSingle = false) {
        $file_name = isset($_REQUEST['file']) ? basename(stripslashes($_REQUEST['file'])) : null;
        if($model) {
        	$info = Array();
			if(!$isSingle){
	        	foreach($model as $fl){
	        		$file = $this->get_file_object($fl->file, isset($fl->{$this->options['post_title']}) ? $fl->{$this->options['post_title']} : '', $fl->id);
		        	$src = Yii::app()->baseUrl.'/images/noformat.png';
					foreach($this->ext as $k => $v){
						if(in_array($fl->ext, $v)){
							if($k == 'IMG'){
								$src = $this->options['upload_url'].$fl->file;
							}else{
								$src = Yii::app()->baseUrl.'/images/'.$k;
							}
						}
					}
					
					if($file){
						$file->thumbnail_url = $this->getVersion($src, '_org');
	        			$info[] = $file;
					}
	        	}
			} else {
				$info[] = $this->get_file_object($model->file, (isset($model->{$this->options['post_title']}) ? $model->{$this->options['post_title']} : ''), $model->id);
			}       
        } elseif ($file_name) {
            $info = $this->get_file_object($file_name);
        } else {
            $info = $this->get_file_objects();
        }
        header('Content-type: application/json');
        echo json_encode($info);
    }
    
    public function post($modelName = null, $attributes = array(), $model = null) {
        if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
            return $this->delete();
        }
        $upload = isset($_FILES[$this->options['param_name']]) ?
            $_FILES[$this->options['param_name']] : null;
        $info = array();

        if ($upload && is_array($upload['tmp_name'])){
            foreach ($upload['tmp_name'] as $index => $value) {
                $file = $this->handle_file_upload(
                    $upload['tmp_name'][$index],
                    isset($_SERVER['HTTP_X_FILE_NAME']) ?
                        $_SERVER['HTTP_X_FILE_NAME'] : 
                        $upload['name'][$index],
                    isset($_SERVER['HTTP_X_FILE_SIZE']) ?
                        $_SERVER['HTTP_X_FILE_SIZE'] : $upload['size'][$index],
                    isset($_SERVER['HTTP_X_FILE_TYPE']) ?
                        $_SERVER['HTTP_X_FILE_TYPE'] : $upload['type'][$index],
                    $upload['error'][$index]
                );
                
	            if(!isset($model) || $model==null){
	            	$model = new $modelName();
	            	$model->setAttributes($attributes, false);
				} elseif(isset($model->file)) {
					$filename = $this->options['upload_dir'].$model->file;
					if(is_file($filename)) unlink($filename);
				}
				if (!isset($file->error)) {
    	            $model->file = $file->name;
    				$parts = explode('.', $model->file);
    	            if(count($parts)>1) $model->ext = end($parts);
    				else $model->ext = '';
                       $model->{$this->options['post_title']} = isset($_POST[$this->options['post_title']][$index]) ? $_POST[$this->options['post_title']][$index] : $file->name;
    	               $model->{$this->options['post_description']} = isset($_POST[$this->options['post_description']][$index]) ? $_POST[$this->options['post_description']][$index] : $file->name;
                    $model->date_add=date('Y-m-d H:i:s',time());
                    
                    
    	            $model->save(); 
                }
	            
	            $src = Yii::app()->baseUrl.'/images/noformat.png';
				foreach($this->ext as $k => $v){
					if(in_array($model->ext, $v)){
						if($k == 'IMG'){
							$src = $this->options['upload_url'].$model->file;
						}else{
							$src = Yii::app()->baseUrl.'/images/'.$k;
						}
					}
				}
        		
				if($file && is_object($file) && isset($file->url)){
					$file->url = $this->getVersion($file->url, '_org');
					$file->delete_url = $this->options['upload_action'].'&file='.$model->id;
	        		$file->thumbnail_url = $this->getVersion($src, '_thumb');
		            $file->name = (isset($model->{$this->options['post_title']}) ? $model->{$this->options['post_title']} : $this->getVersion($model->file, '_org'));
				}
	            $info[] = $file;
            }
        } elseif ($upload || isset($_SERVER['HTTP_X_FILE_NAME'])) {
        	$index=0;
            $file = $this->handle_file_upload(
                isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
                isset($_SERVER['HTTP_X_FILE_NAME']) ?
                    $_SERVER['HTTP_X_FILE_NAME'] : (isset($upload['name']) ?
                    $upload['name'] : null),
                isset($_SERVER['HTTP_X_FILE_SIZE']) ?
                    $_SERVER['HTTP_X_FILE_SIZE'] : (isset($upload['size']) ?
                        $upload['size'] : null),
                isset($_SERVER['HTTP_X_FILE_TYPE']) ?
                    $_SERVER['HTTP_X_FILE_TYPE'] : (isset($upload['type']) ?
                        $upload['type'] : null),
                isset($upload['error']) ? $upload['error'] : null
            );
            
            if(!isset($model) || $model==null){
            	$model = new $modelName();
            	$model->attributes = $attributes;
			}
            if (!isset($file->error)) {
    	        $model->file = $file->name;
    			$parts = explode('.', $file->name);
    	        if(count($parts)>1) $model->ext = end($parts);
    			else $model->ext = '';
    	        $model->title = isset($_POST['title'][$index]) ? $_POST['title'][$index] : $file->name;
    	        $model->save();
	        }
            $src = Yii::app()->baseUrl.'/images/noformat.png';
			foreach(Yii::app()->params['ext_img'] as $k => $v){
				if(in_array($model->ext, $v)){
					if($k == 'IMG'){
						$src = $this->options['upload_url'].$model->file;
					}else{
						$src = Yii::app()->baseUrl.'/images/'.$k;
					}
				}
			}
        		
       		if($file){
	        	$file->thumbnail_url = $this->getVersion($src, '_org');
		        $file->name = (isset($model->{$this->options['post_title']}) ? $model->{$this->options['post_title']} : '');
			}
            $info[] = $file;
        }
        header('Vary: Accept');
        $json = json_encode($info);
        $redirect = isset($_REQUEST['redirect']) ?
            stripslashes($_REQUEST['redirect']) : null;
        if ($redirect) {
            header('Location: '.sprintf($redirect, rawurlencode($json)));
            return;
        }
        if (isset($_SERVER['HTTP_ACCEPT']) &&
            (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)) {
            header('Content-type: application/json');
        } else {
            header('Content-type: text/plain');
        }
        echo $json;
    }
    
    public function delete($model = null) {
        $file_name = isset($_REQUEST['file']) ? basename(stripslashes($_REQUEST['file'])) : null;
        if($model){
        	$file_name = $model->file;
	        $file_path = $this->options['upload_dir'].$file_name;
	        $file_org = $this->options['upload_dir'].$this->getVersion($file_name, '_org');
	        $success = is_file($file_org) && $file_name[0] !== '.' && unlink($file_org);
	        if ($success) {
	            foreach($this->options['image_versions'] as $version => $options) {
	                $file = $this->options['upload_dir'].$this->getVersion($file_name, $version);
	                if (is_file($file)) {
	                    unlink($file);
	                }
	            }
        		$model->delete();
	        }
	        
        	$success = true;
        }else{
	        $file_path = $this->options['upload_dir'].$file_name;
	        $success = is_file($file_path) && $file_name[0] !== '.' && unlink($file_path);
	        if ($success) {
	        	
	        	$file = $this->options['upload_dir'].$this->getVersion($file_name, '_org');
	            foreach($this->options['image_versions'] as $version => $options) {
	                $file = $this->options['upload_dir'].$this->getVersion($file_name, $version);
	                if (is_file($file)) {
	                    unlink($file);
	                }
	            }
	        }
        }
        header('Content-type: application/json');
        echo json_encode($success);
    }
}