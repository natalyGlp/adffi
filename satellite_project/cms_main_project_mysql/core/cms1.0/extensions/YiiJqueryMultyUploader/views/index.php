<form id="fileupload" action="<?php echo $this->options['upload_action'];?>" method="POST" enctype="multipart/form-data">
    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <?php if(!isset($this->options['uploadTemplate'])){ ?>
	    <div class="row fileupload-buttonbar">
	        <div class="span7">
	            <!-- The fileinput-button span is used to style the file input field as button -->
	            <span class="btn btn-success fileinput-button">
	                <i class="icon-plus icon-white"></i>
	                <span><? echo t('Choise files'); ?></span>
	                <input type="file" name="files[]" multiple>
	            </span>
	            <button type="submit" class="btn btn-primary start">
	                <i class="icon-upload icon-white"></i>
	                <span><? echo t('Start upload'); ?></span>
	            </button>
	            <button type="reset" class="btn btn-warning cancel">
	                <i class="icon-ban-circle icon-white"></i>
	                <span><? echo t('Cancel upload'); ?></span>
	            </button>
	            <button type="button" class="btn btn-danger delete">
	                <i class="icon-trash icon-white"></i>
	                <span><? echo t('Delete');  ?></span>
	            </button>
	            <input type="checkbox" class="toggle">
	        </div>
	        <div class="span5">
	            <!-- The global progress bar -->
	            <div class="progress progress-success progress-striped active fade">
	                <div class="bar" style="width:0%;"></div>
	            </div>
	        </div>
	    </div>
	    <!-- The loading indicator is shown during image processing -->
	    <div class="fileupload-loading"></div>
	    <br>
	    <!-- The table listing the files available for upload/download -->
	    <table class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
	<?php } else echo $this->options['uploadTemplate'];?>
</form>

<!-- modal-gallery is the modal dialog used for the image gallery -->
<div id="modal-gallery" class="modal modal-gallery hide fade">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h3 class="modal-title"></h3>
    </div>
    <div class="modal-body"><div class="modal-image"></div></div>
    <div class="modal-footer">
        <a class="btn btn-primary modal-next">
            <span>След.</span>
            <i class="icon-arrow-right icon-white"></i>
        </a>
        <a class="btn btn-info modal-prev">
            <i class="icon-arrow-left icon-white"></i>
            <span>Пред.</span>
        </a>
        <a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
            <i class="icon-play icon-white"></i>
            <span>Слайдшоу</span>
        </a>
        <a class="btn modal-download" target="_blank">
            <i class="icon-download"></i>
            <span>Скачать</span>
        </a>
    </div>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    	<?php if(!isset($this->options['uploadFileTemplate']) || $this->options['uploadFileTemplate']==NULL){ ?>
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><label>Название:</label></td>
        <td class="name"><input type="text" class="css3input" name="title[{%=i%}]" value="{%=file.name%}"/></td>
        <td class="name"><label>Описание:</label></td>
		<td class="name"><input type="text" class="css3input" name="description[{%=i%}]" value=""/></td>
        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td>
                <div class="progress progress-success progress-striped active"><div class="bar" style="width:0%;"></div></div>
            </td>
            <td class="start">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary">
                    <i class="icon-upload icon-white"></i>
                    <span>{%=locale.fileupload.start%}</span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td class="cancel">{% if (!i) { %}
            <button class="btn btn-warning">
                <i class="icon-ban-circle icon-white"></i>
                <span>{%=locale.fileupload.cancel%}</span>
            </button>
        {% } %}</td>
       	<?php } else echo $this->options['uploadFileTemplate']; ?>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    	<?php if(!isset($this->options['downloadFileTemplate']) || $this->options['downloadFileTemplate']==NULL){ ?>
        {% if (file.error) { %}
            <td></td>
            <td class="name"><span>{%=file.name%}</span></td>
            <td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
        {% } else { %}
            <td class="preview">{% if (file.thumbnail_url) { %}
                <a target="_blank" href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img style="max-width:50px; max-height:50px;" src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
            <td class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
            </td>
            <td class="name">
                <td class="name"> <a href="/project/backend/bephotos/update/{%=file.id%}" title="Редактировать">Редактировать</a> </td>
            </td>
            <td colspan="2"></td>
        {% } %}
        <td class="delete">
            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}">
                <i class="icon-trash icon-white"></i>
                <span>{%=locale.fileupload.destroy%}</span>
            </button>
            <input type="checkbox" name="delete" value="1">
        </td>
        <?php } else echo $this->options['downloadFileTemplate']; ?>
    </tr>
{% } %}
</script>