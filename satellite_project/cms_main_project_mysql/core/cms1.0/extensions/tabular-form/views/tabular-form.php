<?php
/**
 * @var string $id
 * @var string $addButtonLabel
 * @var string $content
 * @var array $htmlOptions
 */
?>

<?= CHtml::openTag('div', $htmlOptions); ?>
<div class="tabular-form js-tabular-form">
    <?= $content ?>
</div>
<a href="#" class="btn btn-success js-tabular-form-add"><?= $addButtonLabel; ?></a>
<?= CHtml::closeTag('div'); ?>
