<?php
/**
 * Command for auto email notification
 *
 * yiic emailNotification --when=[weekly|daily]
 *
 * @version 0.1
 * @copyright Copyright &copy; 2014 Sviatoslav Danylenko
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @license MIT ({@link http://opensource.org/licenses/MIT})
 * @link https://github.com/SleepWalker/yii-notify
 */

Yii::import('cms.models.order.*');
Yii::import('application.modules.shop.models.*');
Yii::import('cms.extensions.yii-notify.models.*');

class EmailNotificationCommand extends CConsoleCommand
{
    /**
     * @var string $when 'daily' or 'weekly'
     */
    public $when = 'daily';

    public function actionIndex()
    {
        Yii::log('Starting email notification command', 'info', get_class($this));

        $users = User::model()->findAll();
        if ($this->when == 'daily') {
            $category = 'shopObjectStatus_' . CatalogObject::STATUS_REORDER;
        } else {
            $category = 'shopObjectStatus_' . CatalogObject::STATUS_ORDERED;
        }

        $notifies = Notify::model()->findAllByAttributes(array('category' => $category));

        foreach ($users as $user) {
            foreach ($notifies as $notify) {
                $user->sendEmail(Yii::app()->name . ': ' . $notify->title, CHtml::link($notify->message, $notify->url));
            }
        }

        return 0;
    }
}
