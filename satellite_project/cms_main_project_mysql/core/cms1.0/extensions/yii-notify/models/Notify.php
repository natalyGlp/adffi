<?php

/**
 * This is the model class for table "notify".
 *
 * The followings are the available columns in table 'notify':
 * @property string $id
 * @property string $title
 * @property string $message
 * @property string $url
 * @property string $recipient
 * @property string $category
 * @property string $placement
 * @property string $level
 * @property string $counter
 * @property string $flash
 * @property string $enabled
 * @property string $date_created
 * @property string $date_from
 * @property string $date_to
 *
 * The followings are the available model relations:
 * @property NotifyReads[] $notifyReads
 *
 * @version 0.1
 * @copyright Copyright &copy; 2014 Sviatoslav Danylenko
 * @author Sviatoslav Danylenko <dev@udf.su> 
 * @license MIT ({@link http://opensource.org/licenses/MIT})
 * @link https://github.com/SleepWalker/yii-notify
 */
class Notify extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Notify the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notify';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category', 'required'),
			array('recipient', 'length', 'max'=>255),
			array('category, placement, level', 'length', 'max'=>32),
			array('counter', 'length', 'max'=>11),
			array('flash, enabled', 'numerical', 'integerOnly'=>true),
			array('title, message, url, date_from, date_to', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, message, recipient, category, placement, level, counter, date_created, date_from, date_to, flash, enabled, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reads' => array(self::HAS_MANY, 'NotifyReads', 'notify_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'message' => 'Message',
			'url' => 'Url',
			'recipient' => 'Recipient',
			'category' => 'Category',
			'placement' => 'Placement',
			'level' => 'Level',
			'counter' => 'Counter',
			'date_created' => 'Date Created',
			'date_from' => 'Date From',
			'date_to' => 'Date To',
			'flash' => 'Flash',
			'enabled' => 'Enabled',
		);
	}

	protected function beforeSave()
	{
		// if(!empty($this->to))
		// 	$this->to = new CDbExpression('NULL');
		// if(!empty($this->from))
		// 	$this->from = new CDbExpression('NULL');
		if($this->enabled !== 0)
			$this->enabled = 1;

		if(!empty($this->url) && strpos($this->url, 'http') !== 0)
			$this->url = Yii::app()->createAbsoluteUrl('/').'/'.ltrim($this->url, '/');

		return parent::beforeSave();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('>url',$this->url,true);
		$criteria->compare('recipient',$this->recipient,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('placement',$this->placement,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('flash',$this->flash,true);
		$criteria->compare('enabled',$this->enabled,true);
		$criteria->compare('counter',$this->counter,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_from',$this->date_from,true);
		$criteria->compare('date_to',$this->date_to,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}