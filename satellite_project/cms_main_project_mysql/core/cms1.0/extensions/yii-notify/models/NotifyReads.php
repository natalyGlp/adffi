<?php

/**
 * This is the model class for table "notify_reads".
 *
 * The followings are the available columns in table 'notify_reads':
 * @property string $id
 * @property string $notify_id
 * @property string $user_id
 * @property integer $readed
 *
 * The followings are the available model relations:
 * @property Notify $notify
 *
 * @version 0.1
 * @copyright Copyright &copy; 2014 Sviatoslav Danylenko
 * @author Sviatoslav Danylenko <dev@udf.su> 
 * @license MIT ({@link http://opensource.org/licenses/MIT})
 * @link https://github.com/SleepWalker/yii-notify
 */
class NotifyReads extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NotifyReads the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notify_reads';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('notify_id, user_id', 'required'),
			array('readed', 'numerical', 'integerOnly'=>true),
			array('notify_id, user_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, notify_id, user_id, readed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'notify' => array(self::BELONGS_TO, 'Notify', 'notify_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'notify_id' => 'Notify',
			'user_id' => 'User',
			'readed' => 'Readed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('notify_id',$this->notify_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('readed',$this->readed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}