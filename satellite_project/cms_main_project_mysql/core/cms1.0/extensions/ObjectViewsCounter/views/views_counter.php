<?php
/**
 * @var boolean $display_only
 * @var integer $viewsCount
 * @var integer $uniqueViewsCount
 * @var string $url
 */
if ($this->renderCounter)
{
?>
<div id="views-counter-<?php echo get_class($this->model) . '-' . $this->model->primaryKey?>">
	<span><?php echo Yii::t("PcViewsCounterModule.general", "Views") . ": "?></span><span class="<?php echo "views-counter-" . get_class($this->model) . '-' . $this->model->primaryKey?>"><?php echo $viewsCount ?></span>
</div>
<?php
}

// Ajaxly request the resource that will do the actual 'expression add' action on server side, if requested to:
if (!$display_only) {
	// the inline script itself:
	$jquery_selector = "span.views-counter-" . get_class($this->model) . '-' . $this->model->primaryKey;
	$scr = '
$(function () {
	jQuery.getJSON(
		"'.$url.'?' . ViewsCountWidget::ADD_IMPRESSION_PARAMNAME . '=true",
		function (data) {
			if (data.status == "success" && $("'.$jquery_selector.'").length > 0) {
				$("'.$jquery_selector.'").html(data.count);
			}
		}
	)
});';
	// make a new impression and update counter:
	Yii::app()->clientScript->registerCoreScript("jquery");
	Yii::app()->clientScript->registerScript(
		"views-counter-" . get_class($this->model) . '-' . $this->model->primaryKey,
		$scr,
		CClientScript::POS_END
	);
}
?>
