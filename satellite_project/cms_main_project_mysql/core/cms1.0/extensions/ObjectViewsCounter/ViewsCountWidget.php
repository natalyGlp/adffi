<?php
/**
 * ViewsCountWidget.php
 * Created on 02 07 2012 (4:00 PM)
 *
 * @modified Sviatoslav Danylenko <dev@udf.su>
 * TODO: этот виджет требует рефакторинга я просто на скорую руку немного адаптировал его под наши нужды
 * TODO: object_hits только считаются. Виджеты под них я не переделывал!
 */
 
 
// модель для кеширования пользователей и просмотренных ими страниц
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'ObjectViewsBookeeping.php');
class ViewsCountWidget extends CWidget {
	/* param name that will be sent via AJAX when a content impression is initiated from client side. Acronym of yet-another-content-impression :) */
	const ADD_IMPRESSION_PARAMNAME = "YACE";
	const PERSISTENT_STORAGE_KEY_PREFIX = "ViewsCountWidget_saved_config__";
	const COOKIE_NAME = "ViewsCount";
	const COOKIE_EXPIRATION_LIFETIME = 31536000; // 1 year
	const COOKIE_DEFAULT_IMPRESSION_LIFETIME = 216000; // 60 days - 2 months.
	const COOKIE_ENCRYPT_KEY = "update this or your user's viewing history could be hijacked by a third party!";

	/* @var bool whether the widget should be run in 'display mode only', and not record a view count, or not. */
	public $dontCount = false;
	
	public $renderCounter = false;

	/* @var PageViewsStat */
	public $statsRecord;

	/**
	 * @var string $requestUrl url на который отправлять запрос для обновления счетчика
	 */
	public $requestUrl = '';
	
	public $model;

	/* @var bool we use it internally to sync all on whether the widget was successfully initialized or not */
	private $_isInitialized = false;

	/* @var string */
	private $_clientIpAddress;

	/* @var string, actually enum: "render" or "add impression" modes */
	private $_mainMode = 'render';
	
	

	/**
	 * init method
	 */
	public function init() {
		// verify that class name + id are set. we cannot work without them
		if (!$this->model) {
			Yii::log("Error - cannot really work with no model instance", CLogger::LEVEL_WARNING, __METHOD__);
			return;
		}

		// get client ip address - we need it in various location down this request processing
		$this->_clientIpAddress = ip(); //Yii::app()->geoip->getRemoteIpAddress();

		// check if a 'stats' record exists for this model. If not, create it.
		$this->statsRecord = $this->model;
		if (!$this->statsRecord) {
			// failure occurred during loading of the stats record. log was done in the method used. just abort...
			return;
		}


		// determine if this is an initial load request or a 'advance counter' request.
		if (Yii::app()->request->isAjaxRequest && (Yii::app()->request->getParam(self::ADD_IMPRESSION_PARAMNAME))) {

			// flag mode for this widget instance as 'add impression' mode.
			$this->_mainMode = 'add impression';
		}
		
		$this->_isInitialized = true;
	}

	/**
	 * run method
	 */
	public function run() {
		// first, are we initialized ok?
		if (!$this->_isInitialized) {
			// do nothing...
			Yii::log("Error - aborting operation as this widget is determined to be 'non-initialized'. Please examine error message logged above.", CLogger::LEVEL_WARNING, __METHOD__);
			return;
		}

		// two main modes for us for today: 'render' mode and 'add impression' mode. use code for right mode.
		if ($this->_mainMode == 'render') {
			/*
			 * -------------
			 * render mode
			 * -------------
			 */
			// impression url should be current URL
			$this->render('views_counter', array(
					'display_only' => $this->dontCount,
					'viewsCount' => $this->model->object_uniq_hits,
					'uniqueViewsCount' => $this->model->object_uniq_hits,
					'url' => $this->requestUrl, 
				)
			);
		}
		else {    		
    		while(@ob_end_clean()); // Clear all levels of the output buffer

    		// При каждом запросе инкрементируем хиты
    		$this->statsRecord->object_hits++;
    		$saved = false;

			// Now handle unique impressions.
			// check if the user already viewed this content by checking in the impressions cookie
			// ** Important note **: existence of a cookie is checked for BOTH guest and authenticated users.
			if(!$this->_isContentViewedByCookie(get_class($this->model), $this->model->primaryKey)) {
				// content not marked as 'viewed' in user's cookie. proceed to check DB records:
				if (!Yii::app()->user->isGuest) {
					// authenticated user.
					// search for existence ObjectViewsBookeeping record for this user (based on user id or ip address, and $this->statsRecord->primaryKey)
					$user_id = Yii::app()->user->id;
					$bookeeping_record = ObjectViewsBookeeping::model()->findByIpOrUId($this->_clientIpAddress, $user_id, array(
					    'condition' => 'object_id = ' . $this->statsRecord->primaryKey,
					));
				}
				else {
					// this IS a guest user (but without an impression cookie).
					// add cookie for this guest user unique impression whether he has a record or not
					// search for a bookeeping record just per its IP address
					$bookeeping_record = ObjectViewsBookeeping::model()->findByAttributes(array('ip_address' => $this->_clientIpAddress, 'object_id' => $this->statsRecord->primaryKey));
					$user_id = false;
				}

				if (!$bookeeping_record) {
					// no (unique) impression record was found for current user. Make such a record.
					$ret = $this->_createImpressionBookeepingRecord($user_id, $this->_clientIpAddress, $this->statsRecord->primaryKey);

		            // update the stats record with plus one unique count.
		            $saved = $this->_updateStatsRecordCounter();
				}
				
				$this->_addImpressionToCookie(get_class($this->model), $this->model->primaryKey);
			}

			if(!$saved)
				$this->statsRecord->save(); // сейвим информацию о хитах

			// oki doki. all done! now just return a 'success!' notification to the user, with updated counter (uniq/non - as requested from this instance).
			echo CJSON::encode(array('status' => 'success', 'count' => $this->statsRecord->object_hits, 'uniqCount' => $this->statsRecord->object_uniq_hits));
			Yii::app()->end();
		}
	}

	/**
	 * Creates an ObjectViewsBookeeping record.
	 *
	 * @param $user_id
	 * @param $ip_address
	 * @param $object_id
	 * @return bool noting success or not.
	 */
	private function _createImpressionBookeepingRecord($user_id, $ip_address, $object_id) {
		$bookeeping_record = new ObjectViewsBookeeping();
		if ($user_id !== false) {
			$bookeeping_record->user_id = $user_id;
		}
		$bookeeping_record->ip_address = $ip_address;
		$bookeeping_record->object_id = $object_id;
		try {
			/* there could be race conditions in which the save() will fail. Just log this incident since it means that the record was already created.
			 * At least, that's the only reason I can thing of now. */
			return $bookeeping_record->save();
		}
		catch (Exception $e) {
			// check if such a record was born right under our noses:
			$bookeeping_record = ObjectViewsBookeeping::model()->findByIpOrUId($this->_clientIpAddress, $bookeeping_record->user_id, array(
			    'condition' => 'object_id = ' . $this->statsRecord->primaryKey,
			));

			$num = ObjectViewsBookeeping::model()->count($criteria);
			if ($num == 0) {
				Yii::log("Hmm... . Failed to create a ObjectViewsBookeeping record yet failure is not due to a race-condition-borned-record. Counting number of such" .
						" records even AFTER failure resulted in 0. Please check the reason following this sentence. Model class name + id = " .
						get_class($this->model) . ", " . $this->model->primaryKey . ". Exception's message: " . $e->getMessage(), CLogger::LEVEL_ERROR, __METHOD__);
			}
			else {
				Yii::log("Hmm... . Failed to create a ObjectViewsBookeeping record probably due to DB constraint - when checked before creation none existed yet now " .
						"$num of records exists. Check this out... . Model class name + id = " . get_class($this->model) . ", " .
						$this->model->primaryKey . ". Exception's message: " . $e->getMessage(), CLogger::LEVEL_ERROR, __METHOD__);
			}
			return false;
		}
	}

	/**
	 * @param string $model_name
	 * @param int $model_id
	 * @return bool flagging whether this user have seen the content, or not, as determined by cookie content
	 */
	private function _isContentViewedByCookie($model_name, $model_id) { return false;
		/* @var CHttpCookie $cookie */
		$cookie = (isset(Yii::app()->request->cookies[self::COOKIE_NAME])) ? Yii::app()->request->cookies[self::COOKIE_NAME] : null;
		if (!isset($cookie)) {
			// no cookie at all so user has not seen this content
			return false;
		}

		$data = Yii::app()->securityManager->decrypt($cookie->value, self::COOKIE_ENCRYPT_KEY);
		$data = unserialize($data);
		if (array_key_exists($model_name . '-' . $model_id, $data)) {
			$viewd_on = $data[$model_name . '-' . $model_id];
			if (($viewd_on + self::COOKIE_DEFAULT_IMPRESSION_LIFETIME) < time()) {
				// it was viewed but a long time ago to be accounted as not viewed.
				// clear this value from the cookie:
				unset($data[$model_name . '-' . $model_id]);
				$cookie->value = Yii::app()->securityManager->encrypt(serialize($data), self::COOKIE_ENCRYPT_KEY);
				Yii::app()->request->cookies[self::COOKIE_NAME] = $cookie;
				return false;
			}
			else {
				// cookie has this impression record and its not expired yet
				return true;
			}
		}
		// no impression record in the cookie, for the given model.
		return false;
	}

	/**
	 * Add an impression to 'impressions cookie'
	 *
	 * @param string $model_name
	 * @param int $model_id
	 *
	 * @return bool nothing success/failure
	 */
	private function _addImpressionToCookie($model_name, $model_id) {
		// first, do we have such a cookie? if not, create one
		if (!isset(Yii::app()->request->cookies[self::COOKIE_NAME])) {
			// create it with an empty array. it is filled, serialized and encrypted below, before its saved.
			$cookie = new CHttpCookie(self::COOKIE_NAME, array());
			$cookie->expire = time() + self::COOKIE_EXPIRATION_LIFETIME;
		}
		else {
			// cookie exists, decrypt its content
			$cookie = Yii::app()->request->cookies[self::COOKIE_NAME];
			$cookie->value = Yii::app()->securityManager->decrypt($cookie->value, self::COOKIE_ENCRYPT_KEY);
			$cookie->value = unserialize($cookie->value);
		}
		// add the information to the cookie and save it
		$cookie->value[$model_name . "-" . $model_id] = time();
		$cookie->value = Yii::app()->securityManager->encrypt(serialize($cookie->value), self::COOKIE_ENCRYPT_KEY);
		Yii::app()->request->cookies[self::COOKIE_NAME] = $cookie;
	}

	//$val = Yii::app()->securityManager->encrypt(serialize($data_array), self::COOKIE_ENCRYPT_KEY);
	// private function _encryptCookieValue($value) // $value is mixed - can be anything that is serializable

	/**
	 * Updates the stats record in a 'safe' manner - if record was updated by someone else, we reload it, update its
	 * counter and attempt to update it again (this is repeated 20 times by default)
	 *
	 * @param string $counter_attr the counter attribute name (count_non_uniq or count_uniq)
	 * @param int $num_of_impressions the count/amount to update the counter with.
	 */
	private function _updateStatsRecordCounter() {
		// attempt 20 times to update record. On heavily loaded scenarios YMMV... (you might need to increase this figure).
		// note that on each attempt, if the previous one failed, we load the stats record from the db and advance the 'updated' record
		// since we don't wish to overrun the just-saved-record but rather update it. right?... :)
		$this->statsRecord->object_uniq_hits++;
		for ($i = 0; $i < 20; $i++) {
			// we update the statsRecord with try-catch to catch stale object exceptions that might occur if someone else already updated this record
			try {
				return $this->statsRecord->save();
				// if the above failed - an exception will be thrown. Else, this is considered a success
				break;
			}
			catch (PcStaleObjectErrorException $e) {
				// oops, someone else updated the record just a tiny miny minute before we attempted to save it (but AFTER we have loaded it from the DB).
				// so: load the 'updated' record from the db, advance its needed counter (always) and another attempt will be made in the loop enclosing this
				// try-catch block.
				/* @var PageViewsStat $new_stats_record */
				Yii::log("Stale object exception caught when attempted updating PageviewsStat record (attempt #$i). Will attempt to load again, update "
						. "its $counter_attr counter, and save again.", CLogger::LEVEL_PROFILE, __METHOD__);

				return $this->statsRecord->save();
			}
		}
	}
}
