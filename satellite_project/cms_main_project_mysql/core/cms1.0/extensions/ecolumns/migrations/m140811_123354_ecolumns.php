<?php

class m140811_123354_ecolumns extends CDbMigration
{
	public function up()
	{
		$this->createTable('{{columns}}', array(
			'id' => 'VARCHAR(100) NOT NULL',
			'data' => 'TEXT NULL DEFAULT NULL',
			'PRIMARY KEY (`id`)'
			));
	}

	public function down()
	{
		$this->dropTable('{{columns}}');
	}
}