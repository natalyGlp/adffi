<?php

class UniquePairValidator extends CUniqueValidator
{
    public $pairAttribute;

    protected function validateAttribute($object, $attribute)
    {
        if (!is_string($this->pairAttribute) || empty($this->pairAttribute)) {
            throw new CException('UniquePairValidator::with should be non empty string');
        }
        
        $this->criteria = array(
            'condition' => $this->pairAttribute . ' = :' . $this->pairAttribute,
            'params' => array(
                ':' . $this->pairAttribute => $object->{$this->pairAttribute},
            ),
        );

        parent::validateAttribute($object, $attribute);
    }
}
