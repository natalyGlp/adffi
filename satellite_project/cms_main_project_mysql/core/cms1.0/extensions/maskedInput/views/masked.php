<?php
/* @var $model */
/* @var $attribute */
/* @var $value */
/* @var $model $htmlOptions */

$assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'assets', false, -1, YII_DEBUG);
$cs = Yii::app()->clientScript;
$cs->registerScriptFile($assetsUrl . '/js/inputmask.js', CClientScript::POS_HEAD);
$cs->registerScriptFile($assetsUrl . '/js/bind-first.js', CClientScript::POS_HEAD);
$cs->registerScriptFile($assetsUrl . '/js/jquery.inputmask-multi.js', CClientScript::POS_HEAD);
$cs->registerScript('phoneCheck', '
                $(".js-masked-input").each(function() {
                    $(this).inputmasks({
                        inputmask: {
                            definitions: {
                                "#": {
                                    validator: "[0-9]",
                                    cardinality: 1
                                }
                            },
                            //clearIncomplete: true,
                            showMaskOnHover: false,
                            autoUnmask: true
                        },
                        match: /[0-9]/,
                        replace: "#",
                        list: $.masksSort($.masksLoad("' . $assetsUrl . '/js/phone-codes.json"), ["#"], /[0-9]|#/, "mask"),
                        listKey: "mask",
                        onMaskChange: function(maskObj, completed) {
                            $(this).attr("placeholder", $(this).inputmask("getemptymask"));
                        }
                    });
                });
            ',
    CClientScript::POS_END
);
?>

<?php if(isset($model)): ?>
    <?= CHtml::activeTextField($model, $attribute, $htmlOptions); ?>
<?php else: ?>
    <?= CHtml::textField($name, $value, $htmlOptions); ?>
<?php endif; ?>
