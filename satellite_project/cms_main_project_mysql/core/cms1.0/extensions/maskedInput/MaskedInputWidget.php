<?php
class MaskedInputWidget extends CInputWidget
{
    public function init()
    {
        $class = isset($this->htmlOptions['class']) ? $this->htmlOptions['class'] : '';
        $this->htmlOptions['class'] = !empty($class) ? $class.' js-masked-input' : 'js-masked-input';
    }

    public function run()
    {
        $this->render('masked', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'name' => $this->name,
            'value' => $this->value,
            'htmlOptions' => $this->htmlOptions,
        ));
    }
}
