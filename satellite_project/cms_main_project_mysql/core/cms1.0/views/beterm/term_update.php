<?php
$this->pageTitle = _t('Update Term');
$this->pageHint = _t('Here you can update information for current Term');
$this->widget('cms.widgets.object.TermCreateUpdateWidget', array());
