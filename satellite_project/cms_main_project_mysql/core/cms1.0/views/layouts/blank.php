<!DOCTYPE html>
<html>
    <head>
    	<?php
            $this->renderPartial('cms.views.layouts.header');
        ?>
    </head>
    <body>
        <div class="container" id="page" style="text-align:center">
            <?php echo $content; ?>
        </div><!-- page -->
    </body>
</html>