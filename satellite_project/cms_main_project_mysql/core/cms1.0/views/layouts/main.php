<!DOCTYPE html>
<html>
    <head>
        <?php
        $this->renderPartial('cms.views.layouts.header');
        ?>
    </head>
    <body>
        <div id="maincontainer">
            <header>
                <?php
                $translate = Yii::app()->translate;
                // добываем новые мыла с частотой в 15 мин
                $newEmailCount = EmailLog::model()->cache(60*15)->count(array(
                    'condition'=> 'status = ' . EmailLog::STATUS_UNDONE,
                ));

                $userAvatar = !empty(Yii::app()->user->model->avatar) ? GxcHelpers::getUserAvatar(20, Yii::app()->user->model->avatar, '') : GxcHelpers::assetUrl('img/user_avatar.png', 'gebo');

                if (!function_exists('getLangIconId')) {
                    function getLangIconId($langId)
                    {
                        $languageCssClassMap = array(
                            'en' => 'us',
                            'uk' => 'ua'
                            );

                        $iconId = strtolower($langId);
                        $iconId = strpos($iconId, '_') ? substr($iconId, 3) : $iconId;
                        return isset($languageCssClassMap[$iconId]) ? $languageCssClassMap[$iconId] : $iconId;
                    }
                }

                $curLang = getLangIconId($translate->getLanguage());
                $acceptedLanguages = $translate->acceptedLanguages;

                $languageMenu = array();
                foreach ($acceptedLanguages as $languageId => $language) {
                    $iconId = getLangIconId($languageId);
                    $languageMenu[] = array(
                        'label'=>'<i class="flag-'.$iconId.'"></i> '.$language,
                        'url'=>'?lang='.Yii::app()->translate->getLanguageUrl($languageId),
                    );
                }

                $this->widget('bootstrap.widgets.TbNavbar', array(
                    'brand' => '<img src="'.GxcHelpers::assetUrl('images/logo_small.png', 'backend').'"  title="'._t('Dashboard').'" class="ttip_b" style="height:40px;" />',
                    'fixed' => 'top',
                    'fluid' => true,
                    'items' => array(
                        array(
                            'class' => 'bootstrap.widgets.TbMenu',
                            'htmlOptions'=>array('class'=>'pull-right'),
                            'encodeLabel' => false,
                            'items' => array(
                                '---',
                                array('label' => '<i class="flag-'.$curLang.'"></i>', 'url' => '#',
                                    'items' => $languageMenu,
                                ),
                                '---',
                                array('label'=>'<img src="' . $userAvatar .'" alt="" class="user_avatar" /> '.Yii::app()->user->name, 'url'=>'#',
                                    'items'=>array(
                                        array('label'=>_t('Settings'), 'url'=>array('/beuser/updatesettings')),
                                        array('label'=>_t('Change Password'), 'url'=>array('/beuser/changepass')),
                                        '---',
                                        array('label'=>_t('Log Out'), 'url'=>array('/besite/logout')),
                                    )
                                ),
                            )
                        ),
                        '<ul class="nav user_menu pull-right">
                            <li class="hidden-phone hidden-tablet">
                                <div class="nb_boxes clearfix">
                                    <a title="'._t('New messages').'" href="'.$this->createUrl('/beemail').'" class="label ttip_b">'.$newEmailCount.' <i class="splashy-mail_light"></i></a>
                                </div>
                            </li>
                        </ul>'
                    )
                ));
                ?>
                <div class="clear"></div>
            </header>
            <div id="loading_layer" style="display:none"><img src="<?= GxcHelpers::assetUrl('img/ajax_loader.gif', 'gebo'); ?>" alt="" /></div>
            <div class="style_switcher">
                <div class="sepH_c">
                    <p>Colors:</p>
                    <div class="clearfix">
                        <a href="javascript:void(0)" class="style_item jQclr blue_theme style_active" title="blue">blue</a>
                        <a href="javascript:void(0)" class="style_item jQclr dark_theme" title="dark">dark</a>
                        <a href="javascript:void(0)" class="style_item jQclr green_theme" title="green">green</a>
                        <a href="javascript:void(0)" class="style_item jQclr brown_theme" title="brown">brown</a>
                        <a href="javascript:void(0)" class="style_item jQclr eastern_blue_theme" title="eastern_blue">eastern_blue</a>
                        <a href="javascript:void(0)" class="style_item jQclr tamarillo_theme" title="tamarillo">tamarillo</a>
                    </div>
                </div>
                <div class="sepH_c">
                    <p>Backgrounds:</p>
                    <div class="clearfix">
                        <span class="style_item jQptrn style_active ptrn_def ssw_ptrn_def" title="">ptrn_def</span>
                        <span class="ssw_ptrn_a style_item jQptrn" title="ptrn_a">ptrn_a</span>
                        <span class="ssw_ptrn_b style_item jQptrn" title="ptrn_b">ptrn_b</span>
                        <span class="ssw_ptrn_c style_item jQptrn" title="ptrn_c">ptrn_c</span>
                        <span class="ssw_ptrn_d style_item jQptrn" title="ptrn_d">ptrn_d</span>
                        <span class="ssw_ptrn_e style_item jQptrn" title="ptrn_e">ptrn_e</span>
                    </div>
                </div>
                <div class="sepH_c">
                    <p>Layout:</p>
                    <div class="clearfix">
                        <label class="radio inline"><input type="radio" name="ssw_layout" id="ssw_layout_fluid" value="" checked="checked"> Fluid</label>
                        <label class="radio inline"><input type="radio" name="ssw_layout" id="ssw_layout_fixed" value="gebo-fixed"> Fixed</label>
                    </div>
                </div>
                <div class="sepH_c">
                    <p>Sidebar position:</p>
                    <div class="clearfix">
                        <label class="radio inline"><input type="radio" name="ssw_sidebar" id="ssw_sidebar_left" value="" checked="checked"> Left</label>
                        <label class="radio inline"><input type="radio" name="ssw_sidebar" id="ssw_sidebar_right" value="sidebar_right"> Right</label>
                    </div>
                </div>

                <div class="gh_button-group">
                    <a href="#" id="saveBtn" class="btn btn-primary btn-mini">Save</a>
                    <a href="#" id="resetDefault" class="btn btn-mini">Reset</a>
                </div>
            </div>
            <?= $content; ?>
        </div>
        <div class="modal hide fade" id="myModal"></div>
        <div id="qtip-rcontainer"></div>
    </body>
</html>
