<?php
/**
 * Шаблон, который используется для рендеринга iframe
 */
?>
<!DOCTYPE html>
<html>
    <head>
    	<?php
            $this->renderPartial('cms.views.layouts.header');
        ?>
    </head>

    <body>
        <div id="loading_layer" style="display:none"><img src="<?= GxcHelpers::assetUrl('img/ajax_loader.gif', 'gebo'); ?>" alt="" /></div>
        <div class="container" id="page" style="padding:10px">
            <?php echo $content; ?>
        </div><!-- page -->
    </body>
</html>