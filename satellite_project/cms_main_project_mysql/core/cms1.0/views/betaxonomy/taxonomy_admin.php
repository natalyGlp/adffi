<?php
$this->pageTitle = _t('Manage Taxonomy');
$this->pageHint = _t('Here you can manage your Taxonomy');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'Taxonomy'
));
