<?php
$this->pageTitle = _t('Update Taxonomy');
$this->pageHint = _t('Here you can update information for current Taxonomy');

$form_create_url = Yii::app()->controller->createUrl('beterm/create', array(
    'embed' => 'iframe',
    'taxonomy' => $id,
    'site' => Yii::app()->controller->site,
));
$form_update_url = Yii::app()->controller->createUrl('beterm/update', array(
    'embed' => 'iframe',
    'taxonomy' => $id,
    'site' => Yii::app()->controller->site,
));
$form_change_order_url = Yii::app()->controller->createUrl('beterm/changeorder', array(
    'site' => Yii::app()->controller->site,
));
$form_delete_url = Yii::app()->controller->createUrl('beterm/delete', array(
    'site' => Yii::app()->controller->site,
));

$this->widget('cms.widgets.object.TaxonomyCreateUpdateWidget', array(
    'form_create_term_url' => $form_create_url,
    'form_update_term_url' => $form_update_url,
    'form_delete_term_url' => $form_delete_url,
    'form_change_order_term_url' => $form_change_order_url,
));
