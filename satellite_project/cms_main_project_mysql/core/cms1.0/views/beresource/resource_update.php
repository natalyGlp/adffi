<?php
$this->pageTitle = _t('Update Resource');
$this->pageHint = _t('Here you can update information for current Resource');
$this->widget('cms.widgets.resource.ResourceUpdateWidget');
