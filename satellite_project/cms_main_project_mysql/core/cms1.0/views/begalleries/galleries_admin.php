<?php
$this->pageTitle = _t('Manage Galleries');
$this->pageHint = _t('Here you can manage your Galleries');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'Galleries'
));
