<div class="wrapper">
<center>
    <button class="btn" onclick="window.location.href='<?php echo Yii::app()->createAbsoluteUrl('/admin/'.$this->module->name.'/add/');?>';"><?php echo Yii::t('core', 'Создать');?></button>
</center><br/>
<?php 
$cond = 't.user_id=?';
$params = array(Yii::app()->user->id);

if(!$model->count($cond, $params)){
	$cond = 't.user_id=? AND t.active=?';
	$params = array(0, 1);
}
$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'gallery-grid',
	'dataProvider' => $model->search($cond, $params),
	'template' => '{items}{summary}{pager}',
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
	'filter' => $model,
	'columns' => array(
		'title',
		'path',
      	array(
        	'name' => 'active',
            'type' => 'raw',
            'value' => 'Yii::app()->controller->statuses[$data->active]',
      		'filter' => Yii::app()->controller->statuses
      	),
      	array(
			'class' => 'ext.myButtonColumn',
            'updateButtonImageUrl' => $this->imagesUrl.'/configure.gif',
          	'deleteButtonImageUrl' => $this->imagesUrl.'/close.png',
            'deleteButtonUrl' => 'Yii::app()->createAbsoluteUrl("/admin/'.$this->module->name.'/$data->'.$model->pkey.'/del/")',
            'updateButtonUrl' => 'Yii::app()->createAbsoluteUrl("/admin/'.$this->module->name.'/$data->'.$model->pkey.'/edit/")',
            'updateButtonVisible' => '(Yii::app()->user->id == $data->user_id || Yii::app()->user->isSuperuser)',
            'deleteButtonVisible' => '(Yii::app()->user->id == $data->user_id || Yii::app()->user->isSuperuser)',
            'template'=>'{photos} {update} {on} {off} {delete}',
            'buttons' => array(
            	'off' => array(
                    'label' => 'Активировать',
                    'imageUrl' => $this->imagesUrl.'/cancel.gif',
                    'visible' => '$data->active == 0 && (Yii::app()->user->id == $data->user_id || Yii::app()->user->isSuperuser)',
                    'url'   => 'Yii::app()->createAbsoluteUrl("/admin/'.$this->module->name.'/$data->'.$model->pkey.'/active/")',
                 ),
                 'on' => array(
                     'label' => 'Деактивировать',
                     'imageUrl' => $this->imagesUrl.'/flag.gif',
                     'visible' => '$data->active == 1 && (Yii::app()->user->id == $data->user_id || Yii::app()->user->isSuperuser)',
                     'url'   => 'Yii::app()->createAbsoluteUrl("/admin/'.$this->module->name.'/$data->'.$model->pkey.'/active/")',
                 ),
                 'photos' => array(
                     'label' => 'Фотографии',
                     'imageUrl' => $this->imagesUrl.'/photo.gif',
                     'url'   => 'Yii::app()->createAbsoluteUrl("/admin/'.$this->module->name.'/$data->'.$model->pkey.'/photos/")',
                 	 'visible' => '(Yii::app()->user->id == $data->user_id || Yii::app()->user->isSuperuser)'
                 )
             ),
             'htmlOptions' => array(
             	'width' => '80px'
             )   
        )
	)
));
?>
<br/><br/>
<center>
    <button class="btn" onclick="window.location.href='<?php echo Yii::app()->createAbsoluteUrl('/admin/'.$this->module->name.'/add/');?>';"><?php echo Yii::t('core', 'Создать');?></button>
</center>
</div>
