<?php
$this->pageTitle = _t('Manage Pages');
$this->pageHint = _t('Here you can manage your Pages');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'Page'
));
