<?php
$this->pageTitle = _t('Add new Page');
$this->pageHint = _t('Here you can add new Page for your Site');
$this->widget('cms.widgets.page.PageCreateWidget');
