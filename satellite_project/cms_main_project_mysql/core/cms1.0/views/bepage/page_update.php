<?php
$this->pageTitle = _t('Update Page');
$this->pageHint = _t('Here you can update information for current Page');
$this->widget('cms.widgets.page.PageUpdateWidget');
