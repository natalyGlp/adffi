<?php
$this->pageTitle = _t('Update Block');
$this->pageHint = _t('Here you can update information for current Block');
$this->widget('cms.widgets.page.BlockCreateUpdateWidget');