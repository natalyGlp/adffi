<?php
$this->pageTitle = _t('Manage Blocks');
$this->pageHint = _t('Here you can manage your Blocks');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'Block'
));
