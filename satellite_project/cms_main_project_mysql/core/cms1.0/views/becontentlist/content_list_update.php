<?php
$this->pageTitle = _t('Update Content list');
$this->pageHint = _t('Here you can update information for current Content list');
$this->widget('cms.widgets.page.ContentListUpdateWidget', array(
    'object_update_url' => 'beobject/update'
));
