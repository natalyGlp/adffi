<?php
/**
 * @var string $updateRoute путь для запуска обновлений
 * @var string $checkUpdateRoute путь для проверки списка обновлений
 */
?>
<div id="updatesList" class="well"><?php echo _t('Searching for updates ...'); ?><hr /></div>

<?php
echo CHtml::beginForm();
echo CHtml::passwordField('password', '') . '<br />';
echo CHtml::ajaxButton('Install updates', $this->createUrl($updateRoute), array(
    'type' => 'post',
    'dataType' => 'json',
    'beforeSend' => 'function() {}',
    'success' => 'function(data) {
        var html = [];
        if(data.status)
            html.push("<p>Success</p>");
        else
            html.push("<p>There was an error performing update</p>");

        $("#updatesList").append(html.join("\n"));
    }',
    ), array(
    'class' => 'btn btn-success',
    'disabled' => 'disabled',
    'id' => 'installBtn',
    ));
echo CHtml::endForm();
?>

<script type="text/javascript">
$(function() {
    $.ajax({
        url: '<?php echo $this->createUrl($checkUpdateRoute) ?>',
        type: 'post',
        dataType: 'json',
        success: function(data)
        {
            var html = [];
            if(data.length == 0)
                html.push('<p><?php echo _t('There is no updates available') ?></p>');
            else
            {
                for (var i in data)
                {
                    html.push('<p>'+data[i]+'</p>');
                }
                $('#installBtn').removeProp('disabled');
            }

            $('#updatesList').append(html.join('\n'));
        }
    });
});
</script>
