<?php
$this->pageTitle = _t('Add new Album');
$this->pageHint = _t('Here you can add new Album');
$this->widget('cms.widgets.albums.AlbumsCreateWidget');
