<?php
$this->pageTitle = _t('Manage Albums');
$this->pageHint = _t('Here you can manage your Albums');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'Albums'
));
