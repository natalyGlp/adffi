<?php
$this->pageTitle = _t('Update User');
$this->pageHint = _t('Make sure the username, user url and email are unique ');
$this->widget('cms.widgets.user.UserUpdateWidget');
