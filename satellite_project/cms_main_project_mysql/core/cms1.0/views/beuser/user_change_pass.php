<?php
$this->pageTitle = _t('Change Password');
$this->pageHint = _t('Here you can change your password');
$this->widget('cms.widgets.user.UserChangePassWidget');
