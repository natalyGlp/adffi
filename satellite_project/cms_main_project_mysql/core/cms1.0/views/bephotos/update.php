<?php
$this->pageTitle = _t('Update Photo');
$this->pageHint = _t('Here you can update information for current Photo');
$this->widget('cms.widgets.photo.PhotoUpdateWidget', array(
    'model_name' => 'GalleryPhotos'
));
