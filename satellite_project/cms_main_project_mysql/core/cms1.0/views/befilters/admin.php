<?php
$this->pageTitle = _t('Manage Filters');
$this->pageHint = _t('Here you can manage your Filters');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'FiltersOptions'
));
