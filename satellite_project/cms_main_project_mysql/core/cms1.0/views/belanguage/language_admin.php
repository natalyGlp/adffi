<?php
$this->pageTitle = _t('Manage Languages');
$this->pageHint = _t('Here you can manage your Languages.');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'Language'
));
