<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'language-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'lang_name'); ?>
		<?php echo $form->textArea($model,'lang_name',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'lang_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lang_desc'); ?>
		<?php echo $form->textArea($model,'lang_desc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'lang_desc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lang_required'); ?>
		<?php echo $form->textField($model,'lang_required'); ?>
		<?php echo $form->error($model,'lang_required'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lang_active'); ?>
		<?php echo $form->textField($model,'lang_active'); ?>
		<?php echo $form->error($model,'lang_active'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lang_short'); ?>
		<?php echo $form->textField($model,'lang_short',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'lang_short'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->