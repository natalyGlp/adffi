<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'lang_id'); ?>
		<?php echo $form->textField($model,'lang_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lang_name'); ?>
		<?php echo $form->textArea($model,'lang_name',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lang_desc'); ?>
		<?php echo $form->textArea($model,'lang_desc',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lang_required'); ?>
		<?php echo $form->textField($model,'lang_required'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lang_active'); ?>
		<?php echo $form->textField($model,'lang_active'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lang_short'); ?>
		<?php echo $form->textField($model,'lang_short',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->