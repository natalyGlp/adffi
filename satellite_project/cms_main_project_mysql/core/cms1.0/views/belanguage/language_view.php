<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('lang_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->lang_id), array('view', 'id'=>$data->lang_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lang_name')); ?>:</b>
	<?php echo CHtml::encode($data->lang_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lang_desc')); ?>:</b>
	<?php echo CHtml::encode($data->lang_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lang_required')); ?>:</b>
	<?php echo CHtml::encode($data->lang_required); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lang_active')); ?>:</b>
	<?php echo CHtml::encode($data->lang_active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lang_short')); ?>:</b>
	<?php echo CHtml::encode($data->lang_short); ?>
	<br />


</div>