<?php
$this->pageTitle = _t('Manage System Settings');
$this->pageHint = _t('Here you can manage all Site System Settings');
$this->widget('cms.widgets.settings.SettingsWidget', array(
    'type' => 'system'
));
