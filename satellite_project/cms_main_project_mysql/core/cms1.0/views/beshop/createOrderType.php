<?php
/**
 * @var OrderType $model типы доставки
 */
?>
<div class="form">
<?php
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'orderTypesForm',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>false,
        'clientOptions' => array(
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          // TODO: необходимо как-то вынести этот элемент либо в отдельный файл либо придумать как подловить через js событие успешной валидации. как вариант писать в aftervalidate колбек функцию. сейчас этот же код скопипасчен для вьюхи settings контроллера BeshopController

          // обработчики события afterValidate должны возвращать false, в том случае, если валидация прошла успешно
          'afterValidate' => 'js:function(form, data, hasError) {
            var afterValidateHasErrors = $("body").triggerHandler("afterValidate", [form, data, hasError]);

            //if no error in validation, send form data with Ajax
            if (! hasError) {
              $.ajax({
                type: "POST",
                url: form[0].action,
                context: form[0],
                data: $(form).serialize()+"&submit=1",
                success: function(response) {
                    var $response = $(response);
                    $response.find("input[type=submit]").hide(); // надо обратно спрятать кнопку отправки формы
                    $(this).parent().replaceWith($response);
                    $.fn.yiiGridView.update("orderTypes"); // обновим табличку
                    $.sticky("Способ доставки обновлен", {autoclose : 5000, position: "top-center", type: "st-success" });
                },
                error: function() {
                  $.sticky("Во время сохранения возникли ошибки", {autoclose : 5000, position: "top-center", type: "st-error" });
                }
              });

              // блочим отправку формы, так как мы сделали это через аякс
              return false;
            }
            return (!hasError && !afterValidateHasErrors)
        }',
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )
));
?>
<?php echo $form->textFieldRow($model, 'name', array('class'=>'input-small')); ?>
<?php echo $form->dropDownListRow($model, 'type', OrderType::getTypesList(), array('empty' => _t('Select type'))); ?>
<?php echo $form->textFieldRow($model, 'value', array('class'=>'input-small')); ?>
<?php echo $form->dropDownListRow($model, 'enabled', OrderType::getEnabledList(), array('empty' => _t('Select type'))); ?>
<div class="row buttons">
        <?php echo CHtml::submitButton(_t('Save'),array('class'=>'btn btn-success')); ?>
</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
