<?php
$this->pageTitle = _t('Caching Management');
$this->pageHint = _t('Here you can clear Cache & Assets for Backend and Frontend');
$this->widget('cms.widgets.caching.CachingClearWidget');
