<?php
$this->pageTitle = _t('Update Menu Item');
$this->pageHint = _t('Here you can update information for current Menu Item');
$this->widget('cms.widgets.page.MenuItemCreateUpdateWidget');
