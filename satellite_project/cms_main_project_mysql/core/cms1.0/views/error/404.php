<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<?php
Yii::app()->clientScript->registerCss(__FILE__, '
    body,
    html {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
    }
    
    body {
        background: #eeeeee;
        display: table;
        font-family: "Times new roman";
        font-size: 14px;
        color: #000;
    }

    .error-code,
    .error-message {
        margin: 0;
    }

    .http-error {
        display: table-cell;
        vertical-align: middle;
        text-align: center;
    }

    .error-code {
        color: #bf1e2e;
        font-size: 110px;
        font-family: "Open Sans", sans-serif;
    }

    .error-message {
        font-size: 30px;
    }

    .error-nav {
        font-size: 15px;
        margin-top: 50px;
    }

    a {
        color: #000;
        text-decoration: none;
    }

    a:hover {
        text-decoration: underline;
    }

    .error-back {
        color: #bf1e2e;
        font-weight: bold;
    }
');

$this->widget(GxcHelpers::getTruePath('front_blocks.error_notification.ErrorNotificationBlock'), array(
    'cssClass' => 'http-error',
    ));
?>
</body>
</html>