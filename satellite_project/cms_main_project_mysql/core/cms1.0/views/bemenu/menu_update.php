<style type="text/css">.row{margin-left: 0;}</style>
<?php
$this->pageTitle = _t('Update Menu');
$this->pageHint = _t('Here you can update information for current Menu');
$site = Yii::app()->controller->site;
$form_create_url = Yii::app()->controller->createUrl('bemenuitem/create', array(
    'embed' => 'iframe',
    'menu' => $id,
    'site' => $site
));
$form_update_url = Yii::app()->controller->createUrl('bemenuitem/update', array(
    'embed' => 'iframe',
    'menu' => $id,
    'site' => $site
));
$form_change_order_url = Yii::app()->controller->createUrl('bemenuitem/changeorder', array(
    'site' => $site
));
$form_delete_url = Yii::app()->controller->createUrl('bemenuitem/delete', array(
    'site' => $site
));
$this->widget('cms.widgets.page.MenuCreateUpdateWidget', array(
    'form_create_term_url' => $form_create_url,
    'form_update_term_url' => $form_update_url,
    'form_delete_term_url' => $form_delete_url,
    'form_change_order_term_url' => $form_change_order_url,
));
