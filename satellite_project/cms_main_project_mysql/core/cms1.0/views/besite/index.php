<?php $this->pageTitle = Yii::app()->settings->get('general', 'site_name') . ' - Backend'; ?>
<div>
    <br><br>
    <h2><?= _t('Content Managing') ?></h2>

    <?php
        $this->widget('cms.widgets.object.ObjectCreateUpdateWidget');
    ?>

    <br /><br />

    <h2><?= _t('Other') ?></h2>
    <ul class="dshb_icoNav iconNav_left">
        <li>
            <a href="<?= Yii::app()->createUrl('bepage/create'); ?>" style="background-image: url('<?php
            echo GxcHelpers::assetUrl('img/gCons/add-item.png', 'gebo'); ?>'); background-size: 32px;">
            <?php
                $str = _t('Create new Page');
                echo mb_strlen($str, 'utf-8') > 13 ? mb_substr($str, 0, 11, 'utf-8') . '...' : $str;
            ?>
            </a>
        </li>


        <li>
            <a href="<?= Yii::app()->createUrl('beresource/create'); ?>" style="background-image: url('<?php
            echo GxcHelpers::assetUrl('img/gCons/download.png', 'gebo'); ?>'); background-size: 32px;">
            <?php
                $str = _t('Upload a File');
                echo mb_strlen($str, 'utf-8') > 13 ? mb_substr($str, 0, 11, 'utf-8') . '...' : $str;
            ?>
            </a>
        </li>

        <li>
            <a href="<?= Yii::app()->createUrl('beemail/index'); ?>" style="background-image: url('<?php
            echo GxcHelpers::assetUrl('img/gCons//email.png', 'gebo'); ?>'); background-size: 32px;">
                <?php
                $newEmailCount = EmailLog::model()->cache(60*15)->count(array(
                    'condition'=> 'status = ' . EmailLog::STATUS_UNDONE,
                ));
                ?>
            <span class="label label-important"><?= $newEmailCount ?></span>
            <?php
                $str = _t('New email');
                echo mb_strlen($str, 'utf-8') > 13 ? mb_substr($str, 0, 11, 'utf-8') . '...' : $str;
            ?>
            </a>
        </li>
    </ul>
</div>
<br /><br /><br /><br />
<div align="right">
    <p><?= _t('Powered by ');?><a href="http://glp.ua">GLP</a></p>
    <p><?= _t('Having problems? Contact us at: ')?> <a href="mailto:info@glp.ua">info@glp.ua</a>
</div>
