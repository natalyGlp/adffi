<?php 
Yii::app()->controller->pageTitle = TranslateModule::t('Missing Translations') . " - " . TranslateModule::translator()->acceptedLanguages[Yii::app()->getLanguage()];

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'enableHistory' => true,
    'id' => 'message-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        array(
            'name' => 'message',
            'htmlOptions' => array(
                'style' => 'width: 100%;',
            ),
        ),
        array(
            'name' => 'category',
            'filter' => CHtml::listData($source, 'category', 'category'),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{create}',
            'buttons' => array(
                'create' => array(
                    'label' => t('Create'),
                    'imageUrl' => false,
                    'icon' => 'file',
                    'url' => 'Yii::app()->getController()->createUrl("update",array("id"=>$data->id))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => t('Delete'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->getController()->createUrl("missingdelete",array("id"=>$data->id))',
                ),
            ),
        ),
    ),
));
