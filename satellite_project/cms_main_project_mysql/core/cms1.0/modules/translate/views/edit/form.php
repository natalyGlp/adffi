<?php
Yii::app()->controller->pageTitle = TranslateModule::t('Manage Message')
        ." # ".$source->id;
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'message-form',
    'enableAjaxValidation' => false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->textFieldRow($source, 'category', array('class' => 'span5', 'disabled'=>'disabled')); ?>

<?php echo $form->textFieldRow($source, 'message', array('class' => 'span5', 'disabled'=>'disabled')); ?>

<br>
<br>
<br>

<fieldset>
    <legend>Translations</legend>
    <?php
    foreach ($models as $model) {
        echo $form->textAreaRow($model, '['.$model->language.']translation', array(
            'class' => 'span5',
            'labelOptions' => array(
                'label' => $model->languageName,
                ),
            ));
    }
    ?>
</fieldset>

<div class="form-actions">
<?php 
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type'       => 'primary',
        'label'      => TranslateModule::t('Update'),
    ));
?>
</div>

<?php
$this->endWidget('bootstrap.widgets.TbActiveForm');
