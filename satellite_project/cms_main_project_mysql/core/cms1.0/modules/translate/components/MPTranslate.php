<?php
/**
 * Widget to display Has Many form
 * 
 * @author someone else
 * @author Sviatoslav <dev@udf.su>
 * @version 1.1.0
 * @package application.modules.translate
 */

class MPTranslate extends CApplicationComponent
{
    /**
     * @var string $defaultLanguage defaults language to use if none is set
     * */
    public $defaultLanguage = null;

    /**
     * @var string $googleTranslateApiKey your google translate api key
     * set this if you wish to use googles translate service to translate the messages
     * if empty it will not use the service
     * */
    public $googleApiKey = null;

    /**
     * @var string $languageKey an unique key to be used
     * */
    public $languageKey = 'lang';

    /**
     * @var array $acceptedLanguages contains the languages accepted by your application
     * */
    public $acceptedLanguages = array();

    /**
     * @var boolean wheter to auto translate the missing messages found on the page
     * needs google api key to set
     * */
    public $autoTranslate = false;

    /**
     * @var array $_cache will contain variables during one request
     * */
    private $_cache = array();

    /**
     * @var boolean wheter to automatically set the language on the initialization of the component
     * */
    public $autoSetLanguage = false;

    protected $_sourceMessages = array();

    private $_isCacheInvalidated = false;

    /**
     * @var array $_urlPrefixMap массив, который хранит соответствие префикса url соотв коду языка
     */
    private $_urlPrefixMap = array();
    private $_languageIdByCode = array();

    public function init()
    {
        $this->fetchLanguageData();

        if ($this->autoSetLanguage) {
            $requestUri = $_SERVER['REQUEST_URI'];
            $uriLang = explode('/', trim($requestUri, '/'));
            $uriLang = reset($uriLang);

            // язык формата ru_ru
            if (preg_match('/^(\w\w)_\w\w$/', $uriLang, $matches) && !key_exists($uriLang, $this->_urlPrefixMap) && key_exists($matches[1], $this->_urlPrefixMap)) {
                $this->redirectOnGet($this->replaceUrlLanguage($requestUri, $uriLang, $matches[1]));
            }

            // редирект /ru -> / (где ru - дефолтный язык)
            if ($uriLang == $this->defaultLanguage) {
                $this->setLanguage($this->defaultLanguage);
                $this->redirectOnGet($this->replaceUrlLanguage($requestUri, $uriLang));
            }

            // здесь наш язык в виде ru, en и т.д.
            if (!key_exists($uriLang, $this->_urlPrefixMap)) {
                // не правильный язык, или в url вообще нету языка
                $language = $this->getLanguage();// автоопредиление языка
                $newUriLang = $this->getLanguageUrl();

                // на этом этапе в $language находится правильный идентификатор языка
                if ($language != $this->defaultLanguage && $newUriLang != $uriLang) {
                    $this->redirectOnGet(rtrim('/' . $newUriLang . '/' . ltrim($requestUri, '/'), '/'));
                }
            } elseif ($uriLang !== $this->getLanguage()) {
                // в $uriLang находится полностью валидный язык, проверяем, что с ним можно еще сделать
                if (isset($_GET[$this->languageKey]) || isset($_POST[$this->languageKey])) {
                    // язык поменялся через get/post, делаем редирект
                    $newUriLang = $this->getLanguageUrl();
                    Yii::app()->request->redirect($this->replaceUrlLanguage($requestUri, $uriLang, $newUriLang));
                } else {
                    // меняем язык приложения согласно uri
                    $this->setLanguage($this->_urlPrefixMap[$uriLang]);
                }
            }

            $this->setLanguage();
            $this->setSourceLanguage('en');

            if (key_exists($uriLang, $this->_urlPrefixMap)) {
                // чистим информацию о языке, что бы она не мешала запуску контроллеров
                $_SERVER['REQUEST_URI'] = $this->replaceUrlLanguage($requestUri, $uriLang);
            }
        }

        Yii::app()->attachEventHandler('onEndRequest', array($this, 'checkCache'));

        return parent::init();
    }

    protected function fetchLanguageData()
    {
        $cacheId = get_class($this) . '::fetchLanguageData()';
        if (($data = Yii::app()->cache->get($cacheId)) === false) {
            $allLanguages = Language::model()->findAll('lang_active=1');
            $this->acceptedLanguages = array();

            foreach ($allLanguages as $lng) {
                $this->acceptedLanguages[$lng->code] = $lng->name;
                $this->_languageIdByCode[$lng->code] = $lng->primaryKey;
                $this->_urlPrefixMap[$lng->urlPrefix] = $lng->code;

                if ($lng->lang_required == 1) {
                    $this->defaultLanguage = $lng->code;
                }
            }

            if (empty($this->defaultLanguage)) {
                $this->defaultLanguage = Yii::app()->getLanguage();
            }

            $data = array($this->defaultLanguage, $this->acceptedLanguages, $this->_languageIdByCode, $this->_urlPrefixMap);
            Yii::app()->cache->set($cacheId, $data);
        }

        list($this->defaultLanguage, $this->acceptedLanguages, $this->_languageIdByCode, $this->_urlPrefixMap) = $data;
    }

    /**
     * Редирект только GET запросов
     */
    protected function redirectOnGet($url)
    {
        if (!Yii::app()->request->isPostRequest) {
            Yii::app()->request->redirect($url);
        }
    }

    /**
     * Правильно заменяет идентификатор языка в url
     */
    protected function replaceUrlLanguage($url, $from, $to = '')
    {
        $newUrl = preg_replace(array('#^/?' . $from . '#', '#//#'), array('/' . $to, '/'), $url);
        $newUrl = str_replace('lang=' . $from, '', $newUrl);
        $newUrl = str_replace('&&', '', $newUrl);

        return $newUrl;
    }

    /**
     * method that handles the on missing translation event
     *
     * @param CMissingTranslationEvent $event
     * @return string the message to translate or the translated message if autoTranslate is set to true
     */
    public function missingTranslation($event)
    {
        Yii::import('translate.models.MessageSource');

        $attributes = array(
            'category' => $event->category,
            'message' => $event->message
            );

        if (!$this->hasMessage($attributes)) {
            $model = new MessageSource();
            $model->attributes = $attributes;

            if ($model->save()) {
                $this->_isCacheInvalidated = true;
                $this->_sourceMessages[$model->category][$model->message] = $model->primaryKey;
            } else {
                Yii::log(TranslateModule::t('Message ' . $event->message . ' could not be added to messageSource table'));
            }
        }

        if ($this->shouldAutoTranslate($event->language)) {
            Yii::import('translate.models.Message');

            $translation = $this->googleTranslate($event->message, $event->language, Yii::app()->sourceLanguage);
            $messageModel = new Message();
            $messageModel->attributes = array(
                'id' => $this->getMessageSourcePk($attributes),
                'language' => $event->language,
                'translation' => $translation
                );
            if ($messageModel->save()) {
                $event->message = $translation;
            } else {
                Yii::log(TranslateModule::t('Message ' . $event->message . ' could not be translated with auto-translate'));
            }
        }

        return $event;
    }

    /**
     * Определяет id модели MessageSource по сообщению $message, для которого надо найти перевод
     * @param  array $attributes аттрибуты для поиска сообщения (нужны элементы message и category)
     * @return MessageSource модель отвечающая за перевод или false
     */
    protected function hasMessage($attributes)
    {
        if (empty($this->_sourceMessages)) {
            $this->loadMessages();
        }

        return isset($this->_sourceMessages[$attributes['category']][$attributes['message']]);
    }

    protected function getMessageSourcePk($attributes)
    {
        return $this->hasMessage($attributes) ? $this->_sourceMessages[$attributes['category']][$attributes['message']] : null;
    }

    protected function loadMessages()
    {
        if (($messages = Yii::app()->cache->get($this->cacheId)) === false) {
            $command = $this->getDbConnection()->createCommand('SELECT * FROM '.Yii::app()->getMessages()->sourceMessageTable);
            $messages = array();
            foreach ($command->queryAll() as $row) {
                if (!isset($messages[$row['category']])) {
                    $messages[$row['category']] = array();
                }

                $messages[$row['category']][$row['message']] = $row['id'];
            }

            Yii::app()->cache->set($this->cacheId, $messages);
        }

        $this->_sourceMessages = $messages;
    }

    public function checkCache()
    {
        if ($this->_isCacheInvalidated) {
            $this->clearMessagesCache();
        }
    }

    protected function clearMessagesCache()
    {
        Yii::app()->cache->delete($this->cacheId);
    }

    protected function getCacheId()
    {
        return get_class($this) . '::loadMessages(1.1.0)';
    }

    protected function getDbConnection()
    {
        return Yii::app()->{CONNECTION_NAME};
    }

    /**
     * returns the language in use
     * the language is determined by many variables: session, post, get, header in this order
     * it will filter the language by using accepted languages
     *
     * @return string
     */
    public function getLanguage()
    {
        $key = $this->languageKey;
        if (($language = @$this->_cache['language']) !== null) {
            return $language;
        }

        if (isset($_POST[$key]) && !empty($_POST[$key])) {
            $language = $_POST[$key];
        } elseif (isset($_GET[$key]) && !empty($_GET[$key])) {
            $language = $_GET[$key];
        } elseif (Yii::app()->getSession()->contains($key)) {
            $language = Yii::app()->getSession()->get($key);
        } elseif (Yii::app()->getRequest()->getPreferredLanguage() !== false) {
            $language = Yii::app()->getRequest()->getPreferredLanguage();
        } else {
            $language = $this->defaultLanguage;
        }

        if (key_exists($language, $this->_urlPrefixMap)) {
            $language = $this->_urlPrefixMap[$language];
        }

        if (!key_exists($language, $this->acceptedLanguages)) {
            if (strpos($language, "_") !== false) {
                $language = substr($language, 0, 2);
            }
        }

        if (!key_exists($language, $this->acceptedLanguages)) {
            // Увы, но этот язык не поддерживается
            $language = $this->defaultLanguage;
        }

        $this->setLanguage($language);

        return $language;
    }

    /**
     *
     * set the language that the application will use
     * if $language is null then if you use getLanguage to determine the target language
     *
     * it doesn't check if the language is in the accepted languages
     *
     * @param string | null $language language to set
     * @return string the language setted
     */
    public function setLanguage($language = null)
    {
        if ($language === null) {
            $language = $this->getLanguage();
        }

        $this->_cache['language'] = $language;

        foreach ($this->acceptedLanguages as $key => $value) {
            if (Yii::app()->getSession()->contains($key)) {
                Yii::app()->getSession()->remove($key);
            }
        }

        Yii::app()->getSession()->add($this->languageKey, $language);
        Yii::app()->setLanguage($language);

        return $language;
    }

    public function getLanguageUrl($languageCode = false)
    {
        if (!$languageCode) {
            $languageCode = $this->getLanguage();
        }

        return array_search($languageCode, $this->_urlPrefixMap);
    }

    public function getLanguageId($languageCode = false)
    {
        $languageCode = $languageCode ? $languageCode : $this->language;
        return $this->_languageIdByCode[$languageCode];
    }

    public function getSourceLanguage()
    {
        return Yii::app()->sourceLanguage;
    }

    public function setSourceLanguage($languageCode)
    {
        Yii::app()->sourceLanguage = $languageCode;
    }

    /**
     * helper so you can use MPTransalate::someMethod($args)
     *
     * php 5.3 only
     *
     * @param mixed $method
     * @param mixed $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(TranslateModule::translator(), $method), $args);
    }

    /**
     * creates a link to the page where you edit the translations
     *
     * @param string $label
     * @param string $type accepted types are button and link
     * @return
     */
    public function editLink($label = 'Edit translations', $type = 'link', $htmlOptions = array())
    {
        $url = Yii::app()->createUrl('translate/edit/admin');
        if ($type === 'button') {
            return CHtml::button($label, $url, $htmlOptions);
        } else {

            return CHtml::link($label, $url, $htmlOptions);
        }
    }

    /**
     * creates a link to the page where you check all missing translations
     *
     * @param string $label
     * @param string $type accepted types are button and link
     * @return string
     */
    public function missingLink($label = 'Missing translations', $type = 'link', $htmlOptions = array())
    {
        $url = Yii::app()->createUrl('translate/edit/missing');
        if ($type === 'button') {
            return CHtml::button($label, $url, $htmlOptions);
        } else {

            return CHtml::link($label, $url, $htmlOptions);
        }
    }

    /**
     * translate some message from $sourceLanguage to $targetLanguage using google translate api
     * googleApiKey must be defined to use this param
     * @service string $message to be translated
     * @param string $targetLanguage language to translate the message to, if null it will use the current language in use
     * @param mixed $sourceLanguage language that the message is written in, if null it will use the application source language
     * @return string translated message
     */
    public function googleTranslate($message, $targetLanguage = null, $sourceLanguage = null)
    {
        if ($targetLanguage === null) {
            $targetLanguage = Yii::app()->getLanguage();
        }

        if ($sourceLanguage === null) {
            $sourceLanguage = Yii::app()->sourceLanguage;
        }

        if (empty($sourceLanguage)) {
            throw new CException(TranslateModule::t('Source language must be defined'));
        }

        if ($targetLanguage === $sourceLanguage) {
            throw new CException(TranslateModule::t('targetLanguage must be different than sourceLanguage'));
        }

        $query = $this->queryGoogle(array('q' => $message, 'source' => $sourceLanguage, 'target' => $targetLanguage));
        if (is_array($message)) {
            foreach ($query->translations as $translation) {
                $translated[] = $translation->translatedText;
            }

            return $translated;
        } else {

            return $query->translations[0]->translatedText;
        }
    }

    protected function shouldAutoTranslate($language)
    {
        return $this->autoTranslate && $this->isAutoTranslatable($language);
    }

    protected function isAutoTranslatable($language)
    {
        return substr($language, 0, 2) !== substr($this->sourceLanguage, 0, 2)
            && key_exists($language, $this->getGoogleAcceptedLanguages($language));
    }

    /**
     * returns an array containing all languages accepted by google translate
     *
     * @param string $targetLanguage
     * @return array
     */
    public function getGoogleAcceptedLanguages($targetLanguage = null)
    {
        $cacheKey = $this->languageKey . '-cache-google-languages-' . $targetLanguage;
        if (!isset($this->_cache[$cacheKey])) {
            if (($cache = Yii::app()->getCache()) === null || ($languages = $cache->get($cacheKey)) === false) {
                $queryLanguages = $this->queryGoogle($targetLanguage !== null ? array('target' => $targetLanguage) : array(), 'languages');
                foreach ($queryLanguages->languages as $language) {
                    $languages[$language->language] = isset($language->name) ? $language->name : $language->language;
                }
                if ($cache !== null) {
                    $cache->set($cacheKey, $languages);
                }

                $this->_cache[$cacheKey] = $languages;
            }
        } else {
            $languages = $this->_cache[$cacheKey];
        }

        return $languages;
    }

    /**
     * query google translate api
     *
     * @param array $args
     * @param string $method the method to use, use null to translate
     * accepted values are null(translate), "languages" and "detect"
     * @return stdClass the google response object
     */
    protected function queryGoogle($args = array(), $method = null)
    {
        if (empty($this->googleApiKey)) {
            throw new CException(TranslateModule::t('You must set googleApiKey'));
        }

        if ($method !== null) {
            $method = "/{$method}}";
            $url = preg_replace('/%5B\d+%5D/', '', "https://www.googleapis.com/language/translate/v2{$method}?" . http_build_query(array_merge($args, array('key' => $this->googleApiKey))));

            if (in_array('curl', get_loaded_extensions())) {//curl has much better performance
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//to speed up the query
                $trans = curl_exec($curl);
                curl_close($curl);
            } else {
                $trans = file_get_contents($url);
            }

            if (!$trans) {
                return false;
            }

            $trans = json_decode($trans);

            if (isset($trans->error)) {
                throw new CHttpException($trans->error->code, $trans->error->message);
            } elseif (!isset($trans->data)) {
                throw new CException(print_r($trans, true));
            } else {

                return $trans->data;
            }
        }
    }
}
