<?php
/**
 * Добавляет возможность скрывать меню и подменю
 */
class m141007_135839_remove_source_dups extends CDbMigration
{
    public function safeUp()
    {
        $this->execute(
            'DELETE FROM {{source_message}}'
            .' WHERE id NOT IN (SELECT id FROM {{translated_message}})'
        );
    }

    public function safeDown()
    {
        return true;
    }
}
