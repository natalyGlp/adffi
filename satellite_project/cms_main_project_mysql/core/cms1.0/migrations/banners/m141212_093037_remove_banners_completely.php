<?php

class m141212_093037_remove_banners_completely extends CDbMigration
{
    public function up()
    {
        $this->dropTable('{{banners}}');
    }

    public function down()
    {
        $this->createTable('{{banners}}', array(
            'id' => 'int(11) NOT NULL AUTO_INCREMENT',
            'title' => 'varchar(255) NOT NULL',
            'content' => 'text NOT NULL',
            'image' => 'varchar(255) NOT NULL',
            'url' => 'varchar(255) NOT NULL',
            'type' => 'enum(\'random\') NOT NULL DEFAULT \'random\'',
            'create_date' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'active' => 'tinyint(1) NOT NULL DEFAULT \'1\'',
            'PRIMARY KEY (`id`)',
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }
}
