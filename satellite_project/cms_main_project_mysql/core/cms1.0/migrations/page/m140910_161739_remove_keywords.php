<?php
/**
 * Убирает поле с ключевыми словами
 */
class m140910_161739_remove_keywords extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('{{page}}', 'keywords');
	}

	public function down()
	{
        $this->addColumn('{{page}}', 'keywords', 'varchar(255) NOT NULL');
	}
}