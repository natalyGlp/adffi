<?php
/**
 * Добавляет возможность скрывать меню и подменю
 */
class m140930_135439_menu_item_is_visible extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{menu_item}}', 'is_visible', 'TINYINT(1) UNSIGNED NOT NULL AFTER `description`');
        $this->alterColumn('{{menu_item}}', 'is_visible', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1');

        $this->update('{{menu_item}}', array(
            'is_visible' => 1,
            ));
    }

    public function down()
    {
        $this->dropColumn('{{menu_item}}', 'is_visible');
    }
}
