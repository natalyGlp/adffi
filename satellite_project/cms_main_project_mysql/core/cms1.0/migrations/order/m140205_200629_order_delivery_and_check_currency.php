<?php
/**
 * Добавляет таблицу для настройки типов доставки
 * Добавляет личное поле вылюты для каждого товара в чеке
 */
class m140205_200629_order_delivery_and_check_currency extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('{{order_type}}', array(
			'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'type' => 'TINYINT(1) UNSIGNED NOT NULL',
			'name' => 'VARCHAR(255)',
			'value' => 'DECIMAL(19,2) NOT NULL',
			'message' => 'TEXT',
			'condition' => 'TEXT',
			'enabled' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT "1"',
			),'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->addColumn('{{order_check}}', 'currency', 'VARCHAR(3) AFTER total_price');
	}

	public function safeDown()
	{
		$this->dropTable('{{order_type}}');
		$this->dropColumn('{{order_check}}', 'currency');
	}
}