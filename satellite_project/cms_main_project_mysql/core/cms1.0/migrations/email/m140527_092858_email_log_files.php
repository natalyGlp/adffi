<?php
/**
 * Добавил поле для хранения информации о загруженных файлах
 */
class m140527_092858_email_log_files extends CDbMigration
{
	public function up()
	{
		$this->addColumn('{{email_log}}', 'files', 'TEXT DEFAULT NULL AFTER source');
	}

	public function down()
	{
		$this->dropColumn('{{email_log}}', 'files');
	}
}