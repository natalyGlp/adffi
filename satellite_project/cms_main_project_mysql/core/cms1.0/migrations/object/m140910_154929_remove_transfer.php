<?php
/**
 *  Удаляет таблицу transfer
 */
class m140910_154929_remove_transfer extends CDbMigration
{
	public function up()
	{
        try {
    		$this->dropTable('{{transfer}}');
        } catch(Exception $e) {}
	}

	public function down()
	{
		$this->createTable('{{transfer}}', array(
            'transfer_id' => 'bigint(20) NOT NULL AUTO_INCREMENT',
            'object_id' => 'bigint(20) NOT NULL',
            'from_user_id' => 'bigint(20) NOT NULL',
            'to_user_id' => 'bigint(20) NOT NULL',
            'before_status' => 'tinyint(2) NOT NULL',
            'after_status' => 'tinyint(2) NOT NULL',
            'type' => 'int(11) NOT NULL',
            'note' => 'varchar(125) DEFAULT NULL',
            'time' => 'int(11) NOT NULL',
            'PRIMARY KEY (`transfer_id`)',
            ), 'ENGINE=MyISAM  DEFAULT CHARSET=utf8');
	}
}