<?php
/**
 *  Удаляет поле с ключевыми словами в обьекте
 */
class m140910_161929_remove_object_keywords extends CDbMigration
{
    public function up()
    {
        $this->dropColumn('{{object}}', 'object_keywords');
    }

    public function down()
    {
        $this->addColumn('{{object}}', 'object_keywords', 'text');
    }
}