<?php

/**
 * This is the Widget for Clear Caching
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package  cms.widgets.caching
 */
class CachingClearWidget extends CWidget
{
    public $visible = true;
    protected $errorMessage = array();
    protected $successMessage = array();

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    public function renderContent()
    {
        if (isset($_POST['submit'])) {
            $cache_ids = $_POST;
            foreach ($cache_ids as $cache_id => $value) {
                switch ($cache_id) {
                    case 'frontend_assets':
                        $this->addAssertion($this->deleteAssets('frontend'), 'frontent', 'assets');
                        break;

                    case 'frontend_cache':
                        $this->addAssertion($this->clearCache('frontend'), 'frontent', 'cache');
                        break;

                    case 'backend_assets':
                        $this->addAssertion($this->deleteAssets('backend'), 'backend', 'assets');
                        break;

                    case 'backend_cache':
                        $this->addAssertion($this->clearCache('backend'), 'backend', 'cache');
                        break;

                    case 'images_cache':
                        try {
                            $this->clearImageCache();
                        } catch (Exception $e) {
                            $this->addError('images', 'cache');
                            break;
                        }
                        $this->addSuccess('images', 'cache');
                        break;
                }
            }

            $this->sendResponce();
        }
        $this->render('cms.widgets.views.caching.caching_widget', array(
            'frontend_assets' => !Yii::app()->assetManager->linkAssets,
            'backend_assets' => !Yii::app()->assetManager->linkAssets && $this->hasSeparateBackend(),
            'frontend_cache' => true,
            'backend_cache' => $this->hasSeparateBackend(),
            'images_cache' => true,
        ));
    }

    /**
     * Выполняет действия после отправки формы
     * в зависимости от типа запроса либо возвращает json, либо редиректит
     */
    protected function sendResponce()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            if (!empty($this->errorMessage)) {
                Yii::app()->user->setFlash('error', implode('<br>', $this->errorMessage));
            }
            Yii::app()->user->setFlash('success', implode('<br>', $this->successMessage));
            Yii::app()->controller->redirect(array(
                'becaching/clear',
                'site' => Yii::app()->controller->site,
            ));
        } else {
            if (empty($this->errorMessage)) {
                $data = array(
                    'status' => 'success',
                    'message' => $this->successMessage,
                );
            } else {
                header("HTTP/1.0 500 Internal Server Error");
                $data = array(
                    'status' => 'error',
                    'message' => $this->errorMessage,
                );
            }

            header('Content-Type: application/json');
            echo CJSON::encode($data);

            Yii::app()->end();
        }
    }

    public function clearCache($where)
    {
        $cachePath = '';
        if ($this->hasSeparateBackend()) {
            switch ($where) {
                case 'frontend':
                    $cachePath = FRONT_END . '/runtime';
                    break;

                case 'backend':
                    $cachePath = BACK_END . '/runtime';
                    break;
            }
        } else {
            $cachePath = Yii::app()->runtimePath;
        }
        $cachePath .= '/cache';

        return is_dir($cachePath) && recursive_remove_directory($cachePath, true);
    }

    public function deleteAssets($where)
    {
        $assetsDir = '';
        if ($this->hasSeparateBackend()) {
            switch ($where) {
                case 'frontend':
                    $assetsDir = dirname(CMS_PATH) . '/assets';
                    break;

                case 'backend':
                    $assetsDir = dirname(BACK_END) . '/assets';
                    break;
            }
        } else {
            if (Yii::app()->controller->site) {
                $assetsDir = Yii::getPathOfAlias('webroot.assets');
            } else {
                $assetsDir = Yii::app()->assetManager->basePath;
            }
        }
        return is_dir($assetsDir) && recursive_remove_directory($assetsDir, true);
    }

    public function clearImageCache()
    {
        $resourcesPath = Yii::getPathOfAlias('uploads.resources');
        $years = scandir($resourcesPath);
        foreach ($years as $year) {
            if (!preg_match('/[0-9]{4}/', $year)) {
                continue;
            }
            $months = scandir($resourcesPath . '/' . $year);
            if (empty($months)) {
                continue;
            }

            foreach ($months as $month) {
                $resizedPath = $resourcesPath . '/' . $year . '/' . $month . '/resized';
                if (!preg_match('/[0-9]{2}/', $month) || !is_dir($resizedPath)) {
                    continue;
                }

                foreach (glob($resizedPath . '/resized/*') as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
            }
        }
    }

    protected function hasSeparateBackend()
    {
        return preg_match('/backend$/', Yii::getPathOfAlias('webroot'));
    }

    protected function addAssertion($assertion, $where, $what)
    {
        $assertion ? $this->addSuccess($where, $what) : $this->addError($where, $what);
    }

    protected function addSuccess($where, $what)
    {
        $this->successMessage[] = _t('{where} {what} are cleared!', 'cms', array(
            '{what}' => ucfirst($what),
            '{where}' => ucfirst($where),
        ));
    }

    protected function addError($where, $what)
    {
        $this->errorMessage[] = _t('Something wrong! Can\'t clear {where} {what}!', 'cms', array(
            '{what}' => ucfirst($what),
            '{where}' => ucfirst($where),
        ));
    }
}
