<?php

/**
 * This is the Widget for Clear Caching using ajax
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package  cms.widgets.caching
 */
class AjaxCachingClearWidget extends CWidget
{
    public function init()
    {
        $this->registerClientScript();
    }

    public function run()
    {
        echo CHtml::link('<i class="icon-white icon-trash"></i>', '#', array(
            'title' => _t('Clear Cache'),
            'class' => 'ttip_l',
            'id' => $this->id,
        ));
    }

    protected function registerClientScript()
    {
        $url = CJavaScript::encode(
            Yii::app()->createUrl('becaching/clear', array(
                'site' => isset(Yii::app()->controller->site) ? Yii::app()->controller->site : '',
            ))
        );
        $requestData = CJavaScript::encode(array(
            'frontend_assets' => 1,
            'frontend_cache' => 1,
            'backend_cache' => 1,
            'submit' => 1,
        ));

        Yii::app()->clientScript->registerScript(__FILE__.'#'.$this->id, <<<EOD
        jQuery('body').on('click','#{$this->id}',function() {
            'use strict';

            var options = {
                iconBaseClass: 'icon-white',
                iconNormal: 'trash',
                iconProgress: 'fire',
                iconSuccess: 'ok-circle',
                iconError: 'remove-circle',
                url: $url,
                requestData: $requestData,
                delayTime: 1500
            };

            function setIcon(iconName, delayedIconName) {
                return function() {
                    this.removeClass().addClass(options.iconBaseClass + ' icon-' + iconName);

                    if (delayedIconName) {
                        setTimeout($.proxy(setIcon(delayedIconName), this), options.delayTime);
                    }
                }
            }

            jQuery.ajax({
                'type': 'POST',
                'data': options.requestData,
                'context': jQuery(this).children().eq(0),
                'beforeSend': setIcon(options.iconProgress),
                'success': setIcon(options.iconSuccess, options.iconNormal),
                'error': setIcon(options.iconError, options.iconNormal),
                'url': options.url,
                'cache': false
            });

            return false;
        });
EOD
);
    }
}
