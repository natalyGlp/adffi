<?php

/**
 * This is the Widget for Changing Password.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package  cmswidgets.user
 *
 */
class UserChangePassWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        if (!Yii::app()->user->isGuest) {
            $model = new UserChangePassForm;
            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'userchangepass-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset($_POST['UserChangePassForm'])) {
                $model->attributes = $_POST['UserChangePassForm'];

                // validate user input password
                if ($model->validate()) {
                    $u = User::model()->findbyPk(Yii::app()->user->id);
                    if ($u !== null) {
                        $u->password = $u->hashPassword($model->new_password_1, USER_SALT);
                        $u->salt = USER_SALT;
                        if ($u->save()) {
                            Yii::app()->user->setFlash('success', _t('Changed Password Successfully!'));
                        } else {
                            Yii::app()->user->setFlash('error', _t('There was an error changing password!'));
                        }
                    }
                    $model = new UserChangePassForm;
                }
            }

            $this->render('cms.widgets.views.user.user_change_pass_widget', array('model' => $model));
        } else {
            Yii::app()->request->redirect(Yii::app()->user->returnUrl);
        }
    }
}
