<?php

/**
 * This is the Widget for create new User.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.widgets.user
 *
 */
class UserCreateWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model = new UserCreateForm();

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usercreate-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['UserCreateForm'])) {
            $model->attributes = $_POST['UserCreateForm'];

            // validate user input password
            $model->image = CUploadedFile::getInstance($model, 'image');
            if ($model->validate()) {
                $new_user = new User;
                $new_user->scenario = 'create';
                $new_user->first_name = $model->first_name;
                $new_user->last_name = $model->last_name;
                $new_user->email = $model->email;
                $new_user->display_name = $model->display_name;
                $new_user->password = $model->password;
                $new_user->confirmed = 1;
                $new_user->bio = $model->bio;

                if ($model->image) {
                    $path = UserAvatarForm::processUploadedImage($model->image);
                    UserAvatarForm::updateUserAvatar($path, $new_user, false);
                }

                if ($new_user->save()) {
                    Yii::app()->user->setFlash('success', _t('Create new User Successfully!'));
                    Yii::app()->controller->redirect(array('create'));
                }
            }
        }

        $this->render('cms.widgets.views.user.user_create_widget', array('model' => $model));
    }
}
