<?php

/**
 * This is the Widget for Managing  Settings
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.widgets.settings
 *
 */

Yii::import('cms.front_blocks.contacts.ContactsBlock');
class SettingsWidget extends CWidget
{

    public $visible = true;
    public $type = 'general';

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        switch ($this->type) {
            case 'system':
                $this->showSystemForm();
                break;
            case 'social':
                $this->showSocialForm();
                break;

            default:
                $this->showGeneralForm();
                break;
        }
    }

    protected function showSystemForm()
    {

        $model = new SettingSystemForm;
        settings()->deleteCache();
        //Set Value for the Settings
        $model->sizes = CJSON::decode(Yii::app()->settings->get('system', 'sizes'));
        $model->sizes = is_array($model->sizes) ? $model->sizes : array();

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'settings-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['SettingSystemForm'])) {
            settings()->deleteCache();
            $model->setAttributes($_POST['SettingSystemForm'], false); // FIXME Точно ли нам нужно отключать валидацию? (появилось при добавлении поддержки разных размеров изображений sizes)
            $model->sizes = CJSON::encode(is_array($model->sizes) ? $model->sizes : array());

            if ($model->validate()) {
                foreach ($model->attributes as $key => $value) {
                    Yii::app()->settings->set('system', $key, $value);
                }

                Yii::app()->controller->refresh();
                Yii::app()->user->setFlash('success', _t('System Settings Updated Successfully!'));
            }
        }

        $model->sizes = is_array($model->sizes) ? $model->sizes : array();
        $this->render('cms.widgets.views.settings.settings_system_widget', array('model' => $model));
    }

    protected function showGeneralForm()
    {

        $model = new SettingGeneralForm;

        settings()->deleteCache();
        //Set Value for the Settings
        $model->support_email = Yii::app()->settings->get('system', 'support_email');
        $model->site_url = Yii::app()->settings->get('general', 'site_url');
        $model->site_name = Yii::app()->settings->get('general', 'site_name');
        $model->slogan = Yii::app()->settings->get('general', 'slogan');
        $model->homepage = Yii::app()->settings->get('general', 'homepage');
        $model->logo = Yii::app()->settings->get('general', 'logo');
        $model->phones = Yii::app()->settings->get('general', 'phones');
        $model->address = Yii::app()->settings->get('general', 'address');

        // FIXME: для обратной совместимости. В будущем нужно убрать этот код
        if (!is_array($model->phones)) {
            $model->phones = array(
                $model->phones => array(
                    'phone_filter_type' => 1,
                    'phone_filter' => '',
                ),
            );
        }

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'settings-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['SettingGeneralForm'])) {
            // yii скаждым файловым полем добавляет скрытый input с пустым значением.
            // удаляем этот элемент, что бы он не затер наше лого
            unset($_POST['SettingGeneralForm']['logo']);
            if (isset($_POST['phone']) && isset($_POST['phone_filter_type']) && isset($_POST['phone_filter'])) {
                // обработка телефонов
                foreach ($_POST['phone'] as $key => $phone) {
                    $_POST['SettingGeneralForm']['phones'][$phone] = array(
                        'phone_filter_type' => $_POST['phone_filter_type'][$key],
                        'phone_filter' => str_replace(" ", "", $_POST['phone_filter'][$key]),
                    );
                }
            }

            $model->attributes = $_POST['SettingGeneralForm'];
            if (isset($_FILES['SettingGeneralForm'])) {
                $file = CUploadedFile::getInstance($model, 'logo');
                if ($file && isset($file->type)) {
                    $model->logo = 'data:' . $file->type . ';base64,' . base64_encode(file_get_contents($file->tempName));
                }
            }

            if ($model->validate()) {
                settings()->deleteCache();

                foreach ($model->attributes as $key => $value) {
                    Yii::app()->settings->set('general', $key, $value);
                }
                // DEPRECATED
                Yii::app()->settings->set('system', 'support_email', $model->support_email);
                Yii::app()->user->setFlash('success', _t('General Settings Updated Successfully!'));
            }
        }

        $this->render('cms.widgets.views.settings.settings_general_widget', array(
            'model' => $model,
        ));
    }

    protected function showSocialForm()
    {

        $model = new SettingSocialForm;

        settings()->deleteCache();
        //Set Value for the Settings
        $model->facebook = Yii::app()->settings->get('social', 'facebook');
        $model->vkontakte = Yii::app()->settings->get('social', 'vkontakte');
        $model->vkApiId = Yii::app()->settings->get('social', 'vkApiId');
        $model->gplus = Yii::app()->settings->get('social', 'gplus');
        $model->youtube = Yii::app()->settings->get('social', 'youtube');
        $model->twitter = Yii::app()->settings->get('social', 'twitter');
        $model->skype = Yii::app()->settings->get('social', 'skype');
        $model->linkedin = Yii::app()->settings->get('social', 'linkedin');
        $model->vimeo = Yii::app()->settings->get('social', 'vimeo');
        $model->rss = Yii::app()->settings->get('social', 'rss');
        $model->instagram = Yii::app()->settings->get('social', 'instagram');
        $model->dribbble = Yii::app()->settings->get('social', 'dribbble');
        $model->pinterest = Yii::app()->settings->get('social', 'pinterest');
        $model->odnoklassniki = Yii::app()->settings->get('social', 'odnoklassniki');

        $model->facebook_widget = Yii::app()->settings->get('social', 'facebook_widget');
        $model->vkontakte_widget = Yii::app()->settings->get('social', 'vkontakte_widget');
        $model->gplus_widget = Yii::app()->settings->get('social', 'gplus_widget');
        $model->twitter_widget = Yii::app()->settings->get('social', 'twitter_widget');
        $model->odnoklassniki_widget = Yii::app()->settings->get('social', 'odnoklassniki_widget');

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'settings-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['SettingSocialForm'])) {
            $model->attributes = $_POST['SettingSocialForm'];
            if ($model->validate()) {
                settings()->deleteCache();
                foreach ($model->attributes as $key => $value) {
                    Yii::app()->settings->set('social', $key, $value);
                }
                Yii::app()->user->setFlash('success', _t('Social Settings Updated Successfully!'));
            }
        }

        $this->render('cms.widgets.views.settings.settings_social_widget', array('model' => $model));
    }

}
