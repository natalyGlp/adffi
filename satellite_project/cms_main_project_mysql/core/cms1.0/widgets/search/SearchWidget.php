<?php

/**
 * This is the Widget for Creating new Term
 *
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @version 1.0
 * @package  cmswidgets.object
 *
 *
 */
class SearchWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        Yii::import('cms.models.search.*');
        $dataProvider = new CActiveDataProvider('SearchLucene');

        // if it is ajax validation request
        /*
        if(isset($_POST['ajax']) && $_POST['ajax']==='mailtemplate-form')
        {
        echo CActiveForm::validate($model);
        Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['MailTemplate']))
        {
        $model->attributes=$_POST['MailTemplate'];
        //var_dump($_POST); die();
        if($model->save()){
        Yii::app()->user->setFlash('success',_t('Create new Mail Template Successfully!'));

        if(!isset($_GET['embed'])) {
        $model=new MailTemplate;
        Yii::app()->controller->redirect(array('create'));
        }
        }
        }*/

        $this->render('cms.widgets.views.search.search_index_list', array(
            'model' => $dataProvider->model,
            'dataProvider' => $dataProvider,
        ));
    }
}
