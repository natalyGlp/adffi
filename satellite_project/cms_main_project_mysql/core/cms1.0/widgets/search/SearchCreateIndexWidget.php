<?php

/**
 * This is the Widget for Creating new Term
 *
 * @author Sviatoslav Danilenko <dev@udf.su>
 * @version 1.0
 * @package  cmswidgets.object
 *
 *
 */
class SearchCreateIndexWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model = new SearchLucene;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'indexcreate-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        $modelName = method_exists('CHtml', 'modelName') ? CHtml::modelName($model) : get_class($model);
        if (isset($_POST[$modelName])) {
            $model->attributes = $_POST[$modelName];

            // validate user input password
            if ($model->save()) {
                Yii::app()->user->setFlash('success', t('Create new User Successfully!'));

                // запускаем генерацию нового индекса
                Yii::app()->controller->actionRebuild($model->primaryKey);
                Yii::app()->controller->redirect(array('create'));
            }
        }

        $this->render('cms.widgets.views.search.search_create_index', array('model' => $model)); //cmswidgets.views.user.user_create_widget
    }
}
