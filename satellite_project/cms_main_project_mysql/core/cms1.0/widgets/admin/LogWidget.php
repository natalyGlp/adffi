<?php

/**
 * This is the Widget for displaying Log
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.admin
 */
class LogWidget extends CWidget
{
    public $visible = true;
    protected $filtersForm;

    public function run()
    {
        if ($this->visible) {

            // Create filter model and set properties
            // http://www.yiiframework.com/wiki/232/using-filters-with-cgridview-and-carraydataprovider/
            $this->filtersForm = new FiltersForm;
            if (isset($_GET['FiltersForm'])) {
                unset($_GET['FiltersForm'][0]);
                $this->filtersForm->filters = $_GET['FiltersForm'];
            }

            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $filtersForm = $this->filtersForm;

        // runtimeName => runtimePath
        $runtimes = $this->getRuntimes();

        $selectedRuntimePath = isset($_GET['runtime']) && isset($runtimes[$_GET['runtime']]) ? $runtimes[$_GET['runtime']] : reset($runtimes);

        $availableLogs = $this->getRuntimeLogs($selectedRuntimePath);
        $selectedLogFile = isset($_GET['log']) && in_array($_GET['log'], $availableLogs) ? $_GET['log'] : reset($availableLogs);
        $logFilePath = $selectedRuntimePath . '/' . $selectedLogFile;

        list($logData, $logCategories) = $this->parseLog($logFilePath);

        $dataProvider = new CArrayDataProvider($logData, array(
            'id' => 'log',
            'keyField' => false,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));

        $runtimesDropDown = array_flip($runtimes);
        $runtimesDropDown = array_combine($runtimesDropDown, $runtimesDropDown);
        $this->render('cms.widgets.views.admin.log', array(
            'dataProvider' => $dataProvider,
            'filtersForm' => $filtersForm,
            'logCategories' => $logCategories,
            'runtimes' => $runtimesDropDown,
            'availableLogs' => array_combine($availableLogs, $availableLogs),
        ));
    }

    /**
     * Возвращает массив с данными лога для использования в CGridView
     * @param string $logFilePath путь к файлу лога для парсинга
     * @return $logData, $logCategories
     */
    protected function parseLog($logFilePath)
    {
        $filtersForm = $this->filtersForm;

        $logString = file_get_contents($logFilePath);

        // добавляем разделитель, по которому будем делить строку
        $logString = preg_replace('/^(\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2})/m', '--Separator--$0', $logString);

        // Добавляем еще один сепаратор, что бы отображалась и последняя запись в логе
        $logString .= '--Separator--';
        preg_match_all('/(\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}) \[([^\]]+)\] \[([^\]]+)\] (.*?)--Separator--/s', $logString, $matches, PREG_SET_ORDER);
        $matches = array_reverse($matches);
        $logData = $filtersForm->filter($matches);

        $logCategories = array();
        foreach ($matches as $row) {
            $logCategories[$row[3]] = $row[3];
        }

        asort($logCategories);

        return array(
            $logData,
            $logCategories,
        );
    }

    /**
     * @return array массив с путями к рантаймам runtimeName => runtimePath
     */
    protected function getRuntimes()
    {

        // runtimeName => runtimePath
        $runtimes = array();
        if (Yii::app()->id == 'backend' && strpos(Yii::app()->runtimePath, 'backend')) {

            // в этом приложении два рантайма
            $runtimes['frontend'] = str_replace('backend', 'frontend', Yii::app()->runtimePath);
        }
        $runtimes[empty($runtimes) ? 'main' : Yii::app()->id] = Yii::app()->runtimePath;
        // backend || satellite

        return $runtimes;
    }

    /**
     * @param string $path путь к директории рантайма
     * @return array Названия файлов логов в папке райнтайма $path
     */
    protected function getRuntimeLogs($runtimePath)
    {
        $logPathes = glob($runtimePath . DIRECTORY_SEPARATOR . '*.log');
        $logFiles = array();
        foreach ($logPathes as $logPath) {
            array_push($logFiles, basename($logPath));
        }

        $logFiles = array_reverse($logFiles);

        return $logFiles;
    }
}
