<?php

/**
 * This is the Widget for Creating new Language
 *
 * @author Yurta Kovalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package  cmswidgets.object
 *
 *
 */
class LanguageCreateWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model = new Language;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'language-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['Language'])) {
            $model->attributes = $_POST['Language'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', _t('Create new Language Successfully!'));

                if (!isset($_GET['embed'])) {
                    $model = new Language;
                    Yii::app()->controller->redirect(array(
                        'create',
                        'site' => isset($_GET['site']) ? $_GET['site'] : '',
                    ));
                }
            }
        }

        $this->render('cms.widgets.views.language.language_form_widget', array(
            'model' => $model,
        ));
    }
}
