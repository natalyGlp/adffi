<?php

/**
 * This is the Widget for Creating new Language
 *
 * @author Yurta Kovalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package  cmswidgets.object
 *
 *
 */
class LanguageUpdateWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $lang_id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
        $model = GxcHelpers::loadDetailModel('Language', $lang_id);

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'language-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['Language'])) {
            $model->attributes = $_POST['Language'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', _t('Update Language Successfully!'));
            }
        }
        $this->render('cms.widgets.views.language.language_form_widget', array(
            'model' => $model,
        ));
    }
}
