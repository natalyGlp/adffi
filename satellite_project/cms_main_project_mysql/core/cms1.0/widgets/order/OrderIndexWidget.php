<?php

/**
 * Виджет для отображения списка заказов
 *
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.order
 */
class OrderIndexWidget extends CWidget
{
    public $visible = true;

    public $criteria = false;
    public $id = 'orderIndex';

    public function init()
    {
        Yii::import('cms.models.order.*');
        GxcHelpers::registerJs('sticky/sticky.min.js');
        GxcHelpers::registerCss('sticky/sticky.css');
        GxcHelpers::registerCss('css/order.css', 'backend');
    }

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $orderModel = new Order('search');
        $orderModel->unsetAttributes();
        if (isset($_GET['Order'])) {
            $orderModel->attributes = $_GET['Order'];
        }

        $orderModel->statusArr = isset($_GET['Order']['statusArr']) ? $_GET['Order']['statusArr'] : array(1 => 1, 2, 5 => 5, 6 => 6, 7 => 7);

        $dataProvider = $orderModel->search();

        if ($this->criteria) {
            $dataProvider->criteria->mergeWith($this->criteria);
        }

        $this->render('cms.widgets.views.order.index', array(
            'orderModel' => $orderModel,
            'additionalFilters' => $this->criteria == true,
            'id' => $this->id,
            'listPayments' => CHtml::listData(CurrencyProvider::model()->findAll(), 'id', 'name'),
            'dataProvider' => $dataProvider,
        ));
    }
}
