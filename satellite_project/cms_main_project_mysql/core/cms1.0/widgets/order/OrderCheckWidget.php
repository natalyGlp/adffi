<?php

/**
 * Виджет для отображения содержимого чека заказа
 *
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.order
 */
class OrderCheckWidget extends CWidget
{
    public $visible = true;
    public $prodId;

    public function init()
    {
        Yii::import('cms.models.order.*');
    }

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $dataProvider = new CActiveDataProvider('OrderCheck', array(
            'criteria' => array(
                'condition' => 'order_id=' . $this->prodId,
            ),
            'sort' => array(
                'defaultOrder' => 'price DESC',
            ),
        ));

        $this->render('cms.widgets.views.order.check', array(
            'id' => $this->prodId,
            'dataProvider' => $dataProvider,
        ));
    }
}
