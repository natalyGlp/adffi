<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'settings-form',
    'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>
<div class="row">
    <?php echo $form->labelEx($model,'facebook'); ?>
    <?php echo $form->textField($model,'facebook'); ?>
    <?php echo $form->error($model,'facebook'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'facebook_widget'); ?>
    <?php echo $form->textArea($model,'facebook_widget'); ?>
    <?php echo $form->error($model,'facebook_widget'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'vkontakte'); ?>
    <?php echo $form->textField($model,'vkontakte'); ?>
    <?php echo $form->error($model,'vkontakte'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'vkApiId'); ?>
    <?php echo $form->textField($model,'vkApiId'); ?>
    <?php echo $form->error($model,'vkApiId'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'vkontakte_widget'); ?>
    <?php echo $form->textArea($model,'vkontakte_widget'); ?>
    <?php echo $form->error($model,'vkontakte_widget'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'gplus'); ?>
    <?php echo $form->textField($model,'gplus'); ?>
    <?php echo $form->error($model,'gplus'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'gplus_widget'); ?>
    <?php echo $form->textArea($model,'gplus_widget'); ?>
    <?php echo $form->error($model,'gplus_widget'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'youtube'); ?>
    <?php echo $form->textField($model,'youtube'); ?>
    <?php echo $form->error($model,'youtube'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'twitter'); ?>
    <?php echo $form->textField($model,'twitter'); ?>
    <?php echo $form->error($model,'twitter'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'twitter_widget'); ?>
    <?php echo $form->textArea($model,'twitter_widget'); ?>
    <?php echo $form->error($model,'twitter_widget'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'skype'); ?>
    <?php echo $form->textField($model,'skype'); ?>
    <?php echo $form->error($model,'skype'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'linkedin'); ?>
    <?php echo $form->textField($model,'linkedin'); ?>
    <?php echo $form->error($model,'linkedin'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'odnoklassniki'); ?>
    <?php echo $form->textField($model,'odnoklassniki'); ?>
    <?php echo $form->error($model,'odnoklassniki'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'odnoklassniki_widget'); ?>
    <?php echo $form->textArea($model,'odnoklassniki_widget'); ?>
    <?php echo $form->error($model,'odnoklassniki_widget'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'vimeo'); ?>
    <?php echo $form->textField($model,'vimeo'); ?>
    <?php echo $form->error($model,'vimeo'); ?>
</div>
 <div class="row">
    <?php echo $form->labelEx($model,'rss'); ?>
    <?php echo $form->textField($model,'rss'); ?>
    <?php echo $form->error($model,'rss'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'instagram'); ?>
    <?php echo $form->textField($model,'instagram'); ?>
    <?php echo $form->error($model,'instagram'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'dribbble'); ?>
    <?php echo $form->textField($model,'dribbble'); ?>
    <?php echo $form->error($model,'dribbble'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'pinterest'); ?>
    <?php echo $form->textField($model,'pinterest'); ?>
    <?php echo $form->error($model,'pinterest'); ?>
</div>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'success', 'label'=>_t('Save'))); ?>
</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
