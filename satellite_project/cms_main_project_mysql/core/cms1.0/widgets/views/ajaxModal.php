<?php
/**
 * Диалоговые окошки для просмотра информации через аякс
 * Биндятся по селектору .ajaxInfo ко всем ссылкам
 * @url http://getbootstrap.com/javascript/#modals
 */
?>
<script type="text/template" id="_ajaxModalTpl">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3>{title}</h3>
    </div>
    <div class="modal-body">{content}</div>
    <div class="modal-footer">
        <a href="#" class="btn btn-success btn-modal-submit">Сохранить</a>
        <a href="#" class="btn" data-dismiss="modal">Закрыть</a>
    </div>
</script>
<?php
ob_start();
?>
(function(){
    $('body')
        .on('click', '.ajaxInfo', function(event) {
            event.preventDefault();

            if ($(this).data('in-progress')) {
                console.info('Supressing repeating triggering. The ajax request is already in progress');
                return;
            }

            $.ajax({
                url: this.href,
                context: this,
                data: {
                    ajax: 1,
                },
                success: function(data) {
                    var $modal = createModal(data);

                    var hideSelector;
                    if($modal.find('form').length == 0) {
                        $modal.find('.btn-modal-submit').hide();
                    }
                    $modal.find('.modal-body').scrollTop(0);
                },
                complete: function() {
                    $(this).data('in-progress', false);
                }
            });

            $(this).data('in-progress', true);
        })
        .on('click', '.modal-scrollfix', function(event) {
            if (event.target == this) {
                $(this).find('[data-dismiss=modal]').click();
            }
        })
    ;

    function createModal(data) {
        var $modal = $('<div>');

        var $scrollFix = $('<div>');

        $scrollFix.append($modal);

        // Нужно добавить элемент в DOM до его рендеринга,
        // так как контенте может находиться скрипты, которые будут
        // искать что-то в DOM
        $scrollFix.appendTo($('body'));

        $scrollFix
            .css({
                'overflow-y': 'scroll'
            })
            .addClass('modal-scrollfix')
        ;
        $('html').css({
            'overflow': 'hidden'
        });

        renderModal($modal, {
            content: data
        });

        setModalEvents($modal);

        return $modal;
    }

    function renderModal($modal, data) {
        var modalTmpl = $('#_ajaxModalTpl').html();

        $modal
            .addClass('modal fade modal-ajax')
            .html(
                modalTmpl
                    .replace('{title}', $('head > title').text())
                    .replace('{content}', data.content)
            )
        ;
    }

    function setModalEvents($modal) {
        $modal
            .modal()
            .on('hidden.bs.modal', function () {
              $(this).parent().remove(); // .modal-scrollfix
              $('html').css({
                  'overflow': ''
              });
              $(this).remove();
            })
            .on('click', '.btn-modal-submit', function(event) {
                event.preventDefault();

                $(this).parents('.modal').find('[type=submit]').click();
            })
        ;
    }
}());
<?php
$modalJs = ob_get_clean();
Yii::app()->clientScript
    ->registerScript(__FILE__.'#modal', $modalJs)
    ->registerCss(__FILE__.'#modal', '
        .modal-ajax .modal-body .form-actions,
        .modal-ajax .modal-body [type=submit] {
            display: none;
        }
    ')
    ;
