<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'comment-grid',
    'enableHistory' => true,
    'dataProvider' => $dataProvider,
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'id',
            'htmlOptions' => array(
                'style' => 'width: 50px;',
            ),
        ),
        array(
            'name' => 'subject',
            'type' => 'raw',
            'value' => 'CHtml::link($data->subject, $data->backendViewUrl)',
        ),
        array(
            'name' => 'email',
            'type' => 'raw',
            'value' => 'CHtml::link($data->email, $data->backendViewUrl)',
        ),
        'source',
        'prettyDate',
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'CHtml::image($data->statusIcon, $data->statusLabel)',
            'htmlOptions' => array(
                'style' => 'width: 50px;',
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Manage'),
                    'url' => '$data->backendViewUrl.(isset($_GET["site"]) ? "&site=".$_GET["site"] : "")',
                ),
                'delete' => array(
                    'label' => _t('Delete'),
                    'url' => 'Yii::app()->controller->createUrl("delete", array("id"=>$data->primaryKey, "site" => isset($_GET["site"]) ? $_GET["site"] : ""))',
                ),
            ),
        ),
    ),
));
