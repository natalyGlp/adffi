<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
	'name' => '_variants',
	// TODO: что бы сделать эту ссылку рабочей как на обычном магазине так и в сателлите
	// есть смысл написать аяксовый экшен для ObjectController, который будет возвращать тупо всю инфу об обьекте для автокомплита
	// Возможен вариант добавления метода в модель каждого Обьекта, который будет возвращать тот массив данных, которые должен вернуть экшен контроллера

	// TODO: так же возможен вариант, что бы контроллер возвращал шаблон для пунктов меню для каждого отдельного обьекта
	'source' => 'js:function( request, response ) {
		$.ajax({
			url: "'.Yii::app()->createUrl('shop/default/acprod').'?term="+request.term,
			type: "post",
			dataType: "json",
			success: function(data) {
				// data = data ? data : [];
				// data.push({
				// 	label: "'._t('Add new product').'",
				// 	value: null,
				// 	prodId: "'._t('Add new product').'",
				// 	name: request.term,
				// 	thumb: '.CJavaScript::encode(CHtml::image(GxcHelpers::assetUrl('img/gCons/edit.png', 'gebo'), _t('Add new product'))).'
				// });
				response(data);
			}
		});
	}',

	'options' => array(
		'change' => 'js:function(event,ui){
			if (!ui.item) {
				$(this).val("");
			}
		}',
		'select' => 'js:function(event,ui){
			if(ui.item) {
				$.ajax({
					url: location.href+"&addVariant",
					type: "post",
					data: {id: ui.item.prodId, name: ui.item.name},
					success: function(data)
					{
						$("#'.$id.' tbody").append($(data.replace(/\[\]/g, "["+($("#'.$id.' tbody tr").length)+"]")));
						$("#_variants").val("").change();
					}
				});
			}

			return false;
		}',
		),
	'htmlOptions' => array(
		'placeHolder' => _t('Enter the name of the Variant...'),
	),
));
 ?>
 <table id="<?php echo $id ?>" class="table table-striped">
 	<thead>
	 	<tr>
	 		<td></td>
	 		<td></td>
	 		<td><?php echo _t('Product Name'); ?></td>
	 		<!--td><?php echo _t('Price'); ?></td>
	 		<td><?php echo _t('Amount'); ?></td-->
	 		<td></td>
	 	</tr>
 	</thead>
 	<tbody>
	 	<?php
	 	foreach ($variants as $index=>$variant) {
			$this->renderVariant($variant, $index);
		}
	 	?>
	 </tbody>
 </table>
<?php Yii::app()->clientScript->registerScript(__FILE__, '
	var getProductTitle = function(data) {
		var productTitle = '. CJavaScript::encode('
			<div style="overflow:auto">
				<span style="float:left;margin-right:10px;">{thumb}</span>
				<b>{name}<span class="edited" style="display:none;">*</span></b>
				<br />
				<span style="color:#666666;font-size:10px;">{prodId}</span>
			</div>
		').';
		productTitle = productTitle.replace(/\{name\}/g, data.name);
		productTitle = productTitle.replace(/\{thumb\}/g, data.thumb);
		productTitle = productTitle.replace(/\{prodId\}/g, data.prodId);

		return productTitle;
	}

	$("#_variants").data( "autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( "<a>" + getProductTitle(item) + "</a>" )
			.appendTo( ul );
	};
');
