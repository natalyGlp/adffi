<?php
$this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => _t('Product data'),
));
$filters = isset($model->filters) ? $model->filters : new Filters;
?>

<div class="row">
    <?php echo $form->labelEx($model,'sellable'); ?>
    <?php echo $form->dropDownList($model,'sellable', $model->getSellableStatuses()); ?>
    <?php echo $form->error($model,'sellable'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($filters,'price'); ?>
    <?php echo $form->textField($filters,'price'); ?>
    <?php echo $form->error($filters,'price'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'shop_id'); ?>
    <?php Yii::import(GxcHelpers::getTruePath('content_type.shop.ShopObject'));?>
    <?php
        echo $form->dropDownList(
            $model,
            'shop_id',
            CHtml::listData(ShopObject::model()->findAll(array(
                'select' => array('object_id', 'object_name'),
                'condition' => 'object_type=?',
                'params' => array('shop')
            )), 'object_id', 'object_name'), array('empty' => ''));
    ?>
    <?php echo $form->error($model,'shop_id'); ?>
</div>
<?php $this->endWidget();?>
