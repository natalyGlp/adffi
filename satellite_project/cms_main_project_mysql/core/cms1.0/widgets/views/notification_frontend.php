<?php
$notificationStatusId = '_NESPI_notification_frontend_init';
if (isset(Yii::app()->params[$notificationStatusId]) || !count(Yii::app()->user->getFlashes(false))) {
    return;
}

Yii::app()->params[$notificationStatusId] = true;
$id = 'popup'.uniqid();

$asset=Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.frontend.popup.jquery-popup-overlay'), false, -1, YII_DEBUG);
Yii::app()->clientScript->registerCoreScript('jquery')
    ->registerScriptFile($asset.'/jquery.popupoverlay.min.js')
    ->registerCssFile($asset.'/jquery.popupoverlay.min.css')
    ->registerScript($id, "
        jQuery('#$id').popup('show');
    ");

?>
<div id="<?php echo $id ?>" class="notification-popup" style="display: none;">
    <div class="notification-popup-content">
        <p><b><?php
            if (Yii::app()->user->hasFlash('success')) {
                echo Yii::app()->user->getFlash('success');
            } elseif (Yii::app()->user->hasFlash('error')) {
                echo Yii::app()->user->getFlash('error');
            }
        ?></b></p>
    </div>
    <div class="notification-popup-controls">
        <button class="<?php echo $id ?>_close"><?php echo t('OK'); ?></button>
    </div>
</div>
