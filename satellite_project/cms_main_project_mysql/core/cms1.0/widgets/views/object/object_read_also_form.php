<?php
$this->widget('bootstrap.widgets.TbBox', array(
    'title' => _t('Read also'),
    'content' => $this->render('cms.widgets.views.object.object_reat_also_form2', array(
        'form' => $form,
        'model' => $model,
    ), true, true),
));
