<div id="language-zone">
    <?php if (count($versions) > 1) : ?>
        <div class="row even border-left-silver">
            <?php
                $languageMenuItems = array();
                foreach ($versions as $version) {
                    $hasTranslation = isset($version['links']['update']);

                    array_push($languageMenuItems, array(
                        'label' => ($hasTranslation ? '' : '* ') . $version['name'],
                        'active' => $version['lang'] == $model->lang,
                        'url' => $hasTranslation ? $version['links']['update'] : $version['links']['create'],
                        ));
                }
            ?>
            <?php $this->widget('bootstrap.widgets.TbMenu', array(
                'type' => 'tabs',
                'stacked' => false,
                'items' => $languageMenuItems,
            )); ?>
        </div>
    <?php endif; ?>
    <?php if (Language::model()->isMultilang && !$model->isNewRecord) : ?>
        <?php echo $form->hiddenField($model, 'lang'); ?>
    <?php endif; ?>
</div>

<?php if(isset($allowChangeObjectType) && $allowChangeObjectType): ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'object_type'); ?>
        <?php
        echo CHtml::dropDownList(
            'type',
            isset($_POST['type']) ? $_POST['type'] : $_GET['type'],
            GxcHelpers::getAvailableContentType(true)
        );
        ?>
        <?php echo $form->error($model, 'object_type'); ?>
    </div>
<?php endif; ?>

 <div id="titlewrap" class="row">
    <?php echo $form->labelEx($model, 'object_name'); ?>
    <?php echo $form->textArea($model, 'object_name', array(
        'tabindex' => '1',
        'id' => 'txt_object_name'
    )); ?>
    <?php echo $form->error($model, 'object_name'); ?>
</div>

<div class="bootstrap-widget row row-fluid">
    <div class="input-append span12">
    <?php
        echo $form->label($model, 'object_slug').
        $form->textField($model, 'object_slug', array('class' => 'txt_object_slug', 'id' => 'object_slug')).
        '<a class="add-on" href="javascript:void(0)"><i class="icon-refresh slug-refresh"></i></a>'.
        $form->error($model, 'object_slug');
    ?>
    </div>
</div>
<div id="bodywrap">
    <div class="bootstrap-widget">
        <?php if(empty($model->object_excerpt)): ?>
            <a href="" class="btn btn-warning"><i class="icon-plus-sign"></i> <?php echo _t('Add excerpt'); ?></a>
        <?php endif; ?>
        <div>
            <?php echo $form->labelEx($model, 'object_excerpt'); ?>
            <?php $this->widget('cms.extensions.ckeditor.CKEditor', array(
                'id' => 'ckeditor_excerpt',
                'model' => $model,
                'attribute' => 'object_excerpt',
                'htmlOptions' => array(
                    'tabindex' => '2',
                    'style' => 'height: 200px;',
                ),
            )); ?>
            <?php echo $form->error($model, 'object_excerpt'); ?>
        </div>
    </div>

    <div class="bootstrap-widget">
         <?php echo $form->labelEx($model, 'object_content'); ?>
        <?php $this->widget('cms.extensions.ckeditor.CKEditor', array(
            'id' => 'ckeditor_content',
            'model' => $model,
            'attribute' => 'object_content',
            'htmlOptions' => array(
                'tabindex' => '3',
            ),
        )); ?>
         <?php echo $form->error($model, 'object_content'); ?>
    </div>
</div>
