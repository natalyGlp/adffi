<p><?php echo _t('Please choose a content type to continue'); ?></p>
<div>
<ul class="dshb_icoNav iconNav_left">
<?php foreach($types as $type) : ?>
    <li>
        <a href="<?= $type['url'] ?>" style="background-image:url('<?= $type['iconSrc'] ?>'); background-size: 32px;">
        <?php echo mb_strlen($type['label'], 'utf-8') > 14 ? mb_substr($type['label'], 0, 12, 'utf-8') . '...' : $type['label']; ?>
        </a>
    </li>
<?php endforeach; ?>
</ul>
</div>
