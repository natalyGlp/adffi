<?php
$site = Yii::app()->controller->site;

$defaultColumns = array(
    array(
        'name' => 'object_id',
        'type' => 'raw',
        'htmlOptions' => array(
            'style' => 'width: 50px',
        ),
        'value' => '$data->object_id',
    ),
    array(
        'name' => 'object_date',
        'type' => 'raw',
        'value' => 'date("Y-m-d H:i",$data->object_date)',
    ),
    array(
        'name' => 'object_name',
        'type' => 'raw',
        'value' => 'CHtml::link($data->object_name, Yii::app()->controller->createUrl("update", array("id"=>$data->object_id,"type"=>$data->object_type,"site" => "' . $site . '")))',
    ),
    array(
        'name' => 'object_type',
        'type' => 'raw',
        'value' => 'Object::convertObjectType($data->object_type)',
        'filter' => GxcHelpers::getAvailableContentType(true),
    ),
    array(
        'name' => 'object_status',
        'type' => 'raw',
        'value' => '$data->statusString;',
        'filter' => $model->getObjectStatus(),
    ),
    array(
        'name' => 'lang',
        'type' => 'raw',
        'value' => 'Language::convertLanguage($data->lang)',
        'filter' => CHtml::listData(Language::model()->findAll('lang_active=1'), 'lang_id', 'lang_desc'),
    ),
    array(
        'name' => 'object_hits',
        'sortable' => true,
        'visible' => false,
    ),
    array(
        'name' => 'object_uniq_hits',
        'sortable' => true,
        'visible' => false,
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonColumn',
        'template' => '{update} {delete}',
        'buttons' => array(
            'delete' => array(
                'url' => 'Yii::app()->controller->createUrl("delete", array("id"=>$data->object_id,"site" => "' . $site . '"))',
                'label' => _t('Delete'),
                'imageUrl' => false,
            ),
            'update' => array(
                'label' => _t('Edit'),
                'imageUrl' => false,
                'url' => 'Yii::app()->controller->createUrl("update", array("id"=>$data->object_id,"type"=>$data->object_type,"site" => "' . $site . '"))',
            ),

            // 'translate' => array(
            //    'label' => _t('Translate'),
            //     'imageUrl' => false,
            //     'visible' => (Language::model()->isMultilang) * 1 . '',
            //     'url' => 'Yii::app()->controller->createUrl("create", array("guid"=>$data->guid,"type"=>$data->object_type))',
            // ),

        ),
    ),
);

$columns = CMap::mergeArray($defaultColumns, $model->gridColumns);

$ecolumns = $this->widget('cms.extensions.ecolumns.EColumnsDialog', array(
    'options' => array(
        'title' => _t('Layout settings'),
        'autoOpen' => false,
        'show' => 'fade',
        'hide' => 'fade',
    ),
    'htmlOptions' => array(
        'style' => 'display: none',
    ),
    //disable flush of dialog content
    'ecolumns' => array(
        'gridId' => 'object-grid',
        //id of related grid
        'storage' => 'db',
        //where to store settings: 'db', 'session', 'cookie'
        'fixedRight' => array(
            'bootstrap.widgets.TbButtonColumn',
        ),
        'buttonApply' => '<p></p><input type="submit" value="' . _t('Apply') . '" class="btn btn-success">',
        'buttonCancel' => '<div class="pull-right"><input type="submit" value="' . _t('Reset') . '"  class="reset btn"> <input type="submit" value="' . _t('Cancel') . '"  class="btn btn-danger" onclick="$(this).closest(\'.ui-dialog-content\').dialog(\'close\')">',
        'buttonReset' => '</div>',
        'model' => $model,
        //model is used to get attribute labels

        'columns' => $columns,
    ),
));

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'object-grid',
    'enableHistory' => true,
    'dataProvider' => $result,
    'filter' => $model,
    'summaryText' => $ecolumns->link() . _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),

    'columns' => $ecolumns->columns(),
));
