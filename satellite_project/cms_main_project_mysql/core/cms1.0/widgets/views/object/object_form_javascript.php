<script type="text/javascript">
	$(function () {
    	var config = {
    		height: 350,
    		width : '100%',
    		resize_enabled : false,
            allowedContent: true, // отключае агресивное фильтрование html
    		toolbar :
    		[
    			['Source','-','Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','SelectAll','RemoveFormat'],
    			['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
    			['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    			['BidiLtr', 'BidiRtl'],
    			['Link','Unlink','Anchor'],
    			['Image', 'MediaEmbed', 'Media','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe','-','Save','NewPage','Preview','-','Templates','-','Cut','Copy','Paste','PasteText','PasteFromWord'],
    			'/',
    			['Undo','Redo','-','Find','Replace','-','Styles','Format','Font','FontSize'],
    			['TextColor','BGColor'],
    			['Maximize', 'ShowBlocks','-','About']
    		]
    	};

        //Minimizable the blocks
		$(".bootstrap-widget-header").click(function () {
            $(this).closest('.bootstrap-widget').toggleClass('is-collapsed');
		});


        <?php if($model->isNewRecord): ?>
        CopyString('#txt_object_name','#object_slug','slug');
        CopyString('#txt_object_name','#txt_object_slug','slug');
        <?php endif; ?>
        CopyString('#object_slug','#txt_object_slug','slug');
        CopyString('#txt_object_slug','#object_slug','slug');
        CopyString('#txt_object_name','#txt_object_title','');
        CopyString('#ckeditor_excerpt','#txt_object_description','text');

        <?php
        GxcHelpers::registerJs('autosize/jquery.autosize.min.js');
        ?>
        var inputHeight = $('input[type=text]').height();
        $('#txt_object_name').keypress(function(event) {
            if (event.which == 13) {
                return false;
            }
            $('#txt_object_name').val($('#txt_object_name').val().replace(/\n*/g, ''))
        }) // отключаем перевод строк в textarea для титла
        .autosize();

        $('.slug-refresh').click(function() {
            $('.txt_object_slug').each(function() {
                var str = /[а-я]+/.test($(this).val().toLowerCase()) ? $(this).val() : $('#txt_object_name').val();
                $(this).val(str2url(str));
            });
        });

        (function() {
            // спойлеры
            var $btn = $('#ckeditor_excerpt').parent().prev('a.btn');
            if (!!$btn.length) {
                $('#ckeditor_excerpt').parent()
                    .hide()
                    .prev('a.btn').click(function(){
                        $('#ckeditor_excerpt').parent().show(300);
                        $(this).remove();
                        return false;
                    });
            }

        }());
    });
</script>
