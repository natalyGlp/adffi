<?php
$this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => _t('Publish'),
));

$this->render('cms.widgets.views.object.object_workflow', array(
    'form' => $form,
    'model' => $model,
    'content_status' => $content_status,
    'type' => $type,
));

$this->endWidget('bootstrap.widgets.TbBox');
?>

<?php
if (!isset($no_terms)) {

    // TODO: это на самом деле словари такосномии
    $taxonomies = $terms;

    $this->widget('\cms\widgets\object\ObjectTermsFormWidget', array(
        'form' => $form,
        'model' => $model,
        'taxonomies' => $taxonomies,
        'selectedTerms' => $selected_terms,
    ));
}
?>

<?php

// Динамические списки
$this->widget('cms.widgets.object.DynamicListWidget', array(
    'model' => $model,
));
