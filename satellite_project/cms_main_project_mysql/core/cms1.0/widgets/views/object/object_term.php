<?php
$blockId = 'taxonomy_'.$taxonomy['id'].'_'.$taxonomy['lang'];


$this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => $taxonomy['name'],
    'htmlOptions' => array(
        'id' => $blockId,
        'class' => 'taxonomy_wrap',
        )
    ));


$termNames=array();
foreach ($taxonomy['terms'] as $t) {
    $termNames[]=$t['name'];
}
?>
<div class="row-fluid">
<?php $this->widget('CAutoComplete', array(
    'name' => 'term_auto_'.$taxonomy['id'],
    'data' => $termNames,
    'multiple' => true,
    'mustMatch' => true,
    'htmlOptions' => array(
        'class'=>'span12',
        'id'=>'term_auto_'.$taxonomy['id']
    ),
    'methodChain' => ".result(function(event, item) {
        if (item !== undefined) {
            var \$term = \$('#list_terms_".$taxonomy['id']." [rel=\"'+item+'\"]');
            if (\$term.length > 0) {
                \$term.find('[type=checkbox]').click();
            }
        }

        \$(this).val('');
    })",
)); ?>
</div>

<div class="list_terms">
    <div class="list_terms_inner" id="list_terms_<?php echo $taxonomy['id']; ?>"></div>
</div>
<div class="selected_terms">
    <div class="selected_terms_inner" id="selected_terms_<?php echo $taxonomy['id']; ?>"></div>
</div>

<?php
$this->endWidget('bootstrap.widgets.TbBox');
