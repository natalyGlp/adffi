<div>
	<?php echo $form->labelEx($model,'read_also'); ?>
	<?php echo $form->dropDownList($model, 'read_also', Object::model()->readAlso, array('empty' => '')); ?>
	<?php echo $form->error($model,'read_also'); ?>
</div>
<div>
	<?php echo $form->labelEx($model,'read_also_limit'); ?>
	<?php echo $form->textField($model, 'read_also_limit'); ?>
	<?php echo $form->error($model,'read_also_limit'); ?>
</div>


<div id="static_block">
	<label><?php echo _t('Enter content title');?></label>
	<?php echo CHtml::textField('autocomplete_posts', '', array('id' => 'autocomplete_posts'));?>
	<?php echo $form->hiddenField($model, 'read_also_ids');?>
	<table id="posts_table" class="table table-striped table-bordered table-condensed">
		<tr>
            <th><?php echo _t('Content');?></th>
            <th></th>
        </tr>
		<?php
			$articles = !empty($model->read_also_ids) && count(explode(',', $model->read_also_ids)) ?
						Yii::app()->cms_db
								  ->createCommand()
								  ->select(array('object_id', 'object_name'))
								  ->from('{{object}}')
								  ->where('object_status=1 AND object_id IN('.$model->read_also_ids.')')
								  ->queryAll(false) : array();

			foreach($articles as $article){
				echo '
				<tr data-value="'.$article[0].'" class="article-tr">
					<td>'.$article[1].'</td>
					<td><a class="del-post-btn" data-value="'.$article[0].'">'._t('Delete').'</a></td>
				</tr>';
			}
		?>
	</table>
	<script type="text/javascript">
		var postsFaindaer = function(){
			this.vals = [];
			this.recount_posts();
			this.init();
		};

		postsFaindaer.prototype.recount_posts = function(){
			var that = this;
			that.vals = [];

			$('.article-tr').each(function(i, el){
				var v = parseInt($(el).data('value'), 10);
				if(inArray(v, that.vals) == -1){
					that.vals[that.vals.length] = v;
				}
			});

			this.appendValues();
		};

		postsFaindaer.prototype.appendValues = function(){
			$('#<?php echo get_class($model);?>_read_also_ids').val(this.vals.join(','));
		};

		postsFaindaer.prototype.delPost = function(){
			var that = this;

			$('.del-post-btn').click(function(){
				$('tr.article-tr[data-value="'+$(this).data('value')+'"]').remove();

				that.recount_posts();
			});
		};

		postsFaindaer.prototype.init = function(){
			this.delPost();
			var that = this;

			$( "#autocomplete_posts" ).autocomplete({
		      	source: "/beobject/autocompleteobject/?site=<?php echo isset($_GET['site']) ? $_GET['site'] : '';?>",
		      	minLength: 2,
		      	select: function( event, ui ) {
		      		if(inArray(ui.item.id, that.vals) == -1){
			        	$('#posts_table').append(
			        		$('<tr data-value="'+ui.item.id+'" class="article-tr"></tr>').append($('<td>'+ui.item.label+'</td>'))
			        																	 .append(
			        																	 	$('<td></td>')
			        																	 	.append($('<a class="del-post-btn" data-value="'+ui.item.id+'"><?php echo _t('Delete');?></a>'))
			        																	 )

			        	);

						that.recount_posts();
			        	that.delPost();
			        }else{
			        	alert('<?php echo _t('You have already selected the item.');?>');
			        }

			        this.value = '';
		        	return false;
		      	}
		    });
		};

		$(document).ready(function(){
			new postsFaindaer();
		});
	</script>
</div>
