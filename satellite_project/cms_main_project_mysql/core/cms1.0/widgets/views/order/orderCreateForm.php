<?php
$order = Order::model();
$priceField = '<div class="input-prepend input-append">
  <a class="add-on currencySymbol" href="#"></a>
  ' . CHtml::activeTextField($order, 'totalPrice', array(
    'class' => 'orderPrice',
)) . '
  <a class="add-on js-recalculate" href=""><i class="icon-refresh"></i></a>
</div>';

return array(
    'buttons' => array(
        'submit' => array(
            'type' => 'success',
            'buttonType' => 'submit',
            'label' => Yii::app()->controller->action->id == 'update' ? 'Сохранить' : 'Оформить',
            'htmlOptions' => array(
                'class' => 'submit',
                'id' => 'submit',
                'data-loading-text' => 'Loading...',
            ),
        )
    ),
    'activeForm' => array(
        'class' => 'bootstrap.widgets.TbActiveForm',
        'id' => 'orderManageForm',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            // TODO: необходимо как-то вынести этот элемент либо в отдельный файл либо придумать как подловить через js событие успешной валидации. как вариант писать в aftervalidate колбек функцию. сейчас этот же код скопипасчен для вьюхи settings контроллера BeshopController

            // обработчики события afterValidate должны возвращать false, в том случае, если валидация прошла успешно
            'afterValidate' => 'js:function(form, data, hasError) {
        var afterValidateHasErrors = $("body").triggerHandler("afterValidate", [form, data, hasError]);
        var isNewOrder = ' . ((Yii::app()->controller->action->id == 'create') * 1) . ';

        //if no error in validation, send form data with Ajax
        if (!hasError) {
          $.ajax({
            type: "POST",
            url: form[0].action,
            context: form[0],
            data: $(form).serialize()+"&submit=1",
            success: function(response) {
              if (isNewOrder) {
                $(this).closest(".modal").find(".close").click();
              } else {
                $(this).parent().replaceWith($(response));
              }

              $.fn.yiiGridView.update("orderIndex"); // обновим табличку
              $.sticky("Заказ был успешно обновлен", {autoclose : 5000, position: "top-center", type: "st-success" });
            },
            error: function() {
              $.sticky("Во время обработки заказа возникли ошибки", {autoclose : 5000, position: "top-center", type: "st-error" });
            }
          });

          // блочим отправку формы, так как мы сделали это через аякс
          return false;
        }
        return (!hasError && !afterValidateHasErrors)
    }',
        ),
    ),
    'elements' => array(
        'check' => array(
            'type' => 'form',
            'title' => 'Товары',
            'elements' => array(
                'prod_id' => array(
                    'type' => 'text',
                    'label' => 'Код/название товара',
                    'attributes' => array(
                        'name' => 'Object[object_id]',
                        'value' => '',
                        'placeholder' => 'Введите код/название товара',
                    ),
                ),
                '<div id="checkContainer"></div>',
            ),
        ),
        '<fieldset><legend>Сумма заказа</legend>' . $priceField . '</fieldset>',
        'client' => array(
            'type' => 'form',
            'title' => 'Данные клиента',
            'elements' => array(
                //'<a href="" class="icon_view"></a>',
                'first_name' => array(
                    'type' => 'text',
                ),
                'last_name' => array(
                    'type' => 'text',
                ),
                'email' => array(
                    'type' => 'text',
                ),
            ),
        ),
        'address' => array(
            'type' => 'form',
            'title' => 'Адрес клиента',
            'elements' => array(
                CHtml::textField('address_autocomplete', '', array(
                    'id' => 'address-autocomplete',
                    'placeholder' => _t('Enter client full adress...'),
                )),
                'country' => array(
                    'type' => 'text',
                ),
                'city' => array(
                    'type' => 'text',
                ),
                'telephone' => array(
                    'type' => 'cms.extensions.maskedInput.MaskedInputWidget',
                ),
                'street' => array(
                    'type' => 'text',
                ),
                'house' => array(
                    'type' => 'text',
                ),
                'flat' => array(
                    'type' => 'text',
                ),
            ),
        ),
        'order' => array(
            'type' => 'form',
            'title' => 'Оплата и доставка',
            'showErrorSummary' => true,
            'elements' => array(
                $priceField,
                'type' => array(
                    'type' => 'dropdownlist',
                    'items' => $order->orderType,
                    'prompt' => '--Не выбрано--',
                ),
                'currency_id' => array(
                    'type' => 'dropdownlist',
                    'items' => CHtml::listData(Currency::model()->findAll(), 'id', 'code'),
                ),
                'currency_provider' => array(
                    'type' => 'dropdownlist',
                    'items' => CHtml::listData(CurrencyProvider::model()->findAll(), 'id', 'name'),
                ),
                'status' => array(
                    'type' => 'dropdownlist',
                    'items' => $order->orderStatus,
                ),
                'comment' => array(
                    'type' => 'textarea',
                ),
            ),
        ),
    ),
);
