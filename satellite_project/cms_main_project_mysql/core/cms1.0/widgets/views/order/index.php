<?php
/**
 * @var Order $orderModel
 * @var CActiveDataProvider $dataProvider
 * @var boolean $additionalFilters
 * @var string $id
 * @var string $listPayments
 */
$this->render('cms.widgets.views.ajaxModal');
?>
<div class="btn-toolbar">
    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'success',
        'encodeLabel' => false,
        'buttons'=>array(
            array(
                'label' => _t('Create new order'),
                'url' => Yii::app()->controller->createUrl('create'),
                'icon' => 'icon-shopping-cart icon-white',
                'htmlOptions' => array(
                    'class' => 'ajaxInfo',
                    ),
                ),
        ),
    )); ?>

    <?= CHtml::textField('Order[orderSearch]', '', array(
        'id'=>'order-search',
        'style' => 'margin-left: 10px; top: 5px; position: relative;',
        'placeholder' => 'Search in orders...'
    )); ?>

    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'',
        'htmlOptions' => array(
            'class' => 'pull-right',
            ),
        'encodeLabel' => false,
        'buttons'=>array(
            array(
                'label' => _t('Export orders'),
                'icon' => 'download',
                'items'=> array(
                    array('label' => _t('All'), 'url' => Yii::app()->controller->createUrl('export')),
                    array('label' => _t('Selected'), 'url' => Yii::app()->controller->createUrl('export'), 'linkOptions' => array(
                        'onclick' => 'location.href=this.href+"?Order[id]="+$("#'.$id.'").yiiGridView("getChecked", "orderIndex_c0").join(",");return false;'
                    ))
                ),
            ),
        ),
    )); ?>
</div>

<?php
$statusFilters = '';
if (!$additionalFilters) {
    echo '<p id="statusFilters" style="text-align: left; float: left;">';
    // отображаем фильтр по статусу заказа
    $allStatuses = $orderModel->orderStatus;
    $statusIsChecked = $orderModel->statusArr;

    echo 'Фильтр по статусу: ';
    foreach ($allStatuses as $statusId => $statusName) {
        echo '<span class="filters label status_' . $statusId . '">' . CHtml::checkBox('Order[statusArr][' . $statusId . ']', in_array($statusId, $statusIsChecked), array(
            // 'onchange'=>'this.form.submit();',
            'value'=>$statusId,
        )) .
        CHtml::label($statusName, 'Order_statusArr_' . $statusId) . '</span> ';
    }
    echo '</p>';
} ?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped condensed bordered',
    'dataProvider' => $dataProvider,
    'filter' => $orderModel,
    'filterSelector'=>'{filter}, #statusFilters input, #order-search',
    'id' => $id,
    'rowCssClassExpression' => '!empty($data->comment) ? "warning" : ""',
    'enableHistory' => true,
    'columns' => array(
        array(
            'name' => 'gridSelectedItems',
            'class'=>'CCheckBoxColumn',
            'selectableRows' => 2,
            ),
        array(
            'name' => 'id',
            'headerHtmlOptions' => array('style' => 'width: 50px;'),
            ),
        array(
            'name' => 'Товары',
            'filter' => false,
            'value'=> 'Yii::app()->controller->renderPartial("cms.widgets.views.order._orderGridPartials", array(
                    "order" => $data,
                    "type" => "thumbs",
                ), true)',
            'type'=>'raw',
        ),
        array(
            'name'=>'Цена заказа',
            'filter' => false,//CHtml::activeDropDownList($orderModel,'price',array('empty'=>'---')),
            'value'=>'CHtml::link(
                $data->getTotalPrice(true),
                Yii::app()->controller->createUrl("check", array("id" => $data->primaryKey)),
                array("class"=>"ajaxInfo")
            )
            ."<br>".
            $data->getTotalInternalPrice(true)
            ',
            'type'=>'raw',
        ),
        array(
            'name'=>'Имя/Фамилия',
            'filter' => false,
            'value'=> 'Yii::app()->controller->renderPartial("cms.widgets.views.order._orderGridPartials", array(
                    "order" => $data,
                    "type" => "client",
                ), true)',
            'type'=>'raw',
        ),
        array(
            'name'=>'type',
            'filter' => CHtml::activeDropDownList($orderModel, "type", $orderModel->getOrderType(), array('empty'=>'---')),
            'value'=>'$data->orderTypeString',
        ),
        array(
            'name'=>'currency_provider',
            'type'=>'raw',
            'filter' => $listPayments,
            'value'=>'$data->payMethod',
        ),
        array(
            'header'=>'Контакты',
            'headerHtmlOptions' => array('style' => 'min-width: 160px;'),
            'value'=> 'Yii::app()->controller->renderPartial("cms.widgets.views.order._orderGridPartials", array(
                    "order" => $data,
                    "type" => "clientContacts",
                ), true)',
             'type'=>'raw',
        ),
        array(
            'name' => 'referer',
            'type' => 'raw',
            'filter' => CHtml::activeDropDownList($orderModel, "referer", $orderModel->getReferers(), array('empty'=>'---')),
            'value' => 'CHtml::link($data->referer, "http://".$data->referer, array("target"=>"_blank"))',
            ),
        array(
            'name'=>'operator_id',
            'filter' => false,
            'type' => 'raw',
            'value'=> 'Yii::app()->controller->renderPartial("cms.widgets.views.order._orderGridPartials", array(
                    "order" => $data,
                    "type" => "operator",
                ), true)',
            'visible' => Yii::app()->controller->action->id != 'operator',
        ),
        array(
            'name' => 'date',
            // 'type' => 'datetime',
            'filter' => $this->widget('cms.extensions.daterangepicker.JuiDateRangePicker', array(
                'model' => $orderModel,
                'attributeTo' => 'filterDateTo',
                'attributeFrom' => 'filterDateFrom',
                ), true),
        ),
        array(
            'name'=>'status',
            'filter' => false,
            'value'=> 'Yii::app()->controller->renderPartial("cms.widgets.views.order._orderGridPartials", array(
                    "order" => $data,
                    "type" => "status",
                ), true)',
            'type'=>'raw',
            'cssClassExpression' => '"statusCell status_". $data->status',
            'htmlOptions' => array(
                'style' => 'width:125px',
                ),
        ),

        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update} {export} {print} {more}',
            'buttons'=>array(
                'update' => array(
                    'options'=>array(
                        'class'=>'ajaxInfo',
                    ),
                ),
                'export' => array(
                    'label'=>_t('Export row'),
                    'icon'=>'download',
                    'url'=> 'Yii::app()->controller->createUrl("export", CMap::mergeArray(
                        $_GET,
                        array(
                            "Order" => array(
                                "id"=>$data->primaryKey
                                )
                            )
                        ))',
                ),
                'print' => array(
                    'label'=>_t('Print'),
                    'icon'=>'print',
                    'url'=> 'Yii::app()->controller->createUrl("print", array("id"=>$data->primaryKey))',
                    'visible' => 'false', // TODO: сделать генерацию квитанций
                ),
                'more' => array(
                    'icon'=>'comment',
                    'url'=> 'Yii::app()->controller->createUrl("more", array("id"=>$data->primaryKey))',
                    'visible' => '!empty($data->comment)',
                    'options'=>array(
                        'class'=>'ajaxInfo',
                    ),
                ),
            ),
        ),
    ),
));
