<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'userupdate-form',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
        ));
        ?>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textField($model,'first_name'); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'last_name'); ?>
        <?php echo $form->textField($model,'last_name'); ?>
        <?php echo $form->error($model,'last_name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'display_name'); ?>
        <?php echo $form->textField($model,'display_name'); ?>
        <?php echo $form->error($model,'display_name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'user_url'); ?>
        <?php echo $form->textField($model,'user_url'); ?>
        <?php echo $form->error($model,'user_url'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password'); ?>
        <?php echo $form->error($model,'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status',ConstantDefine::getUserStatus()); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <div class="clearfix">
        <?php echo $form->label($model,'image'); ?>
        <?php if(!empty($model->avatar)):
        $sizes = AvatarSize::getSizes();
        ?>
            <img style="float:none; margin-bottom:3px" src="<?php echo GxcHelpers::getUserAvatar($sizes[0]['id'], $model->avatar,'');?>" class="avatar profile_photo"/>
            <a style="" href="javascript:void(0);" onClick="return removeAvatar();"><?php echo _t('Remove Avatar'); ?></a>
        <?php endif; ?>
        <?php echo $form->fileField($model,'image',array('style'=>'display:block; margin-top:15px')); ?>
        <?php echo $form->error($model,'image',array('style'=>'width: 540px;')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'bio'); ?>
        <?php echo $form->textArea($model,'bio'); ?>
        <?php echo $form->error($model,'bio'); ?>
    </div>

    <script type="text/javascript">
    function removeAvatar(){
        if(confirm('<?php echo _t('Are you sure you want to remove current Avatar?') ?>')){
            $.post("",
                {ResetAvatar: "true", YII_CSRF_TOKEN: "<?php echo Yii::app()->getRequest()->getCsrfToken(); ?>"},
                function(data) {
                    location.reload();
                }
            ).fail(function() {
                alert('<?php echo _t('There was an error while deleting avatar') ?>');
            });
        }
    }
    </script>

    <div class="row buttons">
        <?php echo CHtml::submitButton(_t('Save'),array('class'=>'bebutton')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
