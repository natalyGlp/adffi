<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'user-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'user_id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->user_id',
        ),

        array(
            'name' => 'fullName',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => 'CHtml::link($data->fullName,array("' . Yii::app()->controller->id . '/view","id"=>$data->user_id))',
        ),
        array(
            'name' => 'email',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => '$data->email',
        ),

        array(
            'name' => 'status',
            'type' => 'image',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => 'User::convertUserState($data)',
            'filter' => false,
        ),
        array(
            'header' => _t('Roles'),
            'type' => 'raw',
            'value' => 'User::getStringRoles($data->user_id)',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/update", array("id"=>$data->user_id))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => _t('Delete'),
                    'imageUrl' => false,
                ),
            ),
        ),
    ),
));
