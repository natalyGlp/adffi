<?php
/**
 * @var $createBlockUrl
 * @var $updateBlockUrl
 * @var $suggestBlockUrl
 * @var $inheritParentUrl
 * @var $regions
 * @var $regionsBlocks
 */
?>

<div class="regions-widget" style="margin:30px auto">
    <div class="row">
        <ul id="page-regions-list" class="nav nav-tabs"></ul>
        <div id="page-regions-wrapper"></div>
    </div>
</div>

<script type="text/template" id="template-tab">
    <li><a rel="div_region_{{id}}" href="#">{{title}}</a></li>
</script>

<script type="text/template" id="template-region">
    <div class="extra_block_buttons_wrap">
        <div class="extra_block_buttons btn-group">
            <label class="btn">
                <input type="checkbox" name="check-all" class="check-all-button js-check-all-blocks" value="" style="margin: 0;">
            </label>
            <input type="button" class="btn js-active-checked-blocks" value="<?php echo _t('Active'); ?>" />
            <input type="button" class="btn js-disable-checked-blocks" value="<?php echo _t('Disable'); ?>" />
            <input type="button" class="btn js-delete-checked-blocks" value="<?php echo _t('Delete'); ?>" />
        </div>
    </div>
    <ul class="ul_region" id="ul_region_{{id}}"></ul>
    <div style="display:none; border:1px dotted #CCC" id="div_iframe_region_{{id}}">
        <iframe rel="{{id}}" id="iframe_region_{{id}}" class="region-iframe" width="100%" onLoad="autoResize(this)"  />
    </div>
    <div class="region-actions">
        <input type="button" class="btn js-find-block" name="add-existed-block" value="<?php echo _t('Add existed Block'); ?>" />
        <input type="button" class="btn js-new-block" name="add-new-block" value="<?php echo _t('Add new Block'); ?>" />
    </div>
</script>

<script type="text/template" id="template-block">
    <input type="hidden" value="{{id}}" name="<?php echo CHtml::activeName($model, $attribute); ?>[{{region}}][id][]" />
    <input class="js-block-status" type="hidden" value="{{status}}" name="<?php echo CHtml::activeName($model, $attribute); ?>[{{region}}][status][]" />
    <div class="toggle-block pull-right">
        <input class="js-block-status" type="checkbox" checked="checked" />
    </div>
    <div>
        <input type="checkbox" class="select-block">
        {{type}}: {{title}}
    </div>
    <p>
        <a class="js-change" href="#"><?php echo _t('Change');?></a>
        <a class="js-edit" href="#"><?php echo _t('Edit');?></a>
        <a class="js-remove" href="#"><?php echo _t('Delete');?></a>
    </p>

    <div style="display:none" class="iframe-container">
        <iframe rel="{{region}}" id="iframe_region_{{index}}_{{region}}_{{id}}" width="100%" onLoad="autoResize(this)" />
    </div>
</script>

<script type="text/javascript">
    $(function() {
        function initBlocks() {
            <?php
            foreach ($regionsBlocks as $regionId => $blocksInRegion) {
                if (!isset($blocksInRegion['id']) || empty($blocksInRegion['id'])) {
                    continue;
                }

                $blocks = Block::model()->findAllByPk($blocksInRegion['id'], array(
                    'order' => 'FIELD(block_id, ' . implode(', ', $blocksInRegion['id']) . ')',
                    ));

                $statusByPk = array();
                foreach ($blocksInRegion['id'] as $i => $pk) {
                    $statusByPk[$pk] = $blocksInRegion['status'][$i];
                }

                foreach ($blocks as &$block) {
                    $block = array(
                        'region' => $regionId,
                        'id' => $block->block_id,
                        'type' => $block->type,
                        'title' => $block->name,
                        'status' => $statusByPk[$block->block_id],
                    );
                }

                ?>
                    regions.insertBlocks(<?php echo CJavaScript::encode($blocks) ?>);
                <?php
            }
            ?>
        }

        Region.suggestBlockUrl = '<?php echo $suggestBlockUrl; ?>';
        Region.createBlockUrl = '<?php echo $createBlockUrl; ?>';
        Region.updateBlockUrl = '<?php echo $updateBlockUrl; ?>';
        regions.turnBack = initBlocks;

        regions.create(<?php echo CJavaScript::encode($regions); ?>);
        regions.turnBack();
    });
</script>
