<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'=>'comment-grid',
    'enableHistory' => true,
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'summaryText'=>_t('Displaying').' {start} - {end} '._t('in'). ' {count} '._t('results'),
    'columns'=>array(
        'id',
        'index_id',
        'prettyLastObjectDate',
        'prettyModifiedDate',
        'prettyCreatedDate',
        'documentsCount',
        array(
            'class'=>'CButtonColumn',
            'template'=>'{update}',
            'buttons'=>array(
                'update' => array(
                    'label'=>_t('Update index'),
                    'imageUrl'=>false,
                    'url'=> 'Yii::app()->controller->createUrl("update", array("id"=>$data->primaryKey))',
                ),
            ),
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{optimize}',
            'buttons'=>array(
                'optimize' => array(
                    'label'=>_t('Optimize index'),
                    'imageUrl'=>false,
                    'url'=> 'Yii::app()->controller->createUrl("optimize", array("id"=>$data->primaryKey))',
                ),
            ),
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{rebuild}',
            'buttons'=>array(
                'rebuild' => array(
                    'label'=>_t('Rebuild index'),
                    'imageUrl'=>false,
                    'url'=> 'Yii::app()->controller->createUrl("rebuild", array("id"=>$data->primaryKey))',
                ),
            ),
        ),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{delete}',
            'buttons'=>array(
                'delete' => array(
                    'label'=>_t('Delete index'),
                    'imageUrl'=>false,
                    'url'=> 'Yii::app()->controller->createUrl("delete", array("id"=>$data->primaryKey))',
                ),
            ),
        ),
    ),
)); ?>

<script>
$(function() {
    var Progress = function(data) {
        var private = {
            $progressBar: {},
            $container: {},
            id: '',
        },
        construct = function(obj)
        {
            private.id = data.id;
            var $tableTr = $('table.items td:contains("'+private.id+'")').parent();

            var $tableTd = $tableTr.children().eq(1);

            private.$progressBar = $('<div id="progress_'+private.id+'">').css({'margin': '10px'});

            private.$container = $('<div>').css({
                'background': 'rgba(255,255,255,0.7)',
                'position': 'absolute',
                'marginTop': '-' + $tableTd.css('paddingTop'),
                'marginLeft': '-' + $tableTd.css('paddingLeft'),
                'width': $tableTr.width() - $tableTd.outerWidth(), // Отнимаем ширину колонки с id индекса
                'height': $tableTr.height()
            })
            .prependTo($tableTd)
            .append(private.$progressBar);

            private.$progressBar.progressbar();
            obj.update(data);
        };

        var obj = {
            update: function(data)
            {
                if(data['progress'] != undefined)
                    private.$progressBar.progressbar({value: parseFloat(data.progress)});
            },

            finish: function()
            {
                private.$progressBar.progressbar({value: 100});
            },

            destroy: function()
            {
                private.$container.remove();
            }
        }

        construct(obj);

        return obj;
    };

    var interval = false;
    var progresses = {};
    function checkProgress() {
        $.ajax('<?php echo $this->controller->createUrl('queue') ?>', {
            dataType: 'json',
            success: function(data)
            {
                if (data != '')
                {
                    for(var indexId in data)
                    {
                        if(progresses[indexId] == undefined)
                            progresses[indexId] = new Progress(data[indexId]);
                        else
                            progresses[indexId].update(data[indexId]);
                    }

                    if(!interval)
                        interval = setInterval(checkProgress, 3000);
                } else {
                    if(interval)
                    {
                        clearInterval(interval);
                        interval = false;
                    }
                }



                // обрабатываем завершенные
                for(var indexId in progresses)
                {
                    if(data[indexId] == undefined)
                    {
                        progresses[indexId].finish();
                    }
                }
            }
        });
    }

    // запускаем проверялку прогресса
    checkProgress();
});
</script>
