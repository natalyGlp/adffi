<?php
/**
 * @var Block $model
 * @var array $blocksList
 */
?>
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id' => 'block-start-form',
        'method' => 'GET',
        'enableAjaxValidation' => false,
    )); ?>
        <?php echo $form->errorSummary($model); ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'type'); ?>
            <?php echo $form->dropDownList($model, 'type', $blocksList, array(
                'name' => 'type',
            )); ?>
            <?php echo $form->error($model, 'type'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton('Next', array('class'=>'btn')); ?>
        </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->
