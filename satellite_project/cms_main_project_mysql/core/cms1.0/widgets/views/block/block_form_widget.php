<?php
/**
 * @var Block $model
 * @var BlockWidget $blockWidget
 */
$type = $model->type;
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'block-form',
    'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <?php echo _t('Block type').': <strong>'.$type.'</strong><br/><br/>';?>
</div>
<div class="row">
    <?php echo $form->labelEx($model, 'name'); ?>
    <?php echo $form->textField($model, 'name'); ?>
    <?php echo $form->error($model, 'name'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($blockWidget, 'cssClass'); ?>
    <?php echo $form->textField($blockWidget, 'cssClass'); ?>
    <?php echo $form->error($blockWidget, 'cssClass'); ?>
</div>
<?php
    $blockWidget->render('input', array(
        'model' => $model,
        'block_model' => $blockWidget,
        'form' => $form,
    ));
?>
<script>
    <?php
    foreach ($blockWidget->errors as $key => $error) {
        echo "$('#Block-".$key."'".").attr('class','error')";
    }
    ?>
</script>
<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Save'), array('class' => 'btn')); ?>
</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
