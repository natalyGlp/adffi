<?php
$globalFilters = '<span class="filters">';
$globalFilters .= CHtml::dropDownList('runtime', isset($_GET['runtime']) ? $_GET['runtime'] : '', $runtimes, array('style' => 'float:left; width: 220px;'));
$globalFilters .= CHtml::dropDownList('log', isset($_GET['log']) ? $_GET['log'] : '', $availableLogs, array('style' => 'float:left; width: 220px; margin-left: 10px;'));
$globalFilters .= '</span>';

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $dataProvider,
    'filter' => $filtersForm,
    'summaryText' => $globalFilters . Yii::t('zii', 'Displaying {start}-{end} of {count} result(s).'),
    'columns' => array(
        array(
            'name' => '1',
            'header' => 'Дата',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data[1])',
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $filtersForm,
                'attribute' => '1',
                'language' => Yii::app()->language,
                'defaultOptions' => array(
                    'showOn' => 'focus',
                    'showOtherMonths' => true,
                    'selectOtherMonths' => true,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showButtonPanel' => true,
                    'autoSize' => true,
                    'dateFormat' => "yy/mm/dd",
                ),
                'options' => array(
                    'dateFormat' => "yy/mm/dd",
                ),
            ), true),
        ),
        array(
            'name' => '2',
            'header' => 'Уровень',
            'filter' => array(
                'error' => 'error',
                'warning' => 'warning',
                'info' => 'info',
                'trace' => 'trace',
                'profile' => 'profile',
            ),
            'cssClassExpression' => '"level_" . $data[2]',
        ),
        array(
            'name' => '3',
            'header' => 'Категория',
            'filter' => $logCategories,
        ),
        array(
            'name' => '4',
            'header' => 'Сообщение',
            'value' => '"<pre style=\"height:100px;overflow-y:auto;\">".CHtml::encode(wordwrap($data[4]))."</pre>"',
            'type' => 'raw',
        ),
    ),
    'ajaxUpdate' => false,
));
