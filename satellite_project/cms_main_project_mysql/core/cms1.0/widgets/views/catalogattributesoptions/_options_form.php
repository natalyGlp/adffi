<tr id="tr_<?php echo $i;?>" class="field_tr">
	<td>
		<b><?php echo _t('Name');?></b><br/>
		<?php echo CHtml::textField('CatalogAttributesOptions[options]['.$i.'][name]', isset($v['name']) ? $v['name'] : ''); ?>
	</td>
    <td>
        <b><?php echo _t('Category');?></b><br/>
        <?php
            echo CHtml::dropDownList(
                'CatalogAttributesOptions[options]['.$i.'][category_id]',
                (isset($v['category_id']) ? $v['category_id'] : ''),
                CHtml::listData(CatalogAttributesCategories::model()->findAll(), 'id', 'name')
            );
        ?>
    </td>
	<td>
		<b><?php echo _t('Type');?></b><br/>
		<?php
            echo CHtml::dropDownList(
                'CatalogAttributesOptions[options]['.$i.'][type]',
                (isset($v['type']) ? $v['type'] : ''),
                CatalogAttributesOptions::model()->types,
                array('class' => 'types_select', 'data-value' => $i)
            ); ?>
	</td>
    <td>
        <div class="type_list_place" style="display:none;">
            <b><?php echo _t('List Items');?></b><br/>
            <table id="type_select_<?php echo $i;?>">
            <?php if (isset($v['list_data']) && is_array($v['list_data'])): ?>
    	        <?php foreach ($v['list_data'] as $j => $value): ?>
                    <tr id="str_<?php echo $i;?>_<?php echo $j;?>">
                        <td>
                            <?php
                                echo CHtml::textField(
                                    'CatalogAttributesOptions[options]['.$i.'][list_data][]',
                                    $value
                                );
                            ?>
                        </td>
                        <td>
                            <a href="#str_<?php echo $i;?>_<?php echo $j;?>" class="del-button btn btn-danger"><?php echo _t('Remove');?></a>
                        </td>
                    </tr>
    	        <?php endforeach;?>
            <?php else:?>
                <tr id="str_<?php echo $i;?>_<?php echo (isset($v['list_data']) ? count($v['list_data']) + 1 : 0);?>">
                    <td>
                        <?php
                            echo CHtml::textField(
                                'CatalogAttributesOptions[options]['.$i.'][list_data][]',
                                ''
                            );
                        ?>
                    </td>
                    <td>
                        <a href="#str_<?php echo $i;?>_<?php echo (isset($v['list_data']) ? count($v['list_data']) + 1 : 0);?>" class="del-button btn btn-danger"><?php echo _t('Remove');?></a>
                    </td>
                </tr>
            <?php endif;?>
            </table>
            <a data-value="<?php echo $i;?>" href="#type_select_<?php echo $i;?>" class="add-select-field btn btn-success"><?php echo _t('Add field');?></a>
        </div>
    </td>
	<td>
		<a href="#tr_<?php echo $i;?>" class="del-button btn btn-danger"><?php echo _t('Remove');?></a>
	</td>
</tr>
