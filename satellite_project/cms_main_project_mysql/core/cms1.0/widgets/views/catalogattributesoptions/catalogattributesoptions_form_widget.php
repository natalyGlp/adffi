<div class="form">
<?php
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'language-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype'=>'multipart/form-data')
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <table id="table_fields" width="100%">
        <?php if (count($model->options)): ?>
            <?php foreach($model->options as $i => $v):?>
                <?php $this->render('cms.widgets.views.catalogattributesoptions._options_form', array('v' => $v, 'i' => $i));?>
            <?php endforeach;?>
        <?php else:?>
            <?php
                $this->render('cms.widgets.views.catalogattributesoptions._options_form', array(
                    'v' => null,
                    'i' => count($model->options)+1
                ));
            ?>
        <?php endif;?>
    </table>

    <div class="row buttons">
        <a href="javascript://" class="add-field btn btn-success"><?php echo _t('Add field');?></a>
    </div>

    <br clear="all"/>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn')); ?>
    </div>

    <script type="text/javascript">
        var type_selects = '';
        <?php foreach(CatalogAttributesOptions::model()->types as $k => $v):?>
            type_selects += '<option value="<?php echo $k;?>"><?php echo $v;?></option>';
        <?php endforeach;?>

        function init_del_btn(){
            $('.del-button').click(function(e){
                e.preventDefault();
                $($(this).attr('href')).remove();
                return false;
            });
        }

        function init_types_select(){
            $('.types_select').change(function(e){
                e.preventDefault();
                $('#tr_'+$(this).data('value')+' .type_list_place').hide();
                if($(this).val() == 2 || $(this).val() == 3){
                    $('#tr_'+$(this).data('value')+' .type_list_place').show();
                }

                return false;
            }).change();
        }

        function init_add_select_btn(selector){
            $(selector).click(function(e){
                e.preventDefault();
                var filter = $(this).data('value'),
                    tr_cnt = $('#type_select_'+filter+' tr').length + 1;

                $('#type_select_'+filter).append(
                    $(
                        '<tr id="str_'+tr_cnt+'_0">'+
                            '<td><input type="text" name="CatalogAttributesOptions[options]['+filter+'][list_data][]" /></td>'+
                            '<td>'+
                                '<a href="#str_'+tr_cnt+'_0" class="del-button btn btn-danger"><?php echo _t('Remove');?></a>'+
                            '</td>'+
                        '</tr>'
                    )
                );

                init();
            });
        }

        function init(){
            init_types_select();
            init_del_btn();

            $('.field_tr').removeClass('odd');
            $('.field_tr:nth-child(odd)').addClass('odd');

            $('.type_list_place tr').removeClass('odd');
            $('.type_list_place tr:nth-child(odd)').addClass('odd');
        }

        $(document).ready(function(){
            $(".add-field").click(function(e){
                e.preventDefault();
                var filter = $(this).data('value'),
                    tr_cnt = $('#table_fields tr.field_tr').length + 1;

                $('#table_fields').append(
                    $(
                        '<tr id="tr_'+tr_cnt+'" class="field_tr">'+
                            '<td>'+
                                '<b><?php echo _t('Name');?></b><br/>'+
                                '<input type="text" name="CatalogAttributesOptions[options]['+tr_cnt+'][name]" />'+
                            '</td>'+
                            '<td>'+
                                '<b><?php echo _t('Category');?></b><br/>'+
                                '<?php echo preg_replace('/(\r|\n)/i', '', CHtml::dropDownList('CatalogAttributesOptions[options][{index}][category_id]', '', CHtml::listData(CatalogAttributesCategories::model()->findAll(), 'id', 'name'))); ?>'.replace(/\{index\}/ig, tr_cnt)+
                            '</td>'+
                            '<td>'+
                                '<b><?php echo _t('Type');?></b><br/>'+
                                '<select class="types_select" data-value="'+tr_cnt+'" name="CatalogAttributesOptions[options]['+tr_cnt+'][type]">'+type_selects+'</select>'+
                            '</td>'+
                            '<td>'+
                                '<div class="type_list_place" style="display:none;">'+
                                    '<b><?php echo _t('List Items');?></b><br/>'+
                                    '<table id="type_select_'+tr_cnt+'">'+
                                        '<tr id="str_'+tr_cnt+'_0">'+
                                            '<td><input type="text" name="CatalogAttributesOptions[options]['+tr_cnt+'][list_data][]" /></td>'+
                                            '<td>'+
                                                '<a href="#str_'+tr_cnt+'_0" class="del-button btn btn-danger"><?php echo _t('Remove');?></a>'+
                                            '</td>'+
                                        '</tr>'+
                                    '</table>'+
                                    '<a data-value="'+tr_cnt+'" href="#type_select_'+tr_cnt+'" class="add-select-field btn btn-success"><?php echo _t('Add field');?></a>'+
                                '</div>'+
                            '</td>'+
                            '<td>'+
                                '<a href="#tr_'+tr_cnt+'" class="del-button btn btn-danger"><?php echo _t('Remove');?></a>'+
                            '</td>'+
                        '</tr>'
                    )
                );

                init();
                init_add_select_btn('a[data-value='+tr_cnt+']');
            });

            init();
            init_add_select_btn('.add-select-field');
        });
    </script>
<?php $this->endWidget(); ?>

</div><!-- form -->
