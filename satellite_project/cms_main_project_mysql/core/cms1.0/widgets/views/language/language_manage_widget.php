<?php
$site = Yii::app()->controller->site;
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'term-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'lang_id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->lang_id',
        ),
        array(
            'name' => 'lang_desc',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => 'CHtml::link($data->lang_desc,array("' . Yii::app()->controller->id . '/update","id"=>$data->lang_id, "site" => "' . $site . '"))',
        ),
        'lang_name',
        'lang_short',
        array(
            'name' => 'lang_active',
            'type' => 'raw',
            'value' => '$data->getStatus()',
        ),
        array(
            'name' => 'lang_required',
            'type' => 'raw',
            'value' => '$data->getDefaultLang()',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/update", array("id"=>$data->lang_id, "site" => "' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => _t('Delete'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/delete", array("id"=>$data->lang_id, "site" => "' . $site . '"))',
                ),
            ),
        ),
    ),
));
