<?php if (Yii::app()->user->hasFlash('success') && isset($_GET['embed'])): ?>
    <script type="text/javascript">
        parent.$(frameElement)
            .trigger('add', {name: '<?php echo CHtml::encode($model->name); ?>', id: '<?php echo $model->content_list_id ?>'})
            .trigger('reset')
            ;
    </script>
<?php endif; ?>
<style type="text/css">.left{float:left;}</style>
<div class="form">
    <?php
        $form=$this->beginWidget('CActiveForm', array(
            'id'=>'contentlist-form',
            'enableAjaxValidation'=>true,
        ));
    ?>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size'=>60, 'maxlength'=>255)); ?>
        <?php echo $form->error($model, 'name'); ?>
        <div class="clear"></div>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php echo $form->dropDownList($model, 'type', ContentList::getTypesList(), array('id'=>'queue-type')); ?>
        <?php echo $form->error($model, 'type'); ?>
        <div class="clear"></div>
    </div>

<?php
// TYPE_MANUAL
?>

    <div id="params-<?php echo ContentList::TYPE_MANUAL; ?>" class="contentListContainer" style="display:none">
        <div class="content-box">
            <div class="content-box-header">
                <h3><?php echo _t('Params');?></h3>
            </div>

            <div class="tab-content" style="display: block; padding: 0 0 15px 0">
                <div class="default-tab">
                    <div class="row">
                        <?php echo $form->checkBox($model, 'forceObjectType', array(
                            'checked' =>  $model->forceObjectType != '0'
                        )); ?>
                        <?php echo $form->labelEx($model, 'forceObjectType'); ?>
                        <?php echo $form->error($model, 'forceObjectType'); ?>
                    </div>
                    <div class="row">
                        <div style="padding:15px">
                            <?php echo CHtml::label(_t('Add content'), ''); ?>
                            <?php $this->widget('CAutoComplete', array(
                                'mustMatch'=>true,
                                'name'=>'manual_list_name',
                                'url'=>array('suggestContent', 'site' => isset($_GET['site']) ? $_GET['site'] : ''),
                                'multipleSeparator'=>';',
                                'value'=>'',
                                'multiple'=>false,
                                'htmlOptions'=>array('size'=>50, 'class'=>'maxWidthInput', 'id'=>'form_content_list'),
                                'methodChain'=>".result(function(event,item){ if(item !== undefined )  setContentList(item[0],item[1]); \$(\"#form_content_list\").val('');})",
                            )); ?>

                            <span id="current_content_list_count" style="display:none">0</span>

                            <div class="clear"></div>
                        </div>
                        <ul id="content_list_manual"></ul>

                        <script type="text/javascript">
                        $(window.frameElement).trigger('reset')
                            <?php
                                if (is_array($model->manual_list) && !empty($model->manual_list) > 0): ?>
                                    //Start to get the content based on the ids
                                    <?php
                                        $content_items=array();
                                        foreach ($model->manual_list as $obj_id) {
                                            $temp_object=Object::model()->findByPk($obj_id);
                                            if ($temp_object) {
                                                $content_items['item_'.$temp_object->object_id]['id']=$temp_object->object_id;
                                                $content_items['item_'.$temp_object->object_id]['title']=$temp_object->object_name;
                                            }
                                        }

                                        echo 'var manual_content_list = '.json_encode($content_items).';';
                                    ?>
                                    $.each(manual_content_list, function(k,v) {
                                             setContentList(v.title,v.id);
                                    });
                            <?php endif; ?>
                            function setContentList(linkTitle,linkId){
                                //Update the number for the Upload Count;
                                var current_count=$('#current_content_list_count').html();
                                current_count=parseInt(current_count);
                                current_count++;
                                $('#current_content_list_count').html(current_count.toString());
                                var nextli='list_id_content_list_'+linkId;
                                var li='<li id=\"'+nextli+'\"><input type=\"hidden\" name=\"content_list_title[]\" id=\"input_title_'+nextli+'\" value=\"'+linkTitle+'\" /><input type=\"hidden\" name=\"content_list_id[]\" id=\"input_id_'+nextli+'\" value=\"'+linkId+'\" /><a href=\"<?php echo Yii::app()->controller->createUrl($this->object_update_url); ?>/'+linkId+'\" target="_blank">'+linkTitle+'</a> - <a href=\"javascript:void(0);\" onClick=\"deleteContentList(\''+nextli+'\');\">Delete</a></li>';
                                $('#content_list_manual').append(li);
                                return;

                            }

                            function deleteContentList(id){
                                 var current_count=$('#current_content_list_count').html();
                                 current_count=parseInt(current_count);
                                 current_count--;
                                 if(current_count<0) current_count=0;
                                 $("#"+id).remove();
                                 $('#current_content_list_count').html(current_count.toString());
                            }

                            $('#content_list_manual').sortable();
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
// TYPE_DYNAMIC
?>
    <?php
    // TODO: По сути для динамического списка нужно реализовать тот же набор параметров как и для автоматического,
    // грубо говоря динамический список это комбинация автоматического и ручного
    // При выполнении этого пункта нужно не забыть про то, что сейчас все ивенты вешаются на id, нужно будет переделать на class
    ?>
    <div id="params-<?php echo ContentList::TYPE_DYNAMIC; ?>" class="contentListContainer" style="display:none">
        <div class="content-box ">
            <div class="content-box-header">
                <h3><?php echo _t('Dynamic Content List');?></h3>
            </div>

            <div class="tab-content" style="display: block; padding: 0 0 15px 0">
                <div class="default-tab">
                    <div class="alert alert-info">
                        <p><?php echo _t('The dynamic list allows you to create such a list, that can be managed directly from the content editing page and specify wich content you want to see in this list from within.'); ?></p>
                        <p><?php echo _t('The name of this list will be also shown as the name of the block on the content editing page.'); ?></p>
                    </div>

                    <p><?php echo _t('You can restrict the content types, that are allowed to add to, in this list and max amount of content objects using the form below.'); ?></p>


                    <div class="row">
                        <?php echo $form->labelEx($model, 'number'); ?>
                        <?php echo $form->textField($model, 'number', array(
                            'value'=>$model->isNewRecord ? '10' : $model->number ,
                            'size'=>20,
                            'maxlength'=>20,
                            'style'=>'width:100px',
                            'id' => 'dynamicListNumber',
                        )); ?>
                        <?php echo $form->error($model, 'number'); ?>
                    </div>

                    <div class="row">
                        <?php echo $form->checkBox($model, 'forceObjectType', array('checked' =>  $model->forceObjectType != '0')); ?>
                        <?php echo $form->labelEx($model, 'forceObjectType'); ?>
                        <?php echo $form->error($model, 'forceObjectType'); ?>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                // прокидываем значение поля на вторую форму, так как у нас дублируются поля :(
                // TODO: нужно это исправить. пускай всех полей будет по 1 и просто ненужные уходят в display:none;
                $(function() {CopyString('#dynamicListNumber', '#ContentList_number', '')});
                $('[name=ContentList\\[forceObjectType\\]]').change(function() {
                    $('[name=ContentList\\[forceObjectType\\]]').attr("checked", $(this).is(':checked'));
                });
            </script>
        </div>
    </div>

<?php
// TYPE_AUTO
?>

    <div id="params-<?php echo ContentList::TYPE_AUTO; ?>" class="contentListContainer" style="display:none">
        <div class="closed-box content-box ">
            <div class="content-box-header">
                <h3><?php echo _t('Params');?></h3>
            </div>
            <div class="tab-content" style="display: block;">
                <div class="tab-content default-tab">
                    <div class="controls-row">
                        <div>
                            <div class="left">
                                <?php echo $form->labelEx($model, 'criteria'); ?>
                                <?php echo $form->dropDownList($model, 'criteria', ContentList::getCriteriasList(), array('style'=>'width:100px')); ?>
                                <?php echo $form->error($model, 'criteria'); ?>
                            </div>
                            <div class="left" style="margin-left:20px; margin-top: 25px;">
                                <?php echo $form->checkBox($model, 'showFutureDates'); ?>
                                <?php echo $form->labelEx($model, 'showFutureDates'); ?>
                                <?php echo $form->error($model, 'showFutureDates'); ?>
                            </div>
                            <div class="left" style="margin-left:20px; margin-top: 25px;">
                                <?php echo $form->checkBox($model, 'fallbackToMainLanguage'); ?>
                                <?php echo $form->labelEx($model, 'fallbackToMainLanguage'); ?>
                                <?php echo $form->error($model, 'fallbackToMainLanguage'); ?>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <?php $this->render('cms.widgets.views.contentlist._contentlist_objects_form', array(
                            'model' => $model,
                            'form' => $form,
                            'langAttribute' => 'lang',
                            'contentTypeAttribute' => 'content_type',
                            'termsAttribute' => 'terms',
                        )); ?>

                        <div class="row">
                            <?php echo $form->labelEx($model, 'tags'); ?>
                            <?php $this->widget('CAutoComplete', array(
                                    'model'=>$model,
                                    'mustMatch'=>true,
                                    'attribute'=>'tags',
                                    'url'=>array('suggestTags', 'site' => isset($_GET['site']) ? $_GET['site'] : ''),
                                    'multiple'=>true,
                                    'htmlOptions'=>array('size'=>50, 'style'=>'width:300px'),
                            )); ?>

                            <?php echo $form->error($model, 'tags'); ?>
                        </div>


                        <div>
                            <div class="left">
                                <?php echo $form->labelEx($model, 'number'); ?>
                                <?php echo $form->textField($model, 'number'); ?>
                                <?php echo $form->error($model, 'number'); ?>
                            </div>
                            <div class="left" style="margin-left:20px">
                                <?php echo $form->labelEx($model, 'paging'); ?>
                                <?php $this->widget('bootstrap.widgets.TbToggleButton', array(
                                    'form' => $form,
                                    'model' => $model,
                                    'attribute' => 'paging',
                                )); ?>
                                <?php echo $form->error($model, 'paging'); ?>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <?php if (isset($_GET['type'])): ?>
            <div class="row">
                <?php echo CHtml::submitButton(isset($_GET['action']) ? _t('Save') : _t('Add'), array('class'=>'btn')); ?>
            </div>
        <?php else: ?>
            <div class="row">
                <?php echo CHtml::submitButton($model->isNewRecord ? _t('Create') : _t('Save'), array('class'=>'btn')); ?>
                <div class="clear"></div>
            </div>
        <?php endif; ?>
    <?php $this->endWidget(); ?>
</div><!-- .form -->

<script>
    function checkQT(){
        var qtype=$('#queue-type').val();
        $('.contentListContainer').hide();
        $('#params-'+qtype).show();

        if(qtype==<?php echo ContentList::TYPE_AUTO ;?>){
            if($('#content_type_box').val()==null){
                $('#content_type_box').val('all');
            }
            if($('#content_lang_box').val()==null){
                $('#content_lang_box').val('0');
            }
            if($('#term_id').val()==null){
                $('#term_id').val('0');
            }

            $('#content_type_box').change();
        }
    }

    $('#queue-type').change(checkQT).change();
</script>
