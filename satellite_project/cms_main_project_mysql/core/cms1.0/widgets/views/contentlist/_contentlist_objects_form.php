<?php
/** 
 * Выводит форму с полями для выбора языка обьектов, типа обьектов и терминов таксономии
 *
 * @var  CActiveRecord|BlockWidget $model
 * @var  CActiveForm $form
 * @var  string $langAttribute
 * @var  string $contentTypeAttribute
 * @var  string $termsAttribute
 */
?>

<div class="row-fluid">
    <div class="span4">
        <?php echo $form->labelEx($model, $langAttribute); ?>
        <?php echo $form->listBox($model, $langAttribute, ContentList::getContentLang(), array(                                   
            'multiple'=>'multiple',
            'style'=>'width: 100%; height:170px',
            'class'=>'listbox',
            'id'=>'content_lang_box'
        )); ?>
        <?php echo $form->error($model, $langAttribute); ?>
    </div>
    <div class="span4">
        <?php echo $form->labelEx($model, $contentTypeAttribute); ?>
        <?php echo $form->listBox($model, $contentTypeAttribute, ContentList::getContentType(), array( 
            'multiple'=>'multiple',
            'style'=>'width: 100%; height:170px',
            'class'=>'listbox',
            'id'=>'content_type_box'
        )); ?>
        <script>
            $(document).ready(function () {
                $('#content_type_box, #content_lang_box').change(changeTerms);

                var count=0;
                function changeTerms(){
                    <?php echo CHtml::ajax(array(                            
                        'url'=>array('dynamicTerms', 'site' => isset($_GET['site']) ? $_GET['site'] : ''),                             
                        'data'=>array('q'=>'js:$(\'#content_type_box\').val()', 
                                      $langAttribute=>'js:$(\'#content_lang_box\').val()', 
                                      'YII_CSRF_TOKEN'=>Yii::app()->getRequest()->getCsrfToken()
                                        ),
                        'type'=>'post',
                        'dataType'=>'html',
                        'success'=>"function(data) {
                            count++;
                            $('#term_id').empty();
                            $('#term_id').append(data);
                            console.log(count);
                            if (count==1) {
                                terms='".json_encode($model->terms)."';
                                if (terms!='null') {
                                console.log(typeof terms);
                                $('#term_id').val($.parseJSON(terms)); }
                            }
                        } ",
                        )); 
                    ?> 
                };
            });
        </script>
        <?php echo $form->error($model, $contentTypeAttribute); ?>
    </div>
    <div class="span4">
        <?php echo $form->labelEx($model, $termsAttribute); ?>
        <?php echo $form->listBox($model, $termsAttribute, ContentList::getTerms(), array(
			'id'=>'term_id',
			'multiple'=>'multiple',
			'style'=>'width:100%; height:170px',
			'class'=>'listbox'
        )); ?>
        <?php echo $form->error($model, $termsAttribute); ?>
    </div>
</div>