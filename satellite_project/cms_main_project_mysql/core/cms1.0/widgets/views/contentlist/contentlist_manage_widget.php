<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'contentlist-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'content_list_id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->content_list_id',
        ),
        array(
            'name' => 'name',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => isset($url) && $url ? 'CHtml::link($data->name, array("' . $url . '&model=' . $model_name . '&action=view&id=".$data->content_list_id))' : 'CHtml::link($data->name,array("' . app()->controller->id . '/view","id"=>$data->content_list_id))',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => isset($url) && $url ? 'Yii::app()->createUrl("' . $url . '&model=' . $model_name . '&action=update&id=".$data->content_list_id)' : 'Yii::app()->createUrl("' . app()->controller->id . '/update", array("id"=>$data->content_list_id))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => _t('Delete'),
                    'imageUrl' => false,
                    'url' => isset($url) && $url ? 'Yii::app()->createUrl("' . $url . '&model=' . $model_name . '&action=delete&id=".$data->content_list_id)' : 'Yii::app()->createUrl("' . app()->controller->id . '/delete", array("id"=>$data->content_list_id))',
                ),
            ),
        ),
    ),
));
