<div class="form">
<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'language-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype'=>'multipart/form-data')
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<?php for($i=1; $i<=11; $i++):?>
		<?php $this->render('cms.widgets.views.filtersoptions._filter_form', array('model' => $model, 'form' => $form, 'filter' => 'filter'.$i));?>
	<?php endfor;?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn')); ?>
	</div>

	<script type="text/javascript">
	function init_del_btn(){
		$('.del-button').click(function(e){
			e.preventDefault();
			$($(this).attr('href')).remove();
			return false;
		});
	}

	$(document).ready(function(){
		$('.select_type').change(function(){
			$('#'+$(this).attr('id')+'_drop_down_options, #'+$(this).attr('id')+'_taxonomy_id_options').hide();
			console.log($(this).val());
			switch($(this).val()){
				case '1':
				case '2':{
					$('#'+$(this).attr('id')+'_drop_down_options').show();
					break;
				}
				case '3':
				case '4':{
					$('#'+$(this).attr('id')+'_taxonomy_id_options').show();
					break;
				}
				default:{
					$('#'+$(this).attr('id')+'_drop_down_options, #'+$(this).attr('id')+'_taxonomy_id_options').hide();
					break;
				}
			}
		}).change();

		$(".add-button").click(function(e){
			e.preventDefault();
			var filter = $(this).data('value'),
				tr_cnt = $('#'+filter+'_table tr').length;

			$('#'+filter+'_table').append(
				$(
					'<tr id="'+filter+'_tr_'+tr_cnt+'">'+
						'<td><input type="text" name="FiltersOptions[options][select_options]['+filter+']['+tr_cnt+']" id="FiltersOptions_options_select_options_'+filter+'_'+tr_cnt+'" /></td>'+
						'<td><a href="#'+filter+'_tr_'+tr_cnt+'" class="btn del-button btn-warning"><?php echo _t('Delete');?></a></td>'+
					'</tr>'
				)
			);

			init_del_btn();
		});

		init_del_btn();
	});
	</script>
<?php $this->endWidget(); ?>

</div><!-- form -->
