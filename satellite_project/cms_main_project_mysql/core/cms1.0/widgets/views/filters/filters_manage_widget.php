<?php
$site = Yii::app()->controller->site;
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'filters-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
        ),
        array(
            'name' => 'name',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => 'CHtml::link($data->title,array("' . Yii::app()->controller->id . '/view","id"=>$data->id, "site" => "' . $site . '"))',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/update", array("id"=>$data->id, "site" => "' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => _t('Delete'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/delete", array("id"=>$data->id, "site" => "' . $site . '"))',
                ),
            ),
        ),
    ),
));
