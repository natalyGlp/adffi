<?php
/* @var $form CActiveForm */
/* @var $model Taxonomy */
?>
<div class="form">
<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'taxonomy-form',
        'enableAjaxValidation' => true,
    ));
?>

<?php echo $form->errorSummary($model); ?>
<div id="language-zone">
    <?php if($model->isNewRecord) : ?>
        <?php if(count($versions) > 0): ?>
            <div class="row">
                <strong style='color:#DD4B39'><?= _t("Translated Version of:"); ?></strong><br />
                <?php foreach($versions as $version) :?>
                    <br />
                    <b>-<?= $version; ?></b>
                <?php endforeach; ?>
                <br />
            </div>
        <?php endif; ?>
        <?php if(Language::model()->isMultilang): ?>
            <?php  ?>
            <div class="row">
                <?= $form->labelEx($model,'lang'); ?>
                <?= $form->dropDownList(
                    $model,
                    'lang',
                    Language::items($lang_exclude),
                        array('options' =>
                            array(array_search(Yii::app()->language, Language::items($lang_exclude,false)) => array('selected' => true))
                        )
                    );
                ?>
                <?= $form->error($model,'lang'); ?>
                <div class="clear"></div>
            </div>
        <?php else: ?>
            <?php echo $form->hiddenField($model,'lang',array('value'=>Language::mainLanguage())); ?>
        <?php endif; ?>
    <?php endif; ?>
</div>

<?php if($model->isNewRecord && $type=='') : ?>
    <div class="row">
        <?= $form->labelEx($model,'type'); ?>
        <?= $form->dropDownList($model,'type', GxcHelpers::getAvailableContentType(true)); ?>
        <?= $form->error($model,'type'); ?>
    </div>
<?php else: ?>
    <?= $form->hiddenField($model,'type',array('value'=>$model->type)); ?>
<?php endif; ?>

<div class="row">
    <?= $form->labelEx($model,'name'); ?>
    <?= $form->textField($model,'name'); ?>
    <?= $form->error($model,'name'); ?>
</div>
<div class="row">
    <?= $form->labelEx($model,'description'); ?>
    <?= $form->textField($model,'description'); ?>
    <?= $form->error($model,'description'); ?>
</div>

<?php if (!$model->isNewRecord) : ?>
    <div class="row">
        <?php
        $this->widget('cms.widgets.TreeFormWidget', array(
            'title'=>_t('Terms'),
            'form_create_url'=>$this->form_create_term_url,
            'form_update_url'=>$this->form_update_term_url,
            'form_change_order_url'=>$this->form_change_order_term_url,
            'form_delete_url'=>$this->form_delete_term_url,
            'list_items'=>isset($list_items) ? $list_items : array()
            )
        );
        ?>
    </div>
<?php endif; ?>

<div class="row buttons">
    <?= CHtml::submitButton(_t('Save'),array('class'=>'btn bebutton')); ?>
</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
