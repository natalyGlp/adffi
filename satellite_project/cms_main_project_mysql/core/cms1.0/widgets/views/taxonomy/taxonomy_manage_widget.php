<?php
$site = Yii::app()->controller->site;
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'taxonomy-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'taxonomy_id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->taxonomy_id',
        ),
        array(
            'name' => 'name',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => 'CHtml::link($data->name,array("update","id"=>$data->taxonomy_id, "site"=>"' . $site . '"))',
        ),
        array(
            'name' => 'description',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => '$data->description',
        ),
        array(
            'name' => 'type',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft gridmaxwidth',
            ),
            'value' => 'Object::convertObjectType($data->type)',
        ),
        array(
            'name' => 'lang',
            'type' => 'raw',
            'value' => 'Language::convertLanguage($data->lang)',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{translate}',
            'visible' => Language::model()->isMultilang,
            'buttons' => array(
                'translate' => array(
                    'label' => _t('Translate'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->controller->createUrl("create", array("guid"=>$data->guid,"type"=>$data->type, "site"=>"' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->controller->createUrl("update", array("id"=>$data->taxonomy_id, "site"=>"' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => _t('Delete'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->controller->createUrl("delete", array("id"=>$data->taxonomy_id, "site"=>"' . $site . '"))',
                ),
            ),
        ),
    ),
));
