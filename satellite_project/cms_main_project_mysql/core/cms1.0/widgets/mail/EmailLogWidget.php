<?php

/**
 * This is the Widget for watching emails on backend
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.mail
 */
class EmailLogWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $dataProvider = new CActiveDataProvider('EmailLog');

        $this->render('cms.widgets.views.email.log_index', array(
            'model' => $dataProvider->model,
            'dataProvider' => $dataProvider,
        ));
    }
}
