<?php

/**
 * This is the Widget for Updating a Term
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package  cmswidgets.object
 *
 */
class MailTemplateUpdateWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $mail_id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
        $model = GxcHelpers::loadDetailModel('MailTemplate', $mail_id);

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'mailtemplate-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['MailTemplate'])) {
            $model->attributes = $_POST['MailTemplate'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', _t('Update Mail Template Successfully!'));
            }
        }
        $this->render('cms.widgets.views.mailtemplate.mailtemplate_form_widget', array(
            'model' => $model,
        ));
    }
}
