<?php

/**
 * This is the Widget for Render Blocks of a Region
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets
 */
class BlockRenderWidget extends CWidget
{
    public $visible = true;
    public $region = '0';
    public $cacheOutput = false;

    // DEPRECATED
    public $layout_asset = null;

    protected $_page;

    public function run()
    {
        if(!$this->visible) {
            return;
        }

        if($this->cacheOutput) {
            $this->renderCachedContent();
        } else {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $blocks = $this->page->blocks;
        if(isset($blocks[$this->region])) {
            $blocksInRegion = $blocks[$this->region];

            foreach ($blocksInRegion as $block) {
                $this->renderBlock($block['class'], $block['params']);
            }
        }
    }

    protected function renderCachedContent()
    {
        if($this->beginCache('BlockRenderWidget::region->'.$this->region, array('duration' => 15 * 60))) {
            $this->renderContent();
            $this->endCache();
        }
    }

    /**
     * Рендерит виджет блока
     * @param  string $className алиас класса виджета
     * @param  StdClass $block аттрибуты модели блока
     */
    public function renderBlock($className, $params) {
        $params = $this->cleanWidgetParams($className, $params);
        $this->widget($className, $params);
    }

    /**
     * Удаляет из массива параметров элементы, для которых
     * нету соответствующих свойств в классе
     *
     * @param  string $className  класс, для которого фильтруются свойства
     * @param  array  $params параметры
     * @return array  $params
     */
    public function cleanWidgetParams($className, $params)
    {
        $className = Yii::import($className,true);
        foreach ($params as $key => $value) {
            if(!property_exists($className, $key)) {
                unset($params[$key]);
            }
        }

        return $params;
    }

    /**
     * DEPRECATED: удалить метод в будующем
     * @param BlockWidget $blockWidget
     * @return string возвращает алиас файла с вьюхой блока
     */
    public static function setRenderOutput($blockWidget)
    {
        return 'output';
    }

    public function getPage()
    {
        return !isset($this->_page) ? Yii::app()->controller->page : $this->_page;
    }

    /**
     * @param Page $page модель текущей страницы
     */
    public function setPage($page) {
        $this->_page = $page;
    }
}
