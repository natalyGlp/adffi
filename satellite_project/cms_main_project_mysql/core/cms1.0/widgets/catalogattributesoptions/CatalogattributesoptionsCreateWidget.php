<?php

/**
 * This is the Widget for Creating Model Information.
 *
 * @author Igogo <igogo@glp-centre.com>
 * @version 1.0
 * @package cms.widgets
 *
 */
class CatalogattributesoptionsCreateWidget extends CWidget
{
    public $visible = true;
    public $model_name = 'CatalogAttributesOptions';

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model_name = $this->model_name;
        if ($model_name != '') {
            $model = new $model_name;
            $model->options = array();
            if (isset($_POST['ajax']) && $_POST['ajax'] === strtolower($model_name) . 'create-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST[$model_name])) {
                $model->setAttributes($_POST[$model_name], false);

                if ($model->save()) {
                    $model->unsetAttributes();
                    Yii::app()->user->setFlash('success', _t('Create Successfully!'));
                    Yii::app()->controller->refresh();
                }
            }

            $this->render('cms.widgets.views.catalogattributesoptions.catalogattributesoptions_form_widget', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(404, _t('The requested page does not exist.'));
        }
    }
}
