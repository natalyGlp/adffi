<?php

class GMapsLatLngSelector extends CInputWidget
{
    public $addressSourceSelector;

    public function run()
    {
        list($name, $id) = $this->resolveNameID();
        $value = $this->hasModel() ? $this->model->{$this->attribute} : $this->value;

        $canvasId = $id . '-mapCanvas';

        echo CHtml::hiddenField($name, $value, array(
            'id' => $id,
            ));
        ?>
<div class="alert alert-info">Для выбора локации, <b>кликните</b> на нужный маркер, так, что бы он стал зеленым</div>
<div id="<?php echo $canvasId ?>" style="border:#6e6e6e 1px solid; margin: 18px 0; height:450px;width:100%;"></div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false&language=ru"></script>
<script type="text/javascript">
    (function() {
        var geocoder,
            map,
            markers = [];

        var $shopLocation;
        var $addressSource;

        var pinColor = '00A103',
            pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
            new google.maps.Size(21, 34),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 34)),
            defaultPin = false;


        $(function() {
            initialize();

            $shopLocation = $('#<?php echo $id; ?>');
            $addressSource = $('<?php echo $this->addressSourceSelector; ?>');

            $shopLocation.change(function() {
                var curSelectedMarker = $(this).val();
                eachMarker(function() {
                    var position = this.getPosition();
                    var isSelected = latLngToString(position) == curSelectedMarker;
                    this.setIcon(isSelected ? pinImage : defaultPin);
                });
            });

            $addressSource
                .change(function() {
                    var address = $(this).val();
                    if(address != '') {
                        getGeo(address);
                    }
                }).change()
                .keypress(function(event) {
                    if(event.which == 13) {
                        event.preventDefault();
                        $(this).change();
                    }
                })
                ;
        });

        function getGeo(address) {
            geocoder.geocode( { 'address': address}, function(results, status) {
                removeMarkers();

                if (status == google.maps.GeocoderStatus.OK) {
                    $.each(results, function() {
                        pushMarker(this);
                    });

                    fitBoundsToMarkers();

                    // выделяем маркер с выбраным магазином
                    $shopLocation.change();
                }
            });
        }

        function pushMarker(loactionData) {
            var marker = createMarker(loactionData.geometry.location);

            if(!defaultPin) {
                // init default pin
                defaultPin = marker.getIcon();
            }

            setMarkerEvents(marker);

            markers.push(marker);
        }

        function createMarker(position) {
            return new google.maps.Marker({
                map: map,
                draggable: true,
                position: position,
                animation: google.maps.Animation.DROP,
            });
        }

        function removeMarkers() {
            eachMarker(function() {
                this.setMap(null);
            });

            markers = [];
        }

        function eachMarker(callback) {
            $.each(markers, callback);
        }

        function fitBoundsToMarkers() {
            var bounds = new google.maps.LatLngBounds();
            eachMarker(function() {
                bounds.extend(this.getPosition());
            });

            map.fitBounds(bounds);

            var maxZoomLevel = 18;

            if(map.getZoom() > maxZoomLevel) {
                zoomTo(maxZoomLevel);
            }
        }

        function zoomTo(zoom) {
            var center = map.getCenter();
            map.setZoom(zoom);
            map.panTo(center);
        }

        function setMarkerEvents(marker) {
            google.maps.event.addListener(marker, 'click', updateShopLocation);
            google.maps.event.addListener(marker, 'dragend', updateShopLocation);
        }

        function updateShopLocation() {
            var position = this.getPosition();
            $shopLocation.val(latLngToString(position)).change();
        }

        function latLngToString(latLng)
        {
            return latLng.lat() + ',' + latLng.lng();
        }

        function initialize() {
            geocoder = new google.maps.Geocoder();
            var mapOptions = {
                center: new google.maps.LatLng(50.450100, 30.523400),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(
                document.getElementById("<?php echo $canvasId ?>"),
                mapOptions
            );

            setTimeout(function(){google.maps.event.trigger(map, 'resize')}, 2000);
        }
    })();
</script>
        <?php
    }
}