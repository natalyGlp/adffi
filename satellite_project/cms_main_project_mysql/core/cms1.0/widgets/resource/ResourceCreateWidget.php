<?php

/**
 * This is the Widget for create new Resource.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.widgets.resource
 */
// TODO: методы Storage классов в идеале не должны получать модель формы. Свести кол-во передаваемых параметров к 1-2
class ResourceCreateWidget extends CWidget
{
    public $visible = true;

    protected $_resourceOptions;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model = new ResourceUploadForm();
        $width = (isset($_GET['w']) && $_GET['w'] != 'disabled') ? trim($_GET['w']) : '';
        $height = (isset($_GET['h']) && $_GET['h'] != 'disabled') ? trim($_GET['h']) : '';
        $model->width = $width;
        $model->height = $height;

        $this->fetchResourceOptions();

        $model->where = $this->_resourceOptions['storageId'];

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'resource-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['ResourceUploadForm'])) {
            $model->attributes = $_POST['ResourceUploadForm'];

            $this->processUploads($model);

            if ($this->isMultiUpload && !$model->hasErrors()) {
                Yii::app()->end();
            }
        }

        $this->render('cms.widgets.views.resource.resource_form_widget', array(
            'model' => $model,
            'multiple' => $this->isMultiUpload,
            'is_new' => true,
            'types_array' => ConstantDefine::fileTypes(),
            'uploadRestrictions' => $this->getUploadRestrictions(),
        ));
    }

    protected function fetchResourceOptions()
    {
        $this->_resourceOptions = array(
            'type' => '',
            'name' => '',
            'maxSize' => ConstantDefine::UPLOAD_MAX_SIZE,
            'minSize' => ConstantDefine::UPLOAD_MIN_SIZE,
            'width' => null,
            'height' => null,
            'max' => 1,
            'allowedExtensions' => array(),
            'storageId' => 'local',
        );

        $contentType = isset($_GET['content_type']) ? trim($_GET['content_type']) : null;
        $resourceType = isset($_GET['type']) ? trim($_GET['type']) : null;

        $objectClassName = Object::importByType($contentType, null);
        if (!$objectClassName) {
            return;
        }

        if (!$resourceType) {
            return;
        }

        $object = new $objectClassName();
        $resources = $object->Resources();
        if (isset($resources[$resourceType])) {
            $resourceConfig = $resources[$resourceType];

            $renameMap = array(
                'allow' => 'allowedExtensions',
            );
            foreach ($resourceConfig as $key => $value) {
                if (isset($renameMap[$key])) {
                    $key = $renameMap[$key];
                }
                $this->_resourceOptions[$key] = $value;
            }
        }
    }

    protected function processUploads($model)
    {
        if ($model->link != '') {
            $this->uploadFromLink($model);
        } elseif (($uploads = CUploadedFile::getInstances($model, 'upload')) && count($uploads) > 0) {
            foreach ($uploads as $upload) {
                $this->uploadFile($upload, $model);
            }
        } else {
            $model->addError('upload', 'Choose File before Upload');
        }
    }

    protected function uploadFile(CUploadedFile $upload, $model)
    {
        $model->upload = $upload;

        $upload_handle = $this->getStorageInstance();
        $resource = new Resource();
        if ($upload_handle->uploadFile($resource, $model)) {
            $this->processResourceModel($resource, $model);
        }

    }

    protected function getStorageInstance()
    {
        $storages = GxcHelpers::getStorages(true);

        $storageId = $this->_resourceOptions['storageId'];
        if (!isset($storages[$storageId])) {
            throw new CException("The storage $storageId does not exists");
        }

        return new $storages[$storageId](
            $this->_resourceOptions['maxSize'],
            $this->_resourceOptions['minSize'],
            $this->_resourceOptions['allowedExtensions']
        );
    }

    /**
     * Сейвит модель Resource и генерирует ответ в браузер
     *
     * @param Resource $resource модель ресурса
     * @param ResourceUploadForm $formModel модель формы
     */
    protected function processResourceModel(Resource $resource, ResourceUploadForm $formModel)
    {
        if ($formModel->name != '') {
            $resource->resource_name = trim($formModel->name);
        }
        $resource->resource_where = $formModel->where;
        $resource->resource_type = $formModel->type;
        $resource->resource_body = trim($formModel->body);

        if ($resource->save()) {
            if ((isset($_GET['parent_call']))) {
                $this->render('cms.widgets.views.resource.resource_upload_iframe_return', array(
                    'resource' => $resource
                ));
            } else {
                Yii::app()->user->setFlash('success', _t('New Resource was successfully created!'));

                Yii::app()->controller->redirect(array('create'), array('site' => isset($_GET['site']) ? $_GET['site'] : ''));
            }
        }

        if (!$this->isMultiUpload) {
            Yii::app()->end();
        }
    }

    protected function uploadFromLink($model)
    {
        $resource = new Resource();

        $ext = $this->getUrlFileExtension($model->link);

        if ($model->where == 'external') {
            // сохраняем прямую ссылку на ресурс на внешнем источнике
            $resource->resource_path = trim($model->link);
        } else {
            if ($model->where == 'local' && $model->type == 'image') {
                if (!$this->isImageExtension($ext)) {
                    $model->addError('link', _t('Not valid Image'));
                    return false;
                }
            }

            if (!$this->uploadRemoteFile($resource, $model, $ext)) {
                return false;
            }
        }

        $this->processResourceModel($resource, $model);
    }

    protected function getUrlFileExtension($url)
    {
        $fileMimeType = $this->getUrlMimeType($url);

        $extensions = array(
            'image/jpeg' => 'jpg',
            'image/gif' => 'gif',
            'image/jpeg' => 'jpg',
            'image/png' => 'png',
            'image/x-png' => 'png',
            'text/html' => 'html',
        );

        if (isset($extensions[$fileMimeType])) {
            $ext = $extensions[$fileMimeType];
        } else {
            $ext = pathinfo($url, PATHINFO_EXTENSION);
        }

        return !empty($ext) ? $ext : null;
    }

    /**
     * Пытается определить mime тип контента по $url
     * @param  string $url ссылка на контент, чей миме тип нужно определить
     * @return string      mime тип
     */
    protected function getUrlMimeType($url)
    {
        $oldSafeMode = ini_get('safe_mode');
        ini_set('safe_mode', 0);
        $fileMimeType = false;
        $file_headers = @get_headers($url, 1);
        if ($file_headers && strpos($file_headers[0], '404') === false) {
            $fileMimeType = is_array($file_headers['Content-Type']) ? $file_headers['Content-Type'][0] : $file_headers['Content-Type'];
            preg_match('/^\w+\/\w+/', $fileMimeType, $fileMimeType);
            $fileMimeType = $fileMimeType[0];
        } else {
            // Попытка №2
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, ini_get('safe_mode') == 0);
            curl_setopt($curl, CURLOPT_HEADER, 1);
            curl_setopt($curl, CURLOPT_NOBODY, 1);
            curl_exec($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($statusCode >= 200 && $statusCode <= 300) {
                $fileMimeType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
            }
        }
        ini_set('safe_mode', $oldSafeMode);

        return $fileMimeType;
    }

    protected function isImageExtension($ext)
    {
        $fileTypes = ConstantDefine::fileTypes();

        return in_array(strtolower($ext), $fileTypes['image']);
    }

    protected function uploadRemoteFile(&$resource, $model, $ext)
    {
        if (!$this->remoteFileExists($model->link)) {
            $model->addError('link', _t('Remote file does not exists'));
            return false;
        }

        $upload_handle = $this->getStorageInstance();
        if ($upload_handle->uploadRemoteFile($model->link, $resource, $model, $ext)) {
            return true;
        }

        return false;
    }

    protected function remoteFileExists($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0');
        $result = curl_exec($curl);

        $ret = false;

        if ($result !== false) {
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($statusCode == 200) {
                $ret = true;
            }
        }

        curl_close($curl);

        return $ret;
    }

    protected function getUploadRestrictions()
    {
        $phpPostMaxSize = ini_get('post_max_size');
        $phpUploadMaxSize = ini_get('upload_max_filesize');
        $phpMaxUploadsNumber = ini_get('max_file_uploads');

        return array(
            'maxFileSize' => min($phpPostMaxSize, $phpUploadMaxSize, $this->_resourceOptions['maxSize']),
            'maxFilesSize' => $phpPostMaxSize, //for multiple upload
            'maxUploadsNumber' => $phpMaxUploadsNumber,
        );
    }

    protected function getIsMultiUpload()
    {
        return isset($_GET['multiple']) && $_GET['multiple'] == 'true';
    }
}
