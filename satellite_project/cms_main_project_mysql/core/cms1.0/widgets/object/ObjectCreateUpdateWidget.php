<?php
/**
 * TODO: нужно отказаться от вложенных масивов термов array('id' => 'taxonomyId', 'terms' => array())
 *       так как нам вообще не нужно знать об id таксономии. Нам нужны только термы
 *       единственное, что нужно не забыть, что название таксономии выводится на фронтенде
 *       и что там скрипты тоже завязаны на таксономии, но это не тяжело обойти
 * TODO: обработку ресурсов и массивы POST тоже можно упростить
 * TODO: повыносить функции для обработки связей в отдельные файлы
 */

class ObjectCreateUpdateWidget extends CWidget
{
    protected $model;
    private $_availableTerms;
    private $_selectedTerms;

    public function run()
    {
        if (!Object::importByType($this->getObjectType(), false)) {
            $this->askForObjectType();
            return;
        }

        $this->setupModel();

        // здесь мы привязываемся только к типу из GET,
        // так как он соответствует ключам в POST массиве
        $POSTClassName = CHtml::modelName(Object::importByType($_GET['type']));
        if (isset($_POST[$POSTClassName])) {
            $this->model->attributes = $_POST[$POSTClassName];

            $this->setObjectDates();

            $this->model->total_number_resource = $this->getResourcesCount();

            if ($this->model->save()) {
                // TODO: Было бы круто перевести таблицы на InnoDb и использовать транзакцию
                $this->saveResources();
                $this->saveTerms();
                $this->saveDynamicLists();

                $this->redirectAfterSave();
            }
        }

        $this->renderForm();
    }

    protected function getObjectType()
    {
        $type = null;
        if (isset($_POST['type'])) {
            $type = strtolower(trim($_POST['type']));
        } elseif (isset($_GET['type'])) {
            $type = strtolower(trim($_GET['type']));
        }

        return $type;
    }

    protected function askForObjectType()
    {
        $types = GxcHelpers::getAvailableContentType();
        $typesMenu = array();
        foreach ($types as $type) {
            $path = Yii::getPathOfAlias(GxcHelpers::getTruePath('content_type.' . $type['id'] . '.assets'));
            if (isset($type['icon']) && is_file($path . DIRECTORY_SEPARATOR . $type['icon'])) {
                $iconAsset = Yii::app()->assetManager->publish($path);
                $iconSrc = $iconAsset . '/' . $type['icon'];
            } else {
                $iconSrc = GxcHelpers::assetUrl('img/gCons/edit.png', 'gebo');
            }

            $typesMenu[] = array(
                'label' => $type['name'],
                'url' => Yii::app()->createUrl('beobject/create', array(
                    'type' => $type['id'],
                    'site' => Yii::app()->controller->site,
                )),
                'iconSrc' => $iconSrc,
            );
        }
        $this->render('cms.widgets.views.object.object_start_widget', array(
            'types' => $typesMenu,
        ));
    }

    protected function setupModel()
    {
        $objectClass = Object::importByType($this->getObjectType(), false);

        $obejctId = isset($_GET['id']) ? $_GET['id'] : false;
        if ($obejctId) {
            $this->model = GxcHelpers::loadDetailModel($objectClass, $obejctId);
        } else {
            $this->model = new $objectClass();
            if (isset($_GET['guid'])) {
                $this->model->guid = $_GET['guid'];
            }

            $this->model->lang = isset($_GET['langid']) ? $_GET['langid'] : Language::mainLanguage();
        }
    }

    protected function getResourcesCount()
    {
        if (!isset($_POST['resource'])) {
            return 0;
        }

        return array_reduce($_POST['resource'], function ($acc, $resourcesByType) {
            return $acc + count($resourcesByType['resid']);
        }, 0);
    }

    protected function setObjectDates()
    {
        if ($this->model->object_date == '') {
            $this->model->object_date = date('d.m.Y H:i:s');
        }
        //Convert the date time publish to timestamp
        $this->model->object_date = strtotime($this->model->object_date);
        $this->model->object_date_gmt = local_to_gmt($this->model->object_date);
    }

    protected function saveResources()
    {
        ObjectResource::model()->deleteAll('object_id = :id', array(
            ':id' => $this->model->object_id,
        ));

        foreach ($this->model->Resources() as $resourceConfig) {
            $resourceType = $resourceConfig['type'];
            $resourcesList = GxcHelpers::getArrayResourceObjectBinding($resourceType);
            foreach ($resourcesList as $oid => $resource) {
                $objectResource = new ObjectResource();
                $objectResource->attributes = array(
                    'resource_id' => $resource['resid'],
                    'object_id' => $this->model->object_id,
                    'description' => '',
                    'type' => $resourceType,
                    'resource_order' => $oid + 1,
                );
                $objectResource->save();
            }
        }
    }

    protected function saveTerms()
    {
        $this->updateSelectedTermsFromPost();

        $selectedTerms = $this->getSelectedTerms();

        ObjectTerm::model()->deleteAll('object_id = :id', array(
            ':id' => $this->model->object_id,
        ));

        foreach ($selectedTerms as $taxonomy) {
            foreach ($taxonomy['terms'] as $term) {
                $objectTerm = new ObjectTerm();
                $objectTerm->object_id = $this->model->object_id;
                $objectTerm->term_id = $term['id'];
                $objectTerm->save();
            }
        }
    }

    protected function updateSelectedTermsFromPost()
    {
        $terms = $this->getAvailableTerms();

        // Get the Terms that the User Choose
        $post_terms = isset($_POST['terms']) ? $_POST['terms'] : array();
        $selectedTerms = array();

        foreach ($post_terms as $t) {
            $t = explode('_', $t);
            $termId = $t[0];
            $taxonomyId = $t[1];
            $taxonomy = $terms[$taxonomyId];
            if (!isset($selectedTerms[$taxonomyId])) {
                $selectedTerms[$taxonomyId] = $taxonomy;
                $selectedTerms[$taxonomyId]['terms'] = array();
            }

            $selectedTerms[$taxonomyId]['terms']['item_' . $termId] = $taxonomy['terms']['item_' . $termId];
        }

        // After having the selected Terms, we need to make sure  all parents
        // of the selected Terms must be added also
        foreach ($selectedTerms as $taxonomyId => $taxonomy) {
            $parentTerms = array();

            foreach ($taxonomy['terms'] as $currentTerm) {
                $parentId = $currentTerm['parent'];
                if ($parentId > 0) {
                    $selectedTerms[$taxonomyId]['terms']['item_' . $parentId] = $terms[$taxonomyId]['terms']['item_' . $parentId];
                }
            }
        }

        $this->_selectedTerms = $selectedTerms;
    }

    /**
     * @return array всех таксономий прикрепленных к текущему контент типу
     */
    protected function getAvailableTerms()
    {
        if (!isset($this->_availableTerms)) {
            $taxonomies = Taxonomy::model()->findAllByAttributes(array(
                'type' => array(
                    $this->model->object_type,
                    'NESPI_RSS', // таксономия RSS доступна глобально для всех контент типов
                ),
            ));

            $terms = array();
            foreach ($taxonomies as $taxonomy) {
                $currentTaxonomy = array(
                    'id' => $taxonomy->taxonomy_id,
                    'lang' => $taxonomy->lang,
                    'name' => $taxonomy->name,
                    'terms' => array(),
                );

                //Look for the Term Items belong to this Taxonomy
                $taxonomyTerms = Term::model()->findAll(array(
                    'condition' => 'taxonomy_id=:id',
                    'order' => 't.parent ASC, t.sort_order ASC',
                    'params' => array(':id' => $taxonomy->taxonomy_id),
                ));

                foreach ($taxonomyTerms as $term) {
                    $currentTaxonomy['terms']['item_' . $term->term_id] = array(
                        'id' => $term->term_id,
                        'name' => CHtml::encode($term->name),
                        'parent' => $term->parent,
                    );
                }
                $terms[$taxonomy->taxonomy_id] = $currentTaxonomy;
            }

            $this->_availableTerms = $terms;
        }

        return $this->_availableTerms;
    }

    protected function getSelectedTerms()
    {
        if ($this->model->isNewRecord) {
            return array();
        }

        if (!isset($this->_selectedTerms)) {
            $objectTerms = ObjectTerm::model()
                ->with('term', 'term.taxonomy')
                ->findAllByAttributes(array(
                    'object_id' => $this->model->object_id,
                ));

            $selectedTerms = array();
            foreach ($objectTerms as $model) {
                $taxonomy = $model->term->taxonomy;
                $term = $model->term;

                if (!isset($selectedTerms[$taxonomy->taxonomy_id])) {
                    $selectedTerms[$taxonomy->taxonomy_id] = array(
                        'id' => $taxonomy->taxonomy_id,
                        'lang' => $taxonomy->lang,
                        'name' => $taxonomy->name,
                        'terms' => array(),
                    );
                }

                $selectedTerms[$taxonomy->taxonomy_id]['terms']['item_' . $term->term_id] = array(
                    'id' => $term->term_id,
                    'name' => CHtml::encode($term->name),
                    'parent' => $term->parent,
                );
            }

            $this->_selectedTerms = $selectedTerms;
        }

        return $this->_selectedTerms;
    }

    /**
     * проверяем наличие динамических контент листов
     * @param $model
     */
    protected function saveDynamicLists()
    {
        if (isset($_POST['DynamicLists'])) {
            foreach ($_POST['DynamicLists'] as $pk => $manualList) {
                $contentList = ContentList::model()->findByPk($pk);
                $itemsToDelete = array();
                if (isset($manualList['delete'])) {
                    // сохраним списки помеченные на удаление
                    $itemsToDelete = $manualList['delete'];

                    if (count($itemsToDelete)) {
                        foreach ($itemsToDelete as $value) {
                            if (($key = array_search($value, $contentList->manual_list)) !== false) {
                                unset($contentList->manual_list[$key]);
                            }
                        }

                        $contentList->manual_list = array_values($contentList->manual_list);
                    }
                    unset($manualList['delete']);
                }

                if (count($manualList) > 0) {
                    $contentList->manual_list = array_slice($contentList->manual_list, 0, $contentList->number - 1);
                }

                // если создается новый обьект, нужно дополнительно проверить,
                // не решил ли пользователь сразу поставить его в контент лист
                // у нового обьекта будет 'null' вместо value
                if (($key = array_search('null', $manualList)) !== false) {
                    $manualList[$key] = $this->model->primaryKey;
                }

                if (in_array($this->model->primaryKey, $manualList) && !in_array($model->primaryKey, $contentList->manual_list)) {

                    $curObjKey = array_search($this->model->primaryKey, $manualList);
                    // вставляем текущий обьект на его место в массиве
                    array_splice($contentList->manual_list, $curObjKey, 0, $this->model->primaryKey);
                }

                // сортируем списки основываясь на порядок из $manualList
                foreach ($manualList as $key => $value) {
                    if (($oldKey = array_search($value, $contentList->manual_list)) !== false) {
                        unset($contentList->manual_list[$oldKey]);
                        array_splice($contentList->manual_list, $key, 0, $value);
                    }
                }

                $contentList->manual_list = array_values($contentList->manual_list);
                $contentList->save();
            }
        }
    }

    protected function redirectAfterSave()
    {
        $url = Yii::app()->request->url;
        if (isset($_GET['type'])) {
            $url = str_replace('type=' . $_GET['type'], 'type=' . $this->model->object_type, $url);
        }

        if (isset($_GET['guid']) && isset($_GET['langid'])) {
            // при создании перевода обьекта редиректим на редактирование
            // что бы юзер при желании мог переключиться на создание другого языка
            $url = str_replace('create', 'update', $url);
            $url .= '&id=' . $this->model->primaryKey;
        }
        if (isset($_GET['guid'])) {
            $url = str_replace('guid=' . $_GET['guid'], '', $url);
        }
        if (isset($_GET['langid'])) {
            $url = str_replace('langid=' . $_GET['langid'], '', $url);
        }

        $url = preg_replace('/(&|\?)&*/', '$1', $url);

        Yii::app()->user->setFlash('success', _t(ucfirst(Yii::app()->controller->action->id) . ' new Content Successfully!'));
        $this->controller->redirect($url);
    }

    protected function renderForm()
    {
        if (!empty($this->model->object_date)) {
            $this->model->object_date = date('d.m.Y H:i:s', $this->model->object_date);
        } else {
            $this->model->object_date = '';
        }

        $this->render($this->getFormTemplate(), array(
            'model' => $this->model,
            'type' => $this->model->object_type,
            'content_status' => $this->model->getObjectStatus(),
            'versions' => $this->getTranslations(),
            'terms' => $this->getAvailableTerms(),
            'selected_terms' => $this->getSelectedTerms(),
            'content_resources' => $this->model->Resources(),
            'lang_exclude' => array(), // DEPRECATED
            'lang' => '', // DEPRECATED
        ));
    }

    /**
     * If the guid is not empty, it means we are creating a translated version of a content
     * We will exclude the translated language and include the name of the translated content to $versions
     * @return array
     */
    protected function getTranslations()
    {
        // List of translated versions
        if (empty($this->model->guid)) {
            return array();
        }

        $versions = array();
        $languages = Language::model()->findAll('lang_active=1');
        foreach ($languages as $key => $lang) {
            $versions[$key]['name'] = $lang->lang_desc;
            $versions[$key]['lang'] = $lang->lang_id;
            $objectTranslation = Object::model()->findByAttributes(array(
                'guid' => $this->model->guid,
                'lang' => $lang->lang_id,
            ));
            if ($objectTranslation) {
                $versions[$key]['links']['update'] = array(
                    'update',
                    'id' => $objectTranslation->object_id,
                    'type' => $objectTranslation->object_type,
                );
            } else {
                $versions[$key]['links']['create'] = array(
                    'create',
                    'guid' => $this->model->guid,
                    'langid' => $lang->lang_id,
                    'type' => $this->model->object_type,
                );
            }
        }
        return $versions;
    }

    protected function getFormTemplate()
    {
        $render_template = GxcHelpers::getTruePath('content_type.' . strtolower($this->model->object_type) . '.object_form_widget');
        if (!file_exists(Yii::getPathOfAlias($render_template) . '.php')) {
            $render_template = 'cms.widgets.views.object.object_form_widget';
        }

        return $render_template;
    }
}
