<?php

/* @property object $_model Term model */
class TermCreateUpdateWidget extends CWidget
{
    public $visible = true;
    protected $_model;

    public function init()
    {
        $this->loadModel();
    }

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    /**
     * render content for term's widget
     * else
     * @throws CException
     */
    protected function renderContent()
    {
        $this->ajaxValidate($this->_model);

        // collect user input data
        if (isset($_POST['Term'])) {
            $this->_model->attributes = $_POST['Term'];
            if ($this->_model->save()) {
                Yii::app()->user->setFlash('success', _t('Create new Term Successfully!'));

                if (isset($_GET['id']) && !isset($_GET['embed'])) {
                    Yii::app()->controller->redirect(array(
                        'create',
                        'site' => isset($_GET['site']) ? $_GET['site'] : '',
                    ));
                }
            }
        }

        $this->render('cms.widgets.views.term.term_form_widget', array(
            'model' => $this->_model,
        ));
    }

    /**
     * Ajax validating model
     * @param $model
     */
    protected function ajaxValidate($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'term-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Loading model
     * @return Term
     * @throws CHttpException
     */
    protected function loadModel()
    {
        if (isset($_GET['id'])) {
            $term_id = (int) $_GET['id'];
            $this->_model = GxcHelpers::loadDetailModel('Term', $term_id);
        } else {
            $this->_model = new Term;
        }
    }
}
