<?php

/* @property $_model Taxonomy */
class TaxonomyCreateUpdateWidget extends CWidget
{
    public $visible = true;
    public $form_create_term_url = '';
    public $form_update_term_url = '';
    public $form_change_order_term_url = '';
    public $form_delete_term_url = '';

    protected $_model;

    public function init()
    {
        $this->loadModel();
        $this->ajaxValidation($this->_model);
    }

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $id = isset($_GET['id']) ? (int)$_GET['id'] : null;

        //The type of the content we want to create
        $type = isset($_GET['type']) ? strtolower(trim($_GET['type'])) : '';

        //If it has guid, it means this is a translated version
        $guid = isset($_GET['guid']) ? strtolower(trim($_GET['guid'])) : '';

        //Get the list of Content Type
        $types = GxcHelpers::getAvailableContentType();

        //List of language that should exclude not to translate
        $lang_exclude = array();

        //List of translated versions
        $versions = array();

        $list_items = array();

        //Look for the Term Items belong to this Taxonomy
        $list_terms = Term::model()->findAll(array(
            'select' => '*',
            'condition' => 'taxonomy_id=:id',
            'order' => 't.parent ASC, t.sort_order ASC, t.term_id ASC',
            'params' => array(
                ':id' => $id
            )
        ));

        foreach ($list_terms as $term) {

            //Add Item here to make sure Chrome not change the order of Json Object
            $list_items['item_' . $term->term_id] = array(
                'id' => $term->term_id,
                'name' => CHtml::encode($term->name) ,
                'parent' => $term->parent,
            );
        }

        if ($type != '' && !array_key_exists($type, $types)) {
            throw new CHttpException(404, _t('Page Not Found'));
        } else {

            // If the guid is not empty, it means we are creating a translated version of a content
            // We will exclude the translated language and include the name of the translated content to $versions
            if ($guid != '') {
                $taxonomy_object = Taxonomy::model()->with('language')->findAll('guid=:gid', array(
                    ':gid' => $guid
                ));
                if (count($taxonomy_object) > 0) {
                    foreach ($taxonomy_object as $obj) {
                        $lang_exclude[] = $obj->lang;
                        $versions[] = $obj->name . ' - ' . $obj->language->lang_desc;
                    }
                }
                $this->_model->guid = $guid;
            }

            /**
             * collect user input data
             */
            if (isset($_POST['Taxonomy'])) {
                $this->_model->attributes = $_POST['Taxonomy'];
                if ($this->_model->save()) {
                    if (!isset($_GET['id'])) {
                        Yii::app()->user->setFlash('success', _t('Create new Taxonomy Successfully!'));
                        Yii::app()->controller->redirect(array(
                            'create',
                            'site' => isset($_GET['site']) ? $_GET['site'] : ''
                        ));
                    } else {
                        Yii::app()->user->setFlash('success', _t('Update Taxonomy Successfully!'));
                        Yii::app()->controller->refresh();
                    }
                }
            }
            $this->render('cms.widgets.views.taxonomy.taxonomy_form_widget', array(
                'model' => $this->_model,
                'lang_exclude' => $lang_exclude,
                'versions' => $versions,
                'type' => $type,
                'list_items' => $list_items
            ));
        }
    }

    public function ajaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'taxonomy-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function loadModel()
    {
        if (isset($_GET['id'])) {
            $id = (int)$_GET['id'];
            $this->_model = GxcHelpers::loadDetailModel('Taxonomy', $id);
        } else {
            $this->_model = new Taxonomy;
        }
    }
}
