<?php
class MenuItemCreateUpdateWidget extends CWidget
{
    public $visible = true;

    protected $_model;

    public function init()
    {
        $this->loadModel();
        $this->ajaxValidation();
    }

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        if (isset($_POST['MenuItem'])) {
            $this->_model->attributes = $_POST['MenuItem'];

            if ($this->_model->save()) {

                if (isset($_GET['embed'])) {
                    Menu::clearCache();
                    Yii::app()->user->setFlash('success', _t('Update Menu Item Successfully!'));
                } else {
                    Yii::app()->user->setFlash('success', _t('Create new Item Successfully!'));
                    Yii::app()->controller->redirect(array(
                        'create',
                        'site' => isset($_GET['site']) ? $_GET['site'] : '',
                    ));
                }
            }
        }
        $this->render('cms.widgets.views.menuitem.menuitem_form_widget', array(
            'model' => $this->_model,
        ));
    }

    protected function ajaxValidation()
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'menuitem-form') {
            echo CActiveForm::validate($this->_model);
            Yii::app()->end();
        }
    }

    protected function loadModel()
    {
        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];
            $this->_model = GxcHelpers::loadDetailModel('MenuItem', $id);
        } else {
            $this->_model = new MenuItem;
        }
    }
}
