<?php
class MenuCreateUpdateWidget extends CWidget
{
    public $visible = true;
    public $form_create_term_url = '';
    public $form_update_term_url = '';
    public $form_change_order_term_url = '';
    public $form_delete_term_url = '';

    protected $_model;

    public function init()
    {
        $this->loadModel();
        $this->ajaxValidation($this->_model);
    }

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    public function renderContent()
    {
        $id = isset($_GET['id']) ? (int) $_GET['id'] : null;

        //If it has guid, it means this is a translated version
        $guid = isset($_GET['guid']) ? strtolower(trim($_GET['guid'])) : '';

        //List of language that should exclude not to translate
        $lang_exclude = array();

        //List of translated versions
        $versions = array();

        $list_items = array();

        //Look for the Term Items belong to this Taxonomy
        $list_menu_items = MenuItem::model()->findAll(array(
            'condition' => 'menu_id=:id',
            'order' => 't.parent ASC, t.sort_order ASC',
            'params' => array(
                ':id' => $id,
            ),
        ));

        if ($list_menu_items) {
            foreach ($list_menu_items as $menu_item) {
                $temp_item = array(
                    'id' => $menu_item->menu_item_id,
                    'name' => CHtml::encode($menu_item->name),
                    'parent' => $menu_item->parent,
                    'isVisible' => !!$menu_item->is_visible,
                );

                //Add Item here to make sure Chrome not change the order of Json Object
                $list_items['item_' . $menu_item->menu_item_id] = $temp_item;
            }
        }

        // If the guid is not empty, it means we are creating a translated version of a content
        // We will exclude the translated language and include the name of the translated content to $versions
        if ($guid != '') {
            $menu_object = Menu::model()->with('language')->findAll('guid=:gid', array(
                ':gid' => $guid,
            ));
            if (count($menu_object) > 0) {
                foreach ($menu_object as $obj) {
                    $lang_exclude[] = $obj->lang;
                    $versions[] = $obj->menu_name . ' - ' . $obj->language->lang_desc;
                }
            }
            $this->_model->guid = $guid;
        }

        // collect user input data
        if (isset($_POST['Menu'])) {
            $this->_model->attributes = $_POST['Menu'];
            if ($this->_model->save()) {
                if (!isset($_GET['id'])) {
                    Yii::app()->user->setFlash('success', _t('Create new Menu Successfully!'));
                    Yii::app()->controller->redirect(array(
                        'create',
                        'site' => isset($_GET['site']) ? $_GET['site'] : '',
                    ));
                } else {
                    Menu::clearCache();
                    Yii::app()->user->setFlash('success', _t('Update Menu Successfully!'));
                    Yii::app()->controller->refresh();
                }
            }
        }
        $this->render('cms.widgets.views.menu.menu_form_widget', array(
            'model' => $this->_model,
            'lang_exclude' => $lang_exclude,
            'versions' => $versions,
            'list_items' => $list_items,
        ));
    }

    protected function loadModel()
    {
        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];
            $this->_model = GxcHelpers::loadDetailModel('Menu', $id);
        } else {
            $this->_model = new Menu();
        }
    }

    protected function ajaxValidation($model)
    {
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'menu-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
