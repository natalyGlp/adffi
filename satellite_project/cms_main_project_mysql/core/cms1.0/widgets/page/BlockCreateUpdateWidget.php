<?php

/**
 * This is the Widget for Creating new Block
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package  cms.widgets.page
 */

class BlockCreateUpdateWidget extends CWidget
{
    public $visible = true;

    protected $_blockModel;
    protected $_blockWidget;

    public function init()
    {
        Yii::app()->clientScript->registerCoreScript('jquery');
    }

    public function run()
    {
        if ($this->visible) {
            if (!isset($_GET['type']) && !$this->isUpdate) {
                $this->askForBlockType();
            } else {
                $this->renderForm();
            }
        }
    }

    protected function askForBlockType()
    {
        if ($this->isIframe) {
            $this->addCancelButton();
        }

        $this->render('cms.widgets.views.block.block_start_widget', array(
            'model' => new Block(),
            'blocksList' => GxcHelpers::getAvailableBlocks(true),
        ));
    }

    protected function renderForm()
    {
        $this->instantinateModelAndWidget();
        $blockModel = $this->_blockModel;
        $blockWidget = $this->_blockWidget;

        $this->normalizePost();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'block-form') {
            echo CActiveForm::validate($this->_blockModel);
            Yii::app()->end();
        }

        if (isset($_POST['Block'])) {
            $blockModel->attributes = $_POST['Block'];
            $blockWidget->params = $_POST['Block'];

            if (!$blockWidget->validate()) {
                $blockModel->addErrors($blockWidget->errors);
            } else {
                $blockWidget->beforeBlockSave();
                $this->syncBlockParams();

                if ($blockModel->save()) {
                    $blockWidget->afterBlockSave();

                    if ($this->isIframe) {
                        $this->sendMessageToParent($blockModel);
                    } elseif ($this->isUpdate) {
                        Yii::app()->user->setFlash('success', _t('Block was successfully updated!'));
                    } else {
                        Yii::app()->user->setFlash('success', _t('Create new Block Successfully!'));

                        Yii::app()->controller->redirect(array('create'), array(
                            'type' => $blockModel->type,
                            'site' => Yii::app()->controller->site,
                        ));
                    }
                }
            }
        }

        if ($this->isIframe) {
            $this->addCancelButton();
        }

        $this->render('cms.widgets.views.block.block_form_widget', array(
            'model' => $blockModel,
            'blockWidget' => $blockWidget,
        ));
    }

    protected function instantinateModelAndWidget()
    {
        $this->_blockModel = $this->loadModel();
        $this->_blockWidget = $this->getBlockWidget($this->_blockModel);
    }

    protected function getBlockWidget(Block $model)
    {
        $block_ini = parse_ini_file(Yii::getPathOfAlias(GxcHelpers::getTruePath('front_blocks.' . $model->type . '.info', 'ini')) . '.ini');
        Yii::import(GxcHelpers::getTruePath('front_blocks.' . $model->type . '.' . $block_ini['class']));

        $blockWidget = new $block_ini['class']();
        if (!$blockWidget instanceof BlockWidget) {
            throw new CException('The widget '.$block_ini['class'].' should extend BlockWidget class');
        }

        if (!$model->isNewRecord) {
            $blockWidget->setParams(unserialize($model->params));
        }
        return $blockWidget;
    }

    protected function loadModel()
    {
        if ($this->isUpdate) {
            $model = GxcHelpers::loadDetailModel('Block', $_GET['id']);
        } else {
            $model = new Block();
            $model->type = trim($_GET['type']);
        }

        return $model;
    }

    protected function getIsUpdate()
    {
        return isset($_GET['id']);
    }

    /**
     * Заполняет модель блока параметрами виджета блока из POST
     */
    protected function syncBlockParams()
    {
        $blockParams = array();
        foreach ($this->_blockWidget->paramsInternal() as $key => $param) {
            $blockParams[$key] = $this->_blockWidget->$key;
        }

        $this->_blockModel->params = serialize($blockParams);
    }

    /**
     * Сливает POST под ключем класса текущего виджета блока с POST модели блока
     */
    protected function normalizePost()
    {
        // TODO: FIXME: с Yii 1.1.14 можно настроить конвертер имен, тогда можно будет заставить все классы виджетов конвертироваться в Block,
        // а сейчас мы просто будем использовать оба варианта названий полей
        if (isset($_POST[CHtml::modelName($this->_blockWidget)])) {
            $_POST['Block'] = CMap::mergeArray($_POST['Block'], $_POST[CHtml::modelName($this->_blockWidget)]);
        }
    }

    protected function getIsIframe()
    {
        return isset($_GET['embed']);
    }

    protected function sendMessageToParent($model)
    {
        $params = array(
            'action' => Yii::app()->controller->action->id,
            'data' => array(
                'id' => $model->block_id,
                'title' => $model->name,
                'type' => $model->type,
            ),
        );
        ?>
        <script type="text/javascript">
            window.parent.postMessage(<?= CJavaScript::encode($params) ?>, '*');
        </script>
        <?php
        Yii::app()->end();
    }

    protected function addCancelButton()
    {
        Yii::app()->clientScript->registerScript(__FILE__.'#cancleButton'.$this->id, '
            $(function() {
                var $button = $("<button>");
                $button
                    .html("'. _t('Cancel') .'")
                    .addClass("btn")
                    .click(function() {
                        window.parent.postMessage({
                            action: "cancel",
                        }, "*");

                        return false;
                    })
                    ;
                $(".buttons").append(" ").append($button);
            });
        ');
    }
}
