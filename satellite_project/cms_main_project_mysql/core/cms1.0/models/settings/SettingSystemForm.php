<?php

/**
 * This is the model class for System Settings Form
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.models.settings
 *
 */

class SettingSystemForm extends CFormModel
{
    public $sizes;

    /**
     * Declares the validation rules.
     *
     */
    public function rules()
    {
        return array(
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
        );
    }
}
