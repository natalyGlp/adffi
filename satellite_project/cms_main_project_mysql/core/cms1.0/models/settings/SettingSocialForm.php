<?php
/**
 * This is the model class for Social Settings Form
 *
 * @author Jonny <voinovev@studiobanzai.com>
 * @version 1.0
 * @package cms.models.settings
 *
 */
class SettingSocialForm extends CFormModel
{
    public $facebook;
    public $vkontakte;
    public $vkApiId;
    public $gplus;
    public $facebook_widget;
    public $vkontakte_widget;
    public $gplus_widget;
    public $twitter_widget;
    public $odnoklassniki_widget;
    public $youtube;
    public $twitter;
    public $skype;
    public $linkedin;
    public $odnoklassniki;
    public $vimeo;
    public $rss;
    public $instagram;
    public $dribbble;
    public $pinterest;

    /**
     * Declares the validation rules.
     *
     */
    public function rules()
    {
        return array(
            array('facebook, vkontakte, vkApiId,gplus, facebook_widget, vkontakte_widget,gplus_widget, youtube, twitter,twitter_widget, skype, linkedin, odnoklassniki,odnoklassniki_widget, vimeo, rss, instagram, dribbble, pinterest, vko', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'facebook' => _t('Facebook connection string'),
            'vkontakte' => _t('Vkontakte connection string'),
            'vkApiId' => _t('Vkontakte Api Id'),
            'gplus' => _t('GPlus connection string'),
            'youtube' => _t('Youtube connection string'),
            'twitter' => _t('Twitter connection string'),
            'skype' => _t('Skype login'),
            'linkedin' => _t('LinkedIn connection string'),
            'odnoklassniki' => _t('Odnoklassniki connection string'),
            'vimeo' => _t('Vimeo connection string'),
            'rss' => _t('Rss connection string'),
            'instagram' => _t('Instagram connection string'),
            'dribbble' => _t('Dribbble connection string'),
            'pinterest' => _t('Pinterest connection string'),
        );
    }

}
