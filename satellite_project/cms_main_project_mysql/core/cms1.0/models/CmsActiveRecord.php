<?php
Yii::import('cms.extensions.multiactiverecord.MultiActiveRecord');
class CmsActiveRecord extends MultiActiveRecord
{
	public function connectionId()
    {
        return CONNECTION_NAME;
    }
}