<?php
/**
 * This is the model class for table "{{currency}}".
 *
 * The followings are the available columns in table '{{object}}':
 * @property string $layout
 */
class Currency extends CmsActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{currency}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Currency the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, sign, code, coefficient', 'required'),
            array('name', 'length', 'max' => 50),
            array('is_main', 'length', 'max' => 1),
            array('is_main', 'unique', 'allowEmpty' => true),
            array('is_main', 'unsafe'), // NOTE: не даем менять основную валюту, так как не супортим конвертацию всех денег в системе
            array('coefficient', 'numerical'),
            array('sign, code', 'length', 'max' => 10),
        );
    }

    /**
     * @return array
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => _t('id'),
            'name' => _t('name'),
            'sign' => _t('sign'),
            'code' => _t('code'),
            'coefficient' => _t('coefficient'),
            'is_main' => _t('is_main'),
        );
    }

    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name);
        $criteria->compare('sign', $this->sign, true);
        $criteria->compare('code', $this->code);
        $criteria->compare('coefficient', $this->coefficient);
        $criteria->compare('is_main', $this->is_main);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
