<?php

/**
 * This is the model class for table "{{tag}}".
 *
 * The followings are the available columns in table '{{tag}}':
 * @property string $id
 * @property string $name
 * @property integer $frequency
 * @property string $slug
 */
class Tag extends CmsActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return Tag the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{tag}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, slug', 'required'),
            array('frequency', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('slug', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, frequency, slug', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => _t('ID'),
            'name' => _t('Name'),
            'frequency' => _t('Frequency'),
            'slug' => _t('Slug'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('frequency', $this->frequency);
        $criteria->compare('slug', $this->slug, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns tag names and their corresponding weights logariphmicallly divided on $tagGroupsCount groups.
     * Only the tags with the top weights will be returned.
     * @param integer the maximum number of tags that should be returned
     * @return array weights indexed by tag names.
     */
    public function findTagWeights($limit = 20, $tagGroupsCount = 4)
    {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', CHtml::listData(TagRelationships::model()->findAll(array(
            'distinct' => true,
            'select' => array('tag_id'),
        )), 'tag_id', 'tag_id'));
        $criteria->order = 'frequency DESC';
        $criteria->limit = $limit;
        $models = $this->findAll($criteria);

        $minTagGroupsCount = 1;
        $sizeRange = $tagGroupsCount - $minTagGroupsCount;

        $minCount = log($this->dbConnection->createCommand("SELECT MIN(frequency) FROM " . $this->tableName())->queryScalar() + 1);
        $maxCount = log($this->dbConnection->createCommand("SELECT MAX(frequency) FROM " . $this->tableName())->queryScalar() + 1);
        $countRange = $maxCount - $minCount;

        if ($countRange == 0) {
            $countRange = 1;
        }

        $tags = array();
        foreach ($models as $i => $model) {
            $tags[$i] = $model->attributes;
            $tags[$i]['weight'] = round($minTagGroupsCount + (log($model->frequency + 1) - $minCount) * ($sizeRange / $countRange));
        }

        return $tags;
    }

    /**
     * Suggests a list of existing tags matching the specified keyword.
     * @param string the keyword to be matched
     * @param integer maximum number of tags to be returned
     * @return array list of matching tag names
     */
    public function suggestTags($keyword, $limit = 20)
    {
        $tags = $this->findAll(array(
            'condition' => 'LOWER(name) LIKE :keyword',
            //'order'=>'frequency DESC, Name',
            'order' => 'frequency DESC',
            'limit' => $limit,
            'params' => array(
                ':keyword' => strtolower('%' . strtr($keyword, array('%' => '\%', '_' => '\_', '\\' => '\\\\')) . '%'),
            ),
        ));

        $names = array();
        foreach ($tags as $tag) {
            $names[] = $tag->name;
        }

        return $names;
    }

    public static function string2array($tags)
    {
        return preg_split('/\s*,\s*/', trim($tags), -1, PREG_SPLIT_NO_EMPTY);
    }

    public static function array2string($tags)
    {
        return implode(', ', $tags);
    }

    public function updateFrequency($oldTags, $newTags)
    {
        $oldTags = self::string2array($oldTags);
        $newTags = self::string2array($newTags);
        $this->addTags(array_values(array_diff($newTags, $oldTags)));
        $this->removeTags(array_values(array_diff($oldTags, $newTags)));
    }

    public function addTags($tags)
    {
        $criteria = new CDbCriteria;
        $criteria->addInCondition('name', $tags);
        $this->updateCounters(array('frequency' => 1), $criteria);
        foreach ($tags as $name) {
            if (!$this->exists('name=:name', array(':name' => $name))) {
                $tag = new Tag;
                $tag->name = $name;
                $tag->slug = GxcHelpers::toSlug($name);
                $tag->frequency = 1;
                $tag->save();
            }
        }
    }

    public function removeTags($tags)
    {
        if (empty($tags)) {
            return;
        }

        $criteria = new CDbCriteria;
        $criteria->addInCondition('name', $tags);
        $this->updateCounters(array('frequency' => -1), $criteria);
        $this->deleteAll('frequency<=0');
    }
}
