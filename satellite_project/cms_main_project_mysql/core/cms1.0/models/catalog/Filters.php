<?php

/**
 * This is the model class for table "{{filters}}".
 * Эта модель хранит значения характеристик товаров
 *
 * The followings are the available columns in table '{{filters}}':
 * @property integer $id
 * @property integer $id_object
 * @property string $price
 * @property string $name
 * @property string $filter1
 * @property string $filter2
 * @property string $filter3
 * @property string $filter4
 * @property string $filter5
 * @property string $filter6
 * @property string $filter7
 * @property string $filter8
 * @property string $filter9
 * @property string $filter10
 * @property string $filter11
 * @property string $labels
 */
class Filters extends CmsActiveRecord
{
    /**
     * Разделитель элементов для характеристик Many in List (2)
     */
    const LIST_DELIM = '; ';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Filters the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{filters}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_object', 'required'),
            array('id_object', 'numerical', 'integerOnly' => true),
            array('filter1, filter2, filter3, filter4, filter5, filter6, filter7, filter8, filter9, filter10, filter11', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, id_object, filter1, filter2, filter3, filter4, filter5, filter6, filter7, filter8, filter9, filter10, filter11', 'safe', 'on' => 'search'),
        );
    }

    protected function beforeSave()
    {
        foreach ($this->attributes as $k => $val) {
            if (strpos($k, 'filter') === 0 && is_array($val)) {
                // Many in list
                $this->$k = implode(self::LIST_DELIM, $val);
            }
        }

        return parent::beforeSave();
    }

    protected function afterFind()
    {
        $this->unserlializeFilters();
        return parent::afterFind();
    }

    protected function afterSave()
    {
        $this->unserlializeFilters();
        return parent::afterSave();
    }

    /**
     * Конвертирует в массив такие поля типа Many in list
     */
    public function unserlializeFilters()
    {
        if (isset($this->options->attributes)) {
            foreach ($this->options->attributes as $k => $val) {
                if (strpos($k, 'filter') === 0 && $this->options->options[$k] == 2) {
                    // Many in list
                    $this->$k = explode(self::LIST_DELIM, $this->$k);
                }
            }
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        Yii::import(GxcHelpers::getTruePath('content_type.brand.BrandObject'));
        return array(
            'options' => array(self::BELONGS_TO, 'FiltersOptions', 'id_options'),
            'brand' => array(self::BELONGS_TO, 'BrandObject', 'filter6'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $arr = array(
            'id' => 'ID',
            'name' => _t('Name'),
            'id_options' => _t('Filters'),
            'id_object' => _t('Id Object'),
            'price' => _t('Total price'),
        );

        if (isset($this->options->attributes)) {
            foreach ($this->options->attributes as $k => $val) {
                if (strpos($k, 'filter') === 0) {
                    $arr[$k] = _t($val);
                }
            }
        }
        return $arr;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('id_object', $this->id_object);
        $criteria->compare('name', $this->name, ture);
        $criteria->compare('filter1', $this->filter1, true);
        $criteria->compare('filter2', $this->filter2, true);
        $criteria->compare('filter3', $this->filter3, true);
        $criteria->compare('filter4', $this->filter4, true);
        $criteria->compare('filter5', $this->filter5, true);
        $criteria->compare('filter6', $this->filter6, true);
        $criteria->compare('filter7', $this->filter7, true);
        $criteria->compare('filter8', $this->filter8, true);
        $criteria->compare('filter9', $this->filter9, true);
        $criteria->compare('filter10', $this->filter10, true);
        $criteria->compare('filter11', $this->filter10, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
