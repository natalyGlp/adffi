<?php

/**
 * This is the model class for table "{{filters_options}}".
 * Эта модель хранит настройки характеристик товаров
 *
 * The followings are the available columns in table '{{filters_options}}':
 * @property integer $id
 * @property string $name
 * @property string $filter1
 * @property string $filter2
 * @property string $filter3
 * @property string $filter4
 * @property string $filter5
 * @property string $filter6
 * @property string $filter7
 * @property string $filter8
 * @property string $filter9
 * @property string $filter10
 * @property string $options
 */
class FiltersOptions extends CmsActiveRecord
{
    /**
     *  Поля для установки цены с помощью слайдера (в виджете фильтра)
     */
    public $priceMin;
    public $priceMax;
    protected $_minmax = array(); // кэш min max значений цены
    protected $_minmax4Filter = array(); // кэш min max значений фильтров

    const BRAND = 5;
    public function getTypes()
    {
        return array(
            0 => _t('Text'),
            1 => _t('One in list'),
            2 => _t('Many in list'),
            3 => _t('One in taxonomy list'),
            4 => _t('Many in taxonomy list'),
            self::BRAND => _t('One in brand list'),
            6 => _t('Calendar'),
            7 => _t('Year calendar'),
            8 => _t('Range'), // числовое поле, данные которого должны фильтроваться в виде диапазона
            9 => _t('One in model list'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FiltersOptions the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{filters_options}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('name, filter1, filter2, filter3, filter4, filter5, filter6, filter7, filter8, filter9, filter10', 'length', 'max' => 255),
            array('priceMin, priceMax', 'numerical'),
            array('options', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, filter1, filter2, filter3, filter4, filter5, filter6, filter7, filter8, filter9, filter10, options', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        Yii::import(GxcHelpers::getTruePath('content_type.brand.BrandObject'));
        return array(
            'children' => array(self::HAS_MANY, 'Filters', array('id_options' => 'id')),
            'maxPrice' => array(self::STAT, 'Filters', 'id_options', 'select' => 'MAX(price)'),
            'minPrice' => array(self::STAT, 'Filters', 'id_options', 'select' => 'MIN(price)'),
            // TODO: гребаный костыль... (filter6)
            'brandsId' => array(self::HAS_MANY, 'Filters', array('id_options' => 'id'), 'group' => 'filter6', 'condition' => 'filter6 <> ""'),
            'brands' => array(self::HAS_MANY, 'BrandObject', array('filter6' => 'object_id'), 'through' => 'brandsId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'filter1' => _t('Filter 1'),
            'filter2' => _t('Filter 2'),
            'filter3' => _t('Filter 3'),
            'filter4' => _t('Filter 4'),
            'filter5' => _t('Filter 5'),
            'filter6' => _t('Filter 6'),
            'filter7' => _t('Filter 7'),
            'filter8' => _t('Filter 8'),
            'filter9' => _t('Filter 9'),
            'filter10' => _t('Filter 10'),
            'options' => _t('Options'),
            'priceMin' => _t('From'),
            'priceMax' => _t('To'),
        );
    }

    public static function getList()
    {
        return CHtml::listData(FiltersOptions::model()->findAll(array('select' => array('id', 'name'))), 'id', 'name');
    }

    public function afterFind()
    {
        $this->options = CJSON::decode($this->options);
        $this->options = is_array($this->options) ? $this->options : array();
        return parent::afterFind();
    }

    public function beforeSave()
    {
        $this->options = CJSON::encode(is_array($this->options) ? $this->options : array());
        return parent::beforeSave();
    }

    /**
     *  Возвращает максимальное/минимальное значение характеристики в зависимости от $key
     *  $key может принимать значения min или max
     */
    protected function priceRange($key)
    {
        if (!isset($this->_minmax[$key])) {
            // TODO: кэш
            $this->_minmax[$key] = $this->{$key . 'Price'};
        }

        return (float) $this->_minmax[$key];
    }

    public function getMinPriceVal()
    {
        return $this->priceRange('min');
    }

    public function getMaxPriceVal()
    {
        return $this->priceRange('max');
    }

    /**
     * @return название поля в котором хранится id бренда
     */
    public function getBrandFieldName()
    {
        foreach ($this->filterAttributes as $attribute => $value) {
            if ($this->options[$attribute] == self::BRAND) {
                return $attribute;
            }
        }
    }

    /**
     * @return array названия всех аттрибутов фильтров (например filter1, filter2...)
     */
    public function getFilterAttributes()
    {
        $attributes = $this->attributes;
        // удаляем лишнее
        unset($attributes['id']);
        unset($attributes['name']);
        unset($attributes['options']);

        return $attributes;
    }

    /**
     *  Возвращает максимальное/минимальное значение характеристики в зависимости от $type
     *  $type может принимать значения min или max
     */
    protected function range($type, $key)
    {
        if (!isset($this->$key)) {
            return 0;
        }

        if (!isset($this->_minmax4Filter[$key])) {
            // TODO: Нормальная поддержка префикосв таблиц бд
            // TODO: кэш
            $sql = 'SELECT MAX(' . $key . ') as max, MIN(' . $key . ') as min FROM gxc_filters';
            $command = Yii::app()->{CONNECTION_NAME}->createCommand($sql);
            $this->_minmax4Filter[$key] = $command->queryRow();
        }

        return $this->_minmax4Filter[$key][$type];
    }

    public function getMinValue($key)
    {
        return $this->range('min', $key);
    }

    public function getMaxValue($key)
    {
        return $this->range('max', $key);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('filter1', $this->filter1, true);
        $criteria->compare('filter2', $this->filter2, true);
        $criteria->compare('filter3', $this->filter3, true);
        $criteria->compare('filter4', $this->filter4, true);
        $criteria->compare('filter5', $this->filter5, true);
        $criteria->compare('filter6', $this->filter6, true);
        $criteria->compare('filter7', $this->filter7, true);
        $criteria->compare('filter8', $this->filter8, true);
        $criteria->compare('filter9', $this->filter9, true);
        $criteria->compare('filter10', $this->filter10, true);
        $criteria->compare('options', $this->options, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
