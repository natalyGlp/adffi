<?php

/**
 * This is the model class for table "{{catalog_attributes}}".
 *
 * The followings are the available columns in table '{{catalog_attributes}}':
 * @property integer $id
 * @property integer $id_object
 * @property integer $id_attributes
 * @property string $data
 */
class CatalogAttributes extends CmsActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CatalogAttributes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{catalog_attributes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_object, id_attributes, data', 'required'),
			array('id_object, id_attributes', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_object, id_attributes, data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'options' => array(self::BELONGS_TO, 'CatalogAttributesOptions', 'id_attributes')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_object' => 'Id Object',
			'id_attributes' => 'Attributes',
			'data' => 'Data',
		);
	}

	public function beforeValidate(){
		$this->data = CJSON::encode($this->data);
		return parent::beforeValidate();
	}

	public function afterFind(){
		$this->data = CJSON::decode($this->data);
		$this->data = is_array($this->data) ? $this->data : array();
		return parent::afterFind();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_object',$this->id_object);
		$criteria->compare('id_attributes',$this->id_attributes);
		$criteria->compare('data',$this->data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}