<?php

/**
 * This is the model class for Upload Avatar
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.models.user
 *
 */
class UserAvatarForm extends CFormModel
{
    public $image;

    public function rules()
    {
        return array(
            array('image', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 1024 * 1024 * 2, 'minSize' => 1024),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'image' => _t('Avatar'),
        );
    }

    public static function processUploadedImage(CUploadedFile $uploadedImage)
    {
        //Get the User Id to determine the folder
        $folder = user()->id >= 1000 ? (string) (round(user()->id / 1000) * 1000) : '1000';
        $filename = user()->id . '_' . gen_uuid();
        $avatarsRootPath = Yii::getPathOfAlias('uploads.avatars.root');
        if (!(file_exists($avatarsRootPath . '/' . $folder) && ($avatarsRootPath . '/' . $folder))) {
            mkdir($avatarsRootPath . '/' . $folder, 0777, true);
        }
        if (file_exists($avatarsRootPath . '/' . $folder . '/' . $filename . '.' . strtolower(CFileHelper::getExtension($uploadedImage->name)))) {
            $filename .= '_' . time();
        }

        $filename = $filename . '.' . strtolower(CFileHelper::getExtension($uploadedImage->name));
        $path = $folder . '/' . $filename;

        if ($uploadedImage->saveAs($avatarsRootPath . '/' . $path)) {

            //Generate thumbs
            //
            GxcHelpers::generateAvatarThumb($filename, $folder, $filename);
            return $path;
        } else {
            throw new CHttpException('503', 'Error while uploading!');
        }
    }

    public static function updateUserAvatar($path, $user = false, $save = true)
    {
        //So we will start to check the info from the user
        $current_user = $user ? $user : User::model()->findByPk(user()->id);
        if ($current_user) {
            self::removeUserAvatar($current_user, false);
            $current_user->avatar = $path;

            if ($save && $current_user->save()) {
                user()->setFlash('success', _t('Avatar was successfully updated!'));
            }
        }
    }

    public static function removeUserAvatar($user = false, $save = true)
    {
        $current_user = $user ? $user : User::model()->findByPk(user()->id);
        if ($current_user->avatar != null && $current_user->avatar != '') {
            //We will delete the old avatar here
            $old_avatar_path = $current_user->avatar;
            $avatarsPath = Yii::getPathOfAlias('uploads.avatars');
            if (file_exists($avatarsPath . '/root/' . $old_avatar_path)) {
                @unlink($avatarsPath . '/root/' . $old_avatar_path);
            }

            //Delete old file Sizes
            $sizes = AvatarSize::getSizes();
            foreach ($sizes as $size) {
                if (file_exists($avatarsPath . '/' . $size['id'] . '/' . $old_avatar_path)) {
                    @unlink($avatarsPath . '/' . $size['id'] . '/' . $old_avatar_path);
                }

            }
            $current_user->avatar = '';

            if ($save) {
                if ($current_user->save()) {
                    user()->setFlash('success', _t('Avatar was successfully deleted!'));
                } else {
                    if (Yii::app()->request->isAjaxRequest) {
                        throw new CHttpException(404, "Error Processing Request");
                    }

                }
            }
        }
    }
}
