<?php

/**
 * This is the model class for Register Form.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.models.user
 *
 */
class UserRegisterForm extends CFormModel
{
    public $display_name;
    public $email;
    public $password1;
    public $password2;
    public $first_name;
    public $last_name;

    /**
     * Declares the validation rules.
     * The rules state that display_name and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // display_name, email and password are required
            array('first_name, email, password1, password2', 'required'),
            array('password2', 'compare', 'compareAttribute' => 'password1', 'strict' => true),
            // email need to be email style
            array('email', 'email'),
            array('email', 'unique',
                'attributeName' => 'email',
                'className' => 'cms.models.user.User',
                'message' => _t('This email has been registered.')),
            array('display_name, first_name, last_name', 'match', 'pattern' => '/^[a-zA-Zа-яА-Я0-9_\- ]+$/u'),
            array('display_name, first_name, last_name', 'length', 'max' => 128),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'email' => _t('Email'),
            'password' => _t('Password'),
            'password1' => _t('Password'),
            'password2' => _t('Repeat password'),
            'display_name' => _t('Display Name'),
            'first_name' => _t('First Name'),
            'last_name' => _t('Last Name'),
        );
    }

    public function getPassword()
    {
        return $this->password1;
    }

    /**
     * Function to Register user information
     * @return type
     */
    public function doSignUp($instantLogin = true)
    {
        if ($this->validate()) {
            $newUser = new User;

            $new_user = new User;
            $new_user->scenario = 'create';
            $new_user->email = $this->email;
            $new_user->first_name = $this->first_name;
            $new_user->last_name = $this->last_name;
            $new_user->password = $this->password;

            if (!$new_user->save()) {
                $this->addError('email', _t('Something is wrong with the Registration Process. Please try again later!'));
                return false;
            } else {
                $valid = true;
                //We can start to add Profile record here

                //We can start to add User Activity here

                //We can check to send Email or not
                $new_user->sendActivationEmail();

                //Create new UserLoginForm
                if ($instantLogin) {
                    $login_form = new UserLoginForm();
                    $login_form->username = $newUser->username;
                    $login_form->password = $this->password;
                    $valid = $valid && $login_form->login();
                }

                if ($valid) {
                    return $new_user;
                } else {
                    return false;
                }
            }
        }
    }
}
