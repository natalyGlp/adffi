<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property string $id
 * @property string $user_id
 * @property string $address_id
 * @property integer $status
 * @property integer $type
 * @property integer $currency
 * @property integer $currency_provider сервис через который был оплачен заказ
 * @property string $comment to order
 * @property string $referer
 * @property string $ip
 * @property string $date
 * @property int $operator_id id of operator, that has manualy added this order
 *
 * The followings are the available model relations:
 * @property OrderAddress $address
 * @property AuthUser $user
 * @property OrderCheck[] $orderChecks
 * @property Currency $Currency
 *
 * @author     Sviatoslav Danylenko <dev@udf.su>
 * @package    cms.models.order
 */
class Order extends CmsActiveRecord
{
    // Варианты оплаты заказа
    // @see getOrderCurrency()
    protected $_orderCurrency = array(
        1 => 'Оплата наличными',
        //2 => 'WMR',
        //3 => 'WMZ',
        4 => 'WMU',
        5 => 'Приват24: UAH',
        //6 => 'Приват24: USD',
        //7 => 'Приват24: EUR',
        8 => 'Безналичный расчет',
    );

    protected static $_currencyIdList;

    const NOT_COMPLETE = 1;
    const IN_PROCESS = 2;
    const COMPLETE = 3;
    const CANCELED = 4;
    const APPROVED = 5;
    const SENT_TO_DELIV = 6;
    const DELIVERING = 7;
    const RETURNED = 8;

    public $orderStatus = array(
        self::NOT_COMPLETE => 'Новый', // заказ получен и его еще никто не трогал
        self::APPROVED => 'Подтвержден', // заказ прозвонен, сверен адрес(проставляется коллменеджером)
        self::IN_PROCESS => 'Обработка', // TODO: заморозить товар;
        self::SENT_TO_DELIV => 'Передан сл. доставки', //
        self::DELIVERING => 'В пути', // менеджер получил штрих-код/инфу/цену от сл. доставки
        self::COMPLETE => 'Выполнено', // заказ оплачен, получены деньги
        self::RETURNED => 'Возврат', // товар вернулся к продавцу на нашеотделение.
        self::CANCELED => 'Отмена', // заказ не подтвержден коллменеджером или отменен заказчиком
    );

    // массив с id статусов по которым надо фильтровать с помощью search();
    public $statusArr;
    public $filterDateFrom;
    public $filterDateTo;
    public $gridSelectedItems;
    public $orderSearch;

    protected $_oldStatus;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{order}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, currency_id, currency_provider', 'required'),
            array('status, type, currency_provider', 'numerical', 'integerOnly' => true),
            array('type, status, currency_provider', 'default', 'value' => 1, 'setOnEmpty' => true),
            array('comment, user_id', 'safe'),
            array('referer', 'length', 'max' => 255),
            array('currency_id', 'length', 'max' => 3),
            array('currency_id', 'unsafe', 'on' => 'update'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, user_id, address_id, status, type, ip, date, statusArr, orderSearch, filterDateFrom, filterDateTo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Залогениным юзерам даем доступ только к их
     * материалам или только к материалам на их сайтах
     * @return array критерий поиска
     */
    public function defaultScope()
    {
        $criteria = array();

        if (Yii::app()->id == 'satellite' && !Yii::app()->user->isGuest && !Yii::app()->user->isAdmin) {
            // TODO: убрать этот костыль
            $cacheId = 'user-sites-criteria-' . Yii::app()->user->id;
            if (!($condition = Yii::app()->cache->get($cacheId))) {
                $userSites = User::model()->findByPk(Yii::app()->user->id)->sites;
                $userSites = CHtml::listData($userSites, 'cleanUrl', 'cleanUrl');

                $condition = 'referer IN ("' . implode('","', $userSites) . '")';

                Yii::app()->cache->set($cacheId, $condition, 60);
            }

            $criteria = array(
                'condition' => $condition,
            );
        }

        return $criteria;
    }

    /**
     *  Преобразуем ip в адекватный формат
     */
    protected function afterFind()
    {
        $this->ip = long2ip($this->ip);
        $this->_oldStatus = $this->status;
    }

    /**
     *  Сохраняем ip юзера
     *  Обновляем даты
     */
    protected function beforeSave()
    {
        if (parent::beforeSave()) {

            if ($this->isNewRecord) {
                $this->date = new CDbExpression('NOW()');

                if (empty($this->status)) {
                    $this->status = self::NOT_COMPLETE;
                }

                $this->referer = trim(str_replace('http:', '', $this->referer), '/');
            }

            $this->ip = ip2long($this->ip);

            if (!$this->canChangeStatus) {
                $this->status = $this->_oldStatus;
            }

            return true;
        } else {
            return false;
        }
    }

    protected function getCanChangeStatus()
    {
        return !in_array($this->_oldStatus, array(Order::COMPLETE, Order::CANCELED, Order::RETURNED));
    }

    protected function afterSave()
    {
        if ($this->isOrderJustCompleted) {
            $this->updateWarehouse();
        }

        $this->syncWithAffiliateSystem();

        $this->updateNotifications();

        $this->_oldStatus = $this->status;

        return parent::afterSave();
    }

    /**
     * Обновляет статус заказа в аффилиатной системе
     */
    public function syncWithAffiliateSystem()
    {
        if ($this->isStatusChanged) {

            if ($this->status == self::CANCELED) {
                Yii::app()->controller->module->affiliate->declineOrder($this->id);

            } elseif ($this->_oldStatus == self::NOT_COMPLETE) {
                Yii::app()->controller->module->affiliate->approveOrder($this->id);
            }
        }
    }

    public function getIsOrderJustCompleted()
    {
        return $this->status == Order::COMPLETE && $this->_oldStatus != Order::COMPLETE;
    }

    public function getIsStatusChanged()
    {
        return $this->_oldStatus != $this->status;
    }

    protected function updateWarehouse()
    {
        Yii::import(GxcHelpers::getTruePath('content_type.catalog.CatalogObject'));
        foreach ($this->check as $check) {
            $model = CatalogObject::model()->findByPk($check->prod_id);
            $model->product_count -= $check->quantity;
            $model->save();
        }
    }

    protected function updateNotifications()
    {
        $count = Order::model()->count(array('condition' => 'status=' . Order::NOT_COMPLETE));
        Yii::app()->notify->create('shopNewOrders', array(
            'counter' => $count,
            'title' => 'Новые заказы',
            'message' => 'У вас ' . $count . ' новых заказов',
            'updateByCategory' => true,
        ));
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'address' => array(self::BELONGS_TO, 'OrderAddress', 'address_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'client' => array(self::BELONGS_TO, 'Client', array('id' => 'order_id')),
            'currency' =>array(self::BELONGS_TO, 'Currency', 'currency_id'),
            // для большей универсальности добудем название primaryKey модели User
            // эта модель будет не очень активно использоваться, так что на производительность это не повлияется
            // особенно если учесть кеширование схем бд
            'operator' => array(self::BELONGS_TO, 'User', array('operator_id' => User::model()->tableSchema->primaryKey)),
            'check' => array(self::HAS_MANY, 'OrderCheck', 'order_id'),
            'payment' => array(self::BELONGS_TO, 'CurrencyProvider', 'currency_provider'),
        );
    }

    /**
     *  Возвращает ссылку на просмотр чека
     **/
    public function getViewUrl()
    {
        return Yii::app()->createUrl('beorder/check', array('id' => $this->id));
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'address_id' => 'address',
            'status' => 'Статус заказа',
            'currency_id' => 'Валюта',
            'currency_provider' => 'Способ оплаты',
            'type' => 'Доставка',
            'ip' => 'Ip',
            'referer' => 'Сайт-источник',
            'date' => 'Дата заказа',
            'operator_id' => 'Оператор',
            'comment' => 'Комментарий оператора',
        );
    }

    public function getPayMethod()
    {
        return $this->payment->name;
    }

    /**
     * Возвращает доступные валюты для использования в форме оформления заказа
     *
     * @access public
     * @return array
     */
    public function getOrderCurrency()
    {
        // TODO: настройки цмс!
        $currencyParams = isset(Yii::app()->modules['cart']['params']['emoney'])
        ? Yii::app()                      ->modules['cart']['params']['emoney']
        : array(
            'WM' => array(
                'active' => '1',
                'secretKey' => '',
                'purse' => 'U514759930779',
            ),
            'Privat24' => array(
                'active' => '1',
                'secretKey' => '',
                'purse' => 'UAH',
            ),
            'other' => array(
                0 => '1',
                1 => '8',
            ),
        );
        // прочие, не электронные, способы оплаты
        if (isset($currencyParams['other']) && is_array($currencyParams['other'])) {
            foreach ($currencyParams['other'] as $currencyId) {
                $enabledCurrencies[$currencyId] = $this->_orderCurrency[$currencyId];
            }
            unset($currencyParams['other']);
        }

        foreach ($currencyParams as $emoneyId => $emoney) {
            if ($emoney['active'] == true) {
                switch ($emoneyId) {
                    case 'WM':
                        $enabledCurrencies[4] = $this->_orderCurrency[4];
                        break;
                    case 'Privat24':
                        $enabledCurrencies[5] = $this->_orderCurrency[5];
                        break;
                }
            }
        }

        if (!isset($enabledCurrencies)) {
            throw new CException('Настройте хоть один способ оплаты в админкe');
        }

        return $enabledCurrencies;
    }

    /**
     * Возвращает доступные способы доставки
     *
     * @access public
     * @return array
     */
    public function getOrderType()
    {
        $typeParams = OrderType::model()->cache(5 * 60)->findAllByAttributes(array('enabled' => 1));

        if (!count($typeParams)) {
            Yii::app()->user->setFlash('error', 'Настройте хоть один способ доставки');
        }

        $enabledTypes = CHtml::listData($typeParams, 'id', 'name');

        return $enabledTypes;
    }

    /**
     * Возвращает способ доставки текущего заказа
     *
     * @access public
     * @return string
     */
    public function getOrderTypeString()
    {
        $typeParams = self::getOrderType();

        return isset($typeParams[$this->type]) ? $typeParams[$this->type] : 'undefined';
    }

    /**
     * @return array массив с id валют для выпадающих менюшек
     */
    public static function getCurrencyIdList()
    {
        if (!self::$_currencyIdList) {
            $path = Yii::getPathOfAlias('system.i18n.data.' . Yii::app()->locale->id) . '.php';
            if (!file_exists($path)) {
                $path = Yii::getPathOfAlias('system.i18n.data.ru_ru') . '.php';
            }

            $data = include ($path);

            $currencies = array_values(array_flip($data['currencySymbols']));
            // нужно переместить популярные валюты на верх
            unset($currencies['UAH']);
            unset($currencies['RUR']);
            unset($currencies['USD']);
            unset($currencies['EUR']);
            $currencies = array(
                'UAH',
                'RUR',
                'USD',
                'EUR',
            ) + $currencies;

            self::$_currencyIdList = array_combine($currencies, $currencies);
        }

        return self::$_currencyIdList;
    }

    /**
     * Возвращает символ валюты по ее id
     *
     * @access public
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->currency ? $this->currency->sign : '';
    }

    /**
     * Форматирует деньги согласно текущей локали
     *
     * @param float $value число для форматирования
     * @param boolean $withCurrencySymbol если true, то вместе с числом вернется символ валюты
     * @return string
     */
    public function formatMoney($value, $withCurrencySymbol = true)
    {
        if ($withCurrencySymbol) {
            return Yii::app()->numberFormatter->formatCurrency($value, $this->getCurrencySymbol());
        } else {
            $format = preg_replace('/\p{Zs}?¤\p{Zs}?/u', '', Yii::app()->locale->getCurrencyFormat());
            return Yii::app()->numberFormatter->format($format, $value);
        }
    }

    /**
     * Возвращает общую стоимость заказа
     *
     * @param boolean $formated - если true, то вернет строку со стоимостью, локализированной для пользователя
     * @param boolean $withCurrencySymbol если true, то вместе с числом вернется символ валюты
     * @access public
     * @return string|float
     */
    public function getTotalPrice($formated = false, $withCurrencySymbol = true)
    {
        $totalPrice = 0;
        foreach ($this->check as $check) {
            // $totalPrice += $check->price*$check->quantity;
            $totalPrice += $check->total_price;
        }

        return $formated ? self::formatMoney($totalPrice, $withCurrencySymbol) : (float) $totalPrice;
    }

    /**
     * Возвращает общую стоимость заказа во внутренней валюте
     * @param bool $withCurrencySymbol
     * @return int|string
     */
    public function getTotalInternalPrice($withCurrencySymbol = true)
    {
        if($withCurrencySymbol) {
            $symbol = Currency::model()->find('is_main = 1')->sign;
        }
        $totalPrice = 0;
        foreach ($this->check as $check) {
            // $totalPrice += $check->price*$check->quantity;
            $totalPrice += $check->internal_price;
        }

        return !$withCurrencySymbol ? $totalPrice : $totalPrice.' '.$symbol;
    }

    /**
     * @return array массив с курсами валют
     */
    public static function getCurrencyRates()
    {
        $ccy = Yii::app()->cache->get('ccy');
        if (!$ccy) {
            $ccy = array('UAH' => 1.0);
            $xml = simplexml_load_file('https://privat24.privatbank.ua/p24/accountorder?oper=prp&PUREXML&apicour&country=ua&full');
            $json_string = json_encode($xml);
            $result_array = json_decode($json_string, true);

            if (isset($result_array['exchangerate'])) {
                foreach ($result_array['exchangerate'] as $item) {
                    // <exchangerate ccy="BYR" ccy_name_ru="Беларусский рубль" ccy_name_ua="Бiлоруський рубль" ccy_name_en="Belarussian Rouble" base_ccy="UA" buy="84" unit="10.00000" date="2014.01.08"/>
                    // ccy - код валюты (о том какие они существуют, Вы можете посмотреть здесь)
                    // ccy_name_ru - название валюты на русском языке
                    // ccy_name_ua - название валюты на украинском языке
                    // ccy_name_en - название валюты на английском языке
                    // base_ccy - код выбранной вами страны
                    // buy - курс покупки(коп * 100)
                    // unit - количество единиц валюты, которые можно купить по  курсу покупки
                    // date - дата последнего обновления курсов валют
                    // @see https://api.privatbank.ua/article/8/
                    $ccy[$item['@attributes']['ccy']] = round($item['@attributes']['buy'] / 10000 / $item['@attributes']['unit'], 2);
                }
            }

            Yii::app()->cache->set('ccy', $ccy);
        }

        return $ccy;
    }

    /**
     * @return array массив со списком всех refferer
     */
    public function getReferers()
    {
        // 15 мин кэша
        $models = $this->cache(60 * 15)->findAll(array(
            'group' => 'referer',
        ));

        $data = CHtml::listData($models, 'referer', 'referer');
        if (isset($data[''])) {
            unset($data['']);
        }

        return $data;
    }

    /**
     * Возвращает количество зарезервированного товара
     *
     * @param integer $prodId id товара для которого нужно получить количество резерва
     * @return integer Возвращает количество зарезервированного товара
     */
    public static function getReserved($prodId)
    {
        Yii::import('cms.models.order.OrderCheck');
        $nonReservingStatuses = array(
            Order::NOT_COMPLETE,
            Order::COMPLETE,
            Order::CANCELED,
        );
        $reserved = 0;
        if (!empty($prodId)) {
            $reserved = Order::model()->find(array(
                'select' => 'SUM(check.quantity) as statusArr',
                'condition' => 'status NOT IN (:statuses) AND check.prod_id=' . $prodId,
                'params' => array(':statuses' => implode(',', $nonReservingStatuses)),
                'with' => array('check'),
            ));
            $reserved = $reserved ? $reserved->statusArr : 0;
        }

        return $reserved;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        //$criteria->compare('user_id',$this->user_id,true);
        //$criteria->compare('address_id',$this->address_id,true);
        //$criteria->compare('status',$this->status);
        if (count($this->statusArr)) {
            $criteria->addInCondition('status', $this->statusArr);
        }

        $criteria->compare('currency_provider', $this->currency_provider);
        $criteria->compare('type', $this->type);
        $criteria->compare('referer', $this->referer, true);
        //$criteria->compare('ip',$this->ip,true);
        //$criteria->compare('date',$this->date,true);

        $filterDateFrom = isset($this->filterDateFrom) && trim($this->filterDateFrom) != ""
        ? date_format(new DateTime($this->filterDateFrom), 'Y-m-d 00:00:00')
        : false;

        $filterDateTo = isset($this->filterDateTo) && trim($this->filterDateTo) != ""
        ? date_format(new DateTime($this->filterDateTo), 'Y-m-d 23:59:59')
        : false;

        if ($filterDateFrom && $filterDateTo) {
            $criteria->addBetweenCondition('date', $filterDateFrom, $filterDateTo);
        } else {
            if ($filterDateFrom) {
                $criteria->compare('date', '>=' . $filterDateFrom);
            }

            if ($filterDateTo) {
                $criteria->compare('date', '<=' . $filterDateTo);
            }
        }

        if (!empty($this->orderSearch)) {
            $searchAllCriteria = $this->searchAllCriteria();
            $criteria->mergeWith($searchAllCriteria, 'AND');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'date DESC'),
        ));
    }

    /**
     * Глобальный поиск
     * @return CDbCriteria
     */
    protected function searchAllCriteria()
    {
        $searchAllCriteria = new CDbCriteria();
        $searchAllCriteria->together = true;
        $searchAllCriteria->with = array('client', 'address');
        $searchAllCriteria->addSearchCondition('currency_id', $this->orderSearch, false, 'OR');
        $searchAllCriteria->addSearchCondition($this->tableAlias . '.id', $this->orderSearch, false, 'OR');
        $searchAllCriteria->addSearchCondition('client.first_name', $this->orderSearch, false, 'OR');
        $searchAllCriteria->addSearchCondition('comment', $this->orderSearch, false, 'OR');
        $searchAllCriteria->addSearchCondition('referer', $this->orderSearch, false, 'OR');
        $searchAllCriteria->compare('address.telephone', $this->orderSearch, false, 'OR');

        $operatorsList = Yii::app()->{CONNECTION_NAME}->createCommand()
                                   ->select('user_id')
                                   ->from('{{user}}')
                                   ->where('display_name LIKE :searchTerm OR email LIKE :searchTerm', array(
            ':searchTerm' => '%' . strtr($this->orderSearch, array('%' => '\%', '_' => '\_', '\\' => '\\\\')) . '%',
        ))
            ->queryColumn();
        $searchAllCriteria->compare('operator_id', $operatorsList, false, 'OR');
        return $searchAllCriteria;
    }
}
