<?php

/**
 * Class CurrencyProvider
 * @property $name
 */
class CurrencyProvider extends CmsActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{currency_provider}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Currency the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 150),
        );
    }

    /**
     * @return array
     */
    public function relations()
    {
        return array(
        );
    }

    protected function beforeDelete()
    {
        if ($this->id == 1) {
            return false;
        }
        return parent::beforeDelete();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => _t('id'),
            'name' => _t('name'),
        );
    }
}
