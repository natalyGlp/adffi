<?php

/**
 * This is the model class for table "order_check".
 *
 * The followings are the available columns in table 'order_check':
 * @property string $id
 * @property string $order_id
 * @property string $prod_id
 * @property string $quantity
 * @property string $price
 * @property string $internal_price
 * @property string $currency
 * @property string $total_price сумма заказа с учетом скидок и прочего
 * @property string $meta дополнительная информация о товаре (цвет, размер и т.д.)
 *
 * The followings are the available model relations:
 * @property CatalogObject $prod
 * @property Order $order
 *
 * @author     Sviatoslav Danylenko <dev@udf.su>
 * @package    cms.models.order
 */
class OrderCheck extends CmsActiveRecord
{
    protected $_oldQuantity;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OrderCheck the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{order_check}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('order_id, prod_id, quantity, price, internal_price', 'required', 'message' => 'Добавьте хоть один товар в заказ'),
            array('order_id, quantity', 'length', 'max' => 10),
            array('prod_id', 'length', 'max' => 11),
            array('total_price, price, internal_price', 'numerical'),
            array('currency_id', 'unsafe', 'on' => 'update'),
            array('id, order_id, prod_id, currency_id, quantity, internal_price', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        if (!class_exists('CatalogObject')) {
            Yii::import(GxcHelpers::getTruePath('content_type.catalog.CatalogObject'));
        }

        return array(
            'prod' => array(self::BELONGS_TO, 'CatalogObject', 'prod_id'),
            'currency' => array(self::BELONGS_TO, 'Currency', 'currency_id'),
            'productData' => array(self::BELONGS_TO, 'ShopProductData', array('id' => 'currency_id')),
            'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'order_id' => 'Order',
            'prod_id' => 'Добавить товар',
            'quantity' => 'Количество',
            'meta' => 'Дополнительная информация о товаре',
            'price' => 'Цена товара',
            'total_price' => 'Сумма',
        );
    }

    protected function afterFind()
    {
        parent::afterFind();
        // создаем из мета информации необходимую нам структуру
        if (!empty($this->meta)) {
            $meta = '';
            foreach (unserialize($this->meta) as $name => $value) {
                $meta .= "<b>$name</b>: $value<br />";
            }

            $this->meta = $meta;
        }

        $this->_oldQuantity = $this->quantity;
    }

    protected function beforeValidate()
    {
        $this->recalculatePrice();
        return parent::beforeValidate();
    }

    /**
     * @method recalculatePrice
     */
    public function recalculatePrice()
    {
        if ($this->quantityChanged) {
            $productData = ShopProductData::model()->findByAttributes(array(
                'product_id' => $this->prod_id,
                'currency_id' => $this->currency_id,
            ));

            $currencyModel = $this->currency;
            $this->price = $productData->sellPrice;
            $this->total_price = $productData->calculateTotalPrice($this->quantity);
            $this->internal_price = $currencyModel->is_main ? $this->total_price : $this->total_price * $currencyModel->coefficient;
        }
    }

    public function getQuantityChanged()
    {
        return $this->_oldQuantity != $this->quantity;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('order_id', $this->order_id, true);
        $criteria->compare('prod_id', $this->prod_id, true);
        $criteria->compare('quantity', $this->quantity, true);
        $criteria->compare('internal_price', $this->internal_price, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Проверяет заданы ли fk заказа и продукта
     *
     * @param boolean $throw флаг, включающий выкидывание исключений, если модели не хватает внешних ключей
     * @return boolean true, если все в порядке
     * @throws CException
     */
    protected function isProductBond($throw = false)
    {
        $isBond = !empty($this->order_id) && !empty($this->prod_id);
        if (!$isBond && $throw) {
            throw new CException('You should pass both order and product models for OrderCheck model!');
        }

        return $isBond;
    }

    /**
     * Конвертирует цену товара в другую валюту
     * ВНИМАНИЕ! Этот метод не меняет валюту заказа Order. Что бы корректно изменить валюту нужно использовать Order::convertPriceCurrency()
     *
     * @param string $currency ISO 421 код целевой валюты (например UAH)
     */
    public function convertPriceCurrency($currency)
    {
        $rates = Order::getCurrencyRates();
        if (!isset($rates[$currency])) {
            throw new CException('Ошибка конвертации валюты. Нет данных для валюты ' . $currency);
        }

        $rate = 1 / $rates[$currency];
        if ($this->currency != 'UAH') {
            // конвертируем сначала в базовую валюту
            $rate *= $rates[$this->currency];
        }

        $this->price = round($this->price * $rate, 2);
        $this->total_price = round($this->total_price * $rate, 2);
        $this->currency = $currency;
    }

    /**
     * Возвращает стоимость за единицу товара
     *
     * @param boolean $formated - если true, то вернет строку со стоимостью, локализированной для пользователя
     * @param boolean $withCurrencySymbol если true, то вместе с числом вернется символ валюты
     * @access public
     * @return string|float
     */
    public function getPrice($formated = false, $withCurrencySymbol = true)
    {
        return $formated ? $this->order->formatMoney($this->price, $withCurrencySymbol) : (float) $this->price;
    }

    /**
     * Возвращает общую стоимость товара
     *
     * @param boolean $formated - если true, то вернет строку со стоимостью, локализированной для пользователя
     * @param boolean $withCurrencySymbol если true, то вместе с числом вернется символ валюты
     * @access public
     * @return string|float
     */
    public function getTotalPrice($formated = false, $withCurrencySymbol = true)
    {
        // $totalPrice = $this->price * $this->quantity;
        $totalPrice = $this->total_price;
        return $formated ? $this->order->formatMoney($totalPrice, $withCurrencySymbol) : (float) $totalPrice;
    }
}
