<?php
/**
 * This is the model class for table "{{search_lucene}}".
 *
 * The followings are the available columns in table '{{search_lucene}}':
 * @property string $id
 * @property string $index_id
 * @property string $modified_date
 * @property string $created_date
 * @property string $last_object_date
 * @property string $params
 *
 * @author Sviatoslav Danylenko dev@udf.su
 */

// TODO: нужно оптимизировать класс для работы в режиме итератора, так как сейчас индекс обновляется кусками, а не целиком, как было задумано изначально. лучше всего вынести в консольное приложение логику необходимую для создания/обновления индекса, а тут оставить только то, что необходимо для добавления новых обьектов
// TODO: нужно так же обратить внимание, что бы во время обновления не было частых вызовов commit/optimize
class SearchLucene extends CmsActiveRecord
{
    /**
     * @var array $objectTypes массив с типами обьектов, которые должны быть в индексе
     */
    public $objectTypes = array();

    /**
     * @var array $_queries кэш обьектов Query из Zend_Search_Lucene_Search_QueryParser::parse()
     */
    private static $_queries = array();

    const ALL_TYPES_ID = '_allAvailable';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SearchLucene the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{search_lucene}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('index_id, objectTypes', 'required'),
            array('index_id', 'unique'),
            array('index_id', 'length', 'max' => 128),
            array('created_date, last_object_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, index_id, modified_date, created_date, last_object_date, objectTypes', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => _t('ID'),
            'index_id' => _t('Index ID'),
            'modified_date' => _t('Modified'),
            'created_date' => _t('Created'),
            'prettyLastObjectDate' => _t('Last Object Date'),
            'prettyModifiedDate' => _t('Modified'),
            'prettyCreatedDate' => _t('Created'),
            'last_object_date' => _t('Last Object Date'),
            'objectTypes' => _t('Object types'),
            'documentsCount' => _t('Documents Count'),
        );
    }

    public function findByIndexId($indexId)
    {
        return $this->findByAttributes(array('index_id' => $indexId));
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->created_date = new CDbExpression('NOW()');
        }

        $this->modified_date = new CDbExpression('NOW()');
        $this->params = array(
            'objectTypes' => $this->objectTypes,
        );
        $this->params = CJSON::encode($this->params);

        return parent::beforeSave();
    }

    protected function afterFind()
    {
        $this->params = CJSON::decode($this->params);
        $this->objectTypes = $this->params['objectTypes'];
        parent::afterFind();
    }

    protected function afterSave()
    {
        $this->params = CJSON::decode($this->params);
        $this->objectTypes = $this->params['objectTypes'];

        parent::afterSave();
    }

    protected function afterDelete()
    {
        self::deleteIndex($this->index_id);
        parent::afterDelete();
    }

    /**
     * Создает индекс, но не добавляет туда что либо
     */
    public function createIndex()
    {
        self::importLucene();
        $path = self::getPathOfIndex($this->index_id, true);

        // начинаем с чистого листа
        self::deleteIndex($this->index_id);

        $index = Zend_Search_Lucene::create($path);
        $index->commit();

        return true;
    }

    /**
     * Заполняет индекс новыми моделями
     */
    public function updateIndex($criteria = false, $checkThatExists = true)
    {
        self::importLucene();
        $path = self::getPathOfIndex($this->index_id);

        $index = Zend_Search_Lucene::open($path);

        if (!$criteria) {
            $criteria = $this->getObjectsCriteria(array(
                'latest' => true,
            ));
        }

        $objects = Object::model()->findAll($criteria);
        $this->processObjectList($objects, $index, $checkThatExists);

        return true;
    }

    public function getObjectsCriteria($options = array())
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'object_status = ' . Object::STATUS_PUBLISHED;
        if (!isset($options['latest']) || (isset($options['latest']) && $options['latest'])) {
            $criteria->condition .= ' AND (object_date > :date OR object_modified > :date)';
            $criteria->params = array(':date' => strtotime($this->last_object_date));
        }

        if ($this->objectTypes[0] != self::ALL_TYPES_ID) {
            $criteria->addInCondition('object_type', $this->objectTypes);
        }

        $criteria->order = 'object_date DESC';

        return $criteria;
    }

    /**
     * Добавляет модел Object в $index и обновляет дату обновления модели $SearchLucene
     * @param boolean $update если true, метод сначала попытается удалить обьект из индекса, что бы заметь его более новым
     */
    protected function processObjectList($objects, $index, $checkThatExists = true)
    {
        $last_modified = 0;
        foreach ($objects as $object) {
            if ($checkThatExists) {
                self::deleteObjectFromIndex($object, $index, false);
            }

            self::addObjectToIndex($object, $index);
            $last_modified = max($last_modified, max($object->object_modified, $object->object_date));
        }
        $index->commit();

        $this->last_object_date = date('Y-m-d H:i:s', $last_modified);
    }

    /**
     * Удаляет индекс
     *
     * @param $indexId индекс в который нужно добавить модель
     * @param Object|array of Object $objects модель или массив моделей Object, которые необходимо добавить в индекс
     */
    public static function deleteIndex($indexId)
    {
        self::importLucene();
        $path = self::getPathOfIndex($indexId);

        if (is_dir($path)) {
            self::delTree($path);
        }

        return true;
    }

    public function optimize()
    {
        self::optimizeIndex($this->index_id);
    }

    /**
     * Запускает оптимизацию на индексе $index
     *
     * @param mixed $index Обьект индекса или его id
     */
    public static function optimizeIndex($index)
    {
        self::importLucene();

        if (($index = self::getIndex($index))) {
            $index->optimize();

            return true;
        } else {
            return false;
        }
    }

    public static function addObjectToIndex(Object $object, $index)
    {
        self::importLucene();
        if (!($index = self::getIndex($index))) {
            return;
        }

        if ($object->object_status != Object::STATUS_PUBLISHED) {
            return self::deleteObjectFromIndex($object, $index);
        }

        $doc = new Zend_Search_Lucene_Document();

        $doc->addField(Zend_Search_Lucene_Field::Keyword('primaryKey', $object->object_id));

        $doc->addField(Zend_Search_Lucene_Field::UnStored('title',
            strip_tags(mb_strtolower($object->object_name, 'utf-8')), 'utf-8')
        );

        $doc->addField(Zend_Search_Lucene_Field::UnStored('content',
            strip_tags(mb_strtolower($object->object_content, 'utf-8')), 'utf-8')
        );

        $index->addDocument($doc);
    }

    /**
     * Обновляет только один обьект в индексе. Метод расчитан на использование внутри afterSave модели Object
     */
    public static function updateObjectInIndex(Object $object, $index)
    {
        if (!($index = self::getIndex($index))) {
            return;
        }
        self::deleteObjectFromIndex($object, $index, false);
        self::addObjectToIndex($object, $index);
        $index->commit();
    }

    public static function deleteObjectFromIndex(Object $object, $index, $commit = true)
    {
        self::importLucene();
        if (!($index = self::getIndex($index))) {
            return;
        }

        $query = new Zend_Search_Lucene_Search_Query_Term(new Zend_Search_Lucene_Index_Term($object->primaryKey, 'primaryKey'));
        $hits = $index->find($query);

        foreach ($hits as $hit) {
            $index->delete($hit->id);
        }
        if ($commit) {
            $index->commit();
        }

    }

    /**
     * Ищет по индексу и возвращает результаты
     *
     * @param string $query запрос юзера
     * @param string $indexId id индекса
     *
     * @return array() результаты поиска
     */
    public static function doSearch($query, $indexId)
    {
        self::importLucene();

        $query = self::parseQuery($query);

        $index = new Zend_Search_Lucene(self::getPathOfIndex($indexId));
        $results = $index->find($query);
        unset($index);

        return $results;
    }

    /**
     * Генерирует сниппет выделяя ключевые слова и обрезая текст возле них
     *
     * @param string $query поисковый запрос
     * @param string $content содержимое для генерации сниппета
     * @param array $params дополнительные параметры
     */
    public static function getSearchSnippet($query, $content, $params = array())
    {
        $initialContent = $content;
        if (!empty($query)) {
            $queryParts = preg_split('/\s+/u', $query); // FIXME: было бы не плохо получать ключевики от Lucene

            $wordsToHighlight = implode(' ', $queryParts);

            $content = strip_tags($content);
            $content = self::truncatePreserveWords($content, $wordsToHighlight, 10);
            $content = self::highlightMatches($query, $content);
        } else {
            $content = '';
        }

        if (strlen($content) < 5) {
            $content = mb_substr(strip_tags($initialContent), 0, 200, 'UTF-8') . '...';
        }

        return $content;
    }

    /**
     * Выделяет ключевые слова в $string
     *
     * @param string $query поисковый запрос
     * @param string $string строка в которой нужно выделить ключевики
     */
    public static function highlightMatches($query, $string)
    {
        $query = self::parseQuery($query);

        return $query->htmlFragmentHighlightMatches($string);
    }

    /**
     * @param string $query Запрос введенный юзером
     * @return Обьект Query
     */
    public static function parseQuery($query)
    {
        self::importLucene();
        if (!isset(self::$_queries[$query])) {
            self::$_queries[$query] = Zend_Search_Lucene_Search_QueryParser::parse($query);
        }

        return self::$_queries[$query];
    }

    /**
     * @param $text
     * @param $keywords to find separated by space
     * @see http://snipplr.com/view.php?codeview&id=23666
     *
     * @return текст обрезанный на $w слов от ключевика
     */
    public static function truncatePreserveWords($text, $keywords, $w = 5, $fragmentsAmount = 3)
    {
        Yii::import('cms.models.search.MorphFilter');
        $keywords = explode(" ", trim(strip_tags($keywords))); //needles words
        $textParts = explode(" ", trim(strip_tags($text))); //haystack words

        $morphy = new MorphFilter();

        $smallWords = array();
        // отсеим слова короче трех букв в отдельный массив
        foreach ($keywords as $key => $keyword) {
            if (mb_strlen($keyword) < 3) {
                array_push($smallWords, $keyword);
                unset($keywords[$key]);
            } else {
                $keywords[$key] = $morphy->getRoot($keywords[$key]);
            }
        }

        $keywords = array_values($keywords);

        // array of words to keep/remove
        $c = array();
        for ($j = 0; $j < count($textParts); $j++) {
            $c[$j] = false;
        }

        for ($i = 0; $i < count($textParts); $i++) {
            for ($k = 0; $k < count($keywords); $k++) {
                if (mb_stripos($textParts[$i], $keywords[$k], 0, 'utf-8') !== false) {
                    for ($j = max($i - $w, 0); $j < min($i + $w, count($textParts)); $j++) {
                        $c[$j] = true;
                    }

                    $fragmentsAmount--;
                }
                if ($fragmentsAmount <= 0) {
                    break;
                }

            }

            if ($fragmentsAmount <= 0) {
                break;
            }

        }
        $o = ""; // reassembly words to keep
        for ($j = 0; $j < count($textParts); $j++) {
            if ($c[$j]) {
                $o .= " " . $textParts[$j];
            } else {
                $o .= ".";
            }
        }

        return preg_replace("/\.{3,}/iu", "...", $o);
    }

    /**
     * Импортирует классы Lucene и производит настройку дефолтной кодировки
     */
    public static function importLucene()
    {
        if (!class_exists('Zend_Search_Lucene', false)) {
            Yii::import('cms.vendors.*');
            require_once 'Zend/Search/Lucene.php';
            setlocale(LC_ALL, 'ru_RU.UTF-8'); // TODO i18N support
            Zend_Search_Lucene_Search_QueryParser::setDefaultEncoding('utf-8');
            $analyzer = new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8_CaseInsensitive();

            //инициализируем фильтр стоп-слов
            // $stopWordsFilter = new Zend_Search_Lucene_Analysis_TokenFilter_StopWords();
            // $stopWordsFilter->loadFromFile(Yii::getPathOfAlias('cms.models.search').DIRECTORY_SEPARATOR.'stop_words.ru_ru.dat');
            // $analyzer->addFilter($stopWordsFilter);

            //инициализируем морфологический фильтр
            Yii::import('cms.models.search.MorphFilter');
            $analyzer->addFilter(new MorphFilter('ru_RU'));
            $analyzer->addFilter(new MorphFilter('uk_UA'));
            $analyzer->addFilter(new MorphFilter('en_EN'));

            Zend_Search_Lucene_Analysis_Analyzer::setDefault($analyzer);
        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('index_id', $this->index_id, true);
        $criteria->compare('modified_date', $this->modified_date, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('last_object_date', $this->last_object_date, true);
        $criteria->compare('objectTypes', $this->objectTypes, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param string $indexId id индекса
     * @param boolean $createNew если true и возвращаемой директории не сущесвтует - будет создана новая директория под индекс, иначе будет выброшено исключение.
     * @throws CException
     *
     * @return Путь к индексам Lucene
     */
    public static function getPathOfIndex($indexId = false, $createNew = false)
    {
        // TODO: нужно какой-то логичный Yii алиас, что бы достучаться до рантайма бекенда/фронтенда можно было также и из сателлитки
        // $path = str_replace('backend', 'frontend', Yii::getPathOfAlias('application.runtime.search'));
        $path = FRONT_END . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'search';

        if ($indexId) {
            $path .= DIRECTORY_SEPARATOR . $indexId;

            if (!is_dir($path)) {
                if ($createNew) {
                    mkdir($path, 0777, true);
                } else {
                    throw new CException(_t("Index `{index}` does not exist!", 'cms', array(
                        '{index}' => $indexId,
                    )));
                }

            }
        }

        return $path;
    }

    public function getPrettyCreatedDate()
    {
        return self::getPrettyDate($this->created_date);
    }

    public function getPrettyModifiedDate()
    {
        return self::getPrettyDate($this->modified_date);
    }

    public function getPrettyLastObjectDate()
    {
        return self::getPrettyDate($this->last_object_date);
    }

    public static function getPrettyDate($date)
    {
        return $date != 0 ? Yii::app()->dateFormatter->formatDateTime($date, 'medium', 'short') : '';
    }

    /**
     * Возвращает количество документов в индексе
     */
    public function getDocumentsCount()
    {
        if (empty($this->index_id)) {
            return '';
        }

        if (($index = self::getIndex($this->index_id))) {
            return $index->count();
        } else {
            return -1;
        }
    }

    public static function getIndex($index)
    {
        self::importLucene();

        if (is_string($index)) {
            $path = self::getPathOfIndex($index);

            try {
                $index = Zend_Search_Lucene::open($path);
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', "Error opening index $index: {$e->getMessage()}");
                return null;
            }
        }

        return $index;
    }

    public static function getIndexesList()
    {
        $path = self::getPathOfIndex();
        $dirs = array();
        if (is_dir($path)) {
            $dirs = array_diff(scandir($path), array('.', '..'));
            foreach ($dirs as &$dir) {
                if (!is_dir($dir)) {
                    unset($dir);
                }

            }
            $dirs = array_combine($dirs, $dirs);
        }

        return $dirs;
    }

    public static function delTree($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    public static function getObjectTypesList()
    {

        $types = GxcHelpers::getAvailableContentType();
        $types = CMap::mergeArray(
            array(self::ALL_TYPES_ID => _t('All Available')),
            CHtml::listData($types, 'id', 'name')
        );

        return $types;
    }
}
