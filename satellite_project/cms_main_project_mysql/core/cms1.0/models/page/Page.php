<?php
/**
 * This is the model class for table "{{page}}".
 *
 * The followings are the available columns in table '{{page}}':
 * @property string $page_id
 * @property string $name
 * @property string $title
 * @property string $breadcrumb_title
 * @property string $description
 * @property string $parent
 * @property string $layout
 * @property string $display_type
 * @property string $slug
 * @property integer $lang
 */
class Page extends CmsActiveRecord
{
    /**
     * id для кэша параметров блоков.
     * NOTE: к этому id будет еще добавлять id страницы
     */
    const CACHE_ID_BLOCKS = 'Page::getBlocks()->';

    /**
     * Returns the static model of the specified AR class.
     * @return Page the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{page}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, title, description, parent, layout, slug, lang', 'required'),
            array('lang, status, allow_index, allow_follow', 'numerical', 'integerOnly' => true),
            array('name, title, breadcrumb_title, description, layout, slug', 'length', 'max' => 255),
            array('parent', 'length', 'max' => 20),
            array('guid', 'safe'),
            array('display_type', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('page_id, name, title, breadcrumb_title, description, parent, layout, slug, lang', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'language' => array(self::BELONGS_TO, 'Language', 'lang'),
        );
    }

    /**
     * Возвращает массив с регионами их их блоками,
     * остортироваными по порядку:
     * array(
     *     regionId => array( // блоки в регионе
     *         orderId => array(
     *             'attributes' => (object)Block::attributes, // DEPRECATED
     *             'params' => (object)Block::attributes, // параметры блока
     *             'class' => string, // алиас класса виджета
     *         )
     *     )
     * )
     *
     * @return array массив с блоками регионов
     */
    public function getBlocks()
    {
        // CACHE_ID_BLOCKS
        $cacheId = self::CACHE_ID_BLOCKS . $this->primaryKey;

        if (($blocksByRegion = Yii::app()->cache->get($cacheId)) === false) {
            // используем DAO, что бы секономить на создании обьектов моделей
            // для много уровневых зависимостей
            $blocks = Yii::app()->db->createCommand()
                                ->select('*')
                                ->from('{{block}} block')
                                ->join('{{page_block}} page_block', 'page_block.block_id = block.block_id')
                                ->where('page_block.page_id = :pageId AND page_block.status = :blockStatus', array(
                                    ':pageId' => $this->primaryKey,
                                    ':blockStatus' => PageBlock::PAGE_BLOCK_ACTIVE,
                                ))
                                ->queryAll();

            $blockFields = new Block();
            $blockFields = $blockFields->attributes;

            $blocksInfo = GxcHelpers::getAvailableBlocks();

            $blocksByRegion = array();
            foreach ($blocks as $block) {
                $blocksByRegion[$block['region']][$block['block_order']]['params'] = unserialize($block['params']);

                $blockType = $block['type'];
                $classAlias = GxcHelpers::getTruePath('front_blocks.' . $blockType . '.' . $blocksInfo[$blockType]['class']);
                $blocksByRegion[$block['region']][$block['block_order']]['class'] = $classAlias;
            }

            // сортируем все массивы согласно их block_order
            foreach ($blocksByRegion as &$blocksInRegion) {
                ksort($blocksInRegion);
            }

            // кешируем навсегда: модель сама позаботится об очистке кеша при ее сохранении
            Yii::app()->cache->set($cacheId, $blocksByRegion, 0);
        }

        return $blocksByRegion;
    }

    /**
     * Очищает все кэши, связанные с этой моделью
     * @see  Page::getBlocks()
     */
    public function clearCache()
    {
        // TODO: нужена возможность чистить кеш фронтенда, находясь в бекенде, сейчас решаем это дело костылем
        $oldPath = Yii::app()->cache->cachePath;
        Yii::app()->cache->cachePath = str_replace('backend', 'frontend', $oldPath);
        Yii::app()->cache->keyPrefix = 'frontend';

        Yii::app()->cache->delete(self::CACHE_ID_BLOCKS . $this->primaryKey);
        Yii::app()->cache->cachePath = $oldPath;
        Yii::app()->cache->keyPrefix = 'backend';
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'page_id' => _t('Page'),
            'name' => _t('Name'),
            'title' => _t('Title'),
            'breadcrumb_title' => _t('Breadcrumb Title'),
            'description' => _t('Description'),
            'parent' => _t('Parent'),
            'layout' => _t('Layout'),
            'slug' => _t('Slug'),
            'lang' => _t('Language'),
            'guid' => _t('Guid'),
            'status' => _t('Status'),
            'allow_index' => _t('Allow index page'),
            'allow_follow' => _t('Allow follow page'),
            'display_type' => _t('Display type'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('page_id', $this->page_id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('breadcrumb_title', $this->breadcrumb_title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('parent', $this->parent, true);
        $criteria->compare('layout', $this->layout, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('lang', $this->lang);

        $sort = new CSort;
        $sort->attributes = array(
            'page_id',
        );
        $sort->defaultOrder = 'page_id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
        ));
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                if ($this->guid == '') {
                    $this->guid = uniqid();
                }
            }
            return true;
        } else {
            return false;
        }
    }

    protected function afterDelete()
    {
        PageBlock::model()->deleteAll('page_id = :id', array(':id' => $this->page_id));
    }

    protected function afterSave()
    {
        parent::afterSave();
        $this->clearCache();
    }

    public static function getParentPages($render = true, $page_id = null)
    {
        if ($page_id !== null) {
            $pages = Page::model()->findAll('status = :status and page_id <> :pid', array(':status' => ConstantDefine::PAGE_ACTIVE, ':pid' => $page_id));
        } else {
            $pages = Page::model()->findAll('status = :status', array(':status' => ConstantDefine::PAGE_ACTIVE));
        }

        $data = array(0 => _t("None"));

        if ($pages && count($pages) > 0) {
            $data = CMap::mergeArray($data, CHtml::listData($pages, 'page_id', 'name'));
        }

        if ($render) {
            foreach ($data as $value => $name) {
                echo CHtml::tag('option', array(
                    'value' => $value,
                ), CHtml::encode($name), true);
            }
        } else {
            return $data;
        }
    }

    public static function changeLayout()
    {
        $layout = isset($_POST['layout']) ? $_POST['layout'] : 'base';

        $result = array();
        $result = self::getLayoutsAndRegions($layout);

        header('Content-Type: application/json');
        echo CJSON::encode($result);
        Yii::app()->end();
    }

    public static function getLayoutsAndRegions($layout)
    {
        $result = array();
        $result['layout'] = $layout;

        $available_layouts = GxcHelpers::getAvailableLayouts(false);
        $currentLayout = $available_layouts[$layout];
        $result['regions'] = $currentLayout['regions'];
        $result['types'] = $currentLayout['types'];

        return $result;
    }

    public static function changeParent()
    {
        $layout = 'base';
        $parent = isset($_POST['parent']) ? $_POST['parent'] : 0;

        if ($parent) {
            $page = Page::model()->findByPk($parent);
            if ($page) {
                $layout = $layout = $page->layout;
                $result = self::getLayoutsAndRegions($layout);

                $page_blocks = PageBlock::model()->with('block')->findAll(array(
                    'condition' => 'page_id = :pid',
                    'params' => array(':pid' => $parent),
                    'order' => 'region ASC, block_order ASC',
                ));

                $result['blocks'] = self::listBlocks($page_blocks);
                $result['parentType'] = $page->display_type;
            }
        }

        header('Content-Type: application/json');
        echo CJSON::encode($result);
        Yii::app()->end();

    }

    protected static function listBlocks($blocks)
    {
        $result = array();
        foreach ($blocks as $pb) {
            $result[] = array(
                'region' => $pb->region,
                'id' => $pb->block_id,
                'type' => $pb->block->type,
                'status' => $pb->status,
                'title' => $pb->block->name,
            );
        }

        return $result;
    }

    public static function inheritParent()
    {
        $parent = isset($_POST['parent']) ? (int) $_POST['parent'] : 0;
        $region = isset($_POST['region']) ? (int) $_POST['region'] : null;
        $layout = isset($_POST['layout']) ? (int) $_POST['layout'] : '';

        $result = array();
        $result['blocks'] = array();
        if ($parent && $region !== null) {
            //We now find all blocks of this parent
            $page_blocks = PageBlock::model()->with('block')->findAll(array(
                'condition' => 'page_id = :pid and region = :rid',
                'params' => array(':pid' => $parent, ':rid' => $region),
                'order' => 'region ASC, block_order ASC',
            ));

            $result['blocks'] = self::listBlocks($page_blocks);
        }

        header('Content-Type: application/json');
        echo CJSON::encode($result);
        Yii::app()->end();
    }

    public static function suggestPage()
    {
        if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
            $limit = 10;
            $pages = Page::model()->findAll(array(
                'condition' => 'name LIKE :keyword',
                'limit' => $limit,
                'params' => array(
                    ':keyword' => '%' . strtr($keyword, array('%' => '\%', '_' => '\_', '\\' => '\\\\')) . '%',
                ),
            ));
            $names = array();
            foreach ($pages as $page) {
                $names[] = $page->name . '|' . $page->page_id;

            }

            if ($names !== array()) {
                echo implode("\n", $names);
            }
        }
        Yii::app()->end();
    }

    public function getViewUrl()
    {
        return Yii::app()->createUrl('', array(
            'slug' => $this->slug,
        ));
    }

    public static function getPageName($id)
    {
        if ($id) {
            $page = Page::model()->findByPk($id);
            if ($page) {
                return CHtml::encode($page->name);
            }
        }

        return '';
    }
}
