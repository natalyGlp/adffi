<?php
class Galleries extends CmsActiveRecord
{
    public $pkey = 'id';
    public $image_count;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{galleries}}';
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->user_id = user()->id;
        }
        return parent::beforeSave();
    }

    public static function suggestGalleryies()
    {
        if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
            $limit = 10;
            $objects = self::model()->findAll(array(
                'select' => array('id', 'title'),
                'condition' => 'title LIKE :keyword',
                'limit' => $limit,
                'params' => array(
                    ':keyword' => '%' . strtr($keyword, array('%' => '\%', '_' => '\_', '\\' => '\\\\')) . '%',
                ),
            ));

            $names = array();
            foreach ($objects as $object) {
                $names[] = $object->title . '|' . $object->id;
            }

            if ($names !== array()) {
                echo implode("\n", $names);
            }

        }

        Yii::app()->end();
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('path', 'required'),
            array('path', 'unique'),
            array('image_count', 'safe', 'on' => 'search'),
            array('path', 'match', 'pattern' => '/[a-z0-9]/'),
        );
    }

    public function relations()
    {
        return array(
            'pictures' => array(self::HAS_MANY, 'GalleryPhotos', 'gallery_id'),
            'cover' => array(self::HAS_ONE, 'GalleryPhotos', 'gallery_id'),
        );
    }

    public function attributeLabels()
    {
        return CMap::mergeArray(parent::attributeLabels(), array(
            'path' => _t('Path'),
            'title' => _t('Title'),
            'resize_options_json' => _t('Sizes'),
        ));
    }
    public function getSizes($version, $type = 'sizes')
    {
        $size = array();
        $cur_type = '_thumb';

        $resize_options_json = json_decode($this->resize_options_json, true);

        foreach ($resize_options_json as $key => $sizes) {
            if ($sizes['max_width'] >= $version) {
                $size = $sizes;
                $cur_type = $key;
                break;

            }
        }
        if ($type == "sizes") {
            if (count($size == 0)) {
                //var_dump(end($resize_options_json));
                $size = end($resize_options_json);
            }
            return $size;} else {
            if (count($size == 0)) {
                $temp = array_keys($resize_options_json);
                $cur_type = $temp[count($temp) - 1];
            }
            return $cur_type;
        }

    }
    public function search()
    {
        $criteria = new CDbCriteria;
        $image_table = GalleryPhotos::model()->tableName();
        $image_count_sql = "(select count(*) from $image_table where $image_table.gallery_id = t.id)";
        $criteria->select = array('*', $image_count_sql . " as image_count");
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.path', $this->path, true);
        $criteria->compare('t.active', '=' . $this->active, true);
        $criteria->compare('t.pictures', $this->pictures, true);
        $criteria->compare('$image_count_sql', $this->image_count);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.id',
                'attributes' => array(
                    'image_count' => array(
                        'asc' => 'image_count ASC',
                        'desc' => 'image_count DESC',
                    ),
                    '*',
                ),
            ),
        ));
    }
}
