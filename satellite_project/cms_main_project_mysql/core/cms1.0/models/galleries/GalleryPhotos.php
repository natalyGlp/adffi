<?php
class GalleryPhotos extends CmsActiveRecord{
	public $title;
	public $pkey = 'id';

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return '{{photos}}';
	}
	
	public function getFile(){
		return $this->img.'.'.$this->ext;
	}
	
	public function getSize($path, $size){
		return Yii::app()->baseUrl.'/upload/'.$path.'/'.$this->img.$size.'.'.$this->ext;
	}
	
	public function getImage($size) 
	{
		return GALLERY_URL.'/'.$this->gallery_id.'/'.$this->img.$size.'.'.$this->ext;
	}
	
	public function hasImage($size) 
	{
		$path = Yii::getPathOfAlias('uploads.resources.galleries').'/'.$this->gallery_id.'/'.$this->img.$size.'.'.$this->ext;
		return (file_exists($path) && is_file($path));
	}

	public function setFile($file){
		$fl = explode('.',$file);
		$ext = end($fl);
		$img = str_replace('.'.$ext, '', $file);
		$this->img = $img;
		$this->ext = $ext;
	}

	public function rules(){
		return array(
			//array('title, description','required'),
			array('title, description, link','safe')
			);
	}
	public function relations(){
		return array(
			'gallery' => array(self::BELONGS_TO, 'Galleries', 'gallery_id')
		);
	}
	public function attributeLabels(){
		return CMap::mergeArray(parent::attributeLabels(), array());
	}
	public function search($condition = null, $params = null, $order = null, $only_active = false){
		$criteria = new CDbCriteria;
		$cls = get_class($this);
		if($condition){
			$criteria->condition = $condition;
		}        
		if($params){
			$criteria->params = $params;
		}       
		if ($order){
			$criteria->order = $order;
		}
		if(isset($_GET[$cls])){
			foreach($_GET[$cls] as $k => $v){
				$this->{$k} = $v;
			}
		}
		if($only_active){
			$this->active = 1;
		}
		
		$criteria->compare('t.img', $this->img, true);
		$criteria->compare('t.ext', $this->ext, true);
		$criteria->compare('t.gallery_id', $this->gallery_id, true);
		$criteria->compare('t.title', $this->title, true);
		$criteria->compare('t.alt', $this->alt, true);
		$criteria->compare('t.description', $this->description, true);
		$criteria->compare('t.date_add', $this->date_add, true);
		$criteria->compare('t.active', '='.$this->active, true);
		$page = isset($_GET[$cls.'_page']) ? $_GET[$cls.'_page'] : 1;
		return new CActiveDataProvider($cls, array(
			'criteria' => $criteria,
			'pagination'=>array(
				'pageSize' => Yii::app()->getModule('GalleryPhotos')->per_page,
				'currentPage' => $page-1,
			),
		));
	}
}
