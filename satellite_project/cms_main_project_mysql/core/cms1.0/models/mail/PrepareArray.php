<?php
/**
 * Created by JetBrains PhpStorm.
 * User: codexmen <codexmen@studiobanzai.com>
 */
class PrepareArray
{
    /**
     * @return array return  param array from DataBace Storage
     */
    static public function getParamArray()
    {
        $key_array=array();
        $value_array=array();
        $settings=Settings::model()->findAll();
      //  dump($settings);
        if (!empty($settings))
        {
            foreach ($settings as $key=>$value)
            {
            //    dump($set->id);
                $key_array[]='{'.$value->key.'}';
                $value_array[]=Yii::app()->settings->get($value->category,$value->key);
                //dump($set->category);
            }
        }
        $result=array();
        $result[]=$key_array;
        $result[]=$value_array;
        return $result;
    }
}