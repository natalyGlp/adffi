<?php

/**
 * Class for render a calendar with all objects
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package common.front_blocks.content_calendar
 */
class ContentCalendarBlock extends BlockWidget 
{
    /**
     * @var array $contentTypes контент типы, которые следует выводить в календаре
     */
    public $contentTypes = array();

    /**
     * @var array $langs языки, которые следует выводить в календаре
     */
    public $langs;

    /**
     * @var array $terms термы таксономии, которые следует выводить в календаре
     */
    public $terms;
    
    public function run()
    {
        if(Yii::app()->request->isAjaxRequest && isset($_POST['widget']) && $_POST['widget'] == 'calendar') {
            while(@ob_end_clean());

            $month = $_POST['month'];
            $year = $_POST['year'];
            $delta = $_POST['delta'] > 0 ? '+' : '-';

            $time = mktime(0,0,0,$month,1,$year);
            $date = new DateTime();
            $date->setTimestamp($time);
            $date->modify($delta . '1 month');

            $this->getCalendarPage($date->format('m'), $date->format('Y'));
            Yii::app()->end();
        } else {
            $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias(GxcHelpers::getTruePath('front_blocks.content_calendar.assets')), false, -1, YII_DEBUG);
            Yii::app()->clientScript
                ->registerCoreScript('jquery')
                ->registerScriptFile($assetsUrl.'/js/jquery.calendar.js')
                ;
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $time = time();
        $month = date('m', $time);
        $year = date('Y', $time);

        $this->getCalendarPage($month,$year);
    }

    public function getCalendarPage($month, $year, $size=42)
    {
        $time = mktime(0,0,0,$month,1,$year);
        $days = $this->getCalendarData($month, $year);

        $this->render('output', array(
            'days' => $days,
            'monthNumber' => date('m', $time),
            'yearNumber' => date('Y', $time),
            'monthName' => Yii::app()->dateFormatter->format('LLLL', $time),
            'yearName' => Yii::app()->dateFormatter->format('yyyy', $time),
            ));
    }

    /**
     *
     * @source symbol.ua
     */
    public function getCalendarData($month, $year, $size=42)
    {
        // TODO: тесты на каждый месяц
        $calendarPage = array();
        $time = mktime(0,0,0,$month,1,$year);

        $totalDays = 7*5;
        $daysInMonth = date('t',$time);
        $dayOfWeek = date('N',$time); // 1-7
        $daysBefore = $dayOfWeek == 1 ? 0 : $dayOfWeek - 1;
        $daysAfter = $totalDays - $daysInMonth - $daysBefore;

        // вычисляем начало и конец дат для календаря захватывая предыдущий и след. месяцы так, что бы дни занимали 35 клеток
        $date = new DateTime();
        $date->setTimestamp($time);
        if($daysBefore)
            $date->modify('-'.$daysBefore.' day');
        $beginTime = $date->getTimestamp();
        $date->modify('+'.($totalDays-1).' day');
        $endTime = $date->getTimestamp();

        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('object_date', $beginTime, $endTime);
        $criteria->order = 'object_date ASC';

        if(!empty($this->contentTypes) && !in_array('all', $this->contentTypes)) {
            $criteria->addInCondition('object_type', $this->contentTypes);
        }

        if(!empty($this->langs) && !in_array('0', $this->langs)) {
            $criteria->addInCondition('lang', $this->langs);
        }

        if(!empty($this->terms) && !in_array('0', $this->terms))
        {
            $tempParamNames = array();
            foreach($this->terms as $pi => $term) {
                $tempParamNames[] = ':term'.$pi;
                $criteria->params[end($tempParamNames)] = $term;
            }
            $criteria->addCondition('t.object_id IN (SELECT object_id FROM {{object_term}} WHERE term_id IN (' . implode(', ', $tempParamNames) . '))');
        }

        GxcHelpers::applyObjectStatusCriteria($criteria);

        $objects = Object::model()->findAll($criteria);

        $days = array();
        $date->setTimestamp($beginTime);
        for ($i=0; $i < $totalDays; $i++) { 
            $dateObjects = array();
            if(current($objects)) {
                $curObject = current($objects);
                while($curObject && date('Y-m-d', $curObject->object_date) == $date->format('Y-m-d')) {
                    array_push($dateObjects, current($objects));
                    $curObject = next($objects);
                }
            }

            $isToday = $date->format('Y-m-d') == date('Y-m-d', time());
            $isCurrentMonth = $date->format('m') == $month;
            $hasObjects = count($dateObjects) > 0;
            $day = $date->format('d');
            $isPast = ($date->format('m') . $date->format('d')) < (date('m') . date('d'));
            $cssClass = 'calendar-day';
            if($isToday)
                $cssClass .= ' day-today';
            if($hasObjects)
                $cssClass .= ' day-has-data';
            if($isPast)
                $cssClass .= ' day-past';
            if(!$isCurrentMonth)
                $cssClass .= $day < 7 ? ' day-prev-month' : ' day-next-month';

            array_push($days, array(
                'dayOfWeek' => $i%7+1,
                'day' => $day,
                'cssClass' => $cssClass,
                'isToday' => $isToday,
                'isPast' => $isPast,
                'hasObjects' => $hasObjects,
                'isCurrentMonth' => $isCurrentMonth,
                'objects' => $dateObjects,
                ));
            $date->modify('+1 day');
        }

        return $days;
    }

    public function params()
    {
        return array(
            'contentTypes' => t('Content Types'),
            'langs' => t('Langs'),
            'terms' => t('Terms'),
        );
    }
}

?>