<?php
/**
 * @var array $days массив с данными и обьектами календаря
 * @var integer $monthNumber цифровое представление месяца
 * @var integer $yearNumber цифровое представление года
 * @var string $monthName строковое представление месяца
 * @var string $yearName строковое представление года
 */

$id = 'calendar-'.$this->getId();

// TODO: i18N дней недели
?>
<section class="calendar <?php echo $this->cssClass ?>" id="<?php echo $id ?>" data-month="<?php echo $monthNumber ?>" data-year="<?php echo $yearNumber ?>">
  <header class="calendar-header">
    <a class="calendar-month-prev" href="#" data-action="prev-month">«</a>
    <span class="calendar-month"><?php echo $monthName; ?></span>
    <span class="calendar-year"><?php echo $yearName; ?></span>
    <a class="calendar-month-next" href="#" data-action="next-month">»</a>
  </header>
  <table cellpadding="0" cellspacing="0" class="calendar-table">
    <tr class="calendar-weekdays">
      <th>ПН</th>
      <th>ВТ</th>
      <th>СР</th>
      <th>ЧТ</th>
      <th>ПТ</th>
      <th class="calendar-weekend">СБ</th>
      <th class="calendar-weekend">ВС</th>
    </tr>
    <tr>
      <?php
      foreach ($days as $day) 
      {
        ?>
        <td class="<?php echo $day['cssClass']; ?>"><?php $this->render(GxcHelpers::getTruePath('front_blocks.content_calendar._day'), $day); ?></td>
        <?php
        if($day['dayOfWeek'] == 7)
          echo '</tr><tr>';
      }
      ?>
    </tr>
  </table>
</section>
