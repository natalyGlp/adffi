<?php
/**
 * @var integer $dayOfWeek 1-7
 * @var integer $day
 * @var string $cssClass
 * @var boolean $isToday
 * @var boolean $isPast
 * @var boolean $hasObjects
 * @var boolean $isCurrentMonth
 * @var array of CActiveRecord $objects
 * )
 */
?>

<div class="calendar-day-number"><?php echo $day; ?></div>
<?php if($hasObjects): ?>
    <div class="arrow"></div>
    <div class="calendar-day-entries">
        <?php
        foreach ($objects as $object) 
        {
            ?>
            <div class="calendar-entry">
                <?php
                $thumb=GxcHelpers::getResourceObjectFromDatabase($object, 'thumbnail', 'calendar_');
                if(count($thumb))
                {
                    echo CHtml::image($thumb[0]['link'], $object->object_title);
                }
                ?>
                <?php echo CHtml::link($object->object_name, Object::getLink($object->object_id)) ?>
            </div>
            <?php
        }
        ?>
    </div>
<?php endif; ?>