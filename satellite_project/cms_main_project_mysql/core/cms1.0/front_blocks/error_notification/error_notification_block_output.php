<?php
/**
 * @var array $code
 * @var array $message
 */
?>

<div class="<?php echo $this->cssClass ?>">
    <h1 class="error-code"><?php echo $code; ?></h1>
    <p class="error-message"><?php echo $message; ?></p>
    <nav class="error-nav">
        <a href="/" class="error-back" onclick="history.back(); return false;"><?php echo t('Back'); ?></a> | 
        <a href="/"><?php echo t('Home page'); ?></a>
    </nav>
</div>