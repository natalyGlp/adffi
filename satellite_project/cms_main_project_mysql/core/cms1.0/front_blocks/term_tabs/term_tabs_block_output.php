<?php
/**
 * @var string $cssClass
 * @var string $title
 * @var string $titleLink
 * @var array $tabs
 */
?>
<div class="<?php echo $cssClass ?>">
<?php
if (!empty($title)) {
    echo '<div class="title"><h2>';
    if (!empty($titleLink)) {
        echo CHtml::link(t($title), $titleLink);
    } else {
        echo t($title);
    }
    echo '</h2></div>';
}

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs' => $tabs,
));
?>
</div>