<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'terms'), ''); ?>
        <?php echo CHtml::listBox("Block[terms]", $block_model->terms, ContentList::getTerms(), array(
            'id' => 'Block-terms',
            'multiple'=>'multiple',
            'style'=>'width:100%; height:170px',
            'class'=>'listbox'
            )); ?>
        <?php echo $form->error($model, 'terms'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title'), ''); ?>
        <?php echo CHtml::textField("Block[title]", $block_model->title); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'titleLink'), ''); ?>
        <?php echo CHtml::textField("Block[titleLink]", $block_model->titleLink); ?>
        <?php echo $form->error($model, 'titleLink'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::checkBox("Block[useSliderkit]", $block_model->useSliderkit); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'useSliderkit'), ''); ?>
        <?php echo $form->error($model, 'useSliderkit'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'visibleSlidesAmount'), ''); ?>
        <?php echo CHtml::numberField("Block[visibleSlidesAmount]", $block_model->visibleSlidesAmount); ?>
        <?php echo $form->error($model, 'visibleSlidesAmount'); ?>
    </div>
</div>
