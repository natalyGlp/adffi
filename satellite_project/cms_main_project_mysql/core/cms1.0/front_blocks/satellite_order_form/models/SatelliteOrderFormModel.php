<?php
/**
 * Модель для формы заказа сателлитной сети
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.satellite_order_form
 */
Yii::import('cms.front_blocks.satellite_order_form.models.SatelliteOrderFormBaseModel');
class SatelliteOrderFormModel extends SatelliteOrderFormBaseModel 
{
}
