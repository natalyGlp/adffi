<?php

?>
<div class="<?php echo $cssClass ?>">
    <div class="title">
        <?php
        if (!empty($title)) {
            echo CHtml::tag('h2', array(), $title);
        }
        ?>
    </div>

    <div class="<?php echo $cssClass ?>-tags">
        <?php
        foreach ($tags as $tag) {
            echo CHtml::link($tag['name'], $tag['url'], array(
                'class' => $cssClass . '-tag weight-'.$tag['frequency'],
                ));
        }
        ?>
    </div>
</div>