<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title'), ''); ?>
        <?php echo CHtml::textField("Block[title]", $block_model->title); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>
    
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'maxTags'), ''); ?>
        <?php echo CHtml::numberField("Block[maxTags]", $block_model->maxTags); ?>
        <?php echo $form->error($model, 'maxTags'); ?>
    </div>
    
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'frequencyGroups'), ''); ?>
        <?php echo CHtml::numberField("Block[frequencyGroups]", $block_model->frequencyGroups); ?>
        <?php echo $form->error($model, 'frequencyGroups'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'searchPageId'), ''); ?>
        <?php $this->widget('cms.widgets.page.PageAutoComplete', array(
            'name' => "Block[searchPageId]",
            'value' => $block_model->searchPageId,
        )); ?>
        <?php echo $form->error($model, 'searchPageId'); ?>
    </div> 
</div>
