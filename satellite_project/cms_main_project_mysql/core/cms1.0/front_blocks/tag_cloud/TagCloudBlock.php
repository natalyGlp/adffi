<?php
/**
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.tag_cloud
 */

class TagCloudBlock extends BlockWidget
{
    protected $links = array();
    public $title;
    public $maxTags = 15;
    public $frequencyGroups = 4;
    public $searchPageId;

    public function run()
    {
        $cacheId = __FILE__;
        $minute = 60;
        if ($this->beginCache($cacheId, array('duration' => 20 * $minute))) {
            $tags = Tag::model()->findTagWeights($this->maxTags, $this->frequencyGroups);

            if (count($tags) > 0) {
                $this->shuffleAssoc($tags);

                foreach ($tags as &$tag) {
                    $tag['url'] = $this->getTagUrl($tag);
                }

                $this->render('output', array(
                    'title' => $this->title,
                    'cssClass' => $this->cssClass,
                    'tags' => $tags,
                ));
            }

            $this->endCache();
        }
    }

    public function getTagUrl($tag)
    {
        $url = '#';

        if ($page = $this->getSearchPage()) {
            $url = $page->viewUrl . '?tag='.$tag['slug'];
        }

        return $url;
    }

    protected function getSearchPage()
    {
        static $page = null;

        if (empty($this->searchPageId)) {
            return null;
        }

        if (!$page) {
            $page = Page::model()->findByPk($this->searchPageId);
        }

        return $page;
    }

    public function params()
    {
        return array(
            'title' => t('Block title'),
            'maxTags' => t('Maximum tags amount to display'),
            'frequencyGroups' => t('Tag frequency groups'),
            'searchPageId' => t('Search page name'),
        );
    }

    protected function shuffleAssoc(&$array)
    {
        if (count($array) === 0) {
            return true;
        }

        $keys = array_keys($array);

        shuffle($keys);

        foreach ($keys as $key) {
            $new[$key] = $array[$key];
        }

        $array = $new;

        return true;
    }
}
