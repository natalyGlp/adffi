<?php
$sort_type = false;
$sort_order = false;
    if(isset($_GET['sort_type'])&&isset($_GET['sort_order'])){
        $sort_type = $_GET['sort_type'];
        $sort_order = $_GET['sort_order'];
    }
?>
<div<?php echo($this->cssClass)?' class="'.$this->cssClass.'"':''; ?>>
    <form action="<?php echo $this->path?>">
        <div class="row">
            <?php echo CHtml::label('Сортировка', 'sort_orders')?>
            <?php echo CHtml::dropDownList('sort_order', ($sort_order)?:$this->sort_orders[$this->default_order_value], $this->sort_orders)?>
            <?php echo CHtml::dropDownList('sort_type', ($sort_type)?:$this->sort_types[$this->default_type_value], $this->sort_types)?>
        </div>
        <button type="submit" class="<?php echo $this->cssClass?>Button"><?php echo $this->buttonText?></button>
    </form>
</div>