<div name="div-block-content-<?php echo $block_model->id;?>">
    <div class="row">
        <?php
        $this->widget('cms.widgets.page.RegionsFormWidget', array(
                'model' => $model,
                'attribute' => 'blocksByLanguage',
                'regionsBlocks' => $block_model->blocksByLanguage,
                'regions' => $block_model->languages,
            ));
        ?>
    </div> 
</div> 
