<?php
/**
 * Class for rendering the list of blocks depending of the current language
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 0.1
 * @package common.front_blocks.blocks_i18n
 */

class BlocksI18nBlock extends BlockWidget
{
    public $blocksByLanguage = array();

    public function run()
    {
        if (empty($this->blocksByLanguage)) {
            return;
        }

        $blocks = $this->fetchBlocksWithParams();

        $this->renderBlocks($blocks);
    }

    protected function fetchBlocksWithParams()
    {
        if (!Yii::app()->user->isGuest || ($blocksWithParams = Yii::app()->cache->get($this->cacheId)) === false) {
            $blocks = $this->fetchBlocks();

            $blocksWithParams = array();
            foreach ($blocks as $block) {
                array_push($blocksWithParams, array(
                    'class' => $this->getBlockClass($block),
                    'params' => unserialize($block->params),
                    ));
            }

            Yii::app()->cache->set($this->cacheId, $blocksWithParams);
        }

        return $blocksWithParams;
    }

    protected function fetchBlocks()
    {
        $i18NBlocks = $this->currentLanguageBlocks;

        // TODO: этот класс в идеале не должен знать о структуре $i18NBlocks
        $blocks = array();
        foreach ($i18NBlocks['id'] as $index => $blockId) {
            if ($i18NBlocks['status'][$index] != 1) {
                continue;
            }

            array_push($blocks, $blockId);
        }

        return Block::model()->findAllByPk($blocks);
    }

    protected function getCurrentLanguageBlocks()
    {
        return $this->blocksByLanguage[Yii::app()->translate->languageId];
    }

    protected function getBlockClass($block)
    {
        $blocksInfo = GxcHelpers::getAvailableBlocks();
        $blockType = $block->type;
        $classAlias = GxcHelpers::getTruePath('front_blocks.'.$blockType.'.'.$blocksInfo[$blockType]['class']);

        return $classAlias;
    }

    protected function renderBlocks($blocks)
    {
        $renderer = $this->createWidget('BlockRenderWidget');

        foreach ($blocks as $block) {
            $renderer->renderBlock($block['class'], $block['params']);
        }
    }

    public function getLanguages()
    {
        $languages = array();

        foreach (Yii::app()->translate->acceptedLanguages as $code => $name) {
            $id = Yii::app()->translate->getLanguageId($code);

            $languages[$id] = $name;
        }

        return $languages;
    }

    public function afterBlockSave()
    {
        foreach ($this->blocksByLanguage as $id => $params) {
            Yii::app()->cache->delete($this->getCacheId($params));
        }

        return parent::afterBlockSave();
    }

    protected function getCacheId($params = false)
    {
        $params = $params ? $params : $this->currentLanguageBlocks;
        return __CLASS__.'#'.serialize($params);
    }

    public function params()
    {
        return array(
            'blocksByLanguage' => t('Choose Blocks'),
        );
    }
}
