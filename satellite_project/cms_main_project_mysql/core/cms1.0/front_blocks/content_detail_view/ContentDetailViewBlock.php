<?php

/**
 * Class for render Content Detail View
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.content_detail_view
 */

class ContentDetailViewBlock extends BlockWidget
{
    public $imageSize = '';
    public $listViewPageId = '';

    public function run()
    {
        $post = self::getObject();

        if (!$post) {
            Yii::app()->controller->pageNotFound();
        }

        Yii::app()->controller->pageTitle = CHtml::encode($post->object_title);
        Yii::app()->controller->description = CHtml::encode($post->object_description);
        if (empty(Yii::app()->controller->description)) {
            Yii::app()->controller->description = $post->excerpt;
        }

        $this->updateBreadCrumbs($post);

        $viewAlias = GxcHelpers::getTruePath('content_type.' . $post->object_type . '.item_render');
        $this->render($viewAlias, array(
            'post' => $post,
        ));
    }

    /**
     * Возвращает модель обьекта на основе параметров url
     * @return Object|null модель обьекта или null
     */
    public static function getObject()
    {
        $object = null;
        $slug = Yii::app()->controller->slug;
        $objectSlug = Yii::app()->controller->objectSlug;
        $show_title = $slug == 'home' ? false : true;

        if ($slug == 'home') {
            throw new CException('Unable to get object type of the Homepage');
        }

        if (empty($objectSlug)) {
            return null;
        }

        $modelName = Object::importByType($slug, null);
        if (!$modelName) {
            // FALLBACK:
            // пробуем искать исходя из object_slug. Используется в тех случаях,
            // когда создаются отдельные Page для каждого языка. В таком случае
            // не выходит создать две страницы с одинаковым slug и потому вместо
            // article приходится писать articleen

            $object = Object::model()->findByAttributes(array('object_slug' => $objectSlug));

            if ($object === null) {
                return null;
            }

            $modelName = Object::importByType($object->object_type, null);
            $slug = $object->object_type;
            $objectSlug = $object->object_slug;
        }

        $object_status = GxcHelpers::applyObjectStatusCriteria();

        $object = $modelName::model()->with('language')->findByAttributes(array(
            'object_slug' => $objectSlug,
            'object_type' => $slug,
            'object_status' => $object_status,
        ));

        if (!$object) {
            return null;
        }

        $curLang = Yii::app()->language;
        if ($object->language->code != $curLang) {
            $translatedObject = $modelName::model()->with('language')->findByAttributes(array(
                'object_status' => $object_status,
                'guid' => $object->guid,
            ), array(
                'condition' => 'language.lang_name = :code',
                'params' => array(
                    ':code' => $curLang,
                ),
            ));

            if ($translatedObject) {
                // редирект на правильный objectSlug
                Yii::app()->controller->redirect(Object::getLink($translatedObject->object_id));
            }
        }

        return $object;
    }

    protected function updateBreadCrumbs($post)
    {
        $breadcrumbs = array();
        if (!empty($this->listViewPageId)) {
            $listViewPage = Page::model()->findByPk($this->listViewPageId);
            $sectionTitle = !empty($listViewPage->breadcrumb_title) ? $listViewPage->breadcrumb_title : $listViewPage->title;
            $breadcrumbs[$sectionTitle] = '/' . $listViewPage->slug;
        }
        array_push($breadcrumbs, $post->object_title);

        Yii::app()->controller->breadcrumbs = $breadcrumbs;
    }

    public function params()
    {
        return array(
            'imageSize' => t('Image size'),
            'listViewPageId' => t('List View Page Name'),
        );
    }
}
