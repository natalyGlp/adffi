<?php
/**
 * @var array $phones доступные телефоны
 */
?>
<div class="<?php echo $this->cssClass; ?>">
    <?php
    foreach ($phones as $phone => $phoneHtml) {
        echo CHtml::tag('div', array(
            'class' => 'phone'
        ), $phoneHtml);
    }

    if (!empty($email)) {
        echo CHtml::tag('div', array(
            'class' => 'email'
        ), $email);
    }

    if (!empty($address)) {
        echo CHtml::tag('div', array(
            'class' => 'address'
        ), $address);
    }
    ?>
</div>