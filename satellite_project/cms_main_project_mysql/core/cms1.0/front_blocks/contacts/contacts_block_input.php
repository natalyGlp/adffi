<div name="div-block-content-<?php echo $block_model->id;?>">
    <div class="row">
        <?php
            echo $form->checkBox($block_model,
                "showPhones",
                array('id' => 'Block-showPhones')
            );
        ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'showPhones'), ''); ?>
        <?php echo $form->error($model, 'showPhones'); ?>
    </div>
    <div class="row">
        <?php
            echo $form->checkBox($block_model,
                "showEmail",
                array('id' => 'Block-showEmail')
            );
        ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'showEmail'), ''); ?>
        <?php echo $form->error($model, 'showEmail'); ?>
    </div>
    <div class="row">
        <?php
            echo $form->checkBox($block_model,
                "showAddress",
                array('id' => 'Block-showAddress')
            );
        ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'showAddress'), ''); ?>
        <?php echo $form->error($model, 'showAddress'); ?>
    </div>
</div>
