<?php

/**
 * Class for render Content based on Content list
 *
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.listview
 */
class ListViewBlock extends BlockWidget
{
    //Content list attribute
    public $viewPath;
    public $content_list = null;
    public $display_type = self::DISPLAY_TYPE_HOMEPAGE;
    public $title = '';
    public $title_link = '';
    public $allLink;
    public $allLinkText;
    public $allLinkPosition = array('before', 'after');
    public $imageSize = '';
    public $allowSearch = false;
    public $forbidNonSearchQueries = false;
    public $display_object_layout_menu = false;
    public $ajaxUpload = false;
    public $ajaxUploadLoadingText = 'Загрузка';
    public $ajaxUploadDoneText = 'Доступных записей больше нет';
    public $layouts = array(
        //ключ масива будет цсс класом елемента. shopProductList - класс по умолчанию
        'grid-layout' => 'Плитка',
        'list-layout' => 'Список',
    );

    /**
     * @var array $luceneIndex индексы Lucene, в которых нужно искать
     */
    public $luceneIndex = false;

    //Display types for the list view render

    const DISPLAY_TYPE_HOMEPAGE = 0;
    const DISPLAY_TYPE_CATEGORY = 1;
    const DISPLAY_TYPE_ASIDE = 2;
    const DISPLAY_TYPE_RSS = 3;
    const DISPLAY_TYPE_SEARCH = 4;

    public function run()
    {
        if ($this->forbidNonSearchQueries && !$this->isSearchRequest) {
            Yii::app()->controller->pageNotFound();
        }

        // исправление алиаса расширения yiinfinite-scroll
        Yii::setPathOfAlias('ext.yiinfinite-scroll.assets', Yii::getPathOfAlias('cms.extensions.yiinfinite-scroll.assets'));
        $this->renderContent();
    }

    protected function renderContent()
    {
        if (!empty($this->content_list)) {
            $this->render('output', array(
            ));
        }
    }

    public function validate()
    {
        if ($this->forbidNonSearchQueries) {
            $this->allowSearch = true;
        }

        if (empty($this->content_list)) {
            $this->errors['content_list'] = t('You should provide at least one content list');
            return false;
        }

        return true;
    }

    public function params()
    {
        return array(
            'viewPath' => t('View name'),
            'content_list' => t('Content list'),
            'display_type' => t('Display type'),
            'title' => t('Title'),
            'title_link' => t('Title Link'),
            'imageSize' => t('Image size'),
            'cssClass' => t('Css Class'),
            'allowSearch' => t('Allow searching'),
            'forbidNonSearchQueries' => t('Return 404 for non searching queries'),
            'luceneIndex' => t('Use the following indexes for searching'),
            'display_object_layout_menu' => t('Display layout menu'),
            'ajaxUpload' => t('Ajax items upload'),
            'ajaxUploadLoadingText' => ('Ajax upload loading text'),
            'ajaxUploadDoneText' => ('Ajax upload done text'),
            'allLink' => t('All entries link'),
            'allLinkText' => t('All entries link text'),
            'allLinkPosition' => t('All entries link position'),
        );
    }

    public static function getDisplayTypes()
    {
        return array(
            self::DISPLAY_TYPE_HOMEPAGE => t("Display in Homepage"),
            self::DISPLAY_TYPE_CATEGORY => t("Display in Category page"),
            self::DISPLAY_TYPE_ASIDE => t("Display as Aside"),
            self::DISPLAY_TYPE_RSS => t("Display as RSS feed"),
            self::DISPLAY_TYPE_SEARCH => t("Display as search result"),
        );
    }

    public function getItemCssClass($object)
    {
        return $this->createCssClass($object->object_type, 'listView', true);
    }

    public function getContentList($content_list_id, $max = null, $pagination = null, $return_type = ConstantDefine::CONTENT_LIST_RETURN_DATA_PROVIDER)
    {
        $params = array();
        if ($this->allowSearch) {
            $params['search'] = array(
                'query' => isset($_GET['q']) ? $_GET['q'] : false,
                'date' => isset($_GET['date']) ? $_GET['date'] : false,
                'tag' => isset($_GET['tag']) ? $_GET['tag'] : false,
                'term' => isset($_GET['term']) ? $_GET['term'] : false,
                'authorId' => isset($_GET['author_id']) ? $_GET['author_id'] : false,
                'luceneIndex' => $this->luceneIndex,
            );
        }
        return GxcHelpers::getContentList($content_list_id, null, null, ConstantDefine::CONTENT_LIST_RETURN_DATA_PROVIDER, null, $params);
    }

    protected function getIsSearchRequest()
    {
        return isset($_GET['q']) || isset($_GET['date']) || isset($_GET['term']) || isset($_GET['tag']) || isset($_GET['author_id']);
    }

    public function getViewsFiles()
    {
        $defaultPath = Yii::getPathOfAlias('cms.front_blocks.listview.views');
        $themePath = Yii::getPathOfAlias('theme.front_blocks.listview.views');

        $views = array();

        if (is_dir($defaultPath)) {
            $views = CMap::mergeArray($views, scandir($defaultPath));
        }

        if (is_dir($themePath)) {
            $views = CMap::mergeArray($views, scandir($themePath));
        }

        $views = array_filter($views, function($item) {
            return $item[0] != '.';
        });

        $views = array_map(function ($item) {
            return pathinfo($item, PATHINFO_FILENAME);
        }, $views);

        $views = array_unique($views);

        return array_combine($views, $views);
    }
}
