<?php
if ($this->display_type == ListViewBlock::DISPLAY_TYPE_RSS) {
    foreach ($this->content_list as $content_list) {
        $dataProvider = $this->getContentList($content_list);
        if (!$dataProvider) {
            continue;
        }

        // для RSS отображения нам нужно убрать скрипты и разметку от Yii, потому рендерим руками
        $data = $dataProvider->getData();
        if (count($data) === 0) {
            continue;
        }

        foreach ($data as $i => $item) {
            $data = array(
                'display_type' => $this->display_type,
                'index' => $i,
                'data' => $item,
                'widget' => $this,
            );
            $this->render($dataProvider->render_layout, $data);
        }
    }

    return;
}

$containerHtmlOptions = array(
    'class'=>$this->cssClass,
);

if ($this->display_object_layout_menu) {
    $initLayoutClass = array_keys($this->layouts);
    $initLayoutClass = reset($initLayoutClass);
    $containerHtmlOptions['class'] .= ' ' . $initLayoutClass;
    Yii::app()->clientScript->registerScript(__FILE__.'#'.$this->getId(), '
        $("body").on("change", ".object_layout", function(){
            var $container = $(this).parent().parent();
            var object_layout = $(this).val();
            $container.addClass(object_layout);
            if($(this).data("layout")) {
                $container.removeClass($(this).data("layout"))
            }
            $(this).data("layout", object_layout);
        });');
}

echo CHtml::openTag('div', $containerHtmlOptions)."\n";

if ($this->display_object_layout_menu) {
    ?>
    <div class="layout-menu">
        <?php echo CHtml::label('Расположение', 'layout')?>
        <?php echo CHtml::dropDownList('layout', '', $this->layouts, array(
        'class' => 'object_layout',
        'data-layout' => $initLayoutClass,
        ))?>
    </div>
<?php
}

if (!empty($this->title)) {
    echo '<div class="title"><h2>';
    if (!empty($this->title_link)) {
        echo CHtml::link(t($this->title), Yii::app()->baseUrl.'/'.trim($this->title_link, '/'));
    } else {
        echo t($this->title);
    }
    echo '</h2></div>';
}

if (!empty($this->allLink)) {
    $linkAll = CHtml::link(t($this->allLinkText), $this->allLink, array(
        'class' => 'link-all',
    ));

    if (in_array('before', $this->allLinkPosition)) {
        echo $linkAll;
    }
}

foreach ($this->content_list as $content_list) {
    $dataProvider = $this->getContentList($content_list);
    if (!$dataProvider) {
        continue;
    }

    $this->widget('zii.widgets.CListView', array(
        'id' => $this->getId(),
        'viewData'=> array('display_type' => $this->display_type),
        'dataProvider'=>$dataProvider,
        'itemView'=> $this->viewPath ? $this->viewPath : $dataProvider->render_layout,
        'summaryText'=>'',
        'ajaxUpdate'=>false,
        'cssFile' => false,
        'pager' => array(
            'header' => '',
            'cssFile' => false,
        ),
        'enablePagination'=> !$this->ajaxUpload,
        'enableSorting'=>false,
        'sortableAttributes'=>array(),
    ));
}

if ($this->ajaxUpload) {
    $this->widget('cms.extensions.yiinfinite-scroll.YiinfiniteScroller', array(
        'contentSelector' => '.items',
        'itemSelector'=>'.items>blockquote',
        'loadingText' => $this->ajaxUploadLoadingText,
        'donetext' => $this->ajaxUploadDoneText,
        'pages' => $dataProvider->pagination,
    ));
}

if (isset($linkAll) && in_array('after', $this->allLinkPosition)) {
    echo $linkAll;
}

echo CHtml::closeTag('div');
