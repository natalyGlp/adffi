<?php

/**
 * Class for render Block
 *
 *
 * @author Sviatoslav Danylenko
 * @version 1.0
 * @package cms.front_blocks.google_map
 */

// TODO: validation

class GoogleMapBlock extends BlockWidget
{
    public $address;
    public $latlng;
    public $maxZoomLevel = 17;
    public $style = 'default';
    const STYLE_DEFAULT = 'default';

    public $modalTriggerSelector = false;

    public $markerImage;
    public $markerImageRelativeToAssets = false;
    public $markerWidth;
    public $markerHeight;
    public $markerAnchorX;
    public $markerAnchorY;
    public $markerShape;

    public function init()
    {
        parent::init();

        if (empty($this->address)) {
            $this->address = Yii::app()->settings->get('general', 'address');
        }

        $this->registerClientScripts();
    }

    public function run()
    {
        $this->render('output', array(
            'id' => $this->id,
            'cssClass' => $this->containerCssClass . ' ' . $this->cssClass,
            'closeCssClass' => $this->closeCssClass,
        ));
    }

    public function getStylesList()
    {
        return array(
            self::STYLE_DEFAULT => t('Default'),
            'gray' => t('Gray'),
            'white' => t('White'),
        );
    }

    public function params()
    {
        return array(
            'style' => t('Style'),
            'address' => t('Enter address (leave blank to use one from settings)'),
            'latlng' => t('Google Latitude and Longitude'),
            'maxZoomLevel' => t('Map max zooming level'),

            'modalTriggerSelector' => t('jQuery selector for modal triggering (the map will be shown on click on item with selector)'),

            'markerImage' => t('Marker image src or data uri'),
            'markerImageRelativeToAssets' => t('Use src relative to assets'),
            'markerWidth' => t('Marker image width'),
            'markerHeight' => t('Marker image height'),
            'markerAnchorX' => t('Marker pointer x'),
            'markerAnchorY' => t('Marker pointer y'),
            'markerShape' => t('XY coordinates of marker shape path'),
        );
    }

    protected function registerClientScripts()
    {
        $asstesUrl = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets', false, -1, YII_DEBUG);
        $cs = Yii::app()->clientScript;

        $mapOptions = array(
            'maxZoomLevel' => $this->maxZoomLevel,
        );

        if ($this->hasMarker) {
            $mapOptions['markerImage'] = $this->getMarkerImageOptions();
        }

        if ($this->style != self::STYLE_DEFAULT) {
            $cs->registerScriptFile($asstesUrl . '/js/google-map-style-'.$this->style.'.js', CClientScript::POS_END);
            $mapOptions['styles'] = new CJavaScriptExpression('nespi.googleMapsStyles.'.$this->style);
        }


        $cs->registerScriptFile($asstesUrl . '/js/google-map.js', CClientScript::POS_END);
        $cs->registerScript(__FILE__.'#'.$this->id, '(function(){
            var map = new GoogleMap('.CJavaScript::encode($mapOptions).');
            map.init("#'.$this->id.'").then(function() {
                '.$this->getAddLocationJs().'
                map.spanToLocation("'.$this->id.'");
            });
        }());');

        if (empty($this->modalTriggerSelector)) {
            $cs->registerScript(__FILE__.'#modal', '
                $("body").on("click", "' . $this->modalTriggerSelector . '", function(event) {
                    event.preventDefault();

                    $(".'.$this->containerCssClass.'").addClass("is-active");
                });
                $("body").on("click", ".' . $this->closeCssClass . '", function(event) {
                    $(".'.$this->containerCssClass.'").removeClass("is-active");

                    return false;
                });
            ');
        }
    }

    public function getCloseCssClass() {
        return 'js-gm-close';
    }


    public function getContainerCssClass() {
        return 'js-gm-container';
    }

    protected function getHasMarker()
    {
        return !empty($this->markerImage);
    }

    protected function getMarkerImageOptions()
    {
        $options = array(
            'url' => $this->getMarkerSrc(),
            'width' => $this->markerWidth,
            'height' => $this->markerHeight,
            'anchorX' => $this->markerAnchorX,
            'anchorY' => $this->markerAnchorY,
        );

        $shape = $this->getMarkerShapePath();
        if ($shape) {
            $options['shape'] = $shape;
        }

        return $options;
    }

    protected function getMarkerSrc()
    {
        $src = $this->markerImage;

        if ($this->markerImageRelativeToAssets) {
            $src = Yii::app()->controller->themeAssets . '/' . $src;
        }

        return $src;
    }

    protected function getMarkerShapePath()
    {
        $path = $this->markerShape;

        $path = preg_replace('/\s/', '', $path);
        $path = trim($path, ',');

        return explode(',', $path);
    }

    protected function getAddLocationJs()
    {
        list($lat, $lng) = explode(',', $this->latlng);

        $options = array(
            'id' => $this->id,
            'lat' => $lat,
            'lng' => $lng,
            );

        return 'map.addLocation('.CJavaScript::encode($options).');';
    }
}
