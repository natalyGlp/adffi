<div name="div-block-content-<?php echo $block_model->id;?>">
    <fieldset>
        <legend>Apeareance</legend>
        <div class="row">
            <?= $form->labelEx($block_model, 'address'); ?>
            <?= $form->textField($block_model, 'address', array('id'=>'google-maps-address')); ?>
            <?= $form->error($block_model, 'address'); ?>
            <?php $this->widget('cms.widgets.geo.GMapsLatLngSelector', array(
                'name' => "Block[latlng]",
                'value' => $block_model->latlng,
                'addressSourceSelector' => '#google-maps-address',
            )); ?>
        </div>
        <div class="row">
            <?= $form->labelEx($block_model, 'style'); ?>
            <?= $form->dropDownList($block_model, 'style', $block_model->stylesList); ?>
            <?= $form->error($block_model, 'style'); ?>
        </div>

        <div class="row">
            <?= $form->labelEx($block_model, 'modalTriggerSelector'); ?>
            <?= $form->textField($block_model, 'modalTriggerSelector'); ?>
            <?= $form->error($block_model, 'modalTriggerSelector'); ?>
        </div>
    </fieldset>

    <fieldset>
        <legend>Marker options</legend>

        <div class="row">
            <?= $form->labelEx($block_model, 'markerImage'); ?>
            <?= $form->textField($block_model, 'markerImage'); ?>
            <?= $form->error($block_model, 'markerImage'); ?>
        </div>

        <div class="row">
            <?= $form->checkBox($block_model, 'markerImageRelativeToAssets'); ?>
            <?= $form->labelEx($block_model, 'markerImageRelativeToAssets'); ?>
            <?= $form->error($block_model, 'markerImageRelativeToAssets'); ?>
        </div>

        <div class="row">
            <?= $form->labelEx($block_model, 'markerWidth'); ?>
            <?= $form->numberField($block_model, 'markerWidth'); ?>
            <?= $form->error($block_model, 'markerWidth'); ?>
        </div>

        <div class="row">
            <?= $form->labelEx($block_model, 'markerHeight'); ?>
            <?= $form->numberField($block_model, 'markerHeight'); ?>
            <?= $form->error($block_model, 'markerHeight'); ?>
        </div>

        <div class="alert alert-info">the coordinate system starts in upper left corner of image</div>

        <div class="row">
            <?= $form->labelEx($block_model, 'markerAnchorX'); ?>
            <?= $form->numberField($block_model, 'markerAnchorX'); ?>
            <?= $form->error($block_model, 'markerAnchorX'); ?>
        </div>

        <div class="row">
            <?= $form->labelEx($block_model, 'markerAnchorY'); ?>
            <?= $form->numberField($block_model, 'markerAnchorY'); ?>
            <?= $form->error($block_model, 'markerAnchorY'); ?>
        </div>

        <div class="row">
            <?= $form->labelEx($block_model, 'markerShape'); ?>
            <?= $form->textArea($block_model, 'markerShape', array(
                'rows' => 10,
            )); ?>
            <?= $form->error($block_model, 'markerShape'); ?>
            <div class="alert alert-info">
                <ul>
                    <li>the path is the sequence of X, Y coordinates. E.g. X1, Y1, X2, Y2, Xn, Yn</li>
                    <li>0,0 is the upper left corner</li>
                    <li>the last point will be created automaticaly to match the start point</li>
                    <li>use comma as separator</li>
                    <li>the code can be multiline</li>
                </ul>
            </div>
        </div>
    </fieldset>
</div>
