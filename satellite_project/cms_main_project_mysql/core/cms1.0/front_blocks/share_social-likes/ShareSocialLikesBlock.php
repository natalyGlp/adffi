<?php

/**
 * Class ShareSocialLikesBlock
 * @site https://github.com/sapegin/social-likes
 */
class ShareSocialLikesBlock extends BlockWidget
{
    public $vkontakte;
    public $facebook;
    public $twitter;
    public $plusone;
    public $widgetOrientation;

    protected $_assetsUrl;

    public function init()
    {
        if (empty($this->_assetsUrl)) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                dirname(__FILE__). DIRECTORY_SEPARATOR.'assets'
            );
        }
        parent::init();
    }

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        $this->render('output',array('_assetsUrl'=>$this->_assetsUrl));
    }

    public function params()
    {
        return array(
            'vkontakte' => t('Show Vkontakte Like button'),
            'facebook' => t('Show Facebook Like button'),
            'twitter' => t('Show Twitter Like button'),
            'plusone' => t('Show G+ Like button'),
            'widgetOrientation' => t('Widget Orientation'),
        );
    }
}
