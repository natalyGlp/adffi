<aside class="related-news">
<?php if (!empty($this->title)): ?>
	<h2><?php echo $this->title;?></h2>
<?php endif ?>
    <?php foreach ($related_objects as $i => $data): ?>
    	<?php 
    		$this->render(GxcHelpers::getTruePath('content_type.'.$data->object_type.'.item_render_list'), array(
    			'data' => $data, 
    			'index' => $i,
    			'display_type' => 'related'
    		)); 
    	?>
    <?php endforeach ?>
    <div class="clear"></div> 
</aside>