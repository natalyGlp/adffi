<?php

/**
 * Class for render FeebBack ajax
 *
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @author Dmitriy Mytiansky
 *
 * @version 1.0
 * @package cms.front_blocks.feedback
 */

// DEPRECATED: feedback-btn-$this->cssClass

class FeedbackBlock extends BlockWidget
{
    public $title = '';
    public $mailTemplate; // id шаблона почтового сообщения (шаблон создается в админке)
    public $logEmails = false;
    public $redirectOnSuccess = false;
    public $isPopupForm;
    public $popupFormTriggerText;
    public $bindToSelector = false;
    public $submitButtonTitle = 'Отправить';
    public $subject = 'New email on your site';
    public $formName = 'Feedback Form';
    public $successMessage = 'Mail send.';
    public $allowedExts = '';
    public $useCaptcha = false;
    public $sendTo;

    protected $_activeFormOptions = array();

    public function run()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            GxcHelpers::registerJs('jquery-iframe-transport/jquery.iframe-transport.js');
        }

        $this->renderContent();
    }

    protected function renderContent()
    {
        Yii::import(GxcHelpers::getTruePath('front_blocks.' . $this->blockType . '.models') .'.*');
        $modelClassName = str_replace('Block', 'Model', get_class($this));
        $model = new $modelClassName();
        $model->useCaptcha = $this->useCaptcha;

        $this->fetchAllowedFileExts($model);

        if (isset($_POST[$modelClassName])) {
            if (isset($_POST['ajax']) && $_POST['ajax'] == $this->formId) {
                while (@ob_end_clean());
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            $model->attributes = $_POST[$modelClassName];
            $model->subject = empty($model->subject) ? $this->subject : $model->subject;

            $m = new YiiMailMessage();

            if ($model->validate()) {
                if ($this->logEmails) {
                    $emailLogModel = $this->logEmail($model);
                    $subject = 'NESPI: ' . $emailLogModel->source . (empty($emailLogModel->subject) ? '' : ' - ' . $emailLogModel->subject);
                    $link = trim(Yii::app()->createAbsoluteUrl('/'), '/') . '/project/backend/beemail/view/'.$emailLogModel->primaryKey;
                    $data = $emailLogModel->dataAttributes;
                    $text = '<p>' . t('Some one has send you email, you can see it here') . ': ' . CHtml::link($link, $link) . '</p><br /><br />';
                    foreach ($data as $item) {
                        $text .= CHtml::tag('p', array(), $item['label'].': '.$item['value']);
                    }

                    if (is_array($emailLogModel->files) && !empty($emailLogModel->files)) {
                        // обрабатываем прикрепления к форме
                        foreach ($emailLogModel->files as $file) {
                            $swiftAttachment = Swift_Attachment::fromPath(FeedbackBaseModel::getAttachmentPath($file['path']));
                            $m->attach($swiftAttachment);
                        }
                    }
                } else {
                    $mailtemplate = MailTemplate::getModelBySlug($this->mailTemplate);
                    if ($mailtemplate) {
                        $keys = array_map(function ($val) {
                            return '{' . $val . '}';
                        }, array_keys($model->attributes));
                        $keys[] = '{subject}';
                        /* feedback mail template values*/
                        $values = array_values($model->attributes);
                        $values[] = $mailtemplate->mail_title;
                        $vars = array($keys, $values);

                        $subject=$mailtemplate->prepareMail('mail_title', $vars);
                        $text=$mailtemplate->prepareMail('mail_text', $vars);
                    } else {
                        $subject=$mailtemplate->mail_title;
                        $text='<pre>' . var_export($model->attributes) . '</pre>';
                    }
                }

                $this->setEmailFromTo($m);
                $m->subject=$subject;
                $m->setBody($text, 'text/html');

                Yii::app()->mail->send($m);
                Yii::app()->user->setFlash('success', t($this->successMessage));
                if ($this->isPopupForm) {
                    $this->ajaxRespondSuccess();
                } else {
                    $this->respondSuccess();
                }
            }
        }

        $this->render('output', array(
            'model'=>$model,
        ));
    }

    protected function fetchAllowedFileExts($model)
    {
        // инициализируем расширения файлов, которые можно загружать через форму
        $validators = $model->validators;
        foreach ($validators as $validator) {
            if (get_class($validator) == 'CFileValidator') {
                $this->allowedExts = str_replace(',', '|', $validator->types);
                break;
            }
        }
    }

    protected function setEmailFromTo($m)
    {
        $sendTo = empty($this->sendTo) ? Yii::app()->settings->get('system', 'support_email') : $this->sendTo;

        $sendTo = preg_split('/\s*,\s*/', $sendTo);

        foreach ($sendTo as $email) {
            $m->addTo($email);
        }

        $m->from = Yii::app()->settings->get('system', 'support_email');
    }

    /**
     * Шорткат для создания значений для html аттрибутов placeholder
     * @param  CModel $model модель формы
     * @param  string $attribute аттрибут формы
     * @return string
     */
    public function createPlaceholder($model, $attribute)
    {
        return $model->getAttributeLabel($attribute) . ($model->isAttributeRequired($attribute) ? ' *' : '');
    }

    protected function logEmail($model)
    {
        $model->processAttachments();
        return EmailLog::saveForm($model, $this->formName);
    }

    public function beginForm()
    {
        $this->render('cmswidgets.views.notification_frontend');

        if ($this->isPopupForm) {
            $this->beginDialog();
        } else {
            echo CHtml::openTag('div', array(
                'class' => $this->cssClass,
                'id' => $this->containerId,
                ));

            echo CHtml::tag('a', array('name' => 'to-form'), '');
            if (!empty($this->title)) {
                ?>
                <div class="feedback-title">
                    <h2><?php echo t($this->title); ?></h2>
                </div>
                <?php
            }
        }

        $form = $this->beginWidget('CActiveForm', CMap::mergeArray($this->_activeFormOptions, array(
           'enableClientValidation'=>true,
            'id' => $this->formId,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'validateOnChange'=>false,
            ),
            'htmlOptions' => array(
                'enctype'=>'multipart/form-data'
                ),
        )));

        return $form;
    }

    protected function beginDialog()
    {
        $this->_activeFormOptions['clientOptions']['afterValidate'] = 'js:function(form, data, hasError) {
            var $files = $(":file", form),
                hasFiles = $(":file", form).length > 0,
                throwError = function() {
                    $("#'.$this->containerId.'").html(\'<div class="error"><h3>Произошла ошибка на сервере</h3><p>Перезагрузите страницу и повторите позже</p></div>\');
                };
            if (!hasError) {
                jQuery.ajax({
                    type: "POST",
                    cache: false,
                    data: hasFiles ?  $("#'.$this->formId.'").serializeArray() : $("#'.$this->formId.'").serialize(),
                    dataType: "json",

                    // iFrame transport
                    files: $files,
                    iframe: hasFiles,
                    processData: !hasFiles, // disable jquery post data processing if we use ifran=me transport

                    success: function(data) {
                        if ("redirect" in data) {
                            location.href = data.redirect;
                        } else if ("popup" in data) {
                            $("#'. $this->formId .'")[0].reset();
                            $("#'. $this->containerId .'").dialog("close");
                            $("body").append($(data.popup));
                        } else {
                            throwError();
                        }
                    },

                    error: throwError,
                });
            }
            return false;
        }';

        if (empty($this->bindToSelector)) {
            $id = 'feedback-btn-' . $this->getId();
            ?>
                <div class="<?php if (!empty($this->cssClass)) echo $this->cssClass . '-btn'; ?> <?php /*deprecated*/ if (!empty($this->cssClass)) echo 'feedback-btn-'.$this->cssClass; ?>" id="<?php echo $id; ?>">
                    <button type="button" class="btn"><?php echo t($this->popupFormTriggerText)?></button>
                </div>
            <?php
            $bindToSelector = "#$id";
        } else {
            $bindToSelector = $this->bindToSelector;
        }

        $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
            'id' => $this->containerId,
            'htmlOptions' => array(
                'class' => $this->cssClass,
                'style' => 'display: none;'
                ),
            'options'=>array(
                'autoOpen' => false,
                'title' => t($this->title),
                'modal' => true,
                'draggable' => false,
                'resizable' => false,
            ),
        ));

        // Открытие почтовой формы
        Yii::app()->clientScript->registerScript(__FILE__.'#ajaxFeedback'.$this->getId(), "
            var \$dialog = jQuery('#{$this->containerId}');
            var \$scrollFix;
            function scrollFix() {
                return $('<div>')
                    .css({
                        'overflow-y': 'scroll',
                        position: 'fixed',
                        width: '100%',
                        height: '100%',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        display: 'none',
                        'z-index': 2000,
                        '-webkit-overflow-scrolling': 'touch'
                    })
                    .addClass('dialog-scrollarea')
                    ;
            }
            scrollFix.enable = function() {
                jQuery('body').css('overflow', 'hidden');
                \$scrollFix.show();
            }
            scrollFix.disable = function() {
                jQuery('body').css('overflow', '');
                \$scrollFix.hide();
            }


            \$dialog.parent()
                .css('position', 'absolute')
                .addClass('{$this->cssClass}-dialog')
                .wrap(scrollFix())
            ;
            \$dialog.on('dialogclose', function() {
                \$dialog.parent().removeClass('active');
                scrollFix.disable();
            });

            \$scrollFix = \$dialog.parents('.dialog-scrollarea');

            jQuery('body')
                .on('click', '$bindToSelector', function(event) {
                    event.preventDefault();
                    scrollFix.enable();
                    \$dialog.dialog('open');
                    \$dialog.parent().addClass('active');
                })
                .on('click', '.dialog-scrollarea', function(event) {
                    if (event.target != \$scrollFix[0]) {
                        return;
                    }

                    event.preventDefault();
                    \$dialog.dialog('close');
                })
            ;
        ");
    }

    public function endForm()
    {
        $this->endWidget('CActiveForm');

        if ($this->isPopupForm) {
            $this->endDialog();
        } else {
            echo CHtml::closeTag('div');
        }
    }

    protected function endDialog()
    {
        $this->endWidget('zii.widgets.jui.CJuiDialog');
    }

    protected function ajaxRespondSuccess()
    {
        while (@ob_end_clean());
        Yii::app()->clientScript->scriptMap=array(
            'jquery.js'=>false,
            'jquery.min.js'=>false,
            'jquery-ui.min.js'=>false,
            'jquery-ui.js'=>false,
        );

        if ($this->redirectOnSuccess) {
            $data = array(
                'redirect' => $this->redirectOnSuccess,
                );
        } else {
            $data = array(
                'popup' => $this->controller->renderPartial('cmswidgets.views.notification_frontend', array(), true, true),
                );
        }

        header('Content-Type: application/json');
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    protected function respondSuccess()
    {
        if ($this->redirectOnSuccess) {
            Yii::app()->controller->redirect($this->redirectOnSuccess);
        } else {
            Yii::app()->controller->refresh();
        }
    }

    /**
     * @param  string $label       надпись на кнопке, по умолчанию FeedbackBlock::submitButtonTitle,
     *                             если is_array($label) — тогда параметр будет трактоваться как $htmlOptions
     * @param  array   $htmlOptions html аттрибуты кнопки
     * @return string               кнопка
     */
    public function submitButton($label = false, $htmlOptions = array())
    {
        if (is_array($label)) {
            $htmlOptions = $label;
            $label = false;
        }

        $label = $label ? $label : $this->submitButtonTitle;

        return CHtml::SubmitButton(t($label), $htmlOptions);
    }

    public function params()
    {
        return array(
            'title'=>t('Title'),
            'subject'=>t('Subject'),
            'mailTemplate'=>t('Mail Template Slug'),
            'logEmails' => t('Save emails on backend'),
            'redirectOnSuccess' => t('Redirect url after successfull submit (leave blank, when you don\'t need this)'),
            'submitButtonTitle' => t('Submit Button Title'),
            'isPopupForm' => t('Display form in popup'),
            'popupFormTriggerText' => t('Popup trigger button text'),
            'bindToSelector'=>t('jQuery selector for popup triggering (if empty, the new element and selector will be created)'),
            'useCaptcha' => t('Use Captcha'),
            'formName' => t('Form name to show in admin panel'),
            'successMessage' => t('Success Message'),
            'sendTo' => t('Send To Emails (separate multiple with comma). Default: "Support Email"'),
        );
    }

    public function getFormId()
    {
        return 'feedback-form-' . $this->getId();
    }

    public function getContainerId()
    {
        return 'feedback-' . $this->getId();
    }
}
