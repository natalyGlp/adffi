<?php
/**
 * This is the model class for Feedback Form.
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.feedback
 */

Yii::import(GxcHelpers::getTruePath('front_blocks.feedback.models.FeedbackBaseModel'));
class FeedbackModel extends FeedbackBaseModel 
{
	public $name;
	public $email;
	public $message;
	public $phone;

	public function rules()
	{
		return array(
			array('name, email, phone','required','message'=>t('Поле \'{attribute}\' не может быть пустым.')),
			array('message','safe'),
			array('email','email'),
			array('phone', 'match', 'pattern' => '/^[\(\) \-0-9\+]*$/', 'allowEmpty' => true, 'message' => t('The phone number should contain only numbers, spaces and (,-,+)')),
			$this->captchaRule,
			$this->attachmentsRule,
		);
	}

	public function attributeLabels()
	{
		return array(
	        'name'=>t('Введите Ваше имя'),
			'email'=>t('Введите Ваш e-mail'),
			'phone'=>t('Укажите номер телефона'),
			'message'=>t('Текст сообщения'),
			'attachments'=>t('Прикрепления'),
		);
	}

	public function emailLogLabels()
	{
		return array(
			'name'=>t('Имя отправителя'),
			'message'=>t('Текст сообщения'),
			'phone'=>t('Номер телефона'),
			'email'=>t('E-mail'),
			'attachments'=>t('Прикрепления') . ':html', // html - содержимое будет отображаться как html код
			);
	}
}
