<?php
/**
 * Базовый класс, который должны наследовать всем модели форм обратной связи
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.feedback
 */

abstract class FeedbackBaseModel extends CFormModel
{
    public $attachments = array();
    public $verifyCode;// captcha
    public $useCaptcha = false;// включает валидацию каптчи

    /**
     * @var array $files данные файлов, загруженных через форму (url, путь)
     */
    public $files = array();

    public $subject;
    public $email;

    /**
     * @return array массив настройки валидации каптчи для вставки в метод CModel::rules()
     */
    public function getCaptchaRule()
    {
        return array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements() || !$this->useCaptcha);
    }

    /**
     * @return array массив настройки валидации файлов для вставки в метод CModel::rules()
     */
    public function getAttachmentsRule()
    {
        return array('attachments', 'file', 'allowEmpty' => true,
            'types' => 'jpg,jpeg,gif,png,bmp,tif,tiff,7z,aiff,asf,avi,bmp,csv,doc,docx,fla,flv,gif,gz,gzip,mid,mov,mp3,mp4,mpc,mpeg,mpg,ods,odt,pdf,ppt,pxd,qt,ram,rar,rm,rmi,rmvb,rtf,sdc,sitd,swf,sxc,sxw,tar,tgz,txt,vsd,wav,wma,wmv,xls,xlsx,xml,pptx,zip,swf,flv,aiff,asf,avi,bmp,fla,flv,mid,mov,mp3,mp4,mpc,mpeg,mpg,qt,ram,rm,rmi,rmvb,swf,wav,wma,wmv',
        );
    }

    public function beforeValidate()
    {
        $this->attachments = CUploadedFile::getInstances($this, 'attachments');
        return parent::beforeValidate();
    }

    public function attributeLabels()
    {
        return array(
            'name' => t('Введите Ваше имя'),
            'email' => t('Введите Ваш e-mail'),
            'phone' => t('Укажите номер телефона'),
            'message' => t('Текст сообщения'),
            'attachments' => t('Прикрепления'),
            'verifyCode' => t('Введите код'),
        );
    }

    abstract public function emailLogLabels();

    public function activeCaptcha()
    {
        $code = Yii::app()->controller->createAction('captcha')->getVerifyCode();
        if ($code != $this->verifyCode) {
            $this->addError('verifyCode', 'Неправильный код проверки.');
        }

        if (!Yii::app()->request->isAjaxRequest) {
            Yii::app()->controller->createAction('captcha')->getVerifyCode(true);
        }
    }

    public function processAttachments()
    {
        $uploads = $this->attachments;
        $this->attachments = array();
        $this->files = array();

        if (count($uploads) == 0) {
            $this->attachments = '';
            $this->files = '';
            return;
        }

        $namePrefix = $this->genAttachmentsPrefix();

        $files = array();// обычные файлы соберем отдельно, что бы вывести после картинок отдельным списком
        foreach ($uploads as $upload) {
            $ext = pathinfo($upload->name, PATHINFO_EXTENSION);
            $name = pathinfo($upload->name, PATHINFO_FILENAME) . '_' . uniqid();
            $name = GxcHelpers::toSlug($name);

            $fileName = $name . '.' . $ext;
            $fileRelPath = $namePrefix . '/' . $fileName;
            $filePath = self::getAttachmentPath($fileRelPath);

            if ($upload->saveAs($filePath)) {
                $url = self::getAttachmentUrl($fileRelPath);

                if (!in_array($ext, array('jpg', 'jpeg', 'png', 'gif', 'bmp', 'tiff', 'tif'))) {
                    array_push(
                        $files,
                        '<p class="attachment attachment-file">' . CHtml::link(
                            '<span></span>' . $fileName,
                            $url,
                            array('target' => '_blank')
                        )
                    ) . '</p>';
                } else {
                    array_push(
                        $this->attachments,
                        CHtml::link(
                            CHtml::image($url, $fileName, array('width' => '150')),
                            $url,
                            array('target' => '_blank')
                        )
                    );
                }

                array_push($this->files, array(
                    'path' => $fileRelPath,
                    'type' => $upload->type,
                    'ext' => $ext,
                ));
            }
        }

        $this->attachments = implode("\n", CMap::mergeArray($this->attachments, $files));
    }

    /**
     * Создает директорию для аттачей, если ее нету и воозвращает путь и url этой директории
     * @return string путь к директории аттачей ([год]/[месяц]) относительно их базовой диреткории "feedback_attachments"
     */
    protected function genAttachmentsPrefix()
    {
        $namePrefix = date('Y') . '/' . date('m');
        $path = self::getAttachmentPath($namePrefix);

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        return $namePrefix;
    }

    public static function getAttachmentPath($fileName = false)
    {
        $basePath = Yii::getPathOfAlias("uploads.feedback_attachments");

        if (!$fileName) {
            return $basePath;
        }

        return $basePath . '/' . ltrim($fileName, '/');
    }

    public static function getAttachmentUrl($fileName = false)
    {
        $webPath = rtrim(Yii::app()->controller->createAbsoluteUrl('/'), '/') . '/uploads/feedback_attachments';

        if (!$fileName) {
            return $basePath;
        }

        return $webPath . '/' . ltrim($fileName, '/');
    }
}
