<div name="div-block-content-<?php echo $block_model->id;?>">
    <div class="row">
      <?php echo CHtml::label(Block::getLabel($block_model, "shortname"), ""); ?>
      <?php echo CHtml::textField("Block[shortname]", $block_model->shortname, array("id" => "Block-shortname"));?>
      <?php echo $form->error($model, "shortname"); ?>
    </div>    
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, "containerId"), ""); ?>
        <?php echo CHtml::textField("Block[containerId]", $block_model->containerId, array("id" => "Block-containerid"));?>
        <?php echo $form->error($model, "containerid"); ?>
    </div>