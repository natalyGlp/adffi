<?php
/**
 * Class for render Disqus Comments * 
 * 
 * @author Bazai best programmer <developer@studiobanzai.com>
 * @version 0.1
 * @package common.front_blocks.disqus_comments */

class DisqusCommentsBlock extends BlockWidget
{
    public $shortname;
    public $containerId;
    
    public function run()
    {                 
        $this->renderContent();         
    }       
 
    protected function renderContent()
    {     
    	$this->render('output',array());                                                          	       		     
    }
    
    public function params()
    {
        return array(
           'shortname' => t('DISQUS site shortname'),
           'containerId' => t('Existed Container Id (leave empty to create new one)'),
        );
    }
}

?>