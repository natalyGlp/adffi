<?php
ob_start();
?>
<li class="li_region" id="li_blockForRotation_{id}">
<span class="span_block">{title}</span> - <a onClick="$(this).parent().remove(); return false;" href=""><?php echo t('Delete');?></a>
<input type="hidden" value="{title}" name="Block[blockIds][{id}]" />
</li>
<?php
$blockRowTemplate = ob_get_clean();
?>
<div name="div-block-content-<?php echo $block_model->id; ?>">
	<ul class="ul_region" id="blocksRotator_blocksList">
		<?php
		if(count($block_model->blockIds))
			foreach ($block_model->blockIds as $id => $name) {
				echo strtr($blockRowTemplate, array(
		        	'{id}' => $id,
		        	'{title}' => CHtml::encode($name),
		        ));
			}
		?>
	</ul>
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model, "blockIds"), ""); ?>
		<?php $this->widget('CAutoComplete', array(
			'name'=>'changeBlockForm',                      
			'url'=>array('suggestBlocks'),
			'multiple'=>false,
			'mustMatch'=>true,
			'htmlOptions'=>array('size'=>50,'id'=>'blocksRotator_autoComplete'),
			'methodChain'=>".result(blocksRotatorCallback)",
		)); ?>
		<?php echo $form->error($model, "blockIds"); ?>
	</div>
	<script>
	$(function() {
		window.blocksRotatorCallback = function(event,item) {                                                     
			if(item!==undefined) {           
				createBlockListItem(item[0], item[1]);
			}
			$('#blocksRotator_autoComplete').val('');
		};

	    function createBlockListItem(title,id){           
	        $('#blocksRotator_blocksList').append(<?=strtr(CJavaScript::encode($blockRowTemplate), array(
	        	'{id}' => "'+id+'",
	        	'{title}' => "'+htmlEncode(title)+'",
	        ))?>); 
	    }

	    function htmlEncode(value){
		  //create a in-memory div, set it's inner text(which jQuery automatically encodes)
		  //then grab the encoded contents back out.  The div never exists on the page.
		  return $('<div/>').text(value).html();
		}
	});
	</script>
</div>