<?php
if(count($this->blockIds)) {
	$blocks = array_values($this->blockIds);
	$blockId = $blocks[rand(0, count($blocks) - 1)];


	$block = Block::model()->findByAttributes(array('block_id' => $blockId));
	$blocksInfo = GxcHelpers::getAvailableBlocks();

	// рендерим случайный блок из списка выбранных
	if($block) {
		$renderer = $this->createWidget('BlockRenderWidget');
		$renderer->renderBlock($blocksInfo[$block->type]['class'], unserialize($block->params));
	}
}