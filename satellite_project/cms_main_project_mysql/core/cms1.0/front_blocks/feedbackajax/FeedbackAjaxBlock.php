<?php

/**
 * Блок для отображения формы обратной связи
 * 
 * 
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.feedback
 */

/** 
 * Внимание, этот блок DEPRECATED, вместо него используйте FeedbackBlock
 */

Yii::import(GxcHelpers::getTruePath('front_blocks.feedback.FeedbackBlock'));
class FeedbackAjaxBlock extends FeedbackBlock
{
	public $isPopupForm = true;
}