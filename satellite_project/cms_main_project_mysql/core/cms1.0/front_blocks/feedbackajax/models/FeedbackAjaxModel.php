<?php
/**
 * This is the model class for Feedback Form.
* 
 * 
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.feedbackajax
 */

Yii::import(GxcHelpers::getTruePath('front_blocks.feedback.models.FeedbackBaseModel'));
class FeedbackAjaxModel extends FeedbackBaseModel {
	public $name;
	public $email;
	public $message;
	public $phone;
	public function rules(){
		return array(
			array('name, email','required','message'=>t('The field \'{attribute}\' can not be empty.')),
			array('message','safe'),
			array('email','email'),
			array('phone', 'match', 'pattern' => '/^[\(\) \-0-9\+]*$/', 'allowEmpty' => true, 'message' => t('The phone number should contain only numbers, spaces and (,-,+)')),
		);
	}

	public function attributeLabels(){
		return array(
	        'name'=>t('Укажите Ваше имя'),
			'email'=>t('Укажите E-mail'),
			'phone'=>t('Укажите номер телефона'),
			'message'=>t('Текст сообщения'),
			'attachments'=>t('Прикрепления'),
		);
	}

	public function emailLogLabels(){
		return array(
			'name'=>t('Имя отправителя'),
			'message'=>t('Текст сообщения'),
			'phone'=>t('Номер телефона'),
			'email'=>t('E-mail'),
			'attachments'=>t('Прикрепления') . ':html',
			);
	}
}
