<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model,'separator'),''); ?>
        <?php echo CHtml::textField("Block[separator]",
                        $block_model->separator); ?>
        <?php echo $form->error($model,'separator'); ?>
    </div>
</div>