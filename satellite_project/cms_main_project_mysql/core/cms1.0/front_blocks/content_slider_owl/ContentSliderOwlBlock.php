<?php

/**
 * Class for render Content based on Content list
 *
 * @author Sviatoslav Danulenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.content_slider_owl
 *
 * @see http://www.kyrielles.net/sliderkit/sliderkit_en.html
 */

Yii::import('cms.front_blocks.content_slider_sliderkit.ContentSliderBlockBase');

class ContentSliderOwlBlock extends ContentSliderBlockBase
{
    /**
     * NOTE:
     * items, itemsDesktop, itemsTablet, itemsMobile etc. были убраны в пользу
     * использования универсальной, низкоуровневой настройки itemsCustom
     */

    /**
     * @var array $itemsCustom This allow you to add custom variations of items depending from the width
     *                         Don't forget to include the lowest available screen size, otherwise it will take the default one for screens lower than lowest available.
     *                         Example:
     *                         [[0, 2], [400, 4], [700, 6], [1000, 8], [1200, 10], [1600, 16]]
     */
    public $itemsCustom = array();

    /**
     * @var boolean $singleItem Display only one item
     */
    public $singleItem = false;

    /**
     * @var boolean $itemsScaleUp Option to not stretch items when it is less than the supplied items
     */
    public $itemsScaleUp = false;

    /**
     * @var integer $slideSpeed Slide speed in milliseconds
     */
    public $slideSpeed = 200;

    /**
     * @var integer $paginationSpeed Pagination speed in milliseconds
     */
    public $paginationSpeed = 200;

    /**
     * @var integer $rewindSpeed Rewind speed in milliseconds
     */
    public $rewindSpeed = 200;

    /**
     * @var integer $autoPlay -1 == disabled, otherwise delay between slide changes in ms
     */
    public $autoPlay = -1;

    /**
     * @var boolean $stopOnHover Stop autoplay on mouse hover
     */
    public $stopOnHover = false;

    /**
     * @var boolean $navigation Display "next" and "prev" buttons.
     */
    public $navigation = false;

    /**
     * @var boolean|array $navigationText You can cusomize your own text for navigation. To get empty buttons use navigationText : false. Also HTML can be used here
     * @see ContentSliderOwlBlock::nextButtonLabel
     * @see ContentSliderOwlBlock::prevButtonLabel
     */
    public $navigationText = array('prev', 'next');

    /**
     * @var boolean $rewindNav Slide to first item. Use rewindSpeed to change animation speed.
     */
    public $rewindNav = true;

    /**
     * @var boolean $scrollPerPage Scroll per page not per item. This affect next/prev buttons and mouse/touch dragging.
     */
    public $scrollPerPage = true;

    /**
     * @var boolean $pagination Show pagination.
     */
    public $pagination = true;

    /**
     * @var boolean $paginationNumbers Show numbers inside pagination buttons
     */
    public $paginationNumbers = false;

    /**
     * @var boolean $responsive You can use Owl Carousel on desktop-only websites too! Just change that to "false" to disable resposive capabilities
     */
    public $responsive = true;

    /**
     * @var integer $responsiveRefreshRate Check window width changes every 200ms for responsive actions
     */
    public $responsiveRefreshRate = 200;

    /**
     * @var string $responsiveBaseWidth jQuery selector
     *  Owl Carousel check window for browser width changes. You can use any other
     *  jQuery element to check width changes for example ".owl-demo". Owl will
     *  change only if ".owl-demo" get new width.
     */
    public $responsiveBaseWidth = 'window';

    /**
     * @var boolean $lazyLoad Delays loading of images.
     * Images outside of viewport won't be loaded before user scrolls to them.
     * Great for mobile devices to speed up page loadings. IMG need special markup class="lazyOwl" and data-src="your img path"
     */
    public $lazyLoad = false;

    /**
     * @var boolean $lazyFollow When pagination used, it skips loading the images
     * from pages that got skipped. It only loads the images that get displayed in viewport.
     * If set to false, all images get loaded when pagination used. It is a sub setting of the lazy load function.
     */
    public $lazyFollow = true;

    /**
     * @var boolean|string $lazyEffect Default is fadeIn on 400ms speed. Use false to remove that effect.
     */
    public $lazyEffect = 'fade';

    /**
     * @var boolean $autoHeight Add height to owl-wrapper-outer so you can use diffrent heights on slides. Use it only for one item per page setting.
     */
    public $autoHeight = false;

    /**
     * @var boolean $dragBeforeAnimFinish Ignore whether a transition is done or not (only dragging).
     */
    public $dragBeforeAnimFinish = true;

    /**
     * @var boolean $mouseDrag Turn off/on mouse events.
     */
    public $mouseDrag = true;

    /**
     * @var boolean $touchDrag Turn off/on touch events.
     */
    public $touchDrag = true;

    /**
     * @var boolean $addClassActive Add "active" classes on visible items. Works with any numbers of items on screen.
     */
    public $addClassActive = false;

    /**
     * @var boolean|string $transitionStyle Add CSS3 transition style. Works only with one item on screen
     */
    public $transitionStyle = false;

    /**
     * TODO: UNIMPLEMENTED
     * baseClass
     * theme
     * jsonPath
     * jsonSuccess
     */

    /**
     * Вспомогательные аттрибуты для поддержки опции
     * ContentSliderOwlBlock::navigationText
     */
    public $nextButtonLabel = 'next';
    public $prevButtonLabel = 'prev';

    public function params()
    {
        return CMap::mergeArray(parent::params(), array(
            'itemsCustom' => _t('Breakpoints'),
            'singleItem' => _t('Display only one item'),
            'itemsScaleUp' => _t('Stretch items to fit width, when it is less than the supplied items'),
            'slideSpeed' => _t('Slide speed in ms'),
            'paginationSpeed' => _t('Pagination speed in ms'),
            'rewindSpeed' => _t('Rewind speed in ms'),
            'autoPlay' => _t('Autoplay. -1 - disabled, otherwise delay between slide changes in ms'),
            'stopOnHover' => _t('Stop autoplay on mouse hover'),
            'navigation' => _t('Display navigation buttons ("next" and "prev")'),
            'navigationText' => _t('The text on navigation buttons'),
            'rewindNav' => _t('Activate slider rewind (auto slide to first item)'),
            'scrollPerPage' => _t('Scroll per page not per item. This affect next/prev buttons and mouse/touch dragging'),
            'pagination' => _t('Show pagination (e.g. bullets)'),
            'paginationNumbers' => _t('Show numbers inside pagination buttons'),
            'responsive' => _t('Responsive mode'),
            'responsiveRefreshRate' => _t('Responsive refresh rate (how often to check window width changes'),
            'responsiveBaseWidth' => _t('Jquery selector of an element, to which width to be responsive'),
            'lazyLoad' => _t('Lazy load images'),
            'lazyFollow' => _t('Lazy load next slides page (sub setting of laze loading)'),
            'lazyEffect' => _t('Lazy loading effect'),
            'autoHeight' => _t('Auto height for owl-wrapper-outer (works only with one item displaying)'),
            'dragBeforeAnimFinish' => _t('Allow to drag before previous animation ends'),
            'mouseDrag' => _t('Turn off/on mouse events'),
            'touchDrag' => _t('Turn off/on touch events'),
            'addClassActive' => _t('Add "active" classes on visible items'),
            'transitionStyle' => _t('Slide <a href="http://owlgraphic.com/owlcarousel/demos/transitions.html">transition effect</a> (works only with one item displaying)'),

            'nextButtonLabel' => _t('Text on the "next" button'),
            'prevButtonLabel' => _t('Text on the "prev" button'),
        ));
    }

    public function getLazyEffectsList()
    {
        return array(
            /* none */
            'fade' => 'Fade',
        );
    }

    public function getTransitionStylesList()
    {
        return array(
            /* none */
            'fade' => 'fade',
            'backSlide' => 'backSlide',
            'goDown' => 'goDown',
            'fadeUp' => 'fadeUp',
        );
    }

    public function validate()
    {
        if ($this->autoHeight || $this->transitionStyle) {
            $this->singleItem = true;
        }

        $this->navigationText = array(
            $this->prevButtonLabel,
            $this->nextButtonLabel,
        );

        foreach ($this->itemsCustom as $index => &$setting) {
            $setting = preg_replace('/[^0-9]/', '', $setting);
            if (strlen($setting[0]) === 0) {
                // юзер оставил не заполненным брейкпоинт, игнорируем его
                unset($this->itemsCustom[$index]);
            } else {
                $setting = array_map('intval', $setting);
            }
        }
        $this->itemsCustom = array_values($this->itemsCustom);

        return true;
    }

    protected function registerSliderClientScript()
    {
        $assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets', false, -1, YII_DEBUG);
        Yii::app()->clientScript
            ->registerCoreScript('jquery')
            ->registerCssFile($assetsUrl . '/owl.carousel.css')
            ->registerScriptFile($assetsUrl . "/owl.carousel.min.js")
            ->registerScript(__FILE__ . '#' . $this->id, '
                $("#' . $this->id . '").owlCarousel('.$this->getSliderJsOptions().');
            ');

        if ($this->transitionStyle) {
            Yii::app()->clientScript->registerCssFile($assetsUrl . '/owl.transitions.css');
        }
    }

    protected function getSliderOptions()
    {
        return array(
            'itemsCustom' => $this->itemsCustom,
            'singleItem' => (boolean)$this->singleItem,
            'itemsScaleUp' => (boolean)$this->itemsScaleUp,
            'slideSpeed' => (int)$this->slideSpeed,
            'paginationSpeed' => (int)$this->paginationSpeed,
            'rewindSpeed' => (int)$this->rewindSpeed,
            'autoPlay' => $this->autoPlay == -1 ? false : (int)$this->autoPlay,
            'stopOnHover' => (boolean)$this->stopOnHover,
            'navigation' => (boolean)$this->navigation,
            'navigationText' => $this->navigationText,
            'rewindNav' => (boolean)$this->rewindNav,
            'scrollPerPage' => (boolean)$this->scrollPerPage,
            'pagination' => (boolean)$this->pagination,
            'paginationNumbers' => (boolean)$this->paginationNumbers,
            'responsive' => (boolean)$this->responsive,
            'responsiveRefreshRate' => (int)$this->responsiveRefreshRate,
            'responsiveBaseWidth' => $this->responsiveBaseWidth == 'window' ? new CJavaScriptExpression('window') : $this->responsiveBaseWidth,
            'lazyLoad' => (boolean)$this->lazyLoad,
            'lazyFollow' => (boolean)$this->lazyFollow,
            'lazyEffect' => $this->lazyEffect != 'fade' ? false : 'fade',
            'autoHeight' => (boolean)$this->autoHeight,
            'dragBeforeAnimFinish' => (boolean)$this->dragBeforeAnimFinish,
            'mouseDrag' => (boolean)$this->mouseDrag,
            'touchDrag' => (boolean)$this->touchDrag,
            'addClassActive' => (boolean)$this->addClassActive,
            'transitionStyle' => $this->transitionStyle,
        );
    }
}
