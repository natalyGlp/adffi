<div <?php if ($this->cssClass) echo 'class="'.$this->cssClass.'"'; ?>>
  <?php 
    $form = $this->beginWidget('CActiveForm', array(
      'enableClientValidation'=>true,
      'id' => 'orderForm'.$this->getId(),
      'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>false
      ),
    )); 
  ?>

  <?php
    if(count($products1) > 0) {
      // для обоих слайдеров используем одинаковую разметку
      ob_start();
      foreach ($products1 as $product) {
        ?>
          <div class="item">
            <?php if(isset($product['photos'][0])): ?>
              <img src="<?php echo $product['photos'][0]['link'] ?>">
              <img src="<?php echo isset($product['photos'][1]) ? $product['photos'][1]['link'] : $product['photos'][0]['link'] ?>" class="product-image-large" style="display: none;" />
            <?php endif; ?>
            <div class="content">
              <h3 class="product-name"><?php echo $product['name'] ?></h3>                
              <a class="green btn-open-order">Заказать</a>
            </div>
            <?php if($product['oldPrice'] > 0): ?>
              <p class="old-price"><s><span class="product-old-price"><?php echo $product['oldPrice'] ?></span> грн</s></p>
            <?php endif; ?>
            <p class="new-price"><span class="product-price"><?php echo $product['price'] ?></span> <small>грн</small></p>
            <input type="hidden" class="product-id" value="<?php echo $product['id'] ?>">
          </div>
        <?php
      }

      $productsHtml1 = ob_get_clean();

    }
    if(count($products2) > 0) {
      ob_start();
      foreach ($products2 as $product) {
        ?>
          <div class="item">
            <?php if(isset($product['photos'][0])): ?>
              <img src="<?php echo $product['photos'][0]['link'] ?>">
              <img src="<?php echo isset($product['photos'][1]) ? $product['photos'][1]['link'] : $product['photos'][0]['link'] ?>" class="product-image-large" style="display: none;" />
            <?php endif; ?>
            <div class="content">
              <h3 class="product-name"><?php echo $product['name'] ?></h3>                
              <a class="green btn-open-order">Заказать</a>
            </div>
            <?php if($product['oldPrice'] > 0): ?>
              <p class="old-price"><s><span class="product-old-price"><?php echo $product['oldPrice'] ?></span> грн</s></p>
            <?php endif; ?>
            <p class="new-price"><span class="product-price"><?php echo $product['price'] ?></span> <small>грн</small></p>
            <input type="hidden" class="product-id" value="<?php echo $product['id'] ?>">
          </div>
        <?php
      }

      $productsHtml2 = ob_get_clean();
    }
    ?>



  <div class="top-box">

    <h2>Уникальное предложение!</h2>

      <p>1+1=доставка бесплатно!</p>

  </div>



  <div class="order-form-box-top"> 

      <div class="before-form"></div>

    <div class="content">

      <p class="top-text"><strong>Обложки:</strong></p><br>

      <ul class="select">

        <li>одна обложка с калейдоскопом</li>

        <li>другая обложка с британским флагом</li>

      </ul>



      <div class="prices">

        <p class="new-price"><span>240</span> грн</p>

        <p class="old-price"><s>358 грн</s></p>

      </div>



      <div class="pay">

        <label>

          <input type="checkbox">

            Оплати с <img src="<?php echo Yii::app()->controller->layoutAssets ?>/images/visa.jpg"> или <img src="<?php echo Yii::app()->controller->layoutAssets ?>/images/master.jpg"> ,

            и получи 5% скидку!

          </label>              

      </div>

    </div> 

    <div class="triangles">

      <div class="triangle left"></div>

      <div class="triangle right"></div>

    </div>

  </div>



  <!--Sliders-->

  <div class="bottom-container">

  <div class="sliders-box">

    <div class="sliders-container">

      <div class="offer-slider-box">
        <?php if(isset($productsHtml1)): ?>
          <div id="<?php echo $firstId; ?>">  
            <?php echo $productsHtml1; ?>    
          </div>
        <?php endif; ?>
      </div>



      <div class="plus"></div>



      <div class="offer-slider-box right">
        <?php if(isset($productsHtml2)): ?>
          <div id="<?php echo $secondId; ?>">  
            <?php echo $productsHtml2; ?>    
          </div>
        <?php endif; ?>
      </div>



      <div class="equally"></div>

      <div class="clear"></div>

    </div>

  </div>



  <div class="order-form-box-bottom"> 
      <div class="order-form">
        <?php echo $form->textField($model,'name', array('placeholder' => 'Имя')); ?> 
        <?php echo $form->error($model,'name'); ?>

        <?php echo $form->textField($model,'phone', array('placeholder' => 'Телефон')); ?> 
        <?php echo $form->error($model,'phone'); ?>

        <?php echo $form->hiddenField($model, 'products[]', array('id' => 'onepluseone_product0')); ?> 
        <?php echo $form->hiddenField($model, 'products[]', array('id' => 'onepluseone_product1')); ?> 

        <?php echo CHtml::submitButton('Заказать', array('class' => 'btn-open-ty')); ?>
        <div class="free">Доставка бесплатно</div>
      </div>            
  </div>
  </div><!-- box for slider+bottom form part -->

  <?php $this->endWidget('CActiveForm'); ?>
</div>