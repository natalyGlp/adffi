<?php

/**
 * Class for render Top Menu
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package common.front_blocks.menu
 */

class MenuBlock extends BlockWidget
{
    /**
     *  Флаг, для переключения в режима вывода таксономии
     */
    const HAS_TAXONOMY_ID = 'taxonomy';

    public $menu_id = null;
    public $menu_align = Menu::ALIGN_TYPE_FLOAT;
    public $title = '';
    public $title_href = '';

    public $term_id;
    public $term_with_objects;
    public $term_as_header = false;
    // TODO
    public $max_levels = 0;

    public function run()
    {
        $this->renderContent();

        // TODO: active Для родителей
        Yii::app()->clientScript->registerScript(__FILE__, '
            $(".menu-statefull a").each(function() {
                if(this.pathname == location.pathname) {
                    $(this).parents("li").eq(0)
                        .addClass("active") // DEPRECATED
                        .addClass("is-active");
                }
            });');
    }

    protected function renderContent()
    {
        $cacheId = Menu::CACHE_ID;
        $menuKey = 'menu_' . $this->menu_id . '_' . $this->term_id . '_' . $this->cssClass;

        if (($menus = Yii::app()->cache->get($cacheId)) === false) {
            $menus = array();
        }

        if (!isset($menus[$menuKey])) {
            $menu = $this->renderMenu();
            if (empty($menu)) {
                return;
            }

            $html = $this->render('output', array(
                'menu' => $menu,
            ), true);

            $menus[$menuKey] = $html;

            Yii::app()->cache->set($cacheId, $menus);
        }

        echo $menus[$menuKey];
    }

    /**
     * Рекурсивный рендеринг меню
     */
    public function renderMenu($menus = array(), $level = 1)
    {
        if (empty($menus)) {
            $menus = $this->getMenuItems($this->term_id ? $this->term_id : 0);
        }

        $menuTree = array();
        foreach ($menus as $index => $menu) {
            $nextLvl = $this->getMenuItems($menu['id']);
            $hasChildren = count($nextLvl) > 0;

            $liContent = $this->getLiContent($menu, $hasChildren);
            if ($hasChildren) {
                $liContent .= PHP_EOL . $this->renderMenu($nextLvl, $level + 1);
            }

            $menuTree[] = CHtml::tag('li', array(
                'class' => $this->getLiCssClass($menu, $hasChildren),
            ), $liContent);
        }

        return CHtml::tag('ul', array(
            'class' => $this->cssClass . '_' . $level,
        ), implode("\n", $menuTree));
    }

    protected function getMenuItems($parent_id)
    {
        if ($this->isTaxonomyMenu) {
            return $this->getTaxonomyMenu($parent_id);
        } else {
            $result = array();
            $menu_items = MenuItem::model()->findAll(array(
                'condition' => 'parent=:pid AND menu_id=:mid AND is_visible=1',
                'params' => array(':pid' => $parent_id, ':mid' => $this->menu_id),
                'order' => ' t.sort_order ASC'
            ));

            foreach ($menu_items as $menu_item) {
                $result[] = array(
                    'name' => $menu_item->name,
                    'isHeader' => false,
                    'cssClass' => $menu_item->css_class,
                    'type' => $menu_item->type,
                    'value' => $menu_item->value,
                    'link' => self::buildLink($menu_item),
                    'id' => $menu_item->menu_item_id
                );
            }
            return $result;
        }
    }

    protected function getIsTaxonomyMenu()
    {
        return $this->menu_id == self::HAS_TAXONOMY_ID;
    }

    protected function getTaxonomyMenu($parent_id)
    {
        $result = array();

        $menu_items = Term::model()->findAll(array(
            'condition' => 'parent=:pid',
            'params' => array(':pid' => $parent_id),
            'order' => ' t.name ASC'
        ));

        // Обьекты родительского терма
        if ($parent_id > 0) {
            $result = $result + $this->getMenuTermObjects($parent_id);
        }

        foreach ($menu_items as $menu_item) {
            $result[] = array(
                'name' => $menu_item->name,
                'isHeader' => $this->term_as_header,
                'cssClass' => $this->cssClass . $menu_item->primaryKey,
                'type' => Menu::TYPE_TERM,
                'value' => $menu_item->primaryKey,
                'link' => self::buildLink((object) array(
                    'type' => Menu::TYPE_TERM,
                    'value' => $menu_item->primaryKey,
                )),
                'id' => $menu_item->primaryKey
            );
        }

        return $result;
    }

    /**
     * @param  integer $parent_id id родительского терма
     * @return array возвращает массив для меню в котором находятся обьекты-дети терма
     */
    protected function getMenuTermObjects($parent_id)
    {
        $result = array();

        $criteria = array(
            'condition' => 'termsRelation.term_id = :tid',
            'params' => array(
                ':tid' => $parent_id,
            ),
        );
        GxcHelpers::applyObjectStatusCriteria($criteria);
        $parentObjects = Object::model()->with('termsRelation')->findAll($criteria);

        foreach ($parentObjects as $object) {
            $result[] = array(
                'name' => $object->object_name,
                'isHeader' => false,
                'cssClass' => $this->cssClass . $parent_id . '-' . $object->primaryKey,
                'type' => Menu::TYPE_CONTENT,
                'value' => $object->primaryKey,
                'link' => self::buildLink((object) array(
                    'type' => Menu::TYPE_CONTENT,
                    'value' => $object->primaryKey,
                )),
                'id' => -1// id, который будет использоваться для поиска родителей
            );
        }

        return $result;
    }

    protected function getLiCssClass($menu, $hasChildren)
    {
        $liCssClasses = array();
        if (!empty($menu['cssClass'])) {
            $liCssClasses[] = 'li-' . $menu['cssClass'];
        }

        if ($hasChildren) {
            $liCssClasses[] = 'has-dropdown';
        }
        return implode(' ', $liCssClasses);
    }

    protected function getLiContent($menu)
    {
        $liContent = array();
        $cssClasses = array();
        if (!empty($menu['cssClass'])) {
            $cssClasses[] = $menu['cssClass'];
        }

        if ($menu['type'] == Menu::TYPE_STRING) {
            $menu['link'] = '';
            $htmlOptions['onclick'] = 'return false;';
            $cssClasses[] = 'no-href';
        }

        $htmlOptions = array(
            'class' => implode(' ', $cssClasses),
        );

        if ($menu['isHeader']) {
            return CHtml::tag('h3', $htmlOptions, $menu['name']);
        } else {
            return CHtml::link($menu['name'], $menu['link'], $htmlOptions);
        }
    }

    protected static function buildLink($item)
    {
        switch ($item->type) {
            case Menu::TYPE_PAGE:
                $page = Page::model()->findByPk($item->value);
                if ($page) {
                    return $page->getViewUrl();
                }
                break;
            case Menu::TYPE_CONTENT:
                $content = Object::model()->findByPk($item->value);
                if ($content) {
                    return $content->getObjectLink();
                }
                break;
            case Menu::TYPE_TERM:
                $term = Term::model()->findByPk($item->value);
                if ($term) {
                    return $term->buildLink();
                }
                break;
            case Menu::TYPE_URL:
                return $item->value;
                break;
            case Menu::TYPE_STRING:
            default:
                return $item->value;
                break;

                return 'javascript:void(0);';
        }
    }

    public static function findMenu()
    {
        $menus = Menu::model()->findAll();
        $result = CHtml::listData($menus, 'menu_id', 'menu_name');

        $result[self::HAS_TAXONOMY_ID] = 'Taxonomy';

        return $result;
    }

    public static function findTerms()
    {
        $terms = Term::model()->findAll();
        $terms = CHtml::listData($terms, 'term_id', 'name');

        return $terms;
    }

    public function validate()
    {
        if ($this->menu_id == "") {
            $this->errors['menu_id'] = t('Please select a Menu');
            return false;
        } else {
            return true;
        }
    }

    public function params()
    {
        return array(
            'menu_id' => t('Menu'),
            'menu_align' => t('Menu align'),
            'title' => t('Menu title'),
            'title_href' => t('Menu title link'),
            'term_id' => t('Term'),
            'term_with_objects' => t('List binded objects'),
            'term_as_header' => t('Show term as title'),
            'max_levels' => t('Show menu levels (0 - show the whole tree)'),
        );
    }

    public function getAlign()
    {
        $cssClass = '';
        switch ($this->menu_align) {
            case Menu::ALIGN_TYPE_TOP:
                $cssClass = 'top';
                break;
            case Menu::ALIGN_TYPE_BOTTOM:
                $cssClass = 'bottom';
                break;
            case Menu::ALIGN_TYPE_LEFT:
                $cssClass = 'left';
                break;
            case Menu::ALIGN_TYPE_RIGHT:
                $cssClass = 'right';
                break;
            case Menu::ALIGN_TYPE_FLOAT:
                $cssClass = 'float';
                break;

            default:
                $cssClass = 'float';
                break;
        }
        return 'menu-statefull ' . $cssClass;
    }
}
