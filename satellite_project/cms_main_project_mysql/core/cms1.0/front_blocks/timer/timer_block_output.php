<?php
/**
 * @var string $id id для блока
 * @var array $options настройки, которые передаются скрипту (deprecated)
 */
?>
<div class="timer-wrapper<?php if(!empty($this->cssClass)) echo ' '.$this->cssClass; ?>">
    <div class="timer" id="<?php echo $id ?>">
        <div class="digits-container day">
            <span class="digit day0"></span>
            <span class="digit day1"></span>
        </div>
        <div class="separator">:</div>
        <div class="digits-container digits-container hour">
            <span class="digit hour0"></span>
            <span class="digit hour1"></span>
        </div>
        <div class="separator">:</div>
        <div class="digits-container min">
            <span class="digit min0"></span>
            <span class="digit min1"></span>
        </div>
        <div class="separator">:</div>
        <div class="digits-container sec">
            <span class="digit sec0"></span>
            <span class="digit sec1"></span>
        </div>
        <div class="ended-message"><?php echo $this->timeoutMessage ?></div>
    </div>
</div>
