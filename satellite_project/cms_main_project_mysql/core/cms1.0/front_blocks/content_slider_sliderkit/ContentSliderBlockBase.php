<?php

abstract class ContentSliderBlockBase extends BlockWidget
{
    /**
     * CMS params
     */
    public $contentLists;
    public $slideImageSize = '';
    public $thumbImageSize = '';
    public $itemsInSlide = 1;
    public $thumbView;
    public $slideView;

    // TODO: функция, которая будет показывать выпадашку с типами ресурсов
    // (предварительно функция должна проходиться по всем контент типам и запоминать их ресурсы)
    public $resourceType = 'thumbnail';
    public $lightBox = false;

    /**
     * @var boolean если true, то будет взят только первый ресурс с каждого обьекта
     */
    public $onlyFirstResource = true;

    public $title = '';
    public $titleLink = '';

    public $models = array();
    public $navHtml = array();
    public $slidesHtml = array();

    abstract protected function registerSliderClientScript();
    /**
     * @return array массив настроек, который будет передаваться JS плагину
     */
    abstract protected function getSliderOptions();

    public function init() {
        parent::init();

        $this->registerClientScript();
    }

    public function run()
    {
        list($this->navHtml, $this->slidesHtml) = $this->prepareNavAndContent();

        if (empty($this->navHtml) && empty($this->slidesHtml)) {
            return;
        }

        $this->render('output', array(
            'cssClass' => $this->cssClass,
            'id' => $this->id,
            'slides' => $this->slidesHtml,
            'nav' => $this->navHtml,
            'title' => $this->title,
            'titleLink' => $this->titleLink,
        ));
    }

    protected function prepareNavAndContent()
    {
        if (!empty($this->slidesHtml) || !empty($this->navHtml)) {
            return array(
                $this->navHtml,
                $this->slidesHtml,
            );
        }

        $this->ensureModels();

        $navHtml = array();
        $slidesHtml = array();
        $slidesHtmlBuffer = '';

        $resources = $this->extractResources($this->models);

        foreach ($resources as $i => $resource) {
            if ($this->isSlidesVisible) {
                $resource['imageSrc'] = $resource['slideSrc'];
                $slidesHtmlBuffer .= $this->renderSlideItem($resource);
            }

            if ($i % $this->itemsInSlide === 0) {
                if ($this->isNavVisible) {
                    $resource['imageSrc'] = $resource['thumbSrc'];
                    array_push($navHtml, $this->renderNavItem($resource));
                }

                if ($this->isSlidesVisible) {
                    array_push($slidesHtml, $slidesHtmlBuffer);
                    $slidesHtmlBuffer = '';
                }
            }
        }

        return array(
            $navHtml,
            $slidesHtml,
        );
    }

    protected function ensureModels()
    {
        if ($this->models && !is_array($this->models)) {
            $this->models = array(
                $this->models,
            );
        }

        if (!$this->hasModels) {
            $this->models = $this->loadContentLists();
        }

        if (!$this->hasModels) {
            throw new CException('The ' . get_class($this) . '::models or ' . get_class($this) . '::contentList properties are required');
        }
    }

    protected function getHasModels()
    {
        return !empty($this->models);
    }

    protected function loadContentLists()
    {
        if (!isset($this->contentLists) || !is_array($this->contentLists)) {
            throw new CException('No content list specified. ' . get_class($this) . '::contentList should be an array of content lists pk');
        }

        $list = array();

        foreach ($this->contentLists as $contentListPk) {
            $contentLists = GxcHelpers::getContentList($contentListPk);
            $list = array_merge($list, $contentLists);
        }

        return $list;
    }

    protected function extractResources($models)
    {
        $that = $this;
        return array_reduce($models, function ($acc, $model) use ($that) {
            $images = GxcHelpers::getResourceObjectFromDatabase($model, $that->resourceType);

            // TODO: убрать, когда будет дописан функционал для создания превьюшек без лишних запросов
            if (!empty($that->slideImageSize)) {
                GxcHelpers::getResourceObjectFromDatabase($model, $that->resourceType, $that->slideImageSize);
            }
            if (!empty($that->thumbImageSize)) {
                GxcHelpers::getResourceObjectFromDatabase($model, $that->resourceType, $that->thumbImageSize);
            }

            if ($that->onlyFirstResource && !empty($images)) {
                $images = array(
                    reset($images),
                );
            }

            return array_reduce($images, function ($acc, $image) use ($model, $that) {
                $filename = basename($image['link']);
                $dirname = dirname($image['link']);

                $slideSrc = $thumbSrc = $image['link'];

                if (!empty($that->slideImageSize)) {
                    $slideSrc = $dirname . '/resized/' . $that->slideImageSize . $filename;
                }

                if (!empty($that->thumbImageSize)) {
                    $thumbSrc = $dirname . '/resized/' . $that->thumbImageSize . $filename;
                }

                $acc[] = array(
                    'data' => $model,
                    'fullSrc' => $image['link'],
                    'slideSrc' => $slideSrc,
                    'thumbSrc' => $thumbSrc,
                    'index' => count($acc),
                );

                return $acc;
            }
                , $acc);
        }
            , array());
    }

    protected function renderNavItem($params = array())
    {
        return $this->render($this->thumbView, $params, true);
    }

    protected function renderSlideItem($params = array())
    {
        return $this->render($this->slideView, $params, true);
    }

    protected function getIsNavVisible()
    {
        return !empty($this->thumbView);
    }

    protected function getIsSlidesVisible()
    {
        return !empty($this->slideView);
    }

    public function getSlideViewsList()
    {
        return array_filter($this->getViewsFiles(), function ($view) {
            return $view && preg_match('/^slide_/', $view);
        });
    }

    public function getThumbViewsList()
    {
        return array_filter($this->getViewsFiles(), function ($view) {
            return $view && preg_match('/^(thumb|bullet)_/', $view);
        });
    }

    protected function getViewsFiles()
    {
        $defaultPath = Yii::getPathOfAlias('cms.front_blocks.'.$this->blockType.'.views');
        $themePath = Yii::getPathOfAlias('theme.front_blocks.'.$this->blockType.'.views');

        $views = array();

        if (is_dir($defaultPath)) {
            $views = CMap::mergeArray($views, scandir($defaultPath));
        }

        if (is_dir($themePath)) {
            $views = CMap::mergeArray($views, scandir($themePath));
        }

        $views = array_map(function ($item) {
            return pathinfo($item, PATHINFO_FILENAME);
        }
            , $views);

        $views = array_unique($views);

        return array_combine($views, $views);
    }

    protected function registerClientScript()
    {
        $this->registerSliderClientScript();

        if ($this->lightBox) {
            $this->registerLightBoxScript();
        }
    }

    protected function registerLightBoxScript()
    {
        $fancyBoxUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.frontend.fancybox'), false, -1, YII_DEBUG);
        Yii::app()->getClientScript()->registerCssFile($fancyBoxUrl . "/jquery.fancybox-1.3.4.css")->registerScriptFile($fancyBoxUrl . "/jquery.fancybox-1.3.4.pack.js")->registerScript(__FILE__ . '#fancybox', '
                $("#' . $this->id . ' a:has(img)").fancybox({
                    "onStart": function() {
                        $("#fancybox-wrap").addClass("is-loading");
                    },
                    "onComplete": function() {
                        $("#fancybox-wrap").removeClass("is-loading");
                    }
                });
            ');
    }

    public function params()
    {
        return array(
            'contentLists' => _t('Content list'),
            'slideImageSize' => _t('Slide Image size'),
            'thumbImageSize' => _t('Thumbnail size'),
            'itemsInSlide' => _t('Count Items in slide'),
            'thumbView' => _t('Thumbnail View'),
            'slideView' => _t('Slide View'),
            'title' => _t('Title'),
            'titleLink' => _t('Title link'),
            'resourceType' => _t('Resource type'),
            'lightBox' => _t('Use lightbox'),
            'onlyFirstResource' => _t('Get only first object\'s resource'),
        );
    }

    protected function getSliderJsOptions()
    {
        return CJavaScript::encode($this->arrayRecursiveDiff($this->getSliderOptions(), $this->getDefaultParams()));
    }

    /**
     * работает так же как и array_diff со следующими отличиями:
     * - работает рекурсивно
     * - принимает только два массива
     * - для сравнения используется ==, а не ===
     * - выполняет проверку ключей массива
     * Функция взята из комментариев к документации array_diff()
     *
     * @param  array $aArray1 массив, чьи отличающиеся элементы нужно получить
     * @param  array $aArray2 массив, с которым надо сравнивать
     * @return array          массив с отличающимися элементами
     * @see http://php.net/manual/en/function.array-diff.php
     */
    protected function arrayRecursiveDiff($aArray1, $aArray2)
    {
        $aReturn = array();

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = $this->arrayRecursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {$aReturn[$mKey] = $aRecursiveDiff;}
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        return $aReturn;
    }
}
