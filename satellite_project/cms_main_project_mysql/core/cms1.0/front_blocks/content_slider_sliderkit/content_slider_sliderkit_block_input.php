<div name="div-block-content-<?php echo $block_model->id;?>">
    <?php include Yii::getPathOfAlias('cms.front_blocks.content_slider_sliderkit.content_slider_base_form').'.php'; ?>

    <div class="row">
        <?= $form->labelEx($block_model, 'start') ?>
        <?= $form->numberField($block_model, 'start'); ?>
        <?= $form->error($block_model, 'start'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'auto'); ?>
        <?= $form->labelEx($block_model, 'auto') ?>
        <?= $form->error($block_model, 'auto'); ?>
    </div>

    <div class="row">
        <?= $form->labelEx($block_model, 'autospeed') ?>
        <?= $form->numberField($block_model, 'autospeed'); ?>
        <?= $form->error($block_model, 'autospeed'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'autostill'); ?>
        <?= $form->labelEx($block_model, 'autostill') ?>
        <?= $form->error($block_model, 'autostill'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'mousewheel'); ?>
        <?= $form->labelEx($block_model, 'mousewheel') ?>
        <?= $form->error($block_model, 'mousewheel'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'keyboard'); ?>
        <?= $form->labelEx($block_model, 'keyboard') ?>
        <?= $form->error($block_model, 'keyboard'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'circular'); ?>
        <?= $form->labelEx($block_model, 'circular') ?>
        <?= $form->error($block_model, 'circular'); ?>
    </div>

    <div class="row">
        <?= $form->labelEx($block_model, 'shownavitems') ?>
        <?= $form->numberField($block_model, 'shownavitems'); ?>
        <?= $form->error($block_model, 'shownavitems'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'navitemshover'); ?>
        <?= $form->labelEx($block_model, 'navitemshover') ?>
        <?= $form->error($block_model, 'navitemshover'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'navclipcenter'); ?>
        <?= $form->labelEx($block_model, 'navclipcenter') ?>
        <?= $form->error($block_model, 'navclipcenter'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'navcontinuous'); ?>
        <?= $form->labelEx($block_model, 'navcontinuous') ?>
        <?= $form->error($block_model, 'navcontinuous'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'navscrollatend'); ?>
        <?= $form->labelEx($block_model, 'navscrollatend') ?>
        <?= $form->error($block_model, 'navscrollatend'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'navpanelautoswitch'); ?>
        <?= $form->labelEx($block_model, 'navpanelautoswitch') ?>
        <?= $form->error($block_model, 'navpanelautoswitch'); ?>
    </div>

    <div class="row">
        <?= $form->labelEx($block_model, 'navfx') ?>
        <?= $form->dropDownList($block_model, 'navfx', $block_model->getNavFxList()); ?>
        <?= $form->error($block_model, 'navfx'); ?>
    </div>

    <div class="row">
        <?= $form->labelEx($block_model, 'scroll') ?>
        <?= $form->numberField($block_model, 'scroll'); ?>
        <?= $form->error($block_model, 'scroll'); ?>
    </div>

    <div class="row">
        <?= $form->labelEx($block_model, 'scrollspeed') ?>
        <?= $form->numberField($block_model, 'scrollspeed'); ?>
        <?= $form->error($block_model, 'scrollspeed'); ?>
    </div>

    <div class="row">
        <?= $form->labelEx($block_model, 'panelfx') ?>
        <?= $form->dropDownList($block_model, 'panelfx', $block_model->getFxList()); ?>
        <?= $form->error($block_model, 'panelfx'); ?>
    </div>

    <div class="row">
        <?= $form->labelEx($block_model, 'panelfxspeed') ?>
        <?= $form->numberField($block_model, 'panelfxspeed'); ?>
        <?= $form->error($block_model, 'panelfxspeed'); ?>
    </div>

    <div class="row">
        <?= $form->labelEx($block_model, 'panelfxeasing') ?>
        <?= $form->dropDownList($block_model, 'panelfxeasing', $block_model->getEasingList()); ?>
        <?= $form->error($block_model, 'panelfxeasing'); ?>
    </div>

    <div class="row">
        <?= $form->labelEx($block_model, 'panelfxfirst') ?>
        <?= $form->dropDownList($block_model, 'panelfxfirst', $block_model->getFxList()); ?>
        <?= $form->error($block_model, 'panelfxfirst'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'panelbtnshover'); ?>
        <?= $form->labelEx($block_model, 'panelbtnshover') ?>
        <?= $form->error($block_model, 'panelbtnshover'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'panelclick'); ?>
        <?= $form->labelEx($block_model, 'panelclick') ?>
        <?= $form->error($block_model, 'panelclick'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'verticalnav'); ?>
        <?= $form->labelEx($block_model, 'verticalnav') ?>
        <?= $form->error($block_model, 'verticalnav'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'verticalslide'); ?>
        <?= $form->labelEx($block_model, 'verticalslide') ?>
        <?= $form->error($block_model, 'verticalslide'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'tabs'); ?>
        <?= $form->labelEx($block_model, 'tabs') ?>
        <?= $form->error($block_model, 'tabs'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'freeheight'); ?>
        <?= $form->labelEx($block_model, 'freeheight') ?>
        <?= $form->error($block_model, 'freeheight'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'fastchange'); ?>
        <?= $form->labelEx($block_model, 'fastchange') ?>
        <?= $form->error($block_model, 'fastchange'); ?>
    </div>

    <div class="row">
        <?= $form->checkBox($block_model, 'debug'); ?>
        <?= $form->labelEx($block_model, 'debug') ?>
        <?= $form->error($block_model, 'debug'); ?>
        <div class="alert alert-info">Read <a href="http://www.kyrielles.net/sliderkit/sliderkit_en.html#TOC-debugmode" target="_blank">official docs</a> for more information about debug mode</div>
    </div>
</div>
