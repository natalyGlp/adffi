<?php
/**
 * @var string $id
 * @var string $cssClass
 * @var array $nav
 * @var array $slides
 */
?>
<div class="<?php echo $cssClass; ?>">
    <?php
    if (!empty($title)) {
        echo '<div class="title"><h2>';
        if (!empty($titleLink)) {
            echo CHtml::link(t($title), $titleLink);
        } else {
            echo t($title);
        }
        echo '</h2></div>';
    }
    ?>

    <div id="<?php echo $id ?>" class="sliderkit">
    <?php if (!empty($nav)): ?>
        <div class="sliderkit-nav">
            <div class="sliderkit-nav-clip">
                <ul>
                <?php
                    foreach ($nav as $navItem) {
                        echo CHtml::tag('li', array(), $navItem);
                    }
                ?>
                </ul>
            </div>

            <div class="sliderkit-nav-btn sliderkit-nav-prev"><a rel="nofollow" href="#" title="Previous line"><span>Previous</span></a></div>
            <div class="sliderkit-nav-btn sliderkit-nav-next"><a rel="nofollow" href="#" title="Next line"><span>Next</span></a></div>
        </div>
    <?php endif; ?>
    <?php if (!empty($slides)): ?>
        <div class="sliderkit-panels">
            <div class="sliderkit-go-btn sliderkit-go-prev"><a rel="nofollow" href="#" title="Previous"><span>Previous</span></a></div>
            <div class="sliderkit-go-btn sliderkit-go-next"><a rel="nofollow" href="#" title="Next"><span>Next</span></a></div>

            <?php
                foreach ($slides as $slide) {
                    echo CHtml::tag('div', array(
                        'class' => 'sliderkit-panel',
                        ), $slide);
                }
            ?>
        </div>
    <?php endif; ?>
    </div>
</div>

