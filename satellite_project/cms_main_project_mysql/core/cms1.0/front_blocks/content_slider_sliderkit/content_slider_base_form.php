<?php $this->widget('cms.widgets.page.BlockContentListManageWidget', array(
    'blockWidget' => $block_model,
    'attribute' => 'contentLists',
)); ?>

<div class="row">
    <?= $form->labelEx($block_model, 'resourceType'); ?>
    <?= $form->textField($block_model, 'resourceType'); ?>
    <?= $form->error($block_model, 'resourceType'); ?>
</div>

<div class="row">
    <?= $form->labelEx($block_model, 'slideImageSize') ?>
    <?php $this->widget('cms.widgets.resource.ResourceSizePicker', array(
        'blockWidget' => $block_model,
        'attribute' => 'slideImageSize',
    )); ?>
    <?= $form->error($block_model, 'slideImageSize'); ?>
</div>

<div class="row">
    <?= $form->labelEx($block_model, 'thumbImageSize') ?>
    <?php $this->widget('cms.widgets.resource.ResourceSizePicker', array(
        'blockWidget' => $block_model,
        'attribute' => 'thumbImageSize',
    )); ?>
    <?= $form->error($block_model, 'thumbImageSize'); ?>
</div>

<div class="row">
    <?= $form->labelEx($block_model, 'slideView') ?>
    <?= $form->dropDownList($block_model, 'slideView', $block_model->getSlideViewsList(), array(
        'empty' => 'None',
    )); ?>
    <?= $form->error($block_model, 'slideView'); ?>
    <div class="alert alert-info">Slide view files should be in <b>view</b> directory and begin with <b>'slide_'</b></div>
</div>

<div class="row">
    <?= $form->labelEx($block_model, 'itemsInSlide') ?>
    <?= $form->numberField($block_model, 'itemsInSlide'); ?>
    <?= $form->error($block_model, 'itemsInSlide'); ?>
    <div class="alert alert-warning">Если тебе нужен этот функционал, скажи об этом Святу. Пока суппортим только один обьект на слайд.</div>
</div>

<div class="row">
    <?= $form->labelEx($block_model, 'thumbView') ?>
    <?= $form->dropDownList($block_model, 'thumbView', $block_model->getThumbViewsList(), array(
        'empty' => 'None',
    )); ?>
    <?= $form->error($block_model, 'thumbView'); ?>
    <div class="alert alert-info">Thumbnail view files should be in <b>view</b> directory and begin with <b>'thumb_'</b></div>
</div>

<div class="row">
    <?= $form->labelEx($block_model, 'title') ?>
    <?= $form->textField($block_model, 'title'); ?>
    <?= $form->error($block_model, 'title'); ?>
</div>

<div class="row">
    <?= $form->labelEx($block_model, 'titleLink') ?>
    <?= $form->textField($block_model, 'titleLink'); ?>
    <?= $form->error($block_model, 'titleLink'); ?>
</div>

<div class="row">
    <?= $form->checkBox($block_model, 'lightBox'); ?>
    <?= $form->label($block_model, 'lightBox'); ?>
    <?= $form->error($block_model, 'lightBox'); ?>
</div>
