<?php
/**
 * Class for render Content based on Content list
 *
 * @author Sviatoslav Danulenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.content_slider_sliderkit
 *
 * @see http://www.kyrielles.net/sliderkit/sliderkit_en.html
 */

Yii::import('cms.front_blocks.content_slider_sliderkit.ContentSliderBlockBase');

class ContentSliderSliderkitBlock extends ContentSliderBlockBase
{
    /**
     * @var integer $start Set the start position. First is 0.
     */
    public $start = 0;

    /**
     * @var boolean $auto activate auto scrolling
     */
    public $auto = true;

    /**
     * @var integer $autospeed Set the auto-scrolling speed (ms).
     */
    public $autospeed = 4000;

    /**
     * @var boolean $autostill If set to true, the auto-scrolling won't stop on panels mouseover.
     */
    public $autostill = false;

    /**
     * @var boolean $mousewheel Activate the mousewheel navigation.
     */
    public $mousewheel = false;

    /**
     * @var boolean $keyboard Activate the keyboard navigation. Very basic for now (left/right arrows only).
     */
    public $keyboard = false;

    /**
     * @var boolean $circular Activate the infinite nav scroll.
     */
    public $circular = false;

    /**
     * @var boolean $shownavitems Defines how many thumbnails to display in the nav clip.
     */
    public $shownavitems = 5;

    /**
     * @var boolean $navitemshover If set the panels will slide on nav thumbnails mouseover (by default panels slide on click).
     */
    public $navitemshover = false;

    /**
     * @var boolean $navclipcenter Defines if the nav clip must be center-aligned in the nav container.
     */
    public $navclipcenter = false;

    /**
     * @var boolean $navcontinuous If set to true, will make the carousel scroll continuously (use this option with a "linear" scrolleasing).
     */
    public $navcontinuous = false;

    /**
     * @var boolean $navscrollatend If set to 'true', will make the carousel scroll if a line first or last thumbnail is clicked.
     */
    public $navscrollatend = false;

    /**
     * @var boolean $navpanelautoswitch If set to 'false', will make the navbar scroll without switching panels.
     */
    public $navpanelautoswitch = true;

    /**
     * @var string $navfx Define the carousel transition effect
     */
    public $navfx = 'sliding';

    /**
     * @var integer $scroll Defines how many nav thumbnails must be scrolled when nav buttons are clicked. Can't be greater than the 'shownavitems' option value.
     * if -1 than it will be equal to 'shownavitems' option value.
     */
    public $scroll = -1;

    /**
     * @var integer $scrollspeed Set the nav scroll speed (ms).
     */
    public $scrollspeed = 600;

    /**
     * @var string $scrolleasing Add an easing effect to the nav slide transition.
     */
    public $scrolleasing = 'swing';

    /**
     * @var string $panelfx Define the panels transition effect
     */
    public $panelfx = 'fading';

    /**
     * @var integer $panelfxspeed Set the panel slide transition effect speed (ms)
     */
    public $panelfxspeed = 700;

    /**
     * @var string $panelfxeasing Add an easing effect to the panel slide transition.
     */
    public $panelfxeasing = 'swing';

    /**
     * @var string $panelfxfirst Add a transition effect on the first displayed item. Values: "fading" or "none".
     */
    public $panelfxfirst = 'none';

    /**
     * @var boolean $panelbtnshover If set to true, go buttons will fade in/out on panel mouseover
     */
    public $panelbtnshover = false;

    /**
     * @var boolean $panelclick Activate the 1-click navigation.
     */
    public $panelclick = false;

    /**
     * @var boolean $verticalnav Set the nav clip direction to vertical.
     */
    public $verticalnav = false;

    /**
     * @var boolean $verticalslide  Set the panel sliding direction to vertical (only if "panelfx" is defined as "sliding").
     */
    public $verticalslide = false;

    /**
     * @var boolean $tabs Required to build a tabs menu.
     */
    public $tabs = false;

    /**
     * @var boolean $freeheight Use panels with no fixed height (in this case, 'panelfx' value can't be "sliding").
     */
    public $freeheight = false;

    /**
     * @var boolean $fastchange  By default the user can slide to the next content even if the slider is still running. You can stop this behavior by setting the "fastchange" option to false.
     */
    public $fastchange = true;

    /**
     * @var boolean $debug If set to true, the script will stop on errors and return error code values. Check documentation.
     * @see http://www.kyrielles.net/sliderkit/sliderkit_en.html#TOC-debugmode
     */
    public $debug = false;


    // TODO: возможно добавить поддержку плагинов sliderkit


    public function params()
    {
        return CMap::mergeArray(parent::params(), array(
            'start' => _t('Start from slide number (First is 0)'),
            'auto' => _t('Enable auto slide changing'),
            'autospeed' => _t('Auto-changing speed (ms)'),
            'autostill' => _t('Do not stop auto-changing on slide hover'),
            'mousewheel' => _t('Activate the mousewheel navigation'),
            'keyboard' => _t('Activate the keyboard navigation (left/right arrows only)'),
            'circular' => _t('Activate slider rewind (auto slide to first item)'),
            'shownavitems' => _t('Thumbnails/bullets number in slider navbar'),
            'navitemshover' => _t('Change slide on thumbnail hover'),
            'navclipcenter' => _t('Center-align the thumbnails in the navbar'),
            'navcontinuous' => _t('Make the thumbnails scroll continuously (use this option with a "linear" thumbnail scrolleasing)'),
            'navscrollatend' => _t('Scroll thumbnails if a line first or last thumbnail is clicked'),
            'navpanelautoswitch' => _t('Automatically switch the slides while navbar scrolling'),
            'navfx' => _t('Thumbnail transition effect'),
            'scroll' => _t('How many thumbnails to scroll on nav button click. Can\'t be greater than number of thumbnails in navbar. -1 means equal to number of thumbnails in navbar'),
            'scrollspeed' => _t('Thumbnail transition speed (ms)'),
            'scrolleasing' => _t('Thumbnail transition easing'),
            'panelfx' => _t('Slide transition effect'),
            'panelfxspeed' => _t('Slide transition speed'),
            'panelfxeasing' => _t('Slide transition easing'),
            'panelfxfirst' => _t('Transition effect on the first displayed item'),
            'panelbtnshover' => _t('Fade in/out slide buttons on panel mouseover'),
            'panelclick' => _t('Activate the 1-click navigation.'),
            'verticalnav' => _t('Vertical slider mode'),
            'verticalslide' => _t('Set the panel sliding direction to vertical (only if "slide effect" is defined as "sliding")'),
            'tabs' => _t('Tabs menu mode'),
            'freeheight' => _t('Free slide height (can\'t be used with "slide effect" "sliding")'),
            'fastchange' => _t('Let the user scroll to the next slide when slider is in move'),
            'debug' => _t('Debug mode'),
        ));
    }

    public function getNavFxList()
    {
        return array(
            'sliding' => 'Sliding',
            'none' => 'none',
        );
    }

    public function getFxList()
    {
        return array(
            'sliding' => 'Sliding',
            'fading' => 'Fading',
            'none' => 'none',
        );
    }

    public function getEasingList()
    {
        return array(
            'swing' => 'Swing',
            'linear' => 'Linear',
        );
    }

    protected function registerSliderClientScript()
    {
        $assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets', false, -1, YII_DEBUG);
        Yii::app()->clientScript
            ->registerCoreScript('jquery')
            ->registerCssFile($assetsUrl . '/css/sliderkit-core.css')
            ->registerScriptFile($assetsUrl . '/js/external/jquery.easing.1.3.min.js')
            ->registerScriptFile($assetsUrl . '/js/sliderkit/jquery.sliderkit.1.9.2.pack.js')
            ->registerScript(__FILE__.'#'.$this->id, '
                $("#'.$this->id.'").sliderkit('.$this->getSliderJsOptions().');
            ')
            ;
    }

    protected function getSliderOptions()
    {
        return array(
            'start' => $this->start,
            'auto' => (boolean) $this->auto,
            'autospeed' => $this->autospeed,
            'autostill' => (boolean) $this->autostill,
            'mousewheel' => (boolean) $this->mousewheel,
            'keyboard' => (boolean) $this->keyboard,
            'circular' => (boolean) $this->circular,
            'shownavitems' => $this->shownavitems,
            'navitemshover' => (boolean) $this->navitemshover,
            'navclipcenter' => (boolean) $this->navclipcenter,
            'navcontinuous' => (boolean) $this->navcontinuous,
            'navscrollatend' => (boolean) $this->navscrollatend,
            'navpanelautoswitch' => (boolean) $this->navpanelautoswitch,
            'navfx' => $this->navfx,
            'scroll' => $this->scroll,
            'scrollspeed' => $this->scrollspeed,
            'scrolleasing' => $this->scrolleasing,
            'panelfx' => $this->panelfx,
            'panelfxspeed' => $this->panelfxspeed,
            'panelfxeasing' => $this->panelfxeasing,
            'panelfxfirst' => $this->panelfxfirst,
            'panelbtnshover' => (boolean) $this->panelbtnshover,
            'panelclick' => (boolean) $this->panelclick,
            'verticalnav' => (boolean) $this->verticalnav,
            'verticalslide' => (boolean) $this->verticalslide,
            'tabs' => (boolean) $this->tabs,
            'freeheight' => (boolean) $this->freeheight,
            'fastchange' => (boolean) $this->fastchange,
            'debug' => (boolean) $this->debug,
        );
    }
}
