<div class="horizontal_slider_container">
<?php 
$block_id='Block'.$this->block->block_id;
?>
	<?php if ($this->content_list != null) : ?>
		<?php 
        $content = array();   	
				foreach ($this->content_list as $id => $cont) {
          $content_list_data_provider = ContentSliderFewBlock::getContentList($cont, null , null, ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD);
          $content = array_merge($content,$content_list_data_provider);
        }
                $count=count($content);
                if ($count>=1) {
                $item_width=(int)$this->width/$count;
                $height=$this->full_height;
                $content_height=$this->full_height-$this->nav_height;
                $nav_height=$this->nav_height;
              }
  ?>       
  <?php endif;
  if ($count>=1) {
   ?>
<script>
$(document).ready(function()
{
  $("#<? echo $block_id; ?>.few-items").sliderkit({
          auto: <? if ($this->autoplay) echo 'true'; else echo 'false'; ?>  ,
          autospeed: <? echo $this->autospeed; ?>,
          circular: <? if ($this->circular) echo 'true'; else echo 'false'; ?>,
          shownavitems: <? echo $count ?>,
          panelfx: '<? echo $this->panelfx; ?>',
          panelfxspeed:<? echo $this->panelfxspeed; ?>,
          mousewheel: <? if ($this->mousewheel) echo 'true'; else echo 'false'; ?>,
          navitemshover: <? if ($this->navitemshover) echo 'true'; else echo 'false'; ?>,
          fastchange:false,
          navpanelautoswitch: false
        }); 
});
</script>
<!-- Slider Kit compatibility -->
    <script type="text/javascript" src="http://shared.webtests.in.ua/contentsldier/html5.js"></script>
    <div  id="<? echo $block_id; ?>" class="sliderkit few-items <? if ($this->cssClass!='') echo $this->cssClass; ?>" style="width: <? echo $this->width; ?>px; height: <?= $height; ?>px">
          <div class="sliderkit-panels" style="width: <? echo $this->width; ?>px; height: <?= $content_height; ?>px">
            <div class="sliderkit-news">
            <?
            $i=0;
             foreach ($content as $key => $value) {
              if ($i%$this->count==0) 
              { ?>
                <div class="sliderkit-panel" style="width: <? echo $this->width-20; ?>px; height: <?= $content_height-20; ?>px;">
               <? }
             ?>
                   <div class="slider_item"> 
                      <a href="<? echo $value->getObjectLink() ?>">
                        <? 
                        $list_current_resource=GxcHelpers::getResourceObjectFromDatabase($value,'thumbnail');
                      if (count($list_current_resource)>=1) 
                      {
                        $image=imageCropper::crop($list_current_resource);
                        echo CHtml::image($image,$value->object_name); 
                      } ?>
                    </a>
                  </div>
              <?
              $i++;
               if ($i==$this->count || $i==count($content)) 
                { ?>
                  </div>
             <? }
             ?>
            <? 
            
          } ?>
            </div>
            </div>
            <div class="sliderkit-btn sliderkit-go-btn sliderkit-go-prev"><a href="#" ><span>Previous</span></a></div>
            <div class="sliderkit-btn sliderkit-go-btn sliderkit-go-next"><a href="#" ><span>Next</span></a></div>
</div>
</div>
<? } ?>