<div name="div-block-content-<?php echo $block_model->id;?>">
<div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'refresh'),'Block[refresh]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[refresh]', $block_model->refresh); ?>
                <?php echo $form->error($model,'refresh'); ?>
            </div>
<div class="row">
                    <?php echo CHtml::label(Block::getLabel($block_model,'redirect_to_backend'),'Block[redirect_to_backend]',array('class'=>'inline')); ?>
                <?php  echo CHtml::checkBox('Block[redirect_to_backend]', $block_model->redirect_to_backend); ?>
                <?php echo $form->error($model,'redirect_to_backend'); ?>
            </div>
</div>