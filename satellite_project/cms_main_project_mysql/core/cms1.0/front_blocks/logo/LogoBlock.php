<?php
/**
 * Class for render Logo
 * 
 * @author Tuan Nguyen <voinovev@studiobanzai.com>
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.logo 
 */

class LogoBlock extends BlockWidget
{
    public $src;
    public $appendAssetsUrl = false;

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        if(empty($this->src)) {
            $this->src = settings()->get('general', 'logo');
        } else {
            if($this->appendAssetsUrl) {
                $this->src = Yii::app()->controller->layoutAssets . '/' . ltrim($this->src, '/');
            }
        }
        $this->render('output', array(
            'siteUrl' => settings()->get('general', 'site_url'),
            'logoSrc' => $this->src,
            'siteName' => settings()->get('general', 'site_name'),
            'cssClass' => $this->cssClass,
            ));
    }

    public function params()
    {
        return array(
            'src' => t('Logo image url'),
            'appendAssetsUrl' => t('Append Assets Url'),
        );
    }
}
?>