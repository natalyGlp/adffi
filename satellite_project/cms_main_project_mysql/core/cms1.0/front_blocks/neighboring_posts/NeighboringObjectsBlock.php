<?php
/**
 * Created by PhpStorm.
 * User: Prog
 * Date: 19.01.2015
 * Time: 12:55
 */

class NeighboringObjectsBlock extends BlockWidget
{
    protected $_object;
    protected $_prevObject;
    protected $_nextObject;
    public $viewPath;

    public function run()
    {
        if($this->beginCache('neighboring_posts', array('varyByParam' => true, 'varyByLanguage' => true))) {
            $this->_object = Yii::app()->controller->getObject();
            $this->selectDataForRender();
            $this->render('output',array(
                'cssClass' => $this->cssClass,
                'prevObject' => $this->_prevObject,
                'nextObject' => $this->_nextObject,
                'view'=>$this->viewPath,
            ));
            $this->endCache();
        }
    }

    public function selectDataForRender()
    {
        $params = array(
            ':status' => implode(',', GxcHelpers::applyObjectStatusCriteria()),
            ':type' => $this->_object->object_type,
            ':lang' => $this->_object->lang,
            ':date' => $this->_object->object_date,
        );

        $sqlTpl = 'object_status IN (:status) AND object_type = :type AND lang = :lang AND object_date {{operator}} :date ORDER BY object_date {{sort}}';

        $this->_nextObject = Object::model()->find(strtr($sqlTpl, array(
                '{{operator}}' => '>',
                '{{sort}}' => 'ASC',
            )), $params);

        $this->_prevObject = Object::model()->find(strtr($sqlTpl, array(
                '{{operator}}' => '<',
                '{{sort}}' => 'DESC',
            )), $params);
    }

    public function getViewsFiles()
    {
        $defaultPath = Yii::getPathOfAlias('cms.front_blocks.neighboring_posts.views');
        $themePath = Yii::getPathOfAlias('theme.front_blocks.neighboring_posts.views');

        $views = array();

        if (is_dir($defaultPath)) {
            $views = CMap::mergeArray($views, scandir($defaultPath));
        }

        if (is_dir($themePath)) {
            $views = CMap::mergeArray($views, scandir($themePath));
        }

        $views = array_filter($views, function($item) {
            return $item[0] != '.';
        });

        $views = array_map(function ($item) {
            return pathinfo($item, PATHINFO_FILENAME);
        }, $views);

        $views = array_unique($views);

        return array_combine($views, $views);
    }

    public function params()
    {
        return array(
            'viewPath' => t('View name'),
        );
    }

}
