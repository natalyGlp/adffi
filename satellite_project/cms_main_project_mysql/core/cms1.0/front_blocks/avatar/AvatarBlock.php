<?php

/**
 * Class for render form for changing Avatar
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.avatar
 */

class AvatarBlock extends BlockWidget
{
    public function run()
    {       
        if(!user()->isGuest) { 
                $this->renderContent();
        } else {
            user()->setFlash('error',t('You need to sign in before continue'));                                                            
            Yii::app()->controller->redirect(bu().'/sign-in');
        }
    }       
 
 
    protected function renderContent()
    {     
        $model=new UserAvatarForm;
        if(isset($_POST['UserAvatarForm'])) {
            $model->attributes=$_POST['UserAvatarForm'];
            $model->image=CUploadedFile::getInstance($model,'image');                                                
            if($model->validate()) {
                $path = UserAvatarForm::processUploadedImage($model->image); 
                UserAvatarForm::updateUserAvatar($path);              
            }
        }
           
        $this->render('output',array('model'=>$model));
    }
}

?>