<?php
/**
 * @var $siteUrl
 * @var $logoSrc
 * @var $siteName
 * @var $cssClass
 * @var $this->slogan
 */
?>
<div class="logo-info <?php echo $cssClass; ?>">
	<a href="<?php echo $siteUrl; ?>">
        <?php
        if(!empty($logoSrc)) {
            echo CHtml::image($logoSrc, $siteName, array(
                'title' => $siteName,
                ));
        }
        ?>
	</a>
	<span class="logo-slogan"><?php echo $this->slogan; ?></span>
</div>