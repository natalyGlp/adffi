<?php
/**
 * @var CFormModel $model
 *
 * Форма должна начинаться с $form = $this->beginForm(); и заканчиваться $this->endForm()
 * Для кнопки отправки использовать $this->submitButton() (принимает те же параметры, что и CHtml::submitButton())
 *
 * Пример поля формы:
 * <div class="row">
 *     <?php echo $form->labelEx($model,'name'); ?>
 *     <?php echo $form->textField($model,'name',
 *         array(
 *             'size' => 30, // опционально
 *             'class' => 'userform', // опционально
 *             'autoComplete' => 'off', // опционально
 *             'placeholder' => $this->createPlaceholder($model, 'name'), // опционально, вместо $form->labelEx
 *         )); ?>
 * </div>
 * <?php echo $form->error($model,'name'); ?>
 */
$form = $this->beginForm();
?>
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone'); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>
    <div class="actions">
        <?php
        echo $this->submitButton();
        ?>
    </div>
<?php
$this->endForm();
?>

