<?php
/**
 * This is the model class for Shop Callback Form.
 *
 *
 * @author Dima Mytiansky <dima@glp-centre.com>
 * @version 1.0
 * @package cms.front_blocks.shopcallback
 */

Yii::import(GxcHelpers::getTruePath('front_blocks.feedback.models.FeedbackBaseModel'));
class ShopCallbackFormModel extends FeedbackBaseModel {
    public $name;
    public $phone;
    public $email = 'callback';
    public function rules(){
        return array(
            array('name, phone','required','message'=>t('The field \'{attribute}\' can not be empty.')),
            array('phone', 'match', 'pattern' => '/^[\(\) \-0-9\+]*$/', 'allowEmpty' => true, 'message' => t('The phone number should contain only numbers, spaces and (,-,+)')),
        );
    }

    public function attributeLabels(){
        return array(
            'name'=>t('Укажите Ваше имя'),
            'phone'=>t('Укажите номер телефона'),
        );
    }

    public function emailLogLabels(){
        return array(
            'name'=>t('Имя отправителя'),
            'phone'=>t('Номер телефона'),
        );
    }
}