<?php
/**
 * HLike widget class file
 *
 * @author     Sviatoslav Danylenko <Sviatoslav.Danylenko@udf.su>
 * @package    hamster.widgets.social.HLike
 * @version    1.0
 * @copyright  Copyright &copy; 2012 Sviatoslav Danylenko (http://hamstercms.com)
 * @license    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)
 */
/**
 * HLike это враппер для лайк виджетов социальных сетей Facebook, VK, G+ и Twitter.
 * У виджета есть четыре необязательных параметра.
 *
 * Пример использования виджета
 * <pre>
 * $this->beginWidget('application.widgets.social.HLike', array(
 *   'title'=> 'название страницы', 
 *   'description' => 'описание страницы',
 *   'imgSrc' => 'абсолютный путь к изображению страницы',
 *   'vertical' => true, // Отобразит виджет вертикально. По умолчанию: false
 * ));
 * </pre>
 */

// TODO: small and standard
class HLike extends CWidget 
{
	/**
	 * @var string url ассетов
	 */
	protected $_assetsUrl;
	/**
	 * @var string $imageSrc путь к картинке материала
	 */
	public $imgSrc;
	/**
	 * @var string $description описание материала 
	 */
	public $description;
	/**
	 * @var string $title название материала 
	 */
	public $title;
	/**
	 * @var string $url url материала 
	 */
	public $url;
	/**
	 * @var boolean $vertical включает вертикальную ориентацию виджета 
	 */
	public $vertical = false;

	/**
	 * @var string $size размер социальных кнопок (small|medium|standard|big)
	 */
	public $size = 'big';

	/**
	 * @var boolean $useOG использовать Open Graph теги
	 */
	public $useOGTags = true;

	/**
	 * @var string $sharethisPubKey  ключ api sharethis.com. Если указан, то будут использоваться их виджеты
	 */
	public $sharethisPubKey = false;

	/**
	 * @var string $via twitter via
	 */
	public $twitterVia = '';
	
	/**
	 * @property integer $vkApiId api id для vkZ
	 */
	public $vkApiId = false;

	public function init() {
		if (empty($this->_assetsUrl))
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(
				dirname(__FILE__). DIRECTORY_SEPARATOR.'assets'
			);
		parent::init();
	}

	public function run() 
	{
		$cs = Yii::app()->clientScript;

		// SEO and Sociality meta
		if($this->useOGTags)
		{
			$this->registerMetaTags();
		}

		$options = array(
			'size' => $this->size,
			'vertical' => $this->vertical,
			'description' => $this->description,
			'title' => $this->title,
			'url' => $this->url,
			'hasOGTags' => $this->useOGTags,
			'image' => $this->imgSrc ? Yii::app()->getRequest()->getHostInfo() . $this->imgSrc :  null,
			);

		if($this->size == 'pie') {
			$cs->registerScriptFile('https://www.google.com/jsapi', CClientScript::POS_END);
			$cs->registerScriptFile($this->_assetsUrl.'/js/'.'socialPie.js', CClientScript::POS_END);
			$this->render('pie', array(
				'title' => $this->title,
			));
		} elseif($this->sharethisPubKey && !empty($this->sharethisPubKey)) {
			echo CHtml::openTag('section', array(
				'class' => 'hlike',
				));

			$options['pubKey'] = $this->sharethisPubKey;
			echo self::stButtons($options);

			echo CHtml::closeTag('section');
		} else {
			$this->registerClientScript();

			echo CHtml::openTag('section', array(
				'class' => 'hlike',
				));

			echo self::twButton(CMap::mergeArray($options, array(
					'via' => $this->twitterVia,
				)));
			echo self::fbButton($options);
			echo self::gButton($options);
			echo self::vkButton(CMap::mergeArray($options, array(
					'apiId' => $this->vkApiId ? $this->vkApiId : Yii::app()->params['vkApiId'],
				)));

			echo CHtml::closeTag('section');
		}
	}

	public static function stButtons($options)
	{
		$scriptOptions = array(
			'publisher' => $options['pubKey'],
			'popup' => false,
			'doNotHash' => true,
			'doNotCopy' => true,
			'hashAddressBar' => false,
			);

		Yii::app()->clientScript->registerScriptFile('http://w.sharethis.com/button/buttons.js', CClientScript::POS_END)
			->registerScript(__FILE__.'#shareThisInit', 'stLight.options('.CJavaScript::encode($scriptOptions).');');

		$stConfig = array(
				array(
					'class' => 'st_vkontakte_hcount',
					'displayText' => 'Vkontakte',
				),
				array(
					'class' => 'st_facebook_hcount',
					'displayText' => 'Facebook',
				),
				array(
					'class' => 'st_twitter_hcount',
					'displayText' => 'Tweet',
				),
				array(
					'class' => 'st_googleplus_hcount',
					'displayText' => 'Google +',
				),
			);

		ob_start();
		foreach ($stConfig as $htmlOptions) {
			self::mapOptions(array(
				'url' => 'st_url',
				'description' => 'st_summary',
				'title' => 'st_title',
				'image' => 'st_image',
				), $options, $htmlOptions);

			echo CHtml::tag('span', $htmlOptions, '');

			if($options['vertical']) {
				echo '<br>';
			}
		}

		return ob_get_clean();
	}

	public static function gButton($options)
	{
		switch($options['size'])
		{
			case 'big':
				$size = 'tall';
				break;
			case 'standard':
				$size = 'standard';
				break;
			case 'medium':
				$size = 'medium';
				break;
			case 'small':
				$size = 'medium';
				// $size = 'small';
				break;
		}

		$htmlOptions = array(
			'class' => 'g-plusone',
			'data-action' => 'share',
			'data-size' => $size,
			);

		if(!$options['hasOGTags'])
		{
			self::mapOptions(array(
				'url' => 'data-href',
				), $options, $htmlOptions);
		}

		return CHtml::tag('div', $htmlOptions, '');
	}

	public static function fbButton($options)
	{
		switch($options['size'])
		{
			case 'big':
				$size = 'box_count';
				break;
			case 'standard':
				$size = 'button_count';
				break;
			case 'medium':
				$size = 'button_count';
				break;
			case 'small':
				$size = 'button_count';
				break;
		}

		$htmlOptions = array(
			'class' => 'fb-like',
			'data-action' => 'like',
			'data-show-faces' => 'false',
			'data-share' => 'false',
			'data-layout' => $size,
			);

		if(!$options['hasOGTags'])
		{
			self::mapOptions(array(
				'url' => 'data-href',
				), $options, $htmlOptions);
		}
		

		return CHtml::tag('div', $htmlOptions, '');
	}

	public static function twButton($options)
	{
		switch($options['size'])
		{
			case 'big':
				list($dataSize, $dataCount) = array('vertical', 'vertical');
				break;
			case 'standard':
				list($dataSize, $dataCount) = array('large', null);
				break;
			case 'medium':
				list($dataSize, $dataCount) = array(null, null);
				break;
			case 'small':
				list($dataSize, $dataCount) = array(null, null);
				break;
		}

		$htmlOptions = array(
			'href' => 'https://twitter.com/share',
			'class' => 'twitter-share-button',
			'data-size' => $dataSize,
			'data-count' => $dataCount,
			'data-via' => $options['via'],
			);

		if(!$options['hasOGTags'])
		{
			self::mapOptions(array(
				'url' => 'data-url',
				'title' => 'data-text',
				), $options, $htmlOptions);
		}

		return CHtml::tag('a', $htmlOptions, '');
	}

	public static function vkButton($options)
	{
		$id = 'vklike' . uniqid();
		$scriptId = __CLASS__;
		$selector = '.vk-like';

		switch($options['size'])
		{
			case 'big':
				$size = 'vertical';
				break;
			case 'standard':
				$size = 'button';
				break;
			case 'medium':
				$size = 'mini';
				break;
			case 'small':
				$size = 'mini';
				break;
		}

		$htmlOptions = array(
			'class' => 'vk-like',
			'id' => $id,
			);

		$apiOptions = array(
			'type' => $size,
			'height' => 20,
			);

		if(!$options['hasOGTags'])
		{
			// кнопка с личными настройками
			self::mapOptions(array(
				'description' => 'pageDescription',
				'url' => 'pageUrl',
				'title' => 'pageTitle',
				'image' => 'pageImage',
				), $options, $apiOptions);

			$selector = "#$id";
			$scriptId .= $id;
		}
			
		if(!empty($options['apiId'])) {
			$apiOptions = CJavaScript::encode($apiOptions);

			Yii::app()->clientScript
				->registerCoreScript('jquery')
				->registerScript($scriptId, "\$('$selector').hvklike($apiOptions);")
				->registerMetaTag($options['apiId'], NULL, NULL, array('property' => 'vk:app_id'))
				;
		}

		return CHtml::tag('div', $htmlOptions, '');
	}

	protected static function mapOptions($map, $source, &$dest)
	{
		foreach ($map as $from => $to) 
		{
			if(isset($source[$from]) && !empty($source[$from]))
				$dest[$to] = $source[$from];
		}
	}

	protected function registerMetaTags()
	{
		$cs = Yii::app()->clientScript;

		if(isset($this->description))
		{
			$desc = strip_tags(mb_substr($this->description, 0, 200, 'UTF-8'));
			$cs->registerMetaTag($desc, 'description');
			$cs->registerMetaTag($desc, NULL, NULL, array('property' => 'og:description'));
		}

		if(isset($this->title))
			$cs->registerMetaTag($this->title, NULL, NULL, array('property' => 'og:title'));

		if(isset($this->url))
			$cs->registerMetaTag('Ссылка на материал', NULL, NULL, array('property' => 'og:url'));

		$cs->registerMetaTag(Yii::app()->name, NULL, NULL, array('property' => 'og:site_name'));

		if(isset($this->imgSrc))
		{
			$imgSrc = Yii::app()->getRequest()->getHostInfo() . $this->imgSrc;
			$cs->registerMetaTag($imgSrc, NULL, NULL, array('property' => 'og:image'));
			$cs->registerLinkTag('image_src', NULL, $imgSrc);
		}
	}

	protected function registerClientScript(){
		$cs = Yii::app()->clientScript;
		$scriptFile = YII_DEBUG ? 'social.js' : 'social.min.js';
		$cs->registerScriptFile($this->_assetsUrl.'/js/'.$scriptFile, CClientScript::POS_END);
		$cs->registerCss(__FILE__, '
			.hlike > div, .hlike > iframe {
				vertical-align: '.($this->size == 'big' ? 'bottom' : 'middle').' !important;
				font-size: 1px !important; 
				display: '.($this->vertical ? '' : 'inline-').'block !important;
				'.($this->vertical ? '' : 'margin-right: 5px !important;').'
			}
			.hlike [id^="___plusone"]
			{
				'.($this->size == 'big' ? '' : 'width: 58px !important;').'
			}
			.twitter-share-button
			{
				'.($this->size == 'big' ? '' : 'width: 76px !important;').'
			}
		');
	}
}