<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, "widgetSize"), ""); ?>
        <?php echo CHtml::dropdownList("Block[widgetSize]", $block_model->widgetSize, array(
            'standard' => 'Horizontal counter',
            'big' => 'Vertical counter',
            'pie' => 'Pie',
            'medium' => 'Medium',
            'small' => 'Small',
        ), array("id" => "Block-widgetSize")); ?>
        <?php echo $form->error($model, "widgetSize"); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, "widgetOrientation"), ""); ?>
        <?php echo CHtml::dropdownList("Block[widgetOrientation]", $block_model->widgetOrientation, array(
            'false' => 'Horizontal',
            'vertical' => 'Vertical',
        ), array("id" => "Block-widgetOrientation")); ?>
        <?php echo $form->error($model, "widgetOrientation"); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'sharethisPubKey'), ''); ?>
        <?php echo CHtml::textField("Block[sharethisPubKey]", $block_model->sharethisPubKey, array('id' => 'Block-sharethisPubKey')); ?>
        <?php echo $form->error($model, 'sharethisPubKey'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_fb'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_fb]", $block_model->need_fb, array('id' => 'Block-need_fb')); ?>
        <?php echo $form->error($model, 'need_fb'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_vk'), 'Block-need_vk'); ?>
        <?php echo CHtml::checkBox("Block[need_vk]", $block_model->need_vk, array('id' => 'Block-need_vk')); ?>
        <?php echo $form->error($model, 'need_vk'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_gp'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_gp]", $block_model->need_gp, array('id' => 'Block-need_gp')); ?>
        <?php echo $form->error($model, 'need_gp'); ?>
    </div>   

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_tw'), ''); ?>
        <?php echo CHtml::checkBox("Block[need_tw]", $block_model->need_tw, array('id' => 'Block-need_tw')); ?>
        <?php echo $form->error($model, 'need_tw'); ?>
    </div>   
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, "twitterVia"), ""); ?>
        <?php echo CHtml::textField("Block[twitterVia]", $block_model->twitterVia, array("id" => "Block-twitterVia")); ?>
        <?php echo $form->error($model, "twitterVia"); ?>
    </div> 
</div>
