<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title'), ''); ?>
        <?php echo CHtml::textField("Block[title]", $block_model->title, array('id' => 'Block-title')); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::checkBox("Block[need_fb]", $block_model->need_fb, array('id' => 'Block-need_fb')); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_fb'), ''); ?>
        <?php echo $form->error($model, 'need_fb'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::checkBox("Block[need_vk]", $block_model->need_vk, array('id' => 'Block-need_vk')); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_vk'), 'Block-need_vk'); ?>
        <?php echo $form->error($model, 'need_vk'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::checkBox("Block[need_gp]", $block_model->need_gp, array('id' => 'Block-need_gp')); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_gp'), ''); ?>
        <?php echo $form->error($model, 'need_gp'); ?>
    </div>   

    <div class="row">
        <?php echo CHtml::checkBox("Block[need_tw]", $block_model->need_tw, array('id' => 'Block-need_tw')); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_tw'), ''); ?>
        <?php echo $form->error($model, 'need_tw'); ?>
    </div>  

    <div class="row">
        <?php echo CHtml::checkBox("Block[need_od]", $block_model->need_od, array('id' => 'Block-need_od')); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'need_od'), ''); ?>
        <?php echo $form->error($model, 'need_od'); ?>
    </div>  
</div>