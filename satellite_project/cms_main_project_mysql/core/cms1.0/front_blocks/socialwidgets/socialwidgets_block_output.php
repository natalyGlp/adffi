<?php
$namespace = 'socialwidgets';
$id = $this->id;
// TODO: .tab_btn - DEPRECATED
// TODO: .widgets_content - DEPRECATED
// TODO: .widget_tab_content - DEPRECATED
// TODO: #XX-btn-w - DEPRECATED
?>
<div id="<?php echo $id ?>" <?php echo !empty($this->cssClass) ? " class=\"" . $this->cssClass . "\"" : ""; ?>>
    <?php
    if (!empty($this->title)) {
        echo '<div class="title"><h2>' . $this->title . '</h2></div>';
    }
    ?>
    <?php
        $content = '';
        $checkingMap = array(
            'need_fb' => array(
                'setting' => 'facebook_widget',
                'label' => 'Facebook',
                'id' => 'fb'
                ),
            'need_vk' => array(
                'setting' => 'vkontakte_widget',
                'label' => 'Vkontakte',
                'id' => 'vk'
                ),
            'need_gp' => array(
                'setting' => 'gplus_widget',
                'label' => 'Google+',
                'id' => 'gp'
                ),
            'need_tw' => array(
                'setting' => 'twitter_widget',
                'label' => 'Twitter',
                'id' => 'tw'
                ),
            'need_od' => array(
                'setting' => 'odnoklassniki_widget',
                'label' => 'Odnoklassniki',
                'id' => 'od'
                ),
            );

        echo '<div class="socialwidgets-tabs">';
        foreach ($checkingMap as $key => $params) {
            if ($this->$key){ 
                $line = trim(settings()->get('social', $params['setting']));
                if ($line){
                    $wId = $id . '-' . $params['id'];
                    echo CHtml::link($params['label'], '#'.$wId, array(
                        'id' => $params['id'] . '-btn-w',
                        'class' => 'btn-tab btn-tab-'.$params['id'].' tab_btn',
                        ));
                    $content .= '<div id="'.$wId.'" class="socialwidgets-tab-content widget_tab_content">'.$line.'</div>';
                }
            }
        }
        echo '</div>';
    ?>
    <div class="socialwidgets-content widgets_content"><?php echo $content;?></div>
</div>
<?php
    Yii::app()->clientScript->registerScript(__FILE__, '
        $("#'.$id.'").on("click", ".btn-tab", function(e) {
            e.preventDefault();

            $(this).parents("#'.$id.'").find(".btn-tab, .socialwidgets-tab-content").removeClass("active");
            $(this).addClass("active");
            $($(this).attr("href")).addClass("active");
        });
        $(".btn-tab:first-child").click();
        ');
?>