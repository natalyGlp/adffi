<?php
if($this->beginCache('block'.$this->getId())):
?>
<div<?php echo($this->cssClass) ? " class=\"" . $this->cssClass . "\"" : ""; ?>>
    <ul>
        <?php
        $line = trim($this->social_email);
        if ($line)
            echo '<li id="soc-e-mail"><a target="blank" href="maito:' . $line . '">'.$line. '</a></li>';

        $attributes = array(
            'facebook' => 'need_fb',
            'gplus' => 'need_gp',
            'vkontakte' => 'need_vk',
            'youtube' => 'need_yt',
            'odnoklassniki' => 'need_od',
            'skype' => 'need_sk',
            'linkedin' => 'need_ld',
            'twitter' => 'need_tw',
            'vimeo' => 'need_vi',
            'rss' => 'need_rss',
            'instagram' => 'need_in',
            'dribbble' => 'need_dr',
            'pinterest' => 'need_pr',
            'livejournal' => 'need_lj',
            );

        foreach ($attributes as $snName => $attribute) {
            if ($this->$attribute) {
                $href = trim(settings()->get('social', $snName));
                if (!empty($href))
                    echo '<li class="bt-'.$snName.'"><a target="_blank" href="' . $href . '">' . '</a></li>';
            }
        }
        ?>        
    </ul>
</div>
<?php
$this->endCache();
endif;
?>
