<?php
/**
 * Class for render SocialButtons * 
 * 
 * @author Bazai best programmer <developer@studiobanzai.com>
 * @version 0.1
 * @package front_blocks.socialbuttons 
 */

class SocialButtonsBlock extends BlockWidget
{
    public $need_fb;
    public $need_vk;
    public $need_gp;
    public $need_yt;
    public $need_tw;
    public $need_sk;
    public $need_ld;
    public $need_od;
    public $need_vi;
    public $need_rss;
    public $need_in;
    public $need_dr;
    public $need_pr;
    public $need_lj;
    public $social_email;
    
    public function run()
    {                 
       $this->renderContent();         
    }       
 
 
    protected function renderContent()
    {     
        $this->render('output',array());                                                          	       		     
    }

    public function params()
    {
        return array(
            'need_fb' => t('Show Facebook button'),
            'need_vk' => t('Show Vkontakte button'),
            'need_gp' => t('Show Gmail button'),
            'need_yt' => t('Show Youtube button'),
            'need_sk' => t('Show Skype button'),
            'need_ld' => t('Show LinkedIn button'),
            'need_tw' => t('Show Twitter button'),
            'need_od' => t('Show Odnoklassniki button'),
            'need_vi' => t('Show vimeo button'),
            'need_rss' => t('Show rss button'),
            'need_in' => t('Show instagram button'),
            'need_dr' => t('Show Dribbbble button'),
            'need_pr' => t('Show Pinterest button'),
            'need_lj' => t('Show livejournal button'),
            'social_email' => t('Show this Email in buttons'),
            );
    }
}

?>