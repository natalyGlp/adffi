<?php if (isset($menus)):?>
      <menu id="menu<? echo $this->block->block_id; ?>" class="<?php echo $this->getAlign().' '.$this->cssClass?>">
      	<script>
      		$(document).ready(function()
      		{
      			var image= $('#menu<? echo $this->block->block_id; ?>').find('a');
      			$('.tumbnail-area img').attr('src',$(image[0]).find('span').attr('data-link'));
      			$('#menu<? echo $this->block->block_id; ?> a').hover(
      			function(){
      				$('.tumbnail-area img').attr('src',$(this).find('span').attr('data-link'));
      			},function()
      			{
      				$('.tumbnail-area img').attr('src',$(image[0]).find('span').attr('data-link'));
      			}	)
      		});
      	</script>
		<div class="tumbnail-area">
			<img src="" alt="" />
		</div>
			<? if ($this->title!='')
			{?>
				<h3 class="menu_title"><?  if ($this->title_href=='') echo $this->title; else echo CHtml::link($this->title,$this->title_href); ?> </h3>
			<? } ?>
            <ul class="menu_level_1">
	        	<?php foreach ($menus as $menu) :
	        	?>
	          		<li><a href="<?php echo $menu['link'];?>" <?php echo isset($_GET['slug'])&&$_GET['slug']==$menu['link']?'class="active"':'';?>><?php echo $menu['name'];?>
	          		<?php
	          			if ($menu['type']==Menu::TYPE_CONTENT)
	          			{
	          				$item=Object::model()->findByPk($menu['value']);
	          				$list_current_resource=GxcHelpers::getResourceObjectFromDatabase($item,'thumbnail');
	          			}
                        if (count($list_current_resource)>=1)
                        {
                          $link= $list_current_resource[0]['link'] ;
                          echo '<span data-link="'.$link.'"></span>';
                        }	?>

	          		</a> <?
	          			$second_level_menus=MenuBlock::getMenuItems($menu['id'],$this->menu_id);
						if(count($second_level_menus)>0) : ?>
						<ul class="menu_level_2">
							<?php foreach ($second_level_menus as $second_menu) : ?>
									<li><a href="<?php echo $second_menu['link'];?>"><?php echo $second_menu['name'];?></a>
										<?php
					          			$third_level_menus=MenuBlock::getMenuItems($second_menu['id'],$this->menu_id);
										if(count($third_level_menus)>0) : ?>
										<ul class="menu_level_3">
											<?php foreach ($third_level_menus as $third_menu) : ?>
												<li><a href="<?php echo $third_menu['link'];?>"><?php echo $third_menu['name'];?></a></li>
											<?php endforeach; ?>
										</ul>
										<?php endif; ?>
									</li>
							<?php endforeach; ?>
						</ul>
						<?php endif; ?>
					</li>
	          	<?php endforeach;?>
	        </ul>
      <br style="clear: left">
      </menu>
<?php endif;?>
