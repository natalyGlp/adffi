<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php
        Yii::import('common.front_blocks.menu.*');
         echo CHtml::label(Block::getLabel($block_model, 'menu_id'), ''); ?>
        <?php
        echo CHtml::dropDownList("Block[menu_id]", $block_model->menu_id, MenuBlock::findMenu(), array(
            'id' => 'Block-menu_id',
        ));
        ?>
        <?php echo $form->error($model, 'menu_id'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'menu_align'), '');?>
        <?php
        echo CHtml::dropDownList("Block[menu_align]",
                                $block_model->menu_align,
                                Menu::getMenuAlignType(),
                                array('id' => 'Block-menu_align')
                                );
        ?>
        <?php echo $form->error($model, 'menu_align'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title'), ''); ?>
        <?php echo CHtml::textField("Block[title]",
                                    $block_model->title,
                                    array('id' => 'Block-title')
                                    );
        ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title_href'), ''); ?>
        <?php echo CHtml::textField("Block[title_href]",
                                    $block_model->title_href,
                                    array('id' => 'Block-title_href')
                                    );
        ?>
        <?php echo $form->error($model, 'title_href'); ?>
    </div>

</div>