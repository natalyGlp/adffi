<div name="div-block-content-<?php echo $block_model->id;?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model,'code'), ''); ?>
        <?php echo CHtml::textArea("Block[code]",$block_model->code ,array(
            'rows'=>15, 
            'cols'=>50, 
            'style'=>'width:90%'
        )); ?>
        <?php echo $form->error($model,'code'); ?>
    </div>
    <div class="alert alert-info">
        <ul>
            <li><b>{orderId}</b> - id заказа</li>
            <li><b>{orderPrice}</b> - сумма заказа в основной валюте</li>
            <li><b>{orderStatus}</b> - статус заказа (success или fail)</li>
            <li><b>{orderCurrency}</b> - статус заказа (success или fail)</li>
        </ul>
    </div>
</div>
