
<div<?php echo($this->cssClass)?' class="'.$this->cssClass.'"':''; ?>>
    <form action="<?php echo $this->path?>">
    	<?php
    	echo CHtml::textField('q', isset($_GET['q']) ? $_GET['q'] : '' /*$this->holderText*/, array(
            'placeholder' => CHtml::encode($this->holderText),
            'autocomplete' => 'off',
    		// 'onfocus' => "if(this.value == " . CJavaScript::encode($this->holderText) . ") this.value = '';",
    		// 'onblur' => "if(this.value == '') this.value = " . CJavaScript::encode($this->holderText) . ";",
    		// 'id' => 'searchQuery'.$this->getId(),
    		));
    	?>
        <button type="submit" class="<?php echo $this->cssClass?>Button"><?php echo $this->buttonText ?></button>
    </form>
</div>