<div name="div-block-content-<?php echo $block_model->id;?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'path'), ''); ?>
        <?php echo CHtml::textField("Block[path]",
                                     $block_model->path,
                                     array('id' => 'Block-path')
                                   );
        ?>
        <?php echo $form->error($model, 'path'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'buttonText'), ''); ?>
        <?php echo CHtml::textField("Block[buttonText]",
                                     $block_model->buttonText,
                                     array('id' => 'Block-buttonText')
                                   );
        ?>
        <?php echo $form->error($model, 'buttonText'); ?>
        
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'holderText'), ''); ?>
        <?php echo CHtml::textField("Block[holderText]",
                                     $block_model->holderText,
                                     array('id' => 'Block-holderText')
                                   );
        ?>
        <?php echo $form->error($model, 'holderText'); ?>
    </div>
</div>