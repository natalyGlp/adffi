<?php
/**
 * Блок для отображения фильтра товаров
 * 
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.shop_filter
 */
Yii::import('cms.models.catalog.*');

class ShopFilterBlock extends BlockWidget
{
	/**
	 * @var string $filterTipAlign выравнивание подсказок в фильтре (right|left)
	 */
	public $filterTipAlign = 'right';

	/**
	 * @var integer $filterSchemaId id схемы фильтров. 
	 * Таким образом я назвал id модель FiltersOptions, так как название этой модели малость сбивает с толку
	 */
	public $filterSchemaId = null;

	protected $assetsUrl;

	public function init()
	{
		parent::init();

		if(!(Yii::app()->request->isAjaxRequest || isset($_GET['ajax']))) 
		{
			$this->registerScripts();
		}
	}

	public function run()
	{
		if($this->filterSchemaId) {
			$filterSchema = FiltersOptions::model()->findByPk($this->filterSchemaId);
			if($filterSchema) {
				// убираем GET запрос из url (если такой был)
				$action = preg_replace('/\?[^\?]*$/','',$_SERVER["REQUEST_URI"]);


				$brands = $filterSchema->brands;
				$brandList = array();
				if($brands && count($brands))
				{		
					// фильтр по брендам
					$brandList = CHtml::listData($brands, 'object_id', 'object_name');
					if(!isset($_GET['BF'])) $_GET['BF'] = array_values(array_flip($brandList));
				}

				if(isset($_GET['PF']))
				{
					$filterSchema->priceMin = $_GET['PF'][0];
					$filterSchema->priceMax = $_GET['PF'][1];
				}

				
     
     //  $dataProvider=new CActiveDataProvider('CharShema', array(
     //      'criteria'=>array(
     //        'condition'=>'t.cat_id IN(' . $catIds . ')', 
     //      ),
	    //   )
	    // );

				$this->render('output',array(
					'action' => $action, 
					'brands' => $brands,
					'brandList' => $brandList,
					'blockId' => $this->getId(),
					'filterSchema' => $filterSchema,
					));
			}
		}
	}

	public function params(){
		return array(
			'filterSchemaId' => t('Choose Filter Schema'),
			'filterTipAlign' => t('Filter Tip Align'),
			);
	}

	protected function registerScripts()
	{
		// регестрируем assets
		$this->assetsUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__).DIRECTORY_SEPARATOR.'assets',false,-1,YII_DEBUG);
		$this->registerCssFile('tip-twitter.css');
		$this->registerScriptFile('jquery.poshytip.min.js');
				
		// Ajax 0бновление формы
		$js = '
			var tipsArr = [];
			// при каждой отправке ajax прячем все всплывающие подсказки
			$("body").bind("hideTips", function(){
				while(tipSelector = tipsArr.shift())
				tipSelector.poshytip("hide");
			});
		
			$("body").on("change", "#ShopFilter'.$this->getId().' input", function() {
				var form = $(this).parents("form");
				// Инициализируем подсказку
				var tipSelector = ($(this).is("[type=checkbox]") || $(this).is("[type=radio]")) ? $(this).next("label") : $(this).parent();
				// уничтожаем подсказки этого элемента (на тот случай, если они еще не уничтожены)
				tipSelector.poshytip("destroy");
				tipSelector.poshytip({
					className: "tip-twitter",
					showOn: "none",
					alignTo: "target",
					alignX: "' . $this->filterTipAlign . '",
					alignY: "center",
					offsetX: 10
				});
				
				$.ajax({
					url: form.prop("action"),
					data: form.serialize() + "&ajax=1",
					type: "POST",
					context: $(this),
					success: function (data) {
						// инициируем событие для скрытия подсказок
						$("body").trigger("hideTips");
						tipsArr.push(tipSelector); // добавляем обьект текущей подсказки в массив с активными подсказками                                                                                                                     
						
						tipSelector.poshytip("update", data);
						tipSelector.poshytip("show");
						
					},
				});
			});
		';
		Yii::app()->getClientScript()->registerScript(__CLASS__.'#'.$this->id, $js);
	}

	protected function registerScriptFile($fileName,$position=CClientScript::POS_END)
	{
		Yii::app()->getClientScript()->registerScriptFile($this->assetsUrl.'/js/'.$fileName,$position);
	}

	protected function registerCssFile($fileName)
	{
		Yii::app()->getClientScript()->registerCssFile($this->assetsUrl.'/css/'.$fileName);
	}
}
