<div name="div-block-content-<?php echo $block_model->id;?>">
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'filterSchemaId'),''); ?>
		<?php echo CHtml::dropDownList("Block[filterSchemaId]", $block_model->filterSchemaId, FiltersOptions::getList()); ?>
		<?php echo $form->error($model,'filterSchemaId'); ?>
	</div> 
</div>
