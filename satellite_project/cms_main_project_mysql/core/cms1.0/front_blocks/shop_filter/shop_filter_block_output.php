<?php 
/**
 * @var string $action action аттрибут формы
 * @var array $brands массив моделей Object брендов
 * @var array $brandList список брендов для формы
 * @var integer $blockId id блока
 * @var FiltersOptions $filterSchema
 */

?>
<div id="ShopFilter<?php echo $blockId ?>" class="<?php echo $this->cssClass ?>">
	<?php
		echo CHtml::beginForm($action, 'GET');

		if($filterSchema->minPriceVal != $filterSchema->maxPriceVal) 
		{
			// фильтр по цене
			echo '<div class="filterItem">';
			echo '<h4>Цена</h4>';
			// Переопределяем атрибуты модели данными из гет запроса
			$this->widget('cms.extensions.jui.HFilterRangeSlider', array(
				'model'=>$filterSchema,
				'attribute'=>'priceMin',
				'maxAttribute'=>'priceMax',
				'template' => '{slider}{value}{value2}<br />{label}{label2}',
				'name'=>'PF',
				// additional javascript options for the slider plugin
				'options'=>array(
					'range'=>true,
					'min'=>$filterSchema->minPriceVal,
					'max'=>$filterSchema->maxPriceVal,
				),
				'htmlOptions' => array(
					'name' => 'PF[]',
				),
			));
			echo '</div>';
		}

		$filterSchemaOptions = $filterSchema->options;
		for($i = 1; $i <=11; $i++)
		{
			$key = 'filter'.$i;
			$type = $filterSchemaOptions[$key];
			$name = $filterSchema->$key;
			// Пропускаем поля типов: Text, One in taxonomy list, Many in taxonomy list, Calendar, Year calendar. One in model list
			if(in_array($type, array(0, 3, 4, 6, 7, 9)) || empty($name)) continue;
			echo '<h4>' . $name . '</h4>';
			echo '<div class="filterItem">';
			if($type == 1 || $type == 2)
			{ // поля типа Many in list (select/checkbox) и One in list (radio)
				$chtmlMethodName = $type == 1 ? 'radioButtonList' : 'checkBoxList';
				// для характеристик типа Many in list (checkbox) делаем отдельный элемент в массиве запроса
				// так как там нужно обеспечить сравнения по типу LIKE
				if($type == 2) {
					$inputName = "CF[m][" . $key . "]";
					$inputValues = isset($_GET['CF']['m'][$key]) ? $_GET['CF']['m'][$key] : null;
				}else{
					$inputName = "CF[" . $key . "]";
					$inputValues = isset($_GET['CF'][$key]) ? $_GET['CF'][$key] : null;
					// TODO: заменить на параметр $htmlOptions['empty'] когда проапдейтимся на Yii 1.1.14
					$filterSchemaOptions['select_options'][$key] = array('' => 'Не важно') + $filterSchemaOptions['select_options'][$key];
				}
				echo CHtml::$chtmlMethodName($inputName, $inputValues, $filterSchemaOptions['select_options'][$key]);
			}

			if($type == 5)
			{ // One in brand list
				echo CHtml::checkBoxList("BF[]", $_GET['BF'], $brandList);
			}
			
			if($type == 8)
			{ // числовые поля типа Range, отображаются слайдерами
				// Переопределяем атрибуты модели данными из гет запроса
				$min = $max = null;
				if(isset($_GET['CNF'][$key]))
				{
					$min = $_GET['CNF'][$key][0];
					$max = $_GET['CNF'][$key][1];
				}
				// $charModel->char_id = $model->char_id;
				$this->widget('cms.extensions.jui.HFilterRangeSlider', array(
					'model'=> $filterSchema,
					'value'=> $min,
					'valueLabel' => t('From'),
					'value2'=> $max,
					'value2Label' => t('To'),
					'template' => '{slider}{value}{value2}<br />{label}{label2}',
					'name'=>'CNF[' . $key . ']',
					// additional javascript options for the slider plugin
					'options'=>array(
						'range'=>true,
						'min'=> $filterSchema->getMinValue($key),
						'max'=> $filterSchema->getMaxValue($key),
					),
					'htmlOptions' => array(
						'name' => 'CNF[' . $key . '][]',
					),
				));
			}

			echo '</div>';
		}

		echo CHtml::hiddenField('FID', $filterSchema->primaryKey);
		echo '<p>' . CHtml::submitButton('Применить', array('name'=>''));
		echo '<br />' . CHtml::button('Сброс', array('onclick'=>'location.href=' . CJavaScript::encode($action))) . '</p>';
		echo CHtml::endForm();
	?>
</div>
