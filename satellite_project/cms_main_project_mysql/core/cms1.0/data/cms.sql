-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 13 2014 г., 17:45
-- Версия сервера: 5.5.31-0ubuntu0.13.04.1
-- Версия PHP: 5.4.9-4ubuntu2.2

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


--
-- Структура таблицы `gxc_albums`
--

CREATE TABLE IF NOT EXISTS `gxc_albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `path` varchar(255) NOT NULL,
  `galleries` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_albums_galleries`
--

CREATE TABLE IF NOT EXISTS `gxc_albums_galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_album` int(11) NOT NULL,
  `id_gallery` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_album` (`id_album`),
  KEY `id_gallery` (`id_gallery`),
  KEY `id_gallery_2` (`id_gallery`),
  KEY `id_gallery_3` (`id_gallery`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `gxc_auth_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gxc_auth_assignment`
--

INSERT INTO `gxc_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('Admin', '1', NULL, 'N;'),
('Reporter', '2', NULL, 'N;'),
('Admin', '7', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_auth_item`
--

CREATE TABLE IF NOT EXISTS `gxc_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gxc_auth_item`
--

INSERT INTO `gxc_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('Guest', 2, 'Guest', 'return Yii::app()->user->isGuest;', 'N;'),
('Authenticated', 2, 'Authenticated', 'return !Yii::app()->user->isGuest;', 'N;'),
('Admin', 2, NULL, NULL, 'N;'),
('Reporter', 2, 'Reporter', NULL, 'N;'),
('Besite.*', 1, NULL, NULL, 'N;'),
('Besite.Index', 0, NULL, NULL, 'N;'),
('Besite.Error', 0, NULL, NULL, 'N;'),
('Besite.Login', 0, NULL, NULL, 'N;'),
('Besite.Logout', 0, NULL, NULL, 'N;'),
('Editor', 2, 'Editor', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `gxc_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gxc_auth_item_child`
--

INSERT INTO `gxc_auth_item_child` (`parent`, `child`) VALUES
('Authenticated', 'Besite.Error'),
('Authenticated', 'Besite.Login'),
('Authenticated', 'Besite.Logout'),
('Guest', 'Besite.Error'),
('Guest', 'Besite.Login'),
('Guest', 'Besite.Logout'),
('Reporter', 'Besite.Error'),
('Reporter', 'Besite.Index'),
('Reporter', 'Besite.Login'),
('Reporter', 'Besite.Logout');

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_autologin_tokens`
--

CREATE TABLE IF NOT EXISTS `gxc_autologin_tokens` (
  `user_id` bigint(20) NOT NULL,
  `token` char(40) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`user_id`,`token`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_block`
--

CREATE TABLE IF NOT EXISTS `gxc_block` (
  `block_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT '',
  `created` int(11) DEFAULT '0',
  `creator` bigint(20) NOT NULL,
  `updated` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`block_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Структура таблицы `gxc_catalog_attributes`
--

CREATE TABLE IF NOT EXISTS `gxc_catalog_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_object` int(11) NOT NULL,
  `id_attributes` int(11) NOT NULL,
  `data` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_object` (`id_object`,`id_attributes`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_catalog_attributes_categories`
--

CREATE TABLE IF NOT EXISTS `gxc_catalog_attributes_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `pos` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_catalog_attributes_options`
--

CREATE TABLE IF NOT EXISTS `gxc_catalog_attributes_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `options` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `columns`
--

CREATE TABLE IF NOT EXISTS `gxc_columns` (
  `id` varchar(100) NOT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_comment`
--

CREATE TABLE IF NOT EXISTS `gxc_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_model` varchar(64) NOT NULL,
  `parent_guid` varchar(16) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `author_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`),
  KEY `parent_model` (`parent_model`),
  KEY `parent_guid` (`parent_guid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_content_list`
--

CREATE TABLE IF NOT EXISTS `gxc_content_list` (
  `content_list_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (`content_list_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_filters`
--

CREATE TABLE IF NOT EXISTS `gxc_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_object` int(11) NOT NULL,
  `id_options` int(11) NOT NULL,
  `price` decimal(19,2) NOT NULL,
  `filter1` text,
  `filter2` text,
  `filter3` text,
  `filter4` text,
  `filter5` text,
  `filter6` text,
  `filter7` text,
  `filter8` text,
  `filter9` text,
  `filter10` text,
  `filter11` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_object` (`id_object`),
  KEY `id_options` (`id_options`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_filters_options`
--

CREATE TABLE IF NOT EXISTS `gxc_filters_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `filter1` varchar(255) DEFAULT NULL,
  `filter2` varchar(255) DEFAULT NULL,
  `filter3` varchar(255) DEFAULT NULL,
  `filter4` varchar(255) DEFAULT NULL,
  `filter5` varchar(255) DEFAULT NULL,
  `filter6` varchar(255) DEFAULT NULL,
  `filter7` varchar(255) DEFAULT NULL,
  `filter8` varchar(255) DEFAULT NULL,
  `filter9` varchar(255) DEFAULT NULL,
  `filter10` varchar(255) DEFAULT NULL,
  `filter11` text NOT NULL,
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------------------

--
-- Структура таблицы `gxc_email_log`
--

CREATE TABLE IF NOT EXISTS `gxc_email_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(128) DEFAULT 'No Subject',
  `email` varchar(128) NOT NULL,
  `content` text NOT NULL,
  `source` varchar(128) DEFAULT NULL,
  `files` text,
  `status` tinyint(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Структура таблицы `gxc_galleries`
--

CREATE TABLE IF NOT EXISTS `gxc_galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `resize_options_json` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_language`
--

CREATE TABLE IF NOT EXISTS `gxc_language` (
  `lang_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `lang_name` varchar(255) DEFAULT '',
  `lang_desc` varchar(255) DEFAULT '',
  `lang_required` tinyint(1) DEFAULT '0',
  `lang_active` tinyint(1) DEFAULT '0',
  `lang_short` varchar(10) NOT NULL,
  PRIMARY KEY (`lang_id`),
  KEY `lang_desc` (`lang_desc`),
  KEY `lang_name` (`lang_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gxc_language`
--

INSERT INTO `gxc_language` (`lang_id`, `lang_name`, `lang_desc`, `lang_required`, `lang_active`, `lang_short`) VALUES
(1, 'ru', 'Russian', 1, 1, 'ru');

-- --------------------------------------------------------


--
-- Структура таблицы `notify`
--

CREATE TABLE IF NOT EXISTS `gxc_notify` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` text,
  `message` text,
  `url` text,
  `recipient` varchar(255) DEFAULT NULL,
  `category` varchar(32) NOT NULL,
  `placement` varchar(32) DEFAULT NULL,
  `level` varchar(32) DEFAULT NULL,
  `counter` int(11) unsigned DEFAULT NULL,
  `flash` tinyint(1) unsigned DEFAULT NULL,
  `enabled` tinyint(1) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `notify_reads`
--

CREATE TABLE IF NOT EXISTS `gxc_notify_reads` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notify_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `readed` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `notify_and_user` (`notify_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_mailtemplate`
--

CREATE TABLE IF NOT EXISTS `gxc_mailtemplate` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_title` varchar(255) NOT NULL,
  `mail_text` text NOT NULL,
  `mail_slug` varchar(255) NOT NULL,
  PRIMARY KEY (`mail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_menu`
--


CREATE TABLE IF NOT EXISTS `gxc_menu` (
  `menu_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_description` varchar(255) NOT NULL,
  `lang` tinyint(4) DEFAULT NULL,
  `guid` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_menu_item`
--

CREATE TABLE IF NOT EXISTS `gxc_menu_item` (
  `menu_item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `is_visible` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `type` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `parent` int(10) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  `css_class` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_object`
--
CREATE TABLE IF NOT EXISTS `gxc_object` (
  `object_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `object_author` bigint(20) unsigned DEFAULT '0',
  `object_date` int(11) NOT NULL DEFAULT '0',
  `object_date_gmt` int(11) NOT NULL DEFAULT '0',
  `object_content` longtext,
  `object_title` text,
  `object_excerpt` text,
  `object_status` tinyint(4) NOT NULL DEFAULT '1',
  `comment_status` tinyint(4) NOT NULL DEFAULT '1',
  `object_password` varchar(20) DEFAULT NULL,
  `object_name` varchar(255) NOT NULL DEFAULT '',
  `object_modified` int(11) NOT NULL DEFAULT '0',
  `object_modified_gmt` int(11) NOT NULL DEFAULT '0',
  `object_content_filtered` text,
  `object_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `object_type` varchar(20) NOT NULL DEFAULT 'object',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  `object_slug` varchar(255) DEFAULT NULL,
  `object_description` text,
  `lang` tinyint(4) DEFAULT '1',
  `object_author_name` varchar(255) DEFAULT NULL,
  `total_number_meta` tinyint(3) NOT NULL,
  `total_number_resource` tinyint(3) NOT NULL,
  `tags` text,
  `object_view` int(11) NOT NULL DEFAULT '0',
  `like_count` int(11) NOT NULL DEFAULT '0',
  `dislike_count` int(11) NOT NULL DEFAULT '0',
  `rating_scores` int(11) NOT NULL DEFAULT '0',
  `rating_average` float NOT NULL DEFAULT '0',
  `layout` varchar(125) DEFAULT NULL,
  `read_also` tinyint(1) NOT NULL DEFAULT '0',
  `read_also_limit` int(11) NOT NULL DEFAULT '5',
  `read_also_ids` text NOT NULL,
  `sellable` tinyint(1) NOT NULL DEFAULT '0',
  `shop_id` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `description` varchar(500) NOT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`),
  KEY `object_name` (`object_name`),
  KEY `type_status_date` (`object_type`,`object_status`,`object_date`,`object_id`),
  KEY `object_parent` (`object_parent`),
  KEY `object_author` (`object_author`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_object_meta`
--

CREATE TABLE IF NOT EXISTS `gxc_object_meta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `meta_object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `object_id` (`meta_object_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_object_resource`
--

CREATE TABLE IF NOT EXISTS `gxc_object_resource` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `resource_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `resource_order` int(11) NOT NULL DEFAULT '0',
  `description` longtext,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`object_id`,`resource_id`),
  KEY `resource_id` (`resource_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_object_term`
--

CREATE TABLE IF NOT EXISTS `gxc_object_term` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_id`),
  KEY `term_id` (`term_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Структура таблицы `gxc_object_views_bookeeping`
--

CREATE TABLE IF NOT EXISTS `gxc_object_views_bookeeping` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` int(11) unsigned NOT NULL,
  `ip_address` varbinary(16) DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `single_user_and_stat` (`object_id`,`user_id`),
  UNIQUE KEY `single_ipaddress_and_stat` (`object_id`,`ip_address`),
  KEY `object_id` (`object_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

--
-- Структура таблицы `gxc_order`
--

CREATE TABLE IF NOT EXISTS `gxc_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `address_id` int(10) unsigned NOT NULL,
  `operator_id` int(11) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `currency` varchar(3) NOT NULL,
  `currency_provider` int(10) unsigned NOT NULL,
  `comment` text NOT NULL,
  `referer` varchar(255) NOT NULL,
  `ip` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `adress_id` (`address_id`),
  KEY `operator_id` (`operator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_order_address`
--

CREATE TABLE IF NOT EXISTS `gxc_order_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `country` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `street` varchar(300) NOT NULL,
  `house` varchar(10) DEFAULT NULL,
  `flat` varchar(10) DEFAULT NULL,
  `telephone` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_order_check`
--

CREATE TABLE IF NOT EXISTS `gxc_order_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `prod_id` int(11) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `price` decimal(19,2) unsigned NOT NULL,
  `total_price` decimal(19,2) unsigned NOT NULL,
  `currency` varchar(3),
  `meta` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id` (`order_id`,`prod_id`),
  KEY `prod_id` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_order_client`
--

CREATE TABLE IF NOT EXISTS `gxc_order_client` (
  `order_id` int(10) unsigned NOT NULL,
  `first_name` varchar(125) NOT NULL,
  `last_name` varchar(125) DEFAULT NULL,
  `email` varchar(75) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `gxc_order`
--
ALTER TABLE `gxc_order`
  ADD CONSTRAINT `gxc_order_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `gxc_order_address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `gxc_order_check`
--
ALTER TABLE `gxc_order_check`
  ADD CONSTRAINT `gxc_order_check_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `gxc_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gxc_order_client`
--
ALTER TABLE `gxc_order_client`
  ADD CONSTRAINT `gxc_order_client_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `gxc_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_order_type`
--

CREATE TABLE IF NOT EXISTS `gxc_order_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` decimal(19,2) NOT NULL,
  `message` text,
  `condition` text,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_page`
--

CREATE TABLE IF NOT EXISTS `gxc_page` (
  `page_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `breadcrumb_title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `parent` bigint(20) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` tinyint(4) NOT NULL,
  `guid` varchar(255) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `allow_index` tinyint(1) NOT NULL DEFAULT '1',
  `allow_follow` tinyint(1) NOT NULL DEFAULT '1',
  `display_type` varchar(50) NOT NULL DEFAULT 'none',
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_page_block`
--

CREATE TABLE IF NOT EXISTS `gxc_page_block` (
  `page_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `block_order` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `region` int(11) NOT NULL,
  PRIMARY KEY (`page_id`,`block_id`,`block_order`,`region`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_photos`
--

CREATE TABLE IF NOT EXISTS `gxc_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `ext` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_resource`
--

CREATE TABLE IF NOT EXISTS `gxc_resource` (
  `resource_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `resource_name` varchar(255) DEFAULT '',
  `resource_body` text,
  `resource_path` varchar(255) DEFAULT '',
  `resource_type` varchar(50) DEFAULT NULL,
  `created` int(11) DEFAULT '0',
  `updated` int(11) DEFAULT '0',
  `creator` bigint(20) NOT NULL,
  `resource_where` varchar(50) NOT NULL,
  PRIMARY KEY (`resource_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_rights`
--

CREATE TABLE IF NOT EXISTS `gxc_rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_search_lucene`
--

CREATE TABLE IF NOT EXISTS `gxc_search_lucene` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `index_id` varchar(128) NOT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_date` timestamp NULL DEFAULT NULL,
  `last_object_date` timestamp NULL DEFAULT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_id` (`index_id`),
  UNIQUE KEY `index_id_2` (`index_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Структура таблицы `gxc_session`
--

CREATE TABLE IF NOT EXISTS `gxc_session` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_settings`
--

CREATE TABLE IF NOT EXISTS `gxc_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(64) NOT NULL DEFAULT 'system',
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_key` (`category`,`key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gxc_settings`
--

INSERT INTO `gxc_settings` (`category`, `key`, `value`) VALUES
('system', 'support_email', '{{SUPPORT_EMAIL}}'),
('general', 'site_name', '{{SITE_NAME}}'),
('general', 'homepage', 's:4:"home";'),
('general', 'slogan', '{{SLOGAN}}'),
('general', 'post_link', 's:4:"post";'),
('general', 'error_link', 's:5:"error";');

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_source_message`
--

CREATE TABLE IF NOT EXISTS `gxc_source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_tag`
--

CREATE TABLE IF NOT EXISTS `gxc_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `frequency` int(11) DEFAULT '1',
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_tag_relationships`
--

CREATE TABLE IF NOT EXISTS `gxc_tag_relationships` (
  `tag_id` bigint(20) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  PRIMARY KEY (`tag_id`,`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_taxonomy`
--

CREATE TABLE IF NOT EXISTS `gxc_taxonomy` (
  `taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'article',
  `lang` tinyint(4) DEFAULT '1',
  `guid` varchar(255) NOT NULL,
  PRIMARY KEY (`taxonomy_id`),
  KEY `taxonomy` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_term`
--

CREATE TABLE IF NOT EXISTS `gxc_term` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `taxonomy_id` int(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL DEFAULT '',
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`term_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Структура таблицы `gxc_translated_message`
--

CREATE TABLE IF NOT EXISTS `gxc_translated_message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text,
  PRIMARY KEY (`id`,`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_user`
--

CREATE TABLE IF NOT EXISTS `gxc_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_url` varchar(128) DEFAULT NULL,
  `display_name` varchar(255) NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `fbuid` bigint(20) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_time` int(11) NOT NULL,
  `updated_time` int(11) NOT NULL,
  `recent_login` int(11) NOT NULL,
  `user_activation_key` varchar(255) NOT NULL DEFAULT '',
  `confirmed` tinyint(2) NOT NULL DEFAULT '0',
  `gender` varchar(10) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `bio` text,
  `birthday_month` varchar(50) DEFAULT NULL,
  `birthday_day` varchar(2) DEFAULT NULL,
  `birthday_year` varchar(4) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `email_site_news` tinyint(1) NOT NULL DEFAULT '1',
  `email_search_alert` tinyint(1) NOT NULL DEFAULT '1',
  `email_recover_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  KEY `first_name` (`first_name`,`last_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gxc_user`
--

INSERT INTO `gxc_user` (`user_id`, `user_url`, `display_name`, `first_name`, `password`, `salt`, `email`, `fbuid`, `status`, `created_time`, `updated_time`, `recent_login`, `user_activation_key`, `confirmed`, `gender`, `location`, `bio`, `birthday_month`, `birthday_day`, `birthday_year`, `avatar`, `email_site_news`, `email_search_alert`, `email_recover_key`) VALUES
(1, 'admin', 'Admin', 'Admin', 'fa294f54308202af06eced71e5fe5fb3', 'sdad12313ssgdpahcxrwwqas', 'admin@localhost.com', NULL, 1, 1374674645, 1374676507, 1374676507, '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_user_meta`
--

CREATE TABLE IF NOT EXISTS `gxc_user_meta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1392078574),
('m140129_110457_init', 1392078574),
('m140205_200629_order_delivery_and_check_currency', 1392078574),
('m140521_063812_email_log_subject_source', 1400655063),
('m140527_092858_email_log_files', 1401867257),
('m140603_155539_page_breadcrumbs', 1401867258),
('m140811_123354_ecolumns', 1407768140),
('m140910_154929_remove_transfer', 1407768140),
('m140910_161739_remove_keywords', 1407768140),
('m140910_161929_remove_object_keywords', 1407768140),
('m140930_135439_menu_item_is_visible', 1412086053),
('m141106_131739_listview_max_deprecated', 1411086053),
('m141212_093037_remove_banners_completely', 1418376733);

SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
