<?php
class resizeImage {

   var $image;
   var $image_type;

   function load($filename) {
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      }
   }
   
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
      }
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }

   function output($image_type=IMAGETYPE_JPEG) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image);
      }
   }
   function getWidth() {
      return imagesx($this->image);
   }
   function getHeight() {
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width);
   }
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }

   function loger($text){
     mysql_query('INSERT INTO logs(content) VALUES(\''.$text.'\');');
   }

   function resize($w=0,$h=0) {
      $crop=1;
      $img_size=array();
      $img_size[0]=$this->getWidth();
      $img_size[1]=$this->getHeight();

      $dst_x=$dst_y=$src_x=$src_y=0;
      $src_w=$img_size[0];
      $src_h=$img_size[1];
      $dst_w=$w;
      $dst_h=$h;
      if($w && $h){
        if($img_size[0]<$dst_w) $dst_w=0;
        if($img_size[1]<$dst_h) $dst_h=0;
        if($dst_w && $dst_h){
          $th=$img_size[1]*($w/$img_size[0]);
          if($crop){
            if($th<$h){
              $src_w=$w*($img_size[1]/$h);
              $src_x=($img_size[0]-$src_w)/2;
            }else{
              $src_h=$h*($img_size[0]/$w);
              $src_y=($img_size[1]-$src_h)/2;
            }
          }else{
            if($th<$h) $dst_h=$h=$img_size[1]*($w/$img_size[0]);
            else $dst_w=$w=$img_size[0]*($h/$img_size[1]);
          }
        }elseif($dst_w){
          $dst_h=$img_size[1]*($w/$img_size[0]);
        }elseif($dst_h){
          $dst_w=$img_size[0]*($h/$img_size[1]);
        }else{
          $dst_w=$img_size[0];
          $dst_h=$img_size[1];
        }
      }elseif($w){
        if($img_size[0]<$w) $dst_w=$w=$img_size[0];
        $dst_h=$h=$img_size[1]*($w/$img_size[0]);
      }elseif($h){
        if($img_size[1]<$h) $dst_h=$h=$img_size[1];
        $dst_w=$w=$img_size[0]*($h/$img_size[1]);
      }else{
        $dst_w=$w=$img_size[0];
        $dst_h=$h=$img_size[1];
      }
      $dst_x=$w/2-$dst_w/2;
      $dst_y=$h/2-$dst_h/2;

      $new_image=imagecreatetruecolor($w,$h);

      //if ($this->$image_type == IMAGETYPE_PNG){
        imagealphablending($new_image, false);
        $color = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
        imagefill($new_image, 0, 0, $color);
        imagesavealpha($new_image,true);
      //}

      $white = imagecolorallocate($new_image, 255,255,255);
      imagefill($new_image,1,1,$white);
      imagecopyresampled($new_image,$this->image,$dst_x,$dst_y,$src_x,$src_y,$dst_w,$dst_h,$src_w,$src_h);

      //$new_image = imagecreatetruecolor($width, $height);
      //imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
     
      $this->image = $new_image;
   }
}

/*
        if ($this->$image_type == IMAGETYPE_PNG){
            $transparencyIndex = imagecolortransparent($this->image);
            $transparencyColor = array('red' => 255, 'green' => 255, 'blue' => 255);

            if ($transparencyIndex >= 0) {
                $transparencyColor    = imagecolorsforindex($this->image, $transparencyIndex);
            }

            $transparencyIndex    = imagecolorallocate($new_image, $transparencyColor['red'], $transparencyColor['green'], $transparencyColor['blue']);
            imagefill($new_image, 0, 0, $transparencyIndex);
            imagecolortransparent($new_image, $transparencyIndex);
        }
*/

?>
