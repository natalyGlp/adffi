<?php return  array(
    'beadmin' => array(
        'class' => 'cms.controllers.BeadminController'
    ),
    'besite' => array(
        'class' => 'cms.controllers.BesiteController'
    ),
    'becatalogattributes' => array(
        'class' => 'cms.controllers.BecatalogattributesController'
    ),
    'befilters' => array(
        'class' => 'cms.controllers.BefiltersController'
    ),
    'beobject' => array(
        'class' => 'cms.controllers.BeobjectController'
    ),
    'bealbums' => array(
        'class' => 'cms.controllers.BealbumsController'
    ),
    'beblock' => array(
        'class' => 'cms.controllers.BeblockController'
    ),
    'becaching' => array(
        'class' => 'cms.controllers.BecachingController'
    ),
    'becomment' => array(
        'class' => 'cms.controllers.BecommentController'
    ),
    'becontentlist' => array(
        'class' => 'cms.controllers.BecontentlistController'
    ),
    'begalleries' => array(
        'class' => 'cms.controllers.BegalleriesController'
    ),
    'belanguage' => array(
        'class' => 'cms.controllers.BelanguageController'
    ),
    'bemailtemplate' => array(
        'class' => 'cms.controllers.BemailtemplateController'
    ),
    'bemenu' => array(
        'class' => 'cms.controllers.BemenuController'
    ),
    'besearch' => array(
        'class' => 'cms.controllers.BesearchController'
    ),
    'beshop' => array(
        'class' => 'cms.controllers.BeshopController'
    ),
    'besitemap' => array(
        'class' => 'cms.controllers.BesitemapController'
    ),
    'beemail' => array(
        'class' => 'cms.controllers.BeemailController'
    ),
    'bemenuitem' => array(
        'class' => 'cms.controllers.BemenuitemController'
    ),
    'beorder' => array(
        'class' => 'cms.controllers.BeorderController'
    ),
    'bepage' => array(
        'class' => 'cms.controllers.BepageController'
    ),
    'bephotos' => array(
        'class' => 'cms.controllers.BephotosController'
    ),
    'beresource' => array(
        'class' => 'cms.controllers.BeresourceController'
    ),
    'besettings' => array(
        'class' => 'cms.controllers.BesettingsController'
    ),
    'betaxonomy' => array(
        'class' => 'cms.controllers.BetaxonomyController'
    ),
    'beterm' => array(
        'class' => 'cms.controllers.BetermController'
    ),
    'beupdate' => array(
        'class' => 'cms.controllers.BeupdateController'
    ),
    'beuser' => array(
        'class' => 'cms.controllers.BeuserController'
    ),
);