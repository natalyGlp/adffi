<?php
/**
 * Контент тип для статически страниц
 */
class PageObject extends Object
{
    public $object_type = 'page';

    /**
     * Returns the static model of the specified AR class.
     * @return Object the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function Resources()
    {
        return CMap::mergeArray(Object::Resources(),
            array(
                'slider' => array('type' => 'slider',
                    'name' => 'Slider',
                    'maxSize' => ConstantDefine::UPLOAD_MAX_SIZE,
                    'minSize' => ConstantDefine::UPLOAD_MIN_SIZE,
                    'max' => 100,
                    'allow' => array('jpeg', 'jpg', 'gif', 'png'),
                ),

                // видео с youtube и vimeo
                'videoembed' => array(
                    'type' => 'videoembed',
                    'storageId' => 'remote',
                    'name' => t('Embed Video'),
                    'maxSize' => ConstantDefine::UPLOAD_MAX_SIZE,
                    'minSize' => ConstantDefine::UPLOAD_MIN_SIZE,
                    'max' => 1,
                    'allowMultiple' => false,
                    'allow' => array('youtube.com', 'vimeo.com'),
                ),

                // 'video'=>array('type'=>'video',
                //     'name'=>'Upload Video',
                //     'maxSize'=>ConstantDefine::UPLOAD_MAX_SIZE,
                //     'minSize'=>ConstantDefine::UPLOAD_MIN_SIZE,
                //     'max'=>1,
                //     'allow'=>array('flv', 'mp4',)
                //     )
            )
        );
    }
}
