<?php
$link = $data->getObjectLink();
?>
<div class="item_render_part content_<? echo  $data->object_type; ?>">
<? $thumb=GxcHelpers::getResourceObjectFromDatabase($data,'thumbnail');
if (count($thumb)>=1) { ?>
    <div class="image">
    <?//echo CHtml::link(CHtml::image($thumb[0]['link'],$data->object_title,array('align'=>'left')),Object::getLink($data->object_id)); ?>
    </div>
    <?} ?>
    <h3 class="item_title title_<? echo  $data->object_type; ?>"><a href="<?php echo $link; ?>"><?php echo CHtml::encode($data->object_name); ?></a></h3>
    <span class="item_date"><?php echo date("m/d/Y", $data->object_date); ?></span>
    <div class="content">
        <?php echo $data->getExcerpt(); ?>
        <a  class="read-more" href="<? echo Object::getLink($data->object_id);?>" title=""><? echo t('Читать полностью>'); ?></a>
     </div>
</div>
