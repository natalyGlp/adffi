<?php
$namespace = 'detail-view';
?>
<article class="<?php echo $this->createCssClass($post->object_type, $namespace, true); if(!empty($this->cssClass)) echo $this->cssClass; ?>">
    <h1><?php echo $post->object_name; ?></h1>
    <div class="<?php echo $this->createCssClass($post->object_type.'-content', $namespace, true); ?>">
        <?php echo $post->object_content; ?>
    </div>
</article>
