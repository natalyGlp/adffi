<?php

/**
 * This is the configuration for generating message translations
 * for the Nespi cms. It is used by the 'yiic message' command.
 */
return array(
    'sourcePath' => dirname(__FILE__) . '/..',
    'messagePath' => dirname(__FILE__),
    'translator' => '_t',
    'languages' => array(
        'ru',
    ),
    'fileTypes' => array(
        'php',
    ),
    'overwrite' => true,
    'exclude' => array(
        '/assets',
        '/commands',
        '/content_type',
        '/data',
        '/extensions',
        '/front_blocks',
        '/gii',
        '/messages',
        '/migrations',
        '/modules',
        '/vendors',
    ),
);
