<?php

Yii::app()->controller->pageTitle = TranslateModule::t('Manage Messages');

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'enableHistory' => true,
    'id' => 'message-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',
            'htmlOptions' => array(
                'style' => 'width: 50px;',
            ),
        ),
        array(
            'name' => 'category',
            'filter' => CHtml::listData($source, 'category', 'category'),
        ),
        array(
            'name' => 'language',
            'filter' => CHtml::listData($model->findAll(new CDbCriteria(array('order' => 'language'))), 'language', 'language')
        ),
        'message',
        'translation',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->getController()->createUrl("update",array("id"=>$data->id))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => t('Delete'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->getController()->createUrl("delete",array("id"=>$data->id, "language"=>$data->language))',
                ),
            ),
        ),
    ),
));
