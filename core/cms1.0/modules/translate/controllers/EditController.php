<?php

class EditController extends TranslateBaseController
{
    public $defaultAction = 'admin';
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $source = MessageSource::model()->findByPk($id);
        $models = $this->getTranslationModels($id);

        if (isset($_POST['Message'])) {
            $success = true;

            foreach (TranslateModule::translator()->acceptedLanguages as $languageCode => $languageName) {
                if (!isset($_POST['Message'][$languageCode]) || empty($_POST['Message'][$languageCode]['translation'])) {
                    continue;
                }

                $model = $models[$languageCode];

                $model->attributes = $_POST['Message'][$languageCode];
                $model->id = $source->primaryKey;
                $model->language = $languageCode;

                $success = $success && $model->save();
            }

            if ($success) {
                Yii::app()->user->setFlash('success', TranslateModule::t('The translations was successfully updated'));
            }
        }

        $this->render('form', array(
            'source' => $source,
            'models' => $models,
            ));
    }

    /**
     * @return  array languageCode => model с моделями существующих переводов MessageSource,
     *                а так же новыми моделями для всех не существующих
     */
    protected function getTranslationModels($sourceMessageId)
    {
        $availableTranslations = $this->getAvailableTransionsModels($sourceMessageId);

        $models = array();
        foreach (TranslateModule::translator()->acceptedLanguages as $languageCode => $languageName) {
            if (TranslateModule::translator()->sourceLanguage == $languageCode) {
                // TODO: сделать нормальную поддержку основного языка для sourceMessages
                continue;
            }

            $hasTranslation = isset($availableTranslations[$languageCode]);

            if ($hasTranslation) {
                $model = $availableTranslations[$languageCode];
            } else {
                $model = new Message();
                $model->language = $languageCode;
            }

            $models[$languageCode] = $model;
        }

        return $models;
    }

    protected function getAvailableTransionsModels($id)
    {
        $models = Message::model()->findAllByAttributes(array(
            'id' => $id,
            ));

        $remapedModels = array();
        foreach ($models as $model) {
            $remapedModels[$model->language] = $model;
        }

        return $remapedModels;
    }

    /**
     * Deletes a record
     * @param integer $id the ID of the model to be deleted
     * @param string $language the language of the model to de deleted
     */
    public function actionDelete($id, $language)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->translateLoadModel($id, $language);
            if ($model->delete()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                    echo TranslateModule::t('Message deleted successfully');
                    Yii::app()->end();
                } else {
                    $this->redirect(Yii::app()->getRequest()->getUrlReferrer());
                }
            }
        } else {
            throw new CHttpException(400);
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Message('search');
        $model->unsetAttributes();// clear any default values
        if (isset($_GET['Message'])) {
            $model->attributes = $_GET['Message'];
        }

        $source = MessageSource::model()->findAll(array(
            'order' => 'message ASC',
        ));

        $this->render('admin', array(
            'model' => $model,
            'source' => $source,
        ));
    }
    /**
     *
     */
    public function actionMissing()
    {
        $model = new MessageSource('search');
        $model->unsetAttributes();// clear any default values

        if (isset($_GET['MessageSource'])) {
            $model->attributes = $_GET['MessageSource'];
        }

        $model->language = TranslateModule::translator()->getLanguage();

        $source = MessageSource::model()->findAll(array(
            'order' => 'message ASC',
        ));

        $this->render('missing', array(
            'model' => $model,
            'source' => $source,
        ));
    }
    /**
     * Deletes a record
     * @param integer $id the ID of the model to be deleted
     * @param string $language the language of the model to de deleted
     */
    public function actionMissingdelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = MessageSource::model()->findByPk($id);
            if ($model->delete()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                    echo TranslateModule::t('Message deleted successfully');
                    Yii::app()->end();
                } else {

                    $this->redirect(Yii::app()->getRequest()->getUrlReferrer());
                }
            }
        } else {
            throw new CHttpException(400);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function translateLoadModel($id, $language)
    {
        $model = Message::model()->findByPk(array('id' => $id, 'language' => $language));
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }
}
