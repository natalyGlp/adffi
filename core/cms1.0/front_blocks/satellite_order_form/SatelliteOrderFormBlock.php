<?php
// TODO: подписи запросов
/**
 * Блок формы оформления заказа для сателлитной сети
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.satellite_order_form
 * @var $_model SatelliteOrderFormModel
 */
Yii::import('cms.components.SatelliteOrderApi');
class SatelliteOrderFormBlock extends BlockWidget
{
    protected $_model;
    public $title;

    /**
     * @var url на который будет редирект после успешного оформления заказа
     */
    public $redirectOnSuccess;

    /**
     * @var string $superShopApiUrl url api супермагазина, к которому будет обращаться блок
     */
    public $superShopApiUrl = null;

    protected $_api;

    public function init()
    {
        parent::init();

        Yii::import(GxcHelpers::getTruePath('front_blocks.' . $this->blockType . '.models.SatelliteOrderFormModel'));
        $this->_model = new SatelliteOrderFormModel();
        $this->_api = new SatelliteOrderApi();

        if (!$this->superShopApiUrl) {
            $this->superShopApiUrl = $this->_api->apiUrl;
        }
    }

    public function run()
    {
        $this->registerClientScripts();
        $this->renderContent();
    }

    protected function renderContent()
    {
        if (isset($_POST['SatelliteOrderFormModel'])) {
            $this->_model->attributes = $_POST['SatelliteOrderFormModel'];

            if ($this->_model->validate()) {
                $success = $this->_api->issueOrder($this->_model);

                if ($success) {
                    Yii::app()->user->setFlash('success', t('Order has been successfully issued'));
                    Yii::app()->controller->redirect($this->redirectOnSuccess ? $this->redirectOnSuccess : $_SERVER['REQUEST_URI']);
                } else {
                    Yii::app()->user->setFlash('error', t('There was an error processing your order'));
                }
            }
        }

        $this->render('cmswidgets.views.notification_frontend');
        $this->render('output', array('model' => $this->_model));
    }

    /**
     * Возвращает классы для полей с информацией о продукте, к которым потом будет цепляться скрипт
     * @param  string $infoId название поля, для которой нужен класс
     * @return string         правильное имя класса
     */
    public function getProductInfoClass($infoId)
    {
        return $infoId . '_' . $this->getId();
    }

    protected function registerClientScripts()
    {
        $assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', false, -1, YII_DEBUG);
        $cs = Yii::app()->clientScript;
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($assetsUrl . '/js/orderForm.jquery.js', CClientScript::POS_END);
        $cs->registerScript(__FILE__ . '#' . $this->getId(), '$("#orderForm' . $this->getId() . '").orderForm(' . CJavaScript::encode(array(
            'data' => $this->_api->storeData,
            'widgetId' => $this->getId(),
            'country' => Yii::app()->user->countryCode,
            'apiUrl' => $this->superShopApiUrl,
        )) . ')');
    }

    public function params()
    {
        return array(
            'title' => t('Title'),
            'redirectOnSuccess' => t('Redirect url after successfull submit (leave blank, when you don\'t need this)'),
        );
    }

    public function phoneField(array $options = array())
    {
        $this->widget('cms.extensions.maskedInput.MaskedInputWidget', array(
            'model'=>$this->_model,
            'attribute'=>'phone',
            'htmlOptions'=>$options
        ));
    }
}
