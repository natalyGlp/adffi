<div <?php if ($this->cssClass) echo 'class="'.$this->cssClass.'"'; ?>>
    <div class="title">
        <h2><?php echo t($this->title); ?></h2>
    </div>

    <?php
        $form = $this->beginWidget('CActiveForm', array(
            'enableClientValidation'=>true,
            'id' => 'orderForm'.$this->getId(),
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
                'validateOnChange'=>false
            ),
        ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('placeholder' => 'Шевченко Юрий Викторович')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'country'); ?>
        <?php echo $form->dropDownList($model, 'country', SatelliteOrderFormModel::getCountriesList()); ?>
        <?php echo $form->error($model, 'country'); ?>
        <?php echo $form->dropDownList($model, 'currency', SatelliteOrderFormModel::getCurrenciesList()); ?>
        <?php echo $form->error($model, 'currency'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'city'); ?>
        <?php echo $form->textField($model, 'city'); ?>
        <?php echo $form->error($model, 'city'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'street'); ?>
        <?php echo $form->textField($model, 'street', array('placeholder' => 'ул. Бассейная')); ?>

        <?php echo $form->labelEx($model, 'house'); ?>
        <?php echo $form->textField($model, 'house', array('placeholder' => '3')); ?>

        <?php echo $form->labelEx($model, 'flat'); ?>
        <?php echo $form->textField($model, 'flat', array('placeholder' => '15')); ?>

        <?php echo $form->error($model, 'street'); ?>
        <?php echo $form->error($model, 'house'); ?>
        <?php echo $form->error($model, 'flat'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'zip'); ?>
        <?php echo $form->textField($model, 'zip', array('placeholder' => '08170')); ?>
        <?php echo $form->error($model, 'zip'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'phone'); ?>
        <?php echo $this->phoneField(array('placeholder' => '+38 (067) 000-00-00', 'class'=>'phone-input')); ?>
        <?php echo $form->error($model, 'phone'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('placeholder' => 'ivanov@gmail.com')); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'amount'); ?>
        <?php
        $amountMin = 1;
        $amountMax = 10;
        echo $form->dropDownList($model, 'amount', array_combine(range($amountMin, $amountMax), range($amountMin, $amountMax)));
        ?>
        <?php echo $form->error($model, 'amount'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'variant'); ?>
        <?php echo $form->dropDownList($model, 'variant', SatelliteOrderFormModel::getVariantsList()); ?>
        <?php echo $form->error($model, 'variant'); ?>
    </div>


    <div class="summary">
        <p>Стоимость заказа:</p>
        <p class="<?php echo $this->getProductInfoClass('orderNoTaxPrice') ?>"></p>


        <p>Стоимость заказа с НДС:</p>
        <p class="<?php echo $this->getProductInfoClass('orderPrice') ?>"></p>

        <p>Доставка:</p>
        <p class="controls-label <?php echo $this->getProductInfoClass('deliveryPrice') ?>"></p>

        <p>ИТОГО:</p>
        <p class="<?php echo $this->getProductInfoClass('totalPrice') ?>"></p>
    </div>


    <div class="<?php echo $this->getProductInfoClass('deliveryMessage') ?>">
        Доставка наложенным платежом. <br />Оплата при получении товара!
    </div>

    <div class="row buttons">
        <?php echo $form->hiddenField($model, 'id', array('class' => $this->getProductInfoClass('id'))); ?>
        <?php echo CHtml::submitButton('Заказать'); ?>
    </div>

    <?php $this->endWidget('CActiveForm'); ?>
</div>

