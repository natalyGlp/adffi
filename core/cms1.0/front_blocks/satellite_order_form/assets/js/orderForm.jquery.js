// TODO: автоопределение страны юзера
// TODO: на данном этапе способ доставки захардкоден
// TODO: акции не реализованы
/**
 * Классы плейсхолдеров:
 * - orderPrice_[id]
 * - deliveryPrice_[id]
 * - totalPrice_[id]
 * - orderTaxPrice_[id]
 * - deliveryMessage_[id]
 *
 * @author Sviatoslav Danylenko
 */
(function($) {
    'use strict';

    /**
     * [orderForm description]
     * @param  {[type]} d       данные товара
     * @param  {[type]} options [description]
     * @return {[type]}         [description]
     */
    function orderForm(options) {
        var d, // даныне товара @see init()
            countryId = options.country || 'UA', // id выбранной страны
            defaultCurrency = 'UAH',
            widgetId = options.widgetId, // id виджета, используется как суффикс для классов
            $form = options.$form, // форма заказа
            $countries = $(fid('country')).empty(),
            $currencies = $(fid('currency')).empty(),
            $variant = $(fid('variant')).empty(),
            dataEvents = {},
            product; // текущий продукт, связанный с формой


        /**
         * Возвращает id поля формы
         */
        function fid(id)
        {
            return '#SatelliteOrderFormModel_' + id;
        }

        /**
         * Привязывает форму к определенному id товара
         * @param {string} id индекс в массиве товаров, который вернет api
         */
        function setProduct(id)
        {
            product = d.products[id ? d.productIndexById[id] : 0];

            if(product.variants.length > 0 && $variant.length > 0)
            {
                $variant.empty();
                var html = [];

                for(var vId = 0; vId < product.variants.length; vId++) {
                    html.push('<option value="'+product.variants[vId].id+'" data-oid="'+vId+'">'+product.variants[vId].name+'</option>');
                }
                $variant.append(html.join('\n'));
            }else{
                $variant.parent().hide();
            }


            insertProductData();
            calculatePrice();
        }

        /**
         * Заполняет информацию о продукте (id, название, картинки и т.п.)
         * @param  {object} curProduct обьект с данными продукта (по дефолту будет использовать текуший продукт)
         */
        function insertProductData(curProduct)
        {
            if(!curProduct) {
                curProduct = product;
            }

            setElContent(ph('name'), product.name);

            setElContent(ph('id'), product.id);

            var $images = $form.find(ph('image'));
            $images.each(function() {
                var index = $(this).data('image-index') ? $(this).data('image-index') : 0;

                if(!product.photos[index]) {
                    $(this).remove();
                    return true;
                }
                this.src = product.photos[index].link;
            });

            // TODO: рефакторин, перевести весь код на использование этого метода
            // по сути нам нужен паттерн observerable, попробовать привязать jquery ивенты к обьекту
            if(dataEvents['image']) {
                var _imageIterator = function() {
                    var index = $(this).data('image-index') ? $(this).data('image-index') : 0;
                    this.src = product.photos[index].link;
                };

                for(var i = 0; i < dataEvents['image'].length; i++) {
                    var options = dataEvents['image'][i];
                    var $el = $(options.el);
                    $el.each(_imageIterator);
                }
            }
        }

        /**
         * Привязывает информацию о текущем продукте к элементу el
         * и автоматически ее обновляет при изменении состояния формы
         * @param  {string|object} fieldId название поля или обьект с описанием его параметров, которое нужно слушать
         * @param  {} el    дом обьект, jQuery обьект или селектор элемента
         * @param  {function} callback опциональная функция, которая будет вызываться при изменении состояния формы
         */
        function bindData(fieldId, el, callback)
        {
            if(typeof el == 'string') {
                el = {el: el};
            }

            el = $.extend({callback: callback}, el);

            if(!dataEvents[fieldId]) {
                dataEvents[fieldId] = [];
            }

            dataEvents[fieldId].push(el);
        }

        /**
         * Пересчитывает стоимость заказа с учетом количества товара и валюты
         */
        function calculatePrice()
        {
            var curProduct = product;
            if($variant.length && curProduct.variants.length > 0) {
                var oid = $variant.find(':selected').data('oid');
                if(oid) {
                    curProduct = curProduct.variants[oid];
                }

                insertProductData(curProduct);
            }

            var amount = isNaN(parseInt($(fid('amount')).val(), 10)) ? 1 : $(fid('amount')).val() * 1;

            var prices = getPricesForCurrency(curProduct);

            var deliveryPrice = prices.deliveryPrice;
            var price = prices.price;
            var defaultPrice = prices.defaultPrice;
            var oldPrice = prices.oldPrice;
            var totalPrice = price * amount + deliveryPrice; // цена за один продукт с доставкой
            var tax = prices.tax;
            var sign = prices.currencySign;

            // TODO: скидки должны входить в NoTaxPrice?
            // TODO: поддержка валют и кол-ва
            renderProductData({
                orderPrice: price + ' ' + sign,
                orderNoTaxPrice: defaultPrice + ' ' + sign,

                deliveryPrice: deliveryPrice + ' ' + sign,
                deliveryMessage: 'Доставка наложенным платежом Новой Почтой.<br />Оплата при получении товара!',

                totalPriceNumber: totalPrice,
                totalPrice: totalPrice + ' ' + sign,

                oldPriceNumber: oldPrice,
                oldPrice: oldPrice + ' ' + sign
            });
        }

        function getPricesForCurrency(product)
        {
            var currencyId = $currencies.length ? $currencies.val() : defaultCurrency;
            var prices = product.prices[currencyId];
            if (!prices) {
                throw new Error('Currency ' + currencyId + ' is unavailable');
            }

            return prices;
        }

        function renderProductData(data)
        {
            for (var key in data) {
                setElContent(ph(key), data[key]);
            }
        }

        /**
         * Шорткат для доступа к плейсхолдерам
         */
        function ph(id)
        {
            return '.'+id+'_'+widgetId;
        }

        /**
         * Устанавливает содержимое value или innerHtml для элемента по его селектору
         */
        function setElContent(selector, value)
        {
            var $elem = $form.find(selector);
            if($elem.length === 0) {
                return;
            }

            if(isFormField($elem[0])) {
                $elem.val(value);
            } else {
                $elem.html(value);
            }
        }

        function isFormField(obj)
        {
            return obj.tagName.toUpperCase() == 'INPUT' || obj.tagName.toUpperCase() == 'TEXTAREA' || obj.tagName.toUpperCase() == 'SELECT';
        }

        /**
         * Инициализация формы
         */
        function init(data)
        {
            if($form.data('orderFormInited')) {
                return;
            }

            d = data;

            // заполняем страны и валюты
            renderCurrenciesList();
            renderCountriesList();

            setEvents();

            setProduct();

            $countries.change();

            $form.data('orderFormInited', true);
        }

        function renderCurrenciesList()
        {
            // TODO: пусть api возвращает d.currencies
            var html = [];
            d.currencies = d.products[0].prices;
            for(var ccyId in d.currencies) {
                html.push('<option value="'+ccyId+'">'+d.currencies[ccyId].currencySign+'</option>');
            }
            $currencies.append(html.join('\n'));
        }

        function renderCountriesList()
        {
            var html = [];
            for(var cId in d.countries) {
                html.push('<option value="'+cId+'">'+d.countries[cId].name+'</option>');
            }
            $countries.append(html.join('\n'));
        }

        function setEvents()
        {
            $currencies.change(calculatePrice);

            $countries.change(function() {
                countryId = $(this).val();
                // выбираем дефолтную валюту для текущей страны
                $(fid('currency')).val(d.countries[countryId].currency).change();
            }).val(countryId);

            if($variant.length > 0) {
                $variant.change(calculatePrice);
            }

            $(fid('amount')).keydown(function (event) {
                var num = event.keyCode;
                if ((num > 95 && num < 106) || (num > 36 && num < 41) || num == 9) {
                    return;
                }
                if (event.shiftKey || event.altKey) {
                    event.preventDefault();
                }else if(event.ctrlKey) {
                    //
                } else if (num != 46 && num != 8) {
                    if (isNaN(parseInt(String.fromCharCode(event.which), 10))) {
                        event.preventDefault();
                    }
                }
            })
            .keyup(calculatePrice)
            .change(calculatePrice);
        }

        return {
            init: init,
            setProduct: setProduct,
            bindData: bindData,
        };
    }

    var _productDataCache = {};
    $.fn.orderForm = function(options) {
        if (!options.apiUrl) {
            return;
        }

        var that = this;

        options = $.extend({
            $form: this
        }, options);

        this.data('order', orderForm(options));

        if (!options.data) {
            if(!_productDataCache[options.id]) {
                _productDataCache[options.id] = $.ajax({
                    // url: options.apiUrl+'?id='+options.id,
                    url: options.apiUrl+'?getProducts=1',
                    dataType: 'jsonp',
                    type: 'GET',
                });
            }

            _productDataCache[options.id].then(function(data) {
                that.data('order').init(data);
            });
        } else {
            this.data('order').init(options.data);
        }

        return this;
    };
})(jQuery);