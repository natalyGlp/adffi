<?php
/**
 * Базовая модель для формы заказа сателлитной сети
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.satellite_order_form
 */

class SatelliteOrderFormBaseModel extends CFormModel
{
    public $id;
    public $name;
    public $country;
    public $city;
    public $street;
    public $house;
    public $flat;
    public $phone;
    public $email;
    public $zip;
    public $delivery;
    public $amount = 1;
    public $variant;
    public $currency;
    public $currencyProvider;
    public $products = array(); // id продуктОВ (если их > 1)
    public $comment; // комментарий к заказу

    public function rules()
    {
        return array(
            array('name, phone','required'),
            array('name, street, house, flat, phone, zip, country, city','safe'),
            array('currency', 'length', 'is' => 3),
            array('email', 'email'),
            array('id, amount, delivery, variant, currencyProvider', 'numerical'),
            array('products, comment', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'country'=>t('Страна'),
            'name'=>t('Ф.И.О.'),
            'city' => t('Область, город (село)'),
            'street'=>t('Улица'),
            'house'=>t('Дом'),
            'flat'=>t('Квартира'),
            'phone'=>t('Мобильный телефон'),
            'email' => t('Ваш E-mail'),
            'zip' => t('Индекс'),
            'amount' => t('Количество'),
            'currency' => t('Валюта'),
            'currencyProvider' => t('Способ оплаты'),
            'delivery' => t('Способ доставки'),
            'variant' => t('Продукт'),
        );
    }

    public function emailLogLabels()
    {
        return array(
            'country'=>t('Страна'),
            'name'=>t('Ф.И.О.'),
            'city' => t('Область, город (село)'),
            'phone'=>t('Телефон'),
            'email' => t('Электронная почта'),
            'zip' => t('Индекс'),
            'amount' => t('Количество'),
            'currency' => t('Валюта'),
            'currencyProvider' => t('Способ оплаты'),
            'street'=>t('Улица'),
            'house'=>t('Дом'),
            'flat'=>t('Квартира'),
            'delivery' => t('Способ доставки'),
            'variant' => t('Выбранный продукт'),
            );
    }

    /**
     * Добавляет комментарий к заказу
     * @param string $comment комментарий
     */
    public function addComment($comment)
    {
        $this->comment .= PHP_EOL.$comment;

        return $this;
    }

    public function normalizeProducts()
    {
        // либо один товар - либо несколько товаров
        if (!empty($this->variant)) {
            $this->products[] = array('id' => $this->variant, 'amount' => $this->amount, 'price' => 1);
        } elseif (!empty($this->id)) {
            $this->products[] = array('id' => $this->id, 'amount' => $this->amount, 'price' => 1);
        } else {
            $products = array();
            foreach ($this->products as $productId) {
                $products[] = array('id' => $productId, 'amount' => 1, 'price' => 1);
            }
            $this->products = $products;
        }

        $this->mergeDuplicateProducts();
    }

    protected function mergeDuplicateProducts()
    {
        $products = array();

        foreach ($this->products as $productData) {
            $productId = $productData['id'];
            if (!isset($products[$productId])) {
                $products[$productId] = $productData;
            } else {
                // склеиваем повторяющиеся продукты
                $products[$productId]['amount']++;
            }
        }

        $this->products = array_values($products);
    }

    /**
     * Дефолтные значения для выпадающего меню
     * (будут переопределены скриптом, если он успешно получит данные от супер магазина)
     */
    public static function getCountriesList()
    {
        return array(
            );
    }

    /**
     * Дефолтные значения для выпадающего меню
     * (будут переопределены скриптом, если он успешно получит данные от супер магазина)
     */
    public static function getCurrenciesList()
    {
        return array(
            );
    }

    /**
     * Дефолтные значения для выпадающего меню
     * (будут переопределены скриптом, если он успешно получит данные от супер магазина)
     */
    public static function getDeliveryList()
    {
        return array(
            );
    }

    /**
     * Дефолтные значения для выпадающего меню
     * (будут переопределены скриптом, если он успешно получит данные от супер магазина)
     */
    public static function getVariantsList()
    {
        return array(
            );
    }
}
