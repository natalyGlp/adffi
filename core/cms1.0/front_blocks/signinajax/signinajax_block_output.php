<?php $blockId= !isset($blockId) ? $this->block->block_id : $blockId; /* id блока*/  
$loguotlink='<a id="logoutlink'.$blockId.'" class="loguot_link" href="'.FRONT_SITE_URL.'/app/logout/'.'">'.T('Logout').'</a>'; // ссылка для выхода. ?>
<div id="blocklayout<?php echo $blockId; ?>">
 <?php if (Yii::app()->user->isGuest || $login==true) { 
/* Если пользвалень не залогинен то выводить форму авторизации или регистрации.*/

    ?>
<script>
/* Функция закрытия окна при удачном входе пользователя*/
function close_login_window(data){  
<?php 
if($login==true) {
    if($this->redirect_to_backend) 
    {?>
        setTimeout( function() {
            window.location='<?php echo BACKEND_SITE_URL?>';
            }, 4000);
    <?php } ?>
    <?php if($this->refresh) 
    {?>
        setTimeout( function() {
            window.location='<?php echo $_SERVER["REQUEST_URI"];?>';
            }, 4000);
    <?php } 
}
?> 
    if ($('#login<?php echo $blockId; ?>', '<div>'+data+'</div>').length>=1){
        $("#loginlink<?php echo $blockId; ?>").remove();
        $("#reglink<?php echo $blockId; ?>").remove();
        $("#blocklayout<?php echo $blockId; ?>").append('<?php echo $loguotlink; ?>');
          setTimeout( function() {
        $("#loginForm<?php echo $blockId; ?>").dialog("close");}, 4000);
    } 
 }

/* Функция закрытия окна при удачном входе пользователя*/
function close_reg_window(data){  
<?php if($this->redirect_to_backend && $reg==true) 
{?>
    setTimeout( function() {
        window.location='<?php echo BACKEND_SITE_URL?>';
        }, 4000);
<?php } ?>
<?php if($this->refresh && $reg==true) 
{?>
    setTimeout( function() {
        window.location='<?php echo $_SERVER["REQUEST_URI"];?>';
        }, 4000);
<?php } ?> 
    if ($('#loginreg<?php echo $blockId; ?>', '<div>'+data+'</div>').length>=1){
        /*$("#loginlink<?php echo $blockId; ?>").remove();
        $("#reglink<?php echo $blockId; ?>").remove();
        $("#blocklayout<?php echo $blockId; ?>").append('<?php echo $loguotlink; ?>');*/
          setTimeout( function() {
        $("#registerForm<?php echo $blockId; ?>").dialog("close");}, 4000);
    } 
 }
</script>



<?php /* ФОрма авторизации*/ ?>
<?php 
if ($login) {
    echo CHtml::hiddenField('login'.$blockId,$login,array('id'=>'login'.$blockId));
    echo t('Спасибо что вошли.');
} else {
    ?>
    <div class="dialog-form" id="loginForm<?php echo $blockId; ?>"><?php
        $this->render(GxcHelpers::getTruePath('front_blocks.signin.signin_block_output'),array(
            'blockId' => $blockId,
            'model' => $model,
            'ajaxOptions' => array(
                'success' => 'js:function(data, textStatus, XMLHttpRequest) {
                    $("#block'.$blockId.'").html($("#block'.$blockId.'","<div>"+data+"</div>").html());
                    '.($this->onLoginSuccess ? '('.$this->onLoginSuccess.')($("#loginForm'.$blockId.'")[0]);' : '').'
                    close_login_window(data);
                }',
                'error' => 'js:function(XMLHttpRequest, textStatus, errorThrown) { 
                    $("#block'.$blockId.'").html(\'<h3 class="error">Произошла ошибка на сервере.<br/>Перезагрузите страницу и повторите позже.</h3>\');
                }',
                'complete' => 'js:function(XMLHttpRequest, textStatus, errorThrown) {  }',
            ),
        )) ;
    ?>
    </div>
<?php
} 
?>

<?php /*Форма регистрации */ ?>
<?php if($reg) {
    echo CHtml::hiddenField('loginreg'.$blockId,$login,array('id'=>'loginreg'.$blockId));
    echo t('Спасибо что зарегистрировались.<br/> Вам на почту было отправленно письмо для подтверждения регистрации.');
 } else {
?>
    <div class="dialog-form" id="registerForm<?php echo $blockId; ?>" >
        <?php $this->render(GxcHelpers::getTruePath('front_blocks.signup.signup_block_output'), array(
            'blockId' => $blockId,
            'model'=>$reg_model,
            'block'=>$this->block,
            'ajaxOptions' => array(       
                'success' => 'js:function(data, textStatus, XMLHttpRequest) {
                    $("#registerForm'.$blockId.'").html($("#registerForm'.$blockId.'","<div>"+data+"</div>").html());
                    console.log("asd");
                 close_reg_window(data);
                }',
                'error' => 'js:function(XMLHttpRequest, textStatus, errorThrown) { 
                    $("#registerForm'.$blockId.'").html(\'<h3 class="error">Произошла ошибка на сервере.<br/>Перезагрузите страницу и повторите позже.</h3>\');
                }',
                'complete' => 'js:function(XMLHttpRequest, textStatus, errorThrown) {  }'
            ),
        )) ?>
    </div>
<?php
} 
?>
<?php /* Конец формы регистрации */ ?>

<?php /* ссылка для входа и скрипт открытия окна. */ ?>
<a id="loginlink<?php echo $blockId; ?>" class="login_link" href="#"><?php echo t('Login') ?></a>
<a id="reglink<?php echo $blockId; ?>" class="registration_link" href="#"><?php echo t('Register') ?></a>
<script>

    $(function() {
        <?php /*  Открытие формы авторизации*/ ?>
        $('#loginForm<?php echo $blockId; ?>').dialog({
            autoOpen: false,
            height: 460,
            width: 350,
            modal: true,
            resizable: false
        });
        <?php /*  Открытие формы регистрации*/ ?>
        $('#registerForm<?php echo $blockId; ?>').dialog({
            autoOpen: false,
            height: 550,
            width: 375,
            modal: true,
            resizable: false
        });

        $('#loginlink<?php echo $blockId; ?>, .login-link').live('click', function(event) {
            event.preventDefault();
                $( "#registerForm<?php echo $blockId; ?>" ).dialog( "close" );
                $( "#loginForm<?php echo $blockId; ?>" ).dialog( "open" );

            });
        $('#reglink<?php echo $blockId; ?> , .reg-link').live('click', function(event) {
            event.preventDefault();
                $( "#loginForm<?php echo $blockId; ?>" ).dialog( "close" );
                $( "#registerForm<?php echo $blockId; ?>" ).dialog( "open" );
            });
    });
</script>
<?php } else { echo $loguotlink; } /* Вывод ссылки выхода если пользователь не залогинен. */ ?>
</div>
