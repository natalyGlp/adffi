<?php

/**
 * Class for render Sign in Box ajax
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.signin
 */
// TODO: пускай этот класс наследует SignupBlock
class SigninAjaxBlock extends BlockWidget
{
    public $refresh=false;
    public $redirect_to_backend=false;
    /**
     * @var string $onLoginSuccess функция, которая будет вызвана при успешном логине
     */
    public $onLoginSuccess = false;
    // public $signInSelector = false; // jQuery селектор для вызова диалога входа
    // public $signUpSelector = false; // jQuery селектор для вызова диалога регистрации

  public function run()
  {     
    Yii::app()->clientScript->registerCoreScript('jquery.ui');
    Yii::app()->clientScript->registerCssFile(
        Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');
    Yii::app()->clientScript->registerCssFile(
        Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery.ui.dialog.css');

    $this->renderContent();
}       


protected function renderContent()
{
    $login=false;
    $reg=false; 
    if(isset($_GET['required'])){        	
    	user()->setFlash('error',t('You need to sign in before continue'));
    }
    /* Проверка авторизации пользователя*/
    $model=new UserLoginForm;

    $reg_model=new UserRegisterForm;
     /*$reg_model->username='qwe'.time();
    $reg_model->email='qwe'.time().'@asd.ru';
    $reg_model->password='qweqwe';*/
	// if it is ajax validation request
    if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
    {
         echo CActiveForm::validate($model);
         Yii::app()->end();
     }

	// collect user input data
    if(isset($_POST['UserLoginForm']))
    {
        $model->attributes=$_POST['UserLoginForm'];
		// validate user input and redirect to the previous page if valid
        if($model->validate() && $model->login()){
            $login=true;
        }
    } 
    /* конец проверки авторизации пользователя*/       
    /* Проверка регистрации пользователя*/      
    if(isset($_POST['UserRegisterForm'])){
        $reg_model->attributes=$_POST['UserRegisterForm'];

        // validate user input password
        $reg = $reg_model->doSignUp();
    }      
    /* Проверка регистрации пользователя*/            
    $this->render('output',array(
        'model'=>$model,
        'login'=>$login,
        'reg_model'=>$reg_model, 
        'reg'=>$reg
        ));

}
public function params()
{
   return array(
    'redirect_to_backend'=>t('Redirect to Adminpanel'),
    'refresh'=>t('Refresh the page'));
}
}

?>