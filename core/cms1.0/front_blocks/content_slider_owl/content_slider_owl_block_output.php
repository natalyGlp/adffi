<?php
/**
 * @var string $id
 * @var string $cssClass
 * @var array $nav
 * @var array $slides
 */
?>
<div class="<?php echo $cssClass; ?>">
    <?php
    if (!empty($title)) {
        echo '<div class="title"><h2>';
        if (!empty($titleLink)) {
            echo CHtml::link(t($title), $titleLink);
        } else {
            echo t($title);
        }
        echo '</h2></div>';
    }
    ?>

    <div id="<?php echo $id ?>" class="owl-carousel">
        <?php
            foreach ($slides as $slide) {
                echo CHtml::tag('div', array(
                    'class' => '',
                ), $slide);
            }
        ?>
    </div>
</div>

