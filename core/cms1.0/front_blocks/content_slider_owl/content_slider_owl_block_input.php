<div name="div-block-content-<?php echo $block_model->id;?>">
    <?php include Yii::getPathOfAlias('cms.front_blocks.content_slider_sliderkit.content_slider_base_form').'.php'; ?>

    <div class="row">
        <?php ob_start(); ?>
            <div class="row">
                <?php echo $form->textField($block_model, 'itemsCustom[$index][0]', array(
                    'placeholder' => 'Min window width *',
                )); ?>
                <?php echo $form->textField($block_model, 'itemsCustom[$index][1]', array(
                    'placeholder' => 'Items in slide *',
                )); ?>
                <a href="#" class="btn btn-danger js-tabular-form-remove">
                    <i class="icon icon-remove"></i>
                </a>
            </div>
        <?php $template = ob_get_clean(); ?>
        <?php echo $form->labelEx($block_model, 'itemsCustom'); ?>
        <?php $this->beginWidget('cms.extensions.tabular-form.TabularFormWidget', array(
            'addButtonLabel' => _t('Add Breakpoint'),
            'template' => $template,
        )) ?>
        <?php foreach ($block_model->itemsCustom as $index => $item) {
            ?>
            <div class="row">
                <?php echo $form->textField($block_model, "itemsCustom[$index][0]", array(
                    'placeholder' => 'Min window width',
                    'value' => $item[0],
                )); ?>
                <?php echo $form->textField($block_model, "itemsCustom[$index][1]", array(
                    'placeholder' => 'Items in slide',
                    'value' => $item[1],
                )); ?>
                <a href="#" class="btn btn-danger js-tabular-form-remove">
                    <i class="icon icon-remove"></i>
                </a>
            </div>
            <?php
        } ?>
        <?php $this->endWidget('cms.extensions.tabular-form.TabularFormWidget') ?>
        <?php echo $form->error($block_model, 'itemsCustom'); ?>
    </div>

    <div class="alert alert-info">
        <?= _t('This allows you to set the number of slides for particular window width. Just specify the minimum width of window and the amount of slides to show. For example [[0, 1], [600, 3]] means show 1 slide for all devices with width from 0px to 599px for devices >= 600px show 3 slides.') ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'singleItem'); ?>
        <?php echo $form->labelEx($block_model, 'singleItem'); ?>
        <?php echo $form->error($block_model, 'singleItem'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'itemsScaleUp'); ?>
        <?php echo $form->labelEx($block_model, 'itemsScaleUp'); ?>
        <?php echo $form->error($block_model, 'itemsScaleUp'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'slideSpeed'); ?>
        <?php echo $form->numberField($block_model, 'slideSpeed'); ?>
        <?php echo $form->error($block_model, 'slideSpeed'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'paginationSpeed'); ?>
        <?php echo $form->numberField($block_model, 'paginationSpeed'); ?>
        <?php echo $form->error($block_model, 'paginationSpeed'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'rewindSpeed'); ?>
        <?php echo $form->numberField($block_model, 'rewindSpeed'); ?>
        <?php echo $form->error($block_model, 'rewindSpeed'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'autoPlay'); ?>
        <?php echo $form->numberField($block_model, 'autoPlay'); ?>
        <?php echo $form->error($block_model, 'autoPlay'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'stopOnHover'); ?>
        <?php echo $form->labelEx($block_model, 'stopOnHover'); ?>
        <?php echo $form->error($block_model, 'stopOnHover'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'navigation'); ?>
        <?php echo $form->labelEx($block_model, 'navigation'); ?>
        <?php echo $form->error($block_model, 'navigation'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'nextButtonLabel'); ?>
        <?php echo $form->textField($block_model, 'nextButtonLabel'); ?>
        <?php echo $form->error($block_model, 'nextButtonLabel'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'prevButtonLabel'); ?>
        <?php echo $form->textField($block_model, 'prevButtonLabel'); ?>
        <?php echo $form->error($block_model, 'prevButtonLabel'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'rewindNav'); ?>
        <?php echo $form->labelEx($block_model, 'rewindNav'); ?>
        <?php echo $form->error($block_model, 'rewindNav'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'scrollPerPage'); ?>
        <?php echo $form->labelEx($block_model, 'scrollPerPage'); ?>
        <?php echo $form->error($block_model, 'scrollPerPage'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'pagination'); ?>
        <?php echo $form->labelEx($block_model, 'pagination'); ?>
        <?php echo $form->error($block_model, 'pagination'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'paginationNumbers'); ?>
        <?php echo $form->labelEx($block_model, 'paginationNumbers'); ?>
        <?php echo $form->error($block_model, 'paginationNumbers'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'responsive'); ?>
        <?php echo $form->labelEx($block_model, 'responsive'); ?>
        <?php echo $form->error($block_model, 'responsive'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'responsiveRefreshRate'); ?>
        <?php echo $form->numberField($block_model, 'responsiveRefreshRate'); ?>
        <?php echo $form->error($block_model, 'responsiveRefreshRate'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'responsiveBaseWidth'); ?>
        <?php echo $form->textField($block_model, 'responsiveBaseWidth'); ?>
        <?php echo $form->error($block_model, 'responsiveBaseWidth'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'lazyLoad'); ?>
        <?php echo $form->labelEx($block_model, 'lazyLoad'); ?>
        <?php echo $form->error($block_model, 'lazyLoad'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'lazyFollow'); ?>
        <?php echo $form->labelEx($block_model, 'lazyFollow'); ?>
        <?php echo $form->error($block_model, 'lazyFollow'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'lazyEffect'); ?>
        <?php echo $form->dropDownList(
            $block_model,
            'lazyEffect',
            $block_model->getLazyEffectsList(),
            array(
                'empty' => _t('No effect'),
            )
        ); ?>
        <?php echo $form->error($block_model, 'lazyEffect'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'autoHeight'); ?>
        <?php echo $form->labelEx($block_model, 'autoHeight'); ?>
        <?php echo $form->error($block_model, 'autoHeight'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'dragBeforeAnimFinish'); ?>
        <?php echo $form->labelEx($block_model, 'dragBeforeAnimFinish'); ?>
        <?php echo $form->error($block_model, 'dragBeforeAnimFinish'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'mouseDrag'); ?>
        <?php echo $form->labelEx($block_model, 'mouseDrag'); ?>
        <?php echo $form->error($block_model, 'mouseDrag'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'touchDrag'); ?>
        <?php echo $form->labelEx($block_model, 'touchDrag'); ?>
        <?php echo $form->error($block_model, 'touchDrag'); ?>
    </div>

    <div class="row">
        <?php echo $form->checkBox($block_model, 'addClassActive'); ?>
        <?php echo $form->labelEx($block_model, 'addClassActive'); ?>
        <?php echo $form->error($block_model, 'addClassActive'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($block_model, 'transitionStyle'); ?>
        <?php echo $form->dropDownList(
            $block_model,
            'transitionStyle',
            $block_model->getTransitionStylesList(),
            array(
                'empty' => _t('No transition'),
            )
        ); ?>
        <?php echo $form->error($block_model, 'transitionStyle'); ?>
    </div>
</div>
