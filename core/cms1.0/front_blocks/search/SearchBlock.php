<?php
/**
 * Class for render search results
 *
 * @author Sviatoslav <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.search
 */

Yii::import(GxcHelpers::getTruePath('front_blocks.listview.ListViewBlock'));
Yii::import('cms.models.search.*');
class SearchBlock extends ListViewBlock
{
    public $allowSearch = true;
    public $display_type = self::DISPLAY_TYPE_SEARCH;

    public function run()
    {
        if (!isset($_GET['q']) && !isset($_GET['date']) && !isset($_GET['term']) && !isset($_GET['tag']) && !isset($_GET['author_id'])) {
            throw new CHttpException('404', t('Oops! Page not found!'));
        }

        parent::run();
    }
}
