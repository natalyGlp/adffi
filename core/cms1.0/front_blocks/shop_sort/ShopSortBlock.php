<?php

class ShopSortBlock extends BlockWidget
{
    public $sort_order;
    public $sort_type;
    public $default_order_value = 'desc';
    public $default_type_value = 'price';
    public $path = '';
    public $sort_orders = array(
        'desc' => 'По убыванию',
        'asc' => 'По возростанию',
    );
    public $sort_types = array(
        'price' => 'По цене',
    );
    public $buttonText = "Отсортировать";

    public function run()
    {
        $this->renderContent();
    }

    public function renderContent()
    {
        $this->render('output', array());
    }

    public function params()
    {
        return array(
            'sort_order' => t("Sort Order"),
        );
    }
}