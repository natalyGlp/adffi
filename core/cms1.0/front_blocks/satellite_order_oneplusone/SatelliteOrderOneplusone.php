<?php
/**
 * Акционный блок 1+1 для сателлитной сети
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.satellite_order_oneplusone
 */

Yii::import(GxcHelpers::getTruePath('front_blocks.satellite_owlslider.SatelliteOwlsliderBlock'));
Yii::import(GxcHelpers::getTruePath('front_blocks.satellite_order_form.models.SatelliteOrderFormModel'));
Yii::import(GxcHelpers::getTruePath('front_blocks.satellite_order_form.SatelliteOrderFormBlock'));

class SatelliteOrderOneplusone extends SatelliteOrderFormBlock
{
    protected $_api;

    public function init()
    {
        parent::init();

        $this->_api = new SatelliteOrderApi();

        SatelliteOwlsliderBlock::registerOwlSlider();
    }

    public function run()
    {
        // http://owlgraphic.com/owlcarousel/#customizing
        $firstId = 'offerl-slider' . $this->getId();
        $secondId = 'offerr-slider' . $this->getId();
        $containerId = 'oneplusone-form-' . $this->getId();
        $options = CJavaScript::encode(array(
            'pagination' => false,
            'navigation' => true,
            'singleItem' => true,
            'afterInit' => 'js:getSpecialOfferFunc(i)',
            'afterMove' => 'js:getSpecialOfferFunc(i++)',
        ));

        Yii::app()->clientScript->registerScript(__FILE__ . '#' . $this->getId(), '
            var getSpecialOfferFunc = function(index) {
                return function($el) {
                    var title = $el.find(".product-name").eq(this.owl.currentItem).text();
                    $("#' . $containerId . ' .product-name").eq(index).html(title);

                    $("#onepluseone_product"+index).val($el.find(".product-id").eq(this.owl.currentItem).val());
                };
            },
            i = 0;

            $("#' . $firstId . '").owlCarousel(' . $options . ');
            $("#' . $secondId . '").owlCarousel(' . $options . ');
        ');

        $model = new SatelliteOrderFormModel();

        $products2 = $this->_api->products;
        $products2[] = array_shift($products2);
        $this->render('output', array(
            'model' => $model,
            'products1' => $this->_api->products,
            'products2' => $products2,
            'firstId' => $firstId,
            'secondId' => $secondId,
        ));
    }
}
