<?php
/**
 * @var $block_model ShareSocialLikesBlock
 */
?>
<div name='div-block-content-<?= $block_model->id; ?>'>

    <div class='row'>
        <?= CHtml::label(Block::getLabel($block_model, 'widgetOrientation'), ''); ?>
        <?= CHtml::dropdownList('Block[widgetOrientation]', $block_model->widgetOrientation, array(
            'social-likes_vertical' => _t('Vertical'),
        ), array('id' => 'Block-widgetOrientation', 'empty'=>_t('Horizontal'))); ?>
        <?= $form->error($model, 'widgetOrientation'); ?>
    </div>

    <div class='row'>
        <?= CHtml::label(Block::getLabel($block_model, 'facebook'), ''); ?>
        <?= CHtml::checkBox('Block[facebook]', $block_model->facebook, array('id' => 'Block-facebook')); ?>
        <?= $form->error($model, 'facebook'); ?>
    </div>

    <div class='row'>
        <?= CHtml::label(Block::getLabel($block_model, 'vkontakte'), 'Block-need_vk'); ?>
        <?= CHtml::checkBox('Block[vkontakte]', $block_model->vkontakte, array('id' => 'Block-vkontakte')); ?>
        <?= $form->error($model, 'vkontakte'); ?>
    </div>

    <div class='row'>
        <?= CHtml::label(Block::getLabel($block_model, 'plusone'), ''); ?>
        <?= CHtml::checkBox('Block[plusone]', $block_model->plusone, array('id' => 'Block-plusone')); ?>
        <?= $form->error($model, 'plusone'); ?>
    </div>

    <div class='row'>
        <?= CHtml::label(Block::getLabel($block_model, 'twitter'), ''); ?>
        <?= CHtml::checkBox('Block[twitter]', $block_model->twitter, array('id' => 'Block-twitter')); ?>
        <?= $form->error($model, 'twitter'); ?>
    </div>
</div>
