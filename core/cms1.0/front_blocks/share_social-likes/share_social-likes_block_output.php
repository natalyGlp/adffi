<?php
    $cs = Yii::app()->clientScript;
    $scriptFile = YII_DEBUG ? 'social-likes.js' : 'social-likes.min.js';
    $cs->registerScriptFile($this->_assetsUrl.'/js/'.$scriptFile, CClientScript::POS_HEAD);
    $cs->registerCssFile($this->_assetsUrl.'/css/social-likes_flat.css');
?>
<?= CHtml::openTag('div', array('class'=>$this->cssClass)); ?>
    <?= CHtml::openTag('div', array('class'=>'social-likes '.$this->widgetOrientation)); ?>
        <?= isset($this->vkontakte) ? CHtml::tag('div', array('class' => 'vkontakte'), 'Вконтакте') : ''; ?>
        <?= isset($this->facebook) ? CHtml::tag('div', array('class' => 'facebook'), 'Facebook') : ''; ?>
        <?= isset($this->twitter) ? CHtml::tag('div', array('class' => 'twitter'), 'Twitter') : ''; ?>
        <?= isset($this->plusone) ? CHtml::tag('div',array('class' => 'plusone'), 'Google+') : ''; ?>
    <?= CHtml::closeTag('div'); ?>
<?= CHtml::closeTag('div'); ?>
