<?php
/**
 * Class for render SocialButtons
 * 
 * @author Bazai best programmer <developer@studiobanzai.com>
 * @version 0.1
 * @package front_blocks.socialwidgets 
 */

class SocialWidgetsBlock extends BlockWidget
{
    public $need_fb;
    public $need_vk;
    public $need_gp;
    public $need_yt;
    public $need_tw;
    public $need_sk;
    public $need_ld;
    public $need_od;
    public $need_vi;
    public $need_rss;
    public $need_in;
    public $need_dr;
    public $need_pr;
    public $need_lj;
    public $social_email;

    public $title;
    
    public function run()
    {    
        $this->renderContent();         
    }       
 
 
    protected function renderContent()
    {     
        $this->render('output',array());                                                          	       		     
    }
    
    public function params()
    {
        return array(
            'title' => t('Block title'),
            'need_fb' => t('Show Facebook widget'),
            'need_vk' => t('Show Vkontakte widget'),
            'need_gp' => t('Show Gmail widget'),
            'need_yt' => t('Show Youtube widget'),
            'need_sk' => t('Show Skype widget'),
            'need_ld' => t('Show LinkedIn widget'),
            'need_tw' => t('Show Twitter widget'),
            'need_od' => t('Show Odnoklassniki widget'),
            'need_vi' => t('Show vimeo widget'),
            'need_rss' => t('Show rss widget'),
            'need_in' => t('Show instagram widget'),
            'need_dr' => t('Show Dribbbble widget'),
            'need_pr' => t('Show Pinterest widget'),
            'need_lj' => t('Show livejournal widget'),
            );
    }
}
?>