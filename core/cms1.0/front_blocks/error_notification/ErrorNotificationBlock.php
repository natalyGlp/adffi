<?php

/**
 * Class for render form for Error Notification
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package front_blocks.error_notification
 */

class ErrorNotificationBlock extends BlockWidget
{
    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        if (($error = Yii::app()->errorHandler->error)) {
            $this->render('output', array(
                'code' => $error['code'],
                'message' => $error['message'],
                ));
        }
    }
}
