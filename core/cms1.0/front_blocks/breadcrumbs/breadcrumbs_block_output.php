<?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
        'separator' => '<span class="'.$this->cssClass.'__separator">' . $this->separator . '</span>',
        'activeLinkTemplate' => '<a href="{url}" itemprop="url"><span itemprop="title">{label}</span></a>',
        'inactiveLinkTemplate' => '<span itemprop="title">{label}</span>',
        'htmlOptions' => array(
            'class' => $this->cssClass,
            'itemscope' => true,
            'itemtype' => 'http://data-vocabulary.org/Breadcrumb',
            ),
        'links' => $this->controller->breadcrumbs
    ));
