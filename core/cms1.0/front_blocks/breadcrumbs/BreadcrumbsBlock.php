<?php
/**
 * Class for render Breadcrumbs
 *
 * @author Tuan Nguyen <voinovev@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.breadcrumbs
 */
class BreadcrumbsBlock extends BlockWidget
{
    public $cssClass = 'breadcrumbs';
    public $separator = '&rarr;';
    private $placeholder;

    public function run()
    {
        // на главной странице хлебные крошки незачем
        if (Yii::app()->request->requestUri == '/') {
            return;
        }

        Yii::app()->controller->attachEventHandler('onAfterRender', array($this, 'renderContent'));

        // этот плейсхолдер мы заменим после того, как вся страница отрендерится
        $this->placeholder = "{{breadcrumbs{$this->getId()}}}";
        echo $this->placeholder;
    }

    protected function renderContent($event)
    {
        $widget = $this->render('output', array(), true);
        $event->params['output'] = str_replace($this->placeholder, $widget, $event->params['output']);
    }

    public function params()
    {
        return array(
            'separator' => t('Link Separator'),
        );
    }
}
