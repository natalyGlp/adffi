<?php

/**
 * Class for render Content based on Content list
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.html
 */
class ContentSliderFewBlock extends BlockWidget 
{
    /* Slider propertys */
    public $width = 900;
    public $full_height = 200;
    public $nav_height = 50;
    public $imageSize = 'list_view_';

    /* add params */
    public $autoplay = true; 
    public $circular = true; 
    public $mousewheel = true; 
    public $navitemshover = false; 
    public $panelfx = 'sliding'; 
    public $panelfxspeed = '1000';
    public $autospeed = 1000; 
    public $content_list;
    public $display_type = self::DISPLAY_TYPE_HOMEPAGE;
    public $count=2;

    //Display types for the list view render 

    const DISPLAY_TYPE_HOMEPAGE = 0;
    const DISPLAY_TYPE_CATEGORY = 1;

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        $url = Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.assets.frontend'), false);
        Yii::app()->clientScript->registerCssFile($url . "/slider/css/sliderkit-core.css");
        Yii::app()->clientScript->registerScriptFile($url . "/slider/js/external/jquery.mousewheel.min.js");
        Yii::app()->clientScript->registerScriptFile($url . "/slider/js/external/jquery.easing.1.3.min.js");
        Yii::app()->clientScript->registerScriptFile($url . "/slider/js/sliderkit/jquery.sliderkit.1.9.2.pack.js");
        $this->render('output', array());
    }

    public function validate()
    {
        Yii::import(GxcHelpers::getTruePath('front_blocks.contentslider.ContentSlider'));
        $model = new ContentSlider;
      //  var_dump($_POST['Block']); die();
        $model->attributes = $_POST['Block'];
        if ($model->validate())
        {
            return true;
        } else
        {
            $this->errors = $model->getErrors();
            return false;
        }
    }

    public function params()
    {
        return array(
            'content_list' => t('Content list'),
            'display_type' => t('Display type'),
            'width' => t('Width'),
            'imageSize' => t('Image size'),
            'autoplay' => t('Autoplay'),
            'circular' => t('circular'),
            'mousewheel' => t('Mousewheel'),
            'panelfx' => t('Panel Effect'),
            'panelfxspeed' => t('Panel Effect Speed'),
            'autospeed' => t('Slider Speed'),
            'navitemshover' => t('Change slide when navigation hover. '),
            'nav_height' => t('Navigation height'),
            'full_height' => t('Full height'),
            'count' => t('Count Items in slide'),
        );
    }

    public function getSlideEffects()
    {
        return array(
            'sliding' => 'Sliding',
            'fading' => 'Fading',
            'none' => 'none',
        );
    }

    public static function getDisplayTypes()
    {
        return array(
            self::DISPLAY_TYPE_HOMEPAGE => t("Display in Homepage"),
            self::DISPLAY_TYPE_CATEGORY => t("Display in Category page"));
    }

    public static function getContentList($content_list_id, $max = null, $pagination = null, $return_type = ConstantDefine::CONTENT_LIST_RETURN_ACTIVE_RECORD)
    {
        return GxcHelpers::getContentList($content_list_id, $max, $pagination, $return_type);
    }
}

?>