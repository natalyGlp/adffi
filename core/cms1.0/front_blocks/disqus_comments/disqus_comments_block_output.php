<?php 
$id = empty($this->containerId) ? 'disqus_thread'.$this->getId() : $this->containerId;
?>
<div class="comments<?php echo($this->cssClass)?" ".$this->cssClass:""; ?>" itemtype="http://schema.org/Comment">
<?php if(empty($this->containerId))
{
    echo CHtml::tag('div', array(
        'id' => $id,
        ), 'Загрузка комментариев ...');
}
?>
    <script type="text/javascript">
        var disqus_shortname = '<?php echo $this->shortname?>';
        var disqus_container_id = '<?php echo $id ?>';
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
</div>
