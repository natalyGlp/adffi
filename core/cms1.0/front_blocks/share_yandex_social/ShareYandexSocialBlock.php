<?php

/**
 * Class ShareYandexSocialBlock
 * @site http://api.yandex.ru/share/
 */
class ShareYandexSocialBlock extends BlockWidget
{
    public $vkontakte;
    public $facebook;
    public $twitter;
    public $plusone;

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        $this->render('output',array());
    }

    public function params()
    {
        return array(
            'vkontakte' => t('Show Vkontakte Like button'),
            'facebook' => t('Show Facebook Like button'),
            'twitter' => t('Show Twitter Like button'),
            'plusone' => t('Show G+ Like button'),
        );
    }
}
