<?php
Yii::app()->clientScript->registerScriptFile('//yastatic.net/share/share.js', CClientScript::POS_END);
?>
<?= CHtml::openTag('div', array('class'=>$this->cssClass)); ?>
    <?= CHtml::tag('div', array(
        'data-yashareType' => $this->widgetStyle ? $this->widgetStyle : '',
        'data-yashareQuickServices' => $this->joinSNs(),
        'class' => 'yashare-auto-init',
        'data-yashareTheme' => 'counter',
        )
    );
    ?>
<?= CHtml::closeTag('div'); ?>
