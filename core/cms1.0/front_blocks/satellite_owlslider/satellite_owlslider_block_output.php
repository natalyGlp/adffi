<div <?php if ($this->cssClass) echo 'class="'.$this->cssClass.'"'; ?>>

  <h2>Выбери свою обложку</h2>

  <p>подчеркни свою индивидуальность</p>

  <div class="satellite-owlslider-container">
    <?php if(count($products) > 0): ?>
      <div id="<?php echo $this->getId(); ?>">
        <?php
          foreach ($products as $product) {
            ?>
              <div class="item">
                <?php if(isset($product['photos'][0])): ?>
                  <img src="<?php echo $product['photos'][0]['link'] ?>">
                  <img src="<?php echo isset($product['photos'][1]) ? $product['photos'][1]['link'] : $product['photos'][0]['link'] ?>" class="product-image-large" style="display: none;" />
                <?php endif; ?>
                <div class="content">
                  <h3 class="product-name"><?php echo $product['name'] ?></h3>                
                  <a class="green btn-open-order">Заказать</a>
                </div>
                <?php if($product['oldPrice'] > 0): ?>
                  <p class="old-price"><s><span class="product-old-price"><?php echo $product['oldPrice'] ?></span> грн</s></p>
                <?php endif; ?>
                <p class="new-price"><span class="product-price"><?php echo $product['price'] ?></span> <small>грн</small></p>
                <input type="hidden" class="product-id" value="<?php echo $product['id'] ?>">
              </div>
            <?php
          }
        ?>         
      </div>
    <?php endif; ?>
  </div><!-- . -->
</div>
