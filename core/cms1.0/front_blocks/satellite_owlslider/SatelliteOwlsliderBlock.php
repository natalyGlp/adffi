<?php
// TODO: шорткат для создания классов плейсхолдеров
// TODO: подписи запросов
/**
 * Блок формы оформления заказа для сателлитной сети
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package common.front_blocks.satellite_owlslider
 */

Yii::import('cms.components.SatelliteOrderApi');
class SatelliteOwlsliderBlock extends BlockWidget
{
    protected $_api;

    public function init()
    {
        parent::init();

        $this->_api = new SatelliteOrderApi();

        self::registerOwlSlider();
    }

    public function run()
    {
        // http://owlgraphic.com/owlcarousel/#customizing
        Yii::app()->clientScript->registerScript(__FILE__ . '#' . $this->getId(), '$("#' . $this->getId() . '").owlCarousel(' . CJavaScript::encode(array(
            'pagination' => false,
            'navigation' => true,

            'items' => 3,
            'itemsCustom' => array(array(0, 1), array(767, 3)),
            'theme' => 'owl-satellite-products'
        )) . ')');

        $this->renderContent();
    }

    protected function renderContent()
    {
        $this->render('output', array(
            'products' => $this->_api->products,
        ));
    }

    public static function registerOwlSlider()
    {
        $asstesUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', false, -1, YII_DEBUG);
        Yii::app()->clientScript
                  ->registerCoreScript('jquery')
                  ->registerCssFile($asstesUrl . '/libs/owl-carousel/owl.carousel.css')
                  ->registerCssFile($asstesUrl . '/libs/owl-carousel/owl.transitions.css')
                  ->registerScriptFile($asstesUrl . '/libs/owl-carousel/owl.carousel.min.js', CClientScript::POS_END)
        ;
    }
}
