<div name="div-block-content-<?= $block_model->id;?>">
    <div class="row">
        <?= $form->checkBox($block_model, 'useWrapper'); ?>
        <?= $form->labelEx($block_model, 'useWrapper'); ?>
        <?= $form->error($block_model, 'useWrapper'); ?>
    </div>
    <div class="row">
        <?= $form->checkBox($block_model, 'useWysiwyg'); ?>
        <?= $form->labelEx($block_model, 'useWysiwyg'); ?>
        <?= $form->error($block_model, 'useWysiwyg'); ?>
    </div>
    <div class="row">
        <?= $form->labelEx($block_model, 'html'); ?>
        <?php $this->widget('cms.extensions.ckeditor.CKEditor', array(
            'name' => 'Block[html]',
            'model' => $block_model->html,
            'onOffButtonSelector' => '#'.CHtml::activeId($block_model, 'useWysiwyg'),
            'htmlOptions' => array(
                'style' => 'width:90%',
                'rows' => 15,
            ),
        )); ?>
        <?= $form->error($block_model, 'html'); ?>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScript(__FILE__, '$("#'.CHtml::activeId($block_model, 'cssClass').'").change(function() {
    // useWrapper обязателен, если указан css класс
    if(this.value != "") {
        $("#'.CHtml::activeId($block_model, 'useWrapper').'").prop("checked", "checked").change();
    }
}).change()');
