<?php

/**
 * Class for render Block
 *
 *
 * @author Yura Kovalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package common.front_blocks.html
 */

class HtmlBlock extends BlockWidget
{
    public $html;
    public $useWysiwyg = false;
    public $useWrapper = false;

    public function run()
    {
        $this->render('output', array(
            'content' => $this->html,
            'useWrapper' => $this->useWrapper,
            'cssClass' => $this->cssClass,
        ));
    }

    public function validate()
    {
        if ($this->html == "") {
            $this->errors['html'] = t('HTML content is required');
            return false;
        } else {
            return true;
        }
    }

    public function params()
    {
        return array(
            'html' => t('Html Code'),
            'useWysiwyg' => t('Use Wysiwyg'),
            'useWrapper' => t('Use wrapper'),
        );
    }
}
