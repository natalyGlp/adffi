<?php
if ($useWrapper || !empty($cssClass)) {
    echo CHtml::tag('div', array(
        'class' => $cssClass,
    ), $content);
} else {
    echo $content;
}
