<?php
/**
 * @var Object $data
 * @var integer $index
 * @var string $imageSrc
 * @var string $slideSrc
 * @var string $thumbSrc
 * @var string $fullSrc
 */
?>
<div class="slider-thumb">
    <?php
    if ($imageSrc) {
        echo CHtml::image($imageSrc, $data->object_name);
    } else {
        echo $data->object_name;
    }
    ?>
</div>
