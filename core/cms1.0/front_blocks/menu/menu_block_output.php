<?php
/**
 * @var array $menu
 */
?>
<nav class="<?php echo $this->getAlign().' '.$this->cssClass; ?>">
    <span class="nav-toggle"></span>
<?php
if (!empty($this->title)) {
    echo '<h3 class="menu-title">';
    if (empty($this->title_href)) {
        echo $this->title;
    } else {
        echo CHtml::link($this->title, $this->title_href);
    }
    echo '</h3>';
}

echo $menu;
?>
</nav>
