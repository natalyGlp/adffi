<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'menu_id'), ''); ?>
        <?php
        echo CHtml::dropDownList("Block[menu_id]", $block_model->menu_id, MenuBlock::findMenu(), array(
            'id' => 'Block-menu_id',
        ));
        ?>
        <?php echo $form->error($model, 'menu_id'); ?>
    </div>
    <div class="taxonomy-params" id="taxonomy-params">
        <div class="row">
            <?php echo CHtml::label(Block::getLabel($block_model, 'term_id'), '');?>
            <?php
            echo CHtml::dropDownList("Block[term_id]", $block_model->term_id, MenuBlock::findTerms(), array(
                'id' => 'Block-term_id',
                'empty' => '---',
            ));
            ?>
            <?php echo $form->error($model, 'term_id'); ?>
        </div>

        <div class="row">
            <?php echo CHtml::checkBox("Block[term_with_objects]", $block_model->term_with_objects, array('id' => 'Block-term_with_objects')); ?>
            <?php echo CHtml::label(Block::getLabel($block_model, 'term_with_objects'), '');?>
            <?php echo $form->error($model, 'term_with_objects'); ?>
        </div>

        <div class="row">
            <?php echo CHtml::checkBox("Block[term_as_header]", $block_model->term_as_header, array('id' => 'Block-term_as_header')); ?>
            <?php echo CHtml::label(Block::getLabel($block_model, 'term_as_header'), '');?>
            <?php echo $form->error($model, 'term_as_header'); ?>
        </div>
    </div><!-- .taxonomy-params -->

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'menu_align'), '');?>
        <?php
        echo CHtml::dropDownList("Block[menu_align]",
                                $block_model->menu_align,
                                Menu::getMenuAlignType(),
                                array('id' => 'Block-menu_align')
                                );
        ?>
        <?php echo $form->error($model, 'menu_align'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title'), ''); ?>
        <?php echo CHtml::textField("Block[title]",
                                    $block_model->title,
                                    array('id' => 'Block-title')
                                    );
        ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title_href'), ''); ?>
        <?php echo CHtml::textField("Block[title_href]",
                                    $block_model->title_href,
                                    array('id' => 'Block-title_href')
                                    );
        ?>
        <?php echo $form->error($model, 'title_href'); ?>
    </div>

    <!--div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'max_levels'), '');?>
        <?php echo CHtml::textField("Block[max_levels]", $block_model->max_levels, array('id' => 'Block-max_levels')); ?>
        <?php echo $form->error($model, 'max_levels'); ?>
    </div-->

    <script type="text/javascript">
        $(function() {
            $('#Block-menu_id').change(function() {
                if($(this).val() == '<?php echo MenuBlock::HAS_TAXONOMY_ID ?>') {
                    $('#taxonomy-params').show(400);
                } else {
                    $('#taxonomy-params').hide(400);
                }
            }).change();
        });
    </script>
</div>