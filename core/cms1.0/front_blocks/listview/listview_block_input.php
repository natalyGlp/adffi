<div name="div-block-content-<?php echo $block_model->id;?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'viewPath'), ''); ?>
        <?php echo CHtml::dropDownList("Block[viewPath]", $block_model->viewPath, $block_model->getViewsFiles()); ?>
        <?php echo $form->error($model, 'viewPath'); ?>
        <div class="alert alert-info">View files should be in <b>view</b> directory</div>
    </div>
    <div class="row" style="display: none">
        <?php echo CHtml::label(Block::getLabel($block_model, 'display_type'), ''); ?>
        <?php echo CHtml::dropDownList("Block[display_type]", $block_model->display_type, ListViewBlock::getDisplayTypes()); ?>
        <?php echo $form->error($model, 'display_type'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title'), ''); ?>
        <?php echo CHtml::textField("Block[title]", $block_model->title); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title_link'), ''); ?>
        <?php echo CHtml::textField("Block[title_link]", $block_model->title_link); ?>
        <?php echo $form->error($model, 'title_link'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'allLinkText'), ''); ?>
        <?php echo CHtml::textField("Block[allLinkText]", $block_model->allLinkText); ?>
        <?php echo $form->error($model, 'allLinkText'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'allLink'), ''); ?>
        <?php echo CHtml::textField("Block[allLink]", $block_model->allLink); ?>
        <?php echo $form->error($model, 'allLink'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'allLinkPosition'), ''); ?>
        <?php echo CHtml::checkBoxList("Block[allLinkPosition]", $block_model->allLinkPosition, array(
            'before' => t('Before'),
            'after' => t('After'),
        )); ?>
        <?php echo $form->error($model, 'allLinkPosition'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'imageSize'), ''); ?>
        <?php $this->widget('cms.widgets.resource.ResourceSizePicker', array(
            'blockWidget' => $block_model,
            'attribute' => 'imageSize',
        )); ?>
        <?php echo $form->error($model, 'imageSize'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::checkBox("Block[allowSearch]", $block_model->allowSearch); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'allowSearch'), ''); ?>
        <?php echo $form->error($model, 'allowSearch'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::checkBox("Block[forbidNonSearchQueries]", $block_model->forbidNonSearchQueries); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'forbidNonSearchQueries'), ''); ?>
        <?php echo $form->error($model, 'forbidNonSearchQueries'); ?>
    </div>
    <div class="row">
        <?php
        Yii::import('cms.models.search.*');
        $indexList = SearchLucene::getIndexesList();
        if (!empty($indexList)) {
            ?>
            <?php echo CHtml::label(Block::getLabel($block_model, 'luceneIndex'), ''); ?>
            <?php
            echo CHtml::checkBoxList("Block[luceneIndex]", $block_model->luceneIndex, $indexList); ?>
            <?php echo $form->error($model, 'luceneIndex'); ?>
        <?php
        }
        ?>
    </div>
    <div class="row">
        <?php echo CHtml::checkBox("Block[display_object_layout_menu]", $block_model->display_object_layout_menu); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'display_object_layout_menu'), ''); ?>
        <?php echo $form->error($model, 'display_object_layout_menu'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::checkBox("Block[ajaxUpload]", $block_model->ajaxUpload); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'ajaxUpload'), ''); ?>
        <?php echo $form->error($model, 'ajaxUpload'); ?>
    </div>
    <?php $this->widget('cms.widgets.page.BlockContentListManageWidget', array(
        'blockWidget' => $block_model,
        'attribute' => 'content_list',
    )); ?>
</div>