<?php

/**
 * Class for render form for reset Avatar
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.reset_avatar
 */

class ResetAvatarBlock extends BlockWidget
{
    public function run()
    {       
        if(!user()->isGuest){ 
                $this->renderContent();
          } else {
             user()->setFlash('error',t('You need to sign in before continue'));                                                            
             Yii::app()->controller->redirect(bu().'/sign-in');
        }
    }       
 
    protected function renderContent()
    {     
        if(isset($_POST['ResetAvatar'])) {                        
            //So we will start to check the info from the user
            $current_user=User::model()->findByPk(user()->id);
            if($current_user) {
                if($current_user->avatar!=null && $current_user->avatar!='' ){
                    //We will delete the old avatar here
                    $old_avatar_path=$current_user->avatar;
                    $avatarsPath = Yii::getPathOfAlias('uploads.avatars');
                    if(file_exists($avatarsPath.'/root/'.$old_avatar_path)) {
					 @unlink($avatarsPath.'/root/'.$old_avatar_path);
					}                                     
                    //Delete old file Sizes
                    $sizes=AvatarSize::getSizes();
                    foreach($sizes as $size){
                      if(file_exists($avatarsPath.'/'.$size['id'].'/'.$old_avatar_path))
                          @unlink($avatarsPath.'/'.$size['id'].'/'.$old_avatar_path);
                                                             
                    }
					$current_user->avatar='';
					if($current_user->save()){
						echo "1";	
						Yii::app()->end();
					}
					 
                } 
            } else {
                throw new CHttpException('403','Wrong Link!');
            }                    
        }
		Yii::app()->controller->redirect(bu().'/profile');
    }
}

?>