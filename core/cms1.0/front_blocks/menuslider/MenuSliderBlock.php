<?php

/**
 * Class for render Top Menu
 *
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.search
 */

class MenuSliderBlock extends BlockWidget
{
    public $menu_id;
    public $menu_align = Menu::ALIGN_TYPE_FLOAT;
    public $title;
    public $title_href;

    public function run()
    {

        $this->renderContent();
    }

    protected function renderContent()
    {
        Yii::import('common.front_blocks.menu.*');
        $menu_r0_items = Yii::app()->cache->get('menu_r0_' . $this->menu_id);

        if ($menu_r0_items === false) {
            $menu_r0_items = self::getMenuItems(0, $this->menu_id);

            //Yii::app()->cache->set('menu_r0_'.$this->menu_id,$menu_r0_items,7200);
        }

        $this->render('output', array(
            'menus' => $menu_r0_items,
        ));
    }

    public function validate()
    {

        if ($this->menu_id == "") {
            $this->errors['menu_id'] = t('Please select a Menu');
            return false;
        } else {
            return true;
        }
    }

    public function params()
    {
        return array(
            'menu_id' => t('Menu'),
            'menu_align' => t('Menu align'),
            'title' => t('Menu title'),
            'title_href' => t('Menu title link')
        );
    }

    public function getAlign()
    {
        switch ($this->menu_align) {
            case Menu::ALIGN_TYPE_TOP:
                return 'top';
                break;
            case Menu::ALIGN_TYPE_BOTTOM:
                return 'bottom';
                break;
            case Menu::ALIGN_TYPE_LEFT:
                return 'left';
                break;
            case Menu::ALIGN_TYPE_RIGHT:
                return 'right';
                break;
            case Menu::ALIGN_TYPE_FLOAT:
                return 'float';
                break;

            default:
                return 'float';
                break;
        }
    }

    public static function getMenuItems($parent_id, $menu_id)
    {
        $menu_items = MenuItem::model()->findAll(
            array(
                'condition' => 'parent=:pid AND menu_id=:mid',
                'params' => array(':pid' => $parent_id, ':mid' => $menu_id),
                'order' => ' t.sort_order ASC ')
        );

        $result = array();
        foreach ($menu_items as $menu_item) {
            $result[] = array('name' => $menu_item->name, 'type' => $menu_item->type, 'value' => $menu_item->value, 'link' => self::buildLink($menu_item), 'id' => $menu_item->menu_item_id);
        }

        return $result;
    }

    public static function findMenu()
    {

        $result = array();
        $menus = Menu::model()->findAll();
        if ($menus) {
            foreach ($menus as $menu) {
                $result[$menu->menu_id] = $menu->menu_name;
            }
        }

        return $result;
    }

    public static function buildLink($item)
    {
        $cur_lang_id = Language::model()->findByAttributes(array('lang_name' => Yii::app()->language));
        switch ($item->type) {
            case Menu::TYPE_URL:
                return $item->value;

                break;
            case Menu::TYPE_PAGE:
                $page = Page::model()->findByPk(array($item->value));
                if ($page) {
                    return FRONT_SITE_URL . $cur_lang_id->lang_name . '/' . $page->slug;
                } else {
                    return 'javascript:void(0);';

                }
                break;
            case Menu::TYPE_CONTENT:
                $content = Object::model()->findByPk($item->value);
                if ($content) {
                    return $content->getObjectLink();
                } else {
                    return 'javascript:void(0);';

                }
                break;
            case Menu::TYPE_TERM:
                break;
            case Menu::TYPE_STRING:

                return $item->value;
                break;

            default:
                return $item->value;
                break;
        }
    }
}
