<?php
/**
 * @var $siteUrl
 * @var $logoSrc
 * @var $siteName
 * @var $cssClass
 */
?>
<div class="<?php echo $cssClass ?> logo">
	<a href="<?php echo $siteUrl; ?>">
        <?php
        if(!empty($logoSrc)) {
            echo CHtml::image($logoSrc, $siteName, array(
                'title' => $siteName,
                ));
        }
        ?>
	</a>
</div>