<?php
/**
 * @var string $code код счетчика
 * @var array $orderData информация о заказе
 * @var array $orderDataTpl тоже, что и $orderData, но с {ключами} для strtr()
 */
?>
<div style="overflow:hidden;width:0;height:0;opacity:0;">
  <?php echo $code; ?>
</div>
