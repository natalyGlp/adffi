<?php
/**
 * Блок для размещения кодов партнерских сетей на магазинах сателлитной сети
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package common.front_blocks.satellite_order_tracking
 */

Yii::import('cms.components.SatelliteOrderApi');
class SatelliteOrderTrackingBlock extends BlockWidget
{
    protected $_api;

    public $code;

    public function run()
    {    
        $this->_api = new SatelliteOrderApi();

        $this->renderContent();
    }

    protected function renderContent()
    {
        if(($orderData = SatelliteOrderApi::getOrderState())) {
            $orderDataTpl = array();

            foreach ($orderData as $key => $value) {
                if(is_array($value)) {
                    // TODO: было бы не плохо и массивы поддерживать
                    continue;
                }
                Yii::log("NATABLOCKS = ".$key." - ".print_r($value,true));
                $orderDataTpl['{'.$key.'}'] = $value;
            }
            
            $code = strtr($this->code, $orderDataTpl);

            $this->render('output', array(
                'code' => $code,
                'orderData' => $orderData,
                'orderDataTpl' => $orderDataTpl,
                ));
        }
    }

    public function params()
    {
        return array(
            'code' => t('Tracking code')
        );
    }
}
