<?php

/**
 * Class for render Block
 * 
 * 
 * @author Yura Kovalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package common.front_blocks.perelink
 */

class PerelinkBlock extends BlockWidget
{
    public $title = '';  
    
    public function run()
    {    
        Yii::import(GxcHelpers::getTruePath('front_blocks.listview.ListViewBlock'));    
        $this->renderContent();
    }  

    protected function renderContent()
    {
        $type = $this->controller->slug;
        $slug = $this->controller->objectSlug;


        $show_title = $slug == 'home' ? false : true;

        $modelName = 'Object';
        if($type) {
            $type = $type == 'parent_model' ? 'model' : $type;
            $modelName = Object::importByObjectType($type);
        }

        if($slug) {
            $content = $modelName::model()->with('language')->find(array('select'=>array('object_id'),  'condition'=>'object_slug=:paramId','params'=>array(':paramId'=>$slug))); 
        }

        if (isset($content)) {
            $post_id = $content->object_id; 
                
            if(!isset($post_id)) {
                $post_id=(int)$_GET['id'];
            }

            unset($content);
        }

        if(isset($post_id)) {
            $post = Yii::app()->cache->get(Yii::app()->request->url);
                            
            if(!$post) {
                $post = $modelName::model()->with('language')->find(array(
                    'condition' => 't.object_id=?',
                    'params' => array($post_id)
                ));    

                Yii::app()->cache->set(Yii::app()->request->url, $post, 7200);  
            }   

            $db = Yii::app()->{CONNECTION_NAME};

            $read_also = $db->createCommand()
                            ->select(array('read_also', 'read_also_limit', 'read_also_ids'))
                            ->from("{{object}}")
                            ->where('object_id=?', array($post->object_id))
                            ->queryRow(false);

            $_ra = array();
            if($read_also[0] == 0) {
                $read_also_ids = $db->createCommand()
                                    ->select('object_id')
                                    ->from('{{object}}')
                                    ->where('object_type=? AND object_date<=? AND object_status=? AND object_id!=?', array($post->object_type, time(), 1, $post->object_id))
                                    ->queryAll(false);

                foreach($read_also_ids as $v){
                    $_ra[] = $v[0];
                }
            } else {
                $read_also_ids = explode(',', $read_also[2]);
                foreach($read_also_ids as $v) {
                    if(!empty($v)) {
                        $_ra[] = $v;
                    }
                }
            }

            $related_objects = array();

            if(!empty($_ra)) {
                $related_objects = $modelName::model()->with('language')->findAll(array(
                    'condition' => 't.object_id IN('.implode(',', $_ra).') AND t.object_date<=? AND object_status=? AND t.object_id!=?',
                    'params' => array(time(), 1, $post->object_id),
                    'limit' =>  $read_also[1],
                    'order' => 't.object_date DESC'
                ));
            } 

            $cur_lang=Yii::app()->translate->getLanguage();
            $cur_lang_id=Language::model()->findByAttributes(array('lang_name'=>$cur_lang));
            if ($post->language->lang_name!=$cur_lang) {
                $other_content= $modelName::model()->findByAttributes(array('lang'=>$cur_lang_id->lang_id,'guid'=>$post->guid)); 

                if (!empty($other_content)) {
                    Yii::app()->controller->redirect(Object::getLink($other_content->object_id));
                }
            }

            if($post) {
                $this->render('output',array(
                    'post'=>$post, 
                    'show_title' => $show_title, 
                    'related_objects' => $related_objects
                ));
            } else {
                throw new CHttpException('404',t('Page not found'));
            }
        } else {
            throw new CHttpException('404',t('Page not found'));    
        }
    }
    
    public function params()
    {
        return array('title' => t('Title'));
    }
}
