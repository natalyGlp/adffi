<?php
$currentLanguage = Yii::app()->translate->getLanguage();
?>
<ul class="<?php echo $this->cssClass; ?> active-lang-<?php echo $currentLanguage ?>">
    <?php
    foreach ($languages as $language) {
		$linkClass = 'lang-'.$language->urlPrefix.($language->code == $currentLanguage ? ' active' : '');
		?>
		<li class="<?php echo $linkClass?>">
			<?php echo CHtml::link($language->name, $language->changeLangLink) ?> 
		</li>
		<?php
	} 
	?>
</ul>
