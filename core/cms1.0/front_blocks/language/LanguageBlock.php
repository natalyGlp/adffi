<?php

/**
 * Class for render Language checker
 * 
 * 
 * @author Yura KOvalenko <kimmongod@gmail.com>
 * @version 1.
 * @package common.front_blocks.language
 */

class LanguageBlock extends BlockWidget
{
	public function run()
	{        
		$this->renderContent();
	}       
 
	protected function renderContent()
	{
        $languages = Language::model()->cache(900)->findAll('lang_active=1');
		$this->render('output',array('languages' => $languages));
	}
}

?>