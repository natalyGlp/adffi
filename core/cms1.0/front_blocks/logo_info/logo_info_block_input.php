<div name="div-block-content-<?php echo $block_model->id;?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'src'), ''); ?>
        <?php 
            echo CHtml::textField(
                "Block[src]",
                $block_model->src,
                array('id' => 'Block-src')
            );
        ?>
        <?php echo $form->error($model, 'src'); ?>
    </div>
    <div class="row">
        <?php 
            echo CHtml::checkBox(
                "Block[appendAssetsUrl]",
                $block_model->appendAssetsUrl,
                array('id' => 'Block-appendAssetsUrl')
            );
        ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'appendAssetsUrl'), ''); ?>
        <?php echo $form->error($model, 'appendAssetsUrl'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'slogan'), ''); ?>
        <?php 
            echo CHtml::textArea(
                "Block[slogan]",
                $block_model->slogan,
                array('id' => 'Block-slogan')
            );
        ?>
        <?php echo $form->error($model, 'slogan'); ?>
    </div>
</div>
