<?php

/**
 * Class for render Logo and Info Block
 * 
 * 
 * @author Tuan Nguyen <voinovev@studiobanzai.com>
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.front_blocks.logo_info
 */

Yii::import(GxcHelpers::getTruePath('front_blocks.logo.LogoBlock'));

class LogoInfoBlock extends LogoBlock
{
    public $slogan;

    public function params()
    {
        return CMap::mergeArray(parent::params(), array(
            'slogan' => t('Slogan')
        ));
    }
}