<?php

/**
 * Class for render contacts Block
 *
 *
 * @author Tuan Nguyen <voinovev@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.contacts
 */
class ContactsBlock extends BlockWidget
{
    /**
     * Константы типов фильтрации телефонов
     */
    const PHONE_FILTER_NONE = 1;
    const PHONE_FILTER_INCLUDE = 2;
    const PHONE_FILTER_EXCLUDE = 3;

    public $showPhones = false;
    public $showEmail = false;
    public $showAddress = false;

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        $phones = self::getPhones();
        $this->render('output', array(
            'phones' => $phones,
            'email' => $this->showEmail ? Yii::app()->settings->get('system', 'support_email') : false,
            'address' => $this->showAddress ? Yii::app()->settings->get('general', 'address') : false,
        ));
    }

    /**
     * @return array array('phone' => 'phone html')
     */
    public static function getPhones()
    {
        $phoneSettings = Yii::app()->settings->get('general', 'phones');

        if (empty($phoneSettings)) {
            return array();
        }

        // FIXME: fallback для совместимости
        if (!is_array($phoneSettings)) {
            return array($phoneSettings => $phoneSettings);
        }

        $phones = array();
        $currentCountry = Yii::app()->user->countryCode;
        foreach ($phoneSettings as $phone => $options) {
            $isFilterApplies = preg_match("/(^|[^\w])" . $currentCountry . "($|[^\w])/i", $options['phone_filter']);
            $skipNumber = false;
            switch ($options['phone_filter_type']) {
                case self::PHONE_FILTER_INCLUDE:
                    if (!$isFilterApplies) {
                        $skipNumber = true;
                    }
                    break;

                case self::PHONE_FILTER_EXCLUDE:
                    if ($isFilterApplies) {
                        $skipNumber = true;
                    }
                    break;
            }

            if ($skipNumber) {
                continue;
            }

            $formattedPhone = preg_replace("/((\d+[ \-]?){3})$/", '<span class="phone-number">$1</span>', $phone);

            $phones[$phone] = $formattedPhone;
        }

        return $phones;
    }

    public function params()
    {
        return array(
            'showPhones' => t('Show Phones'),
            'showEmail' => t('Show Email'),
            'showAddress' => t('Show Address'),
        );
    }
}
