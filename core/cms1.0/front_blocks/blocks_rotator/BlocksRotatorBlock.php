<?php
/**
 * Class for rendering random block from the list of provided blockIds
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 0.1
 * @package common.front_blocks.blcks_rotator 
 */

class BlocksRotatorBlock extends BlockWidget
{
	public $blockIds = array();

	public function init()
	{
		parent::init();
		$this->blockIds = array_flip($this->blockIds);
	}
	
	public function run()
	{                 
		$this->renderContent();         
	}       
	
	protected function renderContent()
	{     
		$this->render('output',array());                                                          	       		     
	}
	
	public function params()
	{
		return array(
			'blockIds' => t('Choose Blocks'),
			);
	}
}

?>