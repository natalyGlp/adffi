<div name="div-block-content-<?php echo $block_model->id;?>">
	<div class="row">
		<?php echo CHtml::label(Block::getLabel($block_model,'addToCartSelector'),''); ?>
		<?php echo CHtml::textField("Block[addToCartSelector]", $block_model->addToCartSelector ,array('id'=>'Block-addToCartSelector', )); ?>
		<?php echo $form->error($model,'addToCartSelector'); ?>
	</div>
	<div class="row">
	    <?php echo CHtml::label(Block::getLabel($block_model,'imageSize'),''); ?>
        <?php $this->widget('cms.widgets.resource.ResourceSizePicker', array(
            'blockWidget' => $block_model,
            'attribute' => 'imageSize',
        )); ?>
	    <?php echo $form->error($model,'imageSize'); ?>
	</div>
</div>
