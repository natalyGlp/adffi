<?php 
/**
 * @var integer $amount количество товаров в корзине
 * @var string $summ общая сумма покупки
 * @var boolean $ajax индикатор аякс запроса
 * @var integer $blockId id блока
 */

?>
<?php if(!$ajax):?>
	<div id="shopCartBlock<?php echo $blockId ?>" class="shopCartBlock <?php if($this->cssClass) echo $this->cssClass; ?>">
<?php endif;?>
<span>
	<?php
	if($amount > 0)
	{
		?>
			<a href="/cart/">
				В вашей корзине <b class="cartProductAmount"><?php echo $amount ?></b> 
				<?php echo t('товар|товара|товаров|товара', 'cms', $amount) ?>
				на сумму <b class="cartProductTotalPrice"><?php echo $summ ?> грн.</b>
			</a>
		<?php
	}else{
		echo 'Корзина пуста';
	}	  
	?>
</span>
<?php if(!$ajax):?>
	</div>
<?php endif;?>
