<?php

/**
 * Блок для отображения статуса корзины а так же для добавления заказов в нее
 * 
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.shop_cart
 */
class ShopCartBlock extends BlockWidget
{
	public $addToCartSelector = false;
	public $imageSize = '';

	protected $assetsUrl;
	protected $clearCartUrl = '?action=clear';
	protected $orderUrl = '?action=order?step=last';

	public function init()
	{
		parent::init();

		// ajax экшены
		if(isset($_GET['action']))
			switch($_GET['action']) {
				case 'addToCart':
					if(isset($_POST['productId']) && isset($_GET['ajax'])) {
						$this->actionAdd($_POST['productId']);
						Yii::app()->end();
					}
				break;
				case 'clear':
					$this->actionClear();
					Yii::app()->end();
				break;
				case 'remove':
					if(isset($_GET['id'])) {
						$this->actionRemove($_GET['id']);
						Yii::app()->end();
					}
				break;
				case 'updateCartStatus':
					$this->renderCartStatus();
					Yii::app()->end();
				break;
			}

		// регестрируем assets
		$this->assetsUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__).DIRECTORY_SEPARATOR.'assets',false,-1,YII_DEBUG);

		if(!(Yii::app()->request->isAjaxRequest || isset($_GET['ajax'])))
			$this->registerScriptFile('cart.js');
	}

	public function run()
	{
		// инициализация корзины
		if(!empty($this->addToCartSelector))
		{
			$cs = Yii::app()->clientScript;
			$js = array();
			array_push($js, '$.bindAddToCart('
				.CJavaScript::encode($this->addToCartSelector).', '.CJavaScript::encode(array(
					'addLink' => '?action=addToCart&ajax=1'
					))
				.');');

			$cs->registerScript(__FILE__.'#'.$this->getId(), implode("\n", $js));
		}
		
		$this->renderCartStatus();
	}

	public function validate(){ 
		// TODO: addToCartSelector должен быть обязательным
		return true ;
	}

	public function params(){
		return array(
			'addToCartSelector' => t('jQuery selector of the "Buy button" ("Add to cart" event triggering)'),
			'imageSize' => t('Image size for product photo in cart'),
			);
	}

	protected function renderCartStatus()
	{
		list($amount, $summ) = $this->getOrderAmountSumm();

		$this->render('output',array(
			'summ' => $summ, 
			'amount' => $amount,
			'ajax' => false,
			'blockId' => $this->getId(),
			));
	}

	/**
	 * Действие добавление товара в корзину
	 * @param string $id id товара, который нужно добавить в корзину
	 */
	public function actionAdd($id)
	{
		// чистим вывод
		while(@ob_end_clean());

		$model = Object::model()->findByPk($id);
		if(!$model)
			throw new CHttpException(404,'Ошибка добавления в корзину');   

		if(empty(Yii::app()->session['order']))
			Yii::app()->session->add('order', array());

		$cartContent = Yii::app()->session['order']; 
		if(!isset($cartContent['cart'])) 
			$cartContent['cart'] = array();

		$success = true;

		if($model->object_status != Object::STATUS_PUBLISHED || !$model->sellable)
		{
			$success = false;
		}else{
			$thumb = GxcHelpers::getResourceObjectFromDatabase($model,'thumbnail', $this->imageSize);

			$productInfo = (object) array(
				'img' => count($thumb) ? CHtml::image($thumb[0]['link'], $model->object_name) : false,
				'product_name' => $model->object_name,
				'price' => 123,
				'id' => $model->primaryKey,
				// 'code' => $model->code,
				'viewUrl' => $model->getObjectLink(),
				'quantity' => 1,
				);

			$cartContent['cart'][$id] = $productInfo;
			Yii::app()->session['order'] = $cartContent;
		}

		// Отключаем jquery (так как при ajax он уже подключен)
		Yii::app()->clientscript->scriptMap['jquery.js'] = Yii::app()->clientscript->scriptMap['jquery.min.js'] = false; 

		list($amount, $summ) = $this->getOrderAmountSumm();
		$this->render('_addToCartDialog', array(
			'cart' => $cartContent['cart'],
			'success' => $success,
			'summ' => $summ, 
			'amount' => $amount,
			'model' => $model,
			'blockId' => $this->getId(),
			)); 

		Yii::app()->end();
	}
	
	/**
	 * Удаляет елемент из корзины
	 * Если $id=null очищает всю корзину
	 * @param string $id id товара, который нужно удалить из корзины
	 */
	public function actionClear($id = null)
	{
		if($id)
		{
			$cartContent = Yii::app()->session['order'];  
			unset($cartContent['cart'][$id]);
			Yii::app()->session['order'] = $cartContent;
		}
		else
		{
			Yii::app()->session->remove('order');
		}

		if(!Yii::app()->request->isAjaxRequest)
			Yii::app()->controller->redirect('/');
	}

	/**
	 * @return array из двух элементов - количество продуктов и их сумарная стоимость
	 */
	public function getOrderAmountSumm()
	{
		
		$summ = false;
		$amount = 0;

		$order = Yii::app()->session['order'];
		if(isset($order['cart']))
		{
			if(isset($order['summary'])) // если юзер уже заходил в корзину и частично продвинулся в процессе оформления заказа. TODO: проверить надо ли этот код
			{
				$summary = $order['summary'];
				$summ = $summary['orderPrice'];
				$amount = $summary['prodCount'];
			}else{
				foreach($order['cart'] as $prod)
				{
					$summ += $prod->price * $prod->quantity;
					$amount += $prod->quantity;
				}
				$summ = number_format($summ, 2, ',', ' ');
			}
		}

		return array($amount, $summ);
	}

	/**
	 * Синоним метода {@link actionClear} для случая, когда надо удалить один товар	
	 * 
	 * @access public
	 * @return void
	 */
	public function actionRemove($id)
	{
		$this->actionClear($id);
	}

	protected function registerScriptFile($fileName,$position=CClientScript::POS_END)
	{
		Yii::app()->getClientScript()->registerScriptFile($this->assetsUrl.'/js/'.$fileName,$position);
	}

	protected function registerCssFile($fileName)
	{
		Yii::app()->getClientScript()->registerCssFile($this->assetsUrl.'/css/'.$fileName);
	}
}
