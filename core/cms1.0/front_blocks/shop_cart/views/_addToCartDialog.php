<?php
/**
 * @var integer $amount количество товаров в корзине
 * @var string $summ общая сумма покупки
 * @var array of objects $cart содержимое корзины
 * @var object $model только что добавленный в корзину обьект или null
 * @var boolean $status был ли добавлен товар в корзину
 * @var integer $blockId id блока
 */
if($success) {
	$this->render(GxcHelpers::getTruePath('front_blocks.shop_order.shop_order_block_output'), array(
		'models' => $cart,
		'hidePayButton' => true,
		));
	?>
	<p>
		<a href="<?php echo '/cart'; ?>" class="button" onclick="$(this).parents('.ui-dialog-content').find('form').submit(); return false;">Перейти к оформлению заказа</a>
		<a href="" class="button" onclick="$(this).parents('.ui-dialog-content').prev().find('.ui-icon-closethick').click(); return false;">Продолжить покупки</a>
	</p>
	<script type="text/javascript">
		renderCartStatus(<?php echo $amount ?>, <?php echo $summ ?>);
	</script>
	<?php
}else{
	echo 'Товар <b>' . $model->object_name . '</b> нельзя положить в корзину, так как его нету в наличии.';
	echo '<br />Свяжитесь с нашими операторами для уточнения деталей!';
}