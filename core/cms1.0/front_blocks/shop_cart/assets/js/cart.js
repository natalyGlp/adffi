/**
 * Функционал для поддержкиработы виджета корзины
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 */
$(function() {
	/**
	 *  Глобально проставляет колличество товаров/сумму в корзине
	 */
	function cartSetValues(quantity, summ)
	{
		// обновляем виджет корзины
		renderCartStatus(quantity, summ);
		// Устанавливает сумму с учетом множественного числа у слова "товар"
		if (quantity !== false)
		{
			$('.qtotal').each(function()
			{
				$(this).parent().html(
					$(this).parent().html().replace(/товара?о?в?/g, pluralForm(quantity, 'товар', 'товара', 'товаров'))
					);
			});
			$('.qtotal').html(quantity);
		}
	}
	// делаем функцию глобальной
	window.cartSetValues = cartSetValues;
	
	/**
	 * Форматирует число как цену
	 */
	function number_format(number)
	{
		number = parseInt(number).toFixed(2);
		number = number.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ').replace(".", ",");
		return number;
	}
	
	/**
	 *  Выбирает слово в правильном падеже в зависимости от величины числа $n
	 *  Пример использования: $this->pluralForm(5, 'товар', 'товара', 'товаров')
	 */
	function pluralForm($n, $form1, $form2, $form5)
	{
		$n = Math.abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $form5;
		if ($n1 > 1 && $n1 < 5) return $form2;
		if ($n1 == 1) return $form1;
		return $form5;
	}
	
	function renderCartStatus(quantity, summ)
	{
		if(quantity) // корзина не пустая
			content = $('<a>').prop('href', '/cart').html('В вашей корзине <b class="qtotal">' + quantity + '</b> ' + pluralForm(quantity, 'товар', 'товара', 'товаров') + ' на сумму <b class="sumtotal">' + number_format(summ) + ' грн.</b>');
		else
			content = 'Ваша корзина';// <b>пустая</b>
		$('.shopCartBlock span').html(content);
	}

	/**
	 * ajax Обновление виджета корзины
	 */
	function updateCartStatus()
	{
		$.ajax('/cart?ajax=1&action=updateCartStatus',{
			success: function(content) {
				$('.shopCartBlock').html(content);
			},
		});
	}
	
	// делаем функцию глобальной
	window.renderCartStatus = cartSetValues;

	/**
	 * Удаление из корзины в jquery ui dialog
	 */
	$('body').on('click', '.ui-dialog .delLink', function() {
		var $row = $(this).parents('tr');
		var id = $(this).prop('id').slice(1);
		$.ajax({
			url: $(this).prop('href'),
			success: function()
			{
				updateCartStatus();
				if($row.parents('table').find('tr').length <= 2)
					location.reload();
				$row.remove();
			} 
		}); 
		return false;
	});  

	function openDialog(content, options) 
	{
		options = options ? options : {};
		var $content = $('<div>' + content + '</div>').appendTo($('body'))
			.filter('div') // отфильтруем наш див, так как там еще могли быть теги script, которые тоже попали в набор jQuery
			.dialog($.extend({
				'autoOpen': true,
				'title': 'Товар успешно добавлен в вашу корзину!',
				'modal': true,
				'draggable': false,
				'resizable': false,
			}, options));
			$content.parent().css('position', 'fixed');
			
			return $content;
	}

	window.runDialog = openDialog;

	// Плагин корзины
	(function($){
		$.bindAddToCart = function(selector, options) {
			options = $.extend({
				addLink: '',
			}, options);

			$('body').off('click.addToCart').on('click.addToCart', selector, function() {
				$.ajax(options.addLink.replace('{id}', $(this).data('id')), {
					type: 'post',
					data: {productId: $(this).data('id')},
					success: function(data) {
						openDialog(data);
					}
				});

				return false;
			});
		};
	})(jQuery);
});

function askAboutAccount(strings) {
	strings = $.extend({
		title: 'У вас уже есть аккаунт на нашем сайте?',
		yesBtn: 'Да',
		noBtn: 'Продолжить без регистрации',
		createNewAccBtn: 'Нет, создать новый',
		dialogTitle: 'Корзина'
	}, strings)

	var buttons = {};
	buttons[strings.yesBtn] = function() {
		$('#loginFormCart').dialog('open');       
	};
	buttons[strings.noBtn] = function() { 
		$("#nextStep").click(); // кнопка далее
		$(this).dialog("close"); 
	};
	buttons[strings.createNewAccBtn] = function() { 
		$("<input type=\"hidden\" name=\"newUser\" value=\"1\">").appendTo($("#cartForm"));
		$("#nextStep").click(); // кнопка далее
		$(this).dialog("close"); 
	};


	$dialog = runDialog("<h2 align=center>"+strings.title+"</h2>", {
		title: strings.dialogTitle,
		buttons: buttons
	});

	$dialog[0].id = 'askAboutAccount';

	return false;
}

/**
 * Функционал для поддержки оформления заказа
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 */
$(
	function() {
	/*
	* Изменение количества товаров
	*/
	$('body').on('change click', '.quantity', function() {
		var id = $(this).prop('id').slice(1);
		var newQuantity = $(this).val()*1;
		quantity[id] = newQuantity;
		
		// Обрабатываем цену (сумма, что напротив товара)
		var priceTd = $(this).parents('td').prev();
		if(!priceTd.find('.prodSumm').length)
			priceTd.append('<div class="prodSumm">' + priceTd.html() + '</div>');
		priceTd = priceTd.find('.prodSumm');
		var newPrice = number_format(price[id] * quantity[id]);
		
		priceTd.html(newPrice + priceTd.html().replace(/[\d,]/g, ''));
		
		// Обновляем сумму и колличество товаров
		countSummary();
	});

	/*
	* Удаление из корзины
	*/
	$('body').on('click', '.delLink', function() {
		var row = $(this).parents('tr');
		var id = $(this).prop('id').slice(1);
		$.ajax({
			url: $(this).prop('href'),
			success: function()
			{
				delete price[id];
				delete quantity[id];
				// Обновляем сумму
				countSummary();
				
				// сообщаем, что корзина пуста и переадресовываем юзера на главную
				if(row.parents('table').find('tr').length <= 2  && location.href.indexOf('/cart') != -1) // 2 потому, что одну строку мы сейчас удалим и еще одна строка с суммой
				{ 
					$('#doProcessOrderBtn').hide(); // прячем кнопку продолжения заказа
					$('<div><br />Ваша корзина пуста, через <span id="redirectTimer">3</span> секунд вы будете перемещены на <a href="/">главную страницу</a></div>')
						.dialog({
							'autoOpen': true,
							'title': 'Товар успешно добавлен в вашу корзину!',
							'modal': true,
							'draggable': false,
							'resizable': false,
						}).parent().css('position', 'fixed')

					setInterval(function() {
						var wait = $("#redirectTimer").html()*1;
						if(wait)
							$("#redirectTimer").html(--wait);
						else 
							location.href = "/";
					}, 1000);
				}     
				row.remove();
			} 
		}); 
		return false;
	});

	/**
	* Обновляет сумму покупки
	**/
	function countSummary()
	{
		var summ = 0;
		var amount = 0;
		for (i in price)
		{
			summ += price[i] * quantity[i];
			amount += quantity[i] * 1;
		}
			
		// устанавливаем сумму и колличество товаров (функция, предоставляемая виджетом cartStatus)
		if(window.cartSetValues)
			cartSetValues(amount, summ);
		
		summ = number_format(summ);
		$('#summary').html(summ + $('#summary').html().replace(/[\d,]/g, ''));
	}

	/**
	* Форматирует число как цену
	**/
	function number_format(number)
	{
		number = number.toFixed(2);
		number = number.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ').replace(".", ",");
		return number;
	}

	var d = window.document;
	var timer = false;
	/***********************
	* Отображает анимацию загрузки
	***********************/
	window.startLoad = function() 
	{
		if (timer) return;
		if (d.getElementById('loading_layer')) return;
		// Блокируем все submit
		$('[type="submit"]').prop('disabled', 'disabled');
		
		var loading_layer = d.createElement('div');
		loading_layer.id = 'loading_layer';
		loading_layer.className = 'ajax_loading';
		
		// Что бы не портить впичатление юзера от быстрого интерфейса 
		// включаем задержку до появления загрузчика
		timer = setTimeout(function(){
			d.body.appendChild(loading_layer);
		}, 1000);
	}

	/***********************
	* Прекращает анимацию загрузки
	***********************/
	window.stopLoad = function() {
		if(timer)
			clearTimeout(timer);
		if ($('#loading_layer').parents('body')) // Значит элемент уже в дом
			$('#loading_layer').remove();
	}

	/**
	 *  Обновляет контент блока с анимацией (используется в корзине)
	 */
	window.replaceContent = function (data)
	{
		$('[type="submit"]').removeProp('disabled');
		$('#cartContent').fadeOut(500, function()
		{
			$('#cartContent').replaceWith(data);
			$('html, body').animate({scrollTop:$('#cartContent').offset().top}, 'slow'); 
			$('#cartContent').fadeIn(500); 
		});
	}
});
