<div class="row">
    <?php echo CHtml::label(Block::getLabel($block_model, 'viewPath'), ''); ?>
    <?php echo CHtml::dropDownList("Block[viewPath]", $block_model->viewPath, $block_model->getViewsFiles()); ?>
    <?php echo $form->error($model, 'viewPath'); ?>
    <div class="alert alert-info">View files should be in <b>view</b> directory</div>
</div>