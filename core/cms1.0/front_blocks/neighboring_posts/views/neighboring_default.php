<?php
/* @var $prevObject Object */
/* @var $nextObject Object */
?>
<?php if(isset($prevObject)): ?>
    <a class="neighboring-post neighboring-post--prev" href="<?= $prevObject->getObjectLink(); ?>"><?= t('Previous post'); ?></a>
<?php endif; ?>
<?php if(isset($nextObject)): ?>
    <a class="neighboring-post neighboring-post--next" href="<?= $nextObject->getObjectLink(); ?>"><?= t('Next post'); ?></a>
<?php endif; ?>