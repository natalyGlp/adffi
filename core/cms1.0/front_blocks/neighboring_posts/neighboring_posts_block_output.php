<?php
/* @var $prevObject Object */
/* @var $nextObject Object */
/* @var $view NeighboringObjectBlock::viewPath */
?>
<div class="<?= $this->cssClass ?>">
    <?php $this->render($view, array('prevObject'=>$prevObject, 'nextObject'=>$nextObject)); ?>
</div>