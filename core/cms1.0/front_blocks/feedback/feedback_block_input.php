<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'title'), ''); ?>
        <?php echo CHtml::textField("Block[title]", $block_model->title, array('id' => 'Block-title',)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'subject'), ''); ?>
        <?php echo CHtml::textField("Block[subject]", $block_model->subject, array('id' => 'Block-subject',)); ?>
        <?php echo $form->error($model, 'subject'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'formName'), ''); ?>
        <?php echo CHtml::textField("Block[formName]", $block_model->formName, array('id' => 'Block-formName',)); ?>
        <?php echo $form->error($model, 'formName'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'submitButtonTitle'), ''); ?>
        <?php echo CHtml::textField("Block[submitButtonTitle]", $block_model->submitButtonTitle, array('id' => 'Block-submitButtonTitle',)); ?>
        <?php echo $form->error($model, 'submitButtonTitle'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'successMessage'), ''); ?>
        <?php echo CHtml::textField("Block[successMessage]", $block_model->successMessage, array('id' => 'Block-successMessage',)); ?>
        <?php echo $form->error($model, 'successMessage'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::checkBox("Block[logEmails]", $block_model->logEmails, array('id' => 'Block-logEmails',)); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'logEmails'), ''); ?>
        <?php echo $form->error($model, 'logEmails'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::checkBox("Block[isPopupForm]", $block_model->isPopupForm, array('id' => 'Block-isPopupForm',)); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'isPopupForm'), ''); ?>
        <?php echo $form->error($model, 'isPopupForm'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::checkBox("Block[useCaptcha]", $block_model->useCaptcha, array('id' => 'Block-useCaptcha',)); ?>
        <?php echo CHtml::label(Block::getLabel($block_model, 'useCaptcha'), ''); ?>
        <?php echo $form->error($model, 'useCaptcha'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'popupFormTriggerText'), ''); ?>
        <?php echo CHtml::textField("Block[popupFormTriggerText]", $block_model->popupFormTriggerText, array('id' => 'Block-popupFormTriggerText',)); ?>
        <?php echo $form->error($model, 'popupFormTriggerText'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'bindToSelector'), ''); ?>
        <?php echo CHtml::textField("Block[bindToSelector]", $block_model->bindToSelector, array('id' => 'Block-bindToSelector',)); ?>
        <?php echo $form->error($model, 'bindToSelector'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'redirectOnSuccess'), ''); ?>
        <?php echo CHtml::textField("Block[redirectOnSuccess]", $block_model->redirectOnSuccess, array('id' => 'Block-redirectOnSuccess',)); ?>
        <?php echo $form->error($model, 'redirectOnSuccess'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'sendTo'), ''); ?>
        <?php echo CHtml::textField("Block[sendTo]", $block_model->sendTo, array('id' => 'Block-sendTo',)); ?>
        <?php echo $form->error($model, 'sendTo'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'mailTemplate'), ''); ?>
        <?php echo CHtml::textField("Block[mailTemplate]", $block_model->mailTemplate, array('id' => 'Block-mailTemplate',)); ?>
        <?php echo $form->error($model, 'mailTemplate'); ?>
    </div>
</div>
