<?php
/**
 * @var CFormModel $model
 *
 * Форма должна начинаться с $form = $this->beginForm(); и заканчиваться $this->endForm()
 * Для кнопки отправки использовать $this->submitButton() (принимает те же параметры, что и CHtml::submitButton())
 *
 * Пример поля формы:
 * <div class="row">
 *     <?php echo $form->labelEx($model, 'name'); ?>
 *     <?php echo $form->textField($model, 'name',
 *         array(
 *             'size' => 30, // опционально
 *             'class' => 'userform', // опционально
 *             'autoComplete' => 'off', // опционально
 *             'placeholder' => $this->createPlaceholder($model, 'name'), // опционально, вместо $form->labelEx
 *         )); ?>
 * </div>
 * <?php echo $form->error($model, 'name'); ?>
 */
$form = $this->beginForm();
?>
    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name'); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email'); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'phone'); ?>
        <?php echo $form->textField($model, 'phone'); ?>
        <?php echo $form->error($model, 'phone'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'message'); ?>
        <?php echo $form->textArea($model, 'message'); ?>
        <?php echo $form->error($model, 'message'); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'attachments'); ?>
        <?php
            $this->widget('CMultiFileUpload', array(
                'id' => $this->id.'__attachments',
                'name' => get_class($model).'[attachments]',
                'accept' => $this->allowedExts,
                'duplicate' => t('You already have this file'),
                'denied' => t('You have chosen the wrong file format'),
            ));
        ?>
        <?php echo $form->error($model, 'attachments'); ?>
    </div>
    <div class="actions">
        <?php
        echo $this->submitButton();
        ?>
    </div>
<?php
$this->endForm();
?>