<?php

/**
 * Class for render search * 
 * 
 * @author Jonny <voinovev@studiobanzai.com>
 * @version 1.0
 * @package common.front_blocks.search */
class SearchFormBlock extends BlockWidget
{
    // Unique propertys
    public $path = '/search';
    public $buttonText = 'Искать';
    public $holderText = '';

    public function run() {
        $this->renderContent();
    }

    protected function renderContent() {
        $this->render('output', array());
    }

    public function params() {
        return array(
            'path' =>  t('Search form path'),
            'buttonText' => t('Button text'),
            'holderText' => t('Holder text'),
        );
    }
}

?>