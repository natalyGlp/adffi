<?php
		$blockId= !isset($blockId) ? $this->block->block_id : $blockId; /* id блока*/  
?>
<div class="form-stacked">
	<div class="website-info">
			<h1><?php echo t('Sign up for mywordbook.com'); ?></h1>
	</div>
	<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'userregister-form',
			 'enableClientValidation'=>true,
					'clientOptions'=>array(
									'validateOnSubmit'=>true,
					),   
			)); 
	?>

		<div class="clearfix">
			<?php echo $form->labelEx($model,'first_name'); ?>
			<div class="input">
				<?php echo $form->textField($model,'first_name',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
				<?php echo $form->error($model,'first_name'); ?>
			</div>      
		</div>

		<div class="clearfix">
			<?php echo $form->labelEx($model,'last_name'); ?>
			<div class="input">
				<?php echo $form->textField($model,'last_name',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
				<?php echo $form->error($model,'last_name'); ?>
			</div>      
		</div>
				
		<div class="clearfix">
			<?php echo $form->labelEx($model,'email'); ?></label>
			<div class="input">
				<?php echo $form->textField($model,'email',array('size'=>30,'class'=>'userform')); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		
		<div class="clearfix">
			<?php echo $form->labelEx($model,'password1'); ?>
			<div class="input">
				<?php echo $form->passwordField($model,'password1',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
				<?php echo $form->error($model,'password1'); ?>
			</div>
		</div>
		<div class="clearfix">
			<?php echo $form->labelEx($model,'password2'); ?>
			<div class="input">
				<?php echo $form->passwordField($model,'password2',array('size'=>30,'class'=>'userform','autoComplete'=>'off')); ?>
				<?php echo $form->error($model,'password2'); ?>
			</div>
		</div>
		<div class="actions">
			<?php 
			$htmlOptions = array('class'=>'btn primary','id'=>'bCreateButton');
			if(isset($ajaxOptions))
				echo CHtml::ajaxSubmitButton(t('Register'), $htmlOptions, $ajaxOptions); 
			else
				echo CHtml::submitButton(t('Register'), $htmlOptions); 
			?>
			<p style="margin-top:10px; color:#aaa">
				<?php echo t('By clicking "Register" you confirm that you accept the'); ?>
				<a href="<?php echo FRONT_SITE_URL;?>/terms" onclick="window.open(this.href);return false;">Terms of Service.</a>
			</p>
		
		</div>
		
		<p style="font:15px Verdana; padding-top: 20px"><?php echo t('Already have an account?'); ?> 
			<a class="login-link" href="<?php echo FRONT_SITE_URL;?>/sign-in"><?php echo t('Sign in here'); ?></a>.
		</p>
	<?php $this->endWidget(); ?>
</div>

