<div name="div-block-content-<?php echo $block_model->id;?>">
    <?php $this->render('cms.widgets.views.contentlist._contentlist_objects_form', array(
        'model' => $block_model,
        'form' => $form,
        'langAttribute' => 'langs',
        'contentTypeAttribute' => 'contentTypes',
        'termsAttribute' => 'terms',
    )); ?>
</div>