$(function() {
    $('body').on('click', '.calendar a[data-action]', function() {
        var $el = $(this),
            $root = $el.parents('.calendar'),
            delta = $el.data('action') == 'next-month' ? '+1' : '-1',
            request = {
                    year: $root.data('year'),
                    month: $root.data('month'),
                    widget: 'calendar',
                    delta: delta
                };

        $.ajax(location.href, {
            data: request,
            type: 'post',
            success: function(data) {
                $root.replaceWith(data);
            }
        });

        return false;
    });
});