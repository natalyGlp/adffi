<?php
/**
 * @var strin $id id of the map container
 * @var strin $cssClass main css class
 *
 * For modal mode:
 * @var strin $closeCssClass the class of the button for closing map (should be places in the map container)
 */
?>

<div class="<?= $cssClass ?>">
    <span class="<?= $closeCssClass ?> <?= $cssClass ?>-close"></span>
    <div class="<?= $cssClass ?>-gm" style="width: 100%; height: 100%;" id="<?= $id ?>"></div>
</div>
