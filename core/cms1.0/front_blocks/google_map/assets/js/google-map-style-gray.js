window.nespi = window.nespi || {};
nespi.googleMapsStyles = nespi.googleMapsStyles || {};

nespi.googleMapsStyles.gray = [
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "on" },
      { "color": "#a1a1a1" }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      { "visibility": "on" },
      { "color": "#2e2e2e" }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.stroke",
    "stylers": [
      { "visibility": "off" },
      { "color": "#ffffff" }
    ]
  },
    {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "on" },
      { "color": "#a1a1a1" }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.icon",
    "stylers": [
      { "visibility": "on" },
      { "hue": "#ffaa00" },
      { "invert_lightness": true },
      { "gamma": 1.08 },
      { "saturation": 80 }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.icon",
    "stylers": [
      { "hue": "#1900ff" },
      { "visibility": "on" },
      { "lightness": 2 },
      { "gamma": 0.01 },
      { "saturation": 35 }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry",
    "stylers": [
      { "color": "#a1a1a1" }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      { "visibility": "on" },
      { "color": "#2e2e2e" }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      { "color": "#a1a1a1" }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      { "color": "#2e2e2e" }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.stroke",
    "stylers": [
      { "hue": "#ff0000" },
      { "color": "#808080" },
      { "visibility": "off" }
    ]
  },
  // {
  //   "featureType": "road.highway",
  //   "elementType": "labels.icon",
  //   "stylers": [
  //     { "visibility": "on" },
  //     { "color": "#808080" }
  //   ]
  // },
  {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "on" },
      { "color": "#e3e3e3" }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "on" },
      { "color": "#c1c1c1" }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "on" },
      { "color": "#b5b5b5" }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [
      { "color": "#b6b6b6" },
      { "visibility": "on" }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "on" },
      { "color": "#c1c1c1" }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.icon",
    "stylers": [
      { "hue": "#1900ff" },
      { "visibility": "on" },
      { "lightness": 2 },
      { "gamma": 0.01 },
      { "saturation": 35 }
    ]
  }
  // {
  //   "featureType": "transit.station.rail",
  //   "elementType": "labels.icon",
  //   "stylers": [
  //     { "weight": 0.1 },
  //     { "visibility": "on" },
  //     { "gamma": 0.97 },
  //     { "lightness": 4 },
  //     { "saturation": -62 },
  //     { "hue": "#0088ff" }
  //   ]
  // },
  // {
  //   "featureType": "transit.station.airport",
  //   "stylers": [
  //     { "color": "#a5b5ba" },
  //     { "visibility": "on" }
  //   ]
  // },
  // {
  //   "featureType": "transit.station.airport",
  //   "elementType": "labels.text.fill",
  //   "stylers": [
  //     { "color": "#ffffff" },
  //     { "visibility": "on" }
  //   ]
  // }
];