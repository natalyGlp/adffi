var GoogleMap = (function($) {
    'use strict';

    var googleApiLoading = (function loadGoogleApi() {
        var d = new $.Deferred();
        var date = new Date();
        var functionName = 'gmaps_'+date.getTime();

        window[functionName] =  function() {
            d.resolve();
        };

        $.getScript('http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language=ru&libraries=geometry&callback='+functionName);

        return d.promise();
    }());

    var domLoading = (function() {
        var d = new $.Deferred();

        $(function() {
            d.resolve();
        });

        return d.promise();
    }());

    function registerApi() {
        return $.when(domLoading, googleApiLoading);
    }

    var locations = {};

    function GoogleMap(options) {
      this.options = options;

      this.options.maxZoomLevel = this.options.maxZoomLevel || 17;
    }

    GoogleMap.prototype = $.extend(GoogleMap.prototype, {
        init: function(mapSelector) {
            return registerApi().then($.proxy(function() {
                var mapOptions = {
                    center: this.newLatLng(50.450026, 30.524206), // Киев
                    zoom: 11,
                    streetViewControl: false,
                    mapTypeControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                if (this.options.styles) {
                    mapOptions.styles = this.options.styles;
                }

                var mapContainer = $(mapSelector)[0];

                this.map = new google.maps.Map(mapContainer, mapOptions);
                this.maxZoomService = new google.maps.MaxZoomService();
            }, this));
        },

        addLocation: function(options) {
            var marker = this.createMarker(options.name, this.newLatLng(options.lat, options.lng));

            marker.locationId = options.id;

            this.setMarkerEvents(marker, options);

            locations[options.id] = {
                marker: marker,
                data: options,
            };
        },

        newLatLng: function(lat, lng) {
            return new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
        },

        createMarker: function(title, position) {
            var markerOptions = {
                position: position,
                map: this.map,
                animation: google.maps.Animation.DROP,
                title: title
            };

            if(this.options.markerImage) {
                markerOptions.icon = this.getMarkerIcon();
                markerOptions.shape = this.getMarkerShape();
            }

            return new google.maps.Marker(markerOptions);
        },

        getMarkerIcon: function() {
            if(this.__markerImage) {
                return this.__markerImage;
            }

            var markerImage = this.options.markerImage;

            this.__markerImage = {
                url: markerImage.url,
                origin: new google.maps.Point(0,0), // начало координат в левом верхнем углу
                size: new google.maps.Size(markerImage.width, markerImage.height),
                // scaledSize: new google.maps.Size(25, 25),
                anchor: new google.maps.Point(markerImage.anchorX, markerImage.anchorY)
            };

            return this.__markerImage;
        },

        getMarkerShape: function() {
            return {
                coords: this.options.markerImage.shape,
                type: 'poly'
            };
        },

        setMarkerEvents: function(marker, options) {
            google.maps.event.addListener(marker, 'click', $.proxy(function() {
                this.spanToLocation(options.id);
            }, this));
        },

        fitBoundsToMarkers: function() {
            var bounds = new google.maps.LatLngBounds();

            this.eachMarker(function() {
                bounds.extend(this.getPosition());
            });

            this.map.fitBounds(bounds);

            if(this.map.getZoom() > this.options.maxZoomLevel) {
                this.zoomTo(this.options.maxZoomLevel);
            }

            this.trigger('locationChange', null);
        },

        eachMarker: function(callback) {
            $.each(locations, function(id) {
                callback.call(this.marker, id, this.marker);
            });
        },

        spanToLocation: function(locationId) {
            if(!this.hasLocation(locationId)) {
                this.fitBoundsToMarkers();
                return;
            }

            var center = locations[locationId].marker.getPosition();
            this.map.setCenter(center);

            // this.zoomTo(this.getMaxZoom(center));
            this.zoomTo(this.options.maxZoomLevel);

            this.trigger('locationChange', locationId);
        },

        findLocationsNear: function(locationId, inRangeOfMeters) {
            if (!this.getLocation(locationId)) {
                return [];
            }
            if (!inRangeOfMeters) {
                inRangeOfMeters = 50;
            }
            var locations = [];
            var that = this;

            var location = this.getLocation(locationId);
            var center = location.marker.getPosition();

            this.eachMarker(function() {
                var curMarkerPosition = this.getPosition();
                var distance = that.getDistanceInMeters(center, curMarkerPosition);
                if (distance <= inRangeOfMeters) {
                    locations.push(that.getLocation(this.locationId));
                }
            });
            return locations;
        },

        getDistanceInMeters: function(location1, location2) {
            return google.maps.geometry.spherical.computeDistanceBetween(
                location1, location2
            );
        },

        trigger: function(eventId) {
            $(this).trigger(eventId, [].slice.call(arguments, 1));
        },

        on: function(eventId, callback) {
            $(this).on(eventId, callback);
        },

        hasLocation: function(locationId) {
            return !!locations[locationId];
        },

        getLocation: function(locationId) {
            return locations[locationId];
        },

        resizeViewport: function() {
            var latLng = this.map.getCenter();
            google.maps.event.trigger(this.map, 'resize');
            this.map.panTo(latLng);
        },

        zoomTo: function(zoom) {
            // var zoomCallback = $.bind(function(zoom) {
            // }, this);

            // if (zoom.then) {
            //     zoom.then(zoomCallback);
            // } else {
            //     zoomCallback(zoom);
            // }
            var center = this.map.getCenter();
            this.map.setZoom(zoom);
            this.map.panTo(center);
        },

        getMaxZoom: function(latLng)
        {
            // TODO
            var d = new $.Deferred();
            var that = this;

            this.maxZoomService.getMaxZoomAtLatLng(latLng, function(response) {
                var toBigZoom = response.zoom > that.options.maxZoomLevel;
                var noZoomData = response.status != google.maps.MaxZoomStatus.OK;
                var shouldReturnDefaultMaxZoom = toBigZoom || noZoomData;

                d.resolve(shouldReturnDefaultMaxZoom ? that.options.maxZoomLevel : response.zoom);
            });

            return d;
        }
    });

    return GoogleMap;
}(jQuery));
