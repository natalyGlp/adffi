window.nespi = window.nespi || {};
nespi.googleMapsStyles = nespi.googleMapsStyles || {};

nespi.googleMapsStyles.white = [
    {
        featureType: 'water',
        elementType: 'all',
        stylers: [
            { hue: '#e3e3e3' },
            { saturation: -100 },
            { lightness: 54 },
            { visibility: 'simplified' }
        ]
    },{
        featureType: 'landscape',
        elementType: 'all',
        stylers: [
            { hue: '#ffffff' },
            { saturation: -100 },
            { lightness: 100 },
            { visibility: 'simplified' }
        ]
    },{
        featureType: 'administrative',
        elementType: 'all',
        stylers: [
            { hue: '#e3e3e3' },
            { saturation: 0 },
            { lightness: 78 },
            { visibility: 'on' }
        ]
    },{
        featureType: 'transit',
        elementType: 'all',
        stylers: [
            { hue: '#e3e3e3' },
            { saturation: 0 },
            { lightness: 56 },
            { visibility: 'off' }
        ]
    },{
        featureType: 'poi',
        elementType: 'all',
        stylers: [
            { hue: '#ffffff' },
            { saturation: -100 },
            { lightness: 100 },
            { visibility: 'simplified' }
        ]
    },{
        featureType: 'road',
        elementType: 'all',
        stylers: [
            { hue: '#b9b9b9' },
            { saturation: -100 },
            { lightness: 24 },
            { visibility: 'on' }
        ]
    }
];
