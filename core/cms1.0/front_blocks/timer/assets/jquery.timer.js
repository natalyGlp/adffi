(function($){
	// TODO: рефакторинг
	$.fn.timer = function(options) {
		var date = new Date(),
			oldDate = new Date(),
			$el = this,
			cookieId = this.prop('id'),
			dateEnd,
			interval = false;

		options = $.extend({
			days: 0,
			hours: 0,
			minutes: 0,
			onEnd: false, // колбек по завершению таймера
		}, options);

		var countDown = function() {
			var date = new Date();

			//Проверяем не нужно ли закончить отсчет
			//если дата отсчета еще не истекла, то количество
			//миллисекунд в переменной dateEnd будет больше чем в переменной date
			if(dateEnd > date) {
				if(date.getSeconds() == oldDate.getSeconds()) return;
				var timer = dateEnd - date;
				//Вычисляем сколько осталось дней до даты отсчета.
				//Для этого количество миллисекунд до даты остчета делим
				//на количество миллисекунд в одном дне
				var day = parseInt(timer/(60*60*1000*24));

				//если полученное число меньше 10 прибавляем 0
				if(day < 10) {
					day = '0' + day;
				}

				//Приводим результат к строке
				day = day.toString();

				//Вычисляем сколько осталось часов до даты отсчета.
				//Для этого переменную timer делим на количество
				//миллисекунд в одном часе и отбрасываем дни
				var hour = parseInt(timer/(60*60*1000))%24;

				//если полученное число меньше 10 прибавляем 0
				if(hour < 10) {
					hour = '0' + hour;
				}

				//Приводим результат к строке
				hour = hour.toString();

				//Вычисляем сколько осталось минут до даты отсчета.
				//Для этого переменную timer делим на количество
				//миллисекунд в одной минуте и отбрасываем часы
				var min = parseInt(timer/(1000*60))%60;

				//если полученное число меньше 10 прибавляем 0
				if(min < 10) {
					min = '0' + min;
				}

				//Приводим результат к строке
				min = min.toString();

				//Вычисляем сколько осталось секунд до даты отсчета.
				//Для этого переменную timer делим на количество
				//миллисекунд в одной минуте и отбрасываем минуты
				var sec = parseInt(timer/1000)%60;

				//если полученное число меньше 10 прибавляем 0
				if(sec < 10) {
					sec = '0' + sec;
				}

				//Приводим результат к строке
				sec = sec.toString();

				//Выводим дни
				//Проверяем какие предыдущие цифры времени должны вывестись на экран
				//Для десятков дней
				if(day[1] == 9 && 
					hour[0] == 2 && 
					hour[1] == 3 && 
					min[0] == 5 && 
					min[1] == 9 && 
					sec[0] == 5 && 
					sec[1] == 9) {
					animation($(".day0", $el),day[0]);
				}
				else {
					$(".day0", $el).html(day[0]);
				}

				//Для единиц дней
				if(hour[0] == 2 && 
					hour[1] == 3 && 
					min[0] == 5 && 
					min[1] == 9 && 
					sec[0] == 5 && 
					sec[1] == 9) {
					animation($(".day1", $el),day[1]);
				}
				else {
					$(".day1", $el).html(day[1]);
				}

				//Выводим часы
				//Проверяем какие предыдущие цифры времени должны вывестись на экран
				//Для десятков часов
				if(hour[1] == 3 && 
					min[0] == 5 && 
					min[1] == 9 && 
					sec[0] == 5 && 
					sec[1] == 9) {
					animation($(".hour0", $el),hour[0]);
				}
				else {
					$(".hour0", $el).html(hour[0]);
				}

				//Для единиц чассов	
				if(min[0] == 5 && 
					min[1] == 9 && 
					sec[0] == 5 && 
					sec[1] == 9) {
					animation($(".hour1", $el),hour[1]);
				}
				else {
					$(".hour1", $el).html(hour[1]);
				}

				//Выводим минуты
				//Проверяем какие предыдущие цифры времени должны вывестись на экран
				//Для десятков минут
				if(min[1] == 9 && 
					sec[0] == 5 && 
					sec[1] == 9) {
					animation($(".min0", $el),min[0]);
				}
				else {
					$(".min0", $el).html(min[0]);
				}

				//Для единиц минут
				if(sec[0] == 5 && sec[1] == 9) {
					animation($(".min1", $el),min[1]);
				}
				else {
					$(".min1", $el).html(min[1]);
				}

				//Выводим секунды
				//Проверяем какие предыдущие цифры времени должны вывестись на экран
				//Для десятков секунд
				if(sec[1] == 9) {
					animation($(".sec0", $el),sec[0]);
				}
				else {
					$(".sec0", $el).html(sec[0]);
				}
				animation($(".sec1", $el),sec[1]);	

				oldDate = date;
			}
			else {
				clearInterval(interval);
				$el.addClass('ended')
				if(options.onEnd)
					options.onEnd.call($el[0])
			}
		}

		dateEnd = returnEndDate(options.days, options.hours, options.minutes);
		cookieDateEnd = $.cookie(cookieId) ? new Date($.cookie(cookieId)) : null;
		if(cookieDateEnd && cookieDateEnd > date)
		{
			dateEnd = cookieDateEnd;
		}
		$.cookie(cookieId, dateEnd.toString(), {expires: dateEnd});

		interval = setInterval(countDown, 10);
	}
		
	function returnEndDate(d,h,m){
		var myDate = new Date();
		myDate.setDate(myDate.getDate()+d);
		myDate.setHours(myDate.getHours()+h);
		myDate.setMinutes(myDate.getMinutes()+m);
		return myDate;
	}

	function animation(vibor,param) {
		vibor.html(param)
			.css({'marginTop':'-20px','opacity':'0'})
			.animate({'marginTop':'0px','opacity':'1'});
	}
})(jQuery);
