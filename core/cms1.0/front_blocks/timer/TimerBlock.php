<?php

class TimerBlock extends BlockWidget 
{
    public $days = 0;
    public $hours = 0;
    public $minutes = 0;
    public $useDefaultCss = false;
    public $timeoutMessage = 'Время вышло!!!';
//    public $seconds = '';

    public function run()
    {
        $url = Yii::app()->assetManager->publish(Yii::getPathOfAlias('cms.front_blocks.timer.assets'), false);
        Yii::app()->clientScript
            ->registerCoreScript('jquery')
            ->registerCoreScript('cookie')
            ->registerScriptFile($url."/jquery.timer.js")
            ;
        if($this->useDefaultCss) {
            Yii::app()->clientScript->registerCssFile($url."/timer.css");
        } else {
            // дефолтные стили для скрытия надписи "время вышло"
            Yii::app()->clientScript->registerCss(__FILE__, '
                .timer .ended-message,
                .timer.ended * {
                    display: none;
                }
                
                .timer.ended .ended-message {
                    display: block;
                }
            ');
        }

        $this->renderContent();
    }

    public function getOptions()
    {
        return array(
            'days' => (int)$this->days,
            'hours' => (int)$this->hours,
            'minutes' => (int)$this->minutes,
        );
    }

    public function renderContent()
    {
        $id = 'timer'.$this->getId();
        $options = $this->getOptions();
        $options = CJavaScript::encode($options);
        Yii::app()->clientScript->registerScript(__CLASS__.'#'.$id, "jQuery('#$id').timer($options);");

        $this->render('output',array('options' => $options, 'id' => $id));
    }

    public function params()
    {
        return array(
            'days' => t('Days'),
            'hours' => t('Hours'),
            'minutes' => t('Minutes'),
            'useDefaultCss' => t('Use Default Css File'),
            'timeoutMessage' => t('Timer timeout message'),
//            'seconds' => t('Seconds'),
        );
    }
} 