<div name="div-block-content-<?php $block_model->id?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model,'days'),''); ?>
        <?php echo CHtml::textField("Block[days]",
            $block_model->days,array('id'=>'Block-days')); ?>
        <?php echo $form->error($model,'days'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model,'hours'),''); ?>
        <?php echo CHtml::textField("Block[hours]",
            $block_model->hours,array('id'=>'Block-hours')); ?>
        <?php echo $form->error($model,'hours'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model,'minutes'),''); ?>
        <?php echo CHtml::textField("Block[minutes]",
            $block_model->minutes,array('id'=>'Block-minutes')); ?>
        <?php echo $form->error($model,'minutes'); ?>
    </div>
<!--    <div class="row">
        <?php /*echo CHtml::label(Block::getLabel($block_model,'seconds'),''); */?>
        <?php /*echo CHtml::textField("Block[seconds]",
            $block_model->seconds,array('id'=>'Block-seconds')); */?>
    </div>-->
<!--    <div class="row">
        <?php /*echo CHtml::label('Time', 'time')*/?>
        <?php /*echo CHtml::textField('time', 0)*/?>
    </div>-->
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model,'timeoutMessage'),''); ?>
        <?php echo CHtml::textField("Block[timeoutMessage]", $block_model->timeoutMessage,array('id'=>'Block-timeoutMessage')); ?>
        <?php echo $form->error($model,'timeoutMessage'); ?>
    </div>
    <div class="row">
        <?php echo CHtml::checkBox("Block[useDefaultCss]",
            $block_model->useDefaultCss,array('id'=>'Block-useDefaultCss')); ?>
        <?php echo CHtml::label(Block::getLabel($block_model,'useDefaultCss'),''); ?>
        <?php echo $form->error($model,'useDefaultCss'); ?>
    </div>
</div>