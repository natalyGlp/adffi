<?php 
/**
 * View file for cart
 * 
 * @var integer $amount количество товаров в корзине
 * @var string $summ общая сумма покупки
 * @var boolean $hidePayButton спрятать кнопки оплаты (нужно для рендеринга контента диалога)
 * @var integer $blockId id блока
 */
 
if(!isset($hidePayButton))
	$hidePayButton = false;

$this->controller->breadcrumbs[] = $this->controller->pageTitle;

$total = 0;
foreach($models as $model) 
  $total += $model->quantity;

// сразу кидаем юзера на последний шаг
// скрипт валидации шагов сам определится на какой именно шаг его перекинуть
$action = '/cart?action=order&step=last';

?>
<div id="cartContent" class="form">
<?php
echo '<div id="cartCancelOrder">' . CHtml::button('Отменить заказ', array('onclick'=>'$.get("'.$this->clearCartUrl.'", function() {location.reload()});')) . '</div>';
?>
<h1>В вашей корзине <span class='qtotal'><?php echo $total ?></span> <? echo t('товар|товара|товаров|товара', $total, 'cms')  ?></h1>
<?php 
if($total)
{
  echo CHtml::form($action);
?>
<table>
<?php 
$check = 0;
foreach($models as $index => $model) 
{
  // $index == $model->id за исключением случая с вариантами, 
  // в котором к id еще добавляется хэш варианта
  $check += $model->price * $model->quantity;
  $price[$index] = $model->price;
  $quantity[$index] = $model->quantity;
  ?>
  <tr>
  <td><a href="<?php echo $model->viewUrl ?>" target="_blank"><?php echo $model->img ?></a></td>
  <td><a href="<?php echo $model->viewUrl ?>" target="_blank"><b><?php echo $model->product_name ?></b></a>
  <?php
  if(isset($model->variants))
  {
    echo '<dl>';
    foreach($model->variants as $name => $value)
      echo "<dt>$name</dt><dd>$value</dd>";
    echo '</dl>';
  }
  ?>
</td>
  <td style="min-width:85px;">
  <?php echo number_format($model->price, 2, ',', ' ') ?> грн.
  <?php 
  // сума по товарам текущей позиции
  if($model->quantity > 1) echo '<div class="prodSumm">' . number_format($model->price * $model->quantity, 2, ',', ' ') . ' грн.</div>' 
  ?>
  </td>
  <td><input type="number" class="quantity" id="q<?php echo $index ?>" name="quantity[<?php echo $index ?>]" value="<?php echo $model->quantity; ?>" min="1" max="500" size="4"></td>
  <td><a href="<?php echo '?action=remove&id='. $index ?>" id="d<?php echo $index ?>" class="delLink">Удалить</a></td>
  </tr>
  <?php  
}
?>
<tr><td><b>Итого:</b></td><td></td><td id="summary"><?php echo number_format($check, 2, ',', ' '); ?> грн.</td>
<td colspan="2" align="center">
<?php
if(!$hidePayButton)
  echo CHtml::ajaxSubmitButton('Перейти к оплате', $action.'&ajax=1', array( // добавляем в GET запрос переменную ajax=1 (сработает только если у юзера включен JS)
      'type' => 'POST',
      'success' => 'replaceContent',
      'beforeSend' => 'startLoad',
      'complete' => 'stopLoad',
  ),
  array(
      'type' => 'submit',
      'live' => false,
      'style'=> 'font-size:14px',
      'id' => 'doProcessOrderBtn'
  ));
?>
</td>
</tr>
</table>
<?php 
echo CHtml::endForm();
?>
</div>

<script type="text/javascript">
var quantity = <?php echo CJSON::encode($quantity) ?>;
var price =  <?php echo CJSON::encode($price) ?>;
</script>
<?php
} // конец if
