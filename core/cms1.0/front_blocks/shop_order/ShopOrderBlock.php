<?php

/**
 * Блок для отображения корзины как главного контента страницы и последующей оплаты заказа
 * 
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.shop_order
 */
Yii::import(GxcHelpers::getTruePath('front_blocks.shop_cart.ShopCartBlock'));
Yii::import('cms.models.order.*');
class ShopOrderBlock extends ShopCartBlock
{
	
	// переменная в которой хранится вся информация о корзине и заказе
	protected $order;
	protected $stepNum;
	protected $stepParams;

	public function run()
	{
		if(Yii::app()->request->isAjaxRequest || isset($_GET['ajax']))
			while(@ob_end_clean());

		if(isset($_GET['action'])) 
			switch($_GET['action']) {
				case 'order':
					$this->actionOrder();
					if(Yii::app()->request->isAjaxRequest || isset($_GET['ajax'])) 
						Yii::app()->end();

					return;
				break;
			}
		
		$this->defaultAction();

	}

	public function defaultAction()
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');
		$order = Yii::app()->session['order'];

		$cart = isset($order['cart']) && count($order['cart']) > 0 ? $order['cart'] : false;
		if(!$cart) 
			$this->redirectConsideringAjax();

		if(isset($order['summary'])) {
		// юзер прошел все этапы  по оплате электронными деньгами, показываем ему последний
			$this->redirectConsideringAjax($this->orderUrl);
			Yii::app()->end();
		}

		$this->render('output', array(
			'models' => $cart,	
			'blockId' => $this->getId(),  
			));
	}

	/**
	*  Оформление и оплата заказа
	*  Этот метод загружает данные заказа из сессии
	*  Вызывает методы для выбора шага, обработки информации шага, рендеринга шага
	*  Сохраняет новые данные обратно в сессию
	*  После оформления заказа (если это заказ доставки) очищает сессию
	**/
	public function actionOrder()
	{      
		// подгружаем данные из сессии
		$this->order = Yii::app()->session['order'];
		// если пустая корзина - редиректим юзера на главную 
		if(empty($this->order))
			$this->redirectConsideringAjax();
	
		// проводим проверку наличия данных и на основе этого и данных из $_GET вибираем номер шага
		// этот метод переопределяет значение переменной $this->stepNum
		$this->chooseStep()
		// вызываем метод для обработки данных текущего шага
		->callStep()
		// рендерим шаг
		->renderStep();

		if ($this->stepNum == 4 && !isset($this->order['summary']['action']))
		{
			$this->actionClear(); // Чистим сессию, если оплата наличкой   
		}else { // Сохраняем изменения в сессии 
			Yii::app()->session['order'] = $this->order;
		}
	}
	
	/**
	 *  Обертка для вызова шага по его номеру
	 *  Кроме вызова нужного шага она еще выполняет общий для каждого шага алгоритм
	 *  В качестве номера шага испольщуется $this->stepNum
	 *
	 *  Принцип, по которому чередуются шаги:
	 *    - метод $this->renderStep() выбирает номер шага $this->stepNum (самый ранний шаг, информация которого не сохранена в сессии)
	 *    - далее callStep() вызывает метод шага, по номеру $this->stepNum
	 *    - если в возвращаемых методом шага параметрах $this->stepParams->valid == true, значит шаг прошел проверку данных и сохранил их
	 *    - тогда вызываем метод следующего шага (который запросит у юзера необходимые ему данные)
	 *    - если $this->stepParams->valid != true метод прерывается, так как на предыдущем шаге ошибка
	 */
	protected function callStep()
	{
		$stepMethodName = 'step'.$this->stepNum;
		if(method_exists($this, $stepMethodName))
			$this->stepParams = $this->$stepMethodName();
		if(!$this->stepParams->valid || $this->stepNum == 4) // Если форма не валидная, рендерим текущий шаг еще раз
			return $this;
			
		// если до сюда дошло, значит предыдущий шаг успешно пройден, потому нам нужно переходить к следующему
		unset($_POST); // Чистим пост перед следующим шагом
		Yii::app()->session['order'] = $this->order;

		$this->stepNum++;

		return $this->callStep();
	}
	
	/**
	 *  Рендерит вьюху для шагов в оплате заказа
	 *  В зависимости от запроса использует render или renderPartial
	 *  В качестве массива с параметрами вьюхи используется: $this->stepParams
	 */
	protected function renderStep()
	{
		$params = $this->stepParams->params;
		// Меняем тип рендеринга в зависимости от запроса
		// Если аякс, нам надо рендерить только часть страницы
		if(Yii::app()->request->isAjaxRequest || isset($_GET['ajax']))
		{
			// Отключаем jquery (так как при ajax он уже подключен)
			Yii::app()->clientscript->scriptMap['jquery.js'] = Yii::app()->clientscript->scriptMap['jquery.min.js'] = false; 
		}
			
		$params['step'] = $this->stepNum;
		// маркер, который включит диалог "Есть ли у вас аккаунт?" на первом шагу по нажатию на кнопку "Далее"
		// это же условие дублируется в chooseStep
		$params['askAboutAccount'] = Yii::app()->user->isGuest && empty($this->order['address']);// && empty($this->order['newUser']);
		// если нам опять нужно спрашивать у юзера о том, как он будет регаться - удаляем переменную-маркер $this->order['newUser']
		if($params['askAboutAccount'] && $this->stepNum == 1) 
			unset($this->order['newUser']);

		$output = $this->render('_form', $params, true);
		if(Yii::app()->request->isAjaxRequest || isset($_GET['ajax']))
			Yii::app()->getClientScript()->render($output);
		echo $output;
	}
	
	/**
	 *  Проверяет, можно ли запустить текущий шаг
	 *  Меняет значение переменной $this->stepNum на необходимый шаг
	 *
	 *  last в параметре $_GET['step'] устанавливает шаг с сводной таблицей
	 *  Так же этот метод производит обновление колличества товаров в корзине, если информация о них поступила в $_POST
	 */
	protected function chooseStep()
	{
		// Сохраняем информацию о товарах из корзины
		if(isset($_POST['quantity']) && is_array($_POST['quantity']))
			foreach($_POST['quantity'] as $prodId => $quantity)
				$this->order['cart'][$prodId]->quantity = $quantity;
				
		if(empty($_GET['step'])) $_GET['step'] = 'last';
		$this->stepNum = $_GET['step'] == 'last' ? 3 : $_GET['step'];
		
		/*if(isset($this->order['summary']['action']))
			// скорей всего это повторный возврат на оплату
			// Перескакиваем на 3 этап, если  уже есть элемент summary
			$this->stepNum = 3;*/
		switch($this->stepNum)
		{
			case 4:
				if(!isset($this->order['summary']))
					$this->stepNum = 3;
			case 3:
				if(
				 (!isset($this->order['address']) || !isset($this->order['user']))
				 || isset($_POST['OrderAddress'])
				)
					$this->stepNum = 2;
			case 2:
				// проверка наличия данных с предыдущего шагаs
				if(
					!isset($this->order['order']['type'])
					// юзер должен попадать на первый шаг, пока он не заполнит свою контактную информацию
					|| (Yii::app()->user->isGuest && empty($this->order['address']) && empty($_POST['OrderAddress']))
					|| isset($_POST['Order']['type'])
				)
					$this->stepNum = 1;
		}

		return $this;
	}
	
	/**
	 *  Первый шаг в оформлении заказа
	 *  На этом шаге у юзера узнают какой способ оплаты, валюту и способ доставки он предпочитает
	 */
	protected function step1()
	{   
		$orderModel = new Order;
		$valid = false;
				
		$orderModel->type = $orderModel->currency = 1; // Значение по умолчанию
		if(isset($this->order['order']['type']) && isset($this->order['order']['currency']))
			$orderModel->attributes = $this->order['order'];

		if(isset($_POST['Order']['type']))
		{
			$valid = (array_key_exists($_POST['Order']['type'], $orderModel->orderType) && array_key_exists($_POST['Order']['currency'], $orderModel->orderCurrency));
			if($valid)
			{       
				// сохраняем тип оплаты и доставки
				$orderModel->attributes = $this->order['order'] = $_POST['Order'];
				
				// пользователь решился зарегистрироваться
				if(isset($_POST['newUser']))
					$this->order['newUser'] = $_POST['newUser'];
			}else
				$orderModel->addError('type', 'Выберите правильные способ оплаты и доставки');
		}

		return (object)array(
			'valid' => $valid,
			'params' => array(
			'model' => $orderModel,
			),
		);
	}
	
	/**
	 *  Второй шаг в оформлении заказа
	 *  На этом шаге юзер вводит свои контактные данные
	 */
	protected function step2()
	{    
		// если тип заказа соответствует заказу с доставкой, активируем соответствующий сценарий
		$adressModel = new OrderAddress( ($this->order['order']['type'] == 2?'':'delivery') );
		$valid = false;

		if(Yii::app()->user->isGuest)
		{
			if(isset($this->order['newUser'])) // создаем модель для регистрации
				$userModel = new UserRegisterForm();
			else // создаем модель для хранения гостевых заказов
				$userModel = new Client('withEmail');
		}
		else // Для залогиненого подгружаем его данные
		{
			$userModel = User::model()->findByPk(Yii::app()->user->id);
			$this->order['userRegistered']['id'] = $userModel->primaryKey;
			$this->order['userRegistered']['first_name'] = $userModel->first_name;
			$this->order['userRegistered']['last_name'] = $userModel->last_name;
			$this->order['userRegistered']['email'] = $userModel->email;
			$userAddresses = OrderAddress::model()->findAllByAttributes(array('user_id' => $userModel->primaryKey));
		}

		$userModel->attributes = isset($this->order['user']) ? $this->order['user'] : array();
		$adressModel->attributes = isset($this->order['address']) ? $this->order['address'] : array();
		
		if(isset($_POST['newAddress']) || isset($_POST['oldAddress']) || isset($_POST['OrderAddress']))
		{
			if(isset($_POST[get_class($userModel)]))
				$userModel->attributes = $_POST[get_class($userModel)];

			$adressModel->attributes = $_POST['OrderAddress'];
			
			if(isset($_POST['oldAddress']) && !isset($_POST['newAddress']))
			{
				// Юзер выбрал один из ранее веденных адресов
				$this->order['oldAddress'] = $_POST['oldAddress'];
				$valid = 1;
			}
			else
			{
				$this->performAjaxValidation(array($userModel, $adressModel));
				$this->order['oldAddress'] = false;
				$valid = $adressModel->validate();
			}
			
			$valid = $userModel->validate() && $valid;
			
			if($valid)
			{
				$this->order['address'] = $_POST['OrderAddress'];
				$this->order['user'] = isset($_POST[get_class($userModel)]) ? $_POST[get_class($userModel)] : null;
			}
		} 

		return (object)array(
			'valid' => $valid,
			'params' => array(
				'model1' => $userModel,
				'model2' => $adressModel,
				'oldAddress' => isset($this->order['oldAddress']) ? $this->order['oldAddress'] : false,
				'userAddresses' => isset($userAddresses) ? $userAddresses : array(),
			),
		);
	}
	
	/**
	 *  Третий шаг в оформлении заказа
	 *  На этом шаге мы выводим сводную таблицу с информацией заказа
	 *  На этом шаге не производится никакой обработки данных, кроме парсинга уже имеющихся
	 */
	protected function step3()
	{
		// дабы хоть раз показать этот шаг ставим ему $valid = 0
		$valid = false;
		// вводим коефициент для конвертации валюты для разных способов оплаты
		if($this->order['order']['currency'] > 1 && Yii::app()->params->currency['toEmoney'] > 0 && Yii::app()->params->currency['toDollar'] > 0)
			$currencyCoeff = Yii::app()->params->currency['toEmoney']/Yii::app()->params->currency['toDollar'];
		else
			$currencyCoeff = 1;

		$paymentAmount = 0; // Сумма заказа
		$prodCount = 0;
		foreach($this->order['cart'] as $cartProduct)
		{
			// суммарное колличество товаров
			$prodCount += $cartProduct->quantity;
			// сумма заказа
			$paymentAmount += $cartProduct->price  * $cartProduct->quantity * $currencyCoeff;

			// варианты товара (на пример разный цвет, размер и т.д.)
			if(isset($cartProduct->variants))
			{
				$variants =  '<dl>';
				foreach($cartProduct->variants as $name => $value)
					$variants .= "<dt>$name</dt><dd>$value</dd>";
				$variants .= '</dl>';
			}else{
				$variants = '';
			}
			// Массив для CGridView, что бы выводить информацию о заказе
			$orderInfo[] = array(
				'<a href="' . $cartProduct->viewUrl . '" target="_blank">' . $cartProduct->img . '</a>',
				'<a href="' . $cartProduct->viewUrl . '" target="_blank"><b>' . $cartProduct->product_name . '</b></a>' . $variants,
				$cartProduct->quantity,
				number_format($cartProduct->price * $currencyCoeff, 2, ',', ' ') . ' грн.' .
				($cartProduct->quantity > 1 ? '<br /> <b>' . number_format($cartProduct->price  * $cartProduct->quantity * $currencyCoeff, 2, ',', ' ') . ' грн.</b>' : ''),
			);
		}
		
		$orderModel = new Order;
		
		$this->order['summary'] = array(
			'orderPrice' => number_format($paymentAmount, 2, ',', ' '),
			'amount' => $paymentAmount, // для класса Emoney
			'prodCount' => $prodCount,
			'type' => $orderModel->orderType[$this->order['order']['type']],
			'delivery' => $this->order['order']['type'] == 1,
			'currency' => $orderModel->orderCurrency[$this->order['order']['currency']],
			'orderInfo' => $orderInfo,
		);

		// Если юзер выбрал старый адрес, переносим его параметры в массив address
		if($this->order['oldAddress'])
		{
			$model = OrderAddress::model()->findByPk($this->order['oldAddress']);
			$this->order['address']['street'] = $model->street;
			$this->order['address']['house'] = $model->house;
			$this->order['address']['flat'] = $model->flat;
			$this->order['address']['telephone'] = $model->telephone;
		}
		
		if(isset($this->order['userRegistered']))
			$this->order['user']['first_name'] = $this->order['userRegistered']['first_name'];
		
		return (object)array(
			'valid' => $valid,
			'params' => array(
				'summary' => $this->order['summary'],
				'order' => $this->order['order'],
				'user' => $this->order['user'],
				'address' => $this->order['address'],
			),
		);
	} 
	
	/**
	 *  Четвертый шаг в оформлении заказа
	 *  На этом шаге производится обработка данных заказа и сохранение их в бд
	 */
	protected function step4()
	{
		// Начинаем транзакцию
		$transaction = Yii::app()->db->beginTransaction();
		try
		{
			$valid = 1;
			// для случая оплаты электронными деньгами
			// если элемент массива $this->order['summary']['action'] уже определен, значит юзер уже пытался оплачивать заказ
			// потому пропускаем обработку данных заказа
			// при оплате наличкой к этот блок кода будет выполняться один раз
			if(!isset($this->order['summary']['action']))
			{ // Производим обработку введенных данных
				$orderModel = new Order;
				$userId = isset($this->order['userRegistered']['id']) ? $this->order['userRegistered']['id'] : null;
				// если это новый юзер/гостевой заказ
				if(!$userId)
				{
					// регистрируем юзера, в том случае, если он ввел пароли
					if(isset($this->order['user']['password1']) && !empty($this->order['user']['password1']))
					{
						$userRegisterForm = new UserRegisterForm();
						// подгружаем данные из сессии
						$userRegisterForm->attributes = $this->order['user'];

						// регистрируем и авторизируем юзера
						// Отправляем письмо с подтверждением email адреса
						$userModel = $userRegisterForm->doSignUp();

						$valid = $userModel != false;

						if(!$valid)
							Yii::log("UserModel: \n" . CVarDumper::dumpAsString($this->order['user']) . "\n\nErrors: \n" . CVarDumper::dumpAsString($userModel->errors), 'error', 'order');
						$userId = $userModel->primaryKey;
					}
					else
					{
						// Пишем контактные данные юзера в таблицу order_client
						$clientModel = new Client('withEmail');
						// подгружаем данные из сессии
						$clientModel->attributes = $this->order['user'];
						
						// задаем null как id юзера, что бы переключить на order_client
						$userId = new CDbExpression('NULL');
					}
				}     

				$addressId = $this->order['oldAddress'];
				// если юзер ввел новый адрес
				if(empty($addressId))
				{
					// если тип заказа соответствует заказу с доставкой, активируем соответствующий сценарий
					$adressModel = new OrderAddress( (($this->order['order']['type'] == 2)?'':'delivery') );
					// подгружаем данные из сессии
					$adressModel->attributes = $this->order['address'];
					// вставляем id юзера
					$adressModel->user_id = isset($this->order['userRegistered']) ? $this->order['userRegistered']['id'] : $userId;
					$valid = $adressModel->save() && $valid;
					if(!$valid)
						Yii::log("AddressModel: \n" . CVarDumper::dumpAsString($this->order['address']) . "\n\nErrors: \n" . CVarDumper::dumpAsString($adressModel->errors), 'error', 'order');
					$addressId = $adressModel->primaryKey;
				}

				$this->order['order']['user_id'] = $userId;
				$this->order['order']['address_id'] = $addressId;
				
				// Если оплата электронными деньгами
				// Присваеваем статус "отмененного" заказа, до того момента, пока не произойдет оплата
				if($this->order['order']['currency'] > 1  && $this->order['order']['currency'] != 8)
					$this->order['order']['status'] = 4; 
					
				// подгружаем данные из сессии
				$orderModel->attributes = $this->order['order'];

				$valid = $orderModel->save() && $valid;
				if(!$valid)
					Yii::log("OrderModel: \n" . CVarDumper::dumpAsString($this->order['order']) . "\n\nErrors: \n" . CVarDumper::dumpAsString($orderModel->errors), 'error', 'order');
				 
				// если пользователь не захотел регистрироваться во время заказа
				if(isset($clientModel))
				{
					$clientModel->order_id = $orderModel->primaryKey;
					$valid = $clientModel->save() && $valid;
					if(!$valid)
						Yii::log("clientModel: \n" . CVarDumper::dumpAsString($this->order['user']) . "\n\nErrors: \n" . CVarDumper::dumpAsString($clientModel->errors), 'error', 'order');
				}
				
				// Создаем чек
				$paymentAmount = 0; // Сумма заказа
				// вводим коефициент для конвертации валюты для разных способов оплаты
				// TODO: настроить валюту
				if(0&&$this->order['order']['currency'] > 1 && Yii::app()->params->currency['toDollar']>0 && Yii::app()->params->currency['toEmoney']>0)
					$currencyCoeff = Yii::app()->params->currency['toEmoney']/Yii::app()->params->currency['toDollar'];
				else
					$currencyCoeff = 1;

				foreach($this->order['cart'] as $cartProduct)
				{
					$checkModel = new OrderCheck;
					$checkModel->order_id = $orderModel->primaryKey;
					$checkModel->prod_id = $cartProduct->id;
					$checkModel->quantity = $cartProduct->quantity;
					$checkModel->price = $cartProduct->price * $currencyCoeff;
					if(isset($cartProduct->variants))
						$checkModel->meta = serialize($cartProduct->variants);
					$valid = $checkModel->save() && $valid;
					
					$paymentAmount += $checkModel->price * $cartProduct->quantity;
					if(!$valid)
						Yii::log("CheckModel: \n" . CVarDumper::dumpAsString($checkModel->errors), 'error', 'order');
				}
				
				// Если оплата электронными деньгами, создаем массив с параметрами
				if($orderModel->currency > 1 && $orderModel->currency != 8)
				{
					if($orderModel->currency < 5 ) // WM
						$merchantName = 'WM';
					elseif($orderModel->currency < 8 ) // Privat24
						$merchantName = 'Privat24';
					
					// генерируем форму для оплаты электронными деньгами
					// сохраняем ее в кэш, для последующего использования при валидации запросов от мерчанта
					$emoney = Emoney::choose($merchantName)
					->createPayment(array(
						'orderNo' => $orderModel->id,
						'amount' => $paymentAmount,
						'desc' => Yii::app()->params['shortName'] . ': Оплата заказа №'.$orderModel->primaryKey,
					))
					->save();
					
					$this->order['summary']['action'] = $emoney->formAction;
				}
				// добавляем номер заказа и дату
				$this->order['summary']['orderNo'] = $orderModel->id;
				$this->order['summary']['orderDate'] = Yii::app()->dateFormatter->formatDateTime((string)$orderModel->date);
			}// if(!isset($this->order['summary']['action']))
			
			//коммитим транзакцию
			$transaction->commit();
		}
		catch (Exception $e)
		{
			// откат транзакции, сообщаем юзеру об ошибке
			$transaction->rollBack();
			$valid = 0;
		}

		if($valid && !isset($this->order['summary']['action']))
		{
			// отправляем письмо о успешном оформлении заказа
			$this->sendOrderSummary();
		}

		return (object)array(
			'valid' => $valid,
			'params' => array(
				'valid' => $valid,
				'emoneyAction' => isset($this->order['summary']['action']) ? $this->order['summary']['action'] : false,
				'emoneyFields' => isset($this->order['summary']['fields']) ? $this->order['summary']['fields'] : '',
			),
		);
	}
	
	/**
	*   Обработка запросов от мерчантов
	**/
	public function actionResult()
	{
		Emoney::chooseFromPost()->onSuccess(array($this, 'emoneySuccessHandler'))->resultAction();
	}
	
	/**
	*   Обработчик события успешной оплаты
	**/
	public function emoneySuccessHandler($event)
	{
		$orderModel = Order::model()->findByPk($event->sender->orderNo);
		$orderModel->status = Order::NOT_COMPLETE;
		$orderModel->save();
	}
	
	/**
	* Очищает сессию и перенаправляет юзера на page/success
	**/
	public function actionSuccess()
	{
		Emoney::chooseFromPOST()->successAction();
		// отправляем письмо о успешном оформлении заказа
		$this->sendOrderSummary();
		
		$this->actionClear();
		$this->controller->redirect(array('/page/index', 'path'=>'success'));
	}
	
	/**
	*  Отправляет письмо пользователю с оповещением об успешной оплате заказа
	**/
	public function sendOrderSummary()
	{
		
		$message = new YiiMailMessage;
		$message->view = GxcHelpers::getTruePath('front_blocks.shop_order.views.email.orderSummary');
		
		$user = isset($this->order['userRegistered']) ? $this->order['userRegistered'] : $this->order['user'];
		
		$message->setBody(array(
			'user' => $user,
			'summary' => $this->order['summary'],
			'address' => $this->order['address'],
			'cart' => $this->order['cart'],
		), 'text/html');
			
		
		$message->addTo($user['email']);

		// отправляем скрытые копии для операторов
		$bccEmails = Yii::app()->modules['cart']['params']['bccEmails'];
		if(!empty($bccEmails))
		{
			if(strpos($bccEmails, ',')) $bccEmails = preg_split('/ *, */', $bccEmails);
			else $bccEmails = array($bccEmails);
			$message->setBcc($bccEmails);
		}

		$message->subject = 'Информация о заказе №' . $this->order['summary']['orderNo'];
		$message->from = array(Yii::app()->params['noReplyEmail'] => Yii::app()->params['shortName']);
		Yii::app()->mail->send($message);
	}

	public function redirectConsideringAjax($url = '/')
	{
		if(Yii::app()->request->isAjaxRequest || isset($_GET['ajax']))
		{
			echo '<script>location.href = "'.$url.'";</script>';
		}else
			$this->controller->redirect($url);
	}
	
	/**
	* AJAX валидация формы
	**/
	protected function performAjaxValidation(array $models)
	{
			if(isset($_POST['ajaxValidate']))
			{
				echo CActiveForm::validate($models); 
				Yii::app()->end();
			}
	}
}
