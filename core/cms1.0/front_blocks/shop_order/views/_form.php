<?php
/**
 * @var CActiveRecord $model
 * @var boolean $valid
 *
 * 2nd step:
 * @var User $model1
 * @var OrderAddress $model2
 * @var boolean|integer $oldAddress
 *
 * 3d step:
 * @var $summary
 * @var $order
 * @var CActiveRecord $user
 * @var $address
 *
 * 4th step:
 * 
 * @var boolean|string $emoneyAction - action формы оплаты электронными деньгами
 * @var string $emoneyFields - html код полей необходимых для формы оплаты электронными деньгами
 */
$model = isset($model) ? $model : null;
$model1 = isset($model1) ? $model1 : null;
$model2 = isset($model2) ? $model2 : null;

$formAction = (isset($emoneyAction))?$emoneyAction:'/cart?action=order&step=' . ($step + 1);

$butt = function($value, $step = false, $submit = true) use ($formAction) {
	if($step)
		$formAction = preg_replace('/\d$/', $step, $formAction);

	return CHtml::ajaxButton($value, $formAction . '&ajax=1', array(
			'type' => 'POST',
			'success' => 'replaceContent',
			'beforeSend' => 'startLoad',
			'complete' => 'stopLoad',
			'error' =>'js:function(a){console.log(a.responseText)}',
	),
	array(
			'type' => $submit ? 'submit' : 'button',
			'live' => false,
			'id' => $submit ? 'nextStep' : null,
	));
};

echo '<div id="cartContent" class="form">';
$form = $this->beginWidget('CActiveForm', array(
		'id'=>'cartForm',
		'enableAjaxValidation'=>true,
		'enableClientValidation'=>true,
		'action' => $formAction,
		'clientOptions' => array(
			'ajaxVar' => 'ajaxValidate',
		),
)); 

if (is_object($model))
	echo $form->errorSummary($model);
if (is_object($model1))
	echo $form->errorSummary($model1);
if (is_object($model2))
	echo $form->errorSummary($model2);

switch($step)
{ 
	case 1:
		echo "<h1>Выберите способ оплаты</h1>"; 
		echo '<p>' . $form->radioButtonList($model, 'currency', $model->orderCurrency) . '</p>';
		if(in_array('Безналичный расчет', $model->orderCurrency) && Yii::app()->params['currency']['toEmoney'] > 0)
			echo '<p><b>Внимание!</b> Б/Н оплата и оплата электронными деньгами произвдится по курсу: 1$=' . Yii::app()->params->currency['toEmoney'] . 'грн.<br />
		Пересчет суммы к оплате будет произведен перед последним шагом оформления заказа.
		</p>';
		
		echo "<h1>Выберите способ доставки</h1>"; 
		echo '<p>' . $form->radioButtonList($model, 'type', $model->orderType) . '</p>';
	break;
	case 2:
		echo "<h1>Введите информацию, необходимую для заказа</h1>"; 
		echo '<p>Поля, помеченные <span class="required">*</span> обязательны</p>';
		if(Yii::app()->user->isGuest)
		{
			echo '<div class="row" style="float:left;">' . $form->labelEx($model1, 'first_name') . $form->textField($model1, 'first_name', array('style'=>'width:180px;')) . $form->error($model1,'first_name') . '</div>';
			echo '<div class="row" style="float:right;">' . $form->labelEx($model1, 'last_name') . $form->textField($model1, 'last_name', array('style'=>'width:180px;')) . $form->error($model1,'last_name') . '</div>';
			echo '<div class="row"  style="clear:both;">' . $form->labelEx($model1, 'email') . $form->textField($model1, 'email', array('style'=>'width:565px')) . $form->error($model1,'email') . '</div>';
		}
		
		$addressStyle = '';
		if(!Yii::app()->user->isGuest && $model1 && count($userAddresses))
		{
			echo '<fieldset>
				<legend>Ранее использованные вами контактные данные</legend>';
			
			// У залогиненого юзера могут быть адреса, потому даем ему возможность выбрать один из них
			$addressArr = array();
			foreach($userAddresses as $address)
			{
				$addressArr[$address->id] = 'Тел: <b>' . $address->telephone . '</b>; ';
				if($address->street != '')
					$addressArr[$address->id] .= 'Адрес: <b>' . $address->street . ', ' . $address->house.(($address->flat)?', кв. '.$address->flat:'') . '</b>';
				if(!isset($first))        
					$first = $address->id;
			}
			
			$first = $oldAddress ? $oldAddress : $first;

			if(count($userAddresses))
				$addressStyle = ' style="display:none;"';
			
			echo '<p>' . CHtml::radioButtonList('oldAddress', $first, $addressArr) . '</p>'; 
			
			echo '<br /><p>' . CHtml::checkBox('newAddress', false, array('onchange'=>'js:$("#address").toggle()')) . CHtml::label('Использовать другие контактные данные', 'newAddress') . '</p>';
				
			echo '</fieldset>';
		}

		echo '<fieldset id="address"' . $addressStyle . '>
		<legend>Контактные данные и адрес доставки</legend>';
		echo '<div class="row">' . $form->labelEx($model2, 'telephone') . $form->textField($model2, 'telephone', array('style'=>'width:545px')) . $form->error($model2,'telephone') . '</div>';
		if($model2->scenario == 'delivery') // Если выбрана доставка, то мы еще отображаем поля для ввода адреса
		{
			echo '<div class="row">' . $form->labelEx($model2, 'street') . $form->textField($model2, 'street', array('style'=>'width:545px')) . $form->error($model2,'street') . '</div>';
			echo '<div class="row">' . $form->labelEx($model2, 'house') . $form->textField($model2, 'house', array('style'=>'width:545px')) . $form->error($model2,'house') . '</div>';
			echo '<div class="row">' . $form->labelEx($model2, 'flat') . $form->textField($model2, 'flat', array('style'=>'width:545px')) . $form->error($model2,'flat') . '</div>';
		}  
		echo '</fieldset>';
		if (is_a($model1, 'UserRegisterForm'))
		{
			echo '<fieldset>
			<legend>Пароли для доступа к сайту</legend>';
			echo '<div class="row">' . $form->labelEx($model1, 'password1') . $form->passwordField($model1, 'password1', array('style'=>'width:545px')) . $form->error($model1,'password1') . '</div>'
				. '<div class="row">' . $form->labelEx($model1, 'password2') . $form->passwordField($model1, 'password2', array('style'=>'width:545px')) . $form->error($model1,'password2') . '</div>'
				. '<p><b>Внимание</b>, на ваш <b>Email</b> придет письмо с ссылкой для активации аккаунта</p>';
			echo '</fieldset>';
		}
	break;
	case 3:
		?>
			<h1><?php echo $user['first_name'] ?>, детали вашего заказа</h1>
			<br />
			<h2>Общая информация</h2>
			<p><b>Способ оплаты</b>: <?php echo $summary['currency'] ?></p>
			<p><b>Способ доставки</b>: <?php echo $summary['type'] ?></p>
			<p><b>Контактный номер</b>: <?php echo $address['telephone'] ?></p>
			<p><b>Адрес доставки</b>: <?php echo ($address['street'] ? 'ул. ' . $address['street'] . ', д. ' . $address['house'] . ( $address['flat'] ? ', кв. ' . $address['flat'] : '') : 'Не указан');?></p>
			<br />
			<h2>Товары в вашем чеке</h2>
		<?php

		$dataProvider=new CArrayDataProvider($summary['orderInfo'], array(
				'keyField' => false,
				'pagination'=>array(
					'pageSize'=>count($summary['orderInfo']),
				),
		));
		
		$this->widget('zii.widgets.grid.CGridView', array(
			'dataProvider'=>$dataProvider,
			'columns'=>array(
				array(
					'name' => '0',
					'header' => '',
					'type' => 'raw',
					'footer' => '<b>Итого:</b>',
				),
				array(
					'name' => '1',
					'header' => 'Наименование товара',
					'type' => 'raw',
				),
				array(
					'name' => '2',
					'header' => 'Количество',
					'htmlOptions' => array('align' => 'center'),
				),
				array(
					'name' => '3',
					'header' => 'Цена за шт. <br /> Сумма',
					'footer' => '<b>' . $summary['orderPrice'] . ' грн.</b>',
					'htmlOptions' => array('width' => '100'),
					'type' => 'raw',
				),
			),
			'cssFile'=>false,
			'ajaxUpdate' => false,
			'pager'=>false,
				'summaryText'=>false,
		));
	break;
	case 4:
		if($valid)
		{      
			if($emoneyAction)
			{// Форма для перенаправления на оплату
				echo $emoneyFields;
				// сразу отправляем форму на адрес мерчанта
				?>
				<script type="text/javascript">$("#cartForm").submit();</script>
				<noscript>
					<?php echo CHtml::submitButton('Перейти к оплате заказа'); ?>
				</noscript>
				<?php
			}
			else
			{ 
				?>
				<h1>Спасибо за покупку!</h1>
				<p>В ближайшее время с вами свяжется наш оператор для уточнения подробностей заказа.</p>
				
				<br />
				<p><a href="/" class="button">Вернутся на главную страницу</a></p>
				<?php
			}
		}
		else
		{
			?>
			<h1>Извините, во время заказа произошла ошибка!</h1>
			<p>Для уточнения подробностей, пожалуйста свяжитесь с нами <a href="mailto:<?php echo Yii::app()->params['adminEmail']; ?>"><?php echo Yii::app()->params['adminEmail']; ?></a>.</p>
			<br />
			<p><a href="/" class="button">Вернутся на главную страницу</a></p>
			<?php
		}
	break;
}

echo '<p>'; 

if($step == 3)
		echo '<div style="float:right;">' . $butt( (isset($emoneyAction) ? 'Перейти к оплате заказа' : 'Завершить заказ') ) . '</div>';
if($step != 4)
	if($step > 1)
		echo $butt('Назад', $step - 1, false) . ' ';
	else
		echo '<a href="/cart" class="button">Назад</a> ';
	if($step == 3)
			echo CHtml::ajaxButton('Отмена заказа', $this->clearCartUrl, array('complete'=>'js: function() {location.href="/"}'));
			
if($step == 1 && $askAboutAccount)
{//Справшиваем, есть ли у юзера аккаунт перед переходом на следующий шаг
	echo CHtml::link(CHtml::button('Далее'), '', array(
		'onclick' =>'return askAboutAccount();', 
	));
	
	echo '<p style="display:none;">' . $butt('Далее') . '</p>';
}
else
{    
	if($step < 3)
	{
		echo $butt('Далее');
	}
}
echo '</p>';
echo '</div>'; //#cartContent
	
//echo CHtml::hiddenField('step', $step);

$this->endWidget(); 

if($askAboutAccount) 
{
	// TODO: пускай в SigninAjaxBlock можно будет выбирать какие именно тебе нужны формы (регистрация/логин)
	// TODO: пускай в SigninAjaxBlock можно будет отключать вывод кнопок Login/logout/register
	echo CHtml::tag('div', array('style' => 'display:none'),
		$this->widget(GxcHelpers::getTruePath('front_blocks.signinajax.SigninAjaxBlock'), array(
			'onLoginSuccess' => 'function(container) {$("#askAboutAccount").dialog("close");$(container).dialog("close");$("#nextStep").click();}',
			'block' => (object)array(
				'block_id' => 'Cart',
				'params' => serialize(array()),
				'type' => 'signinajax'
				),
			), true)
	);

}
?>
