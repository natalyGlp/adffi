<div name="div-block-content-<?php echo $block_model->id; ?>">
    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'imageSize'), ''); ?>
        <?php $this->widget('cms.widgets.resource.ResourceSizePicker', array(
            'blockWidget' => $block_model,
            'attribute' => 'imageSize',
        )); ?>
        <?php echo $form->error($model, 'imageSize'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label(Block::getLabel($block_model, 'listViewPageId'), ''); ?>
        <?php $this->widget('cms.widgets.page.PageAutoComplete', array(
            'name' => "Block[listViewPageId]",
            'value' => $block_model->listViewPageId,
        )); ?>
        <?php echo $form->error($model, 'listViewPageId'); ?>
    </div>
</div>