<?php

/**
 * Class for render Comment List * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.comment_list */

class CommentListBlock extends BlockWidget
{
	/**
	 * $object_id The id of the content to which the searched comments belong.
	 * @var int
	 */
	public $object_id;

	public function run()
	{                 
	   $this->renderContent();         
	}       
 
 
	protected function renderContent()
	{     
		$this->render('output',array(
			'dataProvider' => self::getCommentList(),
			));                                                          	       		     
	}

	public function getCssClass($class)
	{
		return $class . ' ' . $this->cssClass . '_' . $class;
	}
	
	/**
	 * Get the comment list of the object or current page by `slug` or `objectSlug`
	 * @return CActiveDataProvider
	 */
	public static function getCommentList()
	{
		$controller = Yii::app()->controller;
		$object = $controller->object;
		$allowComments = false;
		if($object) {
			$allowComments = $object->comment_status == ConstantDefine::OBJECT_ALLOW_COMMENT;
			$guid = $object->guid;
		} else {
			// checking if we have the page with the $slug
			$guid = $controller->page->guid;
			$allowComments = true;
		}

		if(isset($guid) && $allowComments) {

			//Search for the PUBLISHED comments that belong to the $object_id
			$dataProvider = new CActiveDataProvider('Comment',array(
				'criteria'=>array(
					'condition'=>'t.parent_guid = :guid and t.status = :status',
					'params'=>array(
						':guid'=>$guid,
						':status'=>ConstantDefine::COMMENT_STATUS_PUBLISHED,
					),
				),
			));

			return $dataProvider;
		}
		//return null if object not found or not allowed for comment
		return null;
	}
}

?>