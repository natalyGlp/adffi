<?php
/**
 * Class for render SocialButtons
 *
 * @author Bazai best programmer <developer@studiobanzai.com>
 * @version 0.1
 * @package front_blocks.socialwidgets
 */

class TermTabsBlock extends BlockWidget
{
    public $terms = array();
    public $title = '';
    public $titleLink = '';

    public $useSliderkit = false;
    public $visibleSlidesAmount = 4;

    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        $this->render('output', array(
            'cssClass' => $this->cssClass,
            'title' => $this->title,
            'titleLink' => Yii::app()->baseUrl.'/'.trim($this->titleLink, '/'),
            'tabs' => $this->tabs,
            ));
    }

    protected function getTabs()
    {
        $terms = Term::model()->with('objects')->findAllByPk($this->terms);

        $tabs = array();
        foreach ($terms as $term) {
            $tabHeader = $this->render('_tab', array(
                    'term' => $term,
                ), true);

            if ($this->useSliderkit) {
                $tabs[$tabHeader] = $this->getSliderkitTabContent($term);
            } else {
                $tabs[$tabHeader] = implode('', $this->getDefaultTabContent($term));
            }
        }

        return $tabs;
    }

    protected function getDefaultTabContent($term)
    {
        $content = array();
        foreach ($term->objects as $object) {
            $content[] = $this->renderTabContent($object, $term);
        }

        return $content;
    }

    protected function renderTabContent($object, $term)
    {
        $tabContent = $this->render('_tab-content-item', array(
            'data' => $object,
            'term' => $term,
        ), true);

        return CHtml::tag('div', array(
                'class' => 'item',
            ), $tabContent);
    }

    protected function getSliderkitTabContent($term)
    {
        $content = $this->getDefaultTabContent($term);

        return $this->widget(GxcHelpers::getTruePath('front_blocks.content_slider_sliderkit.ContentSliderSliderkitBlock'), array(
            'navHtml' => $content,
            'shownavitems' => $this->visibleSlidesAmount,
            'circular' => true,
            'auto' => false,
            ), true);
    }

    public function params()
    {
        return array(
            'terms' => t('Select the terms to show tabs for'),
            'title' => t('Title'),
            'titleLink' => t('Title Link'),

            'useSliderkit' => t('Use sliderkit'),
            'visibleSlidesAmount' => t('Slides shown at a time'),
        );
    }
}
