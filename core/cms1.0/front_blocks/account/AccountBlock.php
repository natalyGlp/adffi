<?php

/**
 * Class for render Account Page
 * 
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.front_blocks.account
 */

class AccountBlock extends BlockWidget
{
    public function run()
    {       
        if(!user()->isGuest) { 
            $this->renderContent();
        } else {
            user()->setFlash('error',t('You need login to continue!'));                                                         
            Yii::app()->controller->redirect(bu().'/sign-in');
        }
    }       
 
    protected function renderContent()
    {     
        $model=new UserChangePassForm;
		$model_notify=new UserNotifyForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='changepass-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		
		// if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='notify-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

		$u=User::model()->findbyPk(user()->id);
        if($u!==null) {
			//Set Email Notify from User
			$model_notify->email_site_news=$u->email_site_news;				                           
			$model_notify->email_search_alert=$u->email_search_alert;
							
		 	// collect user input data
            if(isset($_POST['UserChangePassForm'])) {
                $model->attributes=$_POST['UserChangePassForm'];
                
                // validate user input password
                if($model->validate()) {				                       
                    $u->password=$u->hashPassword($model->new_password_1,  USER_SALT);
                    $u->salt=USER_SALT;
                    if($u->save()) {               
                        user()->setFlash('success',t('Change Password Successfully!'));                                        
                    }				                            
                    $model=new UserChangePassForm;				
                }
            }
			
			// collect user input data
            if(isset($_POST['UserNotifyForm'])) {
                $model_notify->attributes=$_POST['UserNotifyForm'];				                    
                // validate user input password
                if($model_notify->validate()) {
                    $u->email_site_news=$model_notify->email_site_news;				                           
					$u->email_search_alert=$model_notify->email_search_alert;
                    if($u->save()) {               
                        user()->setFlash('success',t('Update Notification Successfully!'));                                        
                    }				                            				                            			
                }
            }
		} else {
			throw new CHttpException('403','User is not existed');
		}   
        $this->render('output',array('model'=>$model,'model_notify'=>$model_notify));
    }
}

?>