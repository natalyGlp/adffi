/*
Social v 1.0
by Svaitoslav Danylenko - http://hamstercms.com

Licensed under the GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)
- free for use in personal projects
- attribution requires leaving author name, author link, and the license info intact
*/

setTimeout(function() {
	if(!window.hSocialInit)
	{
		window.___gcfg = {                // Настройки языка для Google +1 Button
			lang: 'ru'
		};

		var apis = {
			gpApi: 'https://apis.google.com/js/plusone.js',                         // Google +1 Button
			twApi: '//platform.twitter.com/widgets.js',                             // Twitter Widgets
			fbApi: '//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0',      // Facebook SDK
			vkApi: "//vk.com/js/api/openapi.js?105"                                 // Vkontakte API
		},
		script   = 'script',

		fragment = document.createDocumentFragment(),
		element  = document.createElement(script),

		clone,

		vkAppId = $('meta[property="vk:app_id"]').eq(0).prop('content');
		if(vkAppId == undefined || vkAppId == '')
			delete apis.vkApi;

		// вставляем в дом скрипты для всех apis
		for (var id in apis)
		{
			clone = element.cloneNode(false);
			clone.async = true;
			clone.src = apis[id];
			clone.id = id;
			clone.type = 'text/javascript';
			fragment.appendChild(clone);
		}

		clone = document.getElementsByTagName(script)[0];
		clone.parentNode.insertBefore(fragment, clone);

		
		window.vkAsyncInit = function() {
			$('body').trigger('vkInit');
		};

		window.hSocialInit = true;
	}
}, 200);

(function($) {
	$.fn.hvklike = function(settings) {
		var vkLikeInit = function(obj, ind) {
			obj.id = obj.id == '' ? 'vk-like-' + ind : obj.id;
			window.VK.Widgets.Like(obj.id, settings);
		};

		return this.each(function () {
			var $this = $(this);
			if ($(this).is(':empty'))  
			{
				if(window.VK === undefined)
				{
					$('body').off('vkInit', vkLikeInit);
					$('body').on('vkInit', function() {vkLikeInit(that, i)});
				}
				else
				{
					vkLikeInit(this, i);
				}
			}
		})
	};
})(jQuery);