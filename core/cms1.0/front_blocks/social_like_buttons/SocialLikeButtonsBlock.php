<?php
/**
 * Class for render Social Like Buttons 
 * 
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 0.1
 * @package common.front_blocks.social_like_buttons 
 */

class SocialLikeButtonsBlock extends BlockWidget
{
	public $need_fb;
	public $need_vk;
	public $vkApiId;
	public $need_gp;
	public $need_tw;
	public $widgetSize = 'big';
	public $widgetOrientation = false;
	public $twitterVia = '';
	public $sharethisPubKey = false;
	
	public function run()
	{           
		if(!$this->vkApiId) {
			$this->vkApiId = Yii::app()->settings->get('social', 'vkApiId');
		}

		$this->renderContent();         
	}       
 
	protected function renderContent()
	{     
		$this->render('output',array());                                                          	       		     
	}
	
	public function params()
	{
		return array(
			'need_fb' => t('Show Facebook Like button'),
			'need_vk' => t('Show Vkontakte Like button'),
			'vkApiId' => t('Vkontakte Api Id'),
			'need_gp' => t('Show G+ Like button'),
			'need_tw' => t('Show Twitter Like button'),
			'widgetSize' => t('Widget Size'),
			'widgetOrientation' => t('Widget Orientation'),
			'twitterVia' => t('Twitter Via'),
			'sharethisPubKey' => t('sharethis.com Pub Key'),
		);
	}
}

?>
