'use strict';

(function() {
    /**
     * Сателлитка. Управление тематиками
     */
    function initModalForm(selector, url) {
        $(selector).submit(function(e){
            e.preventDefault();
            $.post(url, $(this).serialize(), function(data) {
                if(data == 'success'){
                    $('#myModal').modal('hide');
                    window.location.reload();
                    return false;
                }
                setModalData(data);
                initModalForm(selector, url);
            });
        });
    }

    function setModalData(data, show) {
        $('#myModal').html('').append($(data));
        if(show)
            $('#myModal').modal('show');
    }

    /**
     * Редактирование/добавление тематик и категорий для раздела Тематики (/thematics)
     */
    function thematicsManage(url) {
        $.get(url, function(data) {
            setModalData(data, true);
            var $form = $('#myModal').find('form');
            if($form.length > 0)
                initModalForm($form.eq(0), url);
        });
    }

    $(function() {
        $('body').on('click', '.doManageThematics', function() {
            thematicsManage(this.href);
            return false;
        });
    });
}());

/**
 *Convert a string to a Slug String
 *
 **/
var ru2en = {
    ru_str : "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя ",
    en_str : ['A','B','V','G','D','E','JO','ZH','Z','I','J','K','L','M','N','O','P','R','S','T',
    'U','F','H','C','CH','SH','SHH','','I','','JE','JU',
    'JA','a','b','v','g','d','e','jo','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f',
    'h','c','ch','sh','shh','','i','','je','ju','ja','-'],

    toSlug: function(org_str) {
        var tmp_str = [];
        for (var i = 0, l = org_str.length; i < l; i++) {
            var s = org_str.charAt(i), n = this.ru_str.indexOf(s);
            if (n >= 0) {
                tmp_str[tmp_str.length] = this.en_str[n];
            } else {
                tmp_str[tmp_str.length] = s;
            }
        }

        var str = tmp_str.join("");
        str = str.toLowerCase();
        str = str.replace(/\s/g, '-');
        str = str.replace(/\-+/g, '-');
        str = str.replace(/[^a-z0-9\-_]/g, '');
        return str;
    }

};

function str2url(str,encoding,ucfirst)
{
    str = ru2en.toSlug(str);
    return str;
}

/**
 *
 * Get the id before @ in an Email Address
 **/
function getIdinEmail(str_email){
    var ind=str_email.indexOf("@");
    var my_slice=str_email.slice(0,ind);
    return my_slice
}

/**
 * Copy String From a Text Field to Another Text Field
 */
function CopyString(id_from,id_to,type){
    var func = function() {
        $(id_to).val($(id_from).val().trim());
    };

    if(type=='slug') {
        var func = function() {
            $(id_to).val(str2url($(id_from).val().trim()));
        };
    }

    if(type=='text') { // strip html tags
        var func = function() {
            $(id_to).val($('<div>').html($(id_from).val().trim()).text());
        };
    }

    if(type=='email'){
        var func = function() {
            $(id_to).val(getIdinEmail($(id_from).val().trim()));
        };
    }
    $(id_from).keyup(func)
              .change(func)
              .bind('drop', function() {
                //when the drop happens the input value will not be updated yet
                //use a timeout to allow the input value to change.
                setTimeout($.proxy(func, this), 0);
              });
}

function autoResize(iframe) {
    var context = iframe.contentWindow;
    var body = context.document.body;
    var interval;

    if (!iframe.src) {
        return;
    }

    var createReference = function() {
        return $('<div>')
            .css({
                height: '0px',
                width: '1px',
                clear: 'both'
            })
            .attr('id', 'autoresize-reference')
            .appendTo(body)
            ;
    };

    var getReference = function() {
        var $reference = $('#autoresize-reference', body);
        if (!$reference.length) {
            $reference = createReference();
        }

        return $reference;
    }

    var resize = function() {
        try {
            var $reference = getReference();

            var offsetTop = $reference.offset().top;

            if (offsetTop === 0) {
                throw new Error('The offset is zero, probably the content is not loaded. Waiting...');
            }

            var shouldResize = offsetTop != $(iframe).height();
            if (shouldResize) {
                $(iframe).height(offsetTop);
            }
        } catch (e) {
            stop();
            console.log('Stopping: ' + e.message);
        }
    };

    var start = function() {
        interval = setInterval(resize, 500);
        $(iframe).attr('data-autoresize', 'true');
    }

    var stop = function() {
        clearInterval(interval);
        interval = null;
        $(iframe).attr('data-autoresize', 'false');
    }

    $(iframe)
        .hover(start, stop)
        .css('height', '')
        ;

    $(context.document).ready(resize);
}
