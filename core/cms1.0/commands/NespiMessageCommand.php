<?php

/**
 * Это копипаст оригинальной комманды yii MessageCommand, который переписан
 * таким образом, что бы правильно считывать подпись функции
 * _t($message, $category = 'cms', $params = array(), $source = 'nespiMessages', $language = null)
 * комманда смотрит только на первых два аргумента, которые в нашом случае были поменяны местами
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @package cms.commands
 */

/**
 * MessageCommand extracts messages to be translated from source files.
 * The extracted messages are saved as PHP message source files
 * under the specified directory.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 */

Yii::import('system.cli.commands.MessageCommand');
class NespiMessageCommand extends MessageCommand
{
    protected function extractMessages($fileName, $translator)
    {
        echo "Extracting messages from $fileName...\n";
        $subject = file_get_contents($fileName);
        $messages = array();
        if (!is_array($translator)) {
            $translator = array(
                $translator,
            );
        }

        foreach ($translator as $currentTranslator) {
            // S: Поменял местами (\'[\w.\/]*?(?<!\.)\'|"[\w.]*?(?<!\.)") и (\'.*?(?<!\\\\)\'|".*?(?<!\\\\)")
            // Так же вторая половина выражения теперь у нас не обязательная, так как по дефолту категория cms
            $n = preg_match_all('/\b' . $currentTranslator . '\s*\(\s*(\'.*?(?<!\\\\)\'|".*?(?<!\\\\)")\s*(,\s*(\'[\w.\/]*?(?<!\.)\'|"[\w.]*?(?<!\.)"))?\s*[,\)]/s', $subject, $matches, PREG_SET_ORDER);

            for ($i = 0; $i < $n; ++$i) {
                // S: поменял местами индексы, так как сообщение теперь - 1, а категория 2
                $message = $matches[$i][1];
                $category = 'cms';

                // S: категория теперь находится под индексом 3, так как индекс 2 скушала скобка (...)?
                if (isset($matches[$i][3])) {
                    if (($pos = strpos($matches[$i][3], '.')) !== false) {
                        $category = substr($matches[$i][3], $pos + 1, -1);
                    } else {
                        $category = substr($matches[$i][3], 1, -1);
                    }
                }

                $messages[$category][] = eval("return $message;"); // use eval to eliminate quote escape
            }
        }
        return $messages;
    }
}
