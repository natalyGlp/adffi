<?php
/**
 * Комманда для генерации карты сайта (sitemap.xml)
 *
 * yiic sitemap --host=[host] --contentRegion=[123]
 *
 * Внимание, если хост не задан, скрипт попытается взять хост из $_SERVER['HTTP_HOST']
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.commands
 */

/**
 * @see https://support.google.com/webmasters/answer/183668
 * TODO: Исходя из инструкции можно генерировать сайтмапы с картинками, новостями, видео.
 * TODO: надо проверять размер файла на выходе. Гугла рекоммендует не больше 50метров
 */
Yii::import('cms.models.object.*');
Yii::import('cms.models.page.*');
Yii::import('cms.components.*');
class SitemapCommand extends CConsoleCommand
{
    /**
     * @var string $host хост сайта, используется для построения url
     */
    public $host = false;

    /**
     * @var integer $contentRegion id региона в котором находится контент.
     * Если задан, то скрипт попытается добавить в сайтмап страницы со списком материалов
     */
    public $contentRegion = false;

    public function actionIndex()
    {
        Yii::log('Starting sitemap generation', 'info', get_class($this));

        $this->host = $this->host ? $this->host : $_SERVER['HTTP_HOST'];
        $this->host = 'http://' . trim($this->host, '/');

        $ignore = array('home');

        $this->beginSiteMap();

        $this->addUrl(array(
            'loc' => '/',
            'priority' => '1.0',
            'changefreq' => 'hourly',
        ));

        if ($this->contentRegion) {
            $listViews = CHtml::listData(Block::model()->findAllByAttributes(array(
                'type' => 'listview',
            )), 'block_id', 'block_id');
            $listViews = array_values($listViews);

            $detailViews = CHtml::listData(Block::model()->findAllByAttributes(array(
                'type' => 'content_detail_view',
            )), 'block_id', 'block_id');
            $detailViews = array_values($detailViews);

            // Добавим страницы со списком материалов
            $pageBlocks = PageBlock::model()->findAllByAttributes(array(
                'status' => 1,
                'block_id' => $listViews,
                'region' => $this->contentRegion,
            ), array('group' => 'page_id'));

            foreach ($pageBlocks as $pageBlock) {
                $page = $pageBlock->page;
                if ($page->status != 1 || $page->allow_index != 1 || $page->allow_follow != 1 || in_array($page->slug, $ignore)) {
                    continue;
                }

                $this->addUrl(array(
                    'loc' => $page->slug,
                    'priority' => '0.9',
                    'changefreq' => 'hourly',
                ));
            }
        }

        // добавим остальные страницы, на которых нету ни списка материалов, ни вида материалов
        $criteria = new CDbCriteria();
        $criteria->addInCondition('block_id', CMap::mergeArray($listViews, $detailViews));
        $criteria->group = 'page_id';
        $forbiddenPages = CHtml::listData(PageBlock::model()->findAllByAttributes(array(
            'status' => 1,
            'region' => $this->contentRegion,
        ), $criteria), 'page_id', 'page_id');
        $forbiddenPages = array_values($forbiddenPages);

        $criteria = new CDbCriteria();
        $criteria->addNotInCondition('page_id', $forbiddenPages);
        $pages = Page::model()->findAllByAttributes(array(
            'status' => 1,
            'allow_index' => 1,
            'allow_follow' => 1,
        ), $criteria);

        foreach ($pages as $page) {
            $this->addUrl(array(
                'loc' => $page->slug,
                'changefreq' => 'weekly',
                // 'lastmod' => date('Y-m-d', А НЕТ У НАС ТАКОГО ПОЛЯ...),
            ));
        }

        // добавим сами обьекты
        $objects = Object::model()->findAll(array(
            'condition' => 'object_status = ' . Object::STATUS_PUBLISHED,
        ));
        foreach ($objects as $object) {
            // лучше использовать методы из модели обьекта, но они на данном этапе "не очень" и их нужно рефактроить, на что нету времени
            $url = $object->object_type . '/' . $object->object_slug . '.html';
            $date = empty($object->object_modified_gmt) ? $object->object_modified_gmt : $object->object_modified;
            if (empty($date)) {
                $date = empty($object->object_date_gmt) ? $object->object_date_gmt : $object->object_date;
            }

            $this->addUrl(array(
                'loc' => $url,
                'changefreq' => 'weekly',
                'lastmod' => date('Y-m-d\TH:i+00:00', $date),
            ));
        }

        $siteMap = $this->endSiteMap();

        $filepath = CMS_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'sitemap.xml';
        file_put_contents($filepath, $siteMap);

        Yii::log('The sitemap was successfully created', 'info', get_class($this));

        if (($size = @filesize($filepath) * pow(10, 6)) > 50 * 0.7) {
            Yii::log('The sitemap file size is reached ' . $size . 'MB size. This is near to the maximum of 50MB that is recomended by google.', 'warning', get_class($this));
        }
        return 0;
    }

    public function beginSiteMap()
    {
        ob_start();
        echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
        echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL;
    }

    public function endSiteMap()
    {
        echo '</urlset>';
        return ob_get_clean();
    }

    public function addUrl($params = array())
    {
        $params['loc'] = $this->host . '/' . trim($params['loc'], '/');
        echo "\t" . CHtml::openTag('url') . PHP_EOL;
        foreach ($params as $tag => $value) {
            echo "\t\t" . CHtml::tag($tag, array(), $value) . PHP_EOL;
        }
        echo "\t" . CHtml::closeTag('url') . PHP_EOL;
    }
}
