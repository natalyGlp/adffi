<?php
/**
 * Комманда для обновления и генерации индекса Lucene
 *
 * Обновить/перестроить индекс (после выполнения автоматически производится отимизация):
 *     > yiic luceneIndex update --id=<indexId> [--rebuild]
 *     шорткат:
 *     > yiic luceneIndex --id=<indexId> [--rebuild]
 * Запустить оптимизацию индекса:
 *     > yiic luceneIndex optimize --id=<indexId>
 *
 * Что бы запустить обработку всех индексов, используйте --all вместо --id
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.commands
 */
Yii::import('cms.models.search.*');
Yii::import('cms.models.object.*');
class LuceneIndexCommand extends CConsoleCommand
{
    /**
     * @var integer $id id индекса Lucene
     */
    public $id = false;

    /**
     * @var integer $rebuild перестроить индекс
     */
    public $rebuild = false;

    /**
     * @var boolean $all если true, то комманда выполнится на все индексы
     */
    public $all = false;

    protected $_models;

    public function actionIndex()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $luceneIndexes = $this->getIndexModels();

        foreach ($luceneIndexes as $lucene) {
            echo ($this->rebuild ? 'Rebuilding' : 'Updating') . " `{$lucene->index_id}` index\n";

            if ($this->updateIndex($lucene) == 1) {
                return 1;
            }
        }

        $this->chmodIndexes();

        echo 'Done!' . "\n";
        return 0;
    }

    public function actionOptimize()
    {
        $luceneIndexes = $this->getIndexModels();

        foreach ($luceneIndexes as $lucene) {
            echo "Optimizing `{$lucene->index_id}` index\n";

            $process = new LuceneProcess($lucene->primaryKey);

            if ($process->isRunning()) {
                echo 'The process is already running' . "\n";
                return 1;
            }

            $process->setProgress(0);

            $lucene->optimize();

            $process->setProgress(100);
            $process->end();
            Yii::log("Index `{$lucene->index_id}` was successfully optimized", CLogger::LEVEL_INFO, get_class($this));
        }

        $this->chmodIndexes();

        echo 'Done!' . "\n";
        return 0;
    }

    /**
     * Метод для проверки статуса работы комманды другими классами извне
     * @return array массив содержащий все процессы на выполнении и их прогресс
     */
    public static function getProcessSummary()
    {
        return LuceneProcess::getProcessSummary();
    }

    protected function updateIndex(SearchLucene $lucene)
    {
        $process = new LuceneProcess($lucene->primaryKey);

        if ($process->isRunning()) {
            echo 'The process is already running' . "\n";
            return 1;
        }

        $initialCriteria = $lucene->getObjectsCriteria(array(
            'latest' => $this->rebuild,
        ));
        $count = Object::model()->count($initialCriteria);

        $pages = new CPagination($count);
        $pages->pageSize = 50;

        $process->setProgress(0);
        if ($this->rebuild) {
            // очищаем текущий индекс
            $lucene->createIndex();
        }

        for ($i = 0; $i < $pages->pageCount; $i++) {
            $pages->currentPage = $i;
            $criteria = new CDbCriteria();
            $criteria->mergeWith($initialCriteria);
            $pages->applyLimit($criteria);

            $lucene->updateIndex($criteria, !$this->rebuild);

            // файл прогресса был удален по средине работы скрипта
            // значит кто-то хочет, что бы скрипт был остановлен
            if (!$process->isRunning()) {
                echo 'Some one has deleted lock file' . "\n";
                return 1;
            }

            $progress = ($i + 1) / $pages->pageCount * 100;
            $process->setProgress($progress);

            echo 'Current progress: ' . $progress . '% ('.($i+1).'/'.$pages->pageCount.')' . "\n";
        }
        Yii::log("Index `{$lucene->index_id}` was successfully updated", CLogger::LEVEL_INFO, get_class($this));
        $process->end();

        $this->actionOptimize();

        return 0;
    }

    protected function chmodIndexes()
    {
        $path = str_replace('backend', 'frontend', Yii::getPathOfAlias('application.runtime.search'));
        $this->recursiveChmod($path, 0666, 0777);
    }

    protected function recursiveChmod($path, $filePerm = 0644, $dirPerm = 0755)
    {
        // Check if the path exists
        if (!file_exists($path)) {
            return (false);
        }

        // See whether this is a file
        if (is_file($path)) {
            // Chmod the file with our given filepermissions
            @chmod($path, $filePerm);

            // If this is a directory...
        } elseif (is_dir($path)) {
            // Then get an array of the contents
            $entries = scandir($path);

            // Remove "." and ".." from the list
            $ignore = array('..', '.');

            // Parse every result...
            foreach ($entries as $entry) {
                // And call this function again recursively, with the same permissions
                if (in_array($entry, $ignore)) {
                    continue;
                }

                $this->recursiveChmod($path . "/" . $entry, $filePerm, $dirPerm);
            }
            // When we are done with the contents of the directory, we chmod the directory itself
            @chmod($path, $dirPerm);
        }

        // Everything seemed to work out well, return true
        return (true);
    }

    protected function getIndexModels()
    {
        if (!$this->_models) {
            $this->_models = $this->all ? SearchLucene::model()->findAll() : SearchLucene::model()->findAllByPk(array($this->id));
        }

        return $this->_models;
    }
}

/**
 * Класс для управления lock файлами, что бы избегать одновременной работы с одним и тем же индексом
 */
class LuceneProcess
{
    protected $_id;
    const LOCK_FILE_PREFIX = 'luceneIndexUpdateProgress_';

    public function __construct($id)
    {
        $this->_id = $id;
    }

    public function getProgress()
    {
        return $this->isRunning() ? file_get_contents($this->getLockFilePath()) : null;
    }

    public function setProgress($progress)
    {
        return file_put_contents($this->getLockFilePath(), (string) $progress);
    }

    public function isRunning()
    {
        return file_exists($this->getLockFilePath());
    }

    public function end()
    {
        return unlink($this->getLockFilePath());
    }

    protected function getLockFilePath()
    {
        return Yii::getPathOfAlias('application.runtime.'.self::LOCK_FILE_PREFIX . $this->_id);
    }

    public static function getProcessSummary()
    {
        $pattern = Yii::getPathOfAlias('application.runtime.'.self::LOCK_FILE_PREFIX);
        $filesInProgress = glob($pattern . '*');

        $status = array();
        foreach ($filesInProgress as $file) {
            $progress = file_get_contents($file);
            $id = substr($file, strrpos($file, '_') + 1);
            $status[$id] = array(
                'progress' => $progress,
                'id' => $id,
            );
        }

        return $status;
    }
}
