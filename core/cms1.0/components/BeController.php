<?php

/**
 * Class of parent Controller for Backend of GXC CMS, extends from RController
 *
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.components
 */

class BeController extends RController
{
    public $layout = 'cms.views.layouts.column2';
    public $pageHint;
    public $titleImageUrl;

    protected $_page;

    /**
     * Filter by using Modules Rights
     *
     * @return type
     */
    public function filters()
    {
        return array(
            'rights',
        );
    }

    /**
     * List of allowd default Actions for the user
     * @return type
     */
    public function allowedActions()
    {
        return 'login,logout';
    }

    protected function beforeAction($action)
    {
        if (isset($_GET['embed']) || isset($_GET['iframe'])) {
            $this->layout = 'cms.views.layouts.clean';
        }

        // TODO: не уверен, что модель Page нужна на каждой странице
        if (Yii::app()->hasComponent(CONNECTION_NAME)) {
            if ($this->site) {
                $site = Sites::model()->find('url LIKE ?', array('%' . $this->site . '%'));
                $this->_page = (object) array(
                    'layout' => isset($site->theme) ? $site->theme : 'default'
                );
            } else {
                $this->_page = (object) array(
                    'layout' => Yii::app()->{CONNECTION_NAME}->createCommand()->select('layout')->from('{{page}}')->queryScalar()
                );
            }

            // устанавливаем алиас для текущей темы
            Yii::setPathOfAlias('theme', Yii::getPathOfAlias('common.front_layouts.' . Yii::app()->controller->page->layout));
            Yii::setPathOfAlias('currentTheme', Yii::getPathOfAlias('common.front_layouts.' . Yii::app()->controller->page->layout));// DEPRECATED
        }

        return parent::beforeAction($action);
    }

    public function renderPartial($view, $data = null, $return = false, $processOutput = false)
    {
        $view = strstr($view, '.') ? $view : 'cms.views.' . strtolower(str_replace('Controller', '', get_class($this))) . '.' . $view;
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($processOutput) {
                $output = $this->processOutput($output);
            }

            if ($return) {
                return $output;
            } else {

                echo $output;
            }
        } else {
            throw new CException(Yii::t('yii', '{controller} cannot find the requested view "{view}".', array(
                '{controller}' => get_class($this), '{view}' => $view
                )));
        }
    }

    public function getSite()
    {
        return isset($_GET['site']) && !empty($_GET['site']) ? $_GET['site'] : false;
    }

    public function getPage()
    {
        return $this->_page;
    }
}
