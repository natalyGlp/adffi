<?php
class GxcUrlManager extends CBaseUrlRule
{
    public $connectionID = 'db';

    public function createUrl($manager, $route, $params, $ampersand)
    {
        $path = '';

        if (Yii::app()->language != Yii::app()->translate->defaultLanguage) {
            $path .= '/' . Yii::app()->translate->languageUrl;
        }

        // виртуальный роут slug
        // FIXME: возможно костыльное решение
        $routeParts = explode('/', $route);
        if (strpos($route, 'slug/') !== false) {
            $path .= '/' . str_replace('slug/', '', $route);
        } elseif ($this->isControllerExists(reset($routeParts))) {
            $path = $path . '/' . $route;
        } else {
            // Object
            if (isset($params['type']) && isset($params['slug'])) {
                $path .= '/' . $params['type'];
                unset($params['type']);

                $params['slug'] .= '.html';
            }

            // Page and Object
            if (isset($params['slug'])) {
                $path .= '/' . $params['slug'];
                unset($params['slug']);
            }
        }

        /**
         * Добавление параметров в строку пути (пагинация, ajax, дополнительные параметры)
         */
        $url = substr($path, 1);
        if (!empty($params)) {
            $url .= '?';
            foreach ($params as $k => $v) {
                $url .= '&' . http_build_query($params);
            }
        }

        return ltrim($url, '/');
    }

    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        // TODO: убрать это отсюда
        if ($pathInfo == "site/captcha") {
            return 'site/captcha';
        }

        $parts = explode('/', $pathInfo);

        if ($this->isControllerExists($parts[0])) {
            return false;
        }

        return 'site/index';
    }

    public function isControllerExists($id)
    {
        $id = ucfirst($id);
        $controllerPath = Yii::app()->controllerPath;
        return file_exists($controllerPath . DIRECTORY_SEPARATOR . $id . "Controller.php");
    }
}
