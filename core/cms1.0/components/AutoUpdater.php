<?php
/**
 * Базовый класс для создания автоапдейтеров cms
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.controllers
 */

class AutoUpdater extends CApplicationComponent
{
    /**
     * @var array $migrations массив с алиасми к папкам в которых надо запустить миграции
     */
    protected $migrations;

    /**
     * @var string $destination алиас директории в которую надо распокавать обновление
     */
    protected $destination;

    public function init()
    {
        Yii::setPathOfAlias('project', dirname(dirname(Yii::getPathOfAlias('common'))));
        parent::init();
    }

    /**
     * Возвращает массив с путями к директориям с миграциями, которые должны быть запущены
     * Для того, что бы сослаться на директорию с файлами обновления необходимо использовать алиас 'update'
     *
     * @return array
     */
    public function getMigrations()
    {
        return $this->migrations ? $this->migrations : array();
    }

    public function getDestination()
    {
        return Yii::getPathOfAlias($this->destination ? $this->destination : 'cms');
    }

    /**
     * Вызывается перед автообновлением.
     *
     * @return boolean если true, обновление продолжится - иначе прервется
     */
    public function beforeUpdate()
    {
        return true;
    }

    /**
     * Вызывается перед тем как переместить файлы в директорию назначения
     */
    public function beforeMove()
    {
        $this->processDeleted();

        return true;
    }

    /**
     * Вызывается после автообновления.
     */
    public function afterUpdate()
    {
    }

    /**
     * Считывает файлы, которые необходимо удалить из файла .update/deletedFiles и безопасно удаляет их
     */
    protected function processDeleted()
    {
        $path = Yii::getPathOfAlias('update') . '/.update/deletedFiles';
        if (!file_exists($path)) {
            return;
        }

        $filesToDelete = file($path);
        $this->safeRm($filesToDelete, $this->getDestination());
    }

    /**
     * Безопасно удаляет файлы по путям в массиве
     *
     * @param array|string $files массив с файлами или путь к файлу, который нужно удалить
     */
    protected function safeRm($files = array(), $root = false)
    {
        if (!is_array($files)) {
            $files = array($files);
        }

        foreach ($files as $file) {
            $file = trim($file);
            if ($root) {
                $file = $root . '/' . $file;
            }

            if (!is_writable($file)) {
                continue;
            }

            $this->unlinkR($file);
        }
    }

    /**
     * Recursively delete a directory
     *
     * @param string $dir Directory name
     * @param boolean $deleteRootToo Delete specified top-level directory as well
     */
    protected function unlinkR($dir, $deleteRootToo = true)
    {
        if (is_file($dir)) {
            @unlink($dir);
            return;
        }

        if (!$dh = @opendir($dir)) {
            return;
        }
        while (false !== ($obj = readdir($dh))) {
            if ($obj == '.' || $obj == '..') {
                continue;
            }

            if (!@unlink($dir . '/' . $obj)) {
                $this->unlinkR($dir . '/' . $obj, true);
            }
        }

        closedir($dh);

        if ($deleteRootToo) {
            @rmdir($dir);
        }

        return;
    }

    /**
     * @return array со списком путей, по которым находятся файлы не доступные для записи
     */
    protected function findUnremovable($path)
    {
        if (!file_exists($path)) {
            return array();
        }

        if (!is_writable($path)) {
            return array($path);
        }

        if (is_file($path)) {
            return array();
        }

        $unRemovable = array();

        $dirs = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($dirs, RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($iterator as $item) {
            if (!$item->isWritable()) {
                $unRemovable[] = $item->getPathname();
            }
        }

        return $unRemovable;
    }
}
