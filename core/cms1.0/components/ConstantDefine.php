<?php

/**
 * Class defined all the Constant value of the CMS.
 *
 *
 * @author Tuan Nguyen
 * @version 1.0
 * @package common.components
 */

class ConstantDefine
{
    const AMAZON_SES_ACCESS_KEY = '';
    const AMAZON_SES_SECRET_KEY = '';

    const AMAZON_SES_EMAIL = '';

    /**
     * Constant related to Upload File Size
     */
    const UPLOAD_MAX_SIZE = 31457280;//30mb
    const UPLOAD_MIN_SIZE = 1;//1 byte

    public static function fileTypes()
    {
        return array(
            'image' => array('jpg', 'gif', 'png', 'bmp', 'jpeg'),
            'audio' => array('mp3', 'wma', 'wav'),
            'video' => array('flv', 'wmv', 'avi', 'mp4', 'mov', '3gp'),
            'flash' => array('swf'),
            'file' => array('*'),
        );
    }

    public static function chooseFileTypes()
    {
        return array(
            'auto' => _t('Auto detect'),
            'image' => _t('Image'),
            'video' => _t('Video'),
            'audio' => _t('Audio'),
            'file' => _t('File'),
        );
    }

    /**
     * Constant related to User
     */
    const USER_ERROR_NOT_ACTIVE = 3;
    const USER_STATUS_DISABLED = 0;
    const USER_STATUS_ACTIVE = 1;

    public static function getUserStatus()
    {
        return array(
            self::USER_STATUS_DISABLED => _t("Disabled"),
            self::USER_STATUS_ACTIVE => _t("Active"));
    }

    const USER_GROUP_ADMIN = 'Admin';
    const USER_GROUP_OWNER = 'Owner';
    const USER_GROUP_EDITOR = 'Editor';
    const USER_GROUP_REPORTER = 'Reporter';

    /**
     * Constant related to Object
     *
     */

    // TODO: пусть по всей цмс будут вызовы к этим константам через Object или его потомкам
    const OBJECT_STATUS_PUBLISHED = 1;
    const OBJECT_STATUS_DRAFT = 2;
    const OBJECT_STATUS_PENDING = 3;
    const OBJECT_STATUS_HIDDEN = 4;

    const OBJECT_ALLOW_COMMENT = 1;
    const OBJECT_DISABLE_COMMENT = 2;

    public static function getObjectCommentStatus()
    {
        return array(
            self::OBJECT_ALLOW_COMMENT => _t("Allow"),
            self::OBJECT_DISABLE_COMMENT => _t("Disable"),
        );
    }

    /**
     * Constant related to Transfer
     *
     */
    const TRANS_ROLE = 1;
    const TRANS_PERSON = 2;
    const TRANS_STATUS = 3;

    /**
     * Constant related to Menu
     * DEPRECATED
     */
    const MENU_TYPE_PAGE = 1;
    const MENU_TYPE_TERM = 2;
    const MENU_TYPE_CONTENT = 5;
    const MENU_TYPE_URL = 3;
    const MENU_TYPE_STRING = 4;

    public static function getMenuType()
    {
        // DEPRECATED
        return array(
            self::MENU_TYPE_URL => _t("Link to URL"),
            self::MENU_TYPE_PAGE => _t("Link to Page"),
            self::MENU_TYPE_CONTENT => _t("Link to a Content Object"),
            self::MENU_TYPE_TERM => _t("Link to a Term Page"),
            self::MENU_TYPE_STRING => _t("Label (No URL)"),
        );
    }

    /**
     * Constant related to Menu aligns
     * DEPRECATED
     */
    const MENU_ALIGN_TYPE_TOP = 1;
    const MENU_ALIGN_TYPE_BOTTOM = 2;
    const MENU_ALIGN_TYPE_LEFT = 3;
    const MENU_ALIGN_TYPE_RIGHT = 4;
    const MENU_ALIGN_TYPE_FLOAT = 5;

    public static function getMenuAlignType()
    {
        // DEPRECATED
        return array(
            self::MENU_ALIGN_TYPE_TOP => _t("Top menu"),
            self::MENU_ALIGN_TYPE_BOTTOM => _t("Bottom menu"),
            self::MENU_ALIGN_TYPE_LEFT => _t("Left menu"),
            self::MENU_ALIGN_TYPE_RIGHT => _t("Right menu"),
            self::MENU_ALIGN_TYPE_FLOAT => _t("No align"),
        );
    }

    /**
     * Constant related to Content List
     *
     */

    const CONTENT_LIST_RETURN_DATA_PROVIDER = 1;
    const CONTENT_LIST_RETURN_ACTIVE_RECORD = 2;

    /**
     * Constant related to Page
     *
     */
    const PAGE_ACTIVE = 1;
    const PAGE_DISABLE = 2;

    public static function getPageStatus()
    {
        return array(
            self::PAGE_ACTIVE => _t("Active"),
            self::PAGE_DISABLE => _t("Disable"),
        );
    }

    const PAGE_ALLOW_INDEX = 1;
    const PAGE_NOT_ALLOW_INDEX = 2;

    public static function getPageIndexStatus()
    {
        return array(
            self::PAGE_ALLOW_INDEX => _t("Allow index"),
            self::PAGE_NOT_ALLOW_INDEX => _t("Not allow Index"),
        );
    }

    const PAGE_ALLOW_FOLLOW = 1;
    const PAGE_NOT_ALLOW_FOLLOW = 2;

    public static function getPageFollowStatus()
    {
        return array(
            self::PAGE_ALLOW_FOLLOW => _t("Allow follow"),
            self::PAGE_NOT_ALLOW_FOLLOW => _t("Not allow follow"),
        );
    }

    const PAGE_BLOCK_ACTIVE = 1;
    const PAGE_BLOCK_DISABLE = 2;

    public static function getPageBlockStatus()
    {
        return array(
            self::PAGE_BLOCK_ACTIVE => _t("Active"),
            self::PAGE_BLOCK_DISABLE => _t("Disable"),
        );
    }

    /**
     * Constant related to Avatar Size
     */

    const AVATAR_SIZE_96 = 96;
    const AVATAR_SIZE_23 = 23;

    public static function getAvatarSizes()
    {
        return array(
            self::AVATAR_SIZE_23 => "23",
            self::AVATAR_SIZE_96 => "96",
        );
    }

    /* Constant related to Comment
     *
     */

    const COMMENT_STATUS_PUBLISHED = 1;
    const COMMENT_STATUS_PENDING = 2;
    const COMMENT_STATUS_DISCARDED = 3;

    public static function getCommentStatus()
    {
        return array(
            self::COMMENT_STATUS_PUBLISHED => _t("Published"),
            self::COMMENT_STATUS_PENDING => _t("Pending"),
            self::COMMENT_STATUS_DISCARDED => _t("Discarded")
        );
    }
}
