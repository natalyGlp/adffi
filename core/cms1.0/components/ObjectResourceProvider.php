<?php
/**
 * Возвращает информацию о ресурсах
 * Проводит конвертацию ресурсов под заданные в thumbOptions размеры
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 */

class ObjectResourceProvider extends CComponent
{
    public $resourceType;
    public $objectId;

    protected $_thumbOptions;

    public function __construct($objectId = false, $resourceType = false)
    {
        if ($objectId) {
            $this->objectId = $objectId;
        }

        if ($resourceType) {
            $this->resourceType = $resourceType;
        }
    }

    /**
     * Устанавливает настройки для ресайзинга изображений
     * @param array|string $thumbOptionsId массив с настройками или id настроек размеров из админки
     */
    public function setThumbOptions($thumbOptionsId)
    {
        // TODO: новые имена для элементов массива (добавить remap options в GxcHelpers)
        $thumbOptions = is_array($thumbOptionsId) ? $thumbOptionsId : GxcHelpers::getResourcesSizes($thumbOptionsId);

        if (empty($thumbOptions)) {
            throw new CException('No sizing options for '.$thumbOptionsId);
        }

        if (!isset($thumbOptions['version'])) {
            throw new CException('Resource should have naming prefix');
        }

        $thumbOptions['max_width'] = !empty($thumbOptions['max_width']) ? $thumbOptions['max_width'] : null;
        $thumbOptions['max_height'] = !empty($thumbOptions['max_height']) ? $thumbOptions['max_height'] : null;
        if (!$thumbOptions['max_width'] && !$thumbOptions['max_height']) {
            throw new CException('Can\'t find sizing options for '.$size);
        }

        $this->_thumbOptions = $thumbOptions;
    }

    public function getThumbOptions()
    {
        return $this->_thumbOptions;
    }

    public function getHasThumb()
    {
        return !!$this->_thumbOptions;
    }

    public function getAsArray()
    {
        $objectResources = $this->getObjectResources();

        if (empty($objectResources)) {
            return array();
        }

        $availableResources = array();
        foreach ($objectResources as $objectResource) {
            $resourceModel = $objectResource->resource;
            if (!$resourceModel) {
                continue;
            }

            $resourcePath = $resourceModel->getFullPath();
            $resourceUrl = $resourceModel->getUrl();

            if ($this->hasThumb) {
                list($resourceUrl, $resourcePath) = $this->createThumb($resourceModel);
            }

            // DEPRECATED: Временное изменение, что бы игнорировать поле имени ресурса в том случае, если оно содержит имя файла
            $extensions = array('jpg', 'gif', 'png');
            $resourceName = !in_array(strtolower(pathinfo($resourceModel->resource_name, PATHINFO_EXTENSION)), $extensions) ? $resourceModel->resource_name : null;

            $resourceData = array(
                'link' => $resourceUrl,
                'local' => $resourcePath,
                'name' => $resourceName,
                'origLink' => $resourceModel->getUrl(),
                'resid' => $resourceModel->resource_id,
                'type' => $resourceModel->resource_type,
                'body' => $resourceModel->resource_body,
                );

            if ($resourceData['type'] == 'videoembed') {
                $resourceData = $this->prepareVideoEmbed($resourceData);
            }

            array_push($availableResources, $resourceData);
        }

        return $availableResources;
    }

    protected function getObjectResources()
    {
        return ObjectResource::model()->with('resource')->findAll(array(
            'condition' => 'object_id = :obj and type = :type',
            'order' => 'resource_order ASC',
            'params' => array(':obj' => $this->objectId, ':type' => $this->resourceType)
        ));
    }

    public static function getThumbUrl($resourceId, $thumbOptions)
    {
        $provider = new self();

        if ($resourceId instanceof Resource) {
            $resource = $resourceId;
            $resourceId = $resource->primaryKey;
        } else {
            $resource = Resource::model()->findByPk($resourceId);

            if (!$resource) {
                return null;
            }
        }

        $provider->setThumbOptions($thumbOptions);

        list($thumbUrl, $thumbPath) = $provider->createThumb($resource);

        return $thumbUrl;
    }

    public function createThumb($resourceModel)
    {
        $thumbPath = dirname($resourceModel->resource_path) . '/resized/'.$this->_thumbOptions['version'] . basename($resourceModel->resource_path);

        // TODO: этот класс не должен знать где хранятся ресурсы и как на них ссылаться
        $thumbUrl = RESOURCE_URL.'/'.$thumbPath;
        $thumbPath = Yii::getPathOfAlias('uploads.resources').'/'.$thumbPath;

        $this->ensureThumbDir($thumbPath);

        $canCreateThumb = !file_exists($thumbPath) && file_exists($resourceModel->getFullPath());
        if ($canCreateThumb) {
            $this->resizeResource($resourceModel->getFullPath(), $thumbPath);
        }

        return array($thumbUrl, $thumbPath);
    }

    protected function ensureThumbDir($thumbPath)
    {
        $resizedDirPath = dirname($thumbPath);
        if (!is_dir($resizedDirPath)) {
            mkdir($resizedDirPath, 0777, true);
            @chmod($resizedDirPath, 0777);
        }
    }

    protected function resizeResource($source, $destination)
    {
        Yii::import('cms.extensions.image.Image');

        $image = Yii::app()->image->load($source);
        $image->resize(
            $this->thumbOptions['max_width'],
            $this->thumbOptions['max_height'],
            $this->getConvertedFit(),
            $this->getConvertedScale()
        );

        $image->save($destination, 0777);
    }

    protected function getConvertedScale()
    {
        $scaleConvert = array(
            0 => Image::DOWN,
            1 => Image::ANY,
        );

        return $scaleConvert[isset($this->thumbOptions['scale_up'])];
    }

    protected function getConvertedFit()
    {
        $fitConvert = array(
            'inside' => Image::INSIDE,
            'outside' => Image::OUTSIDE,
            'fill' => Image::FILL,
        );

        return $fitConvert[(isset($this->thumbOptions['fit']) ? $this->thumbOptions['fit'] : 'inside')];
    }

    protected function prepareVideoEmbed($resourceData)
    {
        $sizeOptions = $this->_thumbOptions;
        if (!$sizeOptions) {
            $sizeOptions = array(
                'max_width' => 420,
                'max_height' => '',
                );
        }
        $videoWidth = $sizeOptions['max_width'];
        $videoHeight = !empty($sizeOptions['max_height']) ? $sizeOptions['max_height'] : $sizeOptions['max_width']/16*9;

        $resourceData['body'] = CJSON::decode($resourceData['body']);

        $resourceData['html'] = '';
        $resourceData['body']['playerId'] = $resourceData['body']['id'];
        $resourceData['body']['playerWidth'] = $videoWidth;
        $resourceData['body']['playerHeight'] = $videoHeight;

        $provider = isset($resourceData['body']['provider']) ? $resourceData['body']['provider'] : parse_url($resourceData['body']['url'], PHP_URL_HOST);
        $provider = str_replace('www.', '', $provider);
        switch ($provider) {
            case 'youtube.com':
                $resourceData['body']['playerUrl'] = '//www.youtube.com/embed/'.$resourceData['body']['id'].'?wmode=opaque';
                break;
            case 'vimeo.com':
                $resourceData['body']['playerUrl'] = '//player.vimeo.com/video/'.$resourceData['body']['id'];
                break;
        }
        $resourceData['html'] = '<iframe width="'.$videoWidth.'" height="'.$videoHeight.'" src="'.$resourceData['body']['playerUrl'].'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

        return $resourceData;
    }
}
