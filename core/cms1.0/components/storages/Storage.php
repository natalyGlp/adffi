<?php

interface Storage
{
    public function uploadFile($resource, $form);
    public function uploadRemoteFile($link, $resource, $form, $ext);
    public static function getFilePath($file);
    public function deleteResource($resource);
}
