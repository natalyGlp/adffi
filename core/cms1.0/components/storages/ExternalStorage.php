<?php

/**
 * Class for handle File from external
 *
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package common.storages
 */

//This is a must Have Resource Class - Don't Delete this
class ExternalStorage extends BaseStorage
{
    public function uploadFile($resource, $form)
    {
        $form->addError('upload', _t('External storage saves only external links!'));
        return false;
    }

    public function uploadRemoteFile($link, $resource, $form, $ext)
    {
        return true;
    }

    public function getFilePath($file)
    {
        return $file;
    }

    public function deleteResource($resource)
    {
        return;
    }
}
