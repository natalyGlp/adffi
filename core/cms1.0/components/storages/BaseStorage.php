<?php

abstract class BaseStorage implements Storage
{
    public $max_file_size;
    public $min_file_size;
    public $allow_types;

    public function __construct($max_file_size = ConstantDefine::UPLOAD_MAX_SIZE, $min_file_size = ConstantDefine::UPLOAD_MIN_SIZE, $allow_types = array())
    {
        $this->max_file_size = $max_file_size;
        $this->min_file_size = $min_file_size;
        $this->allow_types = $allow_types;
    }

    protected function getRemoteFileContents($link)
    {
        $curl = curl_init($link);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0');
        $rawdata = curl_exec($curl);
        curl_close($curl);

        return $rawdata;
    }
}
