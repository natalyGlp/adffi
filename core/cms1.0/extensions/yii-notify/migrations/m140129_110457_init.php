<?php

class m140129_110457_init extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('{{notify}}', array(
			'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'title' => 'TEXT',
			'message' => 'TEXT',
			'url' => 'TEXT',
			'recipient' => 'VARCHAR(255)',
			'category' => 'VARCHAR(32) NOT NULL',
			'placement' => 'VARCHAR(32)',
			'level' => 'VARCHAR(32)',
			'counter' => 'INT(11) UNSIGNED',
			'flash' => 'TINYINT(1) UNSIGNED',
			'enabled' => 'TINYINT(1) UNSIGNED',
			'date_created' => 'TIMESTAMP',
			'date_from' => 'TIMESTAMP NULL DEFAULT NULL',
			'date_to' => 'TIMESTAMP NULL DEFAULT NULL',
			),'engine InnoDB DEFAULT CHARSET=utf8');

		$this->createTable('{{notify_reads}}', array(
			'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
			'notify_id' => 'INT(11) UNSIGNED NOT NULL',
			'user_id' => 'INT(11) UNSIGNED NOT NULL',
			'readed' => 'TINYINT(1) UNSIGNED',
			'UNIQUE KEY `notify_and_user` (`notify_id`, `user_id`)',
			),'engine InnoDB DEFAULT CHARSET=utf8');

		$this->addForeignKey('{{notify_reads}}_fk', '{{notify_reads}}', 'notify_id', '{{notify}}', 'id', 'CASCADE', 'CASCADE');

		return true;
	}

	public function safeDown()
	{
		$this->dropForeignKey('{{notify_reads}}_fk', '{{notify_reads}}');
		$this->dropTable('{{notify}}');
		$this->dropTable('{{notify_reads}}');
		return true;
	}
}