<?php
/**
 * NotifyComponent - the component to manage notifications
 * 
 * @uses CApplicationComponent
 * @version 0.1
 * @copyright Copyright &copy; 2014 Sviatoslav Danylenko
 * @author Sviatoslav Danylenko <dev@udf.su> 
 * @license MIT ({@link http://opensource.org/licenses/MIT})
 * @link https://github.com/SleepWalker/yii-notify
 */

class NotifyComponent extends CApplicationComponent
{
	protected $_notifies;

	public function init()
	{
		Yii::setPathOfAlias('yii-notify', dirname(dirname(__FILE__)));
		Yii::import('yii-notify.models.*');
	}

	/**
	 * Creates notification
	 *
	 * @param string $category the category (id) of notification(s)
	 * @param array $params additional params such as
	 *		title - the title to be displayed
	 *		message - the message to be displayed
	 *		url - url of the notify
	 *		level - the level of the notify. Can be used for displaying different notify style on frontend
	 *		counter - e.g. the number of unread messages
	 *		placement - custom position in layout (e.g. sidebar, header, popup etc.)
	 *		from - the begining date of displating notify (if not specified - start displaying immediately)
	 *		to - the end date of displaying notify (if not specified (null), you should manualy clear this notify by its $category)
	 *		role - string|array. the role wich members should get notify (can not be used with `userId`)
	 *		userId - string|array. id of the user, that should get notify (can not be used with `role`)
	 *		flash - notify should be shown only once
	 *		updateByCategory
	 */
	public function create($category, $params = array())
	{
		if(isset($params['updateByCategory']))
			$model = Notify::model()->findByAttributes(array('category' => $category));

		if(!$model)
		{
			$model = new Notify();
			$model->category = $category;
		}

		$allowedAttributes = array('title', 'message', 'url', 'level', 'counter', 'placement', 'from', 'to', 'role', 'userId', 'flash');

		foreach ($params as $key => $value) 
		{
			if(!in_array($key, $allowedAttributes))
				continue;

			switch ($key) 
			{
				case 'role':
				case 'userId':
					if(is_array($value))
						$value = implode(',', $value);

					$key = 'recipient';
					break;
			}

			if($model->hasAttribute($key))
				$model->$key = $value;
		}

		if(!$model->save())
		{
			throw new CException("Error saving Notify model: \n".var_export($model->errors));
		}
	}

	/**
	 * Returns all notifications for current user and processes expiration
	 *
	 * @param $params array with additional fetch criteria, with following keys:
	 *		placement - fetch all notifies for the specified placement (e.g. sidebar, popup etc.)
	 *
	 * @return array of notify models
	 */
	public function fetch($params = array())
	{
		$notifies = $this->getNotifies($params);

		// process notifies availability
		foreach ($notifies as $notify) 
		{
			$this->processAvailablility($notify);
		}

		return $notifies;
	}

	public function count($params)
	{
		return count($this->getNotifies($params));
	}

	/**
	 * @return all notifications for current user
	 */
	protected function getNotifies($filter)
	{
		$key = serialize($filter);
		if(!isset($this->_notifies[$key]))
		{
			$orCriteria = new CDbCriteria();
			$orCriteria->addCondition('recipient IS NULL');
			$orCriteria->addCondition(':uid IN (recipient)', 'OR');
			$params = array(':uid' => Yii::app()->user->id);
			$userRoles = Yii::app()->authManager->getAuthItems(2, Yii::app()->user->id);
			$userRoles = array_keys($userRoles);
			if(count($userRoles))
				foreach ($userRoles as $i => $role) 
				{
					$orCriteria->addCondition(':role'.$i.' IN(recipient)', 'OR');
					$params[':role'.$i] = $role;
				}
			$orCriteria->params = array_merge($orCriteria->params, $params);

			$criteria = new CDbCriteria();
			if(isset($filter['placement']))
				$criteria->compare('placement', $filter['placement']);
			if(isset($filter['category']))
				$criteria->compare('category', $filter['category']);

			$criteria->mergeWith($orCriteria);
			$criteria->with = array('reads' => array('joinType' => 'LEFT JOIN'));
			$criteria->compare('COALESCE(reads.readed, 0)', 0);
			$criteria->compare('enabled', 1);
			$criteria->addCondition('(NOW() BETWEEN COALESCE(date_from, 0) AND COALESCE(date_to, NOW()+1))', 'AND');

			$this->_notifies[$key] = Notify::model()->findAll($criteria);
		}

		return $this->_notifies[$key];
	}

	/**
	 * Shortcut to delete all Notifies by their attributes.
	 *
	 * @param string $category the category (id) of notification(s)
	 * @param array $params additional params see NotifyComponent::create()
	 */
	public function delete($category, $params = array(), $criteria = null)
	{
		$attributes = $params;
		$attributes['category'] = $category;
		return Notify::model()->deleteAllByAttributes($attributes, $criteria);
	}

	/**
	 * Disables notifications group by their category
	 *
	 * @param string $category the category (id) of notification(s)
	 */
	public function disable($category)
	{
		$this->switchEnabled(0, $category);
	}

	/**
	 * Enables notifications group by their category
	 *
	 * @param string $category the category (id) of notification(s)
	 */
	public function enable($category)
	{
		$this->switchEnabled(1, $category);
	}

	protected function switchEnabled($enabled, $category)
	{
		$attributes['category'] = $category;
		$models = Notify::model()->findAllByAttributes($attributes);

		foreach ($models as $model) 
		{
			$model->enabled = $enabled;
			$model->save();
		}
	}

	/**
	 * Checks notify params and decides whether it should be readed or deleted
	 */
	protected function processAvailablility($notify)
	{
		// TODO
	}
}