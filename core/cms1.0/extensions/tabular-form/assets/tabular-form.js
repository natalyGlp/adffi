(function($) {
    'use strict';

    /**
     * Плагин ожидает внутри элемента найти контейнер с классом
     * .js-tabular-form в котром каждый ребенок - строка табличной формы
     * .js-tabular-form-add - кнопка, которая добавляет новую строку в форму
     * .js-tabular-form-remove - кнопка, которая удаляет строку формы в которой она находится
     */

    $.fn.tabularForm = function(options) {
        var form = new TabularForm(this, options);
        form.render();
    };

    function TabularForm($el, options) {
        this.$el = $el;
        this.options = options;
    }

    TabularForm.prototype = $.extend(TabularForm.prototype, {
        render: function() {
            this.$form = this.$el.find('.js-tabular-form');
            this.$add = this.$el.find('.js-tabular-form-add');

            this.addFormLine();

            this.setEvents();

            return this;
        },

        getRemoveButton: function() {
            return $('<a>')
                .addClass('btn btn-success')
                .attr('href', '#')
                .text(this.options.addButtonLabel);
        },

        addFormLine: function() {
            var html = this.options.template;

            html = html.replace(/\$index/g, this.$form.children().length);

            this.$form.append(html);
        },

        removeFormLine: function($elementInRow) {
            var $row = $elementInRow.parent();
            while ($row.parent()[0] != this.$form[0]) {
                $row = $row.parent();
            }

            $row.hide(400, function () {$row.remove();});
        },

        setEvents: function() {
            this.$el
                .on('click', '.js-tabular-form-add', $.proxy(function (event) {
                    event.preventDefault();

                    this.addFormLine();
                }, this))
                .on('click', '.js-tabular-form-remove', $.proxy(function (event) {
                    event.preventDefault();

                    this.removeFormLine($(event.target));
                }, this))
                ;
        }
    });
}(jQuery));
