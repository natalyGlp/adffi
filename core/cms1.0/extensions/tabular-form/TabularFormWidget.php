<?php

class TabularFormWidget extends CWidget
{
    public $content;
    /**
     * @var string html макет строки, которая должна добавляться в форму
     */
    public $template;
    public $addButtonLabel;
    public $htmlOptions;

    public function init()
    {
        if (empty($this->content)) {
            ob_start();
        }

        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = $this->id;
        }
    }

    public function run()
    {
        if (empty($this->content)) {
            $this->content = ob_get_clean();
        }

        $this->render('tabular-form', array(
            'id' => $this->id,
            'htmlOptions' => $this->htmlOptions,
            'content' => $this->content,
            'addButtonLabel' => $this->addButtonLabel,
        ));

        $this->registerClientScript();
    }

    protected function registerClientScript()
    {
        $options = array(
            'template' => $this->template,
        );
        $assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets', -1, false, YII_DEBUG);
        Yii::app()->clientScript
            ->registerScriptFile($assetsUrl.'/tabular-form.js')
            ->registerScript(__FILE__.'#'.$this->id, '
                $("#'.$this->id.'").tabularForm('.CJavaScript::encode($options).');
            ');
    }
}
