<?php

/**
 * CJuiSliderInput class file.
 *
 * @author Sebastian Thierer <sebathi@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008-2011 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
 
// Это копипаст класса из оригинального Yii CJuiSliderInput
// Отличие от оригинального класса в том, что он показывает скрытые поля в которых хранятся минимальное и максимальное значения слайдера
// Пришлось так сделать, так как используя наследование особо не сэкономишь... всеравно прийдеться переписывать весь метод run


Yii::import('zii.widgets.jui.CJuiInputWidget');

/**
 * CJuiSliderInput displays a slider. It can be used in forms and post its value.
 *
 * CJuiSlider encapsulates the {@link http://jqueryui.com/demos/slider/ JUI
 * slider} plugin.
 *
 * To use this widget, you may insert the following code in a view:
 * <pre>
 * $this->widget('zii.widgets.jui.CJuiSliderInput', array(
 *     'name'=>'rate',
 *     'value'=>37,
 *     // additional javascript options for the slider plugin
 *     'options'=>array(
 *         'min'=>10,
 *         'max'=>50,
 *     ),
 *     'htmlOptions'=>array(
 *         'style'=>'height:20px;'
 *     ),
 * ));
 * </pre>
 *
 * The widget can also be used in range mode which uses 2 sliders to set a range.
 * In this mode, {@link attribute} and {@link maxAttribute} will define the attribute
 * names for the minimum and maximum range values, respectively. For example:
 *
 * <pre>
 * $this->widget('zii.widgets.jui.CJuiSliderInput', array(
 *     'model'=>$model,
 *     'attribute'=>'timeMin',
 *     'maxAttribute'=>'timeMax,
 *     // additional javascript options for the slider plugin
 *     'options'=>array(
 *         'range'=>true,
 *         'min'=>0,
 *         'max'=>24,
 *     ),
 * ));
 *
 * If you need to use the slider event, please change the event value for 'stop' or 'change'.
 *
 * By configuring the {@link options} property, you may specify the options
 * that need to be passed to the JUI slider plugin. Please refer to
 * the {@link http://jqueryui.com/demos/slider/ JUI slider} documentation
 * for possible options (name-value pairs).
 *
 * @author Sebastian Thierer <sebathi@gmail.com>
 * @version $Id: CJuiSliderInput.php 2948 2011-02-09 13:27:05Z haertl.mike $
 * @package zii.widgets.jui
 * @since 1.1
 */
class HFilterRangeSlider extends CJuiInputWidget
{
	/**
	 * @var string the name of the container element that contains the slider. Defaults to 'div'.
	 */
	public $tagName = 'div';
	/**
	 * @var integer determines the value of the slider, if there's only one handle. If there is more than one handle, determines the value of the first handle.
	 */
	public $value;
	/**
	 * @var integer $value2 второе значение в диапазоне
	 */
	public $value2;

	/**
	 * @var string $valueLabel подпись для поля value, если слайдер используется не с моделью
	 */
	public $valueLabel;
	/**
	 * @var string $value2Label подпись для поля value2, если слайдер используется не с моделью
	 */
	public $value2Label;

	/**
	 * @var string the name of the event where the input will be attached to the slider. It
	 * can be 'slide', 'stop' or 'change'. If you want to use 'slide' event change $event property to 'change'
	 */
	public $event = 'slide';

	/**
	 * @var string name of attribute for max value if slider is used in range mode
	 */
	public $maxAttribute;

	/**
	 * @var string $template шаблон range режима
	 */
	public $template = '{label}{label2}<br />{value}{value2}{slider}';

	/**
	 * Run this widget.
	 * This method registers necessary javascript and renders the needed HTML code.
	 */
	public function run()
	{
		list($name,$id)=$this->resolveNameID();

		$isRange=isset($this->options['range']) && $this->options['range'];

		if(isset($this->htmlOptions['id']))
			$id=$this->htmlOptions['id'];
		else
			$this->htmlOptions['id']=$id;
		if(isset($this->htmlOptions['name']))
			$name=$this->htmlOptions['name'];

		if($this->hasModel())
		{
			$attribute=$this->attribute;
			if ($isRange)
			{
			  // Если максимальный и минимальный атрибуты пустые или выходят за пределы диапазона, инициализируем их значениями из опций max/min
			  if(empty($this->model->{$this->attribute}) || $this->model->{$this->attribute} < $this->options['min'])
			    $this->model->{$this->attribute} = $this->options['min'];
			  if(empty($this->model->{$this->maxAttribute}) || $this->model->{$this->maxAttribute} > $this->options['max'])
			    $this->model->{$this->maxAttribute} = $this->options['max'];
			    
				$options=$this->htmlOptions;
				$label = CHtml::activeLabel($this->model,$this->attribute);
				$label2 = CHtml::activeLabel($this->model,$this->maxAttribute);

				$value = CHtml::activeTextField($this->model,$this->attribute,$options);
				$options['id']=$options['id'].'_end';
				$value2 = CHtml::activeTextField($this->model,$this->maxAttribute,$options);
				$attrMax=$this->maxAttribute;
				$this->options['values']=array($this->model->$attribute,$this->model->$attrMax);
			}
			else
			{
				echo CHtml::activeHiddenField($this->model,$this->attribute,$this->htmlOptions);
				$this->options['value']=$this->model->$attribute;
			}
		}
		else
		{
			if ($isRange)
			{
				// Если максимальный и минимальный атрибуты пустые или выходят за пределы диапазона, инициализируем их значениями из опций max/min
				if($this->value === null || $this->value < $this->options['min'])
					$this->value = $this->options['min'];
				if($this->value2 === null || $this->value2 < $this->options['max'])
					$this->value2 = $this->options['max'];
			    
				$options=$this->htmlOptions;
				$label = CHtml::label($this->valueLabel, $name.'Min');
				$label2 = CHtml::label($this->value2Label, $name.'Max');

				$value = CHtml::textField($name.'Min',$this->value,$options);
				$options['id']=$options['id'].'_end';
				$value2 = CHtml::textField($name.'Max',$this->value2,$options);
				$this->options['values']=array($this->value,$this->value2);
			}
			else
			{
				echo CHtml::hiddenField($name,$this->value,$this->htmlOptions);
				if($this->value!==null)
					$this->options['value']=$this->value;
			}
		}

		$idHidden = $this->htmlOptions['id'];
		$nameHidden = $name;

		$this->htmlOptions['id']=$idHidden.'_slider';
		$this->htmlOptions['name']=$nameHidden.'_slider';
		echo '<div class="FilterRangeSlider">';
		echo strtr($this->template, array(
			'{label}' => $label,
			'{label2}' => $label2,
			'{value}' => $value,
			'{value2}' => $value2,
			'{slider}' => CHtml::tag($this->tagName,$this->htmlOptions, ''),
			));
		echo "</div>";
		

		$this->options[$this->event]= $isRange ?
			"js:function(e,ui){ v=ui.values; jQuery('#{$idHidden}').val(v[0]); jQuery('#{$idHidden}_end').val(v[1]);}":
			'js:function(event, ui) { jQuery(\'#'. $idHidden .'\').val(ui.value); }';
			
		$this->options['stop']= "js:function(e,ui){ jQuery('#{$idHidden}').change(); jQuery('#{$idHidden}_end').change(); }";

		// гарантируем, что имеем дело с числами, а не строками
		$this->options['min'] = $this->options['min'] * 1;
		$this->options['max'] = $this->options['max'] * 1;
			
		$options=empty($this->options) ? '' : CJavaScript::encode($this->options);

		$js = "jQuery('#{$id}_slider').slider($options);\n";
		Yii::app()->getClientScript()->registerScript(__CLASS__.'#'.$id, $js);
	}

}
