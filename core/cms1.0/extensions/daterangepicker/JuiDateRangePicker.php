<?php
/**
 * Виджет, подсказывающий два поля, к которым прикреплены дейтпикеры.
 * Удобен для использования в CGridView для фильтрации по диапазонам дат
 *
 * На основе: Using CJuiDatePicker for CGridView filter
 * http://www.yiiframework.com/wiki/318/using-cjuidatepicker-for-cgridview-filter/
 * http://www.yiiframework.com/wiki/345/how-to-filter-cgridview-with-from-date-and-to-date-datepicker/
 * http://www.yiiframework.com/forum/index.php/topic/20941-filter-date-range-on-cgridview-toolbar/
 * 
 * @author Sviatoslav <dev@udf.su>
 * @version 1.0
 * @package cms.extensions.daterangepicker
 */
class JuiDateRangePicker extends CWidget
{
	/**
	 * @var CModel $this->model модель к которой нужно биндить поля
	 */
	public $model;

	/**
	 * @var string $attributeFrom аттрибут модели для выбора начальной даты
	 */
	public $attributeFrom;

	/**
	 * @var string $attributeTo аттрибут модели для выбора конечной даты
	 */
	public $attributeTo;

	/**
	 * @note js скопирован из вьюхи create.php цшвпуе CreateOrderWidget
	 */
	public function run()
	{
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model'=> $this->model, 
			'attribute' => $this->attributeFrom,
			'language' => Yii::app()->language,
			'htmlOptions' => array('class'=>'reinstallDatePicker'),
			'defaultOptions' => array(  
				'showOn' => 'focus', 
				'showOtherMonths' => true,
				'selectOtherMonths' => true,
				'changeMonth' => true,
				'changeYear' => true,
				'showButtonPanel' => true,
				'autoSize' => true,
				'dateFormat' => "yy-mm-dd",
			),
		));
		
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model'=> $this->model, 
			'attribute'=> $this->attributeTo, 
			'language' => Yii::app()->language,
			'htmlOptions' => array('class'=>'reinstallDatePicker'),
		));

		Yii::app()->clientScript->registerScript('reinstallDatePicker', '
			$(document).bind("ajaxComplete", function(){
				$(".reinstallDatePicker").each(function(){
					$(this).datepicker($.datepicker.regional["' . Yii::app()->language . '"])
				});
			});
		');
	}
}