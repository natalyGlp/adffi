<?php

class CKEditor extends CInputWidget
{
    /**
     * @var boolean добавляет дополнительный Input для включения/отключения редактора
     */
    public $onOffButtonSelector = false;

    public function init()
    {
        $this->registerClientScript();
    }

    public function run()
    {
        list($name, $id) = $this->resolveNameID();
        $this->registerEditorInitScript($id);

        $this->htmlOptions['id'] = $id;

        if ($this->hasModel()) {
            echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
        } else {
            echo CHtml::textArea($this->name, $this->value, $this->htmlOptions);
        }
    }

    protected function registerClientScript()
    {
        $assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets');
        Yii::app()->clientScript
            ->registerScriptFile($assetsUrl.'/ckeditor/ckeditor.js', CClientScript::POS_END)
            ->registerScriptFile($assetsUrl.'/ckeditor/adapters/jquery.js', CClientScript::POS_END)
            ;
    }
    protected function registerEditorInitScript($textareaId)
    {
        $assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets');
        $uploadsPath = Yii::getPathOfAlias("uploads").'/editor';
        $site = Yii::app()->controller->site;

        Yii::app()->session->add('KCFINDER'.$site, array(
            'uploadURL' => 'http://'.$site.'/uploads/editor',
            'uploadDir' => $uploadsPath,
        ));
        Yii::app()->clientScript->registerScript(__FILE__.'#'.$this->id, <<<EOD
        (function() {
            var config = {
                height: 350,
                width : '100%',
                resize_enabled : false,
                allowedContent: true, // отключаем агресивное фильтрование html

                filebrowserBrowseUrl: '$assetsUrl/kcfinder/browse.php?opener=ckeditor&type=files&site=$site',
                filebrowserImageBrowseUrl: '$assetsUrl/kcfinder/browse.php?opener=ckeditor&type=images&site=$site',
                filebrowserFlashBrowseUrl: '$assetsUrl/kcfinder/browse.php?opener=ckeditor&type=flash&site=$site',
                filebrowserUploadUrl: '$assetsUrl/kcfinder/upload.php?opener=ckeditor&type=files&site=$site',
                filebrowserImageUploadUrl: '$assetsUrl/kcfinder/upload.php?opener=ckeditor&type=images&site=$site',
                filebrowserFlashUploadUrl: '$assetsUrl/kcfinder/upload.php?opener=ckeditor&type=flash&site=$site'
            };

            var editor;
            function enableEditor() {
                editor = CKEDITOR.replace('$textareaId', config);
            }
            function disableEditor() {
                if (editor) {
                    editor.destroy();
                    editor = null;
                }
            }
            var onOffSelector = "{$this->onOffButtonSelector}";
            if (onOffSelector) {
                $(onOffSelector).change(function() {
                    if ($(this).is(':checked')) {
                        enableEditor();
                    } else {
                        disableEditor()
                    }
                }).change();
            } else {
                enableEditor();
            }
        }());
EOD
        );
    }
}
