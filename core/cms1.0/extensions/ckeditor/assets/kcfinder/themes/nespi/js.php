<?php

namespace kcfinder;

?>
<?php
// Поддержка сайтов сателлитки
?>
(function() {
    var origGetUrl = _.getURL;
    var site = location.search.split('&').filter(function(el) {
        return el.indexOf('site=')>-1;
    })[0].split('=')[1];

    _.getURL = function(arg) {
        return origGetUrl(arg) + '&site=' + site;
    };
}());
<?php

chdir("..");
chdir("..");
require "core/autoload.php";
$theme = basename(dirname(__FILE__));
$min = new minifier("js");
$min->minify("cache/theme_$theme.js");
