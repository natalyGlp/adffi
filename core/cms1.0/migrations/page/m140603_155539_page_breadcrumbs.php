<?php
/**
 * Добавляет возможность задать титл для хлебной крошки страницы
 */
class m140603_155539_page_breadcrumbs extends CDbMigration
{
	public function up()
	{
		$this->addColumn('{{page}}', 'breadcrumb_title', 'varchar(255) NOT NULL AFTER `title`');
	}

	public function down()
	{
		$this->dropColumn('{{page}}', 'breadcrumb_title');
	}
}