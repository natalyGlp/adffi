<?php
/**
 * Добавляет primary key индексы и меняет Int -> unstigned int
 */
class m141120_095439_menu_fix_pk extends CDbMigration
{
    public function up()
    {
        try {
            $this->alterColumn('{{menu}}', 'menu_id', 'int(11) unsigned NOT NULL AUTO_INCREMENT');
        } catch (Exception $e) {
        }
        try {
            $this->alterColumn('{{menu_item}}', 'menu_item_id', 'int(11) unsigned NOT NULL AUTO_INCREMENT');
        } catch (Exception $e) {
        }
        try {
            $this->addPrimaryKey('menu_pk', '{{menu}}', 'menu_id');
        } catch (Exception $e) {
        }
        try {
            $this->addPrimaryKey('menu_item_pk', '{{menu_item}}', 'menu_item_id');
        } catch (Exception $e) {
        }
    }

    public function down()
    {
    }
}
