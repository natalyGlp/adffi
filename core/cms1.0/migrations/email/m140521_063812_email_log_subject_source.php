<?php

/**
 * Поля темы и 
 */
class m140521_063812_email_log_subject_source extends CDbMigration
{
	public function up()
	{
		$this->addColumn('{{email_log}}', 'subject', 'VARCHAR(128) DEFAULT "No Subject" AFTER id');
		$this->addColumn('{{email_log}}', 'source', 'VARCHAR(128) NULL AFTER content');
	}

	public function down()
	{
		$this->dropColumn('{{email_log}}', 'subject');
		$this->dropColumn('{{email_log}}', 'source');
	}
}