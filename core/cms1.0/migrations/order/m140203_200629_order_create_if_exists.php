<?php
/**
 * Проверяет есть ли уже исходные таблицы заказов order. И создает их, если необходимо
 */
class m140203_200629_order_create_if_exists extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		ob_start();
		?>
--
-- Структура таблицы `gxc_order`
--

CREATE TABLE IF NOT EXISTS `gxc_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `address_id` int(10) unsigned NOT NULL,
  `operator_id` int(11) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `currency` varchar(3) NOT NULL,
  `currency_provider` int(10) unsigned NOT NULL,
  `comment` text NOT NULL,
  `referer` varchar(255) NOT NULL,
  `ip` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `adress_id` (`address_id`),
  KEY `operator_id` (`operator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_order_address`
--

CREATE TABLE IF NOT EXISTS `gxc_order_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `country` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `street` varchar(300) NOT NULL,
  `house` varchar(10) DEFAULT NULL,
  `flat` varchar(10) DEFAULT NULL,
  `telephone` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_order_check`
--

CREATE TABLE IF NOT EXISTS `gxc_order_check` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `prod_id` int(11) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `price` decimal(19,2) unsigned NOT NULL,
  `total_price` decimal(19,2) unsigned NOT NULL,
  `meta` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id` (`order_id`,`prod_id`),
  KEY `prod_id` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gxc_order_client`
--

CREATE TABLE IF NOT EXISTS `gxc_order_client` (
  `order_id` int(10) unsigned NOT NULL,
  `first_name` varchar(125) NOT NULL,
  `last_name` varchar(125) DEFAULT NULL,
  `email` varchar(75) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `gxc_order`
--
ALTER TABLE `gxc_order`
  ADD CONSTRAINT `gxc_order_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `gxc_order_address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `gxc_order_check`
--
ALTER TABLE `gxc_order_check`
  ADD CONSTRAINT `gxc_order_check_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `gxc_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gxc_order_client`
--
ALTER TABLE `gxc_order_client`
  ADD CONSTRAINT `gxc_order_client_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `gxc_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

		<?php
		$sql = ob_get_clean();
		$this->execute($sql);
	}

	public function safeDown()
	{
	}
}