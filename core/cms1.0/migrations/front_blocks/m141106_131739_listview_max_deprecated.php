<?php
/**
 * Убирает deprecated параметр ListViewBlock::max
 */
Yii::import('cms.models.page.*');
class m141106_131739_listview_max_deprecated extends CDbMigration
{
    public function safeUp()
    {
        $this->migrateListViews();
    }

    public function safeDown()
    {
        return true;
    }

    protected function migrateListViews()
    {
        $listviews = Block::model()->findAllByAttributes(array('type' => 'listview'));

        foreach ($listviews as $listview) {
            $params = @unserialize($listview->params);
            if (!$params || !isset($params['max'])) {
                continue;
            }

            $this->migrateMax($params);
        }
    }

    protected function migrateMax($params)
    {
        if (!isset($params['content_list']) || empty($params['content_list'])) {
            return;
        }

        $contentLists = ContentList::model()->findAllByPk($params['content_list']);
        foreach ($contentLists as $list) {
            if ($list->type == 2) { // ConstantDefine::CONTENT_LIST_TYPE_AUTO
                $this->updateListDependingOfMax($list, $params['max']);
            }
        }
    }

    protected function updateListDependingOfMax($list, $max)
    {
        if ($max > 0) {
            // отключаем пагинацию и устанавливаем number=max
            $list->paging = '0';
            $list->number = $max;
        } else {
            // убеждаемся, что пагинация включена
            $list->paging = '1';
        }

        if (!$list->save()) {
            throw new CException("Error saving ContentList model: ".var_export($list->errrors, true));
        }
    }
}
