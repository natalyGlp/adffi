<?php
$this->pageTitle = _t('Add new Resource');
$this->pageHint = _t('Here you can add new Resource');
$this->widget('cms.widgets.resource.ResourceCreateWidget');
