<?php
$this->pageTitle = _t('Manage Resource');
$this->pageHint = _t('Here you can manage your Resource');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'Resource'
));
