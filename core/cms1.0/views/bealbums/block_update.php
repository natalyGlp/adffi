<?php
$this->pageTitle = _t('Update Album');
$this->pageHint = _t('Here you can update new Album');
$this->widget('cms.widgets.albums.AlbumsUpdateWidget', array(
    'id' => $id
));
