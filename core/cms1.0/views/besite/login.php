<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>


<div class="login_box">
    <div id="header-login" style="width: 100%;text-align: center; position:absolute; top: -150px;" >
        <img src="<?= GxcHelpers::assetUrl('images/logo.png', 'backend'); ?>" />
    </div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-content',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
        'afterValidate' => 'js:function() {boxHeight(); return true;}',
	),
)); ?>

    <div class="top_b">Sign in to NESPI Admin</div>
    <div class="alert alert-info alert-login">
        <?php echo _t('Please fill in your Username and Password'); ?>
    </div>
    <div class="cnt_b">
        <div class="formRow">
            <div class="input-prepend">
                <span class="add-on"><i class="icon-user"></i></span>
                <?php echo $form->textField($model,'username',array('class'=>'text-input', 'placeholder' => $model->getAttributeLabel('username'))); ?>
                <?php echo $form->error($model,'username',array('style'=>'text-align:right')); ?>
            </div>
        </div>
        <div class="formRow">
            <div class="input-prepend">
                <span class="add-on"><i class="icon-lock"></i></span>
            <?php echo $form->passwordField($model,'password',array('class'=>'text-input', 'placeholder' => $model->getAttributeLabel('password'))); ?>
            <?php echo $form->error($model,'password',array('style'=>'text-align:right')); ?>
            </div>
        </div>
        <div class="formRow clearfix">
            <label class="checkbox">
                <?php echo $form->checkBox($model,'rememberMe',array('style'=>'float:left;margin-right: 5px;')); ?>
                <?php echo $form->label($model,'rememberMe'); ?>
            </label>
        </div>
    </div>
    <div class="btm_b clearfix">
        <button class="btn btn-inverse pull-right" type="submit">Sign In</button>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->

<script>
    var boxHeight;
    $(document).ready(function(){
        $('input.error').each(function() {$(this).parent().addClass('error')});

        //* boxes animation
        form_wrapper = $('.login_box');
        boxHeight = function() {
            form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);
        };
        form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
    });
</script>
