<?php
$this->pageTitle = _t('Manage Social Settings');
$this->pageHint = _t('Here you can manage Social Settings');
$this->widget('cms.widgets.settings.SettingsWidget', array(
    'type' => 'social'
));
