<?php
$this->pageTitle = _t('Manage General Settings');
$this->pageHint = _t('Here you can manage all Site General Settings');
$this->widget('cms.widgets.settings.SettingsWidget');
