<?php for($i=1; $i<=11; $i++): ?>
<?php
	if(is_null($options->{'filter'.$i}) || empty($options->{'filter'.$i})){
		continue;
	}

    // TODO: Заставить обратно работать. Коммит: 001ea71bcc0805bf31cc5fee486499fa3d1fbf8f
?>
<div class="row">
    <label><?php echo $options->{'filter'.$i}; ?></label>
    <?php
    	switch ($options->options['filter'.$i]) {
    		case '1':{
    			echo CHtml::activeDropDownList($model, 'filter'.$i, isset($options->options['select_options']['filter'.$i]) ? $options->options['select_options']['filter'.$i] : array());
    			break;
    		}
    		case '2':{
    			echo CHtml::activeDropDownList($model, 'filter'.$i, isset($options->options['select_options']['filter'.$i]) ? $options->options['select_options']['filter'.$i] : array(), array('multiple' => 'multiple'));
    			break;
    		}
    		case '3':{
    			echo CHtml::activeDropDownList($model, 'filter'.$i, isset($options->options['select_options']['filter'.$i]) ? $options->options['select_options']['filter'.$i] : CHtml::listData(Term::model()->findAll(array('select' => array('term_id', 'name'), 'condition' => 'taxonomy_id=?', 'params' => array((isset($options->options['taxonomy_id']['filter'.$i]) ? $options->options['taxonomy_id']['filter'.$i] : 0)))), 'term_id', 'name'), array('empty' => ''));
    			break;
    		}
    		case '4':{
    			echo CHtml::activeDropDownList($model, 'filter'.$i, isset($options->options['select_options']['filter'.$i]) ? $options->options['select_options']['filter'.$i] : CHtml::listData(Term::model()->findAll(array('select' => array('term_id', 'name'), 'condition' => 'taxonomy_id=?', 'params' => array((isset($options->options['taxonomy_id']['filter'.$i]) ? $options->options['taxonomy_id']['filter'.$i] : 0)))), 'term_id', 'name'), array('empty' => '','multiple' => 'multiple'));
    			break;
    		}
    		case '5':{
                Yii::app()->clientScript->registerCssFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/css/jquery.autocomplete.css');
                Yii::app()->clientScript->registerScriptFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/js/jquery.bgiframe.js');
                Yii::app()->clientScript->registerScriptFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/js/jquery.ajaxqueue.js');
                Yii::app()->clientScript->registerScriptFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/js/jquery.autocomplete.js');

                echo CHtml::textField(
                    'suggestBrand'.$i, 
                    ($model->isNewRecord || empty($model->{'filter'.$i})) 
                    ? '' 
                    : Yii::app()->{CONNECTION_NAME}
                                ->createCommand()
                                ->select('object_name')
                                ->from('{{object}}')
                                ->where('object_id=?', 
                    array((int)$model->{'filter'.$i}))->queryScalar()
                );

                echo '
                <script>
                    $(document).ready(function(){
                         $("#suggestBrand'.$i.'").legacyautocomplete("'.(!isset($_GET['site']) || empty($_GET['site']) ? bu() : '').'/beobject/suggestBrand?site='.(isset($_GET['site']) ? $_GET['site'] : $_SERVER['HTTP_HOST']).'", {
                            "matchCase":false,
                            "mustMatch":false,
                            "multiple":false
                        }).result(function(event,item){ 
                            if(item!==undefined){
                                $("#Filters_filter'.$i.'").val(item[1]);
                            }
                        });
                    });
                </script>
                ';                

                echo CHtml::activeHiddenField($model, 'filter'.$i);
                break;
    		}
    		case '6':{
                Yii::app()->clientScript->registerCssFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/css/ui-lightness/jquery-ui-1.10.3.custom.min.css');
                Yii::app()->clientScript->registerCssFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/css/datepicker.css');
                Yii::app()->clientScript->registerScriptFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/js/jquery-ui-1.10.3.custom.min.js');
                Yii::app()->clientScript->registerScriptFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/js/bootstrap-datepicker.js');
                echo '
                <script>
                    $(document).ready(function(){
                        $("#Filters_filter'.$i.'").datepicker({
                            format: " yyyy",
                            viewMode: "years", 
                            minViewMode: "years"
                        });
                    });
                </script>
                ';     
                
                echo CHtml::activeTextField($model, 'filter'.$i);
                break;
    		}
    		case '7':{
    			Yii::app()->clientScript->registerCssFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/css/datepicker.css');
    			Yii::app()->clientScript->registerScriptFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/js/bootstrap-datepicker.js');
                echo '
                <script>
                    $(document).ready(function(){
                        $("#Filters_filter'.$i.'").datepicker({
                            format: " yyyy",
                            viewMode: "years", 
                            minViewMode: "years"
                        });
                    });
                </script>
                ';     

    		    echo CHtml::activeTextField($model, 'filter'.$i);
                break;
    		}
            case '9':{

                Yii::app()->clientScript->registerCssFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/css/jquery.autocomplete.css');
                Yii::app()->clientScript->registerScriptFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/js/jquery.bgiframe.js');
                Yii::app()->clientScript->registerScriptFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/js/jquery.ajaxqueue.js');
                Yii::app()->clientScript->registerScriptFile((!isset($_GET['site']) || empty($_GET['site']) ? '/project/backend' : '').'/js/jquery.autocomplete.js');

                $db = Yii::app()->{CONNECTION_NAME};
                $modelName = '';

                if($model->{'filter'.$i}){
                    Yii::import(GxcHelpers::getTruePath('content_type.model.ModelObject'));
                    $model_obj = ModelObject::model()->findByPk($model->{'filter'.$i});
                    if($model_obj){
                        Yii::import(GxcHelpers::getTruePath('content_type.brand.BrandObject'));
                        $brand = BrandObject::model()->findByPk($model_obj->brand_id);
                        $modelName = $model_obj->object_name;


                        if($brand){
                            $modelName = $brand->object_name.' '.$model_obj->object_name;
                        }
                    }
                }
                echo CHtml::textField('suggestModel'.$i, ($model->isNewRecord || empty($modelName)) ? '' : $modelName);
                echo '
                <script>
                    $(document).ready(function(){
                        $("#suggestModel'.$i.'").legacyautocomplete("'.(!isset($_GET['site']) || empty($_GET['site']) ? bu() : '').'/beobject/suggestModel?site='.(isset($_GET['site']) ? $_GET['site'] : $_SERVER['HTTP_HOST']).'", {
                            "matchCase":false,
                            "mustMatch":false,
                            "multiple":false
                        }).result(function(event,item){ 
                            if(item!==undefined){
                                $("#Filters_filter'.$i.'").val(item[1]);
                            }
                        });
                    });
                </script>
                ';     
                echo CHtml::activeHiddenField($model, 'filter'.$i);
                break;
            }
    		default:{
    		    echo CHtml::activeTextField($model, 'filter'.$i);
    		    break;
    		}
    	}
    ?>
    <?php echo CHtml::error($model, 'filter'.$i); ?>
</div>
<?php endfor;?>