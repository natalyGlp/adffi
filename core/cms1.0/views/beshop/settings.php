<?php
/**
 * @var CActiveDataProvider $orderTypesDataProvider типы доставки
 * @var OrderType $filterModel модель для фильтра
 */
?>
<div class="form">
<?php $this->renderPartial('cms.widgets.views.ajaxModal'); ?>
<?php
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'settings-form',
        'type'=>'inline',
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>false,
        'clientOptions' => array(
          'validateOnSubmit' => true,
          'validateOnChange' => true,
          // TODO: необходимо как-то вынести этот элемент либо в отдельный файл либо придумать как подловить через js событие успешной валидации. как вариант писать в aftervalidate колбек функцию. сейчас этот же код скопипасчен для вьюхи settings контроллера BeshopController

          // обработчики события afterValidate должны возвращать false, в том случае, если валидация прошла успешно
          'afterValidate' => 'js:function(form, data, hasError) {
            var afterValidateHasErrors = $("body").triggerHandler("afterValidate", [form, data, hasError]);

            //if no error in validation, send form data with Ajax
            if (! hasError) {
              $.ajax({
                type: "POST",
                url: form[0].action,
                context: form[0],
                data: $(form).serialize()+"&submit=1",
                success: function(response) {
                  $(this).parent().replaceWith($(response));
                  $.fn.yiiGridView.update("orderTypes"); // обновим табличку
                  $.sticky("Способ доставки обновлен", {autoclose : 5000, position: "top-center", type: "st-success" });
                },
                error: function() {
                  $.sticky("Во время сохранения возникли ошибки", {autoclose : 5000, position: "top-center", type: "st-error" });
                }
              });

              // блочим отправку формы, так как мы сделали это через аякс
              return false;
            }
            return (!hasError && !afterValidateHasErrors)
        }',
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )
));
?>
<fieldset>

    <legend><?php echo _t('Delivery Types Setup') ?></legend>
    <a href="<?php echo $this->createUrl("ordertypecreate") ?>" class="btn btn-warning ajaxInfo">Add New Delivery Type</a>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $orderTypesDataProvider,
    'filter' => $filterModel,
    'id' => 'orderTypes',
    'columns' => array(
        'id',
        'name',
        array(
            'name' => 'type',
            'filter' => OrderType::getTypesList(),
            'value' => '$data->typeString',
            ),
        'value',
        array(
            'name' => 'enabled',
            'filter' => OrderType::getEnabledList(),
            'value' => '$data->enabledString',
            ),

        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons'=>array(
                'update' => array(
                    'options'=>array(
                        'class'=>'ajaxInfo',
                    ),
                    'url'=> 'Yii::app()->controller->createUrl("ordertypeupdate", array("id" => $data->primaryKey))',
                ),
            ),
        ),
    ),
));
?>
</fieldset>
<!--div class="row buttons">
        <?php echo CHtml::submitButton(_t('Save'),array('class'=>'btn btn-success')); ?>
</div-->
<?php $this->endWidget(); ?>
</div><!-- form -->
