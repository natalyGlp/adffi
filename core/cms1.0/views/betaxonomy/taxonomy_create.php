<?php
$this->pageTitle = _t('Add new Taxonomy');
$this->pageHint = _t('Here you can add new Taxonomy for your Content Type');
$this->widget('cms.widgets.object.TaxonomyCreateUpdateWidget');
