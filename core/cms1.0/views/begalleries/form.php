<?php
$this->pageTitle=_t('Add new Gallery');
$this->pageHint=_t('Here you can add new Gallery');
?>
<?php $this->renderPartial('cmswidgets.views.notification');
?>
<div class="form">
<?php echo CHtml::form(array(
        'id'=>'taxonomy-form',
        'enableAjaxValidation'=>true,
        )); ?>
<?php echo CHtml::errorSummary($model); ?><br/>
    <table class="form" width="90%" align="center" class="main" cellpadding="5" cellspacing="5">
    	<tr>
        	<td width="200px"><b><?php echo CHtml::activeLabel($model, 'title');?></b></td>
        	<td><?php echo CHtml::activeTextField($model, 'title', array('onblur' => 'translit(this.value, "'.$this->model.'_path");'));?></td>
    	</tr>
        <tr>
            <td><b><?php echo CHtml::activeLabel($model, 'path');?></b></td>
            <td><?php echo CHtml::activeTextField($model, 'path'); ?></td>
        </tr>
        <tr>
            <td><b><?php echo CHtml::activeLabel($model, 'resize_options_json');?></b></td>
            <td>
                <div class="alert alert-block">
                    <h4><?php echo _t('Warning');?></h4>
                    <?php echo _t('All size ids should be named in the Latin alphabet in place of space characters to be used an underscore symbol "_"!');?>
                </div>
                <table id="sizes" width="650px">
                    <tr>
                        <td width="170px" valign="top"><h5><?php echo 'Версия';?></h5></td>
                        <td valign="top"><h5><?php echo ( 'Длина');?></h5></td>
                        <td valign="top"><h5><?php echo ( 'Высота');?></h5></td>
                        <td></td>
                    </tr>
                <?php $i = 0; foreach($model->resize_options_json as $k => $v):?>
                    <tr id="tr_<?php echo $i;?>">
                        <td width="170px" valign="top"><?php echo CHtml::textField('versions['.$i.'][version]', $k);?></td>
                        <td valign="top"><?php echo CHtml::textField('versions['.$i.'][max_width]', $v['max_width']);?></td>
                        <td valign="top"><?php echo CHtml::textField('versions['.$i.'][max_height]', $v['max_height']);?></td>
                        <td valign="top"><button type="button" rel="<?php echo $i;?>" class="remove_version btn btn-danger"><?php echo ('Удалить');?> <b class="icon-remove"></b></button></td>
                    </tr>
                <?php $i++; endforeach;?>
                </table>
                <button type="button" class="btn btn-info" id="add_size"><?php echo ('Добавить размер');?> <b class="icon-plus-sign"></b></button>
            </td>
        </tr>
    	<tr>
        	<td colspan="2" align="center">
        		<br/>
        	    <?php echo CHtml::submitButton('Сохранить', array('id' => "submit", 'class' => 'btn')); ?>
        	</td>
        </tr>
    </table>
<?php echo CHtml::endForm(); ?>
</div>
<script type="text/javascript">
    function init_btn(){
        $('.remove_version').click(function(e){
            e.preventDefault();
            $('#tr_'+$(this).attr('rel')).remove();
        });
    }

    $(document).ready(function(){
        $('#add_size').click(function(e){
            e.preventDefault();
            var i = $('#sizes tr').length;
            $('#sizes').append($(
                '<tr id="tr_'+i+'">'+
                    '<td width="170px" valign="top"><input type="text" name="versions['+i+'][version]"/></td>'+
                    '<td valign="top"><input type="text" name="versions['+i+'][max_width]"/></td>'+
                    '<td valign="top"><input type="text" name="versions['+i+'][max_height]"/></td>'+
                    '<td valign="top"><button type="button" rel="'+i+'" class="remove_version btn btn-danger"><?php echo ('Удалить');?> <b class="icon-remove"></b></button></td>'+
                '</tr>'
            ));
            init_btn();
        });

        init_btn();
    });
</script>
