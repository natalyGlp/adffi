<?php
$this->pageTitle = _t('Add new Item');
$this->pageHint = _t('Here you can add new Item to current Menu');
$this->widget('cms.widgets.page.MenuItemCreateUpdateWidget');
