<?php
switch ($type) {
    case Object::STATUS_DRAFT:
        $this->pageTitle = _t('Manage Drafted Content');
        $this->pageHint = _t('Here you can manage all Drafted content on your site');
        break;

    case Object::STATUS_PUBLISHED:
        $this->pageTitle = _t('Manage Published Content');
        $this->pageHint = _t('Here you can manage all Published content on your site');
        break;

    default:
        $this->pageTitle = _t('Manage Content');
        $this->pageHint = _t('Here you can manage all content on your site');
        break;
}

$this->widget('cms.widgets.object.ObjectManageStatusWidget', array(
    'type' => $type,
    'object_type' => $object_type
));
