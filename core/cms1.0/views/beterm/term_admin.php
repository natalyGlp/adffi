<?php
$this->pageTitle = _t('Manage Terms');
$this->pageHint = _t('Here you can manage your Terms. <br /> <b>Note: </b>When you delete a Term, all contents belong to that Term will be moved to Uncategory Term');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'Term'
));
