<?php
$this->pageTitle = _t('Manage Content List');
$this->pageHint = _t('Here you can manage your Content List');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'ContentList'
));
