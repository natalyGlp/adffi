<?php
$this->pageTitle = _t('Add new Content list');
$this->pageHint = _t('Here you can add new Content list');
$this->widget('cms.widgets.page.ContentListCreateWidget', array(
    'object_update_url' => 'beobject/update'
));
