<div class="row">
    <label><?php echo _t('Parent params');?></label>
    <?php
    $db = Yii::app()->{CONNECTION_NAME};

    $_ids = $db->createCommand()
               ->select(array('id_object', 'id'))
               ->from('{{catalog_attributes}}')
               ->where('id_attributes=?', array($aoid))
               ->queryAll(false);

    $ids = array();
    foreach($_ids as $i){
        $ids[$i[0]] = $i[1];
    }

    $crit = new CDbCriteria;
    $crit->select = array('object_id', 'object_name');
    $crit->addInCondition('object_id', array_keys($ids));
    $crit->compare('object_type', 'catalog');
    $objects = CHtml::listData(Object::model()->findAll($crit), 'object_id', 'object_name');

    $data = array();
    foreach($objects as $k => $v){
        $data[$ids[$k]] = $v;
    }

    echo CHtml::dropDownList('parent_attributes', '', $data, array('empty' => ''));
    ?>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#parent_attributes').change(function(){
            $.get('<?php echo bu(); ?>/becatalogattributes/getparent/?id='+$(this).val()+'&site=<?php echo isset($_GET['site']) ? $_GET['site'] : '';?>', function(data){
                $.each(data, function(k, v){
                    $('#CatalogAttributes_data_'+k).val(v);
                });
            }, 'json');
        });
    });
    </script>
</div>

<?php foreach ($options->options as $k => $value): ?>
    <div class="row">
        <label><?php echo $value['name'];?></label>
        <?php
            switch ($value['type']) {
                case 0: {
                    echo CHtml::textArea(get_class($model).'[data]['.$k.']', isset($model->data[$k]) ? $model->data[$k] : '');
                    break;
                }
                case 1: {
                    echo CHtml::textField(get_class($model).'[data]['.$k.']', isset($model->data[$k]) ? $model->data[$k] : 0);
                    break;
                }
                case 2: {
                    echo CHtml::dropDownList(get_class($model).'[data]['.$k.']', isset($model->data[$k]) ? $model->data[$k] : '', $value['list_data']);
                    break;
                }
                case 3: {
                    echo CHtml::dropDownList(
                        get_class($model).'[data]['.$k.'][]',
                        isset($model->data[$k]) ? $model->data[$k] : '',
                        $value['list_data'],
                        array('multiple' => 'multiple', 'size' => 10)
                    );
                    break;
                }
            }
        ?>
    </div>
<?php endforeach ?>
