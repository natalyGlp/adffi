<?php
$this->pageTitle = _t('Manage catalog attributes options');
$this->pageHint = _t('Here you can manage your catalog attributes options');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'CatalogAttributesOptions'
));
