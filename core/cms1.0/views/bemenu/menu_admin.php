<?php
$this->pageTitle = _t('Manage Menu');
$this->pageHint = _t('Here you can manage your Menu');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'Menu'
));
