<?php
$this->breadcrumbs=array(
	'Languages'=>array('index'),
	$model->lang_id,
);

$this->menu=array(
	array('label'=>'List Language', 'url'=>array('index')),
	array('label'=>'Create Language', 'url'=>array('create')),
	array('label'=>'Update Language', 'url'=>array('update', 'id'=>$model->lang_id)),
	array('label'=>'Delete Language', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->lang_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Language', 'url'=>array('admin')),
);
?>

<h1>View Language #<?php echo $model->lang_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'lang_id',
		'lang_name',
		'lang_desc',
		'lang_required',
		'lang_active',
		'lang_short',
	),
)); ?>
