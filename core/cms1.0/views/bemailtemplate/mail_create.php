<?php
$this->pageTitle = _t('Add new Mail Template');
$this->pageHint = _t('Here you can add new Mail Template');
$this->widget('cms.widgets.mail.MailTemplateCreateWidget');
