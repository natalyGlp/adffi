<?php
$this->pageTitle = _t('Update Mail Template');
$this->pageHint = _t('Here you can update information for current Mail Template');
$this->widget('cms.widgets.mail.MailTemplateUpdateWidget');
