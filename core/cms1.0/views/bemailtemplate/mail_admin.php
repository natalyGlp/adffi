<?php
$this->pageTitle = _t('Manage Mail Templates');
$this->pageHint = _t('Here you can manage your Mail Templates');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'MailTemplate'
));
