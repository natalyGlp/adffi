<?php
    $this->beginContent('cms.views.layouts.main');
    $site = isset($this->site) ? $this->site : ''; // rights модуль не имеет свойства site в контроллере
?>

<div id="contentwrapper">
    <div class="main_content">
        <div class="clear-cache">
            <?php $this->widget('cms.widgets.caching.AjaxCachingClearWidget'); ?>
        </div>
        <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
            'links' => $this->breadcrumbs
        ));?><!-- breadcrumbs -->

        <?php if(Yii::app()->hasComponent('translate')): ?>
            <div class="pull-right">
                <?php
                echo Yii::app()->translate->editLink('Manage translations', 'link', array('class' => 'btn')) . ' ';
                echo Yii::app()->translate->missingLink('Missing translations', 'link', array('class' => 'btn'));
                ?>
            </div>
        <?php endif; ?>

        <h2><?php
            $imagesUrl = GxcHelpers::assetUrl('images', 'backend');
            if (isset($this->titleImage) && ($this->titleImage != '')) {
                echo '<img src="' . $imagesUrl . '/' . $this->titleImage . '" />';
            }
            if (isset($this->pageTitle)) {
                echo $this->pageTitle;
            }
        ?></h2>
        <?php if (isset($this->pageHint) && ($this->pageHint != '')) : ?>
            <p><?php echo $this->pageHint; ?></p>
        <?php endif; ?>

        <?php if (isset($this->menu) && count($this->menu) > 0) : ?>
                <div class="header-info">
                    <?php
                    $this->widget('bootstrap.widgets.TbButtonGroup', array(
                        'type' => 'pills',
                        'buttons'=>$this->menu,
                        'htmlOptions' => array('style'=>'margin: 20px 0;')
                    ));
                    ?>
                </div>
        <?php endif; ?>
        <?php $this->renderPartial('cms.widgets.views.notification'); ?>
        <?php echo $content; ?>
    </div>
</div>

<!-- sidebar -->
<a href="javascript:void(0)" class="sidebar_switch on_switch ttip_r" title="Hide Sidebar">Sidebar switch</a>
<div class="sidebar">
    <div class="antiScroll">
        <div class="antiscroll-inner">
            <div class="antiscroll-content">
                <div class="sidebar_inner">
                    <form id="search-box" method="get" action="#" target="_blank" class="input-append">
                        <input id="topSearchBox" autocomplete="off" type="text" maxlength="2048" name="q" size="16" placeholder="<?php echo _t('Search...') ?>" aria-haspopup="true" class="search_query input-medium" />
                        <button type="submit" class="btn"><i class="icon-search" id="searchbutton"></i></button>
                    </form>
                    <div id="side_accordion" class="accordion">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Content');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse in" id="collapseOne">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => CMap::mergeArray(array(
                                            array('label' => _t('Manage Content'), 'url' => array('/beobject/admin', 'site'=>$site)),
                                            array('label' => _t('Create Content'), 'url' => array('/beobject/create', 'site'=>$site))
                                        ), GxcHelpers::getAvailableContentTypeLikeMenu('&site='.$site)),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne1" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Category');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne1">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => _t('Create Term'), 'url' => array('/beterm/create', 'site'=>$site)),
                                            array('label' => _t('Manage Terms'), 'url' => array('/beterm/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beterm' && in_array(Yii::app()->controller->action->id, array('update', 'admin'))),
                                            '---',
                                            array('label' => _t('Create Taxonomy'), 'url' => array('/betaxonomy/create', 'site'=>$site)),
                                            array('label' => _t('Mangage Taxonomy'), 'url' => array('/betaxonomy/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'betaxonomy' && in_array(Yii::app()->controller->action->id, array('update', 'admin')))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne2" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Pages');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne2">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => _t('Create Menu'), 'url' => array('/bemenu/create', 'site'=>$site)),
                                            array('label' => _t('Manage Menus'), 'url' => array('/bemenu/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'bemenu' && in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index'))),
                                            '---',
                                            array('label' => _t('Create Content List'), 'url' => array('/becontentlist/create', 'site'=>$site), 'visible' => user()->isAdmin ? true : false,),
                                            array('label' => _t('Manage Content Lists'), 'url' => array('/becontentlist/admin', 'site'=>$site),
                                                'active' => ((Yii::app()->controller->id == 'becontentlist') && (in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index'))) ? true : false)),
                                            '---',
                                            array('label' => _t('Create Block'), 'url' => array('/beblock/create', 'site'=>$site), 'visible' => user()->isAdmin ? true : false,),
                                            array('label' => _t('Manage Blocks'), 'url' => array('/beblock/admin', 'site'=>$site),
                                                'active' => ((Yii::app()->controller->id == 'beblock') && (in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index'))) ? true : false)),
                                            '---',
                                            array('label' => _t('Create Page'), 'url' => array('/bepage/create', 'site'=>$site)),
                                            array('label' => _t('Manage Pages'), 'url' => array('/bepage/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'bepage' && in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index')))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseRes" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Resources');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseRes">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => _t('Create Resource'), 'url' => array('/beresource/create', 'site'=>$site)),
                                            array('label' => _t('Manage Resource'), 'url' => array('/beresource/admin', 'site'=>$site),
                                                'active' => ((Yii::app()->controller->id == 'resource') && (in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index')))) ? true : false)
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne3" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Gallegies');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne3">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => _t('Create Gallery'), 'url' => array('/begalleries/create', 'site'=>$site)),
                                            array('label' => _t('Manage Galleries'), 'url' => array('/begalleries/admin', 'site'=>$site)),
                                            '---',
                                            array('label' => _t('Manage Albums'), 'url' => array('/bealbums/admin', 'site'=>$site)),
                                            array('label' => _t('Create Albums'), 'url' => array('/bealbums/create', 'site'=>$site)),
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseManage" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Manage');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseManage">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => _t('Comments'), 'url' => array('/becomment/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'comments'),
                                            array('label' => _t('Email log'), 'url' => array('beemail/index', 'site'=>$site)),
                                            array('label' => _t('Lucene Search Indexes'), 'url' => array('besearch/index', 'site'=>$site), 'active' => Yii::app()->controller->id == 'besearch'),
                                            array('label' => _t('Sitemap'), 'url' => array('besitemap/index', 'site'=>$site), 'active' => Yii::app()->controller->id == 'besitemap'),
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne4" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Mails');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne4">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => _t('Create Mail Template'), 'url' => array('/bemailtemplate/create', 'site'=>$site)),
                                            array('label' => _t('Manage Mail Templates'), 'url' => array('/bemailtemplate/admin', 'site'=>$site)),
                                            array('label' => _t('Email log'), 'url' => array('beemail/index', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beemail'),
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>


                        <div class="accordion-group hide">
                            <div class="accordion-heading">
                                <a href="#collapseOne25" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Shop');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne25">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => _t('Settings'), 'url' => array('/beshop/settings', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beshop' && Yii::app()->controller->action->id == 'settings'),
                                            array('label' => _t('Manage Orders'), 'url' => array('/beorder/index', 'site'=>$site)),
                                            '---',
                                            array('label' => _t('Create Brand'), 'url' => array('/beobject/create', 'type' => 'brand', 'site'=>$site)),
                                            array('label' => _t('Manage Brand'), 'url' => array('/beobject/admin', 'type' => 'brand', 'site'=>$site)),
                                            '---',
                                            array('label' => _t('Create Model'), 'url' => array('/beobject/create', 'type' => 'model', 'site'=>$site)),
                                            array('label' => _t('Manage Model'), 'url' => array('/beobject/admin', 'type' => 'model', 'site'=>$site)),
                                            '---',
                                            array('label' => _t('Create Shop'), 'url' => array('/beobject/create', 'type' => 'shop', 'site'=>$site)),
                                            array('label' => _t('Manage Shop'), 'url' => array('/beobject/admin', 'type' => 'shop', 'site'=>$site)),
                                            '---',
                                            array('label' => _t('Create Catalog'), 'url' => array('/beobject/create', 'type' => 'catalog', 'site'=>$site)),
                                            array('label' => _t('Manage Catalog'), 'url' => array('/beobject/admin', 'type' => 'catalog', 'site'=>$site)),
                                            '---',
                                            array('label' => _t('Create Catalog Attributes'), 'url' => array('/becatalogattributes/create', 'site'=>$site)),
                                            array('label' => _t('Manage Catalog Attributes'), 'url' => array('/becatalogattributes/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'becatalogattributes' && in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index'))),
                                            '---',
                                            array('label' => _t('Create Filters'), 'url' => array('/befilters/create', 'site'=>$site)),
                                            array('label' => _t('Manage Filters'), 'url' => array('/befilters/admin', 'site'=>$site), 'active' => Yii::app()->controller->id == 'befilters' && in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index')))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne5" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Languages');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne5">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => _t('Create Language'), 'url' => array('/belanguage/create', 'site'=>$site)),
                                            array('label' => _t('Manage Languages'), 'url' => array('/belanguage/admin', 'site'=>$site))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseUser" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Users');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseUser">
                                <div class="accordion-inner">
                                <?php
                                    $arr = array(
                                            array('label' => _t('Create User'), 'url' => array('/beuser/create', 'site'=>$site)),
                                            array('label' => _t('Manage Users'), 'url' => array('/beuser/admin', 'site'=>$site),
                                                'active' => ((Yii::app()->controller->id == 'beuser') && (in_array(Yii::app()->controller->action->id, array('update', 'view', 'admin', 'index')))) ? true : false
                                            ),
                                            '---',
                                            array('label' => _t('Setup oauth'), 'url' => array('/beuser/oauth', 'site'=>$site), 'active' => in_array(Yii::app()->controller->action->id, array('oauth'))),
                                    );

                                    if (!$site) {
                                        $arr = array_merge($arr, array(
                                            '---',
                                            array(
                                                    'label'=>Rights::t('core', 'Assignments'),
                                                    'url'=>array('/rights/assignment/view'),
                                                    'itemOptions'=>array('class'=>'item-assignments'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                            array(
                                                    'label'=>Rights::t('core', 'Permissions'),
                                                    'url'=>array('/rights/authItem/permissions'),
                                                    'itemOptions'=>array('class'=>'item-permissions'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                            array(
                                                    'label'=>Rights::t('core', 'Roles'),
                                                    'url'=>array('/rights/authItem/roles'),
                                                    'itemOptions'=>array('class'=>'item-roles'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                            array(
                                                    'label'=>Rights::t('core', 'Tasks'),
                                                    'url'=>array('/rights/authItem/tasks'),
                                                    'itemOptions'=>array('class'=>'item-tasks'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                            array(
                                                    'label'=>Rights::t('core', 'Operations'),
                                                    'url'=>array('/rights/authItem/operations'),
                                                    'itemOptions'=>array('class'=>'item-operations'),
                                                    'linkOptions'=>array('class'=>'button'),
                                            ),
                                        ));
                                    }

                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => $arr,
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne6" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Settings');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne6">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array('label' => _t('General setup'), 'url' => array('/besettings/general', 'site'=>$site)),
                                            array('label' => _t('System setup'), 'url' => array('/besettings/system', 'site'=>$site)),
                                            array('label' => _t('Social setup'), 'url' => array('/besettings/social', 'site'=>$site))
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a href="#collapseOne7" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
                                    <?php echo _t('Admin');?>
                                </a>
                            </div>
                            <div class="accordion-body collapse" id="collapseOne7">
                                <div class="accordion-inner">
                                <?php
                                    $this->widget('bootstrap.widgets.TbMenu', array(
                                        'type'=>'list',
                                        'items' => array(
                                            array(
                                                'label' => _t('Caching'),
                                                'url' => array('/becaching/clear','site'=>$site),
                                                'linkOptions' => array('id' => 'menu_8', 'class' => 'menu_8'),
                                                'itemOptions' => array('id' => 'menu_8'),
                                            ),
                                            array('label' => _t('Logs'), 'url' => array('/beadmin/log', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beadmin' && Yii::app()->controller->action->id == 'log'),
                                            array('label' => _t('Backups'), 'url' => array('/beadmin/backup', 'site'=>$site), 'active' => Yii::app()->controller->id == 'beadmin' && Yii::app()->controller->action->id == 'backup'),
                                            array('label' => _t('Check updates'),
                                                'url' => array('/beupdate/index', 'site'=>$site),
                                                'active' => Yii::app()->controller->id == 'beupdate' && Yii::app()->controller->action->id == 'index',
                                                'visible' => !$site,
                                                )
                                        ),
                                        'encodeLabel' => false
                                    ));
                                ?>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->renderPartial('cms.views.layouts.calculator');?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
    $('.accordion-body').removeClass('in');
    $('.accordion-inner li.active').parent('ul')
                                   .parent('.accordion-inner')
                                   .parent('.accordion-body').addClass('in');
});
</script>
<?php $this->endContent(); ?>
