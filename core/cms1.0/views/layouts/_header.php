<meta charset="utf-8" />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta name="robots" content="noindex,nofollow">

<?php
    $cs = Yii::app()->clientScript;
    $cs->registerCoreScript('jquery');
    $cs->registerCoreScript('jquery.ui');

    //GxcHelpers::registerCss('qtip2/jquery.qtip.min.css');
    // GxcHelpers::registerCss('jquery-ui/css/Aristo/Aristo.css');
    // GxcHelpers::registerCss('img/splashy/splashy.css', 'gebo');
    // GxcHelpers::registerCss('img/flags/flags.css', 'gebo');
    // GxcHelpers::registerCss('css/style.css', 'gebo');
    // GxcHelpers::registerCss('css/nespi.css', 'backend');

    GxcHelpers::registerJs('js/functions.js', 'backend', CClientScript::POS_HEAD);
    GxcHelpers::registerJs('qtip2/jquery.qtip.min.js');
    GxcHelpers::registerJs('antiscroll/antiscroll.js');
    GxcHelpers::registerJs('antiscroll/jquery-mousewheel.js');
    GxcHelpers::registerJs('UItoTop/jquery.ui.totop.min.js'); // используется
    GxcHelpers::registerJs('js/jquery.debouncedresize.min.js', 'gebo');
    GxcHelpers::registerJs('js/jquery.actual.min.js', 'gebo');
    GxcHelpers::registerJs('js/jquery_cookie.min.js', 'gebo');
    GxcHelpers::registerJs('js/selectNav.js', 'gebo'); // сжимание навигации для моб девайсов
    GxcHelpers::registerJs('js/gebo_common.js', 'gebo'); // инициализация темы

    $cs->registerScript(__FILE__.'#loader', <<<EOC
        (function () {
            var geboOnloadFunction = function() {jQuery("html").removeClass("js")};
            var geboLoaderTimer = setTimeout(geboOnloadFunction,1000);
            $(document).load(function() {geboOnloadFunction(); clearTimeout(geboLoaderTimer)});

            $(function() {
                //Hide the second level menu
                $('#left-sidebar ul li ul').hide();
                //Show the second level menu if an item inside it active
                $('li.list_active').parent("ul").show();

                $('#left-sidebar').children('ul').children('li').children('a').click(function () {
                    if($(this).parent().children('ul').length>0){
                        $(this).parent().children('ul').toggle();
                    }
                });

                $().UItoTop({inDelay:200,outDelay:200,scrollSpeed: 500});
            })
        })();
EOC
, CClientScript::POS_END);