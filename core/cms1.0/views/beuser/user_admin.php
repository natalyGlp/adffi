<?php
$this->pageTitle = _t('Manage Users');
$this->pageHint = _t('Here you can view all members information of your site');
$this->widget('cms.widgets.ModelManageWidget', array(
    'model_name' => 'User'
));
