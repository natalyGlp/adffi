<?php
$this->pageTitle = _t('Update My Settings');
$this->pageHint = _t('Make sure your email is correct to receive new message from site');
$this->widget('cms.widgets.user.UserUpdateSettingsWidget');
