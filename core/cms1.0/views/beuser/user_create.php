<?php
$this->pageTitle = _t('Create new User');
$this->pageHint = _t('Here you can add new member for the site');
$this->widget('cms.widgets.user.UserCreateWidget');
