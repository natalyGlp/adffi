<?php
/**
 * Backend Site Controller.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package backend.controllers
 *
 */
class BesiteController extends BeController
{
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index');

    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }

        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = new UserLoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['UserLoginForm'])) {

            $model->attributes = $_POST['UserLoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $this->checkEnvironment();
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }

        // display the login form
        $this->layout = 'cms.views.layouts.login';
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(bu() . '/besite/login');
    }

    /**
     * Выполняет некоторые проверки окружения:
     * - права на запись логов
     */
    public static function checkEnvironment()
    {
        $routes = Yii::app()->log->routes;
        foreach ($routes as $route) {
            if ($route instanceof CFileLogRoute) {
                $path = $route->getLogPath() . DIRECTORY_SEPARATOR . $route->getLogFile();
                if (!is_writable($path)) {
                    Yii::app()->user->setFlash('warning', _t('Warning! The following log file is not writable: ') . $path);
                }
                $path = str_replace('backend', 'frontend', $path);
                if (!is_writable($path)) {
                    Yii::app()->user->setFlash('warning', _t('Warning! The following log file is not writable: ') . $path);
                }
            }
        }
    }
}
