<?php
/**
 * Backend Language Controller.
 *
 * @author Yura KOvalenko <kimmongod@gmail.com>
 * @version 1.0
 * @package backend.controllers
 *
 */
class BelanguageController extends BeController
{
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->menu = array(
            array('label' => _t('Manage Languages'), 'url' => array('admin'), 'linkOptions' => array('class' => 'button')),
            array('label' => _t('Add Language'), 'url' => array('create'), 'linkOptions' => array('class' => 'button')),
        );

    }

    public function actionCreate()
    {

        $this->render('language_create');
    }

    /**
     * The function that do Manage Language
     *
     */
    public function actionAdmin()
    {
        $this->render('language_admin');
    }

    /**
     * The function that view Language details
     *
     */
    public function actionView()
    {
        $id = isset($_GET['id']) ? (int) ($_GET['id']) : 0;
        $this->menu = array_merge($this->menu,
            array(
                array('label' => _t('Update this Language'), 'url' => array('update', 'id' => $id), 'linkOptions' => array('class' => 'button')),
                array('label' => _t('View this Language'), 'url' => array('view', 'id' => $id), 'linkOptions' => array('class' => 'button')),
            )
        );
        $this->render('language_view');
    }

    /**
     * The function that update Language
     *
     */
    public function actionUpdate()
    {
        $id = isset($_GET['id']) ? (int) ($_GET['id']) : 0;
        $this->menu = array_merge($this->menu,
            array(
                array('label' => _t('Update this Language'), 'url' => array('update', 'id' => $id), 'linkOptions' => array('class' => 'button')),
                array('label' => _t('View this Language'), 'url' => array('view', 'id' => $id), 'linkOptions' => array('class' => 'button')),
            )
        );
        $this->render('language_update', array());
    }

    /**
     * The function is to Delete Language
     *
     */
    public function actionDelete($id)
    {
        Language::ajaxDeleteModel($id);
    }
}
