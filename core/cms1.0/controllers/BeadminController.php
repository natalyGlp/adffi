<?php
/**
 * Backend Admin Controller.
 *
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.controllers
 */

Yii::import('cms.models.admin.*');
class BeadminController extends BeController
{
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->menu = array(
            array('label' => _t('Logs'), 'url' => array('log'), 'linkOptions' => array('class' => 'button')),
            array('label' => _t('Backups'), 'url' => array('backup'), 'linkOptions' => array('class' => 'button')),
            array('label' => _t('Caching'), 'url' => array('/becaching/clear'), 'linkOptions' => array('class' => 'button')),
        );

    }

    /**
     * Выводит лог приложения
     */
    public function actionLog()
    {
        $this->pageTitle = 'Логи';
        $this->render('log', array(
        ));

    }

    /**
     * Бекап базы данных
     */
    public function actionBackup()
    {
        $this->pageTitle = 'Резервные копии';
        $this->render('backup', array(
        ));

    }
}
