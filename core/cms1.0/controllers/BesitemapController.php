<?php
/**
 * Контроллер для ручного запуска генерации Sitemap
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.controllers
 *
 */
class BesitemapController extends BeController
{
    /**
     * Manage comment given by the comment status (type)
     * and the object (object_id) to which all the comment belong
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Sitemap generation';
        $layouts = GxcHelpers::getAvailableLayouts(false);
        $regions = isset($layouts[$this->page->layout]['regions']) ? $layouts[$this->page->layout]['regions'] :
        $available_layouts['base']['regions'];

        // да, это не mvc, но тут как бы и функционала особо нету
        $this->renderText(
            $this->renderPartial('cmswidgets.views.notification', null, true)
            . CHtml::beginForm($this->createUrl('create'))
            . CHtml::label(_t('Select region with the main content'), 'region')
            . CHtml::dropDownList('region', '', $regions)
            . '<p>' . CHtml::submitButton(_t('Update Sitemap'), array('class' => 'btn btn-gebo'))
            . CHtml::endForm()
        );
    }

    public function actionCreate()
    {
        $result = GxcHelpers::runYiiCommand('sitemap', array(
            'host' => $_SERVER['HTTP_HOST'],
            'contentRegion' => isset($_POST['region']) ? $_POST['region'] : false,
        ), false);

        if ($result['returnCode'] === 0) {
            Yii::app()->user->setFlash('success', _t('Sitemap was successfully updated'));
        } else {
            Yii::app()->user->setFlash('error', _t('There was an error generating sitemap'));
        }

        $this->redirect('index');
    }
}
