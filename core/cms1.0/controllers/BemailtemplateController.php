<?php
/**
 * Backend MailTemplate Controller.
 *
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @version 1.0
 * @package backend.controllers
 *
 */
class BemailtemplateController extends BeController
{
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->menu = array(
            array('label' => _t('Manage Mail Templates'), 'url' => array('admin'), 'linkOptions' => array('class' => 'button')),
            array('label' => _t('Create Mail Template'), 'url' => array('create'), 'linkOptions' => array('class' => 'button')),
        );

    }

    /**
     * The function that do Create new MailTemplate
     *
     */
    public function actionCreate()
    {
        $this->render('mail_create');
    }

    /**
     * The function that do Manage MailTemplate
     *
     */
    public function actionAdmin()
    {
        $this->render('mail_admin');
    }

    /**
     * The function that view Mail details
     * @param integer $id Record ID
     */
    public function actionView($id)
    {
        $id = isset($_GET['id']) ? (int) ($_GET['id']) : 0;
        $this->menu = array_merge($this->menu,
            array(
                array('label' => _t('View this Mail Template'), 'url' => array('view', 'id' => $id), 'linkOptions' => array('class' => 'button')),
            )
        );
        $this->render('mail_view');
    }

    /**
     * The function that update MailTemplate
     * @param integer $id Record ID
     */
    public function actionUpdate()
    {
        $id = isset($_GET['id']) ? (int) ($_GET['id']) : 0;
        $this->menu = array_merge($this->menu,
            array(
                array('label' => _t('View this Mail Template'), 'url' => array('view', 'id' => $id), 'linkOptions' => array('class' => 'button')),
            )
        );
        $this->render('mail_update', array('id' => $id));
    }

    /**
     * The function is to Delete MailTemplate
     * @param integer $id Record ID
     */
    public function actionDelete($id)
    {
        GxcHelpers::deleteModel('MailTemplate', $id);
    }
    /**
     * The function send emails to email lists format " name: <email> ;name: <email> ;name: <email> ;name: <email> ;"
     * @param intereк $id Mail template Id
     */
    public function actionSend($id = null)
    {
        $model = MailTemplate::model()->findByPk($id);
        $mailtemplate = MailTemplate::model()->findByPk($id);
        if (isset($_POST['emails']) && is_string($_POST['emails'])) {

            $emails = $_POST['emails'];
            $emails_array = explode(';', $emails);
            $emails_list = array();
            foreach ($emails_array as $item) {
                $temp_email = explode(':', $item);

                if (isset($temp_email[0]) && ($temp_email[0] != '') && isset($temp_email[1]) && ($temp_email[1] != '') && filter_var($temp_email[1], FILTER_VALIDATE_EMAIL)) {
                    $emails_list[$temp_email[0]] = $temp_email[1];
                    // var_dump($emails_list);
                }
            }

            foreach ($emails_list as $key => $email) {
                $keys = array(array('{username}'), array($key));
                $subject = $mailtemplate->prepareMail('mail_title', $keys);
                $text = $mailtemplate->prepareMail('mail_text', $keys);
                $m = new YiiMailMessage;
                $m->addTo($email);
                $m->from = Yii::app()->settings->get('general', 'support_email');
                $m->subject = $subject;
                $m_content = $text;
                $m->setBody($m_content, 'text/html');
                try {
                    // Yii::app()->mail->send($m);
                    $email_list[$email]['send'] = true;
                } catch (Exception $e) {
                    $email_list[$email]['send'] = false;
                }
            }

            $this->render('mail_sender', array('model' => $model, 'emails' => $email_list));
            // send mails and return status
            //
        } else {
            // render form
            $this->render('mail_sender', array('model' => $model));
        }
    }

}
