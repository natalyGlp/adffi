<?php
/**
 * Backend Caching Controller.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package backend.controllers
 *
 */

class BecachingController extends BeController
{
    /**
     * The function that do clear Cache
     *
     */
    public function actionClear()
    {
        $this->render('clear_cache');
    }
}
