<?php
/**
 * Backend Search Controller.
 *
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.controllers
 *
 */
Yii::import('cms.models.search.*');
class BesearchController extends BeController
{
    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
        $this->menu = array(
            array('label' => _t('View all Indexes'), 'url' => array('index'), 'linkOptions' => array('class' => 'button')),
            array('label' => _t('Create new Index'), 'url' => array('create'), 'linkOptions' => array('class' => 'button')),
        );
    }

    /**
     * The function that do View User
     *
     */
    public function actionUpdate($id)
    {
        $this->runIndexManageScript('update', array(
            'id' => $id,
        ));

        Yii::app()->controller->redirect(array('index'));
    }

    /**
     * Запускает консольный скрипт обработки индекса
     * @param  string $commandAction дополнительные параметры после имени комманды
     * @param  array $args          аргументы --key=value
     */
    protected function runIndexManageScript($commandAction, $args)
    {
        $args['rebuild'] = isset($args['rebuild']) ? $args['rebuild'] : false;
        $result = GxcHelpers::runYiiCommand('luceneIndex ' . $commandAction, $args);

        if ($result['returnCode'] == 0) {
            Yii::app()->user->setFlash('success', _t('The index {action} is in progress now!', 'cms', array(
                '{action}' => $this->action->id,
            )));
        } else {
            Yii::app()->user->setFlash('fail', _t('There was an error processing index {action}!', 'cms', array(
                '{action}' => $this->action->id,
            )));
        }
    }

    public function actionRebuild($id)
    {
        $this->runIndexManageScript('update --rebuild', array(
            'id' => $id,
        ));

        Yii::app()->controller->redirect(array('index'));
    }

    public function actionOptimize($id)
    {
        $this->runIndexManageScript('optimize', array(
            'id' => $id,
        ));

        Yii::app()->controller->redirect(array('index'));
    }

    /**
     * Возвращает прогресс обработки индексов, если такие есть
     */
    public function actionQueue()
    {
        Yii::import('cms.commands.LuceneIndexCommand');
        $status = LuceneIndexCommand::getProcessSummary();
        echo CJSON::encode($status);
        Yii::app()->end();
    }

    /**
     * The function that do View User
     *
     */
    public function actionCreate()
    {
        $this->render('search_create_index');
    }

    /**
     * Manage comment given by the comment status (type)
     * and the object (object_id) to which all the comment belong
     */
    public function actionIndex()
    {
        $this->render('search_index');
    }

    public function actionDelete($id)
    {
        SearchLucene::model()->findByPk($id)->delete();
    }
}
