<?php
/**
 * This is the model class for table "{{content_list}}".
 *
 * The followings are the available columns in table '{{content_list}}':
 * @property string $content_list_id
 * @property string $name
 * @property string $value
 * @property integer $created
 */

// TODO: если у нас MANUAL список, то manual_list переменная должна быть обязательной при валидации
class ContentList extends CmsActiveRecord
{
    // This list is manual or auto
    public $type = self::TYPE_MANUAL;

    const TYPE_MANUAL = 1;
    const TYPE_AUTO = 2;
    const TYPE_DYNAMIC = 3;

    // Language of the Object
    public $lang = 'all';

    // Content type we want to get
    public $content_type = 'all';

    //Object Terms
    public $terms = '0';

    //Object Tags
    public $tags = '';

    public $paging = true;

    //Number Items to get
    public $number = 10;

    // Criteria of the list
    public $criteria = self::CRITERIA_NEWEST;

    const CRITERIA_NEWEST = 1;
    const CRITERIA_MOST_VIEWED_ALLTIME = 2;
    const CRITERIA_WITH_MAIN = 3;
    const CRITERIA_ALPHABETICAL = 4;
    const CRITERIA_RANDOM = 5;

    // Show posts with dates in future
    public $showFutureDates = false;

    public $fallbackToMainLanguage = true;

    // Manual List
    public $manual_list = array();

    /**
     * @var boolean если true, то все обьекты будут создаваться со вьюхами их родного контент типа
     */
    public $forceObjectType = false;

    /**
     * Returns the static model of the specified AR class.
     * @return ContentList the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{content_list}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('number', 'numerical', 'integerOnly' => true, 'min' => -1),
            array('number', 'compare', 'compareValue' => '0', 'operator' => '!='),
            array('number', 'validateNumberWithPagination'),
            array('paging, fallbackToMainLanguage, showFutureDates', 'boolean'),
            array('type,lang,content_type,terms,tags,paging,number,criteria,manual_list,showFutureDates,forceObjectType, fallbackToMainLanguage', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('content_list_id, name, value, created', 'safe', 'on' => 'search'),
        );
    }

    public function validateNumberWithPagination($attribute, $params = array())
    {
        if ($this->number == -1 && $this->paging != 0) {
            $this->paging = 0;
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'content_list_id' => _t('Content List'),
            'name' => _t('Name'),
            'value' => _t('Value'),
            'created' => _t('Created'),

            'type' => _t('Type'),
            'lang' => _t('Language'),
            'content_type' => _t('Content type'),
            'terms' => _t('Terms'),
            'tags' => _t('Tags'),
            'paging' => _t('Pagination'),
            'number' => _t('Items to show per page (-1 — show all)'),
            'criteria' => _t('Criteria'),
            'manual_list' => _t('Manual List'),
            'showFutureDates' => _t('Show posts with dates in future'),
            'fallbackToMainLanguage' => _t('Fallback to main language'),
            'forceObjectType' => _t('Force object type view rendering (detected by first element in the list)'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('content_list_id', $this->content_list_id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('created', $this->created);

        $sort = new CSort;
        $sort->attributes = array(
            'content_list_id',
        );
        $sort->defaultOrder = 'content_list_id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
        ));
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->created = time();
            }

            $arr = array(
                'type' => $this->type,
                'lang' => $this->lang,
                'content_type' => $this->content_type,
                'terms' => $this->terms,
                'tags' => $this->tags,
                'paging' => $this->paging,
                'number' => $this->number,
                'criteria' => $this->criteria,
                'manual_list' => $this->manual_list,
                'showFutureDates' => $this->showFutureDates,
                'fallbackToMainLanguage' => $this->fallbackToMainLanguage,
                'forceObjectType' => $this->forceObjectType,
            );

            if ($this->forceObjectType && count($this->manual_list) > 0) {
                // сохраняем информацию о контент типах
                $arr['forceObjectType'] = Object::model()->findByPk(reset($this->manual_list))->object_type;
            }

            $this->value = serialize($arr);

            return true;
        }

        return false;
    }

    protected function afterFind()
    {
        parent::afterFind();

        $arr = unserialize($this->value);
        foreach ($arr as $key => $value) {
            if (isset($this->$key)) {
                $this->$key = $value;
            }
        }
    }

    public static function getContentType()
    {
        $types = GxcHelpers::getAvailableContentType();

        $result = array('all' => _t('All'));
        foreach ($types as $key => $value) {
            $result[$key] = $value['name'];
        }
        return $result;
    }

    public static function getContentLang()
    {
        $result = array(
            'all' => _t('All'),
            'auto' => _t('Auto (Based on user language)'),
        );
        $result = CMap::mergeArray($result, Language::items());

        return $result;
    }

    public static function getDynamicTerms()
    {
        $type = isset($_POST['q']) ? $_POST['q'] : array('0');
        $lang = isset($_POST['lang']) ? $_POST['lang'] : array('all');

        $criteria = new CDbCriteria();
        $isLangsSelected = !(in_array('all', $lang) || in_array('auto', $lang));
        $isTypesSelected = !in_array('all', $type);
        if ($isLangsSelected) {
            $criteria->addInCondition('lang', $lang);
        }
        if ($isTypesSelected) {
            $criteria->addInCondition('type', $type);
        }

        $terms = Term::model()->with('taxonomy')->findAll($criteria);

        echo CHtml::tag('option', array(
            'value' => '0',
            'selected' => 'selected',
        ), _t('All'), true);

        if (count($terms) > 0) {
            foreach ($terms as $term) {
                echo CHtml::tag('option', array(
                    'value' => $term->term_id,
                ), CHtml::encode($term->name), true);
            }
        }
    }

    public static function suggestTags()
    {
        if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
            $tags = Tag::model()->suggestTags($keyword);
            if ($tags !== array()) {
                echo implode("\n", $tags);
            }
        }
    }

    public static function suggestContent()
    {
        if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
            $type = isset($_GET['type']) ? trim($_GET['type']) : '';
            $tags = Object::model()->suggestContent($keyword, $type);
            if ($tags !== array()) {
                echo implode("\n", $tags);
            }
        }
    }

    public static function getTerms()
    {
        $terms = Term::model()->findAll();
        $result = array('0' => _t('All'));
        foreach ($terms as $term) {
            $result[$term->term_id] = $term->name;
        }
        return $result;
    }

    public static function getTypesList()
    {
        return array(
            self::TYPE_MANUAL => _t("Manual"),
            self::TYPE_AUTO => _t("Auto"),
            self::TYPE_DYNAMIC => _t("Dynamic"),
        );
    }

    public static function getCriteriasList()
    {
        return array(
            self::CRITERIA_NEWEST => _t("Newsest"),
            self::CRITERIA_MOST_VIEWED_ALLTIME => _t("Most viewed all time"),
            self::CRITERIA_WITH_MAIN => _t("With main"),
            self::CRITERIA_ALPHABETICAL => _t("In alphabetical order"),
            self::CRITERIA_RANDOM => _t("Random order"),
        );
    }
}
