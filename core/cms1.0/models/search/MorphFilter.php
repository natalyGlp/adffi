<?php
/**
 * Морфологический фильтр для Lucene с использованием phpMorphy
 */

class MorphFilter extends Zend_Search_Lucene_Analysis_TokenFilter
{
	private $morphy;

	public function __construct($lang = 'ru_RU')
	{
		//инициализируем объект phpMorphy
		// Yii::import('cms.vendors.*');
		// require_once "phpmorphy/src/common.php";
		require_once Yii::getPathOfAlias('cms.vendors.phpmorphy.src') . DIRECTORY_SEPARATOR . 'common.php';

		$dir = Yii::getPathOfAlias('cms.vendors.phpmorphy.dicts');

		$this->morphy = new phpMorphy($dir, $lang);
	}

	/**
	 * Возвращает корень слова или слово целиком, если морфи не справился
	 */
	public function getRoot($str)
	{
		//извлекаем корень слова
		$pseudo_root = $this->morphy->getPseudoRoot(mb_strtoupper($str, "utf-8"));
		if ($pseudo_root === false)
			$str = mb_strtoupper($str, "utf-8");
			//если корень извлечь не удалось, тогда используем все слово целиком
		else
			$str = $pseudo_root[0];

		return $str;
	}

	public function normalize(Zend_Search_Lucene_Analysis_Token $srcToken)
	{
		//извлекаем корень слова
		$newStr = $this->getRoot($srcToken->getTermText());

		//если лексема короче 3 символов, то не используем её    
		if (mb_strlen($newStr, "utf-8") < 3)
			// return null;
			$newStr = mb_strtoupper($srcToken->getTermText(), "utf-8"); // возвращаем все же строку, так как это может быть аббревиатура типа "НБУ"

		$newToken = new Zend_Search_Lucene_Analysis_Token(
			$newStr,
			$srcToken->getStartOffset(),
			$srcToken->getEndOffset()
			);

		$newToken->setPositionIncrement($srcToken->getPositionIncrement());

		return $newToken;
	}
}