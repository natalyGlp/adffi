<?php

/**
 * This is the model class for table "{{object}}".
 *
 * The followings are the available columns in table '{{object}}':
 * @property string $object_id
 * @property string $object_author
 * @property integer $object_date
 * @property integer $object_date_gmt
 * @property string $object_content
 * @property string $object_title
 * @property string $object_excerpt
 * @property integer $object_status
 * @property integer $comment_status
 * @property string $object_password
 * @property string $object_name
 * @property integer $object_modified
 * @property integer $object_modified_gmt
 * @property string $object_content_filtered
 * @property string $object_parent
 * @property string $guid
 * @property string $object_type
 * @property string $comment_count
 * @property string $object_slug
 * @property string $object_description
 * @property integer $lang
 * @property string $object_author_name
 * @property integer $total_number_meta
 * @property integer $total_number_resource
 * @property string $tags
 * @property string $is_main
 * @property integer $object_view
 * @property integer $like_count
 * @property integer $dislike_count
 * @property integer $rating_scores
 * @property double $rating_average
 * @property string $layout
 */
class Object extends CmsActiveRecord
{
    /* -==================CONSTANTS=====================- */
    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT = 2;
    const STATUS_HIDDEN = 4;

    /**
     * Перелинковка
     */
    const RA_RANDOM = 0;
    const RA_STATIC = 1;
    /* -==================END CONSTANTS=================- */

    /*-====================Vars==========================-*/

    //The old Tags
    protected $_oldTags;
    protected $_metaAttributes;

    //This is to check the person the Object will be transferd to
    public $person;

    /*-====================END Vars==========================-*/

    /* -==================BASE METHODS==================- */

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'author' => array(self::BELONGS_TO, 'User', 'object_author'),
            'language' => array(self::BELONGS_TO, 'Language', 'lang'),
            'termsRelation' => array(self::HAS_MANY, 'ObjectTerm', 'object_id'),
            'terms' => array(self::HAS_MANY, 'Term', array('term_id' => 'term_id'), 'through' => 'termsRelation'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className
     * @return CModel
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{object}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            // TODO: валидация даты object_date
            array('total_number_meta, total_number_resource, object_slug, object_date', 'safe'),
            array('object_slug', 'validateSlug'), // этот валидактор должен быть до required, так как он попытается заполнить object_slug, если он пустой
            array('object_name, object_slug', 'required'),
            array('object_content', 'length', 'min' => 10),
            array('object_description,object_excerpt,object_title,guid', 'safe'),
            array('object_status, comment_status, object_modified, object_modified_gmt, lang, total_number_meta, total_number_resource, object_view, object_hits, object_uniq_hits, like_count, dislike_count, rating_scores', 'numerical', 'integerOnly' => true),
            array('rating_average, is_main', 'numerical'),
            array('object_author, object_password, object_parent, object_type, comment_count', 'length', 'max' => 20),
            array('guid, object_author_name', 'length', 'max' => 255),
            array('layout', 'length', 'max' => 125),
            array('tags', 'checkTags'),
            array('tags', 'normalizeTags'),
            array('person', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('object_id, object_author, read_also_ids, object_date, read_also, read_also_limit, object_content, object_title, object_status, object_name', 'safe',
                'on' => 'search,draft,published'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'sellable' => _t('Sellable'),
            'object_id' => _t('Object'),
            'object_author' => _t('Object Author'),
            'object_date' => _t('Object Date'),
            'object_date_gmt' => _t('Object Date Gmt'),
            'object_content' => _t('Object Content'),
            'object_title' => _t('Object Title'),
            'object_excerpt' => _t('Object Excerpt'),
            'object_status' => _t('Object Status'),
            'comment_status' => _t('Comment Status'),
            'object_password' => _t('Object Password'),
            'object_name' => _t('Object Name'),
            'object_modified' => _t('Object Modified'),
            'object_modified_gmt' => _t('Object Modified Gmt'),
            'object_content_filtered' => _t('Object Content Filtered'),
            'object_parent' => _t('Object Parent'),
            'guid' => _t('Guid'),
            'object_type' => _t('Object Type'),
            'comment_count' => _t('Comment Count'),
            'object_slug' => _t('Object Slug'),
            'object_description' => _t('Object Description'),
            'lang' => _t('Language'),
            'object_author_name' => _t('Object Author Name'),
            'total_number_meta' => _t('Total Number Meta'),
            'total_number_resource' => _t('Total Number Resource'),
            'tags' => _t('Tags'),
            'object_view' => _t('Object View'),
            'object_hits' => _t('Object Hits'),
            'object_uniq_hits' => _t('Object Unique Hits'),
            'like_count' => _t('Like'),
            'dislike_count' => _t('Dislike'),
            'rating_scores' => _t('Rating Scores'),
            'rating_average' => _t('Rating Average'),
            'layout' => _t('Layout'),
            'person' => _t('Person'),
            'read_also' => _t('Read also'),
            'read_also_limit' => _t('Read also messages limit'),
            'is_main' => _t('Pinned'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('object_id', $object->object_id, true);
        $criteria->compare('object_author', $object->object_author, true);
        $criteria->compare('object_date', $object->object_date);
        $criteria->compare('object_content', $object->object_content, true);
        $criteria->compare('object_title', $object->object_title, true);
        $criteria->compare('object_status', $object->object_status);

        // FIXME: И зачем тут эта переменная, которая не используется?
        // $sort = new CSort;
        // $sort->attributes = array(
        //     'object_id',
        //     'object_date'
        // );

        // $sort->defaultOrder = 'object_id DESC';

        return new CActiveDataProvider($object, array(
            'criteria' => $criteria,
        ));
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            // $this->read_also_ids = is_array($this->read_also_ids) ? implode(',', $this->read_also_ids) : '';

            if ($this->isNewRecord) {
                if ($this->object_type == '') {
                    $this->object_type = 'object';
                }
                self::extraBeforeSave('create', $this);
            } else {
                self::extraBeforeSave('update', $this);
            }
            return true;
        } else {
            return false;
        }
    }

    protected function afterSave()
    {
        //Check the scenairo if tags updated needed
        $this->updateTagRelationship();

        $this->saveMetaAttributes();

        // обновляем поисковые индексы, если они есть
        try {
            Yii::import('cms.models.search.*');
            $indexes = SearchLucene::model()->findAll();
            foreach ($indexes as $lucene) {
                if (in_array(SearchLucene::ALL_TYPES_ID, $lucene->params['objectTypes']) || in_array($object->object_type, $lucene->params['objectTypes'])) {
                    if ($object->isNewRecord) {
                        SearchLucene::addObjectToIndex($object, $lucene->index_id);
                    } else {

                        SearchLucene::updateObjectInIndex($object, $lucene->index_id);
                    }
                } else {
                    SearchLucene::deleteObjectFromIndex($object, $lucene->index_id);
                }
            }
        } catch (Exception $e) {
            Yii::app()->user->addFlash('error', 'Search index was not updated: '.$e->getMessage());
            Yii::log('Error updating index: '.$e->getMessage(), CLogger::LEVEL_ERROR, 'SearchLucene');
        }
    }

    protected function afterFind()
    {
        $this->extractObjectMeta();

        $this->_oldTags = $this->tags;

        return parent::afterFind();
    }

    /**
     * Excute after Delete Object
     */
    protected function afterDelete()
    {
        parent::afterDelete();

        ObjectMeta::model()->deleteAll('meta_object_id = :obj', array(
            ':obj' => $this->object_id,
        ));

        ObjectResource::model()->deleteAll('object_id = :obj', array(
            ':obj' => $this->object_id,
        ));

        TagRelationships::model()->deleteAll('object_id = :tid', array(
            ':tid' => $this->object_id,
        ));

        ObjectTerm::model()->deleteAll('object_id = :tid', array(
            ':tid' => $this->object_id,
        ));

        // обновляем поисковые индексы, если они есть
        Yii::import('cms.models.search.*');
        $indexes = SearchLucene::getIndexesList();
        foreach ($indexes as $index) {
            SearchLucene::deleteObjectFromIndex($this, $index);
        }
    }

    /* -==================END BASE METHODS==================- */

    public static function getObjectStatus()
    {
        return array(
            self::STATUS_PUBLISHED => _t("Published"),
            self::STATUS_DRAFT => _t("Draft"),
            self::STATUS_HIDDEN => _t("Hidden"),
        );
    }

    public function getReadAlso()
    {
        return array(
            self::RA_RANDOM => _t('Random'),
            self::RA_STATIC => _t('Static'),
        );
    }

    public function validateSlug($attribute, $params)
    {
        $this->$attribute = GxcHelpers::toSlug($this->$attribute);

        if (empty($this->$attribute)) {
            $this->$attribute = GxcHelpers::toSlug($this->object_name);
        }

        // Этот валидатор нельзя записать в ::rules(),
        // так как не будет работать object_type и primaryKey
        Yii::import('cms.extensions.validators.UniquePairValidator', true);
        $validator = new UniquePairValidator();
        $validator->attributes = array($attribute);
        $validator->pairAttribute = 'object_type';

        $validator->validate($this);
    }

    /**
     * Normalize The Tags for the Object - Check Valid
     * @param type $attribute
     * @param type $params
     */
    public function normalizeTags($attribute, $params)
    {
        $this->tags = Tag::array2string(array_unique(Tag::string2array($this->tags)));
    }

    /**
     * Check Tags Valid
     * @param type $attribute
     * @param type $params
     */
    public function checkTags($attribute, $params)
    {
        $result = $this->tags;
        $regex = "/[\^\[\]\$\.\|\?\*\+\(\)\{\}\/\*\%\!\.\'\"\@\#\&\:\<\>\|\-\_\+\=\`\~\;]/";
        if (preg_match($regex, $result)) {
            $this->addError('tags', _t('Tags must contain characters only'));
        }
    }

    public static function extraBeforeSave($type = 'update', $object)
    {
        switch ($type) {
            case 'update':
                $current_time = time();
                $current_time_gmt = local_to_gmt(time());
                $object->object_modified = $current_time;
                $object->object_modified_gmt = $current_time_gmt;
                break;
            case 'create':
                if (empty($object->object_author)) {
                    if (!isConsoleApp() && user()->id) {
                        $object->object_author = user()->id;
                    }
                } else {
                    $object->object_author = 0;
                }
                $current_time = time();
                $current_time_gmt = local_to_gmt(time());
                // $object->object_date=$current_time;
                $object->object_date_gmt = $current_time_gmt;
                $object->object_modified = $current_time;
                $object->object_modified_gmt = $current_time_gmt;
                if ($object->guid == '') {
                    $object->guid = uniqid();
                }
                break;
        }

    }

    /**
     * Update Tag Relationship of the Object
     */
    protected function updateTagRelationship()
    {
        Tag::model()->updateFrequency($this->_oldTags, $this->tags);

        //Start to DElete All the Tag Relationship
        TagRelationships::model()->deleteAll('object_id = :id', array(':id' => $this->object_id));

        //Start to re Insert
        $explode = explode(',', $this->tags);

        foreach ($explode as $ex) {
            $ex = trim($ex);
            $tag = Tag::model()->find('name = :s', array(':s' => $ex));
            if ($tag) {
                $tag_relationship = new TagRelationships;
                $tag_relationship->tag_id = $tag->id;
                $tag_relationship->object_id = $this->object_id;
                $tag_relationship->save();
            }
        }

        $this->_oldTags = $this->tags;
    }

    public function saveMetaAttributes()
    {
        foreach ($this->metaAttributes as $attribute) {
            $this->saveMetaValue($attribute, $this->$attribute, $this);
        }
    }

    protected function extractObjectMeta()
    {
        $this->loadMeta();

        foreach ($this->_metaAttributes as $key => $value) {
            try {
                $this->$key = $value;
            } catch (Exception $e) {
            }
        }
    }

    /**
     * @return array список мета аттрибутов, которые должны быть сохранены в ObjectMeta
     */
    public function getMetaAttributes()
    {
        return array();
    }

    public function getMetaAttribute($key)
    {
        if (!isset($this->_metaAttributes)) {
            $this->loadMeta();
        }

        if (!isset($this->_metaAttributes[$key])) {
            throw new CException("There is no meta attribute with key $key");
        }

        return $this->_metaAttributes[$key];
    }

    protected function loadMeta()
    {
        $this->_metaAttributes = array();

        $objectMeta = ObjectMeta::model()->findAllByAttributes(array(
            'meta_object_id' => $this->object_id,
        ));

        foreach ($objectMeta as $m) {
            $value = @unserialize($m->meta_value);

            if ($value === false && !!$m->meta_value) {
                $value = $m->meta_value;
            }

            $this->_metaAttributes[$m->meta_key] = $value;
        }
    }

    /**
     * Save Meta Data of a Object Content Type
     * @param type $key
     * @param type $value
     * @param type $object
     * @param type $create TODO: DEPRECATED
     */
    public static function saveMetaValue($key, $value, $object, $create = true)
    {
        if ($object->isNewRecord) {
            $object_meta = null;
        } else {
            $object_meta = ObjectMeta::model()->find('meta_key= :key  and meta_object_id = :obj ', array(':key' => $key, ':obj' => $object->object_id));
        }

        $value = serialize($value);

        if ($object_meta !== null) {
            $object_meta->meta_value = $value;
            $object_meta->save();
        } else {
            $object_meta = new ObjectMeta;
            $object_meta->meta_key = $key;
            $object_meta->meta_value = $value;
            $object_meta->meta_object_id = $object->object_id;
            $object_meta->save();
        }
    }

    /**
     * Get Tags of the Object
     * @param type $object_id
     * @return type
     */
    public static function getTags($object_id)
    {
        $req = Yii::app()->{CONNECTION_NAME}->createCommand(
            "SELECT t.name
                                FROM gxc_tag t, gxc_tag_relationships r, gxc_object o
                                WHERE t.id = r.tag_id
                                AND r.object_id = o.user_id
                                AND o.user_id = " . $object_id
        );
        $tags_name = $req->queryAll();
        $result = array();
        if ($tags_name != null) {
            foreach ($tags_name as $tag_name) {
                $result[] = $tag_name['name'];
            }
        }
        return $result;
    }

    /**
     * get Related content by Tags
     * @param type $id
     * @param type $max
     * @return CActiveDataProvider
     */
    public static function getRelatedContentByTags($id, $max)
    {
        $object = Object::loadModel($id);
        $criteria = new CDbCriteria;
        $criteria->join = 'join gxc_tag_relationships ft on ft.object_id = t.object_id';
        $criteria->condition = 'ft.tag_id in (select tag_id from fcms_tag_relationships fr
                                            where fr.object_id = :id)
                                AND t.object_id <> :id
                                AND t.object_status = :status
                                AND t.object_date <= :time
                                AND t.object_type = :type';
        $criteria->distinct = true;
        $criteria->params = array(':id' => $id, ':status' => self::STATUS_PUBLISHED, ':time' => time(), 'type' => $object->object_type);
        $criteria->order = "object_date DESC";
        //$aa = Object::model()->findAll($criteria);
        //$criteria->limit = $max;

        return new CActiveDataProvider('Object', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $max,
            ),
        ));
    }

    /**
     * Load Object that has been published and time is <= time()
     * @param type $id
     * @return type
     */
    public static function loadPublishedModel($id)
    {
        $model = Object::model()->findByPk((int) $id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        } else {
            if (($model->object_status == self::STATUS_PUBLISHED) && ($model->object_date <= time())) {
                return $model;
            } else {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
        }
    }

    /**
     * Возвращает строковое значение статуса обьекта
     * @param type $value
     */
    public function getStatusString()
    {
        $status = $this->getObjectStatus();
        $value = $this->object_status;
        if (isset($status[$value])) {
            return $status[$value];
        } else {
            return _t('undefined');
        }
    }

    /**
     * Convert from value to the String of the Object Status
     * @param type $value
     */
    public static function convertObjectStatus($value)
    {
        $status = self::getObjectStatus();
        if (isset($status[$value])) {
            return $status[$value];
        } else {
            return _t('undefined');
        }
    }

    /**
     * Convert from value to the String of the Object Comment
     * @param type $value
     */
    public static function convertObjectCommentType($value)
    {
        $types = ConstantDefine::getObjectCommentStatus();

        if (isset($types[$value])) {
            return $types[$value];
        } else {
            return _t('undefined');
        }
    }

    /**
     * Convert from value to the String of the Object Type
     * @param type $value
     */
    public static function convertObjectType($value)
    {
        $types = GxcHelpers::getAvailableContentType();
        if (isset($types[$value]['name'])) {
            return $types[$value]['name'];
        } else {
            return _t('undefined');
        }
    }

    /**
     * Импортирует класс обьекта по его типу. Возвращает название класса обьекта
     * @param  string $objectType тип обьекта
     * @param  string $default значение, которое вернется, если не удастся найти обьект
     * @return string             имя класса
     */
    public static function importByObjectType($objectType, $default = 'Object')
    {
        // DEPRECATED
        return self::importByType($objectType, $default);
    }

    public static function importByType($objectType, $default = 'Object')
    {
        // TODO: вообще информация о классе находится в Info.ini, возможно есть смысл отказаться либо от конвертирования, либо от info.ini
        // //Get Types list and type of the Object
        // $types = GxcHelpers::getAvailableContentType();
        // $type = (string)$model->object_type;

        // //Import the Content Type Class
        // Yii::import(GxcHelpers::getTruePath('content_type.'.$type.'.'.$types[$type]['class']));

        if (empty($objectType)) {
            return $default;
        }

        $formatter = new CFormModel();
        $formatedName = $formatter->generateAttributeLabel($objectType);
        $formatedName = str_replace(' ', '', $formatedName);
        $modelName = $formatedName . 'Object';
        if (!class_exists($modelName, false)) {
            $alias = GxcHelpers::getTruePath('content_type.' . $objectType . '.' . $modelName);
            if (!is_file(Yii::getPathOfAlias($alias) . '.php')) {
                return $default;
            }

            Yii::import($alias);
        }

        return $modelName;
    }

    /**
     * Do Search Object based on its status
     * @param type $type
     * @return CActiveDataProvider
     */
    public function doSearch($type = 0, $object_type = '')
    {
        $criteria = new CDbCriteria;
        $sort = new CSort;
        $sort->attributes = array(
            'object_id',
            'object_date',
            'object_name',
            'object_hits',
            'object_uniq_hits',
        );
        $sort->defaultOrder = 'object_date DESC';
        switch ($type) {
            //If looking for DRAFT Content
            case self::STATUS_DRAFT:
                $criteria->condition = 'object_status = :status and object_author = :uid';
                $criteria->params = array(
                    ':status' => self::STATUS_DRAFT,
                    ':uid' => user()->id,
                );
                break;
            //If looking for Published Content
            case self::STATUS_PUBLISHED:
                //Do nothing;
                $criteria->condition = 'object_status = :status';
                $criteria->params = array(':status' => self::STATUS_PUBLISHED);
                break;
        }

        if ($object_type != '') {
            //var_dump($criteria->condition); die();
            if ($type == 0) {
                $criteria->condition .= 'object_type=:type';
            } else {
                $criteria->condition .= 'AND object_type=:type';
            }
            $criteria->params = array_merge($criteria->params, array(':type' => $object_type));
        }

        $criteria->compare('object_id', $this->object_id, true);
        $criteria->compare('object_author', $this->object_author, true);
        $criteria->compare('object_date', $this->object_date);
        $criteria->compare('object_content', $this->object_content, true);
        $criteria->compare('object_title', $this->object_title, true);
        $criteria->compare('object_name', $this->object_name, true);
        $criteria->compare('object_type', $this->object_type, false);
        $criteria->compare('object_status', $this->object_status, false);
        $criteria->compare('lang', $this->lang, false);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    public static function buildLink($obj)
    {
        if ($obj->object_id) {
            return Yii::app()->createUrl('', array(
                'type' => $obj->object_type,
                'slug' => $obj->object_slug,
            ));
        }
    }

    public static function getLink($id)
    {
        if (is_numeric($id)) {
            $link = Object::model()->findByPk($id);
            return $link->getObjectLink();
        }
    }

    public function getObjectLink()
    {
        if ($this->object_id) {
            $className = self::importByObjectType($this->object_type);

            return $className::buildLink($this);
        }
    }

    public function suggestContent($keyword, $type = '', $limit = 20)
    {
        $keyword = preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($keyword));
        $keyword = html_entity_decode($keyword, null, 'UTF-8');
        if ($type == '') {
            $objects = $this->findAll(array(
                'condition' => 'object_name LIKE :keyword',
                'order' => 'object_id DESC',
                'limit' => $limit,
                'params' => array(
                    ':keyword' => '%' . $keyword . '%',
                ),
            ));
        } else {
            $objects = $this->findAll(array(
                'condition' => 'object_type = :t and object_name LIKE :keyword',
                'order' => 'object_name DESC',
                'limit' => $limit,
                'params' => array(
                    ':t' => trim(strtolower($type)),
                    ':keyword' => '%' . $keyword . '%',
                ),
            ));
        }

        $names = array();
        foreach ($objects as $object) {
            $names[] = str_replace(";", "", $object->object_name) . "|" . $object->object_id;

        }
        return $names;
    }

    public static function Resources()
    {
        return array(
            'thumbnail' => array(
                'type' => 'thumbnail',
                'name' => 'Thumbnail',
                'maxSize' => ConstantDefine::UPLOAD_MAX_SIZE,
                'minSize' => ConstantDefine::UPLOAD_MIN_SIZE,
                'width' => 100,
                'height' => 100,
                'max' => 10,
                'allow' => array('jpeg', 'jpg', 'gif', 'png'),
            ));
    }

    /**
     * Increase the comment count by 1 whenever new comment was created.
     */
    public function increaseCommentCount()
    {
        if ($this->comment_count != null) {
            $this->comment_count++;
        } else {

            $this->comment_count = 1;
        }

        $this->save();
    }

    public function getExcerpt($max_characters = 200, $params = array())
    {
        $text = $this->object_content;
        if ((!isset($params['forceContent']) || !$params['forceContent']) && !empty($this->object_excerpt)) {
            return $this->object_excerpt;
        }

        if (isset($params['enableHtml']) && $params['enableHtml']) {
            $p = new CHtmlPurifier();
            if (!(isset($params['preserveImg']) && $params['preserveImg'])) {
                $p->options = array('HTML.ForbiddenElements' => array('img'));
            }
            // отфильтровыем <img>

            $text = $p->purify($text);
        } else {
            $text = trim(strip_tags($text));
        }

        $text = self::truncate($text, $max_characters, ' ...', false, true);

        return $text;
    }

    /**
     * Функция, которая возвращает массив настроек для текущего типа обьекта.
     * Настройки могут влиять к примеру на размер изображений, загружаемых через CKEditor
     */
    public function getOptions()
    {
        return array();
    }

    /**
     * Truncates text.
     *
     * Cuts a string to the length of $length and replaces the last characters
     * with the ending if the text is longer than length.
     *
     * @param string $text String to truncate.
     * @param integer $length Length of returned string, including ellipsis.
     * @param string $ending Ending to be appended to the trimmed string.
     * @param boolean $exact If false, $text will not be cut mid-word
     * @param boolean $considerHtml If true, HTML tags would be handled correctly
     * @return string Trimmed string.
     */
    public static function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false)
    {
        if ($considerHtml) {
            // if the plain text is shorter than the maximum length, return the whole text
            if (mb_strlen(preg_replace('/<.*?>/', '', $text), 'utf-8') <= $length) {
                return $text;
            }

            // splits all html-tags to scanable lines
            preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);

            $total_length = mb_strlen($ending, 'utf-8');
            $open_tags = array();
            $truncate = '';

            foreach ($lines as $line_matchings) {
                // if there is any html-tag in this line, handle it and add it (uncounted) to the output
                if (!empty($line_matchings[1])) {
                    // if it’s an “empty element” with or without xhtml-conform closing slash (f.e.)
                    if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                        // do nothing
                        // if tag is a closing tag (f.e.)
                    } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                        // delete tag from $open_tags list
                        $pos = array_search($tag_matchings[1], $open_tags);
                        if ($pos !== false) {
                            unset($open_tags[$pos]);
                        }
                        // if tag is an opening tag (f.e. )
                    } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                        // add tag to the beginning of $open_tags list
                        array_unshift($open_tags, strtolower($tag_matchings[1]));
                    }
                    // add html-tag to $truncate’d text
                    $truncate .= $line_matchings[1];
                }

                // calculate the length of the plain text part of the line; handle entities as one character
                $content_length = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2]), 'utf-8');
                if ($total_length + $content_length > $length) {
                    // the number of characters which are left
                    $left = $length - $total_length;
                    $entities_length = 0;
                    // search for html entities
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                        // calculate the real length of all entities in the legal range
                        foreach ($entities[0] as $entity) {
                            if ($entity[1] + 1 - $entities_length <= $left) {
                                $left--;
                                $entities_length += mb_strlen($entity[0], 'utf-8');
                            } else {
                                // no more characters left
                                break;
                            }
                        }
                    }
                    $truncate .= mb_substr($line_matchings[2], 0, $left + $entities_length, 'utf-8');
                    // maximum lenght is reached, so get off the loop
                    break;
                } else {
                    $truncate .= $line_matchings[2];
                    $total_length += $content_length;
                }

                // if the maximum length is reached, get off the loop
                if ($total_length >= $length) {
                    break;
                }
            }
        } else {
            if (mb_strlen($text, 'utf-8') <= $length) {
                return $text;
            } else {
                $truncate = mb_substr($text, 0, $length - mb_strlen($ending, 'utf-8'), 'utf-8');
            }
        }

        // if the words shouldn't be cut in the middle...
        if (!$exact) {
            // ...search the last occurance of a space...
            $spacepos = mb_strrpos($truncate, ' ', 'utf-8');
            if (isset($spacepos)) {
                // ...and cut the text in this position
                $truncate = mb_substr($truncate, 0, $spacepos, 'utf-8');
            }
        }

        // add the defined ending to the text
        $truncate .= $ending;

        if ($considerHtml) {
            // close all unclosed html-tags
            foreach ($open_tags as $tag) {
                $truncate .= "</$tag>";
            }
        }

        return $truncate;
    }

    /**
     * @return array дополнительные колонки для CGridView
     */
    public function getGridColumns()
    {
        return array();
    }

    // TODO: убрать в будущем
    /**
     * Фолбек для тех случаев, когда в таблице нету поля хитов
     */
    public function getObject_hits()
    {
        return '';
    }

    /**
     * Фолбек для тех случаев, когда в таблице нету поля хитов
     */
    public function getObject_uniq_hits()
    {
        return '';
    }

    /**
     * Фолбек для тех случаев, когда в таблице нету поля хитов
     */
    public function setObject_hits()
    {
        return '';
    }

    /**
     * Фолбек для тех случаев, когда в таблице нету поля хитов
     */
    public function setObject_uniq_hits()
    {
        return '';
    }
}
