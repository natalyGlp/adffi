<?php

/**
 * This is the model class for table "{{comment}}".
 *
 * The followings are the available columns in table '{{comment}}':
 * @property string $comment_id
 * @property string $parent_model
 * @property string $parent_guid
 * @property string $content
 * @property integer $status
 * @property string $author_name
 * @property string $email
 * @property integer $create_time
 */
class Comment extends CmsActiveRecord
{
    //verify captcha code
    public $verifyCode;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Comment the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{comment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content, author_name, email', 'required'),
            array('status, create_time', 'numerical', 'integerOnly' => true),
            array('parent_guid', 'length', 'max' => 16),
            array('author_name, email', 'length', 'max' => 128),
            array('email', 'email'),
            // TODO: делаем каптчу или не делаем каптчу?
            //array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('comment_id, parent_guid, content, status, author_name, email, create_time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'comment_id' => _t('Comment'),
            'parent_model' => _t('Object model'),
            'parent_guid' => _t('Object guid'),
            'content' => _t('Content'),
            'status' => _t('Status'),
            'author_name' => _t('Author Name'),
            'email' => _t('Email'),
            'create_time' => _t('Create Time'),
            'verifyCode' => _t('Verification Code'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('comment_id', $this->comment_id, true);
        $criteria->compare('parent_guid', $this->parent_model, true);
        $criteria->compare('parent_guid', $this->parent_guid, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('author_name', $this->author_name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('create_time', $this->create_time);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getStatus($status)
    {
        switch ($status) {
            case ConstantDefine::COMMENT_STATUS_PUBLISHED:
                return _t('Published');
            case ConstantDefine::COMMENT_STATUS_PENDING:
                return _t('Pending');
            case ConstantDefine::COMMENT_STATUS_DISCARDED:
                return _t('Discarded');
        }
        return 'Not defined';
    }

    /**
     * @return string форматированная дата создания коммента
     */
    public function getCreateTime()
    {
        return Yii::app()->dateFormatter->formatDateTime($this->create_time, 'medium', 'short');
    }

    protected function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if (!Yii::app()->user->isGuest) {
                if (empty($this->author_name)) {
                    $this->author_name = Yii::app()->user->name;
                }

                if (empty($this->email)) {
                    $this->email = Yii::app()->user->getModel('email');
                }

            }

            return true;
        }
    }

    /*
     * Before saving:
     *         Update the create_time, status and parent_guid
     *         Increase the object's comment_count by 1
     * @see CActiveRecord::beforeSave()
     */
    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                //Update the create_time, status and parent_guid
                $this->create_time = new CDbExpression('NOW()');
                $this->status = ConstantDefine::COMMENT_STATUS_PENDING;

                //Increase the object's comment_count by 1
                $object = Yii::app()->controller->object;
                if ($object != null) {
                    $object->increaseCommentCount();
                    $this->parent_guid = $object->guid;
                    $this->parent_model = get_class($object);
                } else {
                    $this->parent_guid = Yii::app()->controller->page->guid;
                    $this->parent_model = get_class(Yii::app()->controller->page);
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
