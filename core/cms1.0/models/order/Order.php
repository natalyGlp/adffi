<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property string $id
 * @property string $user_id
 * @property string $address_id
 * @property integer $status
 * @property integer $type
 * @property integer $currency
 * @property integer $currency_provider сервис через который был оплачен заказ
 * @property string $comment to order
 * @property string $referer
 * @property string $ip
 * @property string $date
 * @property string $order_id
 * @property string $api_client
 * @property int $operator_id id of operator, that has manualy added this order
 *
 * The followings are the available model relations:
 * @property OrderAddress $address
 * @property AuthUser $user
 * @property OrderCheck[] $orderChecks
 * @property Currency $Currency
 *
 * @author     Sviatoslav Danylenko <dev@udf.su>
 * @package    cms.models.order
 */

class Order extends CmsActiveRecord
{
    // Варианты оплаты заказа
    // @see getOrderCurrency()
    protected $_orderCurrency = array(
        1 => 'Оплата наличными',
        2 => 'WMR',
        3 => 'WMZ',
        4 => 'WMU',
        5 => 'WME',
        6 => 'WMK',
        7 => 'BTC',
        8 => 'Безналичный расчет',
        9 => 'PayPal',
        10=> 'Visa: USD',
        11=> 'Visa: RUB',
        12=> 'Visa: EUR',
        13=> 'Visa: KZT',
        14=> 'Visa: UAH',
        15=> 'MasterCard: USD',
        16=> 'MasterCard: RUB',
        17=> 'MasterCard: EUR',
        18=> 'MasterCard: KZT',
        19=> 'MasterCard: UAH',
    );

    protected static $_currencyIdList;

    const NOT_COMPLETE = 1;
    const IN_PROCESS = 2;
    const COMPLETE = 3;
    const CANCELED = 4;
    const APPROVED = 5;
    const SENT_TO_DELIV = 6;
    const DELIVERING = 7;
    const RETURNED = 8;

    public $orderStatus = array(
        self::NOT_COMPLETE => 'Новый', // заказ получен и его еще никто не трогал
        self::APPROVED => 'Подтвержден', // заказ прозвонен, сверен адрес(проставляется коллменеджером)
        self::IN_PROCESS => 'Обработка', // TODO: заморозить товар;
        self::SENT_TO_DELIV => 'Передан сл. доставки', //
        self::DELIVERING => 'В пути', // менеджер получил штрих-код/инфу/цену от сл. доставки
        self::COMPLETE => 'Выполнено', // заказ оплачен, получены деньги
        self::RETURNED => 'Возврат', // товар вернулся к продавцу на нашеотделение.
        self::CANCELED => 'Отмена', // заказ не подтвержден коллменеджером или отменен заказчиком
    );

    // массив с id статусов по которым надо фильтровать с помощью search();
    public $statusArr;
    public $filterDateFrom;
    public $filterDateTo;
    public $gridSelectedItems;
    public $orderSearch;

    protected $_oldStatus;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{order}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, currency_id, currency_provider', 'required'),
            array('status, type, currency_provider', 'numerical', 'integerOnly' => true),
            array('type, status, currency_provider', 'default', 'value' => 1, 'setOnEmpty' => true),
            array('comment, user_id, order_id, api_client', 'safe'),
            array('referer', 'length', 'max' => 255),
            array('currency_id', 'length', 'max' => 3),
            array('currency_id', 'unsafe', 'on' => 'update'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, user_id, address_id, status, type, ip, date, statusArr, orderSearch, filterDateFrom, filterDateTo,order_id, api_client', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Залогениным юзерам даем доступ только к их
     * материалам или только к материалам на их сайтах
     * @return array критерий поиска
     */
    public function defaultScope()
    {
        $criteria = array();

        if (Yii::app()->id == 'satellite' && !Yii::app()->user->isGuest && !Yii::app()->user->isAdmin) {
            // TODO: убрать этот костыль
            $cacheId = 'user-sites-criteria-' . Yii::app()->user->id;
            if (!($condition = Yii::app()->cache->get($cacheId))) {
                $userSites = User::model()->findByPk(Yii::app()->user->id)->sites;
                $userSites = CHtml::listData($userSites, 'cleanUrl', 'cleanUrl');

                $condition = 'referer IN ("' . implode('","', $userSites) . '")';

                Yii::app()->cache->set($cacheId, $condition, 60);
            }

            $criteria = array(
                'condition' => $condition,
            );
        }

        return $criteria;
    }

    /**
     *  Преобразуем ip в адекватный формат
     */
    protected function afterFind()
    {
        $this->ip = long2ip($this->ip);
        $this->_oldStatus = $this->status;
    }

    /**
     *  Сохраняем ip юзера
     *  Обновляем даты
     */
    protected function beforeSave()
    {
        if (parent::beforeSave()) {

            if ($this->isNewRecord) {
                $this->date = new CDbExpression('NOW()');

                if (empty($this->status)) {
                    $this->status = self::NOT_COMPLETE;
                }
                $t = Currency::model()->find('id=:ID', array(':ID'=>1));
                $this->referer = trim(str_replace('http:', '', $this->referer), '/');
                //begin_upgrade 16.09.2015 for api wertex master=labunets
                // Yii::log("POSY NATA = ".print_r($_POST,true));
                    if(!empty($_POST) ){
                        //ToDo сделать проверку на вертекс
                        $api = $this->processApi($_POST);
                        if($api['order_id']!=''){
                          $this->order_id = $api['order_id'];
                          $this->api_client = $api['api_client'];
                        }
                        YII::log("test ADD");

                        $adinfo = array(
                            'by-bianshi.adffi.com' =>'bel',
                            'medicalbracelet.adffi.com' =>'rus',
                            'kz-bianshi.adffi.com' =>'kaz',
                            'monastery-diabet.adffi.com' => 'rus',
                            'monastery-diabet-kz.adffi.com' => 'kaz',
                            'by-monastery-diabet.adffi.com' => 'bel',
                            'ua-monastery-diabet.adffi.com' => 'ukr',
                            'ua-urological.adffi.com' => 'ukr',
                            'by-urological.adffi.com' => 'bel',
                            'kz-urological.adffi.com' => 'kaz',
                            'urological.adffi.com' => 'rus',
                            'mastopathy.adffi.com' => 'rus',
                            'hemorrhoids.adffi.com' => 'rus',
                            'diabet.adffi.com' => 'rus',
                            'ru-pohudenie.adffi.com' => 'rus'
                        );
                        if( array_key_exists ( $this->referer , $adinfo )){
                            $this->order_id = $this->apiIntegrationAdinfo($_POST,$adinfo[$this->referer]);
                            $this->api_client =MD5('adinfo.assistance@gmail.com');
                        }
                        //test for offers.pro
                        $goods = array(
                           178354 =>'zerosmoke.adffi.com',
                           176020 => 'volcanic-kz.adffi.com',
                           162226 => 'volcanic.adffi.com',
                           160662 => 'gt08.adffi.com',
                           165724 => 'gt08-kz.adffi.com',
                           175508 => 'vhero.adffi.com',
                           171586 => 'kors.adffi.com',
                           151708 => 'hub.adffi.com',
                           170149 => 'hub-kz.adffi.com',
                           151921 => 'sky.adffi.com',
                           151710 => 'swissarmy.adffi.com',
                           155659 => 'swissarmy-kz.adffi.com',
                        );
                        if(strpos($this->referer,"-kz")===false){
                            if(array_search($this->referer,$goods)){
                                $user_api_key = "b5a2ba3d-405b-4703-8adc-1c1e79fa92cf";
                                $customer_api_key = "111babf2-663a-42d4-8899-a07784e5906e";
                                $url= "https://api.e-autopay.com/v02/$user_api_key/orders";
                                Yii::LOG("BEGIN DEBAG offers.pro");
                                $this->integrationJsonApi($customer_api_key,$url,$_POST,array_search($this->referer,$goods),4);
                            }
                        }else{
                            if(array_search($this->referer,$goods)){
                                $user_api_key = "b5a2ba3d-405b-4703-8adc-1c1e79fa92cf";
                                $customer_api_key = "111babf2-663a-42d4-8899-a07784e5906e";
                                $url= "https://api.e-autopay.com/v02/$user_api_key/orders";
                                Yii::LOG("BEGIN DEBAG offers.pro");
                                $this->integrationJsonApi($customer_api_key,$url,$_POST,array_search($this->referer,$goods),5);
                            }
                        }
                        
                      // Проверка на leadvertexML
                        $lends = array(
                            1=>'monastyrchai.adffi.com',
                            2=>'sharkoil.adffi.com',
                            3=>'akula.adffi.com',
                            4=>'andrea-hair.adffi.com',
                            5=>'acai.adffi.com',
                            6=>'chocoslim.adffi.com',
                            7=>'devis.adffi.com',
                            8=>'goji.adffi.com',
                            9=>'carrot.adffi.com',
                            10=>'nightkashtan.adffi.com',
                            11=>'soya.adffi.com'                         
                            );
                        //if($this->referer == "monastyrchai.adffi.com"){
                        if(array_search($this->referer,$lends)){
                            $url="https://ml.leadvertex.ru/api/webmaster/v2/addOrder.html";
                            $client='leadvertexML';
                            $integration = $this->integrationApi($_POST,$url,$client);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        
                        // Проверка на leadvertex
                        $lendsA = array(
                            1=>'tvoi-ochki.adffi.com',
                            );
                        if(array_search($this->referer,$lendsA)){
                          $url="https://adlens.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexAdlens';
                          $integration = $this->integrationApi($_POST,$url,$client);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }

                        // Проверка на leadvertex
                        $lendsB = array(
                            //1=>'tvoi-ochki.adffi.com',
                            1 => 'ulysse-time15.adffi.com',
                            2 => 'spacex-time15.adffi.com',
                            3 => 'hublot-time15.adffi.com',
                            4 => 'rolex-time15.adffi.com',
                            5 => 'curren-time15.adffi.com',
                            6 => 'tissot-time15.adffi.com',
                            7 => 'rado-time15.adffi.com',
                            8 => 'swiss-time15.adffi.com',
                            9 => 'breitling-time15.adffi.com',
                            );
                        if(array_search($this->referer,$lendsB)){
                          $url="https://black-des.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexBlack-des';
                          $integration = $this->integrationApi($_POST,$url,$client);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        // Проверка на ElitBrand
                        $lendsE = array(
                            //1=>'tvoi-ochki.adffi.com',
                            1 => 'ulysse.adffi.com',
                            2 => 'skymoon.adffi.com',
                            3 => 'carrera-space.adffi.com',
                            4 => 'iphone6s.adffi.com',
                        );
                        if(array_search($this->referer,$lendsE)){
                          $url="http://elitbrand.com/api/proffi/submit";
                          $client='ElitBrand';
                          $integration = $this->integrationApi($_POST,$url,$client);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }


                        // Проверка на leadvertexML

                        if($this->referer == "carrera.adffi.com"){

                          $url="https://grandcarrera.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexGrandcarrera';
                          $integration = $this->integrationApi($_POST,$url,$client);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        /*02.12.2015*/
                        if($this->referer == "andrea-ru.adffi.com"){

                          $url="https://andrea.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexAndrea';
                          $webmasterID =68;
                          $token="Qazxsw21";
                          $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        $jarkovaOffers = array(
                            'kz-airwatches.adffi.com' =>'kz',
                            'ru-avrora.adffi.com' =>'rus',
                            'ru-vhero.adffi.com' =>'rus',
                            'ru-vulkan-mylo.adffi.com' => 'rus'
                        );
                        if( array_key_exists ( $this->referer , $jarkovaOffers )){

                            $url="https://tovarka.leadvertex.ru/api/webmaster/v2/addOrder.html";
                            $client='LeadVertexTovarka';
                            $webmasterID =13;
                            $token="Qazxsw21";
                            $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }
                        }
                        if($this->referer == "ru-airwatches.adffi.com"){

                            $url="https://tovarka.leadvertex.ru/api/webmaster/v2/addOrder.html";
                            $client='LeadVertexTovarka';
                            $webmasterID =13;
                            $token="Qazxsw21";
                            $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }
                        }

                        if($this->referer == "shokolight.adffi.com"){

                          $url="https://chokolight.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexShoko';
                          $webmasterID =68;
                          $token="Qazxsw21";
                          $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        if($this->referer == "gassaver.adffi.com"){

                          $url="https://gassave.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexGassaver';
                          $webmasterID =68;
                          $token="Qazxsw21";
                          $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        if(($this->referer == "oj.adffi.com") or ($this->referer == "oj-m.adffi.com")){/*+моб*/

                          $url="https://oj.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexOj';
                          $webmasterID =68;
                          $token="Qazxsw21";
                          $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        if($this->referer == "vagusstop.adffi.com"){

                          $url="https://valgusstop.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexVagus';
                          $webmasterID =68;
                          $token="Qazxsw21";
                          $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        if($this->referer == "kors-kz.adffi.com"){

                          $url="https://kors.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexKors';
                          $webmasterID =1;
                          $token="Qazxsw21";
                          $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }
                        }
                        if($this->referer == "kors-rf.adffi.com"){

                          $url="https://korsrussia.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexKorsRus';
                          $webmasterID =1;
                          $token="Qazxsw122";
                          $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }
                        }

                        if($this->referer == "cofeescrub.adffi.com"){

                          $url="https://bodyblendz.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexCofee';
                          $webmasterID =68;
                          $token="Qazxsw21";
                          $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        if($this->referer == "easynodrink.adffi.com "){

                          $url="https://easynodrink.leadvertex.ru/api/webmaster/v2/addOrder.html";
                          $client='LeadVertexEasynodrink';
                          $webmasterID =68;
                          $token="Qazxsw21";
                          $integration = $this->integrationApiLead($_POST,$url,$client,$webmasterID,$token);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client = $client;
                            }  
                        }
                        
                        /*/02.12.2015/*/
                         // Проверка на LuckyShop
                        if($this->referer == "bottle.adffi.com" 
                            or $this->referer == "luxuryeye.adffi.com"
                            or $this->referer == "walletbest.adffi.com"
                            or $this->referer == "smartwatch.adffi.com"
                            or $this->referer == "kuril-tea.adffi.com"
                            or $this->referer == "keys.adffi.com"
                            or $this->referer == "mitenki.adffi.com"
                            or $this->referer == "sensor-gloves.adffi.com"
                            or $this->referer == "swarovskistyle.adffi.com"
                            ){
                            $url="http://advertlead.ru/api/private/new_lead_v2.php";
                            $client='LuckyShop';
                            $integration = $this->integrationApi($_POST,$url,$client);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client =$client;
                            }  
                        }

                        //russianint
                         $lendsR = array(
                            1=>'scholl.adffi.com',
                            2=>'flashdrive.adffi.com',
                            3=>'baelerry-italia.adffi.com',
                                      
                            );
                        if(array_search($this->referer,$lendsR)){
                            $url="http://personal.russianpostservice.com/receive/proffi/orders";
                            $client='RussianP';
                            $integration = $this->integrationApi($_POST,$url,$client);
                            if($integration){
                                $this->order_id = $integration;
                                $this->api_client =$client;
                            }  
                        }
                    }
                // //end_upgrade
            }

            $this->ip = ip2long($this->ip);
        
            if (!$this->canChangeStatus) {
                $this->status = $this->_oldStatus;
            }

            return true;
        } else {
            return false;
        }
    }
//begin_upgrade 16.09.2015 for api wertex master=labunets
    public function getProductPrice($pr_id,$currency_id=4){
        $productData = ShopProductData::model()->findByAttributes(array(
                'product_id' => $pr_id,
                'currency_id' => $currency_id,
            ));
        Yii::log("PRICE = ".print_r($productData->attributes,true));
        Yii::log("id product = ".$pr_id);

        return $productData->attributes['default_price']; 
    }

    public function getProductInfoR($products){

       
        $pr = CatalogObject::model()->findAllByAttributes(array(
            'object_id' => $products,
            'object_type' => 'catalog',
        ));
        $return_product = $pr[0]->object_slug;
        return $return_product;

        
    }
    public function getProductInfoName($products){


        $pr = CatalogObject::model()->findAllByAttributes(array(
            'object_id' => $products,
            'object_type' => 'catalog',
        ));
        $return_product = $pr[0]->object_name;
        return $return_product;


    }
    /**
     * Возврат кодов для товаров и их количества
     */
    public function getProductInfo($products,$type){
        $return_product ='';
        $return_count ='';
       
        if(!empty($products)){
            foreach ($products as $product => $count) {
            //    YII::log("data product name id = ".$product);
                $pr = CatalogObject::model()->findAllByAttributes(array(
                    'object_id' => $product,
                    'object_type' => 'catalog',
                ));
                if($return_product==='')
                    $return_product = $pr[0]->object_title;
                else
                    $return_product = $return_product.','.$pr[0]->object_title;
                if($return_count==='')
                    $return_count = $count;
                else
                    $return_count = $return_count.','.$count;
            }
            switch ($type) {
                case 'product':
                    return $return_product;
                    break;
                case 'count':
                    return $return_count;
                    break;
            }
        }
        
    }
    public function integrationJsonApi($key,$url,$data,$good_id,$currency_id){
        $fio = (isset($data['orderData']['name'])) ? $data['orderData']['name']:"";
        $phone = (isset($data['orderData']['phone'])) ? $data['orderData']['phone']: "";
        $product =$this->getProductPrice($data['orderData']['products'][0]['id'],$currency_id); //переопределить
        $good_price = (isset($product)) ? $product :"";
        Yii::log("oredr  N = ".print_r($data, true));
        $cur = array(
            1 => 'грн',
            2 => 'долар',
            3 => 'евро',
            4 => 'руб',
            5 => 'тенге',
        );
        //$good_id = 175508;


        $postData = array(
            "customer_api_key" => "$key",
            "orders" =>array( array(
                "customer" => array(
                    "surname" => "$fio",
                    "given_name" => "nodata",
                    "patronymic" => "nodata",
                    "country" => "nodata",
                    "state" => "nodata",
                    "city" => "nodata",
                    "zip" => "nodata",
                    "address" => "nodata",
                    "email" => "nodata",
                    "phone" =>"$phone",
                ),
                "credentials" => array(
                    "created" => date('Y-m-d h:i:s'),
                    "currency" => $cur[$currency_id],
                    "amount" => "1",
                    "delivery_cost" => "0",
                    "notes" =>"",
                ),
                "basket" => array(
                    array(
                    //    "good_id"=>175508,
                        "good_id" =>$good_id,
                        "cost" => $good_price,
                        "quantity"=>1,
                    ),
                ),
            )),
        );
        $ch = curl_init("$url");
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER=> array(
                'Content-Type: application/json',
                ),
            CURLOPT_POSTFIELDS => json_encode($postData)
            )
        );
        // YII::log("data send = ".print_r(json_encode($postData),true));
        $response = curl_exec($ch);
        if($response ===FALSE){
            Yii::log("Nat curl offers.pro error");
            return false;
        }
        $responseData = json_decode($response,true);
        YII::log("data answer = ".print_r($responseData,true));
        
        $this->order_id = $responseData["orders"][0]["order_id"];
        $this->api_client = "offers.pro";
        
       
        return true;
    }

    public function integrationApiLead($data,$url,$client,$webmasterID,$token){
        $url .=  "?webmasterID=".$webmasterID."&token=".$token;
        $dataPost="";
        $dataPost .= (isset($data['orderData']['name'])) ? "fio=".$data['orderData']['name']:"";
        $dataPost .= (isset($data['orderData']['phone'])) ? "&phone=".$data['orderData']['phone']: "";
        $price =$this->getProductPrice($data['orderData']['products'][0]['id']); //переопределить
        $dataPost .= (isset($price)) ? "&price=".$price :"";
        $product = $this->getProductInfoName($data['orderData']['products'][0]['id']);
        $dataPost .= (isset($product)) ? "&additional2=".$product :"";
//        $dataPost .= (isset($product)) ? "&comment=".$product :"";

        $return_data = $this->callCurl($dataPost,$url);
        $callback = $this->checkCurl($return_data);
        Yii::log("LeadVertex log = ".$dataPost.' - '.$url.print_r($return_data,true));
        if(isset($callback->OK)){
            return $callback->OK;
        }

    }
    public function integrationApi($data,$url,$client){
        if($client == 'leadvertexML'){ 
            $url .= "?webmasterID=2&token=Wsxcde34";
            $dataPost="";
            if(isset($_POST['orderData'])){
                $dataPost .= (isset($data['orderData']['name'])) ? "fio=".$data['orderData']['name']:"";
                $dataPost .= (isset($data['orderData']['phone'])) ? "&phone=".$data['orderData']['phone']: "";
                $price =$this->getProductPrice($data['orderData']['products'][0]['id']); //переопределить
                $dataPost .= (isset($products)) ? "&price=".$price :"";
            }else{
                $dataPost .= (isset($data['Order']['totalPrice'])) ? "total=".$data['Order']['totalPrice']: "";
                $dataPost .= "&fio=";
                $dataPost .= (isset($data['Client']['last_name'])) ? $data['Client']['last_name']: "";
                $dataPost .= (isset($data['Client']['first_name'])) ? " ".$data['Client']['first_name']: "";
                $dataPost .= (isset($data['Client']['email'])) ? "&email=".$data['Client']['email']: "";
                $dataPost .= (isset($data['Order']['comment'])) ? "&comment=".$data['Order']['comment']: "";
                $dataPost .= (isset($data['OrderAddress']['telephone'])) ? "&phone=".$data['OrderAddress']['telephone']: "";
                $dataPost .= (isset($data['OrderCheck'])) ? "&quantity=".$this->getProductInfo($data['OrderCheck'],'count'): "";
            }
            $return_data = $this->callCurl($dataPost,$url);
            $callback = $this->checkCurl($return_data);
        //    Yii::log("NATA calllback = ".$url." - ".$dataPost.print_r($callback,true));
            if(isset($callback->OK)){
                return $callback->OK;
            }
        }
        if($client == 'LeadVertexBlack-des'){
            $url .= "?webmasterID=51&token=Qazxsw21";
            $dataPost="";
            if(isset($_POST['orderData'])){
                $dataPost .= (isset($data['orderData']['name'])) ? "fio=".$data['orderData']['name']:"";
                $dataPost .= (isset($data['orderData']['phone'])) ? "&phone=".$data['orderData']['phone']: "";
                $price =$this->getProductPrice($data['orderData']['products'][0]['id']); //переопределить
                $dataPost .= (isset($price)) ? "&price=".$price :"";
            }
            $return_data = $this->callCurl($dataPost,$url);
            $callback = $this->checkCurl($return_data);
            if(isset($callback->OK)){
                return $callback->OK;
            }
        }

        if($client == 'LeadVertexAdlens'){
            $url .= "?webmasterID=3&token=Qazxsw21";
            $dataPost="";
            if(isset($_POST['orderData'])){
                $dataPost .= (isset($data['orderData']['name'])) ? "fio=".$data['orderData']['name']:"";
                $dataPost .= (isset($data['orderData']['phone'])) ? "&phone=".$data['orderData']['phone']: "";
                $price =$this->getProductPrice($data['orderData']['products'][0]['id']); //переопределить
                $dataPost .= (isset($price)) ? "&price=".$price :"";
            }
            $return_data = $this->callCurl($dataPost,$url);
            $callback = $this->checkCurl($return_data);
            if(isset($callback->OK)){
                return $callback->OK;
            }
        }
        if($client == 'ElitBrand'){
            $dataPost="item_id=122&utm_source=proffi&charset=utf8&utm_content=iphone6s_proffi";
            if(isset($_POST['orderData'])){
                $dataPost .= (isset($data['orderData']['name'])) ? "&name=".$data['orderData']['name']:"";
                $dataPost .= (isset($data['orderData']['phone'])) ? "&phone=".$data['orderData']['phone']: "";
            }
            $return_data = $this->callCurl($dataPost,$url);
            $callback = $this->checkCurl($return_data);
            if(isset($callback->result)){
                return $callback->result;
            }
        }
        
        if($client == 'LeadVertexGrandcarrera'){ 

            $url .= "?webmasterID=1&token=dFrku567g";
            $dataPost="";
            if(isset($_POST['orderData'])){
                $dataPost .= (isset($data['orderData']['name'])) ? "fio=".$data['orderData']['name']: "";
                $dataPost .= (isset($data['orderData']['phone'])) ? "&phone=".$data['orderData']['phone']: "";
                $price =$this->getProductPrice($data['orderData']['products'][0]['id']); //переопределить
                $dataPost .= (isset($price)) ? "&price=".$price :"";
            }else{
                $dataPost .= (isset($data['Order']['totalPrice'])) ? "total=".$data['Order']['totalPrice']: "";
                $dataPost .= "&fio=";
                $dataPost .= (isset($data['Client']['last_name'])) ? $data['Client']['last_name']: "";
                $dataPost .= (isset($data['Client']['first_name'])) ? " ".$data['Client']['first_name']: "";
                $dataPost .= (isset($data['Client']['email'])) ? "&email=".$data['Client']['email']: "";
                $dataPost .= (isset($data['Order']['comment'])) ? "&comment=".$data['Order']['comment']: "";
                $dataPost .= (isset($data['OrderAddress']['telephone'])) ? "&phone=".$data['OrderAddress']['telephone']: "";
                $dataPost .= (isset($data['OrderCheck'])) ? "&quantity=".$this->getProductInfo($data['OrderCheck'],'count'): "";
            }
            $return_data = $this->callCurl($dataPost,$url);
            $callback = $this->checkCurl($return_data);
            Yii::log("NATA calllback = ".$url." - ".$dataPost.print_r($callback,true));
            if(isset($callback->OK)){
                return $callback->OK;
            }
        }
        if($client == 'RussianP'){
            $model = Order::model()->find(array(
                          'select' => 'MAX(id) id',
                        ));
            $id = 1+ $model->id;
            $fio = (isset($data['orderData']['name'])) ? $data['orderData']['name']: "";
            $phone = (isset($data['orderData']['phone'])) ? $data['orderData']['phone']: "";
            $id_product = $data['orderData']['products'][0]['id'];
            $product = $this->getProductInfoR($data['orderData']['products'][0]['id']);
            $price =$this->getProductPrice($data['orderData']['products'][0]['id']); //переопределить
           
            $dataPost = "name=$fio&order_id=$id&price=$price&phone=$phone&product_name=$product&key=RZzjbEJmIg2819";
            $return_data = $this->callCurl($dataPost,$url);
            $callback = $this->checkCurl($return_data);
        //    Yii::log("NATA calllback = ".$url." - ".$dataPost.print_r($callback,true));
            $ch = curl_init("$url");
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER=> array(
                    'Content-Type: application/x-www-form-urlencoded',
                    ),
                CURLOPT_POSTFIELDS => $dataPost
                )
            );
            // YII::log("data send = ".print_r(json_encode($postData),true));
            $response = curl_exec($ch);
            if($response ===FALSE){
                Yii::log("Nat curl russian error");
                return false;
            }
            $responseData = json_decode($response,true);
            YII::log("data answer = ".print_r($responseData,true));
            if($responseData["result"]!="false")
                return $responseData["result"];
            
            // if(isset($callback->ID_ORDER)){
            //     return $callback->ID_ORDER;
            // }
        }
        if($client == 'LuckyShop'){
            $dataPost="api_key=OspzKPSGNyLwJxCrQrbweNZbVxsNBeFoWTmGkhIMfGIqiAmxooIWQaCjqhNL";
            //выбор оффера
            if($this->referer == "bottle.adffi.com")
                $id_offer = "lMVtmjASCCpJtaCOrnvQVIFCbgEmGtXpAwaMTTPFcvxLpHELhs";
            if($this->referer == "luxuryeye.adffi.com")
                $id_offer = "tfCtFRkWOhCMlNbHXReeiqdfrvEDshIjnFydmKHZhUizeHxtgY";
            if($this->referer == "walletbest.adffi.com")
                $id_offer = "RolkpODXdAtRvNeaAiakRIZvMWrOikPnTVosHvurxuriEaxPLk";
            if($this->referer == "kuril-tea.adffi.com")
                $id_offer = "glrIQoCmRyZGZSYmFBTDYHiskKPcwuDLqDVZBQsiviZZVsgiLu";
            if($this->referer == "smartwatch.adffi.com")
                $id_offer = "SBOMbFguJknxnNoNnFBqZkTvFhXUOOdOaPrdeSMgCzUltQYeDV";
            if($this->referer == "keys.adffi.com")
                $id_offer = "TDVBhuDXrEPgRMygueVfhoBwQIVKyxkckpxbwpURKwsAkLYMFN";
            if($this->referer == "mitenki.adffi.com")
                $id_offer = "KcDCWqUSJSGAXfIrbvilVFsZgKyWhzWLSnTkhaHuvlglXWznul";
            if($this->referer == "sensor-gloves.adffi.com")
                $id_offer = "tKoBKhOhlanuTIvOMzZrTpWZBRuadkISJYTbbiwvktTFKHzsMK";
            if($this->referer == "swarovskistyle.adffi.com")
                $id_offer = "zRrbLYeuSFqskiuYaiRlEeeFoOsaXqmttFftEyVKPELTSiTYCV";
            $dataPost .= "&id_offer=".$id_offer;
            $model = Order::model()->find(array(
                          'select' => 'MAX(id) id',
                        ));
            $id = 1+ $model->id;
            $dataPost .=  "&click_id=".$id;
            $dataPost .= (isset($data['orderData']['name'])) ? "&name=".$data['orderData']['name']: "";
            $dataPost .= (isset($data['orderData']['phone'])) ? "&phone=".$data['orderData']['phone']: "";
            $product = $this->getProductInfoR($data['orderData']['products'][0]['id']);
            $dataPost .= (isset($products)) ? "&label_case=".$product :"";

            $return_data = $this->callCurl($dataPost,$url);
            $callback = $this->checkCurl($return_data);
        //    Yii::log("NATA calllback = ".$url." - ".$dataPost.print_r($callback,true));
            if(isset($callback->ID_ORDER)){
                return $callback->ID_ORDER;
            }
            
        }
        
        return false;
    }
    /**
     * Процесс связки API, создание заказа у Adinfo
     */
    protected function apiIntegrationAdinfo($dataPost,$geo){
        //данные
        $products = array(
            'by-bianshi.adffi.com' =>147,
            'medicalbracelet.adffi.com' =>147,
            'kz-bianshi.adffi.com' =>147,
            'monastery-diabet.adffi.com' => 92,
            'monastery-diabet-kz.adffi.com' => 92,
            'by-monastery-diabet.adffi.com' => 92,
            'ua-monastery-diabet.adffi.com' => 92,
            'ua-urological.adffi.com' => 107,
            'by-urological.adffi.com' => 107,
            'kz-urological.adffi.com' => 107,
            'urological.adffi.com' => 107,
            'mastopathy.adffi.com' => 124,
            'hemorrhoids.adffi.com' => 113,
            'diabet.adffi.com' => 187,
            'ru-pohudenie.adffi.com' => 128
        );
        $fio = (isset($dataPost['orderData']['name'])) ? $dataPost['orderData']['name']: "";
        $phone = (isset($dataPost['orderData']['phone'])) ? $dataPost['orderData']['phone']: "";
        $ip = (isset($dataPost['orderData']['ip'])) ? $dataPost['orderData']['ip']: '127.0.0.1';
        $index = (isset($dataPost['orderData']['zip'])) ? $dataPost['orderData']['zip']: '';
        $city = (isset($dataPost['orderData']['city'])) ? $dataPost['orderData']['city']: 'нет данных';
        $street= (isset($dataPost['orderData']['street'])) ? $dataPost['orderData']['street']: '';
        $flat = (isset($dataPost['orderData']['flat'])) ? $dataPost['orderData']['flat']: '';
        $adress = $index.' '.$city.' '.$street.' '.$flat;
        $id_product = $products[$this->referer];
        $order = array(
            'id_tovar' => $id_product,
            'name' 	   => $fio,
            'ip'   	   => $ip,
            'country'  => 'country_'.$geo,
            'address'  => $adress,
            'tel'	   => $phone,
            'group_id' => 'group_test',
            'sub_id'   => 'test'
        );
        $data = array('command' => 'add_order','order' => $order);
        Yii::log("DATA ADINFO: ".print_r($data, true));
        $res = $this->SendAdinfo('proffi','iVR8oPcRp7xd6uxsKfl6',$data);
        Yii::log("ANSWER ADINFO: ".print_r($res, true));
        if(isset($res['id_order']) && $res['id_order'])
            return $res['id_order'];
    }

    private function sendAdinfo($login,$PassApikey,$data){
        //инициализируем сеанс
        $curl = curl_init();
        //уcтанавливаем урл, к которому обратимся
        curl_setopt($curl, CURLOPT_URL, 'http://adinfo.ru/api/');
        //выключаем вывод заголовков
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //передаем данные по методу post
        curl_setopt($curl, CURLOPT_POST, 1);
        //теперь curl вернет нам ответ, а не выведет
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //переменные, которые будут переданные по методу post
        curl_setopt($curl, CURLOPT_POSTFIELDS, 'login='.urlencode($login).'&apikey='.urlencode($PassApikey).'&data='.json_encode($data));
        $res = curl_exec($curl);
        //проверяем, если ошибка, то получаем номер и формируем сообщение
        if(!$res)$res = '{"errtext": "'.curl_error($curl).'('.curl_errno($curl).')"}';
        curl_close($curl);
        return (array)json_decode($res); // возращаем результат
    }

    /**
     * Процесс связки API, создание заказа у Reworker
     */
    public function processApi($data){
        $url_send = "http://www.reworker.ru/clients/api/lead_postback.php";
        $url_check = "http://reworker.ru/clients/api/lead_status.php";
        $api['order_id']='';
        $api['api_client']='ReWorker';
        $api['status_id']='';
        $apiStatus = array(
            "processing"=>1,
            "approved"=>5,
            "declined"=>4,
            "shipped"=>6,
            "delivered"=>7,
            "paid"=>3,
            "return"=>8,
        );
       // $newRequest = true;
        Yii::log("POSY_NATA_API_TESTS befor goto = ".print_r($data,true));
        if(isset($_POST['OrderCheck'])){
            $price = $data['Order']['totalPrice'];
            $count = $this->getProductInfo($data['OrderCheck'],'count');
            $product = $this->getProductInfo($data['OrderCheck'],'product');
            $fio = $data['Client']['last_name'].' '.$data['Client']['first_name'];
            $email = $data['Client']['email'];
            $phone = $data['OrderAddress']['telephone'];
            $data_post = "reworker_client_id=100&reworker_shop_id=271&order_product_count=".$count
            ."&order_product_price=".$price."&order_product_sku=".$product
            ."&order_name=".$fio."&order_email=".$email."&order_phone=".$phone."&order_source=proffi";
            $data_check = "reworker_client_id=100&order_ids=";
        }
        if(isset($_POST['orderData'])){
            $price =$this->getProductPrice($data['orderData']['products'][0]['id']); //переопределить
            $count = 1;
            $product = $this->getProductInfoR($data['orderData']['products'][0]['id']);
            $fio = $data['orderData']['name'];
            $phone = $data['orderData']['phone'];
            $data_post = "reworker_client_id=100&reworker_shop_id=271&order_product_count=".$count
            ."&order_product_price=".$price."&order_product_sku=".$product
            ."&order_name=".$fio."&order_phone=".$phone."&order_source=proffi";
            $data_check = "reworker_client_id=100&order_ids=";
        }
    
        Yii::log("POSY_NATA_API = ".$data_post);
        //создания записи и возврат статуса
        $return_data = $this->callCurl($data_post,$url_send);
        $callback = $this->checkCurl($return_data);
        if($callback->result=="success"){
            $return_status = $this->callCurl($data_check.$callback->order_id,$url_check);
            $callback = $this->checkCurl($return_status);
        //    Yii::log("ANSWERANSWER ANT ANTA = ".print_r($callback[0]->order_id,true));
            $api['status_id'] = $apiStatus[$callback[0]->order_status];
            $api['order_id'] = $callback[0]->order_id;
               
        }

   
        return $api;
    }
    /**
     * проверка статуса
     */
    public function checkStatus(){
        $data_check = 'reworker_client_id=100&cpa_access=r$wtr1fprtn7&comments&order_ids=';
        $url_check = "http://www.reworker.ru/clients/api/lead_status.php";

        $apiStatus = array(
            "processing"=>1,
            "approved"=>5,
            "declined"=>4,
            "shipped"=>6,
            "delivered"=>7,
            "paid"=>3,
            "return"=>8,
        );
        /*TODO повыносить в переменные класа*/

        $return_data = $this->callCurl($data_check.$this->order_id,$url_check);
        $callback = $this->checkCurl($return_data);
       // $api = $apiStatus[$callback[0]->order_status];
        $api['status_id'] = $apiStatus[$callback[0]->order_status];
        if(isset($callback[0]->decline_reason))
            $api['comment'] = 'Причина: '.$callback[0]->decline_reason;
        else
            $api['comment'] = ' ~ ';
        $api['info'] = $callback;
        return $api;
    
    }
    /**
     * Проверка ответа от API
     */
    public function checkCurl($return_data){
        if ($return_data['chleadapierr']) {
               Yii::log("Error Error code: ".$return_data['chleadapierr']." Error message: ".$return_data['chleaderrmsg']);
            }
        $callback = json_decode($return_data['chleadresult']);   
        return $callback;   
    }
    /**
     * конект к стороним и получение ответа
     */
     public function callCurl($data_post,$url){
        $chlead = curl_init();
        curl_setopt($chlead, CURLOPT_URL, $url);
        curl_setopt($chlead, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0');
        curl_setopt($chlead, CURLOPT_VERBOSE, 1);
        curl_setopt($chlead, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chlead, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($chlead, CURLOPT_POSTFIELDS, $data_post);
        curl_setopt($chlead, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($chlead, CURLOPT_SSL_VERIFYHOST, false);

        $chleadresult = curl_exec($chlead);
        $chleadapierr = curl_errno($chlead);
        $chleaderrmsg = curl_error($chlead);
        $return_data =  array('chleadresult' => $chleadresult,'chleadapierr'=>$chleadapierr, 'chleaderrmsg'=>$chleaderrmsg);
        return $return_data;

    }
//end_upgrade
    protected function getCanChangeStatus()
    {
        return !in_array($this->_oldStatus, array(Order::COMPLETE, Order::CANCELED, Order::RETURNED));
    }

    protected function afterSave()
    {
    
        if ($this->isOrderJustCompleted) {
            $this->updateWarehouse();
        }

    //    $this->syncWithAffiliateSystem();
        //синхронизируем с новой cpa
        $this->syncWithCpaSystem();

        $this->updateNotifications();

        $this->_oldStatus = $this->status;
       
        return parent::afterSave();

    }

    /**
     * Обновляет статус заказа в аффилиатной системе
     */
    public function syncWithAffiliateSystem()
    {
        if ($this->isStatusChanged) {

            if ($this->status == self::CANCELED) {
                if(!Yii::app()->controller->module->affiliate->declineOrder($this->id))
                  $this->status = $this->_oldStatus;  

            } elseif ($this->_oldStatus == self::NOT_COMPLETE) {
                if(!Yii::app()->controller->module->affiliate->approveOrder($this->id))
                  $this->status = $this->_oldStatus;
            }
        }
    }

    /**
     * Обновляет статус заказа в cpa системе
     */
    public function syncWithCpaSystem()
    {
        if ($this->isStatusChanged) {
            if((!empty($this->comment) and ($this->comment!='')))
                $comment = $this->comment;
            else
                $comment = "Не прислана причина отмены";
            if ($this->status == self::CANCELED) {
               Yii::log("cancel cpa");
               Yii::app()->controller->module->cpa->declineOrder($this->id,$this->referer,$comment);
              //TODO: отменить заказ/транзакцию

            } elseif ($this->status != self::IN_PROCESS) {
                if (($this->_oldStatus == self::NOT_COMPLETE) || ($this->_oldStatus == self::IN_PROCESS)) {
                    Yii::app()->controller->module->cpa->approveOrder($this->id, $this->referer, $comment);
                    //TODO: потвердить заказ/транзакцию
                }
            }
        }
    }

    public function getIsOrderJustCompleted()
    {
        return $this->status == Order::COMPLETE && $this->_oldStatus != Order::COMPLETE;
    }

    public function getIsStatusChanged()
    {
        return $this->_oldStatus != $this->status;
    }

    protected function updateWarehouse()
    {
        Yii::import(GxcHelpers::getTruePath('content_type.catalog.CatalogObject'));
        foreach ($this->check as $check) {
            $model = CatalogObject::model()->findByPk($check->prod_id);
            $model->product_count -= $check->quantity;
            $model->save();
        }
    }

    protected function updateNotifications()
    {
        $count = Order::model()->count(array('condition' => 'status=' . Order::NOT_COMPLETE));
        Yii::app()->notify->create('shopNewOrders', array(
            'counter' => $count,
            'title' => 'Новые заказы',
            'message' => 'У вас ' . $count . ' новых заказов',
            'updateByCategory' => true,
        ));
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'address' => array(self::BELONGS_TO, 'OrderAddress', 'address_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'client' => array(self::BELONGS_TO, 'Client', array('id' => 'order_id')),
            'currency' =>array(self::BELONGS_TO, 'Currency', 'currency_id'),
            // для большей универсальности добудем название primaryKey модели User
            // эта модель будет не очень активно использоваться, так что на производительность это не повлияется
            // особенно если учесть кеширование схем бд
            'operator' => array(self::BELONGS_TO, 'User', array('operator_id' => User::model()->tableSchema->primaryKey)),
            'check' => array(self::HAS_MANY, 'OrderCheck', 'order_id'),
            'payment' => array(self::BELONGS_TO, 'CurrencyProvider', 'currency_provider'),
        );
    }

    /**
     *  Возвращает ссылку на просмотр чека
     **/
    public function getViewUrl()
    {
        return Yii::app()->createUrl('beorder/check', array('id' => $this->id));
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'address_id' => 'address',
            'status' => 'Статус заказа',
            'currency_id' => 'Валюта',
            'currency_provider' => 'Способ оплаты',
            'type' => 'Доставка',
            'ip' => 'Ip',
            'referer' => 'Сайт-источник',
            'date' => 'Дата заказа',
            'operator_id' => 'Оператор',
            'comment' => 'Комментарий оператора',
            'order_id' => 'order_id',
            'api_client' => 'api_client',
        );
    }

    public function getPayMethod()
    {
        return $this->payment->name;
    }

    /**
     * Возвращает доступные валюты для использования в форме оформления заказа
     *
     * @access public
     * @return array
     */
    public function getOrderCurrency()
    {
        // TODO: настройки цмс!
        $currencyParams = isset(Yii::app()->modules['cart']['params']['emoney'])
        ? Yii::app()                      ->modules['cart']['params']['emoney']
        : array(
            'WM' => array(
                'active' => '1',
                'secretKey' => '',
                'purse' => 'U514759930779',
            ),
            'Privat24' => array(
                'active' => '1',
                'secretKey' => '',
                'purse' => 'UAH',
            ),
            'other' => array(
                0 => '1',
                1 => '8',
            ),
        );
        // прочие, не электронные, способы оплаты
        if (isset($currencyParams['other']) && is_array($currencyParams['other'])) {
            foreach ($currencyParams['other'] as $currencyId) {
                $enabledCurrencies[$currencyId] = $this->_orderCurrency[$currencyId];
            }
            unset($currencyParams['other']);
        }

        foreach ($currencyParams as $emoneyId => $emoney) {
            if ($emoney['active'] == true) {
                switch ($emoneyId) {
                    case 'WM':
                        $enabledCurrencies[4] = $this->_orderCurrency[4];
                        break;
                    case 'Privat24':
                        $enabledCurrencies[5] = $this->_orderCurrency[5];
                        break;
                }
            }
        }

        if (!isset($enabledCurrencies)) {
            throw new CException('Настройте хоть один способ оплаты в админкe');
        }

        return $enabledCurrencies;
    }

    /**
     * Возвращает доступные способы доставки
     *
     * @access public
     * @return array
     */
    public function getOrderType()
    {
        $typeParams = OrderType::model()->cache(5 * 60)->findAllByAttributes(array('enabled' => 1));

        if (!count($typeParams)) {
            Yii::app()->user->setFlash('error', 'Настройте хоть один способ доставки');
        }

        $enabledTypes = CHtml::listData($typeParams, 'id', 'name');

        return $enabledTypes;
    }

    /**
     * Возвращает способ доставки текущего заказа
     *
     * @access public
     * @return string
     */
    public function getOrderTypeString()
    {
        $typeParams = self::getOrderType();

        return isset($typeParams[$this->type]) ? $typeParams[$this->type] : 'undefined';
    }

    /**
     * @return array массив с id валют для выпадающих менюшек
     */
    public static function getCurrencyIdList()
    {
        if (!self::$_currencyIdList) {
            $path = Yii::getPathOfAlias('system.i18n.data.' . Yii::app()->locale->id) . '.php';
            if (!file_exists($path)) {
                $path = Yii::getPathOfAlias('system.i18n.data.ru_ru') . '.php';
            }

            $data = include ($path);

            $currencies = array_values(array_flip($data['currencySymbols']));
            // нужно переместить популярные валюты на верх
            unset($currencies['UAH']);
            unset($currencies['RUR']);
            unset($currencies['USD']);
            unset($currencies['EUR']);
            $currencies = array(
                'UAH',
                'RUR',
                'USD',
                'EUR',
            ) + $currencies;

            self::$_currencyIdList = array_combine($currencies, $currencies);
        }

        return self::$_currencyIdList;
    }

    /**
     * Возвращает символ валюты по ее id
     *
     * @access public
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->currency ? $this->currency->sign : '';
    }

    /**
     * Форматирует деньги согласно текущей локали
     *
     * @param float $value число для форматирования
     * @param boolean $withCurrencySymbol если true, то вместе с числом вернется символ валюты
     * @return string
     */
    public function formatMoney($value, $withCurrencySymbol = true)
    {
        if ($withCurrencySymbol) {
            return Yii::app()->numberFormatter->formatCurrency($value, $this->getCurrencySymbol());
        } else {
            $format = preg_replace('/\p{Zs}?¤\p{Zs}?/u', '', Yii::app()->locale->getCurrencyFormat());
            return Yii::app()->numberFormatter->format($format, $value);
        }
    }

    /**
     * Возвращает общую стоимость заказа
     *
     * @param boolean $formated - если true, то вернет строку со стоимостью, локализированной для пользователя
     * @param boolean $withCurrencySymbol если true, то вместе с числом вернется символ валюты
     * @access public
     * @return string|float
     */
    public function getTotalPrice($formated = false, $withCurrencySymbol = true)
    {
        $totalPrice = 0;
        foreach ($this->check as $check) {
            // $totalPrice += $check->price*$check->quantity;
            $totalPrice += $check->total_price;
        }

        return $formated ? self::formatMoney($totalPrice, $withCurrencySymbol) : (float) $totalPrice;
    }
    public function getTotalProduct()
    {
        $totalProduct = "";
        foreach ($this->check as $check) {
            // $totalPrice += $check->price*$check->quantity;
          if($totalProduct!="")
            $totalProduct .= ", ".$check->prod_id;
          else
            $totalProduct .= $check->prod_id;
        }

        return $totalProduct;
    }
    /**
     * Возвращает общую стоимость заказа во внутренней валюте
     * @param bool $withCurrencySymbol
     * @return int|string
     */
    public function getTotalInternalPrice($withCurrencySymbol = true)
    {
        if($withCurrencySymbol) {
            $symbol = Currency::model()->find('is_main = 1')->sign;
        }
        $totalPrice = 0;
        foreach ($this->check as $check) {
            // $totalPrice += $check->price*$check->quantity;
            $totalPrice += $check->internal_price;
        }

        return !$withCurrencySymbol ? $totalPrice : $totalPrice.' '.$symbol;
    }

    /**
     * @return array массив с курсами валют
     */
    public static function getCurrencyRates()
    {
        $ccy = Yii::app()->cache->get('ccy');
        if (!$ccy) {
            $ccy = array('UAH' => 1.0);
            $xml = simplexml_load_file('https://privat24.privatbank.ua/p24/accountorder?oper=prp&PUREXML&apicour&country=ua&full');
            $json_string = json_encode($xml);
            $result_array = json_decode($json_string, true);

            if (isset($result_array['exchangerate'])) {
                foreach ($result_array['exchangerate'] as $item) {
                    // <exchangerate ccy="BYR" ccy_name_ru="Беларусский рубль" ccy_name_ua="Бiлоруський рубль" ccy_name_en="Belarussian Rouble" base_ccy="UA" buy="84" unit="10.00000" date="2014.01.08"/>
                    // ccy - код валюты (о том какие они существуют, Вы можете посмотреть здесь)
                    // ccy_name_ru - название валюты на русском языке
                    // ccy_name_ua - название валюты на украинском языке
                    // ccy_name_en - название валюты на английском языке
                    // base_ccy - код выбранной вами страны
                    // buy - курс покупки(коп * 100)
                    // unit - количество единиц валюты, которые можно купить по  курсу покупки
                    // date - дата последнего обновления курсов валют
                    // @see https://api.privatbank.ua/article/8/
                    $ccy[$item['@attributes']['ccy']] = round($item['@attributes']['buy'] / 10000 / $item['@attributes']['unit'], 2);
                }
            }

            Yii::app()->cache->set('ccy', $ccy);
        }

        return $ccy;
    }

    /**
     * @return array массив со списком всех refferer
     */
    public function getReferers()
    {
        // 15 мин кэша
        $models = $this->cache(60 * 15)->findAll(array(
            'group' => 'referer',
        ));

        $data = CHtml::listData($models, 'referer', 'referer');
        if (isset($data[''])) {
            unset($data['']);
        }

        return $data;
    }

    /**
     * Возвращает количество зарезервированного товара
     *
     * @param integer $prodId id товара для которого нужно получить количество резерва
     * @return integer Возвращает количество зарезервированного товара
     */
    public static function getReserved($prodId)
    {
        Yii::import('cms.models.order.OrderCheck');
        $nonReservingStatuses = array(
            Order::NOT_COMPLETE,
            Order::COMPLETE,
            Order::CANCELED,
        );
        $reserved = 0;
        if (!empty($prodId)) {
            $reserved = Order::model()->find(array(
                'select' => 'SUM(check.quantity) as statusArr',
                'condition' => 'status NOT IN (:statuses) AND check.prod_id=' . $prodId,
                'params' => array(':statuses' => implode(',', $nonReservingStatuses)),
                'with' => array('check'),
            ));
            $reserved = $reserved ? $reserved->statusArr : 0;
        }

        return $reserved;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        //$criteria->compare('user_id',$this->user_id,true);
        //$criteria->compare('address_id',$this->address_id,true);
        //$criteria->compare('status',$this->status);
        if (count($this->statusArr)) {
            $criteria->addInCondition('status', $this->statusArr);
        }

        $criteria->compare('currency_provider', $this->currency_provider);
        $criteria->compare('type', $this->type);
        $criteria->compare('referer', $this->referer, true);
        //$criteria->compare('ip',$this->ip,true);
        //$criteria->compare('date',$this->date,true);

        $filterDateFrom = isset($this->filterDateFrom) && trim($this->filterDateFrom) != ""
        ? date_format(new DateTime($this->filterDateFrom), 'Y-m-d 00:00:00')
        : false;

        $filterDateTo = isset($this->filterDateTo) && trim($this->filterDateTo) != ""
        ? date_format(new DateTime($this->filterDateTo), 'Y-m-d 23:59:59')
        : false;

        if ($filterDateFrom && $filterDateTo) {
            $criteria->addBetweenCondition('date', $filterDateFrom, $filterDateTo);
        } else {
            if ($filterDateFrom) {
                $criteria->compare('date', '>=' . $filterDateFrom);
            }

            if ($filterDateTo) {
                $criteria->compare('date', '<=' . $filterDateTo);
            }
        }

        if (!empty($this->orderSearch)) {
            $searchAllCriteria = $this->searchAllCriteria();
            $criteria->mergeWith($searchAllCriteria, 'AND');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('defaultOrder' => 'date DESC'),
        ));
    }

    /**
     * Глобальный поиск
     * @return CDbCriteria
     */
    protected function searchAllCriteria()
    {
        $searchAllCriteria = new CDbCriteria();
        $searchAllCriteria->together = true;
        $searchAllCriteria->with = array('client', 'address');
        $searchAllCriteria->addSearchCondition('currency_id', $this->orderSearch, false, 'OR');
        $searchAllCriteria->addSearchCondition($this->tableAlias . '.id', $this->orderSearch, false, 'OR');
        $searchAllCriteria->addSearchCondition('client.first_name', $this->orderSearch, false, 'OR');
        $searchAllCriteria->addSearchCondition('comment', $this->orderSearch, false, 'OR');
        $searchAllCriteria->addSearchCondition('referer', $this->orderSearch, false, 'OR');
        $searchAllCriteria->compare('address.telephone', $this->orderSearch, false, 'OR');

        $operatorsList = Yii::app()->{CONNECTION_NAME}->createCommand()
                                   ->select('user_id')
                                   ->from('{{user}}')
                                   ->where('display_name LIKE :searchTerm OR email LIKE :searchTerm', array(
            ':searchTerm' => '%' . strtr($this->orderSearch, array('%' => '\%', '_' => '\_', '\\' => '\\\\')) . '%',
        ))
            ->queryColumn();
        $searchAllCriteria->compare('operator_id', $operatorsList, false, 'OR');
        return $searchAllCriteria;
    }

    public function getAllOrders()
    {
      $criteria = new CDbCriteria;
      $filterDateFrom = date("Y-m-d 00:00:00",time()-2592000);;
      $criteria->compare('date', '>=' . $filterDateFrom);
      $results = count(Order::model()->findAll($criteria));

      return $results;
    }

    public function getApprovedOrders()
    {
      $criteria = new CDbCriteria;
      $filterDateFrom = date("Y-m-d 00:00:00",time()-2592000);;
      $criteria->compare('date', '>=' . $filterDateFrom);
      $criteria->compare('status', '= 5');
      $results = count(Order::model()->findAll($criteria));
      return $results;
    }

    public function getIncome()
    {
      $criteria = new CDbCriteria;
      $filterDateFrom = date("Y-m-d 00:00:00",time()-2592000);
      $criteria->select = array('check.total_price as id');
      $criteria->alias = 'order';
      $criteria->compare('order.date', '>=' . $filterDateFrom);
      $criteria->join = 'LEFT JOIN `gxc_order_check` as `check` ON check.order_id = order.id';
      $results = Order::model()->findAll($criteria);
      $res = 0;
     
      foreach ($results as $key => $data) {
        $res += $data->id;
      }
      return $res;
    }

    public function getProfit()
    {
      $criteria = new CDbCriteria;
      $filterDateFrom = date("Y-m-d 00:00:00",time()-2592000);
      $criteria->select = array('check.total_price as id');
      $criteria->alias = 'order';
      $criteria->compare('order.date', '>=' . $filterDateFrom);
      $criteria->compare('status', '= 5');
      $criteria->join = 'LEFT JOIN `gxc_order_check` as `check` ON check.order_id = order.id';
      $results = Order::model()->findAll($criteria);
      $res = 0;
     
      foreach ($results as $key => $data) {
        $res += $data->id;
      }
      return $res;
    }
}
