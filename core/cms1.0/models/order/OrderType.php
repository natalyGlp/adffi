<?php

/**
 * This is the model class for table "{{order_type}}".
 *
 * The followings are the available columns in table '{{order_type}}':
 * @property string $id
 * @property integer $type
 * @property string $name
 * @property string $value
 * @property string $condition
 */
class OrderType extends CmsActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OrderType the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{order_type}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, name, value', 'required'),
            array('type, enabled', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('type, enabled', 'numerical'),
            array('value', 'length', 'max' => 19),
            array('condition, message', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, type, name, message, value, condition', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Не даем удалять модели
     */
    protected function beforeDelete()
    {
        $this->enabled = false;
        $this->save();
        return false;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'typeString' => 'Type',
            'name' => 'Name',
            'value' => 'Value',
            'condition' => 'Condition',
            'enabled' => 'Status',
            'enabledString' => 'Status',
        );
    }

    const TYPE_FIXED = 1;
    const TYPE_PERCENT = 2;
    const TYPE_FUNC = 3;

    /**
     * @return array типы оплаты за доставку
     */
    public static function getTypesList()
    {
        return array(
            self::TYPE_FIXED => _t('Fixed Amount'),
            self::TYPE_PERCENT => _t('Percent from Total Price'),
            // self::TYPE_FUNC => _t('Functional'),
        );
    }

    public function getStatus()
    {
        if ($this->enabled) {
            return 'Активен';
        } else {
            return 'Не активен';
        }
    }

    /**
     * @return string тип оплаты текущего способа доставки
     */
    public function getTypeString()
    {
        $types = self::getTypesList();
        return isset($types[$this->type]) ? $types[$this->type] : 'undefined';
    }

    /**
     * @return array типы оплаты за доставку
     */
    public static function getEnabledList()
    {
        return array('1' => _t('Enabled'), '0' => _t('Disabled'));
    }

    /**
     * @return string тип оплаты текущего способа доставки
     */
    public function getEnabledString()
    {
        $enabled = self::getEnabledList();
        return isset($enabled[$this->enabled]) ? $enabled[$this->enabled] : 'undefined';
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('condition', $this->condition, true);
        $criteria->compare('enabled', $this->enabled);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
