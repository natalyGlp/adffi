<?php
class Albums extends CmsActiveRecord{
	public $pkey = 'id';

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return '{{albums}}';
	}
	
	public function beforeSave(){
		$this->galleries = is_array($this->galleries) ? serialize($this->galleries) : serialize(array());
		return parent::beforeSave();
	}
	
	public function afterFind(){
		$this->galleries = is_array(unserialize($this->galleries)) ? unserialize($this->galleries) : array();
		return parent::afterFind();
	}

	public function afterSave() {
		/* Получаем список галерей и сверяем их с теми что пришло в POST если они не одинаковые тогда удаляем из базы все значения и записываем новые перезаписываем в базу*/
		$oldgals=$this->galleries_ar;
		$old=array();
		if (count($oldgals)>=1) 
		{
			foreach( $oldgals as $gal) 
			{
				$old[]=$gal->id;
			}
		}
		$newgals=isset($_POST['Albums']['galleries']) ? $_POST['Albums']['galleries'] : array();
		if (count($newgals)>=1) 
		{
			if (count($newgals)>=count($old))
			$diff=array_diff($newgals, $old ); else 
		{
			$diff=array_diff($old,$newgals);
		}
			if (count($diff)>=1) 
			{
				AlbumsGalleries::model()->deleteAll('id_album=:id_album',array(':id_album'=>$this->id));
				foreach($newgals as $gal) 
				{
					$new = new AlbumsGalleries;
					$new->id_album=$this->id;
					$new->id_gallery=$gal;
					$new->save();
				}
			} 
		} else 
		{
			AlbumsGalleries::model()->deleteAll('id_album=:id_album',array(':id_album'=>$this->id));
		}
		return parent::afterSave();
	}
	public function attributeLabels() 
	{
		return array(
			'title'=>'Title',
			'description'=>'Decription',
			'path'=>'URL path',
			'galleries'=>'Galleries',
			'active'=>'Active',
			'sort_order'=>'Order'
		 );
	}
	public function rules(){
		return array(
			array('title', 'required'),
			array('path', 'required'),
			array('path', 'unique'),
			array('description, galleries, active, sort_order', 'safe'),
			array('path', 'match', 'pattern'=>'/[a-z0-9]/'),
			);
	}

	public function relations(){
		return array(
			'galleries_ar'=>array(self::MANY_MANY,'Galleries','gxc_albums_galleries(id_album,id_gallery)')
			);
	}
	public function getGalleries() 
	{
		$galls=array();
		//var_dump($galls); 
		$galleries=explode(',',$this->galleries);
		//var_dump($this->galleries,$galleries);
		if (count($galleries)>=1 && is_numeric($galleries[0])) 
		{
			foreach ($galleries as $g)  
			{	
				$temp=trim($g);
				$galls[$temp]=Galleries::model()->findByPk($temp);
			}
		}
		//var_dump($galls); 
		return $galls;
	}
	public function getGalleriesinPost($array) 
	{
		$galls=array();
		if (count($array)>=1) 
		{
			foreach ($array as $g)  
			{	
				$temp=trim($g);
				$galls[$temp]=Galleries::model()->findByPk($temp);
			}
		}
		return $galls;
	}
	public function search(){
		$criteria=new CDbCriteria;
		$criteria->compare('t.title', $this->title, true);
		$criteria->compare('t.path', $this->path, true);
		$criteria->compare('t.active', '='.$this->active, true);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
            'defaultOrder' => 't.id',
        ),
		));
	}
}
