<?php

/**
 * This is the model class for table "{{user}}".
 *
 * @author Tuan Nguyen
 * @version 1.0
 * @package cms.models.user
 *
 * The followings are the available columns in table '{{user}}':
 * @property string $user_id
 * @property string $user_url
 * @property string $display_name
 * @property string $first_name
 * @property string $last_name
 * @property string $password
 * @property string $salt
 * @property string $email
 * @property string $fbuid
 * @property integer $status
 * @property integer $created_time
 * @property integer $updated_time
 * @property integer $recent_login
 * @property string $user_activation_key
 */
class User extends CmsActiveRecord
{
    public $image; // поле для аплоада аватарки
    protected $_emailSended = false;
    /**
     * Returns the static model of the specified AR class.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('salt,created_time, updated_time, recent_login, confirmed', 'safe'),
            array('first_name, email', 'required'),
            // on oauth scenario this field should be not required
            // FIXME: начиная с версии Yii 1.1.11 можно использовать except
            array('password', 'required', 'on' => 'insert, update, create'),
            array('email', 'email'),

            //Email must be Unique if it is on Create Scenairo
            array('email', 'unique',
                'attributeName' => 'email',
                'className' => 'cms.models.user.User',
                'message' => _t('This email has been registered.'),
            ),

            //Email must be Unique if it is on Create Scenairo
            array('user_url', 'unique',
                'attributeName' => 'user_url',
                'className' => 'cms.models.user.User',
                'message' => _t('Url has been registered.'),
                'allowEmpty' => true,
            ),

            array('location', 'length', 'max' => 100),
            array('bio', 'length', 'max' => 1500),
            array('gender', 'in', 'range' => array('male', 'female', 'other')),
            array('birthday_month', 'populateBirthdayMonth'),
            array('birthday_month', 'in', 'range' => array('january', 'febuary', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december')),
            array('birthday_day', 'numerical'),
            array('birthday_year', 'numerical'),

            array('status, created_time, updated_time, recent_login', 'numerical', 'integerOnly' => true),
            array('user_url, password, salt, email', 'length', 'max' => 128),
            array('display_name', 'length', 'max' => 255),
            array('fbuid', 'length', 'max' => 20),
            array('user_activation_key', 'length', 'max' => 255),
            array('email_recover_key', 'length', 'max' => 255),

            array('avatar', 'safe'),

            // поле для аплоада аватарки
            array('image', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 1024 * 1024 * 2, 'minSize' => 1024, 'allowEmpty' => true),
            array('email_site_news', 'in', 'range' => array('0', '1')),

            array('email_search_alert', 'in', 'range' => array('0', '1')),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('user_id, user_url, display_name, email, fbuid, status, created_time, updated_time, recent_login', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * Преобразует число месяца рождения в строку
     */
    public function populateBirthdayMonth($attribute, $params)
    {
        $monthNames = array('', 'january', 'febuary', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december');
        if (is_numeric($this->{$attribute})) {
            $this->{$attribute} = $monthNames[$this->{$attribute}];
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id' => _t('User'),
            'user_url' => _t('User Url'),
            'display_name' => _t('Display Name'),
            'first_name' => _t('First Name'),
            'last_name' => _t('Last Name'),
            'password' => _t('Password'),
            'salt' => _t('Salt'),
            'email' => _t('Email'),
            'fbuid' => _t('Fbuid'),
            'status' => _t('Status'),
            'created_time' => _t('Created Time'),
            'updated_time' => _t('Updated Time'),
            'recent_login' => _t('Recent Login'),
            'user_activation_key' => _t('User Activation Key'),
            'confirmed' => _t('Confirmed'),
            'avatar' => _t('Avatar'),
            'image' => _t('Avatar'),
            'email_site_news' => _t('Email site news'),
            'email_search_alert' => _t('Email search alert'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('user_url', $this->user_url, true);
        $criteria->compare('display_name', $this->display_name, true);
        $criteria->compare('first_name', $this->display_name, true);
        $criteria->compare('last_name', $this->display_name, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('salt', $this->salt, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('fbuid', $this->fbuid, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created_time', $this->created_time);
        $criteria->compare('updated_time', $this->updated_time);
        $criteria->compare('recent_login', $this->recent_login);

        $sort = new CSort;
        $sort->attributes = array(
            'user_id',
        );
        $sort->defaultOrder = 'user_id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
        ));
    }

    /**
     * Returns User model by its email
     *
     * @param string $email
     * @access public
     * @return User
     */
    public function findByEmail($email)
    {
        return $this->findByAttributes(array('email' => $email));
    }

    /**
     * Sends an activation email to user
     */
    public function sendActivationEmail()
    {
        if ($this->confirmed == 1 || $this->_emailSended) {
            return;
        }

        $siteName = Yii::app()->settings->get('general', 'site_name');

        $m = new YiiMailMessage();

        $m->addTo($this->email);
        $m->from = Yii::app()->settings->get('system', 'support_email');
        $m->subject = '[' . $siteName . '] Confirm your email';

        $m_content = 'Hi ' . $this->display_name . '<br /><br />';
        $m_content .= 'Welcome to ' . $siteName . '! Please take a second to confirm ' . $this->email . ' as your email address by clicking this link: <br /><br />';
        $link_content = FRONT_SITE_URL . '/user-activation/?key=' . $this->user_activation_key . '&user_id=' . $this->user_id;
        $m_content .= '<a href="' . $link_content . '">' . $link_content . '</a><br /><br />';
        $m_content .= 'Thank you for being with us!<br /><br />';
        $m_content .= $siteName . ' Team';
        // $m->setMessageFromString($m_content,$m_content);
        $m->setBody($m_content, 'text/html');
        Yii::app()->mail->send($m);
        $this->_emailSended = true;
    }

    /**
     * Validate password based on Password and Salt String
     * @param string $password
     * @param string $salt
     * @return string
     */
    public function validatePassword($password, $salt)
    {

        return $this->hashPassword($password, $salt) === $this->password;
    }

    /**
     * Return Md5 encrypt of the password
     *
     * @param string $password
     * @param string $salt
     * @return string
     */
    public function hashPassword($password, $salt)
    {
        return md5($password . $salt);
    }

    protected function beforeValidate()
    {
        if (empty($this->display_name)) {
            $this->display_name = $this->fullName;
        }

        return parent::beforeValidate();
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            $this->email = strtolower($this->email);
            $this->user_url = strtolower($this->user_url);
            if ($this->isNewRecord) {

                $this->created_time = $this->updated_time = $this->recent_login = time();
                $this->salt = USER_SALT;

                $this->password = $this->hashPassword($this->password, $this->salt);
                $this->user_activation_key = md5(time() . $this->email . $this->salt);

            } else {

                $this->updated_time = time();
            }

            return true;
        } else {
            return false;
        }
    }

    //Do Clear Session after Save
    protected function afterSave()
    {
        parent::afterSave();

        //If this user updated his own settings, changed the session of him
        if ($this->user_id == user()->id) {
            Yii::app()->getSession()->remove('current_user');
            Yii::app()->getSession()->add('current_user', $this);
        }
    }

    /**
     * Delete information of the User with Afer Delete
     */
    protected function afterDelete()
    {
        parent::afterDelete();
        AuthAssignment::model()->deleteAll('userid = :uid', array(':uid' => $this->user_id));
    }

    /**
     * Static Function retrun String Roles of the User
     * @param bigint $uid
     * @return string
     */
    public static function getStringRoles($uid = 0)
    {
        if ($uid) {
            $roles = Rights::getAssignedRoles($uid, true);
            $res = array();
            foreach ($roles as $r) {
                $res[] = $r->name;
            }
            if (count($res) > 0) {
                return implode(",", $res);
            } else {

                return '';

            }
        }

        return '';

    }

    public function getIsActive()
    {
        return $this->status == ConstantDefine::USER_STATUS_ACTIVE && $this->confirmed;
    }

    /**
     * Return the String to the image
     * @param CActiveRecord $user
     * @return string
     */
    public static function convertUserState($user)
    {

        $image = ($user->status == ConstantDefine::USER_STATUS_ACTIVE) ? 'active' : 'disabled';

        return GxcHelpers::assetUrl('images/icons' . $image . '.png', 'backend');
    }

    /**
     * Suggests a list of existing tags matching the specified keyword.
     * @param string the keyword to be matched
     * @param integer maximum number of tags to be returned
     * @return array list of matching tag names
     */
    public static function suggestPeople($keyword, $limit = 20)
    {
        $users = User::model()->findAll(array(
            'condition' => 'display_name LIKE :keyword',
            'limit' => $limit,
            'params' => array(
                ':keyword' => '%' . strtr($keyword, array('%' => '\%', '_' => '\_', '\\' => '\\\\')) . '%',
            ),
        ));
        $names = array();
        foreach ($users as $user) {
            $names[] = $user->display_name;

        }

        return $names;
    }

    /**
     * Find user with exactly display_name
     * @param type $keyword
     * @param type $limit
     * @return type
     */
    public static function findPeople($keyword, $limit = 20)
    {

        return User::model()->find(array(
            'condition' => 'display_name = :keyword',
            'limit' => $limit,
            'params' => array(
                ':keyword' => strtr($keyword, array('%' => '\%', '_' => '\_', '\\' => '\\\\')),
            ),
        ));
    }

    /**
     * @access public
     * @return string полное имя и фималилию пользвателя
     */
    public function getFullName()
    {
        if (!empty($this->display_name)) {
            return $this->display_name;
        }

        $fullName = $this->first_name;
        if (!empty($this->last_name)) {
            $fullName .= ' ' . $this->last_name;
        }

        return $fullName;
    }
}
