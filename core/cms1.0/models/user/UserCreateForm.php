<?php

/**
 * This is the model class for Create User.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.models.user
 *
 */
class UserCreateForm extends CFormModel
{
    public $first_name;
    public $last_name;
    public $display_name;
    public $password;
    public $email;
    public $image;
    public $bio;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('email, password, first_name', 'required'),
            array('display_name, first_name, last_name', 'length', 'max' => 255),
            array('bio', 'length', 'max' => 1500),
            array('password', 'length', 'min' => 3),
            array('email', 'length', 'max' => 128),
            array('email', 'email', 'message' => _t('Email is not valid')),
            array('email', 'unique',
                'attributeName' => 'email',
                'className' => 'cms.models.user.User',
                'message' => _t('This email has been registered.')),
            array('image', 'file', 'types' => 'jpg, gif, png', 'maxSize' => 1024 * 1024 * 2, 'minSize' => 1024, 'allowEmpty' => true),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'display_name' => _t('Display Name'),
            'first_name' => _t('First Name'),
            'last_name' => _t('Last Name'),
            'password' => _t('Password'),
            'email' => _t('Email'),
            'image' => _t('Avatar'),
            'bio' => _t('Bio'),
        );
    }
}
