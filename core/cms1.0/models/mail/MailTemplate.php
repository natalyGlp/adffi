<?php

/**
 * This is the model class for table "{{mailtemplate}}".
 *
 * The followings are the available columns in table '{{mailtemplate}}':
 * @property integer $mail_id unique
 * @property string $mail_text
 * @property string $mail_title
 * @property string $mail_slug unique
 */
class MailTemplate extends CmsActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return ObjectTerm the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{mailtemplate}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(

            array('mail_text,mail_title,mail_slug', 'required'),
            array('mail_slug', 'unique'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('mail_id,mail_text, mail_title', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(

        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'mail_id' => _t('ID'),
            'mail_text' => _t('Mail template text'),
            'mail_title' => _t('Mail template title'),

        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('mail_id', $this->mail_id, true);
        $criteria->compare('mail_text', $this->mail_text, true);
        $criteria->compare('mail_title', $this->mail_title, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param string $slug self::mail_slug
     * @return array|bool|CActiveRecord| return ActiveRecord
     */
    public static function getModelBySlug($slug)
    {
        $model = self::model()->findByAttributes(array('mail_slug' => $slug));
        if ($model) {
            return $model;
        } else {
            return false;
        }
    }

    /**
     * Prepare email to send operation
     * @param string $type title or text
     * @param array $params array params
     * @param string $id ID active record MAilTemplate
     * @return string $preparemail prepairing email
     */
    public function prepareMail($type = 'mail_text', $params = array(array(), array()), $id = '')
    {
        $model = $this;
        $preparemail = '';
        if (is_numeric($id)) {
            $model = MailTemplate::model()->findByPk($id);
        }
        if (is_object($id)) {
            $model = $id;
        }
        $settings = PrepareArray::getParamArray();
        $settings = array(array_merge($settings[0], $params[0]), array_merge($settings[1], $params[1]));
        $preparemail = str_replace($settings[0], $settings[1], $model->{$type});
        return $preparemail;
    }
}
