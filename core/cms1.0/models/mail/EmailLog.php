<?php

/**
 * This is the model class for table "{{email_log}}".
 *
 * The followings are the available columns in table '{{email_log}}':
 * @property string $id
 * @property string $email
 * @property string $subject
 * @property string $content
 * @property string $source
 * @property string $files
 * @property integer $status
 * @property string $date
 */
class EmailLog extends CmsActiveRecord
{
    const STATUS_UNDONE = 1;
    const STATUS_PENDING = 2;
    const STATUS_DONE = 3;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return EmailLog the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{email_log}}';
    }

    public function defaultScope()
    {
        return array(
            'order' => 'date DESC',
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content', 'required'),
            array('status', 'numerical', 'integerOnly' => true),
            array('email', 'length', 'max' => 128),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, email, content, status, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * Сохраняет письмо на основе аттрибутов обьекта формы
     * @param FeedbackBaseModel $form Обьект формы
     * @param string $source источник обращения, что бы можно было различать обращения из разных форм
     * @return boolean|EmailLog модель если сохранение произошло успешно или false
     */
    public static function saveForm(FeedbackBaseModel $form, $source = 'Feedback')
    {
        $model = new self();
        $attributes = array_keys($form->attributes);

        $model->email = $form->email;
        $model->subject = $form->subject;
        $model->source = $source;

        $emailData = array();
        $labels = $form->emailLogLabels();
        $attributes = array_keys($labels);
        foreach ($attributes as $attribute) {
            // поле прикреплений у нас всегда в html
            if ($attribute == 'attachments' && !preg_match('/\:html$/', $labels[$attribute])) {
                $labels[$attribute] .= ':html';
            }

            $emailData[$labels[$attribute]] = $form->$attribute;
        }

        $model->files = $form->files;

        $model->content = $emailData;

        return $model->save() ? $model : false;
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->status = EmailLog::STATUS_UNDONE;
            $this->date = new CDbExpression('NOW()');
        }

        $this->content = serialize($this->content);
        $this->files = CJSON::encode($this->files);

        return parent::beforeSave();
    }

    protected function afterSave()
    {
        $this->content = unserialize($this->content);
        $this->files = CJSON::decode($this->files);

        parent::afterSave();
    }

    protected function afterFind()
    {
        $this->content = unserialize($this->content);
        parent::afterFind();
    }

    /**
     * @return array возвращает массив с данными письма в формате для CListView
     */
    public function getDataAttributes()
    {
        $content = $this->content;
        $contentAttributes = array_keys($content);
        foreach ($contentAttributes as &$label) {
            $label = array(
                'label' => str_replace(':html', '', $label),
                'type' => 'raw',
                'value' => preg_match('/\:html$/', $label) ? $content[$label] : nl2br(htmlspecialchars($content[$label])),
            );
        }

        return $contentAttributes;
    }

    public function getPrettyDate()
    {
        if (!empty($this->date)) {
            return Yii::app()->dateFormatter->formatDateTime($this->date, 'medium', 'short');
        } else {
            return '';
        }
    }

    public function getStatusLabel()
    {
        return EmailLog::getStatus($this->status);
    }

    /**
     * Return the String to the image
     * @param CActiveRecord $data
     * @return string
     */
    public function getStatusIcon()
    {
        $image = ($this->status == EmailLog::STATUS_DONE) ? 'active' : 'disabled';
        if ($this->status == EmailLog::STATUS_PENDING) {
            $image = 'pending';
        }

        return GxcHelpers::assetUrl('images', 'backend') . '/icons/' . $image . '.png';
    }

    public function getBackendViewUrl()
    {
        return Yii::app()->createUrl(app()->controller->id . '/view', array("id" => $this->primaryKey));
    }

    public static function getStatus($status)
    {
        switch ($status) {
            case EmailLog::STATUS_UNDONE:
                return _t('Undone');
                break;
            case EmailLog::STATUS_DONE:
                return _t('Done');
                break;
            case EmailLog::STATUS_PENDING:
                return _t('Pending');
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => _t('ID'),
            'email' => _t('Email'),
            'content' => _t('Form fields'),
            'status' => _t('Status'),
            'statusLabel' => _t('Status'),
            'date' => _t('Date'),
            'prettyDate' => _t('Date'),
            'source' => _t('Source'),
            'subject' => _t('Subject'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
