<?php

class ResourceUploadForm extends CFormModel
{
    public $name;
    public $body;
    public $link;
    public $upload;
    public $type;
    public $where;
    public $width;
    public $height;

    public function rules()
    {
        return array(
            array('link, name, body, type,where', 'safe'),
            array('width, height', 'required'),
            array('width, height', 'numerical'),
            array('upload', 'file', 'allowEmpty' => true),
        );
    }

    public function attributeLabels()
    {
        return array(
            'upload' => _t('Upload'),
            'link' => _t('Link'),
            'name' => _t('Resource Name'),
            'body' => _t('Description'),
            'where' => _t('Storage'),
            'type' => _t('File type'),
            'width' => _t('Width'),
            'height' => _t('Height'),
        );
    }
}
