<?php

/**
 * This is the model class for General Settings Form
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.1
 * @package cms.models.settings
 *
 */
class SettingGeneralForm extends CFormModel
{
    public $support_email;
    public $site_url;
    public $site_name;
    public $slogan;
    public $homepage;
    public $phones = array(
        '' => array(
            'phone_filter_type' => 1,
            'phone_filter' => '',
        ),
    );
    public $address;
    public $logo;

    /**
     * Declares the validation rules.
     *
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('site_url, site_name, homepage', 'required'),
            array('logo', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => true),
            array('slogan, phones, address', 'safe'),
            array('support_email', 'required'),
            array('support_email', 'email'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'site_url' => _t('Site url'),
            'site_name' => _t('Site name'),
            'phones' => _t('Phone number'),
            'logo' => _t('Logo'),
            'address' => _t('Address'),
            'slogan' => _t('Slogan'),
            'homepage' => _t('Page name used as Homepage'),
            'support_email' => _t('Support email'),
        );
    }
}
