<?php

/**
 * This is the model class for table "{{language}}".
 *
 * The followings are the available columns in table '{{language}}':
 * @property integer $lang_id
 * @property string $lang_name // e.g. en_us. имя, по которому ставится локализация прилоения
 * @property string $lang_short // lang id из url
 * @property string $lang_desc
 * @property integer $lang_required
 * @property integer $lang_active
 */
class Language extends CmsActiveRecord
{
    private static $_items = array();
    public $lang_active = 1;

    /**
     * Returns the static model of the specified AR class.
     * @return Language the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{language}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lang_name, lang_desc, lang_required, lang_active, lang_short', 'required'),
            array('lang_name, lang_short', 'match', 'pattern' => '/[a-z0-9]/'),
            array('lang_required, lang_active', 'numerical', 'integerOnly' => true),
            array('lang_name, lang_desc', 'length', 'max' => 255),
            array('lang_short', 'length', 'max' => 10),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('lang_id, lang_name, lang_desc, lang_required, lang_active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'lang_id' => _t('id'),
            'lang_name' => _t('Language CLDR ID (ru, uk_UA etc.). About <a href="http://cldr.unicode.org/index/cldr-spec/picking-the-right-language-code" target="_blank">CLDR</a>'),
            'lang_desc' => _t('Description'),
            'lang_required' => _t('Default'),
            'lang_active' => _t('Active'),
            'lang_short' => _t('Url Prefix'),
        );
    }

    public static function ajaxDeleteModel($id)
    {
        header('Content-Type: application/json');

        if (Yii::app()->request->isPostRequest) {
            //First make sure that there is no children category
            $model = GxcHelpers::loadDetailModel('Language', $id);
            if ($model->delete()) {
                echo CJSON::encode(array('result' => _t('success'), 'message' => ''));
            } else {
                echo CJSON::encode(array('result' => _t('error'), 'message' => _t('Error deleting language!')));
            }
        } else {
            echo CJSON::encode(array('result' => _t('error'), 'message' => _t('Error! Invalid Request!')));
        }
        Yii::app()->end();
    }

    /**
     * Геттер для обеспечения более правильной API без переименовывания колонок в бд
     * @return string префикс языка для использования в url
     */
    public function getUrlPrefix()
    {
        return $this->lang_short;
    }

    /**
     * Геттер для обеспечения более правильной API без переименовывания колонок в бд
     * @return string cldr идентификатор языка, например en_US
     */
    public function getCode()
    {
        return $this->lang_name;
    }

    /**
     * TODO: убрать после перименовывания колонок в таблице на людские
     */
    public function getName()
    {
        return $this->lang_desc;
    }

    public function afterSave()
    {
        if ($this->lang_required == 1) {
            $this->updateAll(array('lang_required' => 0), 'lang_id <> ' . $this->lang_id);
        }

        return parent::afterSave();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('lang_id', $this->lang_id);
        $criteria->compare('lang_name', $this->lang_name, true);
        $criteria->compare('lang_desc', $this->lang_desc, true);
        $criteria->compare('lang_required', $this->lang_required);
        $criteria->compare('lang_active', $this->lang_active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function items($exclude = array(), $desc = true)
    {
        return self::loadItems($exclude, $desc);

    }

    /**
     * Load Items Language
     * @param type $exclude
     */
    public static function loadItems($exclude = array(), $desc = true)
    {
        $criteria = array(
            'condition' => 'lang_active = :isActive',
            'params' => array(':isActive' => 1),
            'order' => 'lang_id ASC',
        );

        if (!empty($exclude)) {
            $criteria['condition'] .= ' AND lang_id NOT IN (' . implode(',', $exclude) . ')';
        }

        $models = self::model()->findAll($criteria);

        if (count($models) > 0) {
            foreach ($models as $model) {
                if ($desc) {
                    $items[$model->lang_id] = $model->name;
                } else {
                    $items[$model->lang_id] = $model->code;
                }
            }
            return $items;
        } else {
            Yii::app()->getController()->redirect('admin');
        }
    }

    public static function mainLanguage()
    {
        $model = self::model()->findByAttributes(array(
            'lang_active' => 1,
            'lang_required' => 1,
        ));

        if ($model != null) {
            return $model->lang_id;
        } else {
            return self::model()->find(array('order' => 'lang_id ASC'))->lang_id;
        }
    }

    public static function convertLanguage($id)
    {
        $model = Language::model()->findByPk($id);

        if ($model) {
            return $model->lang_desc;
        }

        return '';
    }

    public function getStatus($dropdown = false)
    {
        $status = array(0 => 'Disabled', 1 => 'Active');
        if (!$dropdown) {
            return $status[$this->lang_active];
        } else {
            return $status;
        }
    }

    public function getIsMultilang()
    {
        return $this->cache(60 * 15)->count(array(
            'condition' => 'lang_active = 1',
        )) > 1;
    }

    public function getDefaultLang($dropdown = false)
    {
        $types = array(0 => 'No', 'Yes');
        if (!$dropdown) {
            return $types[$this->lang_required];
        } else {
            return $types;
        }
    }

    public function getChangeLangLink()
    {
        return rtrim('/' . $this->urlPrefix . Yii::app()->request->url, '/');
    }
}
