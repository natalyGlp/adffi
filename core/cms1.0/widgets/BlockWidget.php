<?php
class BlockWidget extends CWidget
{
    protected $_block = null;
    public $cssClass;
    public $errors = array();

    private $_defaults;

    /**
     * Устанавливает параметры блока
     */
    public function init()
    {
        if ($this->_block) {
            // DEPRECATED инициализация параметров блока
            $params = $this->block->params;
            if (!is_array($this->block->params)) {
                $params = unserialize($params);
            }
            $this->setParams($params);
        }

        if (property_exists($this, 'block')) {
            if (YII_DEBUG) {
                Yii::log('Deprecated property '.get_class($this).'::block was defined', CLogger::LEVEL_INFO, 'deprecated');
            }
            $this->block = $this->getBlock();
        }
    }

    public function setParams($params)
    {
        foreach ($params as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

    /**
     * Возвращает label для параметра
     * @param  BlockWidget $block
     * @param  string $param название параметра
     * @return string label для параметра
     *
     * @see  Block::getLabel()
     */
    public static function getLabelForIntance(BlockWidget $block, $param)
    {
        return $block->getAttributeLabel($param);
    }

    public function params()
    {
        return array();
    }

    /**
     * Внутренний метод, который обьединяет дефолтные параметры блока с теми, которые были обьявлены в ::params() методе
     * Используется внутри приложения
     * @return array customized attribute labels (name=>label)
     */
    public function paramsInternal()
    {
        return CMap::mergeArray(array(
            'cssClass' => _t('CSS class'),
        ), $this->params());
    }

    /**
     * @return array Названия дефолтных параметров блока
     */
    protected function getDefaultParams()
    {
        if (!isset($this->_defaults)) {
            $reflect = new ReflectionClass($this);
            $defaults = $reflect->getDefaultProperties();
            $params = $this->params();

            $this->_defaults = array();
            foreach ($params as $property => $value) {
                if (array_key_exists($property, $defaults)) {
                    $this->_defaults[$property] = $defaults[$property];
                }
            }
        }

        return $this->_defaults;
    }

    public function beforeBlockSave()
    {
        return true;
    }

    public function afterBlockSave()
    {
        return true;
    }

    public function validate()
    {
        return true ;
    }

    /**
     * Позволяет рендерить файлы _block_output и _block_input,
     * а так же дефолтные вьюхи из директории views
     */
    public function getViewFile($viewName)
    {
        if (strpos($viewName, '.')) {
            return parent::getViewFile($viewName);
        }

        $blockPath = 'front_blocks.'.$this->blockType;
        if (in_array($viewName, array('output', 'input'))) {
            $viewName = $this->blockType.'_block_'.$viewName;
        }

        $newViewName = GxcHelpers::getTruePath($blockPath.'.views.'.$viewName);
        if ($viewPath = parent::getViewFile($newViewName)) {
            return $viewPath;
        } else {
            $newViewName = GxcHelpers::getTruePath($blockPath.'.'.$viewName);
            return parent::getViewFile($newViewName);
        }
    }

    /**
     * @return string тип блока
     */
    public function getBlockType()
    {
        $viewPath = $this->getViewPath();
        return basename(dirname($viewPath));
    }

    /**
     * Формирует имена классов для блоков из основной части $className и префикса $prefix
     *
     * @param string $className класс-основа для нового класса (например 'content')
     * @param string $prefix префикс, который будет добавлен к $className (по умолчанию: BlockWidget::cssClass)
     * @param boolean $separatePrefix если true, функция вернет дополнительное еще один класс, который == $prefix
     *
     * @return string новое имя класса
     */
    public function createCssClass($className, $prefix = null, $separatePrefix = false)
    {
        if (!$prefix) {
            $prefix = $this->cssClass;
        }

        return ($separatePrefix ? $prefix . ' ' : '').(empty($prefix) ? $className : $className .  ucfirst($prefix));
    }

    /**
     * Методы для интеграции с CModel
     */

    /**
     * Returns the text label for the specified attribute.
     * @param string $attribute the attribute name
     * @return string the attribute label
     */
    public function getAttributeLabel($attribute)
    {
        $labels = $this->paramsInternal();

        if (array_key_exists($attribute, $labels)) {
            return $labels[$attribute];
        } else {
            // Копипаст из CActiveRecord
            return ucwords(trim(strtolower(str_replace(array('-','_','.'), ' ', preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute)))));
        }
    }

    public function isAttributeRequired()
    {
        return false;
    }

    public function hasErrors()
    {
        return !empty($this->errors);
    }

    public function getError($attribute)
    {
        return isset($this->errors[$attribute]) ? $this->errors[$attribute] : null;
    }

    public function getValidators()
    {
        return array();
    }



    public function getBlock()
    {
        if (YII_DEBUG) {
            Yii::log('Deprecated property '.get_class($this).'::block was called', CLogger::LEVEL_INFO, 'deprecated');
        }

        if (!$this->_block) {
            // FIXME: временное решение для перехода на виджиты без зависимости от Block модели
            $this->_block = (object)array(
                'block_id' => $this->getId(),
                'type' => $this->blockType,
                'params' => serialize(array()),
                );
        }

        return $this->_block;
    }

    public function getLayout_asset()
    {
        if (YII_DEBUG) {
            Yii::log('Deprecated property '.get_class($this).'::layout_asset was called', CLogger::LEVEL_INFO, 'deprecated');
        }
    }

    public function setBlock($block)
    {
        $this->_block = $block;
    }

    public function getBlockId()
    {
        return $this->block ? $this->block->block_id : $this->id;
    }
}
