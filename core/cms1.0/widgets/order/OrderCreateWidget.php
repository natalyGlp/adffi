<?php

/**
 * Виджет для ручной проводки заказа или редактирования существующего
 *
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.order
 */
class OrderCreateWidget extends CWidget
{

    public $visible = true;

    public function init()
    {
        Yii::import('cms.models.order.*');
        GxcHelpers::registerJs('sticky/sticky.min.js');
        GxcHelpers::registerCss('sticky/sticky.css');
    }

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $orderCrud = new OrderCrud(isset($_GET['id']) ? $_GET['id'] : null);

        $form = $this->initForm($orderCrud);

        $order = &$form['order']->model;
        $address = &$form['address']->model;
        $client = &$form['client']->model;

        if (isset($_POST['ajax'])) {
            $orderCrud->ajaxValidate();
        }

        if ($form->submitted('submit', false)) {
            $post = $_POST;

            if (isset($post['OrderCheck']) && is_array($post['OrderCheck'])) {
                $post['OrderCheck'] = array_map(function($quantity, $pk) {
                    return array(
                        'prod_id' => $pk,
                        'quantity' => $quantity,
                    );
                }, $post['OrderCheck'], array_keys($post['OrderCheck']));
            }

            $orderCrud->setData($post);
            $orderCrud->setOperator(Yii::app()->user->id);
            try {
                //Yii::log("NNNN ".print_r($orderCrud,true)); 
                $orderCrud->save();

                if (!Yii::app()->request->isAjaxRequest) {
                    Yii::app()->user->setFlash('success', isset($_GET['id']) ? "Заказ успешно обновлен" : "Заказ оформлен успешно");
                    Yii::app()->controller->refresh();
                }
            } catch (Exception $e) {
                if (!Yii::app()->request->isAjaxRequest) {
                    Yii::app()->user->setFlash('error', "Ошибка обработки заказа: " . $e->getMessage());
                    Yii::app()->controller->refresh();
                } else {
                    throw new CHttpException(404, "Ошибка обработки заказа: " . $e->getMessage());
                }
            }
        }

        $this->render('cms.widgets.views.order.create', array(
            'form' => $form,
            'check' => $orderCrud->getModel('checks'),
            'listPayments' => CHtml::listData(CurrencyProvider::model()->findAll(), 'id', 'name'),
        ));
    }

    protected function initForm($orderCrud)
    {
        Yii::import('bootstrap.widgets.*'); // TODO: что-то не так с импортами в TbForm
        Yii::import('bootstrap.widgets.TbForm');
        $form = new TbForm('cmswidgets.views.order.orderCreateForm');

        $form['order']->model = $orderCrud->getModel('order');
        $form['address']->model = $orderCrud->getModel('address');
        $form['client']->model = $orderCrud->getModel('client');

        $form['check']->model = new OrderCheck; // заглушка для работы CForm

        return $form;
    }
}
