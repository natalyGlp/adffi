<?php

/**
 * This is the Widget for suggest a People that User can send content to
 * 
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package  cmswidgets.object
 *
 */
class ObjectExtraWorkWidget extends CWidget
{
    public $visible=true; 
    public $type='suggest_people';

    public function run()
    {
        if($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    { 
        switch ($this->type){
            case 'suggest_people':
                if(isset($_GET['q']) && ($keyword=trim($_GET['q']))!=='')
                {            
                        $users=User::suggestPeople($keyword);
                        if($users!==array())
			echo implode("\n",$users);
                        
                }
                Yii::app()->end(); 
                break;

            case 'suggest_tags':                
                if(isset($_GET['q']) && ($keyword=trim($_GET['q']))!=='')
                {                    
                        echo $this->suggestTags($keyword);
                }
                Yii::app()->end(); 
                break;
        }
    }

    /**
     * Suggest Tags for Object
     * @param type $keyword 
     */
    public  function suggestTags($keyword)
    {                
        $tags=Tag::model()->suggestTags($keyword);
        if($tags!==array())
                echo implode("\n",$tags);                       
    }
}
