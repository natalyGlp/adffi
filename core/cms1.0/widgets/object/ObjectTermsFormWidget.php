<?php

namespace cms\widgets\object;

class ObjectTermsFormWidget extends \CWidget
{
    public $form;
    public $model;
    public $taxonomies = array();
    public $selectedTerms = array();

    public function init()
    {
        $this->registerClientScripts();
    }

    public function run()
    {
        foreach ($this->taxonomies as $key => $taxonomy) {
            $selectedTerms = isset($this->selectedTerms[$key]['terms']) ? $this->selectedTerms[$key]['terms'] : array();
            $this->render('cms.widgets.views.object.object_term', array(
                'form' => $this->form,
                'model' => $this->model,
                'taxonomy' => $taxonomy,
                'selectedTerms' => $selectedTerms,
            ));

            $this->renderTaxonomyScript($taxonomy, $selectedTerms);
        }
    }

    protected function renderTaxonomyScript($taxonomy, $selectedTerms)
    {
        $unSelectedTerms = array();
        foreach ($taxonomy['terms'] as $key => $term) {
            if (!isset($selectedTerms[$key])) {
                $unSelectedTerms[$key] = $term;
            }
        }
        $arguments = array(
            'taxonomyId' => $taxonomy['id'],
            'terms' => \CJavaScript::encode($unSelectedTerms),
            'selectedTerms' => \CJavaScript::encode($selectedTerms),
        );
        \Yii::app()->clientScript->registerScript(
            __FILE__.'#'.$taxonomy['id'],
            'new TaxonomyWidget(' . implode(', ', $arguments) .');'
        );
    }

    protected function registerClientScripts()
    {
        \Yii::app()->clientScript->registerCoreScript('jquery');

        \Yii::app()->clientScript->registerScript(__FILE__.'#TaxonomyWidget', '
            var TaxonomyWidget = function(taxonomyId, terms, selectedTerms) {
                var $terms = $("#list_terms_" + taxonomyId);
                var $selectedTerms = $("#selected_terms_" + taxonomyId);

                function renderTermInput(term) {
                    var $container = $("<div rel=\""+term.name+"\">");
                    var inputId = "term_"+term.id;
                    var $input = $("<input value=\""+term.id+"_" + taxonomyId + "\" id=\""+inputId+"\" type=\"checkbox\" name=\"terms[]\" />");
                    $input.change(changeHandler);

                    $container.append($input);
                    $container.append(" ");
                    $container.append("<label for=\""+inputId+"\">"+term.name+"</label>");
                    return $container;
                }

                function changeHandler() {
                    var $container = $(this).parent();
                    $container.appendTo($(this).is(":checked") ? $selectedTerms : $terms);
                }

                function renderTerms(taxonomyId, $container, termsArray) {
                    $.each(termsArray, function(k, term) {
                        $container.append(renderTermInput(term));
                    });
                }

                renderTerms(taxonomyId, $selectedTerms, selectedTerms);
                $selectedTerms.find("[type=checkbox]").attr("checked", true);

                renderTerms(taxonomyId, $terms, terms);
            };
        ');

        \Yii::app()->clientScript->registerScript(__FILE__.'#change-taxonomy-language', '
            function changeI18NTerms(currentLanguage) {
                if(!$.isNumeric(currentLanguage)) {
                    currentLanguage = $("#lang_select").val();
                }

                if(!currentLanguage) {
                    return;
                }

                $(".taxonomy_wrap").each(function() {
                    var taxonomy = this.id.split("_");
                    var taxonomyId = taxonomy[1];
                    var taxonomyLanguage = taxonomy[2];

                    if(taxonomyLanguage != currentLanguage) {
                        $(this)
                            .hide()
                            .find(":checked").click()
                            ;
                    } else {
                        $(this).show();
                    }
                });
            }

            '.($this->model->isNewRecord ? '$("#lang_select").change(changeI18NTerms).change();' : 'changeI18NTerms('.$this->model->lang.');').'
        ');
    }
}
