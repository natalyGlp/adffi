<?php

/**
 * This is the Widget for manage a Object based on its status.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package  cms.widgets.object
 *
 */
class ObjectManageStatusWidget extends CWidget
{
    public $visible = true;
    public $type = 0;
    public $object_type = 0;
    public $url = false;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $this->doAdminSearch($this->type);
    }

    public function doAdminSearch($type = 0)
    {
        $result = null;

        $modelName = Object::importByObjectType($this->object_type);

        $model = new $modelName();

        $model->unsetAttributes();
        if (isset($_GET[$modelName])) {
            $model->attributes = $_GET[$modelName];
        }

        $result = $model->doSearch($type, $this->object_type);

        $this->render('cms.widgets.views.object.object_manage_widget', array(
            'model' => $model,
            'result' => $result,
            'url' => $this->url
        ));
    }
}
