<?php
/**
 * Виджет отображающий UI для создания динамических списокв обьектов для их использования, к примеру в слайдерах
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 *
 * @version 1.0
 * @package common.front_blocks.feedback
 */

// TODO: вынести стили в css
// TODO: вьюха
class DynamicListWidget extends CWidget
{
    public $model;

    public function init()
    {
        if (!isset($this->model)) {
            throw new CException('DynamicListWidget::model is required');
        }
    }

    public function run()
    {
        // добываем динамические списки (это между прочим не так уж и просто...)
        $lists = ContentList::model()->findAll(array(
            'condition' => 'value LIKE \'%s:4:"type";s:1:"' . ContentList::TYPE_DYNAMIC . '";%\'',
        ));

        if (count($lists) == 0) {
            return;
        }

        $this->registerClientScripts();

        foreach ($lists as $list) {
            $manList = $list->manual_list;

            $isCurObjSelected = count($manList) && in_array($this->model->primaryKey, $manList);

            $content = '';
            $curObjCheckBox = '<div class="toggle-button">'
            . CHtml::hiddenField('DynamicLists[' . $list->primaryKey . '][delete][]', $this->model->primaryKey)
            . CHtml::checkBox('DynamicLists[' . $list->primaryKey . '][]', $isCurObjSelected, array('value' => $this->model->primaryKey ? $this->model->primaryKey : 'null'))
            . '</div>';
            $curObjBtn = '<li class="sepH_b"><a href="" class="btn" style="vertical-align: 9px;width: 96px" onclick="$(this).parent().find(\'input\').click();return false">' . _t('Current Object') . '</a> ' . $curObjCheckBox . '</li>';

            // добавляем остальные обьекты, если они есть в списке
            if (count($manList) && $list->number > 1) {
                $objList = Object::model()->findAllByPk($manList, array(
                    'order' => 'FIELD(object_id, ' . implode(',', $manList) . ')',
                ));
                ob_start();

                if (!$isCurObjSelected) {
                    echo $curObjBtn;
                }

                foreach ($objList as $object) {
                    if ($object->primaryKey == $this->model->primaryKey) {
                        echo $curObjBtn;
                        continue;
                    }
                    ?>
                    <li><div class="btn-group sepH_b">
                        <?php
                        echo CHtml::link($object->object_name, Yii::app()->createUrl('beobject/update', array('id' => $object->primaryKey, 'type' => $object->object_type)), array(
                            'style' => 'width: 165px;overflow: hidden;text-overflow: ellipsis;',
                            'class' => 'btn',
                        ))
                        . CHtml::hiddenField('DynamicLists[' . $list->primaryKey . '][]', $object->primaryKey);
                        ?>
                        <a class="btn btn-danger dynamicListDeleteItem"><i class="icon-trash"></i></a>
					</div></li>
                    <?php
                }
                $content .= '<label>Все материалы в блоке<br>(перетащите, что бы поменять порядок):</label><ul class="dynamikListMembers">' . ob_get_clean() . '</ul>';
            } else {
                $content = $curObjCheckBox;
            }

            $this->widget('bootstrap.widgets.TbBox', array(
                'title' => $list->name,
                'content' => $content,
                'htmlOptions' => array(
                    'style' => 'position:static', // Очень важно, иначе не работает sortable
                )
            ));
        }
    }

    public function registerClientScripts()
    {
        // красивые кнопочки toggle
        GxcHelpers::registerJs('toggle_buttons/jquery.toggle.buttons.js');
        GxcHelpers::registerCss('toggle_buttons/bootstrap-toggle-buttons.css');
        Yii::app()->clientScript
                  ->registerScript(__FILE__ . '#DynamicLists', '
				$(".toggle-button").toggleButtons();
				$(".dynamikListMembers").sortable();
				$("body").on("click", ".dynamicListDeleteItem", function() {
					$(this).parent().hide(400);
					var input = $(this).parent().find("input[type=hidden]")[0];
					input.name = input.name.replace("[]", "[delete][]");

					return false;
				});')
        ;
    }
}
