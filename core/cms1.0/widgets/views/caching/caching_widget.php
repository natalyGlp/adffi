<div class="form">
    <form action="" method="post">
        <?php
        if ($backend_assets) {
            echo CHtml::checkBox('backend_assets', true) . ' ';
            echo CHtml::label(_t('Clear BACKEND Assets'), 'backend_assets') . '<br>';
        }
        if ($frontend_assets) {
            echo CHtml::checkBox('frontend_assets', true) . ' ';
            echo CHtml::label(_t('Clear FRONTEND Assets'), 'frontend_assets') . '<br>';
        }

        if ($frontend_cache) {
            echo CHtml::checkBox('frontend_cache', true) . ' ';
            echo CHtml::label(_t('Clear FRONTEND Cache'), 'frontend_cache') . '<br>';
        }

        if ($backend_cache) {
            echo CHtml::checkBox('backend_cache', true) . ' ';
            echo CHtml::label(_t('Clear BACKEND Cache'), 'backend_cache') . '<br>';
        }

        if ($images_cache) {
            echo CHtml::checkBox('images_cache') . ' ';
            echo CHtml::label(_t('Clear IMAGES Cache'), 'images_cache') . '<br>';
        }
        ?>

        <div class="row form-actions">
            <input class="btn" type="submit" name="submit" value="Clear"/>
        </div>
    </form>
</div>
