<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'comment-grid',
    'enableHistory' => true,
    'dataProvider' => $result,
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'comment_id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->comment_id',
        ),
        array(
            'name' => 'parent_model',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->parent_model',
        ),
        array(
            'name' => 'parent_guid',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->parent_guid',
        ),
        array(
            'name' => 'content',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft ',
            ),
            'value' => 'CHtml::link(substr($data->content, 0, 150),array("' . app()->controller->id . '/view","id"=>$data->comment_id))',
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft gridmaxwidth',
            ),
            'value' => '$data->status',
        ),
        array(
            'name' => 'author_name',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->author_name',
        ),
        array(
            'name' => 'email',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->email',
        ),
    ),
));
