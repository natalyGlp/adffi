<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
            
        array('name'=>'comment_id',
			'type'=>'raw',			
			'value'=>$model->comment_id,
		    ),
      'parent_model',
        array(
			'name'=>'parent_guid',
			'type'=>'raw',
			'value'=>CHtml::link($model->parent_guid,array("beobject/view","id"=>$model->parent_guid)),
			),        
		array(
			'name'=>'content',
			'type'=>'raw',		
			'value'=>$model->content,
		    ),
        'author_name',
        array(
			'name'=>'status',
			'value'=>Comment::getStatus($model->status),
			),
        array(
			'name'=>'createTime',
			),
	),
)); ?>
