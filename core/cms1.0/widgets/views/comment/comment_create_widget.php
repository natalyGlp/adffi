<?php
// Обновление страницы после добавления коммента, что бы сбить пост запрос
if(isset($_POST[get_class($model)]) && !$model->hasErrors())
{
    Yii::app()->user->setFlash('success', _t('Thank you for the comment. It will be displayed after moderator approves it'));
    $this->controller->refresh();
}
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'comment-form',
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php if(Yii::app()->user->isGuest): ?>
    <div class="row">
        <?php echo $form->labelEx($model,'author_name'); ?><br />
        <?php echo $form->textField($model,'author_name',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'author_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?><br />
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    <?php endif; ?>

    <div class="row">
        <?php echo $form->labelEx($model,'content'); ?><br />
        <?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'content'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Summit' : 'Save'); ?>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->
