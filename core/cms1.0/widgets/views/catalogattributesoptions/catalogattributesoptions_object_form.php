<?php
$this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => _t('Add attributes'),
));
$catalogattributes = isset($model->catalogattributes) ? $model->catalogattributes : new CatalogAttributes;
?>

<div class="row">
    <?php echo $form->labelEx($catalogattributes, 'id_attributes'); ?>
    <?php echo $form->dropDownList($catalogattributes, 'id_attributes', CHtml::listData(CatalogAttributesOptions::model()->findAll(array('select' => array('id', 'name'))), 'id', 'name')); ?>
    <?php echo $form->error($catalogattributes, 'id_attributes'); ?>
</div>

<div id="attributes_rows"></div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#CatalogAttributes_id_attributes').change(function(){
            $.ajax({
                url: '<?php echo !isset($_GET['site']) || empty($_GET['site']) ? bu() : ''; ?>/becatalogattributes/getbackendform',
                type: 'get',
                data: {
                    aoid: $(this).val(),
                    id: '<?php echo $catalogattributes->id ?>',
                    site: '<?php echo isset($_GET['site']) ? $_GET['site'] : '';?>',
                },
                success: function(data){
                    $('#attributes_rows').html('').append($(data));
                }
            });
        }).change();
    });
</script>
<?php $this->endWidget();?>
