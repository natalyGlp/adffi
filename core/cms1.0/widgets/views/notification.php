<?php
$flashes = Yii::app()->user->getFlashes();
if($flashes)
{
    foreach($flashes as $key => $message) {
        echo '<div class="notification note' . $key . ' alert alert-' . $key . '">
            <div>' . $message . '</div>
        </div>';
    }
    Yii::app()->clientScript->registerScript(__FILE__, '$(".notification").delay(4000).fadeOut(1300)');
}