<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'albums-form',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array
	(
		'validateOnSubmit'=>true,
		'beforeValidate'=>'js:function(form)
		{
			console.log("asdf");
			$("#lstBox2 option").attr("selected", "selected");
			return false;
		}'
		)
)); ?>
	<script type="text/javascript">
$(document).ready(function() {
	$('#albums-form').submit(function(){
		console.log('asdf');
		$("#lstBox2 option").attr("selected", "selected");
	});
    $('#btnRight').click(function(e) {
        var selectedOpts = $('#lstBox1 option:selected');
        if (selectedOpts.length == 0) {
            alert("Выберите галерею.");
            e.preventDefault();
        }

        $('#lstBox2').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });

    $('#btnLeft').click(function(e) {
        var selectedOpts = $('#lstBox2 option:selected');
        if (selectedOpts.length == 0) {
            alert("Выберите галерею.");
            e.preventDefault();
        }
        $()
        $('#lstBox1').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });
});
	</script>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'path'); ?>
		<?php echo $form->textField($model,'path',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'path'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'galleries'); ?>
		<?php //echo $form->textField($model,'galleries',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'galleries'); ?>
	</div>
	<div>
	<div class="width-40 list-galls" >
		<?
		//var_dump(count($model->galleries_ar));
		$aviablegalls=$galls;

		if (count($model->galleries_ar)>=1 && count($galls)>=1)
		{
			$aviablegalls=array();
			foreach ($galls as $gal) {
				$aviablegalls[$gal->id]=$gal;

			}
			foreach ($model->galleries_ar as $key => $value) {

					if (array_key_exists($value->id, $aviablegalls)) {

					unset($aviablegalls[$value->id]); }
			}

		}
		?>
		<span>Галереи которые можно добавить в альбом: </span>
		<?
		echo CHtml::listBox('galleries',array(),CHtml::listData($aviablegalls,'id','title'),array('id'=>'lstBox1')); ?>

	</div>
	<div id="albums-gall-buttons"><input type='button' id='btnRight' value ='  >  '/>
		<br/>
        <input type='button' id='btnLeft' value ='  <  '/>
    </div>
		<div class="width-40 current-gals">
			<span>Галереи добавленные в альбом: </span>
			<? echo CHtml::listBox('Albums[galleries]',array(),CHtml::listData($model->galleries_ar,'id','title'),array('id'=>'lstBox2','multiple'=>'multiple')); ?>
		</div>
	</div>
	<div class="cl"></div>
	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->dropDownList($model,'active',array('1'=>'Active','2'=>'Unactive')); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sort_order'); ?>
		<?php echo $form->textField($model,'sort_order',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'sort_order'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
