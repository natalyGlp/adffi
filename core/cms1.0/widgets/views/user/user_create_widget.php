<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'usercreate-form',
    'enableAjaxValidation' => true,
    'htmlOptions' => array(
        'enctype'=>'multipart/form-data'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>
<div class="row">
    <?php echo $form->labelEx($model,'email'); ?>
    <?php echo $form->textField($model,'email'); ?>
    <?php echo $form->error($model,'email'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'first_name'); ?>
    <?php echo $form->textField($model,'first_name'); ?>
    <?php echo $form->error($model,'first_name'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'last_name'); ?>
    <?php echo $form->textField($model,'last_name'); ?>
    <?php echo $form->error($model,'last_name'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'display_name'); ?>
    <?php echo $form->textField($model,'display_name'); ?>
    <?php echo $form->error($model,'display_name'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'password'); ?>
    <?php echo $form->passwordField($model,'password'); ?>
    <?php echo $form->error($model,'password'); ?>
</div>

<div class="clearfix">
    <?php echo $form->label($model,'image'); ?>
    <?php echo $form->fileField($model,'image',array('style'=>'display:block; margin-top:15px')); ?>
    <?php echo $form->error($model,'image',array('style'=>'width: 540px;')); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'bio'); ?>
    <?php echo $form->textArea($model,'bio'); ?>
    <?php echo $form->error($model,'bio'); ?>
</div>


<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Save'),array('class'=>'btn bebutton')); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    CopyString('#UserCreateForm_email','#UserCreateForm_username','email');
    CopyString('#UserCreateForm_email','#UserCreateForm_display_name','email');
</script>
</div><!-- form -->
