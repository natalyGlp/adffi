<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'settings-form',
    'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>
<?php
function getSizesRow($id, $size = false) {
    if(!$size)
    {
        $size = array(
            'version' => '',
            'max_width' => '',
            'max_height' => '',
            'scale_up' => '',
            'fit' => '',
            );
    }
    ob_start();
?>
    <tr id="tr_<?php echo $id ?>">
        <td width="170px" valign="top"><?php echo CHtml::textField('SettingSystemForm[sizes]['.$id.'][version]', $size['version']);?></td>
        <td valign="top"><?php echo CHtml::textField('SettingSystemForm[sizes]['.$id.'][max_width]', $size['max_width']);?></td>
        <td valign="top"><?php echo CHtml::textField('SettingSystemForm[sizes]['.$id.'][max_height]', $size['max_height']);?></td>
        <td valign="top" style="text-align:center"><?php echo CHtml::checkBox('SettingSystemForm[sizes]['.$id.'][scale_up]', $size['scale_up']);?></td>
        <td valign="top"><?php echo CHtml::dropDownList('SettingSystemForm[sizes]['.$id.'][fit]', $size['fit'], array(
          'inside' => _t('Inside'),
          'outside' => _t('Outside'),
          'fill' => _t('Fill'),
        ), array('style' => 'width: 85px;'));?></td>
        <td valign="top"><button type="button" rel="<?php echo $id;?>" class="remove_version btn btn-danger"><?php echo ('Удалить');?> <b class="icon-remove"></b></button></td>
    </tr>
<?php
    return ob_get_clean();
}
?>

<table class="form" width="100%" align="center" class="main">
        <tr>
            <td><b><?php echo _t('Sizes for resources');?></b></td>
            <td>
                <div class="alert alert-block">
                    <h4><?php echo _t('Warning');?></h4>
                    <?php echo _t('All size ids should be named in the Latin alphabet in place of space characters to be used an underscore symbol "_"!');?>
                </div>
                <table id="sizes" width="650px">
                    <tr>
                        <td valign="top"><h5><?php echo _t( 'Size id');?></h5></td>
                        <td valign="top"><h5><?php echo _t( 'Width');?></h5></td>
                        <td valign="top"><h5><?php echo _t( 'Height');?></h5></td>
                        <td valign="top" style="text-align:center"><h5><?php echo _t( 'Allow zooming');?></h5></td>
                        <td valign="top" style="text-align:center"><h5><?php echo _t( 'Fit');?></h5></td>
                        <td></td>
                    </tr>
                <?php $i = 0; foreach($model->sizes as $k => $v):
                      // TODO: убрать в будущем. Добавленно сюда, что бы избежать ошибки Notice на версиях цмс, где нету этих полей
                      if(!isset($v['scale_up']))
                        $v['scale_up'] = false;
                      if(!isset($v['fit']))
                        $v['fit'] = 'inside';

                        echo getSizesRow($i, $v);
                     $i++; endforeach;?>
                </table>
                <button type="button" class="btn btn-info" id="add_size"><?php echo ('Добавить размер');?> <b class="icon-plus-sign"></b></button>
            </td>
        </tr>
    </table>
        <script type="text/javascript">
            function init_btn(){
                $('.remove_version').click(function(e){
                    e.preventDefault();
                    $('#tr_'+$(this).attr('rel')).remove();
                });
            }

            $(document).ready(function(){
                $('#add_size').click(function(e){
                    e.preventDefault();
                    var i = $('#sizes tr').length;
                    <?php
                    $sizesRowJS = strtr(CJavaScript::encode(getSizesRow('{i}')), array('{i}' => "'+i+'"));
                    ?>
                    $('#sizes').append($(<?php echo $sizesRowJS ?>));
                    init_btn();
                });

                init_btn();
            });
        </script>


    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'success', 'label'=>_t('Save'))); ?>
    </div>
<?php $this->endWidget();?>
</div><!-- form -->
