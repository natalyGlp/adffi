<div class="form">
<?php
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'settings-form',
    'enableAjaxValidation'=>true,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>
<div class="row">
    <?php echo $form->labelEx($model,'site_url'); ?>
    <?php echo $form->textField($model,'site_url'); ?>
    <?php echo $form->error($model,'site_url'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'site_name'); ?>
    <?php echo $form->textField($model,'site_name'); ?>
    <?php echo $form->error($model,'site_name'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'support_email'); ?>
    <?php echo $form->textField($model,'support_email'); ?>
    <?php echo $form->error($model,'support_email'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'logo'); ?>
    <div>
        <?php echo CHtml::image($model->logo);?>
        <br clear="all"/>
    </div>
    <?php echo $form->fileField($model,'logo'); ?>
    <?php echo $form->error($model,'logo'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'slogan'); ?>
    <?php echo $form->textField($model,'slogan'); ?>
    <?php echo $form->error($model,'slogan'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'homepage'); ?>
    <?php echo $form->textField($model,'homepage'); ?>
    <?php echo $form->error($model,'homepage'); ?>
</div>
<fieldset id="phone-settings">
    <legend><?php echo _t('Phone number settings'); ?></legend>
    <?php
    foreach ($model->phones as $phone => $options) {
        ?>
        <div class="row-fluid row-phone">
            <div class="span1" style="text-align: right">
                <a href="#" class="btn btn-danger action-remove"><i class="icon-trash icon-white"></i></a>
            </div>
            <div class="span4">
                <?php echo CHtml::textField('phone[]', $phone, array(
                        'placeholder' => _t('Phone number'),
                        'class' => 'span12',
                )); ?>
            </div>
            <div class="span4">
                <div class="input-prepend">
                    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
                        'buttons'=>array(
                            array('label' => '', 'items' => array(
                                array('label' => _t('show for all countries'), 'url' => '#',
                                    'linkOptions' => array(
                                        'data-value' => ContactsBlock::PHONE_FILTER_NONE,
                                        ),
                                    ),
                                array('label' => _t('for all except'), 'url' => '#',
                                    'linkOptions' => array(
                                        'data-value' => ContactsBlock::PHONE_FILTER_EXCLUDE,
                                        ),
                                    ),
                                array('label' => _t('show only for'), 'url' => '#',
                                    'linkOptions' => array(
                                        'data-value' => ContactsBlock::PHONE_FILTER_INCLUDE,
                                        ),
                                    ),
                            )),
                        ),
                        'htmlOptions' => array(
                            'class' => 'action-filter'
                            ),
                    )); ?>
                    <?php echo CHtml::textField('phone_filter[]', $options['phone_filter'], array(
                            'placeholder' => _t('Comma separated list, e.g. "UA, RU"'),
                            'class' => 'span10',
                    )); ?>
                    <?php echo CHtml::hiddenField('phone_filter_type[]', $options['phone_filter_type'], array(
                        'class' => 'filter-type-value',
                    )); ?>
                </div>
            </div>
            <?php echo $form->error($model,'phones'); ?>
        </div>
        <?php
    }
    ?>
    <div class="row-fluid">
        <div class="span1"></div>
        <div class="span11">
            <a href="#" class="btn btn-warning action-add"><i class="icon-plus icon-white"></i> <?php echo _t('Add new phone number'); ?></a>
        </div>
    </div>
    <?php
    Yii::app()->clientScript->registerScript(__FILE__.'#phone-settings', '
        $("#phone-settings").on("click", ".action-add", function(e) {
            e.preventDefault();
            var $parent = $(this).parents(".row-fluid");

            var $newRow = $parent.prev().clone();

            $newRow.find("input[type=text], input[type=hidden]").val("");

            $parent.before($newRow.hide());
            $newRow.show(400);
        }).on("click", ".action-remove", function (e) {
            e.preventDefault();
            if($("#phone-settings .row-phone").length == 1) {
                return;
            }

            var $row = $(this).parents(".row-phone");
            $row.hide(400, function() {$row.remove()});
        }).on("click", ".action-filter a", function (e) {
            e.preventDefault();

            if($(this).data("value")) {
                processFilterValue($(this).parents(".row-phone"), $(this).data("value"));
            }
        });

        function processFilterValue($container, value) {
            value = value ? value : 1;
            var $menu = $container.find(".action-filter"),
                $selected = $menu.find("[data-value="+value+"]"),
                $currentSelection = $menu.find(".dropdown-toggle");

            $container.find(".filter-type-value").val(value);
            var children = $currentSelection.children();
            $currentSelection.html($selected.text()+" ").append(children);

            $menu.next("input")[value > 1 ? "show" : "hide"]();
            $menu.parent()[value > 1 ? "addClass" : "removeClass"]("input-prepend");
        }

        $("#phone-settings .filter-type-value").each(function() {
            processFilterValue($(this).parents(".row-phone"), $(this).val());
        });
        ');
    ?>
</fieldset>
<div class="row">
    <?php echo $form->labelEx($model,'address'); ?>
    <?php echo $form->textArea($model,'address'); ?>
    <?php echo $form->error($model,'address'); ?>
</div>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'success', 'label'=>_t('Save'))); ?>
</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
