<div class="row">
    <?php echo $form->labelEx($model,$filter); ?>
    <?php echo $form->textField($model,$filter); ?>
    <label><?php echo _t('Type');?></label>
    <?php echo CHtml::dropDownList('FiltersOptions[options]['.$filter.']', (isset($model->options[$filter]) ? $model->options[$filter] : ''), $model->types, array('class' => 'select_type')); ?>

    <div id="FiltersOptions_options_<?php echo $filter; ?>_drop_down_options">
        <a href="javascript://" data-value="<?php echo $filter; ?>" class="btn add-button btn-primary"><?php echo _t('Add new value');?></a>
        <table id="<?php echo $filter; ?>_table">
            <?php if (isset($model->options['select_options'][$filter]) && is_array($model->options['select_options'][$filter])): ?>
                <?php foreach ($model->options['select_options'][$filter] as $key => $value): ?>
                    <tr id="<?php echo $filter; ?>_tr_<?php echo $key;?>">
                        <td><?php echo CHtml::textField('FiltersOptions[options][select_options]['.$filter.']['.$key.']', $value);?></td>
                        <td><a href="#<?php echo $filter; ?>_tr_<?php echo $key;?>" class="btn del-button btn-warning"><?php echo _t('Delete');?></a></td>
                    </tr>
                <?php endforeach ?>
            <?php endif ?>
        </table>
    </div>
    <div class="row" id="FiltersOptions_options_<?php echo $filter; ?>_taxonomy_id_options">
        <label><?php echo _t('Taxanomy');?></label>
        <?php
            echo CHtml::dropDownList('FiltersOptions[options][taxonomy_id]['.$filter.']', isset($model->options['taxonomy_id'][$filter]) ? $model->options['taxonomy_id'][$filter] : '', Taxonomy::getTaxonomy());
        ?>
    </div>
    <?php echo $form->error($model,$filter); ?>
</div>
<hr/>
