<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'resource-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array('name' => 'resource_id',
            'type' => 'raw',
            'htmlOptions' => array('style' => 'width: 50px;'),
            'value' => '$data->resource_id',
        ),
        array(
            'header' => 'View',
            'type' => 'raw',
            'value' => 'GxcHelpers::renderLinkPreviewResource($data)',
        ),
        array(
            'name' => 'resource_path',
            'type' => 'raw',
            'value' => 'GxcHelpers::renderTextBoxResourcePath($data)',
        ),
        array(
            'name' => 'resource_name',
            'type' => 'raw',
            'value' => 'CHtml::link($data->resource_name,array("' . app()->controller->id . '/view","id"=>$data->resource_id))',
        ),
        array(
            'name' => 'resource_type',
            'type' => 'raw',
            'value' => '$data->resource_type',
        ),
        array(
            'name' => 'resource_where',
            'type' => 'raw',
            'value' => '$data->resource_where',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . app()->controller->id . '/update", array("id"=>$data->resource_id))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => _t('Delete'),
                    'imageUrl' => false,
                ),

            ),
        ),
    ),
));
?>

<script type="text/javascript">
    function selectAllText(textbox) {
        textbox.focus();
        textbox.select();
    }
    $('.pathResource').click(function() { selectAllText(jQuery(this)) });
</script>
