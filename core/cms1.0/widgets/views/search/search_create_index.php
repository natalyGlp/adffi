<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'indexcreate-form',
    'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>
<div class="row">
    <?php echo $form->labelEx($model,'index_id'); ?>
    <?php echo $form->textField($model,'index_id'); ?>
    <?php echo $form->error($model,'index_id'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'objectTypes'); ?>
    <?php echo $form->checkBoxList($model,'objectTypes', SearchLucene::getObjectTypesList()); ?>
    <?php echo $form->error($model,'objectTypes'); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Save'),array('class'=>'bebutton')); ?>
</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
