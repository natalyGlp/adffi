<?php
if (Yii::app()->controller->site) {
    $menu = array(
        array(
            'label' => _t('Set this email pending'),
            'url' => array(
                'set',
                'id' => $model->primaryKey,
                'status' => EmailLog::STATUS_PENDING,
                'site' => $_GET['site'],
            ),
            'linkOptions' => array(
                'class' => 'button',
            ),
        ),
        array(
            'label' => _t('Set this email as done'),
            'url' => array(
                'set',
                'id' => $model->primaryKey,
                'status' => EmailLog::STATUS_DONE,
                'site' => $_GET['site'],
            ),
            'linkOptions' => array(
                'class' => 'button',
            ),
        ),
    );

    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type' => 'pills',
        'buttons' => $menu,
        'htmlOptions' => array(
            'style' => 'margin: 20px 0;',
        ),
    ));
}

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'email',
        'subject',
        array(
            'name' => 'content',
            'type' => 'raw',
            'value' => $this->widget('zii.widgets.CDetailView', array(
                'data' => $model->content,
                'attributes' => $model->dataAttributes,
            ), true),
        ),
        'prettyDate',
        'source',
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => CHtml::image($model->statusIcon, $model->statusLabel),
        ),
    ),
));
