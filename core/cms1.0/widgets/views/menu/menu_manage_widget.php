<?php
$site = Yii::app()->controller->site;
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'taxonomy-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'menu_id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->menu_id',
        ),
        array(
            'name' => 'menu_name',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => 'CHtml::link($data->menu_name,array("' . Yii::app()->controller->id . '/update","id"=>$data->menu_id, "site"=>"' . $site . '"))',
        ),
        array(
            'name' => 'menu_description',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => '$data->menu_description',
        ),
        array(
            'name' => 'lang',
            'type' => 'raw',
            'value' => 'Language::convertLanguage($data->lang)',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{translate}',
            'visible' => Language::model()->isMultilang,
            'buttons' => array(
                'translate' => array(
                    'label' => _t('Translate'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/create", array("guid"=>$data->guid, "site"=>"' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/update", array("id"=>$data->menu_id, "site"=>"' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => _t('Delete'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/delete", array("id"=>$data->menu_id, "site"=>"' . $site . '"))',
                ),
            ),
        ),
    ),
));
