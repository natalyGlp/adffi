<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'taxonomy-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'block_id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->block_id',
        ),
        array(
            'name' => 'name',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => 'CHtml::link($data->name, array("update","id"=>$data->block_id, "site" => Yii::app()->controller->site))',
        ),
        'type',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->controller->createUrl("update", array("id"=>$data->block_id, "site" => Yii::app()->controller->site))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => _t('Delete'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->controller->createUrl("delete", array("id"=>$data->block_id, "site" => Yii::app()->controller->site))',
                ),
            ),
        ),
    ),
));
