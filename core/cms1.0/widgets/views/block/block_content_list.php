<?php
/**
 * @var BlockWidget $blockWidget экземпляр виджета блока
 */

$contentListId = 'content-list-'.$this->getId();
$iframeId = 'contentlist-iframe-'.$this->getId();
?>
<div class="row">
    <div class="content-box ">
        <div class="content-box-header">
            <h3><?php echo _t('Content list');?></h3>
        </div>
        <div class="content-box-content" style="display: block; padding: 0 0 15px 0">
            <div class="tab-content default-tab">
                <ul id="<?php echo $contentListId ?>" style="margin:0px 0 10px 0px"></ul>
            </div>
        </div>
    </div>
</div>

<p><?php echo '<b>'._t('Note:').'</b> '._t('When you create a content list here, it will appear on the above Content list box'); ?></p>
<div class="row" style="border:1px dotted #CCC">
      <iframe id="<?php echo $iframeId ?>"  src="<?php echo bu();?>/becontentlist/create?embed=iframe&site=<?php echo isset($_GET['site']) ? $_GET['site'] : ''; ?>" frameborder="0" onLoad="autoResize(this);" height="30px" width="100%"></iframe>
</div>

<?php
// TODO: спрятать в виджет?
$content_items = array();
if (is_array($contentLists) && !empty($contentLists)) {
    foreach ($contentLists as $obj_id) {
        $temp_object = ContentList::model()->findByPk($obj_id);
        if($temp_object) {
            $content_items['item_'.$temp_object->content_list_id]['id'] = $temp_object->content_list_id;
            $content_items['item_'.$temp_object->content_list_id]['title'] = $temp_object->name;
        }
    }
}
?>
<script type="text/javascript">
    $(function() {
        'use strict';

        var $iframe = $('#<?php echo $iframeId ?>'),
            $list = $('#<?php echo $contentListId ?>');

        var manual_content_list = <?php echo CJavaScript::encode($content_items) ?>;
        $.each(manual_content_list, function(k,v) {
            addContentList(v.title,v.id);
        });

        $iframe
            .on('add', function(e, data) {
                addContentList(data.name, data.id);
            })
            .on('reset', resetIframe)
            ;

        $list
            .on('click', '.content-list-item', function(event) {
                var action = $(event.target).data('action'),
                    contentListId = $(this).data('list-id');

                if(!action || !contentListId)
                    return;

                event.preventDefault();

                switch(action) {
                    case 'update':
                        updateContentList(contentListId);
                    break;
                    case 'delete':
                        deleteContentList(contentListId);
                    break;
                }
            })
            .sortable()
            ;

        function addContentList(linkTitle,linkId) {
            if($list.find('[data-list-id='+linkId+']').length>0){
                 $list.find('.input_title_item_'+linkId).val(linkTitle);
                 $list.find('.link_item_title_item_'+linkId).html(linkTitle);
            } else {
                var nextli='item_'+linkId;
                var li='<li id=\"'+nextli+'\" class=\"content-list-item\" data-list-id=\"'+linkId+'\"><input type=\"hidden\" name=\"content_list_title[]\" class=\"input_title_'+nextli+'\" value=\"'+linkTitle+'\" /><input type=\"hidden\" name=\"<?php echo get_class($blockWidget); ?>[<?php echo $attribute; ?>][]\" class=\"input_id_'+nextli+'\" value=\"'+linkId+'\" /><a class=\"link_item_title_'+nextli+'\" href=\"#\" data-action=\"update\">'+linkTitle+'</a> - <a href=\"#\" data-action=\"delete\">Delete</a></li>';
                $list.append(li);
            }
        }

        function deleteContentList(id) {
            $list.find('[data-list-id='+id+']').remove();
        }

        function updateContentList(id) {
            $iframe.attr('src','<?php echo bu();?>/becontentlist/update/?id='+id+'&embed=iframe&site=<?php echo isset($_GET['site']) ? $_GET['site'] : ''; ?>');
        }

        function resetIframe() {
            $iframe.attr('src','<?php echo bu();?>/becontentlist/create?embed=iframe&site=<?php echo isset($_GET['site']) ? $_GET['site'] : ''; ?>');
        }
    });
</script>
