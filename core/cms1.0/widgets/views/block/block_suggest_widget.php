<p><?php echo _t('Please select a Block: ');?></p>
<div class="form">
<div class="row" id="changeBlocksWrapper">
        <div id="changeBlocks">
         <?php $this->widget('CAutoComplete', array(
                'name'=>'changeBlockForm',
                'url'=>array('suggestBlocks', 'site' => isset($_GET['site']) ? $_GET['site'] : ''),
                'multiple'=>false,
                'mustMatch'=>true,
                'htmlOptions'=>array('size'=>50,'id'=>'change_block_form'),
                'methodChain'=>".result(function(event,item){
                    if(item!==undefined) {
                        \$('#block_title').val(item[0]);
                        \$('#block_id').val(item[1]);
                        \$('#block_type').val(item[2]);
                    }
                })",
            )); ?>
            <input type="hidden" id="block_title" value="" />
            <input type="hidden" id="block_id" value="" />
            <input type="hidden" id="block_type" value="" />
        <input type="button" class="btn js-add" value="<?php echo('Save'); ?>" />
        <input type="button" class="btn js-cancel" value="<?php echo('Cancel'); ?>" />
        </div>
        <script type="text/javascript">
            $(function() {
                $('.js-cancel').click(function() {
                    window.parent.postMessage({
                        action: 'cancel',
                    }, '*');
                });
                $('.js-add').click(function () {
                    if($('#change_block_form').val()=='') {
                        alert('<?php echo _t('Please choose a block before adding!')?>');
                    } else {
                        var title=$('#block_title').val();
                        var id=$('#block_id').val();
                        var type=$('#block_type').val();
                        window.parent.postMessage({
                            action: 'update',
                            data: {
                                id: id,
                                title: title,
                                type: type
                            }
                        }, '*');
                    }
                });
            });
        </script>
    </div>
</div>
