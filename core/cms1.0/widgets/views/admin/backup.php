<?php
echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
));
?>
<div class="alert alert-info">
    <p><?php echo _t('Create backup'); ?>:</p>
    <p>
        <?php echo CHtml::checkBox('blocks_only'); ?>
        <?php echo CHtml::label(_t('Backup only pages, blocks and content lists'), 'blocks_only'); ?>
    </p>
    <p>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => _t('Backup database'),
        )); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'disabled' => 'disabled',
            'label' => _t('Flush database'),
            'htmlOptions' => array('name' => 'flushDb'),
        )); ?>
    </p>
    <?php
    echo CHtml::endForm();
    ?>
</div>

<?php
echo CHtml::beginForm('', 'post', array(
    'enctype' => 'multipart/form-data',
));
?>
<div class="alert alert-warning">
    <div class="row-fluid">
        <label for="dump"><?php echo _t('Upload backup'); ?>:</label>
        <input required type="file" name="dump" id="dump" />
    </div>
    <div class="row-fluid">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => _t('Upload'),
        )); ?>
    </div>
</div>
<?php
echo CHtml::endForm();

$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'name' => 'name',
            'header' => 'Имя файла',
        ),
        array(
            'name' => 'size',
            'header' => 'Размер',
            'value' => 'formatSize($data["size"])',
        ),
        array(
            'name' => 'time',
            'header' => 'Дата',
            'value' => 'Yii::app()->dateFormatter->formatDateTime($data["time"])',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{download} {restore} {delete}',
            'deleteConfirmation' => 'Вы действительно хотите удалить этот бекап?',
            'buttons' => array(
                'download' => array(
                    'label' => 'Скачать бекап',
                    'url' => '"?download=" . CHtml::encode($data["name"]) ."&site='.Yii::app()->controller->site.'"',
                    'icon' => 'icon-download-alt',
                ),
                'restore' => array(
                    'label' => 'Восстановить бекап',
                    'url' => '"?restore=" . CHtml::encode($data["name"]) ."&site='.Yii::app()->controller->site.'"',
                    'icon' => 'icon-refresh',
                ),
                'delete' => array(
                    'url' => '"?delete=" . CHtml::encode($data["name"]) ."&site='.Yii::app()->controller->site.'"',
                    'label' => 'Удалить бекап',
                ),
            ),
        ),
    ),
    'ajaxUpdate' => false,
));

function formatSize($value, $decimals = 2, $base = 1024)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');
    for ($i = 0; $base <= $value; $i++) {
        $value = $value / $base;
    }

    return round($value, $decimals) . $units[$i];
}
