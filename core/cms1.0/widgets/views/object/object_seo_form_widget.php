
<?php ob_start(); ?>
    <?= $form->label($model, 'object_author_name'); ?>
    <?= $form->textField($model, 'object_author_name', array(
        'id' => 'txt_object_author_name'
    )) ?>
    <?= $form->error($model, 'object_author_name') ?>

    <?= $form->labelEx($model, 'tags') ?>
    <?php $this->widget('CAutoComplete', array(
        'model' => $model,
        'attribute' => 'tags',
        'url' => array('suggestTags', 'site' => Yii::app()->controller->site),
        'multiple' => true,
        'htmlOptions' => array(
            'size' => 50,
            'id' => 'txt_object_tags',
        ),
    )); ?>
    <?= $form->error($model, 'tags') ?>
    <?= $form->labelEx($model, 'comment_status') ?>
    <?= $form->dropDownList($model, 'comment_status', ConstantDefine::getObjectCommentStatus()) ?>
    <?= $form->error($model, 'comment_status') ?>
<?php $summaryContent = ob_get_clean(); ?>


<?php ob_start(); ?>
    <?= $form->label($model, 'object_slug') ?>
    <?= $form->textField($model, 'object_slug', array(
        'id' => 'txt_object_slug',
        'class' => 'txt_object_slug',
    )) ?>
    <?= $form->error($model, 'object_slug') ?>
    <?= $form->label($model, 'object_title') ?>
    <?= $form->textField($model, 'object_title', array(
        'id' => 'txt_object_title'
    )) ?>
    <?= $form->error($model, 'object_title') ?>
    <?= $form->label($model, 'object_description') ?>
    <?= $form->textArea($model, 'object_description', array(
        'id' => 'txt_object_description',
    )) ?>
    <?= $form->error($model, 'object_description') ?>
<?php $seoContent = ob_get_clean(); ?>

<?php
$this->widget('bootstrap.widgets.TbTabs', array(
    'type' => 'tabs',
    'tabs' => array(
        array(
            'label' => _t('Summary'),
            'content' => $summaryContent,
            'active' => true
        ),
        array(
            'label' => _t('SEO'),
            'content' => $seoContent,
        ),
    ),
));


