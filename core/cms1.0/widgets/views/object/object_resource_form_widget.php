<?php
    // prettyPhoto для попапов редактирования/добавления ресурсов
    GxcHelpers::registerCss('prettyPhoto/css/prettyPhoto.css');
    GxcHelpers::registerJs('prettyPhoto/jquery.prettyPhoto.js');
    Yii::app()->clientScript->registerScript(__FILE__.'#pretty-photo', '
        $("a[rel^=\'prettyPhoto\']").prettyPhoto({show_title: true,social_tools: \'\',deeplinking: false});
    ');
?>
<script type="text/javascript">

 function buildResourceList(id,link,key,file_type){
    var current_resource_count=parseInt($('#resource_'+key+'_upload_count').val());
    var current_resource_max=parseInt($('#resource_'+key+'_upload_max').val());
    var next_resource_count=current_resource_count+1;

    <?php
        $imagesUrl = GxcHelpers::assetUrl('images/content_icons', 'backend');
	?>
    var link_resource_icon='<?= $imagesUrl;?>/'+file_type+'.png';
    //console.log(link_resource_icon);
    if(file_type=='image' || file_type=='videoembed'){
    	link_resource_icon=link;
    }

    $('#resource_'+key+'_upload_count').val(next_resource_count);
	var resource_string=
		'<li id="list_id_resource_'+key+'_'+next_resource_count+'">'+
			'<input type="hidden" id="resource_'+key+'_link_'+next_resource_count+'" name="resource['+key+'][link][]" value="'+link+'" />'+
			'<input type="hidden" id="resource_'+key+'_resid_'+next_resource_count+'" name="resource['+key+'][resid][]" value="'+id+'" />'+
			'<input type="hidden" id="resource_'+key+'_type_'+next_resource_count+'" name="resource['+key+'][type][]" value="'+file_type+'" />'+
	            '<div class="item_div_wrapper">'+
	                '<div class="item_name"><img style="background:#fff;padding:5px; border:1px dotted #CCC" src="<?php echo $this->controller->site ? "http://".$this->controller->site : false; ?>'+link_resource_icon+'" width="50" /></div>'+
	                '<div class="item_buttons" style="margin-top:3px; margin-left:5px">'+
	                '<a class="icon-pencil" onclick="$.prettyPhoto.open(this.href + \'&embed=1&iframe=true\',\'<?php echo _t('Update Resource');?>\',\'\'); return false" href="<?php echo bu(); ?>/beresource/update/?id='+id+'&site=<?php echo isset($_GET['site'])?$_GET['site']:'';?>" name="editItem"></a>&nbsp;'+
	                '<a class="icon-remove" href="javascript:void(0)" onclick="fnRemoveResourceItem(\'list_id_resource_'+key+'_'+next_resource_count+'\',\''+key+'\');" name="removeItem"></a>'+
	                '</div>'+
	            '</div>'+
	 	'</li>';

    $('#'+key+'_resource_list li.clear').remove();
	$('#'+key+'_resource_list').append(resource_string);
	$('#resource_'+key+'_upload_count').val(next_resource_count.toString());
	$('#resource_modified').val(1);
}

function fnRemoveResourceItem(item,key){
    // TODO: достаточно просто посчитать количество DOM нодов
   	$("#"+item).remove();
   	var resourcesCount=$('#resource_'+key+'_upload_count').val()*1;
   	resourcesCount=resourcesCount-1;
	if(resourcesCount<0) resourcesCount=0;
	$('#resource_'+key+'_upload_count').val(resourcesCount.toString());
	$('#resource_modified').val(1);
}

function sortResourceList(){
	$('.resource_list').sortable({
	   update: function(event, ui) {
	   		$('#resource_modified').val(1);
	   },
	   change: function(event, ui) {
	   		var key=$(this).attr('id').toString().replace('_resource_list','');
	   		$('#resource_modified').val(1);
	   }
	});
}

function doUploadResource(type,width,height, multiple){
  if(type=='thumbnail'){
    width = 'disabled';
    height = 'disabled';
  }
  if (window.event) // Добавленно, что бы избежать ошибки Undefined
    window.event.preventDefault(); // что это делает?? (C) Свят
  multiple = multiple ? 'true' : 'false';
	var current_resource_count=parseInt($('#resource_'+type+'_upload_count').val());
	var current_resource_max=parseInt($('#resource_'+type+'_upload_max').val());
	var next_resource_count=current_resource_count+1;
	if(next_resource_count<=current_resource_max){
        var url='<?php echo bu() ?>/beresource/createframe?multiple='+multiple+'&embed=1&content_type=<?php echo $type;?>&parent_call=true&w='+width+'&h='+height+'&type='+type+'&site=<?php echo isset($_GET['site'])?$_GET['site']:'';?>&iframe=true';
        $.prettyPhoto.open(url,'<?php echo _t('Upload Resource');?>','');
	} else {
		alert('<?php echo _t('Max file Upload Reached!'); ?>');
	}
    return false;
}

function afterUploadResource(resource_id,resource_path,type,file_type){
	buildResourceList(resource_id,resource_path,type,file_type);
    try
    {
	   $.prettyPhoto.close(); // если у нас была мультизагрузка ресурсов, то закрыть prettyPhoto мы сможем только один раз
    }catch(e){}
	return;
}
</script>
<!--Start Resource Box -->
<br/>
<?php
$tabs = array();
if (is_array($content_resources) && count($content_resources)) {
    foreach ($content_resources as $resourceType => $value) {
        $content = '';
        $width= isset($value['width']) ? $value['width'] : '';
        $height= isset($value['height']) ? $value['height'] : '';

        if ($model->isNewRecord) {
            $list_current_resource = GxcHelpers::getArrayResourceObjectBinding($resourceType);
        } else {
            $list_current_resource = GxcHelpers::getResourceObjectFromDatabase($model, $resourceType, '_cms_admin_thumb_');
        }

        $content .= '<input type="hidden" id="resource_'.$resourceType.'_upload_max" value="'.$value['max'].'" />
                    <input type="hidden" id="resource_'.$resourceType.'_upload_count" value="0" />
                    <ul class="resource_list" id="'.$resourceType.'_resource_list" style="margin:0; padding:0px; clear:both;" ></ul>
                        <script type="text/javascript">';
        foreach ($list_current_resource as $current_resource) {
            $content .= 'buildResourceList("'.$current_resource['resid'].'", "'.$current_resource['link'].'", "'.$resourceType.'", "'.$current_resource['type'].'");';
        }
        $content .= '</script>';
        $content .= '<div class="button_add" style="text-align:right; padding-right:10px; padding-top:10px; padding-bottom:10px"><span class="label label-warning">Максимум файлов: <b>'.$value['max'].'</b></span> ';

        if (!isset($value['allowMultiple']) || (isset($value['allowMultiple']) && $value['allowMultiple'])) {
            $content .= '<input type="button" onClick="return doUploadResource(\''.$resourceType.'\',\''.$width.'\',\''.$height.'\', true); return false;" class="btn" name="'.$resourceType.'_button_multiple" id="'.$resourceType.'_button_multiple" value="'._t('Add multiple').'"/> ';
        }
        $content .= '<input type="button" onClick="return doUploadResource(\''.$resourceType.'\',\''.$width.'\',\''.$height.'\'); return false;" class="btn btn-primary" name="'.$resourceType.'_button" id="'.$resourceType.'_button" value="'._t('Add').'"/>
                    </div>';

        $tabs[] = array(
            'label' => $value['name'],
            'content' => $content,
        );
    }

    $tabs[0]['active'] = true;
}
?>


<?php
echo '
<input type="hidden" id="resource_modified" value="0" />
<script type="text/javascript">
    $("#resource_modified").val(0);
    sortResourceList();
</script>
';
$this->widget('bootstrap.widgets.TbTabs', array(
    'type' => 'tabs',
    'tabs' => $tabs,
));
?>



<!-- End Resource Box -->
