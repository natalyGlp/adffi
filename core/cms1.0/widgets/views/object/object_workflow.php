<div class="row-fluid">
    <?php echo $form->label($model, 'object_date'); ?>
    <?php $this->widget('cms.extensions.datepicker.CJuiDateTimePicker', array(
        'model' => $model,
        'attribute' => 'object_date',
        'options' => array(
            'dateFormat' => 'dd.mm.yy ',
            'timeFormat' => 'HH:mm:ss',
            'changeMonth' => true,
        ),
        'htmlOptions' => array(
            'class' => 'span12',
        ),
    )); ?>
    <?php echo $form->error($model, 'object_date'); ?>
</div>

<div class="row-fluid">
    <?php echo $form->labelEx($model, 'object_status'); ?>
    <?php echo $form->dropDownList($model, 'object_status', $content_status, array(
        'class' => 'span12',
    )); ?>
</div>

<div class="row-fluid">
    <?php echo $form->checkBox($model, 'is_main'); ?>
    <?php echo $form->labelEx($model, 'is_main'); ?>
</div>

<div class="row-fluid">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => _t($model->isNewRecord ? 'Create' : 'Save'),
        'buttonType' => 'submit',
        'type' => 'primary',
        'block' => true,
    )); ?>
</div>
