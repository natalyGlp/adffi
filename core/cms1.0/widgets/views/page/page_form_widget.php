<?php
$site = Yii::app()->controller->site;

if (!empty($site) && Yii::app()->id == 'satellite') {
    $satelliteSite = Sites::model()->find(array(
        'condition' => 'url LIKE :url',
        'params' => array(
            ':url' => "%$site%",
            ),
        ));

    $model->layout = $satelliteSite ? $satelliteSite->theme : 'base';
    if ($model->isNewRecord && !empty($satelliteSite->layout)) {
        $model->display_type = $satelliteSite->layout;
    }
}

$themes = GxcHelpers::getAvailableLayouts(true);
$themeRegions = array();
$themeLayouts = array();
if (!empty($themes)) {
    $themeMeta = Page::getLayoutsAndRegions($model->layout ? $model->layout : key($themes));
    $themeRegions = $themeMeta['regions'];
    $themeLayouts = array_combine($themeMeta['types'], $themeMeta['types']);
}
?>
<div class="form">
    <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id'=>'page-form',
            'enableAjaxValidation'=>true,
        ));
    ?>
    <?php echo $form->errorSummary($model); ?>
    <div id="language-zone">
        <?php
        // TODO: обьединить с формой добавления обьектов
        if ($model->isNewRecord) {
            if (count($versions) > 0) {
                ?>
                <div class="row">
                <?php
                    echo "<strong style='color:#DD4B39'>"._t("Translated Version of:")."</strong><br />";
                foreach ($versions as $version) {
                    echo "<br /><b>- ".$version."</b>";
                }
                ?>
                </div>
                <?php
            }

            if (Language::model()->isMultilang) {
                ?>
                <div class="row">
                    <?php echo $form->labelEx($model, 'lang'); ?>
                    <?php echo $form->dropDownList($model, 'lang', Language::items($lang_exclude), array(
                        'options' => array(
                            array_search(Yii::app()->language, Language::items($lang_exclude, false)) => array('selected'=>true)
                            )
                    )); ?>
                    <?php echo $form->error($model, 'lang'); ?>
                    <div class="clear"></div>
                </div>
                <?php
            } else {
                echo $form->hiddenField($model, 'lang', array('value' => Language::mainLanguage()));
            }
        }
        ?>
    </div>
    <div>
        <div class="pull-left span6" style="margin-left:0;">
            <div class="row">
                <?php echo $form->labelEx($model, 'name'); ?>
                <?php echo $form->textField($model, 'name', array('class' => 'block_w100')); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>
            <div class="row" >
                <?php echo $form->labelEx($model, 'title'); ?>
                <?php echo $form->textField($model, 'title', array('class' => 'block_w100')); ?>
                <?php echo $form->error($model, 'title'); ?>
            </div>
            <div class="row" >
                <?php echo $form->labelEx($model, 'slug'); ?>
                <?php echo $form->textField($model, 'slug', array('class' => 'block_w100')); ?>
                <?php echo $form->error($model, 'slug'); ?>
            </div>
        </div>
        <div class="pull-left span8">
            <div class="row" >
                <?php echo $form->labelEx($model, 'breadcrumb_title'); ?>
                <?php echo $form->textField($model, 'breadcrumb_title', array('class' => 'block_w100')); ?>
                <?php echo $form->error($model, 'breadcrumb_title'); ?>
            </div>
            <div class="row">
                    <?php echo $form->labelEx($model, 'description'); ?>
                    <?php echo $form->textArea($model, 'description', array('style'=>'min-height:85px', 'class' => 'block_w100')); ?>
                    <?php echo $form->error($model, 'description'); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'status', array('style'=>'display:inline')); ?>
        <?php echo $form->dropDownList($model, 'status', ConstantDefine::getPageStatus());?>
        <?php echo $form->error($model, 'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->checkBox($model, 'allow_index', array()); ?>
        <?php echo $form->labelEx($model, 'allow_index', array('style'=>'display:inline')); ?>
        <?php echo $form->error($model, 'allow_index'); ?>

        <?php echo $form->checkBox($model, 'allow_follow', array()); ?>
        <?php echo $form->labelEx($model, 'allow_follow', array('style'=>'display:inline')); ?>
        <?php echo $form->error($model, 'allow_follow'); ?>
    </div>

    <div class="row" style="border-top: 1px dotted #CCC; padding-top:20px">
       <?php echo $form->labelEx($model, 'parent'); ?>
        <p class="hint"><?php echo _t('Use Select box or Autocomplete box for Parent Select:');?></p>
        <?php
            echo CHtml::dropDownList('parent', $model->parent, Page::getParentPages(false, $model->page_id), array(
                'id'=>'parent_page_select'
            ));
        ?>

        <?php
        $this->widget('CAutoComplete', array(
            'name'=>'suggest_page',
            'url'=>array('suggestPage', 'site' => isset($_GET['site']) ? $_GET['site'] : ''),
            'value'=>Page::getPageName($model->parent),
            'multiple'=>false,
            'mustMatch'=>true,
            'htmlOptions'=>array('size'=>50, 'class'=>'maxWidthInput', 'id'=>'form_suggest_page'),
            'methodChain'=>".result(function (event,item) { if (item!==undefined) \$(\"#parent_value\").val(item[1]); \$(\"#parent_page_select\").val(item[1])})",
        ));
        ?>

        <input type="button" class="btn js-change-parent" name="btnchangeParent" value="<?php echo _t('Apply Parent Blocks');?>" />
        <?php echo $form->hiddenField($model, 'parent', array(
            'id'=>'parent_value',
            'value'=>$model->parent===null ? 0 : $model->parent
        )); ?>
        <?php echo $form->error($model, 'parent'); ?>
        <?php
        if (Yii::app()->id == 'satellite') {
            echo $form->hiddenField($model, 'layout');
        } else {
            ?>
            <div class="row">
                <?php echo $form->labelEx($model, 'layout'); ?>
                <?php echo $form->dropDownList($model, 'layout', $themes); ?>
                <?php echo $form->error($model, 'layout'); ?>
            </div>
        <?php
        }
        ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'display_type', array()); ?>
        <?php echo $form->dropDownList($model, 'display_type', $themeLayouts); ?>
        <?php echo $form->error($model, 'display_type'); ?>
    </div>

    <?php
    if (!$model->isNewRecord) {
        ?>
        <input type="button" class="btn js-turn-back" name="btnturnBack" value="<?php echo _t('Turn back');?>" />
        <?php
    }
    ?>

    <?php
        $this->widget('cms.widgets.page.RegionsFormWidget', array(
            'model' => $model,
            'regionsBlocks' => $regions_blocks,
            'regions' => $themeRegions,
            ));
    ?>

    <div class="row form-actions">
        <?php echo CHtml::submitButton(_t('Save'), array('class'=>'btn bebutton')); ?>
    </div>
<?php $this->endWidget('CActiveForm'); ?>
</div><!-- form -->
<script type="text/javascript">
    (function () {
        // addInheritBtn
        var $btn = $('<input>');
        $btn.val('<?php echo _t('Inherit from Parent'); ?>');
        $btn.attr('type', 'button');
        $btn[0].className = 'btn js-region-inherit';

        var $tpl = $('#template-region');
        var $tplContent = $('<div>').html($tpl.html());
        $tplContent.find('.region-actions').prepend($btn);
        $tpl.html($tplContent.html());
    }());

    $(function () {
        $('#Page_layout').change(getRegionsByLayouts);

        $('#parent_page_select').change(function () {
            $('#parent_value').val( $('#parent_page_select').val());
            $('#form_suggest_page').val( $('#parent_page_select option:selected:first').html());
        });

        $('body')
            .on('click', '.js-turn-back', turnBack)
            .on('click', '.js-change-parent', changePageParent)
            .on('click', '.js-region-inherit', inheritRegionFromParent)
            ;

        <?php if ($model->isNewRecord) : ?>
            CopyString('#Page_name', '#Page_title', '');
            CopyString('#Page_name', '#Page_slug', 'slug');
        <?php endif; ?>

        function changePageParent(event) {
            event.preventDefault();

            requestParentBlocks($('#parent_value').val());
        }

        function turnBack(event) {
            event.preventDefault();

            requestParentBlocks(<?php echo CJavaScript::encode($model->page_id) ?>);
        }

        function requestParentBlocks(parent) {
            postRequrest({
                url: <?php echo CJavaScript::encode(Yii::app()->controller->createUrl('changeParent', array('site' => $site))) ?>,
                data: {
                    parent: parent,
                },
                success: function (data) {
                    createRegionsFromJson(data);
                    if(data.blocks) {
                        regions.insertBlocks(data.blocks);
                    }
                }
            });
        }

        function getRegionsByLayouts() {
            postRequrest({
                url: <?php echo CJavaScript::encode(Yii::app()->controller->createUrl('changeLayout', array('site' => $site))) ?>,
                data: {
                    layout: $('#Page_layout').val(),
                },
                success: function (data) {
                    createRegionsFromJson(data);
                    regions.turnBack();
                }
            });
        }

        function inheritRegionFromParent(event) {
            event.preventDefault();

            postRequrest({
                url: <?php echo CJavaScript::encode(Yii::app()->controller->createUrl('inheritParent', array('site' => $site))) ?>,
                data: {
                    region: regions.getCurrentRegion(),
                    parent: $('#parent_value').val(),
                    layout: $('#Page_layout').val()
                },
                success: function(data) {
                    regions.get(regions.getCurrentRegion())
                        .reset()
                        .create(data.blocks)
                    ;
                }
            });
        };

        function postRequrest(options) {
            options = $.extend({
                    data: {},
                }, options);

            $.ajax({
                url: options.url,
                type: 'post',
                dataType: 'json',
                success: options.success,
                data: $.extend({
                        YII_CSRF_TOKEN: <?php echo CJavaScript::encode(Yii::app()->getRequest()->getCsrfToken()) ?>
                    }, options.data)
            });
        };

        //Function to get the Page Regions
        function createRegionsFromJson(data) {
            regions
                .reset()
                .create(data.regions)
                ;

            handleLayouts(data);
        }

        function handleLayouts(data)
        {
            $('#Page_layout').val(data.layout);

            var $layoutSelect = $('#Page_display_type');
            var currentLayout = data.parentType ? data.parentType : $layoutSelect.val();
            var html = [];
            $layoutSelect.empty();
            $.each(data.types, function (key, val) {
                html.push('<option value="'+val+'">'+val+'</option>');
            });
            $layoutSelect
                .html(html.join())
                .val(currentLayout)
                ;
        }
     });
</script>
