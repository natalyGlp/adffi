<?php
$site = Yii::app()->controller->site;
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'taxonomy-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->id',
        ),
        array(
            'name' => 'title',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->title',
        ),
        array(
            'name' => 'path',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->path',
        ),

        /*        array(
        'header'=>'Кол-во изображений',
        'value'=>'count($data->pictures)',

        ),*/
        array(
            'name' => 'image_count',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->image_count',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/edit", array("id"=>$data->id, "site" => "' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Photos'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/photos", array("id"=>$data->id, "site" => "' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'afterDelete' => 'function(link,success,data){ if(success) $("#statusMsg").html(data); }',
            'template' => '{delete}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('delete'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/delete", array("id"=>$data->id, "site" => "' . $site . '"))',
                ),
            ),
        ),
    ),
));
