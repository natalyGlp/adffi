<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'mailtemplate-form',
    'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <?php echo $form->labelEx($model,'mail_title'); ?>
    <?php echo $form->textField($model,'mail_title'); ?>
    <?php echo $form->error($model,'mail_title'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'mail_text'); ?>
    <?php echo $form->textArea($model,'mail_text'); ?>
    <?php echo $form->error($model,'mail_text'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'mail_slug'); ?>
    <?php echo $form->textField($model,'mail_slug'); ?>
    <?php echo $form->error($model,'mail_slug'); ?>
</div>
<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Save'),array('class'=>'btn')); ?>
</div>
<div class="row">
    <h2>Previev</h2>
    <div class="previev">
        <?php echo $model->prepareMail(); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
<?php if($model->isNewRecord) : ?>
<script type="text/javascript">
 CopyString('#MailTemplate_mail_title','#MailTemplate_mail_slug','slug');
</script>
<?php endif; ?>
