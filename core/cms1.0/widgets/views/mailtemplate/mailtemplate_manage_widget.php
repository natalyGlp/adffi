<?php
$site = Yii::app()->controller->site;
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'id' => 'mailtemplate-grid',
    'enableHistory' => true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'summaryText' => _t('Displaying') . ' {start} - {end} ' . _t('in') . ' {count} ' . _t('results'),
    'columns' => array(
        array(
            'name' => 'mail_id',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridmaxwidth',
            ),
            'value' => '$data->mail_id',
        ),
        array(
            'name' => 'mail_title',
            'type' => 'raw',
            'htmlOptions' => array(
                'class' => 'gridLeft',
            ),
            'value' => 'CHtml::link($data->mail_title,array("' . Yii::app()->controller->id . '/update","id"=>$data->mail_id, "site" => "' . $site . '"))',
        ),
        'mail_slug',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'label' => _t('Edit'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/update", array("id"=>$data->mail_id, "site" => "' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'label' => _t('Delete'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/delete", array("id"=>$data->mail_id, "site" => "' . $site . '"))',
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{send}',
            'buttons' => array(
                'send' => array(
                    'label' => _t('Send'),
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("' . Yii::app()->controller->id . '/send", array("id"=>$data->mail_id))',
                ),
            ),
        ),
    ),
));
