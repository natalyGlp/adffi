<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'language-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'lang_name'); ?>

	<?php echo $form->textFieldRow($model,'lang_short',array('size'=>10,'maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'lang_desc'); ?>

	<?php echo $form->dropDownListRow($model,'lang_required', $model->getDefaultLang(true)); ?>

	<?php echo $form->dropDownListRow($model,'lang_active',$model->getStatus(true)); ?>

	<div class="row form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => $model->isNewRecord ? 'Create' : 'Save')); ?>
	</div>

<?php $this->endWidget(); ?>
