<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $dataProvider,
    'id' => 'popup_' . $id,
    'columns' => array(
        array(
            'name' => 'prod.photo',
            'value' => '"<div class=\"quantity\">" . GxcHelpers::getObjectThumb($data->prod_id) . "<span>" . $data->quantity . "</span></div>"',
            'type' => 'raw',
        ),

        // 'prod.code',
        'prod.primaryKey',
        'prod.object_name',
        array(
            'name' => 'price',
            'header' => 'Цена / Сумма',
            'value' => '$data->getPrice(true) . " / " . $data->getTotalPrice(true)',
        ),
        array(
            'name' => 'meta',
            'type' => 'raw',
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
            'buttons' => array(
                'view' => array(
                    'label' => _t('View'),
                    'imageUrl' => false,
                    'visible' => '!!$data->prod',
                    'url' => '$data->prod->getObjectLink()',
                ),
            ),
        ),
    ),
));
