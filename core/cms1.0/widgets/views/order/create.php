<?php
/* @var $form CActiveForm*/
?>
<div class="form">
<?php
    echo $form->render();
    $orderModel = $form['order']->model;
    $orderAdressModel = $form['address']->model;
?>
</div>

<script type="text/template" id="productTitle">
    <div class="product-card__title">
      <span class="product-card__thumb">{thumb}</span>
      <b>{name}<span class="js-edited" style="display:none;">*</span></b>
      <br />
      <small class="muted">{prodId}</small>
    </div>
</script>
<script type="text/template" id="productTemplate">
  <div class="product-card js-check-row well" id="check-row-{prodId}">
    {productTitle}
    <div class="product-card__quantity">
      Кол-во: <b class="js-quantity">{quantity}</b>
    </div>
    <a href="#" class="product-card__control product-card__control--minus icon icon-chevron-down js-minus"></a>
    <a href="#" class="product-card__control product-card__control--plus icon icon-chevron-up js-plus"></a>
    <a href="#" class="product-card__control product-card__control--remove icon icon-trash js-remove"></a>
    <input type="hidden" name="OrderCheck[{prodId}]" value="{quantity}" class="js-quantity-input" />
  </div>
</script>
<?php
$js = '
$(function() {
    $("#Object_object_id").autocomplete({
        source: "'.$this->controller->createUrl('acprod').'",
        focus: function( event, ui ) {
            $( "#Object_object_id" ).val( ui.item.value );
            return false;
        },
        select: function( event, ui ) {
            // При выборе нам нужно добавить скрытые поля и строку с инфой о выбранном товаре
            $( "#Object_object_id" ).val("");
            if($("#checkContainer .check-row-"+ui.item.value).length > 0) {
                return false; // такой продукт уже есть
            }

            var data = ui.item;
            addProduct($.extend(data, {
                prodId: ui.item.value*1,
                quantity: 1
            }));
            $("#checkContainer").parents("form").trigger("calculate");

            //а также можно затереть противные красные надписи валидатора
            $("#Object_object_id").siblings().removeClass("error").removeClass("errorMessage");

            return false;
        }
    });

    $("#Object_object_id").data( "autocomplete" )._renderItem = function( ul, item ) {
        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + getProductTitle(item) + "</a>" )
            .appendTo( ul );
    };

    window.getProductTitle = function(data) {
        var productTitle = $("#productTitle").html();

        productTitle = fillHtmlWithData(productTitle, data);

        return productTitle;
    }

    window.addProduct = function(data) {
        var html = $("#productTemplate").html();

        html = fillHtmlWithData(html, $.extend({
            productTitle: getProductTitle(data)
        }, data));

        $("#checkContainer").append(html);
    }

    function fillHtmlWithData(html, data) {
        $.each(data, function(key, value) {
            html = html.replace(new RegExp("\\{"+key+"\\}", "g"), value);
        });

        return html;
    }

    function changeQuantity(delta) {
        var $span = $(this).parent().find(".js-quantity");
        var val = $span.text()*1+delta;
        if (val > 0) {
            $span.html(val);
            $(this).parent().find(".js-quantity-input").val(val);
        }
    }

    $("body").off("click.orderCreate").on("click.orderCreate", ".js-check-row .icon", function(e) {
        e.preventDefault();
        var $checkRow = $(this).parents(".js-check-row");
        var $form = $(this).parents("form");

        if($(this).hasClass("js-minus")) {
            changeQuantity.call(this, -1);
            $checkRow.find(".js-edited").show();
        }
        if($(this).hasClass("js-plus")) {
            changeQuantity.call(this, 1);
            $checkRow.find(".js-edited").show();
        }
        if($(this).hasClass("js-remove")) {
            $checkRow.hide("fast", function() {
                $checkRow.remove();
                $form.trigger("calculate");
            });
        }
        $form.trigger("calculate");
    });

    $("body").on({
        mouseenter: function(){
            $(this).addClass("alert-info");
        },
        mouseleave: function(){
            $(this).removeClass("alert-info");
        }
    }, ".js-check-row");
    $("#Currency_id").change(function() {

        $("#Order_currency_id").val($(this).val());
        $(this).parents("form").trigger("calculate");
    });
    $("#Order_currency_id").change(function() {
        $(this).parents("form").trigger("calculate");
        $("#Currency_id").val($(this).val());
    });
});
';

// Добавим товары, если они есть в чеке
$js .= '$(function() {';
foreach ($check as $checkProd) {
    $options = array(
        'prodId' => $checkProd->prod->primaryKey,
        'name' => $checkProd->prod->object_name,
        'quantity' => $checkProd->quantity,
        'thumb' => GxcHelpers::getObjectThumb($checkProd->prod),
    );
    $js .= 'addProduct('.CJavaScript::encode($options).');';
}
$js .= '});';

// пересчет сумы заказа
$js .= '
  function calculatePrice(form) {
    $.ajax({
        url: '.CJavaScript::encode($this->controller->createUrl('price', array('id'=>$orderModel->primaryKey))).',
        type: "POST",
        data: form ? $(form).serialize() : {}, // отправка всей формы нужна, что бы экшен подсчитал новые товары в чеке
        success: function(data) {
            $(".currencySymbol").html(data.currencySymbol);
            $(".orderPrice").val((data.totalPrice*1.0).toFixed(2));
        },
    });
  };
  $("body")
    .off("calculate").on("calculate", "form", function() {
        calculatePrice(this);
    })
    .off("click.calculate").on("click.calculate", ".js-recalculate", function() {
        var $form = $(this).parents("form");
        if($form.length > 0)
        $form.trigger("calculate");
        return false;
    })
  ;
  calculatePrice();
  ';

Yii::app()->getClientScript()
  ->registerScript(__CLASS__.'#shopProdAutoComplete', $js)
  // API автокомплита адресов
  ->registerScript(__CLASS__.'#orderAdressAutoComplete', '
    (function() {
        if(!window.google) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = "http://maps.googleapis.com/maps/api/js?v=3&libraries=places&callback=initAddressAutocomplete";
            document.body.appendChild(script);
        } else {
            initAddressAutocomplete();
        }
    }());

    function initAddressAutocomplete() {
        var $acField = $("#address-autocomplete");
        var autocomplete = new google.maps.places.Autocomplete($acField[0], {
            types: ["geocode"],
        });

        $acField.on("keypress", function(e) {
            if(e.which == 13) {
                return false;
            }
        });

        google.maps.event.addListener(autocomplete, "place_changed", function() {
            var components = autocomplete.getPlace().address_components;
            var addressMap = {
                route: "'.CHtml::activeId($orderAdressModel, 'street').'",
                street_number: "'.CHtml::activeId($orderAdressModel, 'house').'",
                locality: "'.CHtml::activeId($orderAdressModel, 'city').'",
                country: "'.CHtml::activeId($orderAdressModel, 'country').'"
            };

            for(var i = 0; i < components.length; i++) {
                var key = components[i].types[0];

                if(addressMap[key]) {
                    console.log("#"+addressMap[key])
                    $("#"+addressMap[key]).val(components[i].short_name);
                }
            }
        });
    }
    ', CClientScript::POS_END);
