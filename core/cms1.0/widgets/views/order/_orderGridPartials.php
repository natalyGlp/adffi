<?php
/**
 * Выводит превьюшки товаров в заказе и их количество
 *
 * @var  Order $order
 * @var  string $type
 */

switch ($type) {
    case "thumbs":
        foreach ($order->check as $check) {
            echo '<div class="quantity">' . GxcHelpers::getObjectThumb($check->prod_id) . '<span>' . $check->quantity . '</span></div>';
        }
        break;
    case "client":
        if (empty($order->user_id)) {
            echo CHtml::encode($order->client->first_name) . "<br />" . CHtml::encode($order->client->last_name);
        } else {
            echo CHtml::link(CHtml::encode($order->user->first_name) . "<br />" . CHtml::encode($order->user->last_name), Yii::app()->controller->createUrl("user", array("id" => $order->user_id)), array("class" => "ajaxInfo"));
        }
        break;

    case 'clientContacts':
        $emailCssClasses = array(
            'confirmed' => 'label label-success',
            'notConfirmed' => 'label label-important',
            'guest' => 'label label-inverse',
        );
        $html = array();

        $user = empty($order->user_id) ? $order->client : $order->user;
        $email = CHtml::encode($user->email);
        if (empty($order->user_id)) {
            $cssClass = $emailCssClasses["guest"];
        } elseif ($order->user->isActive) {
            $cssClass = $emailCssClasses["confirmed"];
        } else {
            $cssClass = $emailCssClasses["notConfirmed"];
        }
        if (!empty($email)) {
            $html[] = CHtml::tag('span', array(
                'class' => $cssClass,
            ), $email);
        }

        $html[] = CHtml::encode($order->address->telephone);

        if ($order->address->fullAddress) {
            $html[] = CHtml::encode($order->address->fullAddress);
        }

        $html[] = CHtml::link($order->ip, "http://www.utrace.de/?query={$order->ip}");

        echo implode('<br>', $html);
        break;
    case 'operator':
        if (!$order->operator) {
            break;
        }

        echo CHtml::link($order->operator->fullName, Yii::app()->controller->createUrl("operator", array("id" => $order->operator->primaryKey)), array("class" => "ajaxInfo"));
        break;

    case 'status':
        $canNotChangeStatusJs = "value == ".Order::COMPLETE." || value == ".Order::CANCELED." || value == ".Order::RETURNED;
        $noComment = ((!$order->comment) and ($order->comment=="")) ? 0 : 1 ;
       // $noComment = false;
        echo CHtml::dropDownList("status_" . $order->id, $order->status, $order->orderStatus, array(
            "ajax" => array(
                "type" => "POST",
                "url" => Yii::app()->controller->createUrl('status'),
                "beforeSend" => "js:function(){

                    if(".$noComment."==1){
                        var value = jQuery(this).val();
                        if(".$canNotChangeStatusJs.") {
                            if(!confirm('Изменить статус?')) {
                                return false;
                            }
                        }
                    }else{
                        alert('Не возможно изменить статус, не указано в комментарии причины отмены!');
                        return false;
                    }
                }"    ,
                "context" => "js:jQuery('#status_" . $order->primaryKey . "')",
                "success" => "js:function () {
                    jQuery(this).parents('td').prop('class', 'statusCell status_'+jQuery(this).val());
                    var value = jQuery(this).val();
                    jQuery(this).prop('disabled', ".$canNotChangeStatusJs.")
                }",
                "data" => "js:{
                    status: $(this).val(),
                    id: " . $order->id . ",
                }",
            ),
            "disabled" => !$order->canChangeStatus,
            "style" => "width:125px;",
        ));
        break;
}
