<?php
$order = Order::model();

if(Yii::app()->controller->action->id !='update' ){
$currency ='<div class="form-group"><label class="col-sm-2 control-label">Валюта</label>
           <div class="col-sm-8 success">' .

    CHtml::dropDownList('Currency_id',array('selected'=>2,'name'=>'Order[currency_id]') , CHtml::listData(Currency::model()->findAll(), 'id', 'code') ,array(
    'class' => 'form-control',

)) . '</div>
</div>';
}else
    $currency ="";
$priceField = '<div class="col-sm-8"><div class="input-group">
  <a class="input-group-addon add-on currencySymbol" href="#"></a>
  ' . CHtml::activeTextField($order, 'totalPrice', array(
    'class' => 'form-control orderPrice',
)) . '
  <a class="input-group-addon add-on js-recalculate" href=""><i class="icon-refresh"></i></a>
</div></div>';

return array(
    'buttons' => array(
        'submit' => array(
            'type' => 'success',
            'buttonType' => 'submit',
            'label' => Yii::app()->controller->action->id == 'update' ? 'Сохранить' : 'Оформить',
            'htmlOptions' => array(
                'class' => 'submit',
                'id' => 'submit',
                'data-loading-text' => 'Loading...',
            ),
        )
    ),
    'activeForm' => array(
        'class' => 'bootstrap.widgets.TbActiveForm',
        'id' => 'orderManageForm',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'htmlOptions' => array(
            'class' => 'form-horizontal',
        ),
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            // TODO: необходимо как-то вынести этот элемент либо в отдельный файл либо придумать как подловить через js событие успешной валидации. как вариант писать в aftervalidate колбек функцию. сейчас этот же код скопипасчен для вьюхи settings контроллера BeshopController

            // обработчики события afterValidate должны возвращать false, в том случае, если валидация прошла успешно
            'afterValidate' => 'js:function(form, data, hasError) {
        var afterValidateHasErrors = $("body").triggerHandler("afterValidate", [form, data, hasError]);
        var isNewOrder = ' . ((Yii::app()->controller->action->id == 'create') * 1) . ';

        //if no error in validation, send form data with Ajax
        if (!hasError) {
          $.ajax({
            type: "POST",
            url: form[0].action,
            context: form[0],
            data: $(form).serialize()+"&submit=1",
            success: function(response) {
              if (isNewOrder) {
                $(this).closest(".modal").find(".close").click();
              } else {
                $(this).parent().replaceWith($(response));
              }

              //$.fn.yiiGridView.update("orderIndex"); // обновим табличку
              $.sticky("Заказ был успешно обновлен", {autoclose : 5000, position: "top-center", type: "st-success" });
              document.location.href = "/shop";

            },
            error: function() {
              $.sticky("Во время обработки заказа возникли ошибки", {autoclose : 5000, position: "top-center", type: "st-error" });
            }
          });

          // блочим отправку формы, так как мы сделали это через аякс
          return false;
        }
        return (!hasError && !afterValidateHasErrors)
    }',
        ),
    ),
    'elements' => array(
        '<div class="panel panel-default panel-not-legend">',
        'check' => array(
            'type' => 'form',
            'title' => '<div class="panel-heading"><h2>Товары</h2></div>',
            'elements' => array(
                '<div class="panel-body">',
                    '<div class="form-group"><label class="col-sm-2 control-label">Код/название товара</label>',
                        '<div class="col-sm-8">',
                        'prod_id' => array(
                            'type' => 'text',
                            'label' => '',
                            'attributes' => array(
                                'name' => 'Object[object_id]',
                                'value' => '',
                                'placeholder' => 'Введите код/название товара',
                                'class' => 'form-control'
                            ),
                        ),
                        '</div>',
                    '</div>',

                '<div class="form-group"><div id="checkContainer"></div></div>'

                . $currency .
                    '<div class="form-group"><label class="col-sm-2 control-label">Сумма заказа</label>' . $priceField . '</div>',
                '</div>',
            ),
        ),
        '</div>',
        '<div class="panel panel-default panel-not-legend">',
        'client' => array(
            'type' => 'form',
            'title' => '<div class="panel-heading"><h2>Данные клиента</h2></div>',
            'elements' => array(
                '<div class="panel-body">',
                    '<div class="form-group"><label class="col-sm-2 control-label">Имя</label>',
                        '<div class="col-sm-8">',
                            'first_name' => array(
                                'type' => 'text',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                )
                            ),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Фамилия</label>',
                        '<div class="col-sm-8">',
                            'last_name' => array(
                                'type' => 'text',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                )
                            ),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Email (на емейл отправятся данные о заказе)</label>',
                        '<div class="col-sm-8">',
                            'email' => array(
                                'type' => 'text',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                )
                            ),
                        '</div>',
                    '</div>',
                '</div>'
            ),
        ),
        '</div>',
        '<div class="panel panel-default panel-not-legend">',
        'address' => array(
            'type' => 'form',
            'title' => '<div class="panel-heading"><h2>Адрес клиента</h2></div>',
            'elements' => array(
                '<div class="panel-body">',
                    '<div class="form-group"><label class="col-sm-2 control-label">Телефон</label>',
                        '<div class="col-sm-8">',
                            'telephone' => array(
                                'type' => 'cms.extensions.maskedInput.MaskedInputWidget',
                                'label' => '',
                                'htmlOptions' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Введите полный адрес</label>',
                        '<div class="col-sm-8">',
                            CHtml::textField('address_autocomplete', '', array(
                                'id' => 'address-autocomplete',
                                'placeholder' => _t('Enter client full adress...'),
                                'class' => 'form-control'
                            )),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Улица</label>',
                        '<div class="col-sm-8">',
                            'street' => array(
                                'type' => 'text',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Дом</label>',
                        '<div class="col-sm-8">',
                            'house' => array(
                                'type' => 'text',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Квартира</label>',
                        '<div class="col-sm-8">',
                            'flat' => array(
                                'type' => 'text',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Город</label>',
                        '<div class="col-sm-8">',
                            'city' => array(
                                'type' => 'text',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Страна</label>',
                        '<div class="col-sm-8">',
                            'country' => array(
                                'type' => 'text',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',
                '</div>',
            ),
        ),
        '</div>',
        '<div class="panel panel-default panel-not-legend">',
        'order' => array(
            'type' => 'form',
            'title' => '<div class="panel-heading"><h2>Оплата и доставка</h2></div>',
            'showErrorSummary' => true,
            'elements' => array(
                '<div class="panel-body">',
                    '<div class="form-group"><label class="col-sm-2 control-label">Сумма заказа</label>' . $priceField . '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Доставка</label>',
                        '<div class="col-sm-8">',
                            'type' => array(
                                'type' => 'dropdownlist',
                                'items' => $order->orderType,
                                'prompt' => '--Не выбрано--',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Валюта</label>',
                        '<div class="col-sm-8">',
                        'currency_id' => array(
                            'type' => 'dropdownlist',
                            'items' => CHtml::listData(Currency::model()->findAll(), 'id', 'code'),
                            'labelOptions' => array(
                                'label' => false
                            ),
                            'attributes' => array(
                              'class' => 'form-control'
                            ),
                            'options'=>array('2'=>array('selected'=>true)),
                        ),
                        '</div>',
                    '</div>',
                    '<div class="form-group"><label class="col-sm-2 control-label">Способ оплаты</label>',
                        '<div class="col-sm-8">',
                            'currency_provider' => array(
                                'type' => 'dropdownlist',
                                'items' => CHtml::listData(CurrencyProvider::model()->findAll(), 'id', 'name'),
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',                   
                    '<div class="form-group"><label class="col-sm-2 control-label">Статус заказа</label>',
                        '<div class="col-sm-8">',
                            'status' => array(
                                'type' => 'dropdownlist',
                                'items' => $order->orderStatus,
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',                    
                    '<div class="form-group"><label class="col-sm-2 control-label">Комментарий оператора</label>',
                        '<div class="col-sm-8">',
                            'comment' => array(
                                'type' => 'textarea',
                                'label' => '',
                                'attributes' => array(
                                    'class' => 'form-control'
                                ),
                            ),
                        '</div>',
                    '</div>',  
                '</div>'
            ),
        ),
        '</div>'
    ),
);
