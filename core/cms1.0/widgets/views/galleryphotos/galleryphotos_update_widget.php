<div class="form">
<?php
    $form=$this->beginWidget('CActiveForm', array(
    'id'=>'userupdate-form',
    'enableAjaxValidation'=>true,
));
?>
<a href="<?php echo Yii::app()->controller->createUrl('begalleries/photos', array('id' => $model->gallery_id))?>">Return to gallery</a>
<?php
echo $form->errorSummary($model); ?>
<div class="row">
    <?php echo $form->labelEx($model,'title'); ?>
    <?php echo $form->textField($model,'title'); ?>
    <?php echo $form->error($model,'title'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'description'); ?>
    <?php echo $form->textField($model,'description'); ?>
    <?php echo $form->error($model,'description'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'link'); ?>
    <?php echo $form->textField($model,'link'); ?>
    <?php echo $form->error($model,'link'); ?>
</div>
<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Save'),array('class'=>'btn')); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
