<?php
$this->beginWidget('bootstrap.widgets.TbBox', array(
    'title' => _t('Filters data'),
));
$filters = isset($model->filters) ? $model->filters : new Filters;
?>

<div class="row">
    <?php echo $form->labelEx($filters, 'id_options'); ?>
    <?php echo $form->dropDownList($filters, 'id_options', FiltersOptions::getList()); ?>
    <?php echo $form->error($filters, 'id_options'); ?>
</div>

<div id="filter_rows"></div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#Filters_id_options').change(function(){
            $.ajax({
                url: '<?php echo !isset($_GET['site']) || empty($_GET['site']) ? bu() : ''; ?>/befilters/getbackendform',
                type: 'get',
                data: {
                    foid: $(this).val(),
                    id: '<?php echo $filters->id;?>',
                    site: '<?php echo isset($_GET['site']) ? $_GET['site'] : '';?>',
                },
                success: function(data){
                    $('#filter_rows').html('').append($(data));
                }
            });
        }).change();
    });
</script>
<?php $this->endWidget();?>
