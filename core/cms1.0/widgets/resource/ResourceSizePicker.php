<?php

class ResourceSizePicker extends CInputWidget
{
    public $blockWidget;
    public $attribute;

    public function init()
    {
        if (!empty($this->blockWidget)) {
            $this->model = $this->blockWidget;
        }

        if (!property_exists($this->model, $this->attribute)) {
            throw new CException("Block should have `{$this->attribute}` property in order to use " . get_class($this) . ". You can change property name with " . get_class($this) . "::attribute.");
        }
    }

    public function run()
    {
        echo CHtml::dropDownList(
            "Block[{$this->attribute}]",
            $this->model->{$this->attribute},
            $this->getSizesList(),
            array(
                'empty' => 'Original size',
            )
        );
    }

    protected function getSizesList()
    {
        $sizes = CJSON::decode(Yii::app()->settings->get('system', 'sizes'));
        $sizes = is_array($sizes) ? $sizes : array();
        $data = array();
        foreach ($sizes as $size) {
            $height = empty($size['max_height']) ? 'Auto' : $size['max_height'];
            $data[$size['version']] = "{$size['max_width']}x{$height} ({$size['version']})";
        }

        return $data;
    }
}
