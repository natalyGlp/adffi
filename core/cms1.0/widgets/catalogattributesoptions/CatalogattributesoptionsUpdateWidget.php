<?php

/**
 * This is the Widget for Updating Model Information.
 *
 * @author Igogo <igogo@glp-centre.com>
 * @version 1.0
 * @package cms.widgets
 *
 */
class CatalogattributesoptionsUpdateWidget extends CWidget
{
    public $visible = true;
    public $model_name = 'CatalogAttributesOptions';

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model_id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
        if ($model_id !== 0) {
            $model_name = $this->model_name;
            if ($model_name != '') {
                $model = $model_name::model()->findbyPk($model_id);
                if (isset($_POST['ajax']) && $_POST['ajax'] === strtolower($model_name) . 'update-form') {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }

                if (isset($_POST[$model_name])) {
                    $model->setAttributes($_POST[$model_name], false);
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', _t('Updated Successfully!'));
                        Yii::app()->controller->refresh();
                    }
                }

                $this->render('cms.widgets.views.catalogattributesoptions.catalogattributesoptions_form_widget', array(
                    'model' => $model,
                ));
            } else {
                throw new CHttpException(404, _t('The requested page does not exist.'));
            }
        } else {
            throw new CHttpException(404, _t('The requested page does not exist.'));
        }
    }
}
