<?php

/**
 * This is the Widget for Updating Model Information.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.widgets
 *
 */
class PhotoUpdateWidget extends CWidget
{

    public $visible = true;
    public $model_name = 'GalleryPhotos';

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model_id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
        if ($model_id !== 0) {
            //  var_dump($_POST);
            $model_name = $this->model_name;
            if ($model_name != '') {
                $model = $model_name::model()->findbyPk($model_id);
                //  var_dump($model_id);
                // if it is ajax validation request
                if (isset($_POST['ajax']) && $_POST['ajax'] === strtolower($model_name) . 'update-form') {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }
                // collect user input data
                if (isset($_POST[$model_name])) {
                    $model->setAttributes($_POST[$model_name]);
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', _t('Updated Successfully!'));
                    }

                }

                $this->render('cms.widgets.views.galleryphotos.galleryphotos_update_widget', array('model' => $model));
            } else {
                throw new CHttpException(404, _t('The requested page does not exist.'));
            }

        } else {
            throw new CHttpException(404, _t('The requested page does not exist.'));
        }

    }
}
