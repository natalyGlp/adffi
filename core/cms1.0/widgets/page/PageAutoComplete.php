<?php

class PageAutoComplete extends CAutoComplete
{
    protected $targetFieldId;
    protected $targetFieldName;
    protected $targetFieldValue;

    public function init()
    {
        $this->setupWidget();

        parent::init();
    }

    public function run()
    {
        parent::run();

        echo CHtml::hiddenField($this->targetFieldName, $this->targetFieldValue, array(
            'id' => $this->targetFieldId
            ));
    }

    protected function setupWidget()
    {
        $this->saveTargetFieldNameID();

        Yii::import('cms.models.page.MenuItem');
        $this->url = array('bemenuitem/suggestPage', 'site' => isset($_GET['site']) ? $_GET['site'] : '');
        $this->methodChain = ".result(function(event,item){
            if(item!==undefined) {
                \$(\"#{$this->targetFieldId}\").val(item[1]);
            } else {
                \$(\"#{$this->targetFieldId}\").val('');
            }
        })";
        $this->multiple = false;
        $this->matchCase = true;
        $this->mustMatch = true;
    }

    protected function saveTargetFieldNameID()
    {
        // save target field name id
        list($name, $id) = $this->resolveNameID();
        $this->targetFieldId = $id;
        $this->targetFieldName = $name;
        $this->targetFieldValue = $this->value;

        // reset name for AutoComplete field
        $this->name = $this->id;
        $this->value = !empty($this->value) ? MenuItem::ReBindValueForMenuType(Menu::TYPE_PAGE, $this->value) : '';
    }
}
