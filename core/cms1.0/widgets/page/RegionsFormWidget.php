<?php
/**
 * Widget to manage regions of blocks
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 *
 * @version 1.0
 * @package cms.widgets.page
 */

class RegionsFormWidget extends CWidget
{
    /**
     * @var array of regionId => regionTitle
     */
    public $regions = array();

    /**
     * @var array(regionId => array('id' => blockId, 'title' => '', 'type' =>, 'status' => ''))
     */
    public $regionsBlocks = array();

    public $model;
    public $attribute = 'regions';

    public function init()
    {
        $this->registerClientScripts();
    }

    public function run()
    {
        $urlDefaultParams = array(
            'embed' => 'iframe',
            'site' => $this->controller->site,
            );

        $this->render('cms.widgets.views.region.regions', array(
            'createBlockUrl' => Yii::app()->createUrl('beblock/create', $urlDefaultParams),
            'updateBlockUrl' => Yii::app()->createUrl('beblock/update', $urlDefaultParams),
            'suggestBlockUrl' => Yii::app()->createUrl('beblock/suggestblock', $urlDefaultParams),
            'regions' => $this->regions,
            'regionsBlocks' => $this->regionsBlocks,
            'model' => $this->model,
            'attribute' => $this->attribute,
            ));
    }

    protected function registerClientScripts()
    {
        $assetsUrl = Yii::app()->assetManager->publish(dirname(__FILE__).'/assets', false, -1, YII_DEBUG);

        // красивые кнопочки toggle
        Yii::app()->clientScript
            ->registerCoreScript('jquery')
            ->registerScriptFile($assetsUrl.'/js/regions-app.js', CClientScript::POS_END)
            ;    
        GxcHelpers::registerCss('toggle_buttons/bootstrap-toggle-buttons.css');
        GxcHelpers::registerJs('toggle_buttons/jquery.toggle.buttons.js');
    }
}
