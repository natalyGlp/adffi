// TODO: regions И regionsViews -> private
// TODO: сделать класс коллекций
// TODO: autoResize
// TODO: initialize - events, el etc
// TODO: отслеживать смену id блоков и сейвить в индексе (события или дописать в create)
// TODO: убрать get().get() дети не должны знать о родителях родителей

(function($) {
    'use strict';

    function RegionCollection() {
        $('#page-regions-list').on('click', 'a', function(event) {
            // switch tabs
            event.preventDefault();

            $('#page-regions-list a').removeClass('tab-current');
            $('#page-regions-list li').removeClass('active');
            $("#page-regions-wrapper").children().hide();

            $(this)
                .addClass('tab-current')
                .parent('li').addClass('active')
                ;
            $('div#'+$(this).attr('rel')).show();
        });

        this.reset();
    }

    $.extend(RegionCollection.prototype, {
        reset: function() {
            this.regions = [];
            this.regionsViews = {};
            $('#page-regions-list').empty();
            $('#page-regions-wrapper').empty();

            return this;
        },

        getCurrentRegion: function() {
            var current_region = $('a.tab-current:first').attr('rel');
            current_region = current_region.split('_');
            current_region = current_region[2];

            return current_region;
        },

        create: function(data) {
            if(!(data instanceof Array)) {
                if(data.id) {
                    data = [data];
                }
            }

            $.each(data, $.proxy(function(index, data) {
                if(!$.isPlainObject(data)) {
                    data = {
                        id: index,
                        title: data
                    };
                }
                var region = new Region(data);

                region.render();

                this.add(region);
            }, this));

            if(!$('#page-regions-list a.tab-current').length) {
                $('#page-regions-list a').first().click();
            }
        },

        add: function(region) {
            this.regions.push(region.data.id);
            this.regionsViews[region.data.id] = region;

            var template = $('#template-tab').html();
            template = template.replace(/\{\{id\}\}/g, region.data.id);
            template = template.replace(/\{\{title\}\}/g, region.data.title);

            region.$el.hide();
            if(!$('#page-regions-list').find('.active').length) {
                $('#page-regions-list a').first().click();
            }

            $('#page-regions-wrapper').append(region.el);
            $('#page-regions-list').append(template);
        },

        insertBlocks: function(data) {
            if(!(data instanceof Array)) {
                data = [data];
            }

            $.each(data, $.proxy(function(index, data) {
                this.get(data.region).create(data);
            }, this));
        },

        // TODO: unused
        at: function(index) {
            var id = this.regions[index];
            return this.get(id);
        },

        get: function(id) {
            return this.regionsViews[id];
        }
    });

    function Region(data) {
        this.data = data;

        this.reset();
    }

    Region.suggestBlockUrl = '';
    Region.createBlockUrl = '';
    Region.updateBlockUrl = '';

    $.extend(Region.prototype, {
        reset: function() {
            this.blocks = [];
            this.blocksViews = {};
            this._ensureElement();
            this.$('.ul_region').empty();

            return this;
        },

        render: function() {
            this._ensureElement();

            this.el.id = 'div_region_'+this.data.id;
            this.el.className = 'div_regions';

            var template = $('#template-region').html();
            template = template.replace(/\{\{id\}\}/g, this.data.id);


            this.$el.html(template);

            this.$('.ul_region').sortable();

            $(window).on('message', $.proxy(this.messageHandler, this));
            this.$el.on('click', '.js-find-block', $.proxy(this.addExistedBlock, this));
            this.$el.on('click', '.js-new-block', $.proxy(this.addNewBlock, this));
            this.$el.on('click', '.js-check-all-blocks', $.proxy(this.selectAllBlocks, this));
            this.$el.on('click', '.js-active-checked-blocks', $.proxy(this.doActive, this));
            this.$el.on('click', '.js-disable-checked-blocks', $.proxy(this.doDisable, this));
            this.$el.on('click', '.js-delete-checked-blocks', $.proxy(this.doDelete, this));
        },

        create: function(data) {
            if(!(data instanceof Array)) {
                data = [data];
            }

            $.each(data, $.proxy(function(index, data) {
                var block = new Block($.extend({region: this.data.id}, data));

                block.region = this;

                block.render();

                this.add(block);
            }, this));
        },

        add: function(block) {
            this.blocks.push(block.data.id);
            this.blocksViews[block.data.id] = block;

            this.$('.ul_region').append(block.$el);
        },

        addExistedBlock: function(event) {
            event.preventDefault();

            this.navigateIframe(Region.suggestBlockUrl);
        },

        addNewBlock: function(event) {
            event.preventDefault();

            this.navigateIframe(Region.createBlockUrl);
        },

        selectAllBlocks: function(event) {
            var $checkboxes = this.$('.select-block');
            var isChecked = this.$('.js-check-all-blocks').is(':checked');

            $checkboxes.prop('checked', isChecked);
        },

        doActive: function(event) {
            // TODO: мы можем оперировать только с вьюхами блоков, а не с их DOM
            event.preventDefault();

            this.eachSelectedBlock(function() {
                $(this).closest('li').find('.js-block-status').prop('checked', true).change();
            });
        },

        doDisable: function(event) {
            event.preventDefault();

            this.eachSelectedBlock(function() {
                $(this).closest('li').find('.js-block-status').prop('checked', false).change();
            });
        },

        doDelete: function(event) {
            event.preventDefault();

            this.eachSelectedBlock(function() {
                $(this).closest('li').remove();
            });

            this.$('.js-check-all-blocks').prop('checked', false);
        },

        eachSelectedBlock: function(callback) {
            this.$('.select-block:checked').each(callback);
        },

        messageHandler: function(event) {
            var data = event.originalEvent.data;

            var iframe = event.originalEvent.source.frameElement;

            if(this.$('.region-iframe')[0] !== iframe) {
                return;
            }

            this.closeIframe();

            if(data.action != 'cancel' && !data.data) {
                throw new Error('There is no block data in message');
            }

            if(data.data) {
                this.create(data.data);
            }
        },

        navigateIframe: function(url) {
            var $iframe = this.$('.region-iframe');

            $iframe.attr('src', url);
            $iframe.attr('height', '30');
            $iframe.parent().show();
        },

        closeIframe: function() {
            this.navigateIframe('');
            this.$('.region-iframe').parent().hide();
        },

        // TODO: unused
        at: function(index) {
            var id = this.blocks[index];
            return this.get(id);
        },

        get: function(id) {
            return this.blocksViews[id];
        }
    });

    function Block(data) {
        this.data = $.extend({status: 1}, data);

        this.uid = Block.uid;
        Block.uid++;

        this.tagName = 'li';
    }
    Block.uid = 0;

    $.extend(Block.prototype, {
        render: function() {
            var index = this.region.blocks.length;

            var id = this.data.id;
            var region = this.data.region;
            var type = this.data.type;
            var title = this.data.title;
            var status = this.data.status;

            this._ensureElement();

            this.el.id = 'li_region_'+index+'_'+region+'_'+id;
            this.el.className = 'li_region';

            this.$el.empty();

            var template = $('#template-block').html();
            template = template.replace(/\{\{id\}\}/g, this.data.id);
            template = template.replace(/\{\{region\}\}/g, this.data.region);
            template = template.replace(/\{\{type\}\}/g, this.data.type);
            template = template.replace(/\{\{title\}\}/g, this.data.title);
            template = template.replace(/\{\{status\}\}/g, this.data.status);
            template = template.replace(/\{\{index\}\}/g, index);

            this.$el.html(template);

            this.$('.js-block-status[type=checkbox]').prop('checked', status > 0);
            this.$('.toggle-block').toggleButtons();

            this.$el.on('click.uid'+this.uid, '.js-remove', $.proxy(this.remove, this));
            this.$el.on('click.uid'+this.uid, '.js-change', $.proxy(this.changeToBlock, this));
            this.$el.on('click.uid'+this.uid, '.js-edit', $.proxy(this.edit, this));
            this.$el.on('change.uid'+this.uid, '.js-block-status', $.proxy(this.syncBlockStatus, this));
            $(window).on('message.uid'+this.uid, $.proxy(this.messageHandler, this));
        },

        remove: function(event) {
            event.preventDefault();
            $(window).off('.uid'+this.uid);
            $(this.$el).off('.uid'+this.uid);

            this.$el.remove();
        },

        edit: function(event) {
            event.preventDefault();

            var url = Region.updateBlockUrl+'&id='+this.data.id;

            this.navigateIframe(url);
        },

        changeToBlock: function(event) {
            event.preventDefault();

            this.navigateIframe(Region.suggestBlockUrl);
        },

        syncBlockStatus: function(event) {
            this.$('.js-block-status').val(this.$('.js-block-status[type=checkbox]').is(':checked')*1);
        },

        messageHandler: function(event) {
            var data = event.originalEvent.data;

            var iframe = event.originalEvent.source.frameElement;

            if(this.$('iframe')[0] !== iframe) {
                return;
            }

            this.closeIframe();

            if(data.action != 'cancel' && !data.data) {
                throw new Error('There is no block data in message');
            }

            if(data.data) {
                this.refresh(data.data);
            }
        },

        refresh: function(newData) {
            $.extend(this.data, newData);
            this.render();
        },

        navigateIframe: function(url) {
            var $iframe = this.$('iframe');

            $iframe.attr('src', url);
            $iframe.attr('height', '30');
            this.$('.iframe-container').show();
        },

        closeIframe: function() {
            this.navigateIframe('');
            this.$('.iframe-container').hide();
        }
    });


    Region.prototype._ensureElement = Block.prototype._ensureElement = function() {
        if(!this.el) {
            var tagName = this.tagName ? this.tagName : 'div';
            this.$el = $('<'+tagName+'>');
            this.el = this.$el[0];
            this.$ = $.proxy(this.$el.find, this.$el);
        }
    };

    // TODO: изолировать
    window.Region = Region; // что бы можно было настроить ссылки

    window.regions = new RegionCollection();
}(jQuery));