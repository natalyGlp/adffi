<?php

/**
 * This is the Widget for Suggesting a Block for a Page
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package  cmswidgets.page
 */
class BlockSuggestWidget extends CWidget
{

    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $this->render('cms.widgets.views.block.block_suggest_widget');
    }
}
