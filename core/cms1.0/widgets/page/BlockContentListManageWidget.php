<?php
/**
 * Widget to display form fields to manage content lists attached to block
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 *
 * @version 1.0
 * @package cms.widgets.page
 */

class BlockContentListManageWidget extends CInputWidget
{
    /**
     * @var BlockWidget $blockWidget экземпляр виджета блока
     */
    public $blockWidget;

    /**
     * @var string $attribute аттрибут, в котором должен храниться контент лист
     */
    public $attribute = 'contentLists';

    public function init()
    {
        if (!empty($this->blockWidget)) {
            $this->model = $this->blockWidget;
        }

        if (!property_exists($this->model, $this->attribute)) {
            throw new CException("Block should have `{$this->attribute}` property in order to use " . get_class($this) . ". You can change property name with " . get_class($this) . "::attribute.");
        }
    }

    public function run()
    {
        $this->render('cms.widgets.views.block.block_content_list', array(
            'blockWidget'  => $this->model,
            'attribute'    => $this->attribute,
            'contentLists' => $this->model->{ $this->attribute},
        ));
    }
}
