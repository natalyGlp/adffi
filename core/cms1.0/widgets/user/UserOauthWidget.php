<?php

/**
 * This is the Widget for configuration of openauth
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.user
 *
 */
Yii::import('cms.extensions.hoauth.*');
class UserOauthWidget extends CWidget
{
    public $visible=true;

    public function run()
    {
        if($this->visible)
        {
            $action = new HOAuthAdminAction($this->controller, $this->id);
            $action->configPath = FRONT_END . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'hoauth.php';
            $action->endPointUrl =  FRONT_SITE_URL . '/app/oauth';

            $this->render('cms.widgets.views.user.user_oauth_widget',array(
                'form'=> $action->form,
            ));
        }
    }
}
