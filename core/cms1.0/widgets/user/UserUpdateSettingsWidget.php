<?php

/**
 * This is the Widget for User update his own settings.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.widgets.user
 *
 */
class UserUpdateSettingsWidget extends CWidget
{

    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        if (!Yii::app()->user->isGuest) {

            $user = User::model()->findbyPk(Yii::app()->user->id);
            $model = new UserSettingsForm();

            //Bind Value from User to
            $model->display_name = $user->display_name;
            $model->email = $user->email;

            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'userupdatesettings-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset($_POST['UserSettingsForm'])) {
                $model->attributes = $_POST['UserSettingsForm'];

                // validate user input password
                if ($model->validate()) {
                    $u = User::model()->findbyPk(Yii::app()->user->id);
                    $u->scenario = 'update';
                    if ($u !== null) {
                        $u->display_name = $model->display_name;
                        $u->email = $model->email;
                        if ($u->save()) {
                            Yii::app()->user->setFlash('success', _t('Updated Successfully!'));
                        }
                    }
                    $model->display_name = $u->display_name;
                    $model->email = $u->email;

                }
            }

            $this->render('cms.widgets.views.user.user_update_settings_widget', array('model' => $model));
        } else {
            Yii::app()->request->redirect(Yii::app()->user->returnUrl);
        }

    }
}
