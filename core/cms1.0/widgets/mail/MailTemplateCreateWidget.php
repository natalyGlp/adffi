<?php

/**
 * This is the Widget for Creating new Term
 *
 * @author Yura Kovalenko <codexmen@studiobanzai.com>
 * @version 1.0
 * @package  cms.widgets.object
 */
class MailTemplateCreateWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model = new MailTemplate();

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'mailtemplate-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['MailTemplate'])) {
            $model->attributes = $_POST['MailTemplate'];

            if ($model->save()) {
                Yii::app()->user->setFlash('success', _t('Create new Mail Template Successfully!'));

                if (!isset($_GET['embed'])) {
                    $model = new MailTemplate;
                    Yii::app()->controller->redirect(array(
                        'create',
                        'site' => Yii::app()->controller->site
                    ));
                }
            }
        }
        $this->render('cms.widgets.views.mailtemplate.mailtemplate_form_widget', array(
            'model' => $model
        ));
    }
}
