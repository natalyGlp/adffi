<?php

/**
 * This is the Widget for Viewing Model information.
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.widgets
 *
 */
class ModelViewWidget extends CWidget
{
    public $visible = true;
    public $model_name = '';
    public $folder = false;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model_name = $this->model_name;
        if ($model_name != '') {
            $id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
            $model = GxcHelpers::loadDetailModel($model_name, $id);
            $folder = $this->folder ? $this->folder : $model_name;
            $this->render(strtolower($folder) . '/' . strtolower($model_name) . '_view_widget', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(404, _t('The requested page does not exist.'));
        }
    }
}
