<?php

/**
 * This is the Widget for backuping database
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 * @version 1.0
 * @package cms.widgets.admin
 */
class BackupWidget extends CWidget
{
    public $visible = true;

    public function init()
    {
        Yii::import('cms.extensions.yii-database-dumper.SDatabaseDumper');
    }

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        // Скачивание бекапа
        if (isset($_GET['download']) && !empty($_GET['download'])) {
            $this->actionDownload($_GET['download']);
        }

        // Восстановление из бекапа
        if (isset($_GET['restore']) && !empty($_GET['restore'])) {
            $this->actionRestore($_GET['restore']);
        }

        // удаление бекапа
        if (isset($_GET['delete']) && !empty($_GET['delete'])) {
            $this->actionDelete($_GET['delete']);
        }

        if (Yii::app()->request->isPostRequest) {
            if (isset($_POST['flushDb']) && $_POST['flushDb']) {
                $this->actionFlush();
            } elseif (isset($_FILES['dump'])) {
                $this->actionUpload($_FILES['dump']);
            } else {
                if (isset($_POST['blocks_only'])) {
                    $this->actionBackupBlocks();
                } else {
                    $this->actionBackupAll();
                }
            }
        }

        // список файлов в директории
        $fileListOfDirectory = array();
        $pathTofileListDirectory = $this->backupDir;
        foreach (new DirectoryIterator($pathTofileListDirectory) as $file) {
            if ($file->isFile() === true) {
                array_push($fileListOfDirectory, array(
                    'name' => $file->getBasename(),
                    'size' => $file->getSize(),
                    'time' => $file->getMTime()
                ));
            }
        }

        $dataProvider = new CArrayDataProvider($fileListOfDirectory, array(
            'keyField' => false,
            'pagination' => array(
                'pageSize' => 20,
            ),
            'sort' => array(
                'attributes' => array(
                    'time',
                ),
                'defaultOrder' => array(
                    'time' => CSort::SORT_DESC,
                )
            ),
        ));

        $this->render('cms.widgets.views.admin.backup', array(
            'dataProvider' => $dataProvider,
        ));
    }

    protected function actionDownload($file)
    {
        $sqlFile = $this->getSqlFile($file);

        header('Content-Disposition: attachment; filename="' . basename($sqlFile) . '"');
        header('Content-type: application/x-gzip; name=' . basename($sqlFile));
        header('Content-Length: ' . filesize($sqlFile));

        readfile($sqlFile);
        Yii::app()->end();
    }

    protected function actionRestore($file)
    {
        $sqlFile = $this->getSqlFile($file);
        $sql = file_get_contents($sqlFile);
        if (strpos($sqlFile, 'gz')) {
            $sql = gzinflate(substr($sql, 10, -8));
        }

        // запускаем sql комманды
        $db = Yii::app()->{CONNECTION_NAME};
        $command = $db->createCommand($sql);
        $rowCount = $command->execute();

        Yii::app()->user->setFlash('success', 'База успешно восстановлена. Затронуто строк: ' . $rowCount);
        Yii::app()->controller->redirect(array('backup', 'site' => Yii::app()->controller->site));
    }

    protected function actionDelete($file)
    {
        $sqlFile = $this->getSqlFile($file);
        if (unlink($sqlFile) === true) {
            Yii::app()->user->setFlash('success', 'Бекап ' . $file . ' успешно удален');
        } else {
            Yii::app()->user->setFlash('error', 'Не удалось удалить бекап ' . $file);
        }
        Yii::app()->controller->redirect(array('backup', 'site' => Yii::app()->controller->site));
    }

    protected function actionFlush()
    {
        $dumper = $this->getDumper();
        if ($dumper->flushDb()) {
            Yii::app()->user->setFlash('success', 'База успешно очищена');
        } else {
            Yii::app()->user->setFlash('error', 'При очистке базы данных произошла ошибка');
        }

        Yii::app()->controller->refresh();
    }

    protected function actionBackupAll()
    {
        $file = $this->backupDir . '/' . $this->generateDumpName();

        $this->backupToFile($file);

        Yii::app()->controller->refresh();
    }

    protected function actionBackupBlocks()
    {
        $file = $this->backupDir . '/' . $this->generateDumpName('blocks');

        $this->backupToFile($file, array(
            'tableFilter' => function ($tableName) {
                return preg_match('/page|block|content_list/', $tableName);
            },
        ));

        Yii::app()->controller->refresh();
    }

    protected function backupToFile($destination, $options = array())
    {
        $dumper = $this->getDumper();
        $dump = $dumper->getDump($options);

        if (function_exists('gzencode')) {
            file_put_contents($destination . '.gz', gzencode($dump));
        } else {
            file_put_contents($destination, $dump);
        }
    }

    protected function generateDumpName($prefix = '')
    {
        if (!empty($prefix)) {
            $prefix .= '_';
        }

        return $prefix . 'dump_nespi_' . substr(Yii::app()->request->getHostInfo(null), 3) . '_' . date('Y-m-d_H_i_s') . '.sql';
    }

    protected function actionUpload($file)
    {
        $isSql = strpos($file['type'], 'sql') !== false;
        $isZip = strpos($file['type'], 'gz') !== false;

        if (!$isSql && !$isZip) {
            Yii::app()->controller->refresh();
        }

        $destination = $this->backupDir . '/' . $file['name'];
        if (move_uploaded_file($file['tmp_name'], $destination)) {
            @chmod($destination, 0777);
            Yii::app()->user->setFlash('success', 'Файл ' . $file['name'] . ' успешно загружен');
        } else {
            Yii::app()->user->setFlash('error', 'Не удалось загрузить файл ' . $file['name']);
        }
        Yii::app()->controller->refresh();
    }

    /**
     * Производит базовую фильтрацию имени файла
     * @param  string $fileName имя файла
     * @return string           отфильтрованное имя файла с путем к нему
     */
    protected function getSqlFile($fileName)
    {
        $fileName = str_replace('/', '', $fileName);

        $sqlFile = $this->backupDir . DIRECTORY_SEPARATOR . $fileName;

        if (!file_exists($sqlFile)) {
            throw new CException('Invalid file');
        }

        return $sqlFile;
    }

    /**
     * @return string путь к директории с бекапами
     */
    protected function getBackupDir()
    {
        $backupDir = Yii::app()->runtimePath . DIRECTORY_SEPARATOR . 'backup';
        if (!is_dir($backupDir)) {
            mkdir($backupDir);
        }

        return $backupDir;
    }

    protected function getDumper()
    {
        $dumper = new SDatabaseDumper();
        $dumper->setDbConnection(Yii::app()->{CONNECTION_NAME});
        return $dumper;
    }
}
