<?php

/**
 * This is the Widget for Creating new Model
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.widgets
 *
 */
class AlbumsUpdateWidget extends CWidget
{
    public $visible = true;
    public $model_name = 'Albums';
    public $id = '';

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model_name = $this->model_name;
        if ($model_name != '') {
            $model = Albums::model()->findByPk($this->id);

            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === strtolower($model_name) . 'create-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset($_POST[$model_name])) {
                $model->attributes = $_POST[$model_name];
                if ($model->save()) {
                    $model = Albums::model()->findByPk($this->id);
                    Yii::app()->user->setFlash('success', _t('Update Successfully!'));
                }
            }

            $this->render('cms.widgets.views.albums.albums_create_widget', array(
                'model' => $model,
                'galls' => Galleries::model()->findAll(),
            ));
        } else {
            throw new CHttpException(404, _t('The requested page does not exist.'));
        }
    }
}
