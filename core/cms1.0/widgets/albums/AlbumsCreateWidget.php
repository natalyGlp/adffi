<?php

/**
 * This is the Widget for Creating new Model
 *
 * @author Tuan Nguyen <nganhtuan63@gmail.com>
 * @version 1.0
 * @package cms.widgets
 *
 */
class AlbumsCreateWidget extends CWidget
{
    public $visible = true;
    public $model_name = 'Albums';

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model_name = $this->model_name;
        if ($model_name != '') {
            $model = new $model_name();

            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === strtolower($model_name) . 'create-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset($_POST[$model_name])) {
                $model->attributes = $_POST[$model_name];

                if ($model->save()) {
                    Yii::app()->user->setFlash('success', _t('Create new {name} Successfully!', 'cms', array(
                        '{name}' => $model_name,
                    )));
                    $model = new $model_name();
                }
            }

            $this->render('cms.widgets.views.albums.albums_create_widget', array(
                'model' => $model,
                'galls' => Galleries::model()->findAll()
            ));
        } else {
            throw new CHttpException(404, _t('The requested page does not exist.'));
        }
    }
}
