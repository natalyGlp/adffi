<?php
/**
 * Виджет привязки одних обьектов к другим
 *
 * @author Sviatoslav Danylenko <dev@udf.su>
 *
 * @version 1.0
 * @package common.front_blocks.feedback
 */

class ShopProductVariants extends CWidget
{
    public $model;

    public function init()
    {
        if (!isset($this->model) && $this->model->object_type == 'catalog') {
            throw new CException('ShopProductVariants::model is required and should by Object of type catalog');
        }

        if (Yii::app()->request->isAjaxRequest && isset($_GET['addVariant'])) {
            $this->returnVariant();
        }

        $this->registerClientScripts();
    }

    public function run()
    {
        $variants = array();
        if ($this->model->id_parent > 0) {
            $object = CatalogObject::model()->findByPk($this->model->id_parent);
            if ($object) {
                $objectUpdateUrl = Yii::app()->createUrl('shop/product/update', array('id' => $object->primaryKey, 'type' => $object->object_type));
                $content = CHtml::link('This object is Variant of ' . $object->object_name, $objectUpdateUrl);
            }
        } else {
            $variants = $this->model->variants;
        }

        if (!isset($content)) {
            $content = $this->render('cms.widgets.views.shop._product_variants_form', array(
                'variants' => $variants,
                'id' => $this->id,
            ), true);
        }

        $this->widget('bootstrap.widgets.TbBox', array(
            'title' => _t('Продукты'),
            'content' => $content,
            'id' => $this->id,
        ));
    }

    protected function returnVariant()
    {
        while(@ob_end_clean());

        $object = CatalogObject::model()->findByPk($_POST['id']);

        if (!$object) {
            $object = new CatalogObject();
            $object->object_name = $_POST['name'];
        }

        $this->renderVariant($object);

        Yii::app()->end();
    }

    protected function renderVariant($object, $index = '')
    {
        // TODO: ссылка должна работать как в shop сателлитке так и в обычной
        // $objectUpdateUrl = Yii::app()->createUrl('beobject/update', array('id' => $object->primaryKey, 'type' => $object->object_type));
        $objectUpdateUrl = Yii::app()->createUrl('shop/product/update', array('id' => $object->primaryKey, 'type' => $object->object_type));
        ?>
        <tr>
            <td style="vertical-align: middle; width:20px;">
                <a href="#" class="icon-move"></a>
            </td>
            <td>
                <?php
                $img = GxcHelpers::getObjectThumb($object);
                echo $object->isNewRecord ? $img : CHtml::link($img, $objectUpdateUrl);
                ?>
            </td>
            <td>
                <?php
                echo CHtml::textField('ProductVariants['.$index.'][object_name]', $object->object_name)
                    .CHtml::hiddenField('ProductVariants['.$index.'][object_id]', $object->primaryKey);
                ?>
            </td>
            <!--td>
                <?php /*echo CHtml::textField('ProductVariants['.$index.'][price]', $object->price);*/ ?>
            </td>
            <td>
                <?php /*echo CHtml::textField('ProductVariants['.$index.'][product_count]', $object->product_count);*/ ?>
            </td-->
            <td>
                <?php
                if (!$object->isNewRecord) {
                    echo CHtml::link('<i class="icon-pencil"></i>', $objectUpdateUrl, array(
                        'class' => 'btn',
                    ));
                }
                ?>
                <a class="btn btn-danger" onclick="$(this).parents('tr').hide(200, function() {$(this).remove();});return false;"><i class="icon-trash"></i></a>
            </td>
        </tr>
        <?php
    }

    protected function registerClientScripts()
    {
        Yii::app()->clientScript
            ->registerPackage('jquery')
            ->registerPackage('jquery.ui')
            ->registerScript(__FILE__.'#sortable'.$this->id, '(function(){
                $("#'.$this->id.' tbody").sortable({
                    stop: function() {
                        $("#'.$this->id.' tbody tr").each(changePostIndex);
                    }
                });
                function changePostIndex(index)
                {
                    var $inputs = $("input", this);
                    $inputs.each(function() {
                        var name = $(this).attr("name");
                        name = name.replace(/\[\d+\]/, "["+index+"]");

                        $(this).attr("name", name);
                    })
                }
            }())');
    }
}
